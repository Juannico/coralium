﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[InitializeOnLoad]
public class Initializer
{
	private static bool IsInitialized = false;

	static Initializer()
	{
		EditorApplication.update += Update;
	}

	static void Update()
	{
		if (IsInitialized == false)
		{
			if (PlayerPrefs.GetInt("DontShowAgain") == 0)
			{
				WelcomeMessage window = (WelcomeMessage)EditorWindow.GetWindow(typeof(WelcomeMessage), true, "Ultimate GPU Particle System");
				window.maxSize = new Vector2(600f, 800f);
				window.minSize = new Vector2(600f, 400f);
				window.Show();
			}

			IsInitialized = true;
			EditorApplication.update -= Update;
		}
	}
}

public class WelcomeMessage : EditorWindow
{
	
	private static bool dontShowAgain = false;
	private static bool tab0 = false;
	private static bool tab1 = false;
	private static bool tab2 = false;
	private static bool tab3 = false;
	private static bool tab4 = false;
	private static bool tab5 = false;
	private static bool tab6 = false;
	private static bool tab7 = false;
	private static Texture2D image;
	private Vector2 scroll = new Vector2();
	private static string persistenDataPath = "";
	private static string dataPath = "";

	private void OnEnable()
	{
		persistenDataPath = Application.persistentDataPath;
		dataPath = Application.dataPath;
		image = new Texture2D(1, 1);
		image.LoadImage(System.IO.File.ReadAllBytes(dataPath + "/Ultimate GPU Particle System/Resources/Logo.png"));
		image.Apply();

		if (PlayerPrefs.GetInt("DontShowAgain") == 0)
		{
			dontShowAgain = false;
		}
		else
		{
			dontShowAgain = true;
		}
	}

	[MenuItem("Window/WelcomeMessage")]
	static void Init()
	{
		WelcomeMessage window = (WelcomeMessage)EditorWindow.GetWindow(typeof(WelcomeMessage));
		window.maxSize = new Vector2(600f, 800f);
		window.minSize = new Vector2(600f, 400f);
		window.Show();
	}

	void OnGUI()
	{
		Rect r = GUILayoutUtility.GetRect(600f, 110f);
		GUI.DrawTexture(r, image);

		GUIStyle gs = new GUIStyle(GUI.skin.label);
		gs.alignment = TextAnchor.MiddleLeft;
		gs.fontStyle = FontStyle.Bold;

		EditorGUILayout.BeginVertical("Box");
		{
			EditorStyles.label.wordWrap = true;
			EditorGUILayout.LabelField("Ultimate GPU Particle System v1.2", EditorStyles.boldLabel);
			EditorGUILayout.LabelField("Thank you very much for purchasing Ultimate GPU Particle System. If you experience any bugs or have questions regarding the particle system, please contact me via m4xproud@gmail.com. Below you can find some frequently asked questions, tips and tricks and notifications.");
			//GUILayout.FlexibleSpace();

			scroll = EditorGUILayout.BeginScrollView(scroll, GUILayout.Width(580f), GUILayout.Height(Screen.height - 230f));
			{
				EditorGUILayout.BeginVertical("Box");
				{
					if (GUILayout.Button("What's new", gs))
					{
						tab0 = !tab0;
					}

					if (tab0)
					{
						EditorGUILayout.LabelField("Version 1.2 comes with a variety of new features, fixes and improvements. The biggest new feature is the texture target feature. See the documentation for further info.");

					}
				}
				EditorGUILayout.EndHorizontal();

				EditorGUILayout.BeginVertical("Box");
				{
					if (GUILayout.Button("Getting started", gs))
					{
						tab1 = !tab1;
					}

					if (tab1)
					{
						EditorGUILayout.LabelField("To create a new GPU particle system, go to the toolbar and click GameObject/Effects/GPU Particle System. This will add a new GPU Particle System to the scene and can be edited using the inspector very much like Shuriken.");

					}
				}
				EditorGUILayout.EndHorizontal();

				EditorGUILayout.BeginVertical("Box");
				{
					if (GUILayout.Button("Trails", gs))
					{
						tab7 = !tab7;
					}

					if (tab7)
					{
						EditorGUILayout.LabelField("Trails are stored in the Particle Buffer. That means, that the Particle width and heigth information changes to Num trails and Num segments. Each segment follows its preceding segment in the Position Buffer. By changing segment count or the follow speed setting in the rendering tab, you can make trails shorter or longer. The first element of a trail behaves just like a particle and is affected by forces and other manipulators. Please see the documentation for further details.");

					}
				}
				EditorGUILayout.EndHorizontal();

				EditorGUILayout.BeginVertical("Box");
				{
					if (GUILayout.Button("Render Pipelines", gs))
					{
						tab2 = !tab2;
					}

					if (tab2)
					{
						EditorGUILayout.LabelField("Ultimate GPU Particle System supports HDRP and URP. However, the Demos use the standard Input system. HDRP by default imports the new ImportSystem and will spam errors. To fix this, you can change the Input method in the Project settings. This is only necessary to test the demos.");
					}
				}
				EditorGUILayout.EndVertical();

				EditorGUILayout.BeginVertical("Box");
				{
					if (GUILayout.Button("First time import", gs))
					{
						tab3 = !tab3;
					}

					if (tab3)
					{
						EditorGUILayout.LabelField("If you import Ultimate GPU Particle system for the first time, errors can occur. Usually you can fix them by right-clicking on the Ultimate GPU Particle System folder and clicking reimport. This usually resolves errors.");

					}
				}
				EditorGUILayout.EndVertical();

				EditorGUILayout.BeginVertical("Box");
				{
					if (GUILayout.Button("Best practices", gs))
					{
						tab4 = !tab4;
					}

					if (tab4)
					{
						EditorGUILayout.LabelField("Ultimate GPU Particle System is a pseudo-instanced gpu particle system. That means, that all particles are always generated and ready in memory for usage. That makes it very fast and efficient but it has a few disadvantages: \n\n 1. Use as few emitters as possible. If you have the same effect multiple times, use the same particle system again using Emit(). \n\n 2. Make sure that you don't use more particles than necessary. The buffer width and height define the size in RAM. \n\n 3. Avoid transparent overdraw. Even though the meta data of particles can be efficiently calculated using shaders, filling the screen with millions of screen size particles is simply not possible with modern hardware.");
					}
				}
				EditorGUILayout.EndVertical();

				EditorGUILayout.BeginVertical("Box");
				{
					if (GUILayout.Button("Shader keywords update", gs))
					{
						tab5 = !tab5;
					}

					if (tab5)
					{
						EditorGUILayout.LabelField("Previous versions of Ultimate GPU Particle System used shader keywords such as multi_compile and shader_feature. Shaders now are compiled by using local shader keywords and are added to a shader variant collection, which you can find in the main folder:\n\n Ultimate GPU Particle System/Resources/ShadersShaderVariants \n\n This doesn't use up any shader keywords for your project and doesnt require to compile all variants. Instead only variants used in the project are added to the collection. Always make sure to include it in your build!");
					}
				}
				EditorGUILayout.EndVertical();

				EditorGUILayout.BeginVertical("Box");
				{
					if (GUILayout.Button("Post processing", gs))
					{
						tab6 = !tab6;
					}

					if (tab6)
					{
						EditorGUILayout.LabelField("To make post effects work, you have to import the post processing package from the package manager. Go to Toolbar>Window>Package Manager. In the top left, enable all packages. After the preview packages are refreshed, click on Post Processing and choose version 2.1.2. Then click download.");
					}
				}
				EditorGUILayout.EndVertical();

			}
			EditorGUILayout.EndScrollView();
		}
		EditorGUILayout.EndVertical();

		GUILayout.FlexibleSpace();
		
		EditorGUILayout.BeginVertical();
		{
			EditorGUI.BeginChangeCheck();
			dontShowAgain = EditorGUILayout.Toggle("Don't show again", dontShowAgain);

			if (EditorGUI.EndChangeCheck())
			{
				if (dontShowAgain == true)
				{
					PlayerPrefs.SetInt("DontShowAgain", 1);
				}
				else
				{
					PlayerPrefs.SetInt("DontShowAgain", 0);
				}
			}
		}
		EditorGUILayout.EndVertical();
	}
	
}