﻿using UnityEngine;

[System.Serializable]
public class SingleVector3CurveBundle
{
#if UNITY_EDITOR
    public bool showEditor = false;
#endif

	public GPUParticleSystem.SimpleValueMode mode = GPUParticleSystem.SimpleValueMode.Value;
    public Vector3 value1 = Vector3.zero;
    public AnimationCurve curve1_1 = AnimationCurve.Linear(0f, 1f, 1f, 1f);
    public AnimationCurve curve1_2 = AnimationCurve.Linear(0f, 1f, 1f, 1f);
    public AnimationCurve curve1_3 = AnimationCurve.Linear(0f, 1f, 1f, 1f);
    public Vector2 minMax = new Vector2(0f,1f);
    public float seed = 0f;

	public SingleVector3CurveBundle(Vector3 v1)
	{
		value1 = v1;
	}

	public SingleVector3CurveBundle()
	{
		value1 = Vector3.one;
	}
}
