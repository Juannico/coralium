﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class AnimateParticleSystem : MonoBehaviour
{
	public float speed = 5f;
	public Vector2 strength = new Vector2(16f, 7f);
	public float zDistance = 22f;
	public bool interactive = true;

	void Start () {
		Application.targetFrameRate = 60;
	}
	
	void Update ()
	{
		if (Input.GetMouseButton(0) && interactive)
		{
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit rayHit = new RaycastHit();

			if (Physics.Raycast(ray, out rayHit))
			{
				transform.position = rayHit.point;
			}
		}
		else
		{
			transform.position = new Vector3(Mathf.Sin(Time.time * speed) * strength.x, Mathf.Sin(Time.time * 2f * speed) * strength.y, zDistance);
		}
	}
}
