%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: BackFinPlayer
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: bend1Handle
    m_Weight: 0
  - m_Path: bend2Handle
    m_Weight: 0
  - m_Path: j_center
    m_Weight: 1
  - m_Path: j_center/j_aleta_baja_der
    m_Weight: 0
  - m_Path: j_center/j_aleta_baja_der/j_aleta_baja_der2
    m_Weight: 1
  - m_Path: j_center/j_aleta_baja_der/j_aleta_baja_der2/j_aleta_baja_der3
    m_Weight: 1
  - m_Path: j_center/j_aleta_baja_izq
    m_Weight: 0
  - m_Path: j_center/j_aleta_baja_izq/j_aleta_baja_izq2
    m_Weight: 1
  - m_Path: j_center/j_aleta_baja_izq/j_aleta_baja_izq2/j_aleta_baja_izq3
    m_Weight: 1
  - m_Path: j_center/j_aleta_der
    m_Weight: 0
  - m_Path: j_center/j_aleta_der/j_aleta_der2
    m_Weight: 1
  - m_Path: j_center/j_aleta_izq
    m_Weight: 0
  - m_Path: j_center/j_aleta_izq/j_aleta_izq2
    m_Weight: 1
  - m_Path: j_center/j_aleta_superior
    m_Weight: 0
  - m_Path: j_center/j_aleta_superior/j_aleta_superior
    m_Weight: 1
  - m_Path: j_center/joint2
    m_Weight: 1
  - m_Path: j_center/joint2/j_cabeza
    m_Weight: 0
  - m_Path: j_center/joint2/j_cabeza/joint1
    m_Weight: 1
  - m_Path: j_center/joint2/j_cadera_1
    m_Weight: 1
  - m_Path: j_center/joint2/j_cadera_1/j_cadera_2
    m_Weight: 1
  - m_Path: j_center/joint2/j_cadera_1/j_cadera_2/j_cadera_3
    m_Weight: 1
  - m_Path: j_center/joint2/j_cadera_1/j_cadera_2/j_cadera_3/j_cola
    m_Weight: 1
  - m_Path: j_center/joint2/j_cadera_1/j_cadera_2/j_cadera_3/j_cola/j_cola_1
    m_Weight: 1
  - m_Path: j_center/joint2/j_cadera_1/j_cadera_2/j_cadera_3/j_cola/j_cola_2
    m_Weight: 1
  - m_Path: Normal
    m_Weight: 0
