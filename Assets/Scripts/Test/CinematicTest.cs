using UnityEngine;

public class CinematicTest : MonoBehaviour
{
    [SerializeField] private CollisionHelper checkpoint;
    [SerializeField] private string text;
    private void OnEnable()
    {
        if (checkpoint != null)
            checkpoint.TriggerEnter += MyFuction;
    }
    private void OnDisable()
    {
        if (checkpoint != null)
            checkpoint.TriggerEnter -= MyFuction;
    }
    private void MyFuction(Collider collider)
    {
        if (collider.CompareTag("Player"))
            Debug.Log(collider.name + "_" + text);
    }
}
