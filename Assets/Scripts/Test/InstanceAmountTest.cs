using NaughtyAttributes;
using UnityEngine;

public class InstanceAmountTest : MonoBehaviour
{
    [SerializeField] private float amountToInstance;
    
    [Button("Instiantate", enabledMode: EButtonEnableMode.Editor)]
    public void Instiante()
    {

        for (int i = 0; i < amountToInstance; i++)
        {
            GameObject instance = Instantiate(gameObject,transform.parent);
            instance.transform.position += Vector3.up * 1.25f * (i + 1);
        }
    }
}
