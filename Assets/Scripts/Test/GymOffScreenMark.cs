using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GymOffScreenMark : MonoBehaviour
{
    [SerializeField] private GymMarkUI markUIprefab;
    public string MarkUIText;
    private MarkUI markUI;
    private MarkUIManager markUIManager;
    private bool isActivated;

    private void Awake()
    {
        if (markUIprefab == null)
            throw new System.Exception($" MarkUIprefab is not assigned in GameObject : {gameObject.name}");
        markUIManager = GameManager.Instance.UIManager.MarkUIManager;
        markUI = markUIManager.GetMarkUI(markUIprefab);
        SetTargetMark(transform);
        SetText(MarkUIText);
    }
    private void OnEnable()
    {
        if (isActivated)
            markUI.gameObject.SetActive(true);
    }
    private void OnDisable()
    {
        if (markUI != null)
            markUI.gameObject.SetActive(false);
    }
    public void SetActive(bool state)
    {
        markUI.gameObject.SetActive(state);
        isActivated = state;
    }
    public void SetText(string text) => (markUI as GymMarkUI).SetText(text);
    public void SetTargetMark(Transform transform) => markUI.ShowMark(transform, Vector3.zero);
}
