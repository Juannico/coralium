using NaughtyAttributes;
using UnityEditor;
using UnityEngine;

public class MaterialTest : MonoBehaviour
{
#if UNITY_EDITOR
    [SerializeField] private Material sourceMaterial;
    // _AlphaP
    private SetMaterialProvider[] setMaterialProviders;
    private SetMaterialProvider setMaterialProvider;

    private void Awake()
    {
        setMaterialProviders = GetComponentsInChildren<SetMaterialProvider>();
        setMaterialProvider = GetComponent<SetMaterialProvider>();
    }
    [Button("Change Alpha By Children", enabledMode: EButtonEnableMode.Playmode)]
    private void ChangeAlphaByChildren()
    {
        float startTime = Time.realtimeSinceStartup;

        for (int i = 0; i < setMaterialProviders.Length; i++)
        {
            setMaterialProviders[i].SetMaterial(sourceMaterial, (material) =>
            {
                material.SetFloat("_AlphaP", 1);
            });
        }

        float endTime = Time.realtimeSinceStartup;
        float elapsedTime = endTime - startTime;
        Debug.Log("Elapsed Time Change Alpha By Children: " + elapsedTime + " seconds");
        EditorApplication.isPaused = true;
    }
    [Button("Change Alpha By Parent", enabledMode: EButtonEnableMode.Playmode)]
    private void ChangeAlphaByParent()
    {
        float startTime = Time.realtimeSinceStartup;

        setMaterialProvider.SetMaterial(sourceMaterial, (material) =>
        {
            material.SetFloat("_AlphaP", 1);
        });

        float endTime = Time.realtimeSinceStartup;
        float elapsedTime = endTime - startTime;
        Debug.Log("Elapsed Time Change Alpha By Parent: " + elapsedTime + " seconds");
        EditorApplication.isPaused = true;
    }
    [Button("Switchc By Children", enabledMode: EButtonEnableMode.Playmode)]
    private void SwitchcByChildren()
    {
        float startTime = Time.realtimeSinceStartup;

        for (int i = 0; i < setMaterialProviders.Length; i++)
            setMaterialProviders[i].SetMaterial(sourceMaterial);

        float endTime = Time.realtimeSinceStartup;
        float elapsedTime = endTime - startTime;
        Debug.Log("Elapsed Time Switchc By Children: " + elapsedTime + " seconds");
        EditorApplication.isPaused = true;
    }
    [Button("Switchc By Parent", enabledMode: EButtonEnableMode.Playmode)]
    private void SwitchByParent()
    {
        float startTime = Time.realtimeSinceStartup;

        setMaterialProvider.SetMaterial(sourceMaterial);

        float endTime = Time.realtimeSinceStartup;
        float elapsedTime = endTime - startTime;
        Debug.Log("Elapsed Time Switchc By Parent: " + elapsedTime + " seconds");
        EditorApplication.isPaused = true;
    }
#endif
}
