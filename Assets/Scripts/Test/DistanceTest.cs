using UnityEngine;

public class DistanceTest : MonoBehaviour
{
    [SerializeField] CollisionHelper collisionHelper1;
    [SerializeField] CollisionHelper collisionHelper2;
    bool counting = false;
    float timer = 0;
    private void OnEnable()
    {
        collisionHelper1.TriggerEnter += other => Count();
        collisionHelper2.TriggerEnter += other => Count();
    }
    private void OnDisable()
    {
        collisionHelper1.TriggerEnter -= other => Count();
        collisionHelper2.TriggerEnter -= other => Count();
    }
    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
    }
    private void Count() {
        counting = !counting;
        if (!counting)
            Debug.Log(timer);
        timer = 0f;
    }
}
