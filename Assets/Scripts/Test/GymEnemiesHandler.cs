using UnityEngine;

public class GymEnemiesHandler : MonoBehaviour
{
    [SerializeField] private GameObject enemyPrefab;
    [SerializeField] private CollisionHelper activeCollider;
    [SerializeField] private EnemyController currentEnemy;
    [SerializeField] private GymOffScreenMark gymMarkUI;
    private Vector3 startPosition;
    private bool gymMarkShoulBeActive;
    private bool playerEntered;
    private void Awake()
    {
        activeCollider.TriggerEnter += CheckEnter;
        startPosition = currentEnemy.transform.parent.position;
    }
    private void Start()
    {
        DeactiveSpawn();
    }
    private void CheckEnter(Collider collider)
    {
        if (!LayerUtilities.IsSameLayer(currentEnemy.Character.BaseCharacter.LayerData.PlayerLayer, collider.gameObject.layer))
            return;
        Spawn();
    }
    private void ActiveChaseEnter(Collider collider)
    {
        if (!LayerUtilities.IsSameLayer(currentEnemy.Character.BaseCharacter.LayerData.PlayerLayer, collider.gameObject.layer))
            return;
        if (!gymMarkShoulBeActive)
            gymMarkUI.SetActive(false);
        playerEntered = true;
    }
    private void ActiveChaseExit(Collider collider)
    {
        if (!LayerUtilities.IsSameLayer(currentEnemy.Character.BaseCharacter.LayerData.PlayerLayer, collider.gameObject.layer))
            return;
        gymMarkUI.SetActive(true);
        playerEntered = false;
    }
    private void Spawn()
    {
        GameObject lastEnemy = currentEnemy.transform.parent.gameObject;
        currentEnemy = Instantiate(enemyPrefab).GetComponentInChildren<EnemyController>();
        currentEnemy.transform.parent.position = startPosition;
        Destroy(lastEnemy);
        DeactiveSpawn();
    }
    private void ActiveSpawn()
    {
        activeCollider.gameObject.SetActive(true);
        currentEnemy.Character.BaseCharacter.Data.OnCharacterDead -= ActiveSpawn;
        if (currentEnemy.ActiveChase != null)
        {
            currentEnemy.ActiveChase.TriggerEnter -= ActiveChaseEnter;
            currentEnemy.ActiveChase.TriggerExit -= ActiveChaseExit;
        }
        gymMarkUI.SetText($"Spawm {gymMarkUI.MarkUIText}");
        gymMarkUI.SetTargetMark(activeCollider.transform);
        gymMarkUI.SetActive(true);
        gymMarkShoulBeActive = true;
    }
    private void DeactiveSpawn()
    {
        activeCollider.gameObject.SetActive(false);
        currentEnemy.Character.BaseCharacter.Data.OnCharacterDead += ActiveSpawn;
        if (currentEnemy.ActiveChase != null)
        {
            currentEnemy.ActiveChase.TriggerEnter += ActiveChaseEnter;
            currentEnemy.ActiveChase.TriggerExit += ActiveChaseExit;
        }
        gymMarkUI.SetText(gymMarkUI.MarkUIText);
        gymMarkUI.SetTargetMark(gymMarkUI.transform);
        if (playerEntered)
            gymMarkUI.SetActive(false);
        gymMarkShoulBeActive = false;
    }
}
