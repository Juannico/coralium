using UnityEngine;
using BehaviorDesigner.Runtime.Tasks;

[TaskCategory("Coralium")]
public class InitializeTree : Composite
{
	public SharedEnemyController EnemyController;

    public override void OnAwake()
    {
		
		base.OnAwake();
	}

	public override TaskStatus OnUpdate()
	{
		return TaskStatus.Success;
	}

}