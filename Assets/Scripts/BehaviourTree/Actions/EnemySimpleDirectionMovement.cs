using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using UnityEngine;

[TaskCategory("Coralium")]
[TaskDescription("Simple constant diretion move. Return failes if direction is 0 or null")]
public class EnemySimpleDirectionMovement : BaseEnemyaActionBehaviourTree
{
    public SharedVector3 Direction;
    private MoveStateComponent moveState;
    public override void OnAwake()
    {
        base.OnAwake();
        moveState = ComponentUtilities.SetComponent<MoveStateComponent>(enemyCharacter.gameObject);
        moveState.Speed = enemyCharacter.BaseCharacter.Data.Speed;
    }
    public override void OnStart()
    {
        moveState.Enter();
    }

    public override TaskStatus OnUpdate()
    {       
        if(Direction.Value == Vector3.zero || Direction == null)
            return TaskStatus.Failure;
        enemyController.SetStateDataDirectionValues(Vector3.one, Direction.Value.normalized);
        return TaskStatus.Running;
    }
    public override void OnEnd()
    {
        moveState.Exit();
    }
    public override void OnConditionalAbort()
    {
        moveState.Exit();
    }
}
