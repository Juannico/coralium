using BehaviorDesigner.Runtime.Tasks;
using UnityEngine;

[TaskCategory("Coralium")]
[TaskDescription("Start to chase \"StateSherdData\" target or run away from him")]
public class EnemyChangeTarget : BaseEnemyaActionBehaviourTree
{
    [SerializeField] private bool shouldTargetPlayer = true;
    public override void OnStart()
    {
        enemyController.SetTarget(shouldTargetPlayer);
    }
    public override TaskStatus OnUpdate()
    {
        return TaskStatus.Success;
    }

}
