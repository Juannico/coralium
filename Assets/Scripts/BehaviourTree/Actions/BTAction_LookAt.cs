using BehaviorDesigner.Runtime.Tasks;
using UnityEngine;

[TaskCategory("Coralium")]
[TaskDescription("Runs the task until the owner is facing the specified target.")]
public class BTAction_LookAt : BTAction_BaseAction
{
    [SerializeField] private float acceptanceAngle = 45.0f;

    private GameObject target = null;
    private StateComponentRotate stateComponentRotate = null;

    private TaskStatus currentStatus = TaskStatus.Inactive;

    public override void OnStart()
    {
        base.OnStart();

        // #TODO: Target should come from a BT shared variable.
        target = enemyControllerHandle.Value.Player.gameObject;

        if (target == null)
        {
            Debug.LogError("BTAction_LookAt::OnStart: target is null.");
            currentStatus = TaskStatus.Failure;
            return;
        }

        stateComponentRotate = enemyCharacterHandle.Value.GetComponent<StateComponentRotate>();

        if (stateComponentRotate == null)
        {
            Debug.LogError("BTAction_LookAt::OnStart: enemyCharacterHandle doesn't have a stateComponentRotate component.");
            currentStatus = TaskStatus.Failure;
            return;
        }

        stateComponentRotate.LookAt(target.transform);
        currentStatus = TaskStatus.Running;
    }

    public override TaskStatus OnUpdate()
    {
        if (IsFacingTarget())
        {
            currentStatus = TaskStatus.Success;
        }

        return currentStatus;
    }

    private bool IsFacingTarget()
    {
        Vector3 forwardVector = enemyCharacterHandle.Value.BaseTransform.forward;
        Vector3 ownerToTargetVector = (target.transform.position - enemyCharacterHandle.Value.BaseTransform.position).normalized;

        float angle = Vector3.Angle(forwardVector, ownerToTargetVector);
        return angle <= acceptanceAngle;
    }

    public override void OnEnd()
    {
        base.OnEnd();

        currentStatus = TaskStatus.Inactive;
        target = null;

        if (stateComponentRotate == null)
        {
            Debug.LogError("BTAction_LookAt::OnEnd: Failed to StopLookAt due to stateComponentRotate being null.");
            return;
        }

        stateComponentRotate.StopLookAt();
    }
}
