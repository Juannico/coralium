using BehaviorDesigner.Runtime.Tasks;

[TaskCategory("Coralium")]
public class BTTask_SummonEnemies : BTAction_BaseAbility<SummonEnemiesAbilityDescriptor>
{
    protected override BaseAbility GetAbilityImplementation()
    {
        return enemyCharacterHandle.Value.GetComponent<Ability_SummonEnemies>();
    }
}