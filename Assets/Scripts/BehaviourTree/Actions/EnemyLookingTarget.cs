using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
[TaskCategory("Coralium")]
[TaskDescription("Rotate until look \"StateSherdData\" follow object")]
public class EnemyLookingTarget : BaseEnemyaActionBehaviourTree
{
    public SharedBool CanAttack;
    [SerializeField] private float lookTargetVelocity = 2.5f;
    [SerializeField] private float velocity = 0.25f;
    [SerializeField] private float maxTimeLooking = 1.2f;
    [SerializeField] private float acceptanceAngle = 15;
    private Vector3 targetForward;
    private Vector3 targetPosition;
    private MoveStateComponent moveState;
    private float timer = 0;
    public override void OnAwake()
    {
        base.OnAwake();
        moveState = ComponentUtilities.SetComponent<MoveStateComponent>(enemyCharacter.gameObject);
        moveState.Speed = enemyCharacter.BaseCharacter.Data.Speed;
    }
    public override void OnStart()
    {
        targetPosition = moveState.StateSharedData.CurrentFollowObject.transform.position;
        targetForward = (targetPosition - enemyController.transform.position).normalized;
        enemyController.SetStateDataDirectionValues(Vector3.zero, targetForward);
        moveState.StateSharedData.MovementSpeedModifier = velocity;
        moveState.StateSharedData.SelfRotationMultiplier = lookTargetVelocity;
        CanAttack.Value = true;
        moveState.Enter();
        timer = 0;
    }
    public override TaskStatus OnUpdate()
    {
        timer += Time.deltaTime;
        if (timer > maxTimeLooking)
            return TaskStatus.Success;
        targetPosition = moveState.StateSharedData.CurrentFollowObject.transform.position;
        targetForward = (targetPosition - enemyController.transform.position).normalized;
        enemyController.SetStateDataDirectionValues(Vector3.one, targetForward);
        if (IsFacingTarget())
            return TaskStatus.Success;
        return TaskStatus.Running;
    }
    private bool IsFacingTarget()
    {
        Vector3 forwardVector = enemyCharacter.BaseTransform.forward;
        Vector3 ownerToTargetVector = (targetPosition - enemyCharacter.BaseTransform.position).normalized;
        float angle = Vector3.Angle(forwardVector, ownerToTargetVector);
        return angle <= acceptanceAngle;
    }

    public override void OnEnd()
    {
        moveState.Exit();
        moveState.StateSharedData.MovementSpeedModifier = 1f;
        moveState.StateSharedData.SelfRotationMultiplier = 1;
    }
    public override void OnConditionalAbort()
    {
        moveState.Exit();
        moveState.StateSharedData.MovementSpeedModifier = 1f;
        moveState.StateSharedData.SelfRotationMultiplier = 1;
    }
}
