﻿using UnityEngine;
using BehaviorDesigner.Runtime.Tasks;

public abstract class BTAction_BaseAction : Action
{
	[SerializeField] protected SharedEnemyCharacter enemyCharacterHandle;
	[SerializeField] protected SharedEnemyController enemyControllerHandle;

    public override TaskStatus OnUpdate()
    {
        Debug.LogError("BTAction_BaseAction::OnUpdate: This function needs to be overriden by the parent class.");
        return TaskStatus.Failure;
    }
}