using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
[TaskCategory("Coralium")]
[TaskDescription("Use only for change \"CanAttack \" to false")]

public class EnemyEndAttack : BaseEnemyaActionBehaviourTree
{
    public SharedBool CanAttack;
    public override void OnStart()
    {
        CanAttack.Value = false;
    }
    public override TaskStatus OnUpdate()
    {
        return TaskStatus.Success;
    }
}
