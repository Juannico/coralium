using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
[TaskCategory("Coralium")]
[TaskDescription("Move enemy to random position inside active enemy area")]
public class EnemyMoveToPosition : BaseEnemyaActionBehaviourTree
{
    public SharedBool IsFocusingPlayer;
    [SerializeField] [Range(0, 1)] private SharedFloat areaMultiplier = 0.75f;
    protected Vector2 targetPosition;
    private Vector2 startPosition;
    private Vector3 area;
    private MoveStateComponent moveState;
    private Vector3 lastPosition;
    [SerializeField] private float maxStandingTime = 1f;
    private float standingTime;
    public override void OnAwake()
    {
        base.OnAwake();
        startPosition = enemyController.transform.position;
        moveState = ComponentUtilities.SetComponent<MoveStateComponent>(enemyCharacter.gameObject);
        moveState.Speed = enemyCharacter.BaseCharacter.Data.Speed;
    }
    public override void OnStart()
    {
        base.OnStart();
        targetPosition = startPosition + MathUtilities.RandomVector2(-area * areaMultiplier.Value, area * areaMultiplier.Value);
        area = enemyController.ActiveChase.Collider.bounds.extents;
        if (IsFocusingPlayer.Value)
        {
            while (Vector3.Distance(targetPosition, moveState.StateSharedData.CurrentFollowObject.transform.position) < area.magnitude * 0.1f)
                targetPosition = startPosition + MathUtilities.RandomVector2(-area * areaMultiplier.Value * 0.9f, area * areaMultiplier.Value * 0.9f);
        }
        if (enemyController.HorizontalMove)
            targetPosition.y = startPosition.y;
        enemyController.SetStateDataDirectionValues(Vector2.one, (targetPosition - (Vector2)enemyController.transform.position).normalized);
        moveState.Enter();
        standingTime = 0;
    }

    public override TaskStatus OnUpdate()
    {
        enemyController.SetStateDataDirectionValues(Vector2.one, (targetPosition - (Vector2)enemyController.transform.position).normalized);
        if (Vector3.Distance(enemyController.transform.position, lastPosition) < 0.1f)
            standingTime += Time.deltaTime;
        else
            standingTime = 0;
        if (standingTime >= maxStandingTime)
            return TaskStatus.Success;
        lastPosition = enemyController.transform.position;
        if (Vector2.Distance(targetPosition, enemyController.transform.position) > 5)
            return TaskStatus.Running;
        return TaskStatus.Success;
    }
    public override void OnEnd()
    {
        moveState.Exit();
    }
    public override void OnConditionalAbort()
    {
        moveState.Exit();
    }
}