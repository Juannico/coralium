using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using System;
using NaughtyAttributes;

[TaskCategory("Coralium")]
[TaskDescription("Throw ability, it is chosen from \"EnemyAbilitesHandle\" abilities list according a internal index that increases as you cast abilities ")]
public class EnemyAttackWithAbility : BaseEnemyaActionBehaviourTree
{
    [SerializeField] private bool abilityByIndex = false;
    [HideIf("abilityByIndex")] [SerializeField] private bool nextAbility = true;
    [HideIf("abilityByIndex")] public SharedInt AttackIndex;
    [ShowIf("abilityByIndex")] [SerializeField] private int abilityIndex;
    private bool throwing = false;
    protected EnemyAbilities enemyAbilities;
    private StateSharedData stateSharedData;

    public int FixAttackIndex
    {
        get
        {
            if (enemyAbilities == null)
                return 0;
            return AttackIndex.Value % enemyAbilities.BaseAbilities.Length;
        }
    }
    public override void OnAwake()
    {
        base.OnAwake();
        if (abilityByIndex)
            nextAbility = false;
        AttackIndex.Value = 0;
        enemyAbilities = enemyController.EnemyAbilities;
        if (!enemyController.gameObject.TryGetComponent(out StateSharedData stateSharedData))
            stateSharedData = enemyController.gameObject.AddComponent<StateSharedData>();
        this.stateSharedData = stateSharedData;
        for (int i = 0; i < enemyAbilities.Abilities.Length; i++)
            enemyAbilities.Abilities[i].EndCastAbility += () => throwing = false;
    }
    public override void OnStart()
    {
        enemyController.SetStateDataDirectionValues(Vector3.zero, (stateSharedData.CurrentFollowObject.transform.position - transform.position).normalized);
        if (abilityByIndex)
            AttackIndex.Value = abilityIndex;
        enemyAbilities.SetCurrenAbility(FixAttackIndex);
        enemyAbilities.CurrentAbility.Load();
        throwing = true;

        if (!enemyAbilities.CurrentAbility.Data.HasLoadTime)
        {
            Attack();
            return;
        }
        CoroutineHandler.ExecuteActionAfter(() => Attack(), enemyAbilities.CurrentAbility.Data.LoadTime, enemyController);
    }
    public override void OnEnd()
    {
        base.OnEnd();
        if (throwing)
            enemyCharacter.AbilitiesHandle.DisrupAbilities();
    }
    public override TaskStatus OnUpdate()
    {
        if (throwing)
            return TaskStatus.Running;
        return TaskStatus.Success;
    }

    protected virtual void Attack()
    {
        enemyAbilities.CurrentAbility.Loaded();
        enemyAbilities.CurrentAbility.Cast(true);
        if (nextAbility)
            AttackIndex.Value++;
    }
}