using BehaviorDesigner.Runtime.Tasks;
using UnityEngine;

[TaskCategory("Coralium")]
public class BTTask_DashAttack : BTAction_BaseAbility<DashAbilityDescriptor>
{
    protected override BaseAbility GetAbilityImplementation()
    {
        return enemyCharacterHandle.Value.GetComponent<Ability_Dash>();
    }

    protected override AbilityParams GenerateAbilityParams()
    {
        AbilityParams abilityParams = base.GenerateAbilityParams();

        Transform target = enemyControllerHandle.Value.GetTarget();
        abilityParams.vector1Value = target.position - enemyCharacterHandle.Value.transform.position;

        return abilityParams;
    }
}