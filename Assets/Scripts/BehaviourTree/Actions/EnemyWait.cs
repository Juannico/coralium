using UnityEngine;
using BehaviorDesigner.Runtime.Tasks;

[TaskCategory("Coralium")]
[TaskDescription("Enter to wait state . Always return Running value")]
public class EnemyWait : BaseEnemyaActionBehaviourTree
{
	private WaitStateComponent waitState;

	public override void OnAwake()
	{
		base.OnAwake();
		waitState = ComponentUtilities.SetComponent<WaitStateComponent>(enemyCharacter.gameObject);
	}
	public override void OnStart()
	{
		enemyController.SetStateDataDirectionValues(Vector3.zero, enemyCharacter.BaseTransform.forward);
		enemyController.Character.Rigidbody.velocity = Vector3.zero;
		waitState.Enter();
	}

	public override TaskStatus OnUpdate()
	{
		return TaskStatus.Running;
	}
	public override void OnEnd()
	{
		waitState.Exit();
	}
}
