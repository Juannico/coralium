using BehaviorDesigner.Runtime.Tasks;

[TaskCategory("Coralium")]
public class BTTask_ShootProjectile : BTAction_BaseAbility<RangedAttackAbilityDescriptor>
{
    protected override BaseAbility GetAbilityImplementation()
    {
        return enemyCharacterHandle.Value.GetComponent<Ability_ShootProjectile>();
    }

    protected override AbilityParams GenerateAbilityParams()
    {
        AbilityParams abilityParams = base.GenerateAbilityParams();

        abilityParams.vector1Value = enemyControllerHandle.Value.Player.transform.position;

        return abilityParams;
    }
}