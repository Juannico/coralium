using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
[TaskCategory("Coralium")]
[TaskDescription("Start or end electrify")]
public class EnemyChangeStatusEffect : BaseEnemyaActionBehaviourTree
{
    public SharedBool statusEffect;
    [SerializeField] private DamageTypes damageType;
    private EnemyStatusEffect enemyStatusEffect;
    public override void OnAwake()
    {
        base.OnAwake();
        enemyStatusEffect = enemyController.gameObject.GetComponentInChildren<EnemyStatusEffect>(true);
    }
    public override void OnStart()
    {
        if (enemyStatusEffect == null)
            throw new System.Exception($"{enemyController.name} Doesn�t have {typeof(EnemyStatusEffect)} componet");
        base.OnStart();
        statusEffect.Value = !statusEffect.Value;
        if (statusEffect.Value)
        {
            enemyStatusEffect.ApplyEffect(damageType);
            return;
        }
        enemyStatusEffect.OffEffect();
    }
    public override TaskStatus OnUpdate()
    {
        return TaskStatus.Success;
    }
}
