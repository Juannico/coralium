using UnityEngine;
using BehaviorDesigner.Runtime.Tasks;
[TaskCategory("Coralium")]
[TaskDescription("Enter to idle state")]
public class EnemyIdle : BaseEnemyaActionBehaviourTree
{
    private IdleStateComponent idleState;
    private Rigidbody rb;

    public override void OnAwake()
    {
        base.OnAwake();
        idleState = ComponentUtilities.SetComponent<IdleStateComponent>(enemyCharacter.gameObject);
        rb = enemyCharacter.GetComponent<Rigidbody>();
        idleState.IdleData = enemyCharacter.MovementData.IdleData;
        idleState.Speed = enemyCharacter.BaseCharacter.Data.Speed;
    }
    public override void OnStart()
    {
        enemyController.SetStateDataDirectionValues(Vector3.zero, rb.velocity);
        idleState.Enter();
    }

    public override TaskStatus OnUpdate()
    {
        enemyController.SetStateDataDirectionValues(Vector3.zero, rb.velocity);
        return TaskStatus.Running;
    }
    public override void OnEnd()
    {
        idleState.Exit();
    }
}