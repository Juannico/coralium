using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
[TaskCategory("Coralium")]
[TaskDescription("Start knockback")]
public class EnemyKnockBack : BaseEnemyaActionBehaviourTree
{
    public SharedBool OnKnockBack;
    private KnockBackStateComponent knockBackState;
    private MoveStateComponent moveStateComponent;
    public SharedBool moveOnKnockBack = false;
    [SerializeField] private TaskStatus status;
    private float duration = 0;
    public override void OnAwake()
    {
        base.OnAwake();
        knockBackState = ComponentUtilities.SetComponent<KnockBackStateComponent>(enemyCharacter.gameObject);
        moveStateComponent = ComponentUtilities.SetComponent<MoveStateComponent>(enemyCharacter.gameObject);
        knockBackState.OnStart += () => enemyCharacter.BaseCharacter.Data.IsImmune = true;
        knockBackState.OnStop += (componentState) =>
        {
            OnKnockBack.Value = false;
            status = TaskStatus.Success;
            enemyCharacter.BaseCharacter.Data.IsImmune = false;
        };
        enemyCharacter.BaseCharacter.Data.OnCharacterKnockBack = (knockBackDirection, duration, isCrashing) => StartKnockBack(knockBackDirection, duration, isCrashing);
    }
    public override TaskStatus OnUpdate()
    {
        return status;
    }

    public override void OnEnd()
    {
        base.OnEnd();
        knockBackState.Exit();

    }
    private void StartKnockBack(Vector2 knockBackDirection, float duration, bool isCrashing)
    {
        this.duration = duration;
        OnKnockBack.Value = true;
        knockBackState.StateSharedData.isCrashing = isCrashing;
        knockBackDirection.y *= 2;
        knockBackState.StateSharedData.KnockBackDirection = knockBackDirection.normalized;
        if (!moveOnKnockBack.Value)
        {
            knockBackState.StateSharedData.isCrashing = false;
            knockBackState.StateSharedData.KnockBackDirection = Vector2.zero;
        }
        knockBackState.StateSharedData.KnockBackDuration = duration;
        moveStateComponent.Exit();
        knockBackState.Enter();
        status = TaskStatus.Running;
    }
}

