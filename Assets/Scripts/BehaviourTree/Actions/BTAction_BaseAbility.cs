﻿using UnityEngine;
using BehaviorDesigner.Runtime.Tasks;

/*
 * This BTAction handles the activation of an ability. It stops running once the ability is done.
 */
public abstract class BTAction_BaseAbility<T> : BTAction_BaseAction where T : BaseAbilityDescriptor
{
    // We make this generic so that the inspector only queries for ScriptableObjects of the specified type
	[SerializeField] protected T abilityDescriptor;

    protected AbilitySystemComponent abilitySystemComponent;
    protected BaseAbility abilityImplementation;

    protected TaskStatus currentTaskStatus;

    public override void OnAwake()
    {
        currentTaskStatus = TaskStatus.Running;
    }

    public override void OnStart()
    {
        if (abilityDescriptor == null)
        {
            // Set status to failure so that the BT reacts to this task failure
            Debug.LogError("BTAction_BaseAbility::OnStart: abilityDescriptor is null.");
            currentTaskStatus = TaskStatus.Failure;
            return;
        }

        abilitySystemComponent = gameObject.GetComponent<AbilitySystemComponent>();

        if (abilitySystemComponent == null)
        {
            // Set status to failure so that the BT reacts to this task failure
            Debug.LogError("BTAction_BaseAbility::OnStart: abilitySystemComponent is null.");
            currentTaskStatus = TaskStatus.Failure;
            return;
        }

        abilityImplementation = GetAbilityImplementation();

        if (abilityImplementation == null)
        {
            // Set status to failure so that the BT reacts to this task failure
            Debug.LogError("BTAction_BaseAbility::OnStart: abilityImplementation is null.");
            currentTaskStatus = TaskStatus.Failure;
            return;
        }

        abilityImplementation.onAbilityEnded += OnAbilityEnded;

        AbilityParams abilityParams = GenerateAbilityParams();

        bool bActivated = abilitySystemComponent.TryActivateAbility(abilityImplementation, abilityDescriptor, abilityParams);

        if (!bActivated)
        {
            currentTaskStatus = TaskStatus.Failure;
        }
    }

    protected virtual BaseAbility GetAbilityImplementation()
    {
        // #TODO: Find a way to specify the ability class within as ScriptableObject
        Debug.LogError("BTAction_BaseAbility::GetAbilityComponent: This function needs to be overriden by the child class.");
        return null;
    }

    protected virtual AbilityParams GenerateAbilityParams()
    {
        // Empty params by default, override if needed
        return new AbilityParams();
    }

    public override TaskStatus OnUpdate()
    {
        return currentTaskStatus;
    }

    private void OnAbilityEnded(BaseAbility ability, bool wasCancelled)
    {
        // If the ability go canceled, it means something went wrong
        currentTaskStatus = wasCancelled ? TaskStatus.Failure : TaskStatus.Success;
    }

    public override void OnEnd()
    {
        if (abilityImplementation)
        {
            abilityImplementation.onAbilityEnded -= OnAbilityEnded;
        }

        // Reset task state
        abilitySystemComponent = null;
        abilityImplementation = null;

        currentTaskStatus = TaskStatus.Inactive;
    }
}