using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
[TaskCategory("Coralium")]
[TaskDescription("Enter to die state")]
public class EnemyDie : BaseEnemyaActionBehaviourTree
{
    private DieStateComponent dieState;
    private MoveStateComponent moveStateComponent;
    private BehaviorTree behaviorTree;
    public override void OnAwake()
    {
        base.OnAwake();
        behaviorTree = enemyController.GetComponent<BehaviorTree>();
        dieState = ComponentUtilities.SetComponent<DieStateComponent>(enemyCharacter.gameObject);
        moveStateComponent = ComponentUtilities.SetComponent<MoveStateComponent>(enemyCharacter.gameObject);
        dieState.DieData = enemyCharacter.MovementData.DieData;
        enemyController.OnDied.AddListener(() =>
        {
            moveStateComponent.Exit();
            dieState.Enter();
            behaviorTree.enabled = false;
        });
    }
    public override TaskStatus OnUpdate() => TaskStatus.Running;
}
