using BehaviorDesigner.Runtime.Tasks;
using UnityEngine;

[TaskCategory("Coralium")]
[TaskDescription("Move araound \"StateSherdData\"  follow object with enemy distance radius")]
public class EnemyMoveAroundTarget : BaseEnemyaActionBehaviourTree
{ 
    private Transform targetTransform;
    private float angle;
    private int sideDirection;
    private float radius;
    private MoveStateComponent moveState;
    public override void OnAwake()
    {
        base.OnAwake();
        moveState = ComponentUtilities.SetComponent<MoveStateComponent>(enemyCharacter.gameObject);
        moveState.Speed = enemyCharacter.BaseCharacter.Data.Speed;
    }
    public override void OnStart()
    {
        targetTransform = moveState.StateSharedData.CurrentFollowObject.transform;
        angle = Vector2.Angle(enemyController.transform.position, targetTransform.position);
        angle *= Mathf.Deg2Rad;

        sideDirection = Random.Range(0f, 1f) > 0.5f ? 1 : -1;
        radius = (enemyController.Character.BaseCharacter.Data.AttackDistance) * 0.65f;
        moveState.Enter();

    }
    public override TaskStatus OnUpdate()
    {
        float distanceToPlayer = Vector2.Distance(targetTransform.position, enemyController.transform.position);
        if (radius > distanceToPlayer)
        {
            radius = distanceToPlayer;
            radius += Time.deltaTime * 2.5f;
        }
        Vector2 targetPosition = new Vector2(Mathf.Sin(angle), Mathf.Cos(angle)) * radius;
        targetPosition += (Vector2)targetTransform.position;
        if (Vector2.Distance(targetPosition, enemyController.transform.position) < 3)
            angle += Time.deltaTime * 2 * sideDirection;
        if (ShouldChangeAngle(targetPosition))
            angle += Mathf.Deg2Rad * sideDirection * 90;
        enemyController.SetStateDataDirectionValues(Vector2.one, (targetPosition - (Vector2)enemyController.transform.position).normalized);
        return TaskStatus.Running;
    }
    public override void OnEnd()
    {
        moveState.Exit();
    }
    public override void OnConditionalAbort()
    {
        moveState.Exit();
    }
    private bool ShouldChangeAngle(Vector3 targetPosition)
    {
        if (enemyController.Player.StateHandler.Data.MovementVector.magnitude == 0)
            return false;
        if (Vector2.Distance(targetPosition, enemyController.transform.position) > (enemyCharacter.BaseCharacter.Data.AttackDistance) * 0.55f)
            return false;
        Vector2 direction = (moveState.StateSharedData.CurrentFollowObject.transform.position - targetPosition);
        Vector2 forwardDirection = enemyController.Player.Character.BaseTransform.forward;
        return Vector2.Dot(direction, forwardDirection) < -25;
    }
}
