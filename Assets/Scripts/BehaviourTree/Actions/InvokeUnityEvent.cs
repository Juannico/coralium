using BehaviorDesigner.Runtime.Tasks;
using BehaviorDesigner.Runtime;
using UnityEngine.Events;

[TaskCategory("Coralium")]
[TaskDescription("Invoke Unity Event")]
public class InvokeUnityEvent : BaseEnemyaActionBehaviourTree
{
    public SharedUnityEvent UnityEvent;
    
}


[System.Serializable]
public class SharedUnityEvent : SharedVariable<UnityEvent>
{
    public override string ToString() { return mValue == null ? "null" : mValue.ToString(); }
    public static implicit operator SharedUnityEvent(UnityEvent value) { return new SharedUnityEvent { mValue = value }; }
}
