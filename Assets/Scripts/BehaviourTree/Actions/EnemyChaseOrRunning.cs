using BehaviorDesigner.Runtime.Tasks;
using UnityEngine;

[TaskCategory("Coralium")]
[TaskDescription("Start to chase \"StateSherdData\" target or run away from him")]
public class EnemyChaseOrRunning : BaseEnemyaActionBehaviourTree
{
    [SerializeField] private bool shouldChase = true;
    [SerializeField] private float acceptanceRadius = -1.0f;

    private ChaseRunStateComponent chaseRunState;
    private Transform playerTransform;

    public override void OnAwake()
    {
        base.OnAwake();
        chaseRunState = ComponentUtilities.SetComponent<ChaseRunStateComponent>(enemyCharacter.gameObject);
        chaseRunState.Speed = enemyCharacter.BaseCharacter.Data.Speed;
    }
    public override void OnStart()
    {
        // This does a nasty FindGameObjectWithTag, so, let's only do it at the start of the task.
        playerTransform = enemyController.GetTarget();

        enemyController.SetStateDataDirectionValues(Vector3.one, enemyCharacter.BaseTransform.forward);
        chaseRunState.StateSharedData.IsChassing = shouldChase;
        chaseRunState.Enter();
    }
    public override TaskStatus OnUpdate()
    {
        if (acceptanceRadius > 0 && playerTransform != null)
        {
            float currentDistance = (playerTransform.position - enemyCharacter.transform.position).magnitude;
            // Is it in an acceptable distance to the player?
            if (currentDistance <= acceptanceRadius)
            {
                return TaskStatus.Success;
            }
        }

        return TaskStatus.Running;
    }
    public override void OnEnd()
    {
        chaseRunState.Exit();
        playerTransform = null;
    }
}
