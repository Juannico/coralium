using BehaviorDesigner.Runtime;

public class SharedColisionHelper : SharedVariable<CollisionHelper>
{
    public override string ToString() { return mValue == null ? "null" : mValue.ToString(); }
    public static implicit operator SharedColisionHelper(CollisionHelper value) { return new SharedColisionHelper { mValue = value }; }
}
