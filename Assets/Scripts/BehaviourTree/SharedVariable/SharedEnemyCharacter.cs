using BehaviorDesigner.Runtime;

[System.Serializable]
public class SharedEnemyCharacter : SharedVariable<EnemyCharacter>
{
	public override string ToString() { return mValue == null ? "null" : mValue.ToString(); }
	public static implicit operator SharedEnemyCharacter(EnemyCharacter value) { return new SharedEnemyCharacter { mValue = value }; }
}