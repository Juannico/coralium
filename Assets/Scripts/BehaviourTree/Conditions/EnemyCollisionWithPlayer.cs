using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using UnityEngine;
[TaskCategory("Coralium")]
[TaskDescription("Return success when collide with player")]
public class EnemyCollisionWithPlayer : BaseEnemyConditionalBehaviourTree
{
    [SerializeField]private SharedBool collisionEnter = false;
    private SpecialImpulseStateComponent specialImpulseState;
    public override void OnAwake()
    {
        base.OnAwake();
        specialImpulseState = ComponentUtilities.SetComponent<SpecialImpulseStateComponent>(gameObject);
        collisionEnter.Value = false;
    }
    public override TaskStatus OnUpdate()
    {
        return collisionEnter.Value ? TaskStatus.Success : TaskStatus.Failure;
    }
    public override void OnCollisionEnter(Collision collision)
    {
        if (!LayerUtilities.IsSameLayer(enemyCharacter.BaseCharacter.LayerData.PlayerLayer, collision.gameObject.layer) || specialImpulseState.enabled)
            return;
        collisionEnter.Value = true;
    }
}

