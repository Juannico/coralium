using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
[TaskCategory("Coralium")]
public class BaseEnemyConditionalBehaviourTree : Conditional
{
	public SharedEnemyController EnemyController;
	protected EnemyController enemyController;
	public SharedEnemyCharacter EnemyCharacter;
	protected EnemyCharacter enemyCharacter;
	public override void OnAwake()
	{
		enemyController = EnemyController.Value;
		enemyCharacter = EnemyCharacter.Value;
	}

	public override TaskStatus OnUpdate()
	{
		return TaskStatus.Running;
	}
}