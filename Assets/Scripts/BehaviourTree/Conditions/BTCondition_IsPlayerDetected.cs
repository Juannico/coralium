using BehaviorDesigner.Runtime.Tasks;
using UnityEngine;

[TaskCategory("Coralium")]
[TaskDescription("Determines if the controlled character has detected the Player character.")]
public class BTCondition_IsPlayerDetected : BaseEnemyConditionalBehaviourTree
{
    BossEnemyController bossEnemyController = null;

    public override void OnStart()
    {
        base.OnStart();

        // #TODO: Make this BTCondition usable by any NPC.
        bossEnemyController = (BossEnemyController)enemyController;
    }

    public override TaskStatus OnUpdate()
    {
        if (bossEnemyController == null)
        {
            Debug.Log("BTDecorator_IsPlayerDetected::OnUpdate: bossEnemyController is null.");
            return TaskStatus.Failure;
        }

        return bossEnemyController.PlayerDetected ? TaskStatus.Success : TaskStatus.Failure;
    }
}
