using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using UnityEngine;
[TaskCategory("Coralium")]
[TaskDescription("Store \"EnemyExitArea\" value  when enemy enter or exit to \"collisionHelper\" assigned in the inspector.")]
public class EnemySelfOnCollisionHelperEnter : BaseEnemyConditionalBehaviourTree
{
    [SerializeField] private SharedColisionHelper collisionHelper;
    public SharedBool EnemyExitArea;

    public override void OnAwake()
    {
        base.OnAwake();
        collisionHelper.Value.TriggerEnter += CheckEnter;
        collisionHelper.Value.TriggerExit += CheckExit;
    }
    public override TaskStatus OnUpdate()
    {
        return  TaskStatus.Failure;
    }
    private void CheckEnter(Collider collider)
    {
        Character character = collider.GetComponentInParent<Character>();
        if (character == null)
            return;
        if (character != enemyCharacter)
            return;
        EnemyExitArea = false;
    }
    private void CheckExit(Collider collider)
    {
        Character character = collider.GetComponentInParent<Character>();
        if (character == null)
            return;
        if (character != enemyCharacter)
            return;
        EnemyExitArea = true;
    }
}
