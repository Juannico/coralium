using BehaviorDesigner.Runtime.Tasks;
using UnityEngine;

[TaskCategory("Coralium")]
[TaskDescription("Check the distance to \"StateSherdData\" follow object. Return succes if the distance is less than enemy \"attackDistance\" when \"shoulBeInrange\" in or if the distance is more than enemy \"attackDistance\" when not \"shoulBeInrange\".")]
public class EnemyCheckRangeToTarget : BaseEnemyConditionalBehaviourTree
{
    [SerializeField] private bool shouldBeInRange = true;
    [SerializeField] private bool checkPlayer = false;
    [SerializeField] private float rangeMultiplier = 1;
    private Transform targetTransform;
    private StateSharedData stateSharedData;
    public override void OnAwake()
    {
        base.OnAwake();
        if (!enemyController.gameObject.TryGetComponent(out StateSharedData stateSharedData))
            stateSharedData = enemyController.gameObject.AddComponent<StateSharedData>();
        this.stateSharedData = stateSharedData;
    }
    public override void OnStart()
    {
        targetTransform = checkPlayer ? enemyController.Player.transform : stateSharedData.CurrentFollowObject.transform;
    }
    public override TaskStatus OnUpdate()
    {
        Vector3 targetPosition = targetTransform.position;
        if (enemyController.HorizontalMove)
            targetPosition.y = enemyController.transform.position.y;
        bool isInRange = Vector3.Distance(targetPosition, enemyController.transform.position) < enemyController.Character.BaseCharacter.Data.AttackDistance * rangeMultiplier * enemyController.transform.lossyScale.magnitude;
        return isInRange && shouldBeInRange ? TaskStatus.Success : shouldBeInRange || isInRange ? TaskStatus.Failure : TaskStatus.Success;
    }
}
