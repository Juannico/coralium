using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using UnityEngine;

[TaskCategory("Coralium")]
[TaskDescription("Store the avoidance vector and return succes if this vector is different to 0.")]
public class EnemyShouldAvoid : BaseEnemyConditionalBehaviourTree
{
    public SharedBool CanAttack;
    private float baseDistance = 1;
    [SerializeField] private SharedFloat avoidDistanceOffset;

    private LayerMask avoidLayer;

    private RaycastHit[] hits = new RaycastHit[5];
    public override void OnAwake()
    {
        base.OnAwake();
        baseDistance = enemyController.Character.Model.transform.lossyScale.magnitude * enemyCharacter.Collider.radius;
        avoidLayer = enemyCharacter.BaseCharacter.LayerData.EnviromenLayer;
    }

    public override TaskStatus OnUpdate()
    {
        int index = Physics.SphereCastNonAlloc(enemyCharacter.BaseTransform.position, baseDistance, enemyCharacter.BaseTransform.forward, hits, baseDistance + avoidDistanceOffset.Value, avoidLayer);
        if (index > 0)
        {
            CanAttack.Value = false;
            return TaskStatus.Failure;
        }
        return TaskStatus.Success;
    }
}
