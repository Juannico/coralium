using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
[TaskCategory("Coralium")]
[TaskDescription("Return succes when player enter to \"collisionHelper\" assigned in the inspector.")]
public class EnemyOnCollisionHelperPlayerEnter : BaseEnemyConditionalBehaviourTree
{
    private bool enteredTrigger = false;
    [SerializeField] private LayerData layerData;
    [SerializeField] private SharedColisionHelper collisionHelper;
    public SharedBool IsFocusingPlayer;
    private StateSharedData stateSharedData;
    public override void OnAwake()
    {
        base.OnAwake();
        enteredTrigger = false;
        collisionHelper.Value.TriggerEnter += CheckEnter;
        collisionHelper.Value.TriggerExit += CheckExit;
        if (!enemyController.gameObject.TryGetComponent(out StateSharedData stateSharedData))
            stateSharedData = enemyController.gameObject.AddComponent<StateSharedData>();
        this.stateSharedData = stateSharedData;
    }
    public override TaskStatus OnUpdate()
    {
        return enteredTrigger ? TaskStatus.Success : TaskStatus.Failure;
    }   
    private void CheckEnter(Collider collider)
    {
        if (!LayerUtilities.IsSameLayer(layerData.PlayerLayer, collider.gameObject.layer))
            return;
        enteredTrigger = true;
        IsFocusingPlayer = true;
        enemyController.Player = collider.GetComponentInParent<PlayerController>();
        stateSharedData.CurrentFollowObject = enemyController.Player.Character.Model.gameObject;
    }
    private void CheckExit(Collider collider)
    {
        if (!LayerUtilities.IsSameLayer(layerData.PlayerLayer, collider.gameObject.layer))
            return;
        enteredTrigger = false;
        IsFocusingPlayer = false;
    }
}