using UnityEngine;

public class SwitchAlphaMaterial : MonoBehaviour
{
    [SerializeField] private float alphaValue = 0.5f;
    private Renderer selfRenderer;
    private float startAlphaClipping;
    private void Awake()
    {
        selfRenderer = GetComponent<Renderer>();
        startAlphaClipping = selfRenderer.material.GetFloat("_AlphaClipping");
    }
    public void SwitchMaterial(bool shouldHide)
    {
        if (shouldHide)
        {
            selfRenderer.material.EnableKeyword("_BLENDMODE_ALPHA");
            selfRenderer.material.EnableKeyword("_SURFACE_TYPE_TRANSPARENT");
            selfRenderer.material.SetFloat("_SurfaceType", 1);
            selfRenderer.material.SetFloat("_RenderQueueType", 5);
            selfRenderer.material.SetFloat("_AlphaDstBlend", 10f);
            selfRenderer.material.SetFloat("_DstBlend", 10f);
            selfRenderer.material.SetFloat("_ZTestDepthEqualForOpaque", 4f);
            selfRenderer.material.renderQueue = (int)UnityEngine.Rendering.RenderQueue.Transparent;
            selfRenderer.material.SetFloat("_AlphaClipping", alphaValue);
            selfRenderer.material.SetFloat("_Alpha", alphaValue);
            return;
        }
        selfRenderer.material.DisableKeyword("_SURFACE_TYPE_TRANSPARENT");
        selfRenderer.material.SetInt("_SurfaceType", 0);
        selfRenderer.material.SetInt("_RenderQueueType", 1);
        selfRenderer.material.SetFloat("_AlphaDstBlend", 0f);
        selfRenderer.material.SetFloat("_DstBlend", 0f);
        selfRenderer.material.SetFloat("_ZTestDepthEqualForOpaque", 3f);
        selfRenderer.material.renderQueue = (int)UnityEngine.Rendering.RenderQueue.Geometry;
        selfRenderer.material.SetFloat("_AlphaClipping", startAlphaClipping);
        selfRenderer.material.SetFloat("_Alpha", 1);
    }
}
