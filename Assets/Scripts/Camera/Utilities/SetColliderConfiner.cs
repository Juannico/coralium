using Cinemachine;
using UnityEngine;

public class SetColliderConfiner : MonoBehaviour
{
    [SerializeField] private Collider2D confinerCollider;
    [SerializeField] private bool autoInialize;
    private CinemachineConfiner cinemachineConfiner2D;
    private Collider2D lastCofinerCollider;
    private void Awake()
    {
        if (confinerCollider == null)
            confinerCollider = gameObject.GetComponent<Collider2D>();
    }
    private void Start()
    {
        cinemachineConfiner2D = GameObject.FindGameObjectWithTag(Tags.PlayerVirtualCamera)?.GetComponent<CinemachineConfiner>();
        if (cinemachineConfiner2D == null)
        {
            StartCoroutine(CoroutineHandler.ExecuteActionAfterUpdateUntil(() =>
            {
                cinemachineConfiner2D = GameObject.FindGameObjectWithTag(Tags.PlayerVirtualCamera)?.GetComponent<CinemachineConfiner>();
                return cinemachineConfiner2D != null;
            }, () =>
            {
                if (autoInialize)
                    SetCollider(true);
            }));
            return;
        }
        if (autoInialize)
            SetCollider(true);
    }
    public void SetCollider(bool active)
    {
        if (!active)
        {
            cinemachineConfiner2D.m_BoundingShape2D = lastCofinerCollider;
            return;
        }
        lastCofinerCollider = cinemachineConfiner2D.m_BoundingShape2D;
        cinemachineConfiner2D.m_BoundingShape2D = confinerCollider;
    }

}
