using UnityEngine;

public class DetectHideObjects : MonoBehaviour
{
    [SerializeField] private LayerMask objectsToHideLayer;
    RaycastHit[] raycastHits = new RaycastHit[5];
    SwitchAlphaMaterial[] switchMaterials = new SwitchAlphaMaterial[5];
    private int raycastHitsLengh = 0;
    private GameObject player;
    private Vector3 direction;
    void Start()
    {
        player = GameObject.FindGameObjectWithTag(Tags.Player);
    }
    void Update()
    {
        HideRaycastHits();
        DetectObjecstInTheWay();
    }
    private void HideRaycastHits() 
    {
        if (raycastHitsLengh < 1)
            return;
        for (int i = 0; i < raycastHitsLengh; i++)
        {
            switchMaterials[i].SwitchMaterial(false);
        }
    }
    private void DetectObjecstInTheWay() {
        float distance = Vector3.Distance(transform.position, player.transform.position);
        direction = player.transform.position - transform.position;
        direction.Normalize();
        raycastHitsLengh = Physics.SphereCastNonAlloc(transform.position,1, direction, raycastHits, distance * 0.9f, objectsToHideLayer);
        if (raycastHitsLengh <= 0)
            return;
        for (int i = 0; i < raycastHitsLengh; i++)
        {
            switchMaterials[i] = raycastHits[i].transform.GetComponent<SwitchAlphaMaterial>();
            switchMaterials[i].SwitchMaterial(true);
        }
    }
}
