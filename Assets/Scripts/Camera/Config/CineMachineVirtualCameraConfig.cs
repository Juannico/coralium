using Cinemachine;
using System;
using UnityEngine;

public class CineMachineVirtualCameraConfig : MonoBehaviour
{
    private CinemachineVirtualCamera virtualCamera;
    private CinemachineFramingTransposer transposer;
    [SerializeField] [Range(0.01f, 0.99f)] private float minScaleZoom = 0.25f;
    [SerializeField] [Range(1.01f, 5f)] private float maxScaleZoom = 4;
    [SerializeField] [Range(0.01f, 0.99f)] private float minScaleXSensibility = 0.25f;
    [SerializeField] [Range(1.01f, 5f)] private float maxScaleXSensibility = 4f;
    [SerializeField] [Range(0.01f, 0.99f)] private float minScaleYSensibility = 0.25f;
    [SerializeField] [Range(1.01f, 5f)] private float maxScaleYSensibility = 4f;
    private float startZoom = 1;
    private void Awake()
    {
        virtualCamera = GetComponent<CinemachineVirtualCamera>();
        transposer = virtualCamera.GetCinemachineComponent<CinemachineFramingTransposer>();
        startZoom = transposer.m_CameraDistance;
    }
    public void ZoomByDebug(float zoomValue) => transposer.m_CameraDistance = startZoom * Mathf.Lerp(maxScaleZoom, minScaleZoom, zoomValue);
    public float GetZoomByDebug(bool scale = false) => scale ? Mathf.InverseLerp(maxScaleZoom, minScaleZoom, transposer.m_CameraDistance / startZoom) : transposer.m_CameraDistance;

}
