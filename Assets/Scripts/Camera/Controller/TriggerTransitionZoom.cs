using UnityEngine;

public class TriggerTransitionZoom : MonoBehaviour
{
    [SerializeField] private LayerData layerData;
    [SerializeField] private float zoomDuration;
    [SerializeField] private float zoomDistance;
    private CameraController cameraController;
    private void Awake()
    {
        cameraController = GameObject.FindGameObjectWithTag(Tags.CameraController).GetComponent<CameraController>();
    }
    private void OnTriggerEnter(Collider other)
    {
        if (!LayerUtilities.IsSameLayer(layerData.PlayerLayer, other.gameObject.layer))
            return;
        ChangeZoom();
    }
    private void OnTriggerExit(Collider other)
    {
        if (!LayerUtilities.IsSameLayer(layerData.PlayerLayer, other.gameObject.layer))
            return;
        RestartZoom();
    }
    public void ChangeZoom() => cameraController.ChangeZoom(zoomDistance, zoomDuration);
    public void RestartZoom() => cameraController.RestoreZoomValue(zoomDuration, true);
}
