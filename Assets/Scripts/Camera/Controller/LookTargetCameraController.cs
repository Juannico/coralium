using Cinemachine;
using UnityEngine;

public class LookTargetCameraController : MonoBehaviour
{
    [SerializeField] private CinemachineVirtualCamera lookTargetCamera;
    [SerializeField] private CinemachineTargetGroup targetGroup;

    public void StartTarget(Transform secondTarget,float targetWeight = 1,float targetRadius =5)
    {
        lookTargetCamera.Priority = 1;
        targetGroup.AddMember(secondTarget, targetWeight, targetRadius);
    }
    public void StopTarget(Transform secondTarget)
    {
        lookTargetCamera.Priority = 0;
        targetGroup.RemoveMember(secondTarget);
    }
}
