using Cinemachine;
using UnityEngine;

public class OffSetCamera : MonoBehaviour
{
    [SerializeField] private Vector2 offSet = new Vector2(20, 15);
    [SerializeField] private float offsettSpeed = 0.25f;
    private Vector3 refTargetTrackedObjecOffsetSmoothDamp;
    private CinemachineCameraOffset cinemachineCameraOffset;
    private Vector3 lastPosition;
    private PlayerController playerController;
    private CinemachineConfiner cinemachineConfiner2D;
    private CinemachineVirtualCamera virtualCamera;
    private void Awake()
    {
        playerController = GameObject.FindGameObjectWithTag(Tags.Player).GetComponent<PlayerController>();
        lastPosition = playerController.BaseTransform.position;
        virtualCamera = GameObject.FindGameObjectWithTag(Tags.PlayerVirtualCamera).GetComponent<CinemachineVirtualCamera>();
        cinemachineCameraOffset = virtualCamera.GetComponent<CinemachineCameraOffset>();
        cinemachineConfiner2D = virtualCamera.GetComponent<CinemachineConfiner>();
    }

    private void FixedUpdate()
    {
        OffSetTarget(playerController.BaseTransform.position - lastPosition);
        lastPosition = playerController.BaseTransform.position;
    }
    private void OffSetTarget(Vector2 targetOffset)
    {
        if (cinemachineConfiner2D.CameraWasDisplaced(virtualCamera))
            targetOffset = Vector2.zero;
        Vector2.ClampMagnitude(targetOffset, 1);
        targetOffset.x *= Mathf.Abs(playerController.MoveVector.normalized.x);
        targetOffset.y *= Mathf.Abs(playerController.MoveVector.normalized.y);
        targetOffset.x = Mathf.Clamp(targetOffset.x * offSet.x, -offSet.x, offSet.x);
        targetOffset.y = Mathf.Clamp(targetOffset.y * offSet.y, -offSet.y, offSet.y);
        Vector3 targetTrackedObjectOffset = Vector3.SmoothDamp(cinemachineCameraOffset.m_Offset, targetOffset, ref refTargetTrackedObjecOffsetSmoothDamp, offsettSpeed, 100, Time.fixedDeltaTime);
        cinemachineCameraOffset.m_Offset = targetTrackedObjectOffset;
    }
}
