using Cinemachine;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField] private GameObject playerCamera;
    public GameObject EcolocalizationCamera;
    [field: SerializeField] public float CameraYOffset { get; private set; } = 1.5f;
    [field: SerializeField] public float CameraYAngleOffset { get; private set; } = 15;
    private CineMachineVirtualCameraConfig cineMachineVirutalCameraConfig;
    private Animator animator;
    private CinemachineVirtualCamera playerCinemachineVirtualCamera;
    private CinemachineBasicMultiChannelPerlin cinemachineBasicMultiChannelPerlin;
    private float startCameraDistance;
    private float tempStartCameraDistance;
    private Coroutine zoomingCourutine;
    private float targetCameraDistance;
    [SerializeField] private float zoomTransitionDuration = 0.25f;
    private bool stopZoom;
    private Camera mainCamera;
    private float startFOV;

    private void Awake()
    {
        mainCamera = Camera.main;
        if (playerCamera == null)
            playerCamera = GameObject.FindGameObjectWithTag(Tags.PlayerVirtualCamera);
        cineMachineVirutalCameraConfig = playerCamera.GetComponent<CineMachineVirtualCameraConfig>();
        playerCinemachineVirtualCamera = playerCamera.GetComponent<CinemachineVirtualCamera>();
        cinemachineBasicMultiChannelPerlin = playerCinemachineVirtualCamera.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();
        startCameraDistance = playerCinemachineVirtualCamera.m_Lens.OrthographicSize;
        tempStartCameraDistance = startCameraDistance;
        animator = GetComponent<Animator>();
        startFOV = playerCinemachineVirtualCamera.m_Lens.FieldOfView;
        targetCameraDistance = startCameraDistance;
    }
    public void SetCameraPlayerTarget(Transform target)
    {
        playerCinemachineVirtualCamera.Follow = target;
    }
    public void ShakeCamera(float amplitude = 1, float frecuency = 1, float duration = 0)
    {
        cinemachineBasicMultiChannelPerlin.m_AmplitudeGain = amplitude;
        cinemachineBasicMultiChannelPerlin.m_FrequencyGain = frecuency;
        if (duration != 0)
            CoroutineHandler.ExecuteActionAfter(() => StopCameraShake(), duration, this);

    }
    public void StopCameraShake()
    {
        cinemachineBasicMultiChannelPerlin.m_AmplitudeGain = 0f;
        cinemachineBasicMultiChannelPerlin.m_FrequencyGain = 0f;
    }
    public void ChangeFOV(float targetFOV, float transitionDuration = 0, float duration = 0)
    {
        ChangeFOVTransition(startFOV, targetFOV, transitionDuration);
        if (duration != 0)
            CoroutineHandler.ExecuteActionAfter(() => ChangeFOVTransition(targetFOV, startFOV, transitionDuration), duration, this);
    }

    public void RestoreFOV(float transitionDuration = 0) => ChangeFOVTransition(playerCinemachineVirtualCamera.m_Lens.FieldOfView, startFOV, transitionDuration);

    private void ChangeFOVTransition(float startFOV, float targetFOV, float transitionDuration)
    {
        if (startFOV == targetFOV)
            return;
        if (transitionDuration == 0)
            playerCinemachineVirtualCamera.m_Lens.FieldOfView = targetFOV;
        float timer = 0;
        StartCoroutine(CoroutineHandler.ExecuteActionAfterUpdateUntil(() =>
        {
            timer += Time.deltaTime;
            playerCinemachineVirtualCamera.m_Lens.FieldOfView = Mathf.Lerp(startFOV, targetFOV, timer / transitionDuration);
            return timer > transitionDuration;
        }, () => playerCinemachineVirtualCamera.m_Lens.FieldOfView = targetFOV));
    }
    public void SetCameraState(string boolName, bool state) => animator.SetBool(boolName, state);
    public void ChangeZoom(float targetZoomValue = 1, float duration = default)
    {
        float zoomDuration = zoomTransitionDuration;
        if (duration != default)
            zoomDuration = duration;
        tempStartCameraDistance = targetZoomValue;
        float currentAddingValue = targetCameraDistance - playerCinemachineVirtualCamera.m_Lens.OrthographicSize;
        Zoom(playerCinemachineVirtualCamera.m_Lens.OrthographicSize, targetZoomValue + currentAddingValue, zoomDuration);
    }
    public void AddZoom(float zoomValueToAdd = 1, float duration = default)
    {
        float zoomDuration = zoomTransitionDuration;
        if (duration != default)
            zoomDuration = duration;
        float currenCameraDistance = playerCinemachineVirtualCamera.m_Lens.OrthographicSize;
        float currentAddingValue = targetCameraDistance - currenCameraDistance;
        Zoom(currenCameraDistance, currenCameraDistance + zoomValueToAdd + currentAddingValue, zoomDuration);
    }
    public void RestoreZoomValue(float duration = default, bool overwriteZoom = false)
    {
        float zoomDuration = zoomTransitionDuration;
        if (duration != default)
            zoomDuration = duration;
        if (overwriteZoom)
            tempStartCameraDistance = startCameraDistance;
        float currenCameraDistance = playerCinemachineVirtualCamera.m_Lens.OrthographicSize;
        Zoom(currenCameraDistance, tempStartCameraDistance, zoomDuration);
    }
    private void Zoom(float to, float from, float duration)
    {
        stopZoom = false;
        if (duration <= 0)
        {
            playerCinemachineVirtualCamera.m_Lens.OrthographicSize = from;
            return;
        }
        targetCameraDistance = from;
        float timer = 0;
        if (zoomingCourutine != null)
            StopCoroutine(zoomingCourutine);
        zoomingCourutine = StartCoroutine(CoroutineHandler.ExecuteActionAfterUpdateUntil(() =>
        {
            timer += Time.deltaTime;
            float camereDistance = Mathf.Lerp(to, targetCameraDistance, timer / duration);
            playerCinemachineVirtualCamera.m_Lens.OrthographicSize = camereDistance;
            return timer > duration || stopZoom;
        }));
    }
    public Vector3 GetCameraForward() => Quaternion.AngleAxis(-CameraYAngleOffset, mainCamera.transform.right) * mainCamera.transform.forward;
    public void StopZoom() => stopZoom = true;
    #region Debug
    public void SetCameraPlayer(bool lookPlayer)
    {
        //cameraAnimator.SetBool("LookPlayer", lookPlayer);
    }

    public void ZoomCameraByDebug(float zoomValue) => cineMachineVirutalCameraConfig?.ZoomByDebug(zoomValue);

    public float GetZoomCameraByDebug(bool scale = false) => cineMachineVirutalCameraConfig.GetZoomByDebug(scale);

    #endregion
}
