using Cinemachine;
using NaughtyAttributes;
using UnityEngine;

public class SensibilityCamera : MonoBehaviour
{
    [SerializeField] [Expandable] private CameraData cameraData;
    private CinemachinePOV cinemachinePOV;
    private void Awake()
    {
        cinemachinePOV = gameObject.GetComponent<CinemachineVirtualCamera>().GetCinemachineComponent<CinemachinePOV>();
        SetSensibility(cameraData.SensibilityValue);
    }
    private void OnEnable()
    {
        cameraData.OnSensibilityChange += SetSensibility;
    }
    private void OnDisable()
    {
        cameraData.OnSensibilityChange -= SetSensibility;
    }
    private void OnDestroy()
    {
        cameraData.OnSensibilityChange -= SetSensibility;
    }

    private void SetSensibility(float sensibilityValue)
    {
        if(cinemachinePOV == null)
            cinemachinePOV = gameObject.GetComponent<CinemachineVirtualCamera>().GetCinemachineComponent<CinemachinePOV>();
        cinemachinePOV.m_HorizontalAxis.m_MaxSpeed = cameraData.StartXSensibility * sensibilityValue;
        cinemachinePOV.m_VerticalAxis.m_MaxSpeed = cameraData.StartYSensibility * sensibilityValue;
    }

}
