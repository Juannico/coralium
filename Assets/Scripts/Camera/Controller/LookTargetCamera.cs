using Cinemachine;
using TamarilloTools;
using UnityEngine;
using UnityEngine.InputSystem;

public class LookTargetCamera : MonoBehaviour
{
    [SerializeField] protected InputActionReference inputAction;
    [SerializeField] private ButtonPromptData uiBackButtonPrompInfo;
    [SerializeField] private GameObject decalCamera;
    [SerializeField] private bool useFade;
    private CinemachineBrain cinemachineBrain;
    private PlayerInputManager playerInputManager;
    public delegate void OnAction();
    public OnAction OnLookTarget;
    private void Awake()
    {
        if (cinemachineBrain == null)
            cinemachineBrain = Camera.main.GetComponent<CinemachineBrain>();
        playerInputManager = PlayerInputManager.Instance;
        decalCamera.gameObject.SetActive(false);
    }
    public void StartLookTarget()
    {
        if (useFade)
        {
            FadeUI.FadeOut();
            CoroutineHandler.ExecuteActionAfter(() => FadeUI.FadeIn(), Constants.OneSecondDelay + FadeUI.GetFadeDuration(), this);
            CoroutineHandler.ExecuteActionAfter(LookTartget, FadeUI.GetFadeDuration() * 2, this);
            return;
        }
        LookTartget();
    }
    private void LookTartget()
    {
        UIManager.Instance.ShowButtonPrompt(uiBackButtonPrompInfo);
        inputAction.action.Enable();
        inputAction.action.canceled += StopWatchingInputAction;
        SetLookState(true);
        OnLookTarget?.Invoke();
    }

    private void SetLookState(bool lookState)
    {
        decalCamera.gameObject.SetActive(lookState);
        if (cinemachineBrain == null)
            cinemachineBrain = Camera.main.GetComponent<CinemachineBrain>();
        cinemachineBrain.enabled = !lookState;
        playerInputManager.SetInputState(!lookState);
    }
    protected void StopWatchingInputAction(InputAction.CallbackContext obj)
    {

        if (useFade)
        {
            FadeUI.FadeOut();
            CoroutineHandler.ExecuteActionAfter(() => FadeUI.FadeIn(), Constants.OneSecondDelay + FadeUI.GetFadeDuration(), this);
            CoroutineHandler.ExecuteActionAfter(StopWatching, FadeUI.GetFadeDuration() * 2, this);
            return;
        }
        StopWatching();
    }
    private void StopWatching()
    {
        UIManager.Instance.HideButtonPrompt();
        inputAction.action.canceled -= StopWatchingInputAction;
        inputAction.action.Disable();
        SetLookState(false);
    }

}
