using Cinemachine;
using UnityEngine;

public class KinematicsHandler : MonoBehaviour
{
    [SerializeField] private CinemachineVirtualCamera virtualCamera;
    [SerializeField] private Transform cameraTransformToFollowAndLook;
    [SerializeField] private bool shouldMoveCamera;
    [SerializeField] private bool shouldOverrideCameraTarget;

    public delegate void OnEnd();
    public OnEnd OnEndKinematic;
    public void StartSmoothPathCinematic(CinemachineSmoothPath cinemachineSmoothPath = null, float waitTime = 0f, float moveTime = 0f, bool stop = false)
    {
        virtualCamera.Priority = 10;
        if (shouldOverrideCameraTarget)
        {
            if (cinemachineSmoothPath != null)
                virtualCamera.transform.position = cinemachineSmoothPath.EvaluatePosition(0);
            virtualCamera.transform.LookAt(cameraTransformToFollowAndLook);
        }
        if (!shouldMoveCamera)
            return;
        float timer = 0;
        virtualCamera.m_Follow = cameraTransformToFollowAndLook;
        virtualCamera.m_LookAt = cameraTransformToFollowAndLook;
        CinemachineTrackedDolly cinemachineTrackedDolly = virtualCamera.GetCinemachineComponent<CinemachineTrackedDolly>();
        if (cinemachineSmoothPath != null)
            cinemachineTrackedDolly.m_Path = cinemachineSmoothPath;
        cinemachineTrackedDolly.m_PathPosition = 0;
        CoroutineHandler.ExecuteActionAfter(() =>
        {
            StartCoroutine(CoroutineHandler.ExecuteActionAfterUpdateUntil(() =>
            {
                timer += Time.deltaTime;
                cinemachineTrackedDolly.m_PathPosition = timer * cinemachineSmoothPath.m_Waypoints.Length / moveTime;
                return timer > moveTime;
            }, () =>
             {
                 if (stop)
                     StopSmoothPathCinematic();
             }));
        }, waitTime, this);
    }
    public void StopSmoothPathCinematic()
    {
        OnEndKinematic?.Invoke();
        CoroutineHandler.ExecuteActionAfter(() => virtualCamera.Priority = 0, Constants.OneSecondDelay, this);
    }


#if UNITY_EDITOR
    public void SetTestPath(float path)
    {
        CinemachineTrackedDolly cinemachineTrackedDolly = virtualCamera.GetCinemachineComponent<CinemachineTrackedDolly>();
        CinemachineSmoothPath cinemachineSmoothPath = cinemachineTrackedDolly.m_Path as CinemachineSmoothPath;
        cinemachineTrackedDolly.m_PathPosition = cinemachineSmoothPath.m_Waypoints.Length * path;
    }
#endif
}
