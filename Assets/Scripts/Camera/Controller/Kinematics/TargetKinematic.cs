using UnityEngine;

public class TargetKinematic : BaseKinematic
{
    [SerializeField] private Transform targetTransfrom;
    [SerializeField] private BasicStartConversation conversation;

    public override void StartKinematic()
    {
        base.StartKinematic();
        lookTargetCameraController.StartTarget(targetTransfrom,2);
        if (!startConversation)
        {
            CoroutineHandler.ExecuteActionAfter(() => StopKinematic(), kinematicDuration, this);
            return;
        }
        CoroutineHandler.ExecuteActionAfter(() => conversation.StartConversation((transform) => StopKinematic()),kinematicDelay,this);
    }

    public override void StopKinematic()
    {
        base.StopKinematic();
        lookTargetCameraController.StopTarget(targetTransfrom);
    }
}
