using Cinemachine;
using UnityEngine;

public class BaseKinematic : MonoBehaviour
{
    [SerializeField] protected KinematicsHandler kinematicsHandler;
    [SerializeField] protected float kinematicDelay;
    [SerializeField] protected float kinematicDuration;
    [SerializeField] protected CinemachineSmoothPath cinemachineSmoothPath;
    [SerializeField] protected bool startConversation;
    protected LookTargetCameraController lookTargetCameraController;
    protected CameraController cameraController;
    public delegate void OnAction();
    public OnAction OnStartKinematic;
    public OnAction OnEndKinematic;
    private UIManager uiManager;
    protected virtual void Awake()
    {
        lookTargetCameraController = GameObject.FindGameObjectWithTag(Tags.Player).GetComponentInChildren<LookTargetCameraController>();
        cameraController = GameObject.FindGameObjectWithTag(Tags.CameraController).GetComponentInChildren<CameraController>();
        uiManager = GameManager.Instance.UIManager;
    }
    public virtual void StartKinematic() => uiManager.SetUiVisibility(false);
    public virtual void StopKinematic() => uiManager.SetUiVisibility(true);

    public virtual float GetDelay() => kinematicDelay;
}
