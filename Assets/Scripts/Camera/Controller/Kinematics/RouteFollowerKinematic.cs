using NaughtyAttributes;
using TamarilloTools;
using UnityEngine;

public class RouteFollowerKinematic : BaseKinematic
{
    [Header("Rout Follower Kinematic Settings")]
    [SerializeField] private RouteFollower routeFollower;
    [SerializeField] private bool putInFrontPlayer;
    [ShowIf("putInFrontPlayer")] [SerializeField] private Transform targetPlayerPosition;
    [SerializeField] private BusPath volumen;
    [SerializeField] [Expandable] private SoundVolumenData soundVolumenData;
    [ShowIf("putInFrontPlayer")] [SerializeField] private float offset = 25;
    [Header("Debug")]
    [SerializeField] private bool debug;
    [ShowIf("debug")] [SerializeField] [Range(0, 1)] private float pathPosition;
    private VolumenData volumenData;
    private float PreviouseVolume;
    protected override void Awake()
    {
        base.Awake();
        routeFollower.ShouldMove = false;
        ComponentUtilities.SetComponent<UnHideGameObject>(routeFollower.gameObject);
        routeFollower.gameObject.SetActive(false);
        volumenData = soundVolumenData.GetVolumenData(volumen);
    }
    public override void StartKinematic()
    {
        base.StartKinematic();
        CoroutineHandler.ExecuteActionAfter(() =>
        {
            kinematicsHandler.StartSmoothPathCinematic(cinemachineSmoothPath, kinematicDelay, kinematicDuration);
            OnStartKinematic?.Invoke();
        }, Constants.halfSecondDelay, this);
        StartMove();
        FadeUI.FadeOut();
        CoroutineHandler.ExecuteActionAfter(() => FadeUI.FadeIn(), Constants.OneSecondDelay + FadeUI.GetFadeDuration(), this); // TODO FIX Magic number
        SetEnd();
        PreviouseVolume = volumenData.Volume;
        volumenData.SetVolumen(PreviouseVolume * 0.25f);
    }

    private void StartMove()
    {
        CoroutineHandler.ExecuteActionAfter(() =>
        {
            routeFollower.gameObject.SetActive(true);
            routeFollower.SetVelocityByDuration(Constants.halfSecondDelay + kinematicDelay + kinematicDuration * 1.1f);
            routeFollower.SetStart();
            routeFollower.ShouldMove = true;
        }, Constants.halfSecondDelay + kinematicDelay, this);
    }

    private void SetEnd()
    {
        CoroutineHandler.ExecuteActionAfter(() =>
        {
            OnEndKinematic?.Invoke();
            kinematicsHandler.StopSmoothPathCinematic();
            AfterEndFade();
            OnStartKinematic?.Invoke();
        }, Constants.halfSecondDelay + kinematicDelay + kinematicDuration, this);
    }

    private void AfterEndFade()
    {

        //CoroutineHandler.ExecuteActionAfter(() =>
        //{
        FadeUI.FadeOut();
        CoroutineHandler.ExecuteActionAfter(() => FadeUI.FadeIn(), Constants.OneSecondDelay + FadeUI.GetFadeDuration(), this); // TODO FIX Magic number
        routeFollower.gameObject.SetActive(false);
        routeFollower.ShouldMove = false;
        volumenData.SetVolumen(PreviouseVolume);
        if (putInFrontPlayer)
        {
            routeFollower.gameObject.SetActive(true);
            routeFollower.enabled = false;
            Character player = GameObject.FindGameObjectWithTag(Tags.Player).GetComponentInChildren<Character>();
            StateComponentRotate playerRotateComponent = player.GetComponentInChildren<StateComponentRotate>();
            playerRotateComponent.InstantRotate(targetPlayerPosition.forward);
            player.transform.position = targetPlayerPosition.position;
            playerRotateComponent.StateSharedData.StartIdlingPosition = targetPlayerPosition.position;
            playerRotateComponent.StateSharedData.MovementDirection = Vector3.zero;
            player.RigibodyForceHandle.VelocityForceVector = Vector3.zero;

            routeFollower.transform.position = player.transform.position + targetPlayerPosition.forward * offset;
            routeFollower.transform.forward = -targetPlayerPosition.forward;
        }
        if (startConversation)
        {
            GetComponentInChildren<BasicStartConversation>()?.StartConversation();
        }
        StopKinematic();
        // }, Constants.OneSecondDelay, this); // TODO FIX Magic number
    }

    public override float GetDelay() => base.GetDelay() + Constants.halfSecondDelay;
#if UNITY_EDITOR
    private void OnValidate()
    {
        if (!debug)
            return;
        routeFollower.GuideDistanceTraveled = pathPosition * routeFollower.Route.path.length;
        routeFollower.SetTestRoutPath();
        kinematicsHandler.SetTestPath(pathPosition);
    }
#endif
}
