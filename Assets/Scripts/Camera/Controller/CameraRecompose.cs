using UnityEngine;

public class CameraRecompose : MonoBehaviour
{
    private PlayerCharacter character;
    private CinemachineCameraOffset cinemachineCameraOffset;
    private StateSharedData stateSharedData;
    private CameraController cameraController;
    private IdleStateComponent idleState;
    private Vector3 cinemachineCameraOffsetSmoothVelocity;
    public void Initialize(PlayerController playerController, CinemachineCameraOffset cinemachineCameraOffset) 
    {
        character = playerController.Character;
        this.cinemachineCameraOffset = cinemachineCameraOffset;
        stateSharedData = playerController.StateHandler.Data;
        cameraController = playerController.CameraController;
        idleState = gameObject.GetComponentInParent<IdleStateComponent>();
       
    }
    private void Update()
    {
        Debug.Log("Recompose");
        ReComposeCamera();
    }
    private void ReComposeCamera()
    {
        if (!idleState.enabled || character.BaseCharacter.Data.IsInExternalForce)
        {
            if (cinemachineCameraOffset.m_Offset != Vector3.up * cameraController.CameraYOffset)
                cinemachineCameraOffset.m_Offset = Vector3.SmoothDamp(cinemachineCameraOffset.m_Offset, Vector3.up * cameraController.CameraYOffset, ref cinemachineCameraOffsetSmoothVelocity, 0.2f);
            stateSharedData.StartIdlingPosition = character.Model.transform.position;
            return;
        }
        cinemachineCameraOffset.m_Offset = stateSharedData.StartIdlingPosition - transform.position + Vector3.up * cameraController.CameraYOffset;
    }
}
