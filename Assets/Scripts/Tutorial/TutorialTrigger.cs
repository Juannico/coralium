using System;
using UnityEngine;

/// <summary>
/// Class to be put into the different elemnts that start tutorial windows.
/// Then the tutorial handler will do the rest
/// </summary>
public class TutorialTrigger : MonoBehaviour
{
    [SerializeField] private TutorialHandlerComponent.TutorialType tutorialType;
    [SerializeField] private TutorialData tutorialData;

    public void StartTutorial()
    {
        if (tutorialData == null) throw new Exception("TutorialTrigger:: No tutorial data provided");
        GameManager.Instance.TutorialHandler.StartTutorial(tutorialData, tutorialType);
    }

    public void SetTutorialProperties(TutorialHandlerComponent.TutorialType tutorialType, TutorialData tutorialData)
    {
        this.tutorialType = tutorialType;
        this.tutorialData = tutorialData;
    }
}
