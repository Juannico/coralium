using System.Collections;
using System.Collections.Generic;
using TamarilloTools;
using UnityEngine;

public class TutorialHandlerComponent : MonoBehaviour
{
    public enum TutorialType
    {
        InputControl,
        SimplePopUp,
        FullScreenAbilityPopUp,
        FullScreenItemPopUp
    }

    [SerializeField] private GameObject inputControlTutorialPrefab;
    [SerializeField] private GameObject simplePopUpTutorialPrefab;
    [SerializeField] private GameObject fullscreenAbilityTutorialPanelPrefab;
    [SerializeField] private GameObject fullscreenItemTutorialPanelPrefab;

    private Dictionary<TutorialType, GameObject> tutorials;

    public void StartTutorial(TutorialData tutorialData, TutorialType type)
    {
        if (tutorialData == null) throw new System.Exception("TutorialHandlerComponent:: No tutorial data was passed");
        if(tutorialData.IsCompleted == true)
        {
#if UNITY_EDITOR
            Debug.Log($"TutorialHandlerComponent:: Tutorial has already been completed for this object -> {tutorialData.name}");
#endif
            return;
        }
        CreateTutorialMap();
        GameObject createdtutorial = UIStackSystem.Instance.OpenOverlay(UIManager.Instance.MainCanvas, tutorials[type]);
        createdtutorial.GetComponent<BaseTutorialUI>().Init(tutorialData);
    }

    private void CreateTutorialMap()
    {
        if (tutorials != null) return;
        tutorials = new Dictionary<TutorialType, GameObject>
        {
            {TutorialType.InputControl,inputControlTutorialPrefab },
            {TutorialType.SimplePopUp,simplePopUpTutorialPrefab },
            {TutorialType.FullScreenAbilityPopUp,fullscreenAbilityTutorialPanelPrefab },
            {TutorialType.FullScreenItemPopUp,fullscreenItemTutorialPanelPrefab }
        };
    }
}
