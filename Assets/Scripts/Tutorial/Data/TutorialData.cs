using NaughtyAttributes;
using UnityEngine;
using UnityEngine.InputSystem;

[CreateAssetMenu(fileName = "New Tutorial Data", menuName = "Coralium/UI/Create Tutorial Data", order = 52)]
public class TutorialData : ScriptableObject, ISerializationCallbackReceiver
{
    public TextTableFieldName TutorialTitle;
    public TextTableFieldName TutorialDescription;
    [ShowAssetPreview] public GameObject[] TutorialImageSequences;
    public InputActionReference nextTutorialStepInput;
    public bool IsCompleted;

    public void OnAfterDeserialize()
    {
        IsCompleted = false;
    }
    public void OnBeforeSerialize(){}
}