using NaughtyAttributes;
using UnityEngine;

[CreateAssetMenu(fileName = "New Control Tutorial Data", menuName = "Coralium/UI/Create Control Tutorial Data", order = 52)]
public class ControlTutorialData : TutorialData
{
    [BoxGroup("KBM")] public TextTableFieldName TutorialTitleKBM;
    [BoxGroup("Gamepad")] public TextTableFieldName TutorialTitleGamepad;

    [BoxGroup("KBM")] public TextTableFieldName TutorialDescriptionKBM;
    [BoxGroup("Gamepad")] public TextTableFieldName TutorialDescriptionGamepad;
}
