using NaughtyAttributes;
using System;
using UnityEngine;
[CreateAssetMenu(fileName = "New Ability Tutorial Data", menuName = "Coralium/UI/Create Ability Tutorial Data", order = 52)]

public class AbilityTutorialData : TutorialData
{
    public bool ShowWarning = true;
    [AllowNesting][ShowIf("ShowWarning")] public TextTableFieldName WarningMessage;
    public bool ShowUseInfo = true;
    [AllowNesting][ShowIf("ShowUseInfo")] public ButtonPromptData AbilityPromptData;
    public TextTableFieldName AbilityName;
    public TextTableFieldName AbilityDescription;
}
