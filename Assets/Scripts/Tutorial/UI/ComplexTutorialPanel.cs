using FMODUnity;
using PixelCrushers;
using TamarilloTools;
using UnityEngine;
using UnityEngine.InputSystem;

public class ComplexTutorialPanel : BaseTutorialUI
{
    [Header("Tutorial Panel Objects")]
    [SerializeField] private LocalizeUI titleText;
    [SerializeField] private LocalizeUI abilityName;
    [SerializeField] private LocalizeUI descriptionText;
    [SerializeField] private LocalizeUI warningMessage;
    [SerializeField] private GameObject imagesContainer;
    [SerializeField] private ButtonPrompt buttonPromptTutorial;
    [Header("SFX")]
    [SerializeField] private StudioEventEmitter exitSFX;
    private AbilityTutorialData abilityTutorialData;

    public override void Init(TutorialData dataToDisplay)
    {
        base.Init(dataToDisplay);
        abilityTutorialData = dataToDisplay as AbilityTutorialData;

        nextTutorialStepInput = abilityTutorialData.nextTutorialStepInput;
        PlayerInputManager.Instance.BindInputActionPerformed(nextTutorialStepInput, NextTutorialStep);
        LocalizeUIUtilities.SetLocalizeUI(abilityTutorialData.TutorialTitle, titleText);
        LocalizeUIUtilities.SetLocalizeUI(abilityTutorialData.AbilityName, abilityName);
        LocalizeUIUtilities.SetLocalizeUI(abilityTutorialData.AbilityDescription, descriptionText);
        LocalizeUIUtilities.SetLocalizeUI(abilityTutorialData.WarningMessage, warningMessage);

        for (int i = 0; i < abilityTutorialData.TutorialImageSequences.Length; i++)
        {
            Instantiate(abilityTutorialData.TutorialImageSequences[i], imagesContainer.transform);
        }

        if (buttonPromptTutorial != null)
        {
            buttonPromptTutorial.ResetButton();
            buttonPromptTutorial.SetButtonPromptData(abilityTutorialData.AbilityPromptData);
            buttonPromptTutorial.Init();
        }
    }

    public override void NextTutorialStep(InputAction.CallbackContext ctx)
    {
        // Don't exit until prompt appears
        base.NextTutorialStep(ctx);
        PlayerInputManager.Instance.SetInputModePlayer();
        UIStackSystem.Instance.CloseOverlayScreen(gameObject);
    }
}
