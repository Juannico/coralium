using NaughtyAttributes;
using PixelCrushers;
using System;
using UnityEngine;
using UnityEngine.InputSystem;

public class FloatingTutorialPanel : BaseTutorialUI, ISaveStateJSON, IUIAdapter
{
    [Header("Blink Animation Properties")]
    [BoxGroup("Animation")][SerializeField] private float blinkDuration = 0.5f;
    [BoxGroup("Animation")][InfoBox("Set to 0 for full blink")][SerializeField] private float alphaValue = 0.2f;
    [BoxGroup("Animation")][SerializeField] private float finalFadeDuration = 3f;
    [Header("Properties")]
    [SerializeField] private GameObject kbmTutorialContainer;
    [SerializeField] private GameObject gamepadTutorialContainer;
    [SerializeField] private LocalizeUI instructionText;

    private bool tutoCompleted;
    private CanvasGroup alphaCanvas;
    private ControlTutorialData controlTutorial;

    protected void Awake()
    {
        alphaCanvas = GetComponent<CanvasGroup>();
        gameObject.AddComponent<UIInputAdapter>();
        DataManager.UpdateStateInterfaceValue(this);
    }
    protected void Start()
    {
        LeanTween.alphaCanvas(alphaCanvas, alphaValue, blinkDuration).setRepeat(-1).setLoopPingPong();
    }

    public void UpdateOnInputDeviceChanged(string newDevice)
    {
        if (tutorialData == null) throw new Exception("FloatingTutorialPanel::No tutorial data found");
        if(newDevice == Constants.GamepadString)
        {
            instructionText.textTable = controlTutorial.TutorialDescriptionGamepad.TextTable;
            instructionText.fieldName = controlTutorial.TutorialDescriptionGamepad.FieldName;
            kbmTutorialContainer.SetActive(false);
            gamepadTutorialContainer.SetActive(true);
        }
        else if(newDevice == Constants.KBMString)
        {
            instructionText.textTable = controlTutorial.TutorialDescriptionKBM.TextTable;
            instructionText.fieldName = controlTutorial.TutorialDescriptionKBM.FieldName;
            kbmTutorialContainer.SetActive(true);
            gamepadTutorialContainer.SetActive(false);
        }
        instructionText.UpdateText();
    }

    public override void Init(TutorialData dataToDisplay)
    {
        base.Init(dataToDisplay);
        controlTutorial = dataToDisplay as ControlTutorialData;
        Instantiate(tutorialData.TutorialImageSequences[0], kbmTutorialContainer.transform);
        Instantiate(tutorialData.TutorialImageSequences[1], gamepadTutorialContainer.transform);
        UpdateOnInputDeviceChanged(PlayerInputManager.Instance.DeviceChange.CurrentControlSchema);
    }

    public override void NextTutorialStep(InputAction.CallbackContext ctx)
    {
        // We basically need to show the next tutorial after vanishing the current one
        base.NextTutorialStep(ctx);

        alphaCanvas.alpha = 1f;
        LeanTween.cancelAll();
        LeanTween.alphaCanvas(alphaCanvas, 0, finalFadeDuration)
            .setOnComplete(() => GameManager.Instance.UIManager.StartNextTutorial(gameObject));
    }

    #region ISaveStateJSON
    public string KeyName => "TutoCompleted";
    public string Path => $"FloatingTutorialPanel";
    public bool State
    {
        get => tutoCompleted;
        set
        {
            tutoCompleted = value;
            Save();
        }
    }
    public void Save() => DataManager.SaveInterface(this);
    #endregion
}
