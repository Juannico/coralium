using UnityEngine;
using UnityEngine.InputSystem;

public class BaseTutorialUI : MonoBehaviour
{
    // This could go in the data too but only if we have tutorials that
    // use the same UI but behave differently in game
    [SerializeField] private bool tutorialPausesGame;
    protected InputActionReference nextTutorialStepInput;
    protected TutorialData tutorialData;

    public virtual void Init(TutorialData dataToDisplay)
    {
        if (tutorialPausesGame)
        {
            PlayerInputManager.Instance.SetInputModeUI();
            GameManager.Instance.SetGamePaused(true);
        }

        tutorialData = dataToDisplay;
        nextTutorialStepInput = tutorialData.nextTutorialStepInput;
        PlayerInputManager.Instance.BindInputActionPerformed(nextTutorialStepInput, NextTutorialStep);
    }

    public virtual void NextTutorialStep(InputAction.CallbackContext ctx)
    {
        // Next step for the tutorial, can be closing too.
        if (tutorialPausesGame)
        {
            PlayerInputManager.Instance.SetInputModePlayer();
            GameManager.Instance.SetGamePaused(false);
        }
        PlayerInputManager.Instance.UnbindInputActionPerformed(nextTutorialStepInput, NextTutorialStep);
        tutorialData.IsCompleted = true;
    }
}
