using PixelCrushers;
using TamarilloTools;
using UnityEngine;
using UnityEngine.InputSystem;

public class SimpleTutorialPanel : BaseTutorialUI
{
    [SerializeField] private LocalizeUI itemInstruction;
    [SerializeField] private LocalizeUI itemDescription;
    [SerializeField] private GameObject imagesContainer;

    public override void Init(TutorialData dataToDisplay)
    {
        base.Init(dataToDisplay);

        LocalizeUIUtilities.SetLocalizeUI(dataToDisplay.TutorialTitle, itemInstruction);
        LocalizeUIUtilities.SetLocalizeUI(dataToDisplay.TutorialDescription, itemDescription);

        for (int i = 0; i < dataToDisplay.TutorialImageSequences.Length; i++)
        {
            Instantiate(dataToDisplay.TutorialImageSequences[i], imagesContainer.transform);
        }
    }

    public override void NextTutorialStep(InputAction.CallbackContext ctx)
    {
        // Don't exit until prompt appears
        base.NextTutorialStep(ctx);
        PlayerInputManager.Instance.SetInputModePlayer();
        UIStackSystem.Instance.CloseOverlayScreen(gameObject);
    }
}
