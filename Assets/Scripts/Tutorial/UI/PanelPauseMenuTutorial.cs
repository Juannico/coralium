using PixelCrushers;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class PanelPauseMenuTutorial : BaseScreenUI, IUIAdapter
{
    [SerializeField] private LocalizeUI tutoText;
    [SerializeField] private TextTableFieldName presskbTextTable;
    [SerializeField] private TextTableFieldName pressGamePadTextTable;
    [SerializeField] private LocalizeUI pressText;
    [SerializeField] private TextTableFieldName exitTextTable;
    [SerializeField] private LocalizeUI exitText;
    private TextTableFieldName tutoTextTable;
    private bool tutorialActivated;
    private BaseScreenUI targetPanel;
    protected override void OnEnable()
    {
        base.OnEnable();
        if (tutorialActivated)
            ShowTutorial(tutoTextTable,targetPanel);
    }
    protected override void DelayEnabled()
    {
        base.DelayEnabled();
        PlayerInputManager.Instance.SetInputModeUI();
    }
    protected override void OnDisable()
    {
        base.OnDisable();
        PlayerInputManager.Instance.SetInputModePlayer();
    }
    public void UpdateOnInputDeviceChanged(string newDevice)
    {
        LocalizeUIUtilities.SetLocalizeUI(exitTextTable, exitText);
        TextTableFieldName pressTextTable = newDevice == Constants.GamepadString 
            ? pressGamePadTextTable 
            : presskbTextTable;
        LocalizeUIUtilities.SetLocalizeUI(pressTextTable, pressText);
        if (tutoTextTable != null)
            LocalizeUIUtilities.SetLocalizeUI(tutoTextTable, tutoText);
    }

    protected virtual void Close(InputAction.CallbackContext ctx) => CloseTutorial();
    public void CloseTutorial()
    {
        gameObject.SetActive(false);
        tutorialActivated = false;
    }
    public void ShowTutorial(TextTableFieldName tutoTextTable, BaseScreenUI targetPanel)
    {
        this.tutoTextTable = tutoTextTable;
        this.targetPanel = targetPanel;
        gameObject.SetActive(true);
        tutorialActivated = true;
    }
}
