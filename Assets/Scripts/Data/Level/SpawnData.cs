using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class SpawnData
{
    [JsonConverter(typeof(Vector3Converter))]
    [HideInInspector] public Vector3 CheckPointPosition;
    [JsonConverter(typeof(Vector3Converter))]
    [HideInInspector] public Vector3 CheckPointDirection;
    [SerializeField] public List<bool> ZonesLoadedSaved;
    public bool IsSaved;
    public void ResetValues()
    {
        CheckPointPosition = Vector3.zero;
        CheckPointDirection = Vector3.zero;
        IsSaved = false;
        ZonesLoadedSaved.Clear();
    }
    public void SetLevelCheckPoint(Vector3 starPosition, Vector3 startDirection, LevelManager levelManager)
    {
        if (levelManager.IsChangingLevel)
            return;
        IsSaved = true;
        CheckPointPosition = starPosition;
        CheckPointDirection = startDirection;
        levelManager.LastSpawmLevelDataIndex = levelManager.LevelIndex;
        ZonesLoadedSaved = new List<bool>(levelManager.CurrentLevelData.ZonesLoaded);
    }
}
