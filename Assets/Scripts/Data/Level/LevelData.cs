using NaughtyAttributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

[Serializable]
[JsonObject(MemberSerialization.OptIn)]
public class LevelData
{
    [Scene] public string LevelSceneName;
    [JsonProperty]
    [ReadOnly] [AllowNesting] public List<bool> ZonesLoaded = new List<bool>();
    [JsonProperty]
    public SpawnData Spawn;
    public bool CanTravel;
    [JsonProperty]
    [ShowIf("CanTravel")] public TravelData Travel;
    [NonSerialized] public GrowCoralData GrowCoralData;
    [JsonProperty] [ReadOnly] [AllowNesting] public int GrowCoralFoodAmount;
    public EventEmitterData eventEmitterData;
    public void ResetValues()
    {
        Spawn.ResetValues();
        ZonesLoaded.Clear();
        GrowCoralFoodAmount = 0;
        Travel.Locked = true;
    }
    public void UnloadLevel()
    {
        if (GrowCoralData == null)
            return;
        GrowCoralFoodAmount = GrowCoralData.collectableData.Amount;
    }
}
