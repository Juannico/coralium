using Newtonsoft.Json;
using System;
using UnityEngine;

[Serializable]
[JsonObject(MemberSerialization.OptIn)]
public class TravelData
{
    public int Value;
    [JsonProperty]
    [SerializeField] private bool isLocked = true;
    public bool Locked
    {
        get => isLocked;
        set
        {
            isLocked = value;
            OnLockChanged?.Invoke(value);
        }
    }
    public delegate void OnBoolChanged(bool boolChanged);
    public OnBoolChanged OnLockChanged;
}
