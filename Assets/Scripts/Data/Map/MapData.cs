using NaughtyAttributes;
using UnityEngine;

[CreateAssetMenu(fileName = "MapData", menuName = "Coralium/Map/Map Data")]
public class MapData : ScriptableObject, IResetSOOnExitPlay, ISaveStateJSON
{
    [SerializeField] private TextTableFieldName UpdatedMapText;
    [SerializeField] private IconItemDataPopUp mapTutoDataPopUp;
    [Expandable] public MapItem[] mapParts;
    private bool firstUnlocked = true;
    public delegate void OnBoolChange(bool boolState);
    public OnBoolChange OnUnlockedChange;

    public void Initialize()
    {
        for (int i = 0; i < mapParts.Length; i++)
        {
            mapParts[i].PartIndex = i;
            mapParts[i].OnPurchaseChange += ShowTutoMap;
        }
    }
    public void ShowTutoMap(bool purchaseState)
    {
        if (!Application.isPlaying)
            return;
        if (!purchaseState)
            return;
        if (!State)
            return;
        State = false;
        DialogueTutorialPanel ItemCollectedPopUp = GameManager.Instance.UIManager.ItemCollectedTutoPopUp;
        if (mapTutoDataPopUp != null)
            ItemCollectedPopUp.OpenPanel(mapTutoDataPopUp);
        ShowIconItemCollected iconItemCollectedTutoPopUp = GameManager.Instance.UIManager.IconItemCollectedTutoPopUp;
        if (iconItemCollectedTutoPopUp != null)
            iconItemCollectedTutoPopUp.Show(UpdatedMapText, mapTutoDataPopUp.Icon);
        foreach (MapItem mapItem in mapParts)
            mapItem.OnPurchaseChange -= ShowTutoMap;
    }

    #region ISaveStateJSON
    public bool State
    {
        get => firstUnlocked;
        set
        {
            firstUnlocked = value;
            Save();
        }
    }
    public string KeyName => "FirstUnlocked";
    public string Path => $"Map";
    public void Save() => DataManager.SaveInterface(this, false);
    #endregion

    #region Reset OnSwitchPlaymode
    public void ResetOnExitPlay()
    {
        firstUnlocked = true;
        foreach (MapItem mapItem in mapParts)
            mapItem.OnPurchaseChange -= ShowTutoMap;
    }
    #endregion

}
