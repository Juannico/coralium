using NaughtyAttributes;
using Newtonsoft.Json;
using System;
using UnityEngine;

[CreateAssetMenu(fileName = "MapItem", menuName = "Coralium/Map/Item", order = 52)]
[JsonObject(MemberSerialization.OptIn)]
public class MapItem : Item, ISaveSOJSONPath
{
    [Header("Map Item Settings")]
    [JsonProperty]
    [SerializeField] private bool isUnlocked;
    public Action<int> OnUnlockeIndex;
    public bool IsUnlocked
    {
        get => isUnlocked;
        set
        {
            isUnlocked = value;
            Save();
            if (isUnlocked)
                OnUnlockeIndex?.Invoke(PartIndex);
        }
    }
    [ReadOnly] public int PartIndex;

    #region Override Item Methods
    public override bool Buy(CollectableData coin)
    {
        if (!base.Buy(coin))
            return false;
        IsUnlocked = true;
        return true;
    }
    #endregion

    #region Reset OnSwitchPlaymode
    public override void ResetOnExitPlay()
    {
        base.ResetOnExitPlay();
        IsUnlocked = false;
    }
    #endregion

    #region SaveJSON
    public string Path { get => $"MapData/{name}"; }
    private void Save() => DataManager.SaveObject(this, Path);
    #endregion
}
