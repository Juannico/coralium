using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.IO;
using UnityEngine;

public static class DataManager
{
    private const int maxAmountOfData = 3;
    private static int currentDataIndex = 1;
    public static bool[] DataActive = new bool[maxAmountOfData];
    private static string dataPath
    {
        get => $"{Application.persistentDataPath}/GameData_{currentDataIndex}.json";
    }
    private static string defaultDataPath = $"{Application.persistentDataPath}/DefaultGameData.json";
    private static string leveltPath = $"Data/Levels";
    private static string objectPath = $"Data/ObjectData";
    private static bool isUpdating = false;
    public static int LanguageIndex;
    private static UIManager uiManager;
    private static readonly JsonSerializerSettings jsonSetting = new();
    /// <summary>
    /// Sets the JSON settings and creates default data if it does not exist.
    /// </summary>
    /// <param name="gameData">The GameData with the SO to save.</param>
    /// <returns>
    /// Returns true if the default data does not exist; otherwise, returns false.
    /// </returns>
    /// <remarks>
    /// The return value is used in UnityEditor to test the game when it starts without using the main menu.
    /// </remarks>
    public static bool Initialize(GameData gameData)
    {
        jsonSetting.Formatting = Formatting.Indented;
        jsonSetting.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
        if (!File.Exists(defaultDataPath))
        {
            CreateDefaulData(gameData);
            return true;
        }
        return false;
    }
    public static void SaveObject(Object obj, string objPath)
    {
        if (!Application.isPlaying)
            return;
        if (isUpdating)
            return;
        objPath = $"{objectPath}/{objPath}";
        string valueToAdd = JsonConvert.SerializeObject(obj, jsonSetting);
        SaveJson(objPath, valueToAdd);
    }

    public static void SaveInterface(ISaveStateJSON interfaceToSave, bool includeLevelPath = true)
    {
        if (isUpdating)
            return;    
        string interfacePath = $"{interfaceToSave.Path}";
        if (includeLevelPath)
            interfacePath = $"{leveltPath}/{interfacePath}";
        JObject jsonObjectToAdd = new JObject();
        jsonObjectToAdd[interfaceToSave.KeyName] = JToken.FromObject(interfaceToSave.State);
        string valueToAdd = jsonObjectToAdd.ToString();
        SaveJson(interfacePath, valueToAdd);
    }
    private static void SaveJson(string objPath, string valueToAdd, bool defaultData = false)
    {
        string currentdataPath = defaultData ? defaultDataPath : dataPath;
        bool create = !File.Exists(currentdataPath);
        if (create && !defaultData)
            return;
        JObject jsonObjectToAdd = JObject.Parse(valueToAdd);
        JObject jsonObject = new JObject();
        if (!create)
            jsonObject = JObject.Parse(File.ReadAllText(currentdataPath));
        string[] keys = objPath.Split('/');
        JObject currentObject = jsonObject;
        for (int i = 0; i < keys.Length - 1; i++)
        {
            string key = keys[i];
            if (currentObject.ContainsKey(key))
            {
                currentObject = (JObject)currentObject[key];
                continue;
            }
            JObject newObject = new JObject();
            currentObject[key] = newObject;
            currentObject = newObject;
        }
        string lastKey = keys[keys.Length - 1];
        currentObject[lastKey] = jsonObjectToAdd;
        if(!defaultData)
            SetUIManager();
        WriteJson(jsonObject.ToString(), defaultData);
    }
    public static void UpdateStateInterfaceValue(ISaveStateJSON interfaceToSave, bool includeLevelPath = true)
    {
        string interfacePath = $"{interfaceToSave.Path}";
        if (includeLevelPath)
            interfacePath = $"{leveltPath}/{interfacePath}";
        try
        {
            string json = File.ReadAllText(dataPath);
            JObject jsonObject = JObject.Parse(json);
            JToken jTokenValue = GetJsonObject(jsonObject, interfacePath);
            if (jTokenValue != null)
                interfaceToSave.State = jTokenValue.Value<bool>(interfaceToSave.KeyName);
        }
        catch (FileNotFoundException)
        {
            SaveInterface(interfaceToSave);
        }
    }

    public static void Update(GameData gameData)
    {
        isUpdating = true;
        try
        {
            string json = File.ReadAllText(dataPath);
            JObject jsonObject = JObject.Parse(json);
            ISaveSOJSONPath[] scriptableObjectsToUpdate = gameData.SaveJSONPaths;
            foreach (ISaveSOJSONPath scriptableObject in scriptableObjectsToUpdate)
            {
                string objPath = $"{objectPath}/{scriptableObject.Path}";
                JToken jTOkenValue = GetJsonObject(jsonObject, objPath);
                if (jTOkenValue != null && scriptableObject is ScriptableObject)
                    JsonUtility.FromJsonOverwrite(jTOkenValue.ToString(), scriptableObject);
            }
        }
        catch (FileNotFoundException e)
        {
            Debug.LogError($"Error to read file: {e}");
        }
        isUpdating = false;
    }
    private static JToken GetJsonObject(JObject jsonObject, string objPath)
    {
        string[] keys = objPath.Split('/');
        JObject currentObject = jsonObject;
        for (int i = 0; i < keys.Length - 1; i++)
        {
            string key = keys[i];
            if (currentObject.ContainsKey(key))
                currentObject = (JObject)currentObject[key];
        }
        string lastKey = keys[keys.Length - 1];
        if (currentObject.ContainsKey(lastKey))
            return currentObject[lastKey];
        return null;
    }
    public static void Reset(bool delete = false)
    {
        for (int i = 1; i < maxAmountOfData + 1; i++)
            DeleteData(i);
        if (!delete)
            return;
        if (File.Exists(defaultDataPath))
            File.Delete(defaultDataPath);

    }
    private static void CreateDefaulData(GameData gameData)
    {
        foreach (ISaveSOJSONPath scriptableObject in gameData.SaveJSONPaths)
        {
            string objPath = $"{objectPath}/{scriptableObject.Path}";
            string valueToAdd = JsonConvert.SerializeObject(scriptableObject, jsonSetting);
            SaveJson(objPath, valueToAdd, true);
        }
    }

    private static void WriteJson(string json, bool defaultData = false)
    {

        if (defaultData)
        {
            File.WriteAllText(defaultDataPath, json);
            return;
        }
        DataActive[currentDataIndex - 1] = true;
        File.WriteAllText(dataPath, json);
    }

    public static void LoadData(int index)
    {
        currentDataIndex = index;
        if (DataActive[currentDataIndex - 1])
            return;
        string json = File.ReadAllText(defaultDataPath);
        WriteJson(json);
    }
    public static void DeleteData(int index)
    {
        currentDataIndex = index;
        if (!File.Exists(dataPath))
            return;
        DataActive[currentDataIndex - 1] = false;
        File.Delete(dataPath);
    }
    private static void SetUIManager()
    {
        if (uiManager == null)
        {
            GameManager gameManager = GameManager.Instance;
            if (gameManager == null)
                return;
            uiManager = GameManager.Instance.UIManager;
        }
        uiManager.ShowSaveIcon();
    }

}
