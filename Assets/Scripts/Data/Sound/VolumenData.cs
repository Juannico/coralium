using FMOD.Studio;
using FMODUnity;
using NaughtyAttributes;
using System;
using UnityEngine;

[Serializable]
public class VolumenData
{
    [SerializeField] private BusPath path;
    [ReadOnly] public Bus bus;
    [AllowNesting] [ReadOnly] [Range(0, 1)] public float Volume;
    public void Initialize()
    {
        string busPath = GetBusPath();
        bus = RuntimeManager.GetBus(busPath);
        bus.getVolume(out Volume);
    }
    private string GetBusPath()
    {
        switch (path)
        {
            case BusPath.Master:
                return "bus:/";
            case BusPath.Music:
                return "bus:/Music";
            case BusPath.Sfx:
                return "bus:/SFX";
            case BusPath.UI:
                return "bus:/UI";
        }
        return "";
    }
    public void SetVolumen(float newVolumen)
    {
        Volume = newVolumen;
        bus.setVolume(Volume);
    }   
}

public enum BusPath
{
    Master,
    Music,
    Sfx,
    UI,
}

