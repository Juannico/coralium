using FMODUnity;
using System;

[Serializable]
public class EventEmitterData 
{
    public EventReference EventReference;
    public string ParameterName;
    public float ParameterValue;
    public int ParameterIndex;

    public EventEmitterData(EventEmitterData eventEmitterData)
    {
        EventReference = eventEmitterData.EventReference;
        ParameterName = eventEmitterData.ParameterName;
        ParameterValue = eventEmitterData.ParameterValue;
        ParameterIndex = eventEmitterData.ParameterIndex;
    }
}
