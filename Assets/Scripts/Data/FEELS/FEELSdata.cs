using MoreMountains.Feedbacks;
using System;
using System.Collections.Generic;
using UnityEngine;
[Serializable]
public class FEELSdata
{
    [field: Header("Idle")]
    [field: SerializeField] public MMFeedbacks StartIdle { get; private set; }
    [field: SerializeField] public MMFeedbacks StopIdle { get; private set; }
    [field: Header("Stop")]
    [field: SerializeField] public MMFeedbacks StartStop { get; private set; }
    [field: SerializeField] public MMFeedbacks StopStop { get; private set; }
    [field: Header("Move")]
    [field: SerializeField] public MMFeedbacks StartMove { get; private set; }
    [field: SerializeField] public MMFeedbacks StopMove { get; private set; }
    [field: Header("Dash")]

    [field: SerializeField] public MMFeedbacks StartDash { get; private set; }
    [field: SerializeField] public MMFeedbacks StopDash { get; private set; }
    [field: Header("Lunge")]
    [field: SerializeField] public MMFeedbacks StartLunge { get; private set; }
    [field: SerializeField] public MMFeedbacks StopLunge { get; private set; }
    [field: Header("KnockBack")]
    [field: SerializeField] public MMFeedbacks StartKnockBack { get; private set; }
    [field: SerializeField] public MMFeedbacks StopKnockBack  { get; private set; }
    [field: SerializeField] public MMFeedbacks StartCrashKnockBack  { get; private set; }
    [field: SerializeField] public MMFeedbacks StopCrashKnockBack   { get; private set; }

    [HideInInspector] public MMFeedbacks currentStretchFEEL;
}
