using NaughtyAttributes;
using UnityEngine;

[CreateAssetMenu(fileName = "AbilityData", menuName = "Coralium/Abilities/Ability Data")]
public class AbilityData : ScriptableObject
{
    public AbilityTutorialData abilityTutorialData;
    [field: SerializeField] [field: Range(0.1f, 50f)] public float Distance { get; set; }
    [field: SerializeField] [field: Range(0f, 100f)] public float Speed { get; set; }
    [field: SerializeField] [field: Range(0f, 10f)] public float Cooldown { get; set; }
    [field: SerializeField] [field: Range(0.1f, 10f)] public int Damage { get; set; }
    [field: SerializeField] [field: Range(0.1f, 50f)] public float Duration { get; set; }
    [field: SerializeField] [field: Range(0f, 100f)] public float EnergyCost { get; set; }
    [field: SerializeField] public DamageTypes DamageType { get; private set; }
    [field: SerializeField] [field: ShowAssetPreview] public GameObject BehaviourPrefab { get; private set; }
    [field: SerializeField] public bool ChangeColorOnCast { get; private set; }
    [SerializeField] [ColorUsage(true, true)] [ShowIf("ChangeColorOnCast")] public Color CharacterColor;
    [HideInInspector] public bool IsCasting;
    [HideInInspector] public bool IsCombined;
    [HideInInspector] public bool IsLoaded;
    [HideInInspector] public Vector3 Direction;
    [field: SerializeField] public bool HasInputAction { get; private set; }
    [ShowIf("HasInputAction")] [SerializeField] public InputReference InputReference;

    [field: SerializeField] public bool HasLoadTime;
    [ShowIf("HasLoadTime")] [SerializeField] private float loadTime = 0;
    public float LoadTime
    {
        get
        {
            if (InputReference == null)
                return loadTime;
            if (InputReference.InputReferenceData.InputActionInteractionData.IsHold)
            {
                loadTime = InputReference.InputReferenceData.InputActionInteractionData.HoldDuration;
                HasLoadTime = true;
            }
            return loadTime;
        }
    }

    [HideInInspector] public BaseAbilityComboCast BaseAbilityComboCast;
    [HideInInspector] public Vector3 castPosition;


    public delegate void OnAction();
    public OnAction LoadAbility;
    public OnAction LoadedAbility;
    public OnAction CastAbility;
    public OnAction EndAbility;
    public delegate void OnActionBool(bool boolState);

    public OnActionBool OnSwitchLockAbility;

    public delegate void OnComboAbility(BaseAbilityCast baseAbility);
    public OnComboAbility LoadComboAbility;
    public OnComboAbility LoadedComboAbility;
    public OnComboAbility CastCombobility;
    /*
    public InputAction inputAction
    {
        get
        {
            if (!hasInputAction)
                return null;
            return actionReference.action;
        }
    }*/

    public void ResetValue()
    {
        LoadAbility = null;
        LoadedAbility = null;
        CastAbility = null;
        EndAbility = null;

        LoadComboAbility = null;
        LoadedComboAbility = null;
        CastCombobility = null;
        Direction = Vector3.zero;
        //InputReference.CombineInputetReferenceData.Clear();
    }
}
