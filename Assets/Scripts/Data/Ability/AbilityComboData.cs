using UnityEngine;

[CreateAssetMenu(fileName = "AbilityComboData", menuName = "Coralium/Abilities/Ability Combo Data")]
public class AbilityComboData : ScriptableObject
{
    [field: SerializeField][field: Range(0.1f, 5f)] public float DistanceMultiplier { get; private set; }
    [field: SerializeField][field: Range(0.1f, 5f)] public float SpeedMultiplier { get; private set; }
    [field: SerializeField][field: Range(0.1f, 5f)] public float DamageMultiplier { get; private set; }
    [field: SerializeField][field: Range(0.1f, 5f)] public float DurationMultiplier { get; private set; }
    [field: SerializeField][field: Range(0.1f, 5f)] public float EnergyCostMultiplier { get; private set; }

}
