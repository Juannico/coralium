using NaughtyAttributes;
using System;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GameData", menuName = "Coralium/Manager/Game Data")]
public class GameData : ScriptableObject
{
    [Expandable] public Item[] Items;
    [CheckISaveJSONPath] public ScriptableObject[] scriptableObjectsToSave;
    private ISaveSOJSONPath[] saveJSONPaths;
    public ISaveSOJSONPath[] SaveJSONPaths
    {
        get
        {
            if (saveJSONPaths == null || saveJSONPaths.Length == 0)
            {
                List<ISaveSOJSONPath> objectToSave = new List<ISaveSOJSONPath>();
                for (int i = 0; i < scriptableObjectsToSave.Length; i++)
                    objectToSave.Add(scriptableObjectsToSave[i] as ISaveSOJSONPath);
                saveJSONPaths = objectToSave.ToArray();
            }
            return saveJSONPaths;

        }
    }

    public Item GetItem(int id)
    {
        if (id > Items.Length || id < 0)
        {
            Debug.LogError($"Does not exit item with id: {id}");
            return null;
        }
        return Items[id];
    }
}


