using Newtonsoft.Json;
using System;
using UnityEngine;
[CreateAssetMenu(fileName = "GrowCoralData", menuName = "Coralium/ExtraData/Grow Coral Data")]
[JsonObject(MemberSerialization.OptIn)]
public class GrowCoralData : ScriptableObject, IResetSOOnExitPlay, ISaveSOJSONPath, ISerializationCallbackReceiver
{
    /// <summary>
    /// #TODO This needs to change. This is way to convoluted and can be misleading.
    /// Why are there so many Ability Classes?
    /// Having independent systems is ok until data becomes redundant and too complex to follow.
    /// Having Ability Data is fine. All the abilities behaviours (player abilities for a handlers of the player abilities too) is fine. But I don't think UnlockAbiltiy is necessary
    /// You can have either the gamenanager or player Abilities unlock the ability by passing it the ability you want to unlock and then
    /// that class handles what it needs to be done with the ability.
    /// 
    /// A quick fix would be to move the unlock abiltiy to the ability data class and reference abiltiy data here. 
    /// That would be the first step but more refactoring has to be done to keep it more simple and organized.
    /// 
    /// 
    /// Also I think we should look to isntantiate on demand. Meaning that we don't instantiate all the abilities in one go but as we unlock them.
    /// This might seem bad but we only have a few abilities. Performance is a non issue in this case.
    /// </summary>
    [NonSerialized] public PlayerProgressHandle ProgressHandle;

    /// <summary>
    /// I added this because this is the place where all ability data should be stored and handled
    /// </summary>
    public AbilityData abilityData;
    [field: SerializeField] public UnlockAbility AbilityToUnlock { get; private set; }
    [field: SerializeField] public int FoodNumber { get; private set; } = 5;
    [field: SerializeField] public float LevelIntensityMusic { get; private set; } = 1;
    [field: SerializeField] public LayerData LayerData { get; private set; }
    [field: SerializeField] public CollectableData collectableData { get; private set; }
    [JsonProperty]
    [SerializeField] private bool isPlanted = false;
    public bool IsPlanted
    {
        get { return isPlanted; }
        set
        {
            isPlanted = value;
            if (value)
                OnGrowCoralPlanted?.Invoke();
            Save();
        }
    }

    public Action OnGrowCoralPlanted;

    public void ResetOnExitPlay()
    {
        isPlanted = false;
    }
    #region SaveJSON
    public string Path { get => $"CoralData/{name}"; }
    private void Save() => DataManager.SaveObject(this, Path);
    #endregion

    #region ISerializationCallbackReceiver
    public void OnBeforeSerialize()
    {
        //  isPlanted = false;
    }

    public void OnAfterDeserialize()
    {

    }
    #endregion
}
