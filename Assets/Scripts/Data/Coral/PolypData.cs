using Newtonsoft.Json;
using System;
using UnityEngine;

[JsonObject(MemberSerialization.OptIn)]
[CreateAssetMenu(fileName = "PolypData", menuName = "Coralium/ExtraData/Polyp Data")]
public class PolypData : ScriptableObject, IResetSOOnExitPlay, ISaveSOJSONPath, ISerializationCallbackReceiver
{
    public GrowCoralData GrowCoralData;
    [JsonProperty]
    [SerializeField] private bool isCaptured;
    public bool IsCaptured
    {
        get => isCaptured;
        set
        {
            isCaptured = value;
            Save();
        }
    }
    [NonSerialized] public Transform Player = null;
    [JsonProperty]
    [JsonConverter(typeof(Vector3Converter))]
    [SerializeField] private Vector3 offsetPosition = Vector3.zero;
    public Vector3 OffsetPosition
    {
        get => offsetPosition;
        set
        {
            offsetPosition = value;
            Save();
        }
    }
#if UNITY_EDITOR
    private void OnEnable()
    {
        Save();
    }
#endif
    public void ResetOnExitPlay()
    {
        IsCaptured = true;
        Player = null;
        OffsetPosition = Vector3.zero;
    }
    #region SaveJSON
    public string Path { get => $"PolypData/{name}"; }
    private void Save() => DataManager.SaveObject(this, Path);
    #endregion

    #region ISerializationCallbackReceiver
    public void OnBeforeSerialize()
    {
        // isCaptured = true;
    }

    public void OnAfterDeserialize()
    {

    }
    #endregion
}
