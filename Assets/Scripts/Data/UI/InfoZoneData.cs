using System;
using UnityEngine;

[Serializable]
public class InfoZoneData
{
    public Sprite Icon;
    public Color Color;
    public TextTableFieldName Title;
    public TextTableFieldName Description;
    public TextTableFieldName Progress;
    public TextTableFieldName Instruction;
}
