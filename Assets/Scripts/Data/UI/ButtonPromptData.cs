using NaughtyAttributes;
using System;
using UnityEngine;

[CreateAssetMenu(fileName = "New Prompt Data", menuName = "Coralium/UI/Create Button Prompt Data", order = 52)]
public class ButtonPromptData : ScriptableObject
{
    [BoxGroup("KBM")]public TextTableFieldName kbPromptText;
    [BoxGroup("Gamepad")] public TextTableFieldName gamepadPromptText;
    [BoxGroup("KBM")] public GameObject kbButtonPrefab;
    [BoxGroup("Gamepad")] public GameObject gamepadButtonPrefab;
}

