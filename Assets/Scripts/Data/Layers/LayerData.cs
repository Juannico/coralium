using UnityEngine;

[CreateAssetMenu(fileName = "LayerData", menuName = "Coralium/ExtraData/Layer Data")]
public class LayerData:ScriptableObject
{
    [field: SerializeField] public LayerMask PlayerLayer { get; private set; }
    [field: SerializeField] public LayerMask EnemyLayer { get; private set; }
    [field: SerializeField] public LayerMask IgnoreCollider { get; private set; }
    [field: SerializeField] public LayerMask EnviromenLayer { get; private set; }
    [field: SerializeField] public LayerMask Trigger { get; private set; }
    [field: SerializeField] public LayerMask RadarPointLayer { get; private set; }
    [field: SerializeField] public LayerMask FollowFishLayer { get; private set; }
    [field: SerializeField] public LayerMask FishObstacleLayer { get; private set; }


}
