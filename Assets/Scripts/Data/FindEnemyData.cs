using UnityEngine;
[CreateAssetMenu(fileName = "FindEnemyData", menuName = "Coralium/ExtraData/Find Enemy Data")]
public class FindEnemyData: ScriptableObject
{
    [field: SerializeField] public LayerMask LockOnLayer { get; private set; }
    [field: SerializeField][field: Range(1f, 100f)] public float MaxDistanceToFindObjects { get; private set; } = 40f;
    [field: SerializeField][field: Range(0f, 360f)] public float FOVAngle { get; private set; } = 100f;
    public EnemyCharacter TargetEnemy { get;  set; }
    public delegate void OnBoolAction(bool state);
    public OnBoolAction OnSwitchLockOn;
}
