using NaughtyAttributes;
using UnityEngine;
[CreateAssetMenu(fileName = "CameraData", menuName = "Coralium/ExtraData/Camera Data")]
public class CameraData : ScriptableObject
{
    [field: Header("Camera Settings")]
    [field: Header("Sensibilty")]
    [field: Range(0.01f, 0.99f)] [field: SerializeField] public float MinSensibility { get; private set; } = 0.25f;
    [field: Range(1.01f, 5f)] [field: SerializeField] public float MaxSensibility { get; private set; } = 3;
     private float sensibilityValue = 1;

    [field: SerializeField] public float StartXSensibility { get; private set; } = 0.22f;
    [field: SerializeField] public float StartYSensibility { get; private set; } = 0.35f;

    [field: Header("ShakeData")]
    [field: Header("Damage")]
    [field: SerializeField] public float DamageShakeAmplitude { get; private set; }
    [field: SerializeField] public float DamageShakeFrecuency { get; private set; }
    [field: Header("KnockBack")]
    [field: SerializeField] public float KnockBackShakeAmplitude { get; private set; }
    [field: SerializeField] public float KnockBackShakeFrecuency { get; private set; }

    [field: Header("FOVData")]
    [field: SerializeField] public float RestoreFOVDurationMultiplier { get; private set; } = 2;

    [field: Header("Dash")]

    [field: SerializeField] public float DashFOV { get; private set; }
    [field: SerializeField] public float DashFOVTransitionDuration { get; private set; }

    public delegate void OnFloatChange(float floatToChange);
    public OnFloatChange OnSensibilityChange;
    public float SensibilityValue
    {
        get => sensibilityValue;
        set
        {
            sensibilityValue = Mathf.Clamp(value, MinSensibility, MaxSensibility);
            OnSensibilityChange?.Invoke(sensibilityValue);
        }
    }
}
