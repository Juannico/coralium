using System;
using UnityEngine;

[Serializable]
public struct DieData 
{
    [field: SerializeField][field: Range(0.1f, 5f)] public float KnockBackDuration { get; set; }
}
