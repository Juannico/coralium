using System;
using UnityEngine;

[Serializable]
public struct FollowFishData 
{
    [field: SerializeField]public bool canFollowFish { get; set; }
    [field: SerializeField][field: Range(0.5f, 15f)] public float SpeedModifer { get;  set; }
}
