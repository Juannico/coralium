using System;
using UnityEngine;

[Serializable]
public struct SwinData
{
    [field: SerializeField][field: Range(0.1f, 10f)] public float SpeedModifer { get;  set; }
}
