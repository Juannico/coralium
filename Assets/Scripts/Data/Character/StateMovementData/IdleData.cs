using System;
using UnityEngine;

[Serializable]
public struct IdleData 
{
    [field: SerializeField][field: Range(0.1f, 5f)] public float minRandomSpeed { get; set; }
    [field: SerializeField][field: Range(1f, 5f)] public float maxRandomSpeed { get; set; }
    [field: SerializeField][field: Range(0.1f, 25f)] public float minRandomDistance { get; set; }
    [field: SerializeField][field: Range(1f, 50f)] public float maxRandomDistance { get; set; }
}
