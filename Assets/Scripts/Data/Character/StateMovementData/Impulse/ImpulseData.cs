using NaughtyAttributes;
using System;
using UnityEngine;

[Serializable]
public struct ImpulseData 
{
    public float ImpulseDuration { get;  set; } 
    public float ImpulseDistance { get;  set; }
    [field: SerializeField][field: Range(0.1f, 10f)] public float AccelerationSpeed { get;  set; }
    [field: SerializeField][field: Range(0.1f, 2f)] public float TurnDuration { get;  set; }
}
