using System;
using UnityEngine;

[Serializable]
public struct StopData 
{
    [field: SerializeField][field: Range(0.1f, 2.5f)] public float  stopSpeed { get;  set; }
}
