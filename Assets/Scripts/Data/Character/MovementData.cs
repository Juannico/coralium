using System;
using UnityEngine;

[Serializable]
public class MovementData : ScriptableObject
{
    [field: SerializeField][field: Range(0.1f, 2)] public float RotatingSpeedMultiplier { get; set; } = 0.5f;
    [field: SerializeField][field: Range(0.1f, 75)] public float ModelSpeedRotation { get; set; } = 5f;
}
