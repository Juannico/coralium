using NaughtyAttributes;
using System;
using UnityEngine;

[Serializable]
public class Data
{
    [field: SerializeField] [field: Range(0f, 75f)] public float Speed { get; set; }
    [field: SerializeField] [field: Range(0f, 2f)] public float SmoothSpeed { get; set; }
    [field: SerializeField] [field: Range(0f, 100f)] public int MaxHealth { get; private set; }
    [SerializeField] [Range(0f, 100f)] private int health;
    [SerializeField] [Range(0f, 100f)] private int currentHealth;
    [field: SerializeField] [field: Range(0f, 100f)] public float Energy { get; set; }
    [field: SerializeField] [field: Range(0f, 250f)] public float AttackDistance { get; set; }
    [field: SerializeField] [field: Range(0f, 20f)] public int DamageMultiplier { get; set; }
    [field: SerializeField] [field: Range(0f, 20f)] public int Shield { get; set; }
    [field: SerializeField] [field: Range(0f, 20f)] public float InmunityDuration { get; set; } = 1.25f;
    public bool ChangeColorOnInmunity;
    [ShowIf("ChangeColorOnInmunity")] [AllowNesting][ColorUsage(true,true)] public Color InmunityColor;
    [field: SerializeField] public Material DiedMaterial { get; set; }
     

    [NonSerialized] private int extraHealth;
    [NonSerialized] private float currentEnergy;
    public int Health
    {
        get => health;
        set
        {
            bool updateCurrenHelath = currentHealth == health;
            updateCurrenHelath = updateCurrenHelath ? true : value - health > 0;
            int valueClamped = Math.Clamp(value, 0, MaxHealth);
            int valueToAdd = valueClamped - health;
            health = valueClamped;
            if (updateCurrenHelath)
                CurrentHealth += valueToAdd;
            if (value == health)
                OnCharacterHealthAmountChange?.Invoke(health);
        }
    }
    public int ExtraHealth
    {
        get { return extraHealth; }
        set
        {
            extraHealth = value;
            if (extraHealth < 0)
                extraHealth = 0;
            OnCharacterExtraHealthChange?.Invoke(extraHealth);
        }
    }
    public int CurrentHealth
    {
        get { return currentHealth + extraHealth; }
        set
        {
            if (ExtraHealth > 0 && value < CurrentHealth)
            {
                int damageExpextedToRealHealth = (CurrentHealth - value) - extraHealth;
                ExtraHealth -= (CurrentHealth - value);
                if (damageExpextedToRealHealth > 0)
                    CurrentHealth -= damageExpextedToRealHealth;
                return;
            }
            currentHealth = Math.Clamp(value - extraHealth, 0, health);
            OnCharacterHealthChange?.Invoke(currentHealth);
        }
    }
    public float CurrentEnergy
    {
        get { return currentEnergy; }
        set
        {
            if (IsInfinteEnergy)
                return;
            currentEnergy = Math.Clamp(value, 0, Energy);
            OnCharacterEnergyChange?.Invoke(currentEnergy);
        }
    }
    public float ReduceDamagePercentage { get; set; }
    public bool IsInsideDamageArea { get; set; }
    public float TimeInsideDamageArea { get; set; }
    public bool IsRecievingDamage { get; set; }
    public bool IsDead { get; set; }

    [NonSerialized] private int currentShield;
    public int CurrentShield
    {
        get { return currentShield; }
        set
        {
            currentShield = Math.Clamp(value, 0, Shield);
            OnCharacterShieldChange?.Invoke(currentShield);
        }
    }
    public bool CanExternalImpulse { get; set; }
    public Vector3 ExternalImpulseDirection { get; set; }
    public float ImpulseExternalModifier { get; set; }
    [SerializeField] private bool isInmune;
    [HideInInspector] public bool ControlOnInmune;
    public bool IsImmune
    {
        get => isInmune;
        set
        {
            if (ControlOnInmune)
                return;
            isInmune = value;
            OnInmunityChange?.Invoke(value);
        }
    }
    public bool IsInfinteEnergy { get; set; } = false;
    public bool CanMove { get; set; }
    public bool IsInExternalForce { get; set; }
    public bool IsInmuneToExternalForce { get; set; }
    public bool IsPoisioned { get; set; }
    public bool IsScared { get; set; }

    [ColorUsage(true, true)] public Color UIColor;

    public delegate void OnCharacterAction();
    public delegate void OnCharacterIntChanged(int intToChange);
    public delegate void OnCharacterFloatChanged(float floatToChange);
    public delegate void OnCharacterBoolChanged(bool floatToChange);
    public OnCharacterAction OnCharacterInitialized;
    public OnCharacterIntChanged OnCharacterHealthChange;
    public OnCharacterIntChanged OnCharacterHealthAmountChange;
    public OnCharacterIntChanged OnCharacterExtraHealthChange;
    public OnCharacterIntChanged OnCharacterShieldChange;
    public OnCharacterFloatChanged OnCharacterEnergyChange;
    public OnCharacterBoolChanged OnInmunityChange;
    public OnCharacterAction OnCharacterDead;
    public OnCharacterAction EndStateAction { get; set; }
    public Action<Vector2, float, bool> OnCharacterKnockBack { get; set; }
    public Data(Data previouseData)
    {
        Speed = previouseData.Speed;
        SmoothSpeed = previouseData.SmoothSpeed;
        MaxHealth = previouseData.MaxHealth;
        previouseData.health = Mathf.Clamp(previouseData.health, 0, previouseData.MaxHealth);
        health = previouseData.health;
        Energy = previouseData.Energy;
        AttackDistance = previouseData.AttackDistance;
        DamageMultiplier = previouseData.DamageMultiplier;
        Shield = previouseData.Shield;
        InmunityDuration = previouseData.InmunityDuration;
        DiedMaterial = previouseData.DiedMaterial;
        CurrentHealth = previouseData.CurrentHealth;
        ExtraHealth = previouseData.ExtraHealth;
        CurrentEnergy = previouseData.CurrentEnergy;
        ExtraHealth = previouseData.ExtraHealth;
        ReduceDamagePercentage = previouseData.ReduceDamagePercentage;
        IsInsideDamageArea = previouseData.IsInsideDamageArea;
        TimeInsideDamageArea = previouseData.TimeInsideDamageArea;
        IsRecievingDamage = previouseData.IsRecievingDamage;
        IsDead = previouseData.IsDead;
        CurrentShield = previouseData.CurrentShield;
        CanExternalImpulse = previouseData.CanExternalImpulse;
        ExternalImpulseDirection = previouseData.ExternalImpulseDirection;
        ImpulseExternalModifier = previouseData.ImpulseExternalModifier;
        IsImmune = previouseData.IsImmune;
        ControlOnInmune = previouseData.ControlOnInmune;
        ChangeColorOnInmunity = previouseData.ChangeColorOnInmunity;
        InmunityColor = previouseData.InmunityColor;
        IsInfinteEnergy = previouseData.IsInfinteEnergy;
        CanMove = previouseData.CanMove;
        IsInExternalForce = previouseData.IsInExternalForce;
        IsInmuneToExternalForce = previouseData.IsInmuneToExternalForce;
        IsPoisioned = previouseData.IsPoisioned;
        IsScared = previouseData.IsScared;
        UIColor = previouseData.UIColor;
        OnCharacterInitialized = previouseData.OnCharacterInitialized;
        OnCharacterHealthChange = previouseData.OnCharacterHealthChange;
        OnCharacterHealthAmountChange = previouseData.OnCharacterHealthAmountChange;
        OnCharacterExtraHealthChange = previouseData.OnCharacterExtraHealthChange;
        OnCharacterShieldChange = previouseData.OnCharacterShieldChange;
        OnCharacterEnergyChange = previouseData.OnCharacterEnergyChange;
        OnInmunityChange = previouseData.OnInmunityChange;
        OnCharacterDead = previouseData.OnCharacterDead;
        EndStateAction = previouseData.EndStateAction;
        OnCharacterKnockBack = previouseData.OnCharacterKnockBack;
    }

    public void Initilize()
    {
        OnCharacterInitialized?.Invoke();
        CurrentEnergy = Energy;
        ReduceDamagePercentage = 0;
        IsInsideDamageArea = false;
        TimeInsideDamageArea = 0;
        IsDead = false;
        CurrentShield = 0;
        CanExternalImpulse = false;
        CanMove = true;
        ImpulseExternalModifier = 1;
        IsImmune = false;
        IsInExternalForce = false;
        IsInmuneToExternalForce = false;
        IsPoisioned = false;
        IsScared = false;
        IsDead = false;
        IsRecievingDamage = false;
        ExtraHealth = 0;
    }
    public void ResetDelegates()
    {
        OnCharacterInitialized = null;
        OnCharacterHealthChange = null;
        OnCharacterExtraHealthChange = null;
        OnCharacterShieldChange = null;
        OnCharacterEnergyChange = null;
        OnCharacterDead = null;
        OnInmunityChange = null;
        OnCharacterHealthAmountChange = null;
    }
}
