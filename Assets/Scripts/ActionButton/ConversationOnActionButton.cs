using NaughtyAttributes;
using PixelCrushers.DialogueSystem;
using UnityEngine;

public class ConversationOnActionButton : BaseAddActionToButton
{
    [Header("Covnersation Settings")]
    [SerializeField] protected BasicStartConversation conversation;
    [SerializeField] private bool useVariable = false;
    [ShowIf("useVariable")] [VariablePopup(true)] [SerializeField] private string luaBoolName;
    private bool luaBoolValue;
    protected string currentMultipleConversation;

    #region Override Action Button Methods
    protected override void DoAction()
    {
        base.DoAction();
        StartConversation();
    }
    #endregion

    #region Main Methods
    protected void StartConversation()
    {
        DisableActionButton();
        if (useVariable)
            DialogueLua.SetVariable(luaBoolName, luaBoolValue);
        conversation.StartConversation(ActionAfterConversation);
    }
    /// <summary>
    /// Set the Lua bool value if useVairbale is active.
    /// </summary>
    /// <param name="boolValue"> New Lua bool value. </param>
    public void SetLuaBoolValue(bool boolValue)
    {
        if (useVariable)
            luaBoolValue = boolValue;
    }
    #endregion

    #region Reusable Conversation Methods
    /// <summary>
    /// Executes the action when the conversation is ended.
    /// This method is meant to be overridden by child classes to implement custom logic.
    /// </summary>
    /// <param name="transform">
    /// The optional transform parameter that can be used to specify a target transform for the action.
    /// </param>
    protected virtual void ActionAfterConversation(Transform transform = null) => CoroutineHandler.ExecuteActionAfter(DelayActionAfterConversation, Constants.OneSecondDelay, this);
    /// <summary>
    /// Handles the delayed action after the ActionAfterConversation method is called.
    /// This method is meant to be overridden by child classes to implement custom logic.
    /// </summary>
    protected virtual void DelayActionAfterConversation() => EnableActionButton();
    #endregion

}
