using NaughtyAttributes;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;

public class BaseAddActionToButton : MonoBehaviour
{
    [Header("Add action button Settings")]
    [SerializeField] protected InputActionReference inputAction;
    [SerializeField] protected LayerData layerData;
    [SerializeField] private ButtonPromptData promptData;
    [SerializeField] private bool useCollisionHelper;
    [SerializeField] [ShowIf("useCollisionHelper")] private CollisionHelper collisionHelper;
    [SerializeField] private bool useActionEvent;
    [SerializeField] [ShowIf("useActionEvent")] private UnityEvent actionEvent;
    private bool isActionButtonEnabled;
    private bool isApplicationQuitting = false;

    #region Unity Methods
    protected virtual void Awake()
    {
        if (!useCollisionHelper)
            return;
        collisionHelper.TriggerEnter += TriggerEnter;
        collisionHelper.TriggerExit += TriggerExit;
    }
    private void OnDestroy()
    {
        if (!isApplicationQuitting)
            DisableActionButton();
    }
    private void OnApplicationQuit() => isApplicationQuitting = true;
    private void OnTriggerEnter(Collider other)
    {
        if (!useCollisionHelper)
            TriggerEnter(other);
    }
    private void OnTriggerExit(Collider other)
    {
        if (!useCollisionHelper)
            TriggerExit(other);
    }
    #endregion

    #region Main Methods
    private void TriggerEnter(Collider other)
    {
        if (!LayerUtilities.IsSameLayer(layerData.PlayerLayer, other.gameObject.layer))
            return;
        isActionButtonEnabled = true;
        EnableActionButton();
    }
    private void TriggerExit(Collider other)
    {
        if (!LayerUtilities.IsSameLayer(layerData.PlayerLayer, other.gameObject.layer))
            return;
        isActionButtonEnabled = false;
        DisableActionButton();
    }
    protected void EnableActionButton()
    {
        if (!isActionButtonEnabled)
            return;
        PlayerInputManager.Instance.BindInputActionCanceled(inputAction, SetAction);
        UIManager.Instance.ShowButtonPrompt(promptData);
    }
    protected void DisableActionButton()
    {
        PlayerInputManager.Instance.UnbindInputActionCanceled(inputAction, SetAction);
        UIManager.Instance.HideButtonPrompt();
    }
    protected void SetAction(InputAction.CallbackContext obj) => DoAction();
    #endregion

    #region Reusable Action Button Methods
    /// <summary>
    /// Executes the action associated with the button press.
    /// This method is meant to be overridden by child classes to implement custom logic.
    /// </summary>
    protected virtual void DoAction() => actionEvent?.Invoke();
    #endregion

    public void TestDoAction()
    {
        DoAction();
    }

}
