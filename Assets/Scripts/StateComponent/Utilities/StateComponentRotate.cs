using UnityEngine;

public class StateComponentRotate : MonoBehaviour
{
    private Character character;
    public StateSharedData StateSharedData { get; private set; }
    private Camera mainCamera;
    private IdleStateComponent idleState;
    private ImpulseStateComponent impulseState;
    private bool rotateOnIdle;
    private Vector3 selfRotateVelocity;
    private bool lookAt = false;
    private Transform targetLookAt;

    #region Unity Methods
    private void FixedUpdate()
    {
        Rotate();
        SelfRotate();
    }
    #endregion

    #region Main Methods
    public void Initialize(Character character, StateSharedData stateSharedData, bool rotateOnIdle = false)
    {
        this.character = character;
        StateSharedData = stateSharedData;
        StateSharedData.TargetRotation = Quaternion.Euler(Vector3.right);
        this.rotateOnIdle = rotateOnIdle;
        mainCamera = Camera.main;
        idleState = ComponentUtilities.SetComponent<IdleStateComponent>(gameObject);
        impulseState = ComponentUtilities.SetComponent<ImpulseStateComponent>(gameObject);
        character.BaseTransform.localRotation = stateSharedData.TargetRotation;
    }
    private void Rotate()
    {
        if ((idleState.enabled && !rotateOnIdle) && character.BaseCharacter.Data.CanMove && character.BaseTransform.localRotation.eulerAngles.z == 0)
            return;
        Vector3 rotationVector = StateSharedData.MovementDirection;
        if (character.BaseCharacter.Data.IsInExternalForce && StateSharedData.MovementVector.magnitude == 0)
            rotationVector += character.Rigidbody.velocity;
        if (!character.BaseCharacter.Data.CanMove)
            rotationVector = mainCamera.transform.forward;
        if (rotationVector != Vector3.zero)
            StateSharedData.TargetRotation = Quaternion.LookRotation(rotationVector.normalized);
        Rotating();
    }
    private void SelfRotate()
    {
        if (StateSharedData.ExtraRotation != Vector3.zero && !impulseState.enabled)
            StateSharedData.ExtraRotation = Vector3.SmoothDamp(StateSharedData.ExtraRotation, Vector3.zero, ref selfRotateVelocity, 0.1f);
        Vector3 playerRotation = character.BaseTransform.rotation.eulerAngles;
        playerRotation.z = 0;
        Vector3 rotationVector = playerRotation + StateSharedData.ExtraRotation;
        StateSharedData.TargetRotation = Quaternion.Euler(rotationVector);
        Rotating();
    }
    private void Rotating()
    {
        if (lookAt)
            StateSharedData.TargetRotation = Quaternion.LookRotation((targetLookAt.position - character.BaseTransform.position).normalized);
        if (StateSharedData.TargetRotation == Quaternion.Euler(Vector3.zero) && character.BaseTransform.forward == Vector3.zero)
            return;
        character.BaseTransform.rotation = Quaternion.RotateTowards(character.BaseTransform.rotation, StateSharedData.TargetRotation, 10f * character.BaseMovementData.ModelSpeedRotation * Time.fixedDeltaTime * StateSharedData.SelfRotationMultiplier);
        StateSharedData.RotatingMultiplier = Vector3.Dot(StateSharedData.MovementDirection.normalized, character.BaseTransform.forward);
        if (StateSharedData.RotatingMultiplier < 0.9f)
            StateSharedData.RotatingMultiplier = character.BaseMovementData.RotatingSpeedMultiplier;
    }
    /// <summary>
    /// Instantly rotates the character to face a specified forward direction.
    /// </summary>
    /// <param name="forward">The forward direction to rotate the character towards.</param>
    public void InstantRotate(Vector3 forward)
    {
        StateSharedData.TargetRotation = Quaternion.LookRotation(forward);
        character.BaseTransform.forward = forward;    
    }
    /// <summary>
    /// Initiates a look-at behavior where the character continuously faces a specified target.
    /// </summary>
    /// <param name="targetLookAt">The transform of the target the character should look at.</param>
    public void LookAt(Transform targetLookAt)
    {
        lookAt = true;
        this.targetLookAt = targetLookAt;
    }

    public void StopLookAt() => lookAt = false;
    #endregion
}

