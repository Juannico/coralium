using UnityEngine;

public class FollowStateComponent : MoveStateComponent
{
    protected GameObject objectToFollow;
    protected float offSetDistance;

    #region State Methods
    public override void Enter()
    {
        base.Enter();
        objectToFollow = StateSharedData.CurrentFollowObject;
    }
    protected override void Update()
    {
        base.Update();
        StateSharedData.FollowForce = GetDirection();
    }
    protected override void FixedUpdate()
    {
        Vector3 direction = GetDirection();
        if (StateSharedData.HorizontalMove)
            direction.y = 0;
        StateSharedData.MovementDirection = direction;
        base.FixedUpdate();
    }
    #endregion

    #region Reusable FollowState Methods 
    protected virtual Vector3 GetDirection() => Vector3.zero;
    #endregion

}
