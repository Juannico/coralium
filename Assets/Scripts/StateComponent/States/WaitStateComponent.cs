
using UnityEngine;

public class WaitStateComponent : StateComponent
{
    private bool enterByExternalForce;

    #region State Methods
    protected override void Awake()
    {
        base.Awake();
        startFEELS = StateSharedData.FEELSdata.StartIdle;
        stopFEELS = StateSharedData.FEELSdata.StopIdle;
    }
    public override void Enter()
    {
        enterByExternalForce = rigibodyForceHandle.ExternalVelocityForceVector != Vector3.zero;
        base.Enter();
    }
    protected override void Update()
    {
        if (StateSharedData.MovementVector.magnitude != 0)
            OnMove?.Invoke(this);

    }
    protected override void FixedUpdate()
    {
        if (enterByExternalForce && rigibodyForceHandle.ExternalVelocityForceVector == Vector3.zero)
            OnStop?.Invoke(this);
    }
    #endregion
}
