using UnityEngine;

public class SwimStateComponent : MoveStateComponent
{
    public SwinData SwinData;

    #region State Methods
    public override void Enter()
    {
        base.Enter();
#if UNITY_EDITOR
        TriggerDataUpdated(ref SwinData);
#endif
        StateSharedData.MovementSpeedModifier = SwinData.SpeedModifer;

    }
    protected override void FixedUpdate()
    {
        base.FixedUpdate();
        if (StateSharedData.MovementVector.magnitude == 0)
            OnStop?.Invoke(this);
    }
#endregion

}
