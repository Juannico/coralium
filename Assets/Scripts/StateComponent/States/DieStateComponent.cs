using UnityEngine;

public class DieStateComponent : StateComponent
{
    public DieData DieData;
    private float timer;
    private Vector3 startDirection;
    private float direction;
    private float amountOfTurn;

    #region State Methods
    public override void Enter()
    {
        base.Enter();
#if UNITY_EDITOR
        TriggerDataUpdated(ref DieData);
#endif
        startDirection = StateSharedData.BaseTransform.forward;
        StateSharedData.KnockBackDirection = Quaternion.AngleAxis(-90, Vector3.right) * -startDirection;
        direction = Random.Range(0f, 1f) > 0.5f ? 1 : -1;
        timer = 0f;
        StateSharedData.ExtraRotation = Vector3.zero;
        amountOfTurn = 0.5f;
    }
    protected override void Update()
    {
        base.Update();
        timer += Time.deltaTime;
    }
    protected override void FixedUpdate()
    {
        base.FixedUpdate();
        if (StateSharedData.HorizontalMove)
            return;
        RandomRotation();
        SetExtraRotation();
        float smoothMultiplier = DieData.KnockBackDuration * 0.75f / (timer + 0.1f);
        rigibodyForceHandle.VelocityForceVector += (Vector3)StateSharedData.KnockBackDirection.normalized * smoothMultiplier;
    }
    #endregion

    #region Main Methods
    private void RandomRotation()
    {
        StateSharedData.MovementDirection = Vector3.Lerp(StateSharedData.KnockBackDirection, -startDirection, timer / 5f);
    }
    /// <summary>
    /// Set the ExtraRotation uses for rotate on itself
    /// </summary>
    private void SetExtraRotation()
    {
        float angle = Mathf.Lerp(0, 360 * amountOfTurn, timer * 2f / DieData.KnockBackDuration);
        //float angle = 0;
        angle %= 360;
        if (angle > 180)
            angle -= 360;
        StateSharedData.ExtraRotation = Vector3.forward * angle * direction;
    }
    #endregion

}
