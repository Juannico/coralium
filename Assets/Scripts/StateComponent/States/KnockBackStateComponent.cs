using UnityEngine;

public class KnockBackStateComponent : StateComponent
{
    private Vector2 offsetDirection;
    private Vector2 offsetDirectionLerp;
    private float timer;
    private float direction;
    private float amountOfTurn;
    private float timerReclamp;

    #region State Methods
    protected override void Awake()
    {
        base.Awake();
        startFEELS = StateSharedData.isCrashing ? StateSharedData.FEELSdata.StartCrashKnockBack : StateSharedData.FEELSdata.StartKnockBack;
        stopFEELS = StateSharedData.isCrashing ? StateSharedData.FEELSdata.StopCrashKnockBack : StateSharedData.FEELSdata.StopKnockBack;
    }
    public override void Enter()
    {    
        base.Enter();
        offsetDirection = Vector2.Perpendicular(StateSharedData.KnockBackDirection.normalized) * 2;

        direction = Random.Range(0f, 1f) > 0.5f ? 1 : -1;
        timer = 0f;
        StateSharedData.ExtraRotation = Vector3.zero;
        StateSharedData.SelfRotationMultiplier = 5f;
        amountOfTurn = 0.99f;
        rigibodyForceHandle.AffectByExternalForce = false;
    }
    public override void Exit()
    {
        rigibodyForceHandle.AffectByExternalForce = true;
        StateSharedData.SelfRotationMultiplier = 1f;
        base.Exit();
    }
    protected override void Update()
    {
        base.Update();
        timer += Time.deltaTime;
        timerReclamp = timer / StateSharedData.KnockBackDuration;
        if (timer > StateSharedData.KnockBackDuration)
            OnStop?.Invoke(this);
    }
    protected override void FixedUpdate()
    {
        base.FixedUpdate();
        if (StateSharedData.HorizontalMove)
            return;
        if (StateSharedData.KnockBackDirection == Vector2.zero)
            return;
        RandomRotation();
        SetExtraRotation();
        float smoothMultiplier = StateSharedData.KnockBackDuration * 5 / (timer + 0.1f);
        offsetDirectionLerp = Vector2.Lerp(Vector2.zero,offsetDirection,Mathf.PingPong(timerReclamp,1));
        rigibodyForceHandle.VelocityForceVector += (Vector3)(StateSharedData.KnockBackDirection + offsetDirectionLerp).normalized * smoothMultiplier;
    }
    #endregion

    #region Main Methods 
    private void RandomRotation()
    {
        StateSharedData.MovementDirection = (Vector2.Lerp(StateSharedData.KnockBackDirection, -StateSharedData.KnockBackDirection, Mathf.Sqrt(timerReclamp)) - offsetDirectionLerp).normalized;
    }
    /// <summary>
    /// Set the ExtraRotation uses for rotate on itself
    /// </summary>
    private void SetExtraRotation()
    {
        float angle = Mathf.Lerp(0, 360 * amountOfTurn, timerReclamp * 1.1f);
        angle %= 360;
        if (angle > 180)
            angle -= 360;
        StateSharedData.ExtraRotation = Vector3.forward * angle * direction;
    }
    #endregion

}
