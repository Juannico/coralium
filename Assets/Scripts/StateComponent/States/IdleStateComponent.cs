    using UnityEngine;

public class IdleStateComponent : MoveStateComponent
{
    public IdleData IdleData;
    private float distance;
    private Vector3 direction;
    private Vector3 targetPosition;
    private Vector3 lastTargetPosition;
    private Vector3 startForward;
    private float maxDistance;

    #region State Methods
    protected override void Awake()
    {      
        base.Awake();
        startFEELS = StateSharedData.FEELSdata.StartIdle;
        stopFEELS = StateSharedData.FEELSdata.StopIdle;
    }
    public override void Enter()
    {
        base.Enter();
#if UNITY_EDITOR
        TriggerDataUpdated(ref IdleData);
#endif
        StateSharedData.StartIdlingPosition = transform.position;
        lastTargetPosition = transform.position;
        targetPosition = GetTargetPosition();
        startForward = StateSharedData.BaseTransform.forward;
    }
    protected override void Update()
    {
        base.Update();
        if (StateSharedData.MovementVector.magnitude == 0)
            return;
        OnMove?.Invoke(this);
    }
    protected override void FixedUpdate()
    {
        if (rigibodyForceHandle.ExternalVelocityForceVector != Vector3.zero)
        {
            OnMove?.Invoke(this);
            return;
        }
        base.FixedUpdate();
        RandomMovement();
    }
    #endregion

    #region Main Methods
    private void RandomMovement()
    {
        distance = Vector3.Distance(targetPosition, transform.position);
        if (distance < Random.Range(0.12f, 0.2f))
            targetPosition = GetTargetPosition();
        float randomSpeed = Random.Range(IdleData.minRandomSpeed, IdleData.maxRandomSpeed);
        direction = (targetPosition - transform.position).normalized * randomSpeed * GetSmoothSpeed();
        if (startForward != StateSharedData.BaseTransform.forward && Vector3.Dot(direction.normalized, StateSharedData.BaseTransform.forward) < 0.5f)
            direction = StateSharedData.BaseTransform.forward * randomSpeed * GetSmoothSpeed() * 2;
        rigibodyForceHandle.VelocityForceVector += direction;
    }
    private float GetSmoothSpeed()
    {
        float smoothSpeed = (maxDistance - distance) / maxDistance;
        if (distance / maxDistance < 0.25f)
            smoothSpeed = 1 - smoothSpeed;
        return smoothSpeed + 0.25f;
    }
    private Vector3 GetTargetPosition()
    {
        Vector3 targetPosition = MathUtilities.RandomVector(-Vector3.one,Vector3.one) * Random.Range(IdleData.minRandomDistance, IdleData.maxRandomDistance) + StateSharedData.StartIdlingPosition;
        if (lastTargetPosition != StateSharedData.StartIdlingPosition)
            targetPosition = StateSharedData.StartIdlingPosition;
        targetPosition.z = StateSharedData.StartIdlingPosition.z;
        StateSharedData.IsBackingStarIdlePosition = targetPosition == StateSharedData.StartIdlingPosition;
        lastTargetPosition = targetPosition;
        maxDistance = Vector3.Distance(targetPosition, transform.position);
        return targetPosition;
    }
    #endregion
}
