using UnityEngine;

public class ChaseRunStateComponent : FollowStateComponent
{
    private Vector3 speedVelocity;
    private Vector3 lastDirection;
    private int sideDirection = 1;
    private float smoothSpeed;

    #region State Methods
    public override void Enter()
    {
        base.Enter();
        lastDirection = Vector3.zero;
        sideDirection = 1;
        if (!StateSharedData.IsChassing)
            sideDirection *= -1;
    }
    protected override void FixedUpdate()
    {
        base.FixedUpdate();
    }
    #endregion

    #region Override FollowState Methods 
    protected override Vector3 GetDirection()
    {
        if (objectToFollow == null)
            return Vector3.zero;
        Vector3 direction = (objectToFollow.transform.position - transform.position) * sideDirection;
        direction = direction.normalized * Speed;
        Vector3 smoothDirection = Vector3.SmoothDamp(lastDirection, direction, ref speedVelocity, smoothSpeed);
        lastDirection = smoothDirection;
        return smoothDirection;
    }
    #endregion

}
