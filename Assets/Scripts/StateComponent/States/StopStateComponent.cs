using UnityEngine;

public class StopStateComponent : StateComponent
{
    public StopData StopData;
    private Vector3 reduceForce;
    private Vector3 velocity = Vector3.zero;

    #region State Methods
    protected override void Awake()
    {
        base.Awake();
        startFEELS = StateSharedData.FEELSdata.StartStop;
        stopFEELS = StateSharedData.FEELSdata.StopStop;
    }
    public override void Enter()
    {
        StateSharedData.FEELSdata.currentStretchFEEL = null;
        base.Enter();
#if UNITY_EDITOR
        TriggerDataUpdated(ref StopData);
#endif
        reduceForce = rigibodyForceHandle.Rb.velocity;
    }
    protected override void Update()
    {
        base.Update();
        if (StateSharedData.MovementVector.magnitude == 0)
            return;
        OnMove?.Invoke(this);
    }
    protected override void FixedUpdate()
    {
        base.FixedUpdate();
        rigibodyForceHandle.VelocityForceVector = reduceForce;
        if (Vector3.zero == reduceForce)
        {
            OnStop?.Invoke(this);
            return;
        }
        reduceForce = Vector3.SmoothDamp(reduceForce, Vector3.zero, ref velocity, StopData.stopSpeed);
    }
    #endregion

}
