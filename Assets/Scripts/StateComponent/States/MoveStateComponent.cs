using UnityEngine;

public class MoveStateComponent : StateComponent
{
    [HideInInspector]public float Speed;
    [SerializeField]private float acelerationSpeed = 1;

    #region State Methods
    protected override void Awake()
    {       
        base.Awake();
        startFEELS = StateSharedData.FEELSdata.StartMove;
        stopFEELS = StateSharedData.FEELSdata.StopMove;
        acelerationSpeed = 0;   
    }
    protected override void FixedUpdate()
    {
        base.FixedUpdate();
        Moving();
        if (StateSharedData.MovementDirection.magnitude == 0)
        {
            acelerationSpeed = 0;
            OnStop?.Invoke(this);
        }
    }
    #endregion

    #region Main Methods
    private void Moving()
    {
        if (StateSharedData.MovementVector.magnitude == 0)
        {
            acelerationSpeed = 0;
            return;
        }
        acelerationSpeed = Mathf.Clamp01(acelerationSpeed + Time.fixedDeltaTime * 2f);
        Vector3 direction = StateSharedData.IsInOceanCurrent ? StateSharedData.MovementDirection : StateSharedData.BaseTransform.forward;
        direction.z = 0;
        rigibodyForceHandle.VelocityForceVector += direction * GetMovementSpeed() * StateSharedData.RotatingMultiplier * acelerationSpeed;
    }
    #endregion

    #region Reusable MoveState Methods 
    protected float GetMovementSpeed() => Speed * StateSharedData.MovementSpeedModifier;
    #endregion

}
