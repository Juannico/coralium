using System;

[Obsolete("Use ImpulseStateComponent instead. Waiting for change BT")]
public class SpecialImpulseStateComponent : ImpulseStateComponent
{
    public ImpulseData SpecialImpulseData;
    #region State Methods
    protected override void Awake()
    {
        base.Awake();
        startFEELS = StateSharedData.FEELSdata.StartLunge;
        stopFEELS = StateSharedData.FEELSdata.StopLunge;
    }
    public override void Enter()
    {
        turnDuration = SpecialImpulseData.TurnDuration;
        acceleration = SpecialImpulseData.AccelerationSpeed * 10;
        base.Enter();
    }
    #endregion

}
