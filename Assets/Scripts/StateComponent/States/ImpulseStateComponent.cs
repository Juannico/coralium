using UnityEngine;

public class ImpulseStateComponent : StateComponent
{
    public ImpulseData ImpulseData;
    private float impulseDistance;
    private float targetImpulseVelocity;
    private float impulseVelocity;
    private int amountOfTurn;
    private float rotateDirection;
    protected float turnDuration;
    private float timer = 0;
    protected float acceleration;
    private float startVelocity;
    private bool maxVelocityReached;

    #region State Methods
    protected override void Awake()
    {
        base.Awake();
        startFEELS = StateSharedData.FEELSdata.StartDash;
        stopFEELS = StateSharedData.FEELSdata.StopDash;
    }
    public override void Enter()
    {
        base.Enter();
#if UNITY_EDITOR
        TriggerDataUpdated(ref ImpulseData);
#endif
        Impulse();
        turnDuration = ImpulseData.TurnDuration;
        acceleration = ImpulseData.AccelerationSpeed * 10;
        SetStartValues();
        rotateDirection = Random.Range(0f, 1f) > 0.5f ? 1 : -1;
        rigibodyForceHandle.AffectByExternalForce = false;
        rigibodyForceHandle.VelocityForceVector = Vector3.zero;
        timer = 0;
        maxVelocityReached = false;
    }
    public override void Exit()
    {
        base.Exit();
        StateSharedData.SelfRotationMultiplier = 1f;
        rigibodyForceHandle.AffectByExternalForce = true;
        StateSharedData.StateAction = null;
    }
    protected override void FixedUpdate()
    {
        Impulse();
        SetExtraRotation();
        timer += Time.fixedDeltaTime;
        if (impulseVelocity < targetImpulseVelocity)
            impulseVelocity = Mathf.Clamp(startVelocity + Mathf.Pow(timer, 2) * acceleration, 0, targetImpulseVelocity);
        if (!maxVelocityReached && impulseVelocity == targetImpulseVelocity)
        {
            StateSharedData.StateAction?.Invoke();
            maxVelocityReached = true;
        }
        if (timer <= StateSharedData.ImpulseDuration)
            return;
        if (StateSharedData.ImpulseIsInfinity)
            return;
        OnStop?.Invoke(this);
    }
    #endregion

    #region Main Methods 
    private void Impulse() => rigibodyForceHandle.VelocityForceVector += StateSharedData.BaseTransform.forward * impulseVelocity;
    /// <summary>
    /// Set the ExtraRotation uses for rotate on itself
    /// </summary>
    private void SetExtraRotation()
    {
        float fixedTime = timer / StateSharedData.ImpulseDuration;
        fixedTime *= (impulseVelocity / targetImpulseVelocity);
        float angle = Mathf.LerpUnclamped(0, 360 * amountOfTurn, fixedTime);
        angle %= 360;
        if (angle > 180)
            angle -= 360;
        StateSharedData.ExtraRotation = Vector3.forward * angle * rotateDirection;
    }
    private void SetStartValues()
    {
        impulseDistance = StateSharedData.ImpulseDistance;
        impulseVelocity = 0;
        startVelocity = rigibodyForceHandle.Rb.velocity.magnitude;
        targetImpulseVelocity = impulseDistance / StateSharedData.ImpulseDuration;
        amountOfTurn = (int)Mathf.Round(StateSharedData.ImpulseDuration / turnDuration);
        if (amountOfTurn < 1)
            amountOfTurn = 1;
        StateSharedData.SelfRotationMultiplier = targetImpulseVelocity * amountOfTurn * 0.1f;
    }
    #endregion

}
