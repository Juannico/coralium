using NaughtyAttributes;
using UnityEngine;

/// <summary>
/// Data container class for sharing  information between  state component.
/// </summary>
public class StateSharedData : MonoBehaviour
{
    #region Movement 
    [ReadOnly] public bool HorizontalMove = false;
    [ReadOnly] public Vector3 MovementDirection;
    [ReadOnly] public Vector2 MovementVector = Vector2.one;
    [ReadOnly] public float MovementSpeedModifier = 1f;
    [ReadOnly] public float RotatingMultiplier = 1;
    #endregion

    #region Rotation 
    [ReadOnly] public Quaternion TargetRotation = Quaternion.identity;
    [ReadOnly] public Vector3 ExtraRotation = Vector3.zero;
    [ReadOnly] public Vector3 RotationAngle = Vector3.zero;
    [ReadOnly] public float SelfRotationMultiplier = 1;
    #endregion
    [HideInInspector] public Transform BaseTransform;

    [ReadOnly] public Vector3 StartIdlingPosition = Vector3.zero;
    [ReadOnly] public bool IsBackingStarIdlePosition = false;

    #region KnockBack
    [ReadOnly] public float KnockBackDuration = 0f;
    [ReadOnly] public Vector2 KnockBackDirection = Vector2.zero;
    [ReadOnly] public bool isCrashing = false;
    #endregion

    #region Impulse
    [ReadOnly] public float ImpulseDuration = 0f;
    [ReadOnly] public float ImpulseDistance = 0f;
    [ReadOnly] public bool ImpulseIsInfinity = false;
    [ReadOnly] public bool IsChassing = false;
    #endregion

    #region Follow
    [ReadOnly] public GameObject CurrentFollowObject = null;
    [ReadOnly] public float FollowForceOutsideTime = 0f;
    [ReadOnly] public Vector3 FollowForce = Vector3.zero;
    #endregion

    #region External force
    [ReadOnly] public bool IsInExternalControl = false;
    [ReadOnly] public bool IsInOceanCurrent = false;
    #endregion

    [ReadOnly] public FEELSdata FEELSdata;
    [HideInInspector] public GameObject CurrenBoostStrength;
    public System.Action StateAction;

    private void Awake()
    {
        if (BaseTransform == null)
            BaseTransform = transform;
    }

}
