using MoreMountains.Feedbacks;
using System;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Base class for state components in a state machine system.
/// Manages state-related events, feedbacks, and provides methods for entering and exiting states.
/// </summary>
public class StateComponent : MonoBehaviour
{
    public StateSharedData StateSharedData;
    protected RigibodyForceHandle rigibodyForceHandle;

    public delegate void OnActionState(StateComponent state);
    public delegate void OnAction();
    public OnActionState OnMove;
    public OnActionState OnStop;
    public OnAction OnEnd;
    public OnAction OnStart;
    protected MMFeedbacks startFEELS;
    protected MMFeedbacks stopFEELS;

    #region State Methods
    protected virtual void Awake()
    {
        StateSharedData = ComponentUtilities.SetComponent<StateSharedData>(gameObject);
        rigibodyForceHandle = GetComponent<RigibodyForceHandle>();
        if (rigibodyForceHandle == null)
            throw new System.Exception($"{gameObject.name} doesn't have RigibodyForceHandle component");
        OnMove += (state) => state.Exit();
        OnStop += (state) => state.Exit();
        if (enabled)
            enabled = false;
    }
    protected virtual void Update() { }
    protected virtual void FixedUpdate() { }

    public virtual void Enter()
    {
        enabled = true;
        OnStart?.Invoke();
        OnEnterFeedbacks();
    }
    public virtual void Exit()
    {
        OnEnd?.Invoke();
        OnExitFeedbacks();
        if (enabled)
            enabled = false;
    }
    #endregion

    #region Main Methods
    private void OnEnterFeedbacks()
    {
        StopCurrentFEELS();
        StateSharedData.FEELSdata.currentStretchFEEL = SetCurrentFEELS(startFEELS);
    }
    private void OnExitFeedbacks()
    {
        StopCurrentFEELS();
        StateSharedData.FEELSdata.currentStretchFEEL = SetCurrentFEELS(stopFEELS);
    }
    private MMFeedbacks SetCurrentFEELS(MMFeedbacks mMFeedbacks)
    {
        if (mMFeedbacks == null)
            return null;
        mMFeedbacks.PlayFeedbacks();
        return mMFeedbacks;
    }
    private void StopCurrentFEELS()
    {
        if (StateSharedData.FEELSdata.currentStretchFEEL != null)
            StateSharedData.FEELSdata.currentStretchFEEL.StopFeedbacks();
    }
    #endregion

    #region Update data in PlayMode 
#if UNITY_EDITOR

    public delegate void DataUpdatedEventHandler<T>(ref T data);
    private Dictionary<Type, Delegate> eventDelegates = new Dictionary<Type, Delegate>();
    /// <summary>
    ///Adds a listener delegate to the dictionary for the specified data type.
    /// </summary>
    /// <typeparam name="T">The type of data to listen for.</typeparam>
    /// <param name="listener">The delegate to be added as a listener.</param>
    public void AddEventListener<T>(DataUpdatedEventHandler<T> listener)
    {
        Type type = typeof(T);
        if (!eventDelegates.ContainsKey(type))
        {
            eventDelegates.Add(type, listener);
            return;
        }
        eventDelegates[type] = Delegate.Combine(eventDelegates[type], listener);
    }
    /// <summary>
    /// Removes a listener delegate from the dictionary for the specified data type.
    /// </summary>
    /// <typeparam name="T">The type of data to stop listening for.</typeparam>
    /// <param name="listener">The delegate to be removed as a listener.</param>
    public void RemoveEventListener<T>(DataUpdatedEventHandler<T> listener)
    {
        Type type = typeof(T);
        if (eventDelegates.ContainsKey(type))
            eventDelegates[type] = Delegate.Remove(eventDelegates[type], listener);
    }
    /// <summary>
    /// Triggers the data update event for the specified data type, invoking all registered listeners.
    /// </summary>
    /// <remarks>
    /// This method is supposed to be called in each Enter method of state components to update the data.
    /// Use #if UNITY_EDITOR to avoid errors in build.
    /// </remarks>
    /// <typeparam name="T">The type of data being updated.</typeparam>
    /// <param name="data">Reference to the updated data.</param>
    public void TriggerDataUpdated<T>(ref T data)
    {
        Type type = typeof(T);
        if (eventDelegates.ContainsKey(type))
            ((DataUpdatedEventHandler<T>)eventDelegates[type]).Invoke(ref data);
    }
#endif
    #endregion
}
