using System;
using System.Collections.Generic;
using UnityEngine;

public class StateComponentHandler : MonoBehaviour
{
    public Dictionary<Type, StateComponent> states;
    protected StateComponent currentState { get; private set; }
    public StateComponent nextState { get; private set; }
    public StateSharedData Data { get; set; }

    public void SwitchState(Type newState)
    {
        if (currentState?.GetType() == typeof(DieStateComponent))
            return;
        if (currentState?.GetType() == newState)
            return;
        nextState = states[newState];
        currentState?.Exit();
        currentState = nextState;
        currentState?.Enter();
        nextState = null;
    }
}
