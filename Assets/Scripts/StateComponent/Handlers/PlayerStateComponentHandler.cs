using System;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStateComponentHandler : StateComponentHandler
{
    private PlayerController player;
    private PlayerCharacter character;
    private Camera mainCamera;

    private Vector3 speedVelocity = Vector3.zero;

    private float scaredTimer;
    private float scaredTimeChange;

    public StateComponentRotate StateComponentRotate { get; private set; }
    #region Unity Methods
    private void Awake()
    {
        player = gameObject.GetComponent<PlayerController>();
        character = player.Character;
        SetStateSharedData();
        SetStates();
        StateComponentRotate = gameObject.AddComponent<StateComponentRotate>();
        StateComponentRotate.Initialize(character, Data);
        mainCamera = Camera.main;
        scaredTimer = 0;
        scaredTimeChange = UnityEngine.Random.Range(0.1f, 1f);
    }

    private void Update() => SetExternalForce();

    private void FixedUpdate() => SetMoveVector();
    #endregion
    #region Main Methods
    private void SetMoveVector()
    {
        character.MovementSoundUpdate(Data.MovementVector.normalized.magnitude);
        /*if (character.BaseCharacter.Data.IsRecievingDamage)
        {
            Data.MovementVector = Vector2.zero;
            return;
        }*/
        if (character.BaseCharacter.Data.IsScared)
            Scaring();
        else
        {
            Vector3 moveVector = player.MoveVector;
            if (character.BaseCharacter.Data.IsInExternalForce && moveVector.x < 0 && Data.IsInOceanCurrent)
                moveVector.x = 0;
            Data.MovementVector = moveVector;
        }
        if (Data.MovementVector.magnitude == 0 || base.Data.IsInExternalControl)
            return;
        base.Data.MovementDirection = Vector3.SmoothDamp(base.Data.MovementDirection, Data.MovementVector, ref speedVelocity, character.BaseCharacter.Data.SmoothSpeed);
    }
    private void Scaring()
    {
        scaredTimer += Time.deltaTime;
        if (scaredTimer < scaredTimeChange)
            return;
        scaredTimer = 0;
        scaredTimeChange = UnityEngine.Random.Range(0.12f, 0.5f);
        Data.MovementVector = MathUtilities.RandomVector2() * UnityEngine.Random.Range(1.75f, 3.2f);
        if (character.BaseCharacter.Data.IsInExternalForce)
            Data.MovementVector.Normalize();
    }
    private void SetExternalForce()
    {
        ImpulseStateComponent impulsingState = currentState as ImpulseStateComponent;
        character.BaseCharacter.Data.IsInmuneToExternalForce = impulsingState != null;
    }
    private void OnStop(StateComponent state)
    {
        if (character.BaseCharacter.Data.IsInExternalForce || character.RigibodyForceHandle.ExternalVelocityForceVector != Vector3.zero)
        {
            SwitchState(typeof(WaitStateComponent));
            return;
        }
        if (state as StopStateComponent)
        {
            SwitchState(typeof(IdleStateComponent));
            return;
        }
        SwitchState(typeof(StopStateComponent));
    }
    private void OnMove(StateComponent state)
    {
        if (!character.BaseCharacter.Data.CanMove)
            return;
        SwitchState(typeof(SwimStateComponent));
    }

    public void Stop()
    {
        base.Data.MovementDirection = character.RigibodyForceHandle.Rb.velocity;
        OnStop(currentState);
    }
    /// <summary>
    /// Initiates a impulse state, updating data to the KnockBackStateComponent.
    /// </summary>
    public void OnImpulseStarted(float impulseDuration, float impulseDistance, Action onMaxVelocityReached)
    {
        Data.ImpulseIsInfinity = true;
        Data.ImpulseDuration = impulseDuration;
        Data.ImpulseDistance = impulseDistance;
        Data.StateAction = onMaxVelocityReached;
        SwitchState(typeof(ImpulseStateComponent));
    }
    public void StopImpulse()
    {
        OnStop(currentState);
    }
    /// <summary>
    /// Initiates a knockback state, updating data to the KnockBackStateComponent.
    /// </summary>
    /// <param name="knockBackDirection">The normalized direction of the knockback force.</param>
    /// <param name="duration">The duration of the knockback.</param>
    /// <param name="isCrashing">Indicates whether the knockback action involves crashing (default is false).</param>
    public void OnKnokBack(Vector3 knockBackDirection, float duration, bool isCrashing = false)
    {
        Data.isCrashing = isCrashing;
        Data.KnockBackDirection = knockBackDirection.normalized;
        Data.KnockBackDuration = duration;
        SwitchState(typeof(KnockBackStateComponent));
    }
    private void SetStates()
    {
        IdleStateComponent idle = gameObject.AddComponent<IdleStateComponent>();
        idle.IdleData = character.MovementData.IdleData;
        GameObject idleTarget = new GameObject("IdelTarget");
        idleTarget.transform.parent = player.transform;
        idleTarget.transform.localPosition = Vector3.zero;
        idleTarget.transform.localEulerAngles = Vector3.zero;
        idle.OnStart += () => player.CameraController.SetCameraPlayerTarget(idleTarget.transform);
        idle.OnEnd += () => player.CameraController.SetCameraPlayerTarget(character.Model.transform.parent);

        SwimStateComponent swinm = gameObject.AddComponent<SwimStateComponent>();
        swinm.SwinData = character.MovementData.SwinData;

        StopStateComponent stop = gameObject.AddComponent<StopStateComponent>();
        stop.StopData = character.MovementData.StopData;

        WaitStateComponent wait = gameObject.AddComponent<WaitStateComponent>();

        KnockBackStateComponent knockback = gameObject.AddComponent<KnockBackStateComponent>();
        knockback.OnStart += () => character.BaseCharacter.Data.IsImmune = true;
        knockback.OnEnd += () => character.BaseCharacter.Data.IsImmune = false;

        ImpulseStateComponent impulse = gameObject.AddComponent<ImpulseStateComponent>();
        impulse.ImpulseData = character.MovementData.ImpulseData;
        impulse.OnStart += () => character.BaseCharacter.Data.IsImmune = true;
        impulse.OnEnd += () =>
        {
            character.BaseCharacter.Data.IsImmune = false;
            character.BaseCharacter.Data.EndStateAction?.Invoke();
        };

        DieStateComponent die = gameObject.AddComponent<DieStateComponent>();
        die.DieData = character.MovementData.DieData;

        states = new Dictionary<Type, StateComponent>()
        {
            { typeof(IdleStateComponent), idle},
            { typeof(SwimStateComponent), swinm},
            { typeof(WaitStateComponent), wait},
            { typeof(StopStateComponent), stop},
            { typeof(KnockBackStateComponent), knockback},
            { typeof(ImpulseStateComponent), impulse},
            { typeof(DieStateComponent), die},
        };
        ConfigureStates();

#if UNITY_EDITOR
        AddDataUpdateListeners(idle, character.MovementData.IdleData);
        AddDataUpdateListeners(stop, character.MovementData.StopData);
        AddDataUpdateListeners(swinm, character.MovementData.SwinData);
        AddDataUpdateListeners(die, character.MovementData.DieData);
        AddDataUpdateListeners(impulse, character.MovementData.ImpulseData);
#endif
    }
    private void ConfigureStates()
    {
        foreach (StateComponent state in states.Values)
        {
            if (state as MoveStateComponent)
            {
                MoveStateComponent moveState = state as MoveStateComponent;
                moveState.Speed = character.BaseCharacter.Data.Speed;
            }
            if (state as ImpulseStateComponent)
            {
                float targetFOV = player.CameraData.DashFOV;
                float FOVTransitionDuration = player.CameraData.DashFOVTransitionDuration;
                state.OnStart += () =>
                {
                    if (Vector3.Dot(player.BaseTransform.forward, mainCamera.transform.forward) > 0.5)
                        player.CameraController.ChangeFOV(targetFOV, FOVTransitionDuration);
                };
                state.OnEnd += () => player.CameraController.RestoreFOV(FOVTransitionDuration * player.CameraData.RestoreFOVDurationMultiplier);
            }
            state.OnMove = (componentState) => OnMove(componentState);
            state.OnStop = (componentState) => OnStop(componentState);
        }
    }
#if UNITY_EDITOR
    private void AddDataUpdateListeners<T>(StateComponent state, T data) => state.AddEventListener((ref T stateData) => stateData = data);
#endif
    private void SetStateSharedData()
    {
        Data = ComponentUtilities.SetComponent<StateSharedData>(player.gameObject);
        Data.FEELSdata = character.FEELSData;
        Data.BaseTransform = player.BaseTransform.transform;
    }
    #endregion 
}
