
public class PlayerAnimationController : BaseAnimationController
{
    private string upFinClipAtributeName = "UpFinSpeed";
    private int upFinLayerIndex = 6;


    private string backFinClipAtributeName = "BackFinSpeed";
    private int backFinLayerIndex = 1;
    private string downLeftFinClipAtributeName = "DownLeftFinSpeed";
    private int downLeftFinLayerIndex = 2;
    private string downRightFinClipAtributeName = "DownRightFinSpeed";
    private int downRightFinLayerIndex = 3;
    private string leftFinClipAtributeName = "LeftFinSpeed";
    private int leftFinLayerIndex = 4;
    private string rightFinClipAtributeName = "RightFinSpeed";
    private int rightFinLayerIndex = 5;
    public override void SetBaseAnimationValues(float layerWeight, float animationClipSpeed, float sideValue)
    {
        float backFinSpeedMulplier = 1;
        float backFinLayetWeigth = backFinRotatingLayerWeight;
        SettingAnimationValues(upFinLayerIndex, layerWeight * 0.9f, upFinClipAtributeName, animationClipSpeed * 0.9f);
        SettingAnimationValues(backFinLayerIndex, backFinLayetWeigth, backFinClipAtributeName, animationClipSpeed * backFinSpeedMulplier);
        base.SetBaseAnimationValues(layerWeight, animationClipSpeed, sideValue);
        SettingAnimationValues(downRightFinLayerIndex, currentRightLayerMaskWeight, downRightFinClipAtributeName, currentRightClipSpeed);
        SettingAnimationValues(downLeftFinLayerIndex, currentLeftLayerMaskWeight, downLeftFinClipAtributeName, currentLeftClipSpeed);
        SettingAnimationValues(rightFinLayerIndex, currentRightLayerMaskWeight, rightFinClipAtributeName, currentRightClipSpeed);
        SettingAnimationValues(leftFinLayerIndex, currentLeftLayerMaskWeight, leftFinClipAtributeName, currentLeftClipSpeed);
    }
}
