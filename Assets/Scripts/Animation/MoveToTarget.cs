using NaughtyAttributes;
using UnityEngine;

public class MoveToTarget : MonoBehaviour
{
    [SerializeField] private Transform target;
    [SerializeField] private float velocity;
    [SerializeField] private float offset = 10;
    [SerializeField] private bool moveOnAwake;
    [SerializeField] private bool changeForward;

#if UNITY_EDITOR
    [SerializeField] private bool showDebugGuide;
    [ShowIf("showDebugGuide")] [SerializeField] [Range(0, 1)] private float path;
#endif
    private Vector3 startPosition;
    private Vector3 pivotVector;
    private float interpolateAmount = 0;
    private void Awake()
    {
        enabled = moveOnAwake;
        if (moveOnAwake)
            StartToMove();
    }
    private void Update()
    {
        if (target == null)
            return;
        interpolateAmount = Mathf.Clamp01(interpolateAmount + Time.deltaTime * velocity * 0.1f);
        Vector3 middlePoint = MathUtilities.GetOppositeVectorInPlane(pivotVector, startPosition, target.position, 0.5f, 0.5f);
        Vector3 nextPosition = MathUtilities.QuadraticLerp(startPosition, middlePoint, target.position, interpolateAmount);
        if (changeForward)
            transform.forward = (nextPosition.normalized - transform.position.normalized).normalized;
        transform.position = nextPosition;
        if (interpolateAmount == 1)
            enabled = false;
    }
    public void StartToMove(bool changeForward = true)
    {
        if (target == null)
            return;
        startPosition = transform.position;
        pivotVector = (target.position - transform.position) * 0.5f;
        pivotVector = pivotVector + startPosition + Vector3.up * -offset * 10;
        interpolateAmount = 0;
        this.changeForward = changeForward;
        enabled = true;
    }
    public void SetPosition(float pathValue)
    {
        Vector3 middlePoint = MathUtilities.GetOppositeVectorInPlane(pivotVector, startPosition, target.position, 0.5f, 0.5f);
        Vector3 nextPosition = MathUtilities.QuadraticLerp(startPosition, middlePoint, target.position, pathValue);
        transform.forward = (nextPosition.normalized - transform.position.normalized).normalized;
        transform.position = nextPosition;
    }
    public void SetTarget(Transform target) => this.target = target;

#if UNITY_EDITOR
    private bool lastDebugState;
    private void OnDrawGizmosSelected()
    {
        if (!showDebugGuide)
        {
            if (lastDebugState)
                transform.position = startPosition;
            lastDebugState = false;
            startPosition = transform.position;
            return;
        }
        lastDebugState = true;
        Vector3 pivotVector = (target.position - transform.position) * 0.5f;
        pivotVector = pivotVector + startPosition + Vector3.up * -offset * 10;
        Vector3 middlePoint = MathUtilities.GetOppositeVectorInPlane(pivotVector, startPosition, target.position, 0.5f, 0.5f);
        Gizmos.color = Color.red;
        Gizmos.DrawRay(middlePoint, Vector3.up * 10);
        Gizmos.DrawRay(middlePoint, Vector3.down * 10);
        Gizmos.DrawRay(middlePoint, Vector3.right * 10);
        Gizmos.DrawRay(middlePoint, Vector3.left * 10);
        Gizmos.DrawRay(middlePoint, Vector3.forward * 10);
        Gizmos.DrawRay(middlePoint, Vector3.back * 10);
        Vector3 pathPosition = MathUtilities.QuadraticLerp(startPosition, middlePoint, target.position, path);
        Gizmos.DrawSphere(pathPosition, 10);
    }
#endif
}
