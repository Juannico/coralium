using UnityEngine;
/// <summary>
/// Contains base methods to set layer weigth and speed 
/// </summary>
public class BaseAnimationController : MonoBehaviour
{
    protected Animator animator;
    protected float currentspeed = 1;
    protected float speedSmoothDamp;
    protected float currentLayerMaskWeight = 1;
    protected float layerMaskWeightSmoothDamp;
    [SerializeField] private float minClipSpeed = 0.75f;
    [SerializeField] private float maxClipSpeed = 1.85f;
    [SerializeField] private float inIdleClipSpeedMultiplier = 0.82f;
    [SerializeField] private float inIdleLayerWeightMultiplier = 0.8f;
    [SerializeField] private float startLayerWeight = 1f;
    [SerializeField] protected float increaseClipSpeedMultiplier = 1.2f;
    [SerializeField] protected float decreaseLayerWeightMultiplier = 0.85f;
    [SerializeField] protected float backFinRotatingLayerWeight = 0.5f;
    protected float currentLeftClipSpeed = 1;
    protected float leftClipSpeedSmoothDamp;
    protected float currentRightClipSpeed = 1;
    protected float rightClipSpeedSmoothDamp;
    protected float currentLeftLayerMaskWeight = 1;
    protected float leftLayerMaskWeightSmoothDamp;
    protected float currentRightLayerMaskWeight = 1;
    protected float rightLayerMaskWeightSmoothDamp;
    private void Awake()
    {
        animator = GetComponent<Animator>();
    }
    public void Initialize() => animator = GetComponent<Animator>();
    /// <summary>
    /// Sets the layer weight and layer speed of the clips based on the specified parameters.
    /// </summary>
    /// <param name="isIdelingState">Indicates if the character is in an idle state.</param>
    /// <param name="sideDirection">If greater than 0, sets the values for the right side; otherwise, sets for the left side.</param>
    /// <param name="playerSpeed">The speed of the clips based on the player's movement speed.</param>
    public void SetAnimationState(bool isIdelingState, float sideDirection, float playerSpeed)
    {
        float speed = Mathf.Clamp(playerSpeed, minClipSpeed, maxClipSpeed);

        float layerWeight = startLayerWeight;
        if (isIdelingState)
        {
            speed *= inIdleClipSpeedMultiplier;
            layerWeight *= inIdleLayerWeightMultiplier;
        }
        currentspeed = Mathf.SmoothDamp(currentspeed, speed, ref speedSmoothDamp, 0.1f);
        if (currentspeed < 0)
            currentLayerMaskWeight = Mathf.SmoothDamp(currentLayerMaskWeight, layerWeight, ref layerMaskWeightSmoothDamp, 0.1f);
        currentLayerMaskWeight = Mathf.Clamp01(currentLayerMaskWeight);
        SetBaseAnimationValues(currentLayerMaskWeight, currentspeed, sideDirection);
    }
    /// <summary>
    /// Sets the layer weight and layer speed of the clips.
    /// </summary>
    /// <param name="layerWeight">The target weight of the layer.</param>
    /// <param name="animationClipSpeed">The target speed of the layer.</param>
    /// <param name="sideValue">If greater than 0, sets the values for the right side; otherwise, sets for the left side.</param>
    public virtual void SetBaseAnimationValues(float layerWeight, float animationClipSpeed, float sideValue)
    {
        float rightClipSpeedMultiplier = sideValue >= 0 ? 1 : increaseClipSpeedMultiplier;
        float leftClipSpeedMultiplier = sideValue <= 0 ? 1 : increaseClipSpeedMultiplier;
        float rightLayerWeightMultiplier = sideValue <= 0 ? 1 : decreaseLayerWeightMultiplier;
        float leftLayerWeightMultiplier = sideValue >= 0 ? 1 : decreaseLayerWeightMultiplier;
        GetClipsSpeed(animationClipSpeed, rightClipSpeedMultiplier, leftClipSpeedMultiplier);
        GetLayersWeight(layerWeight, rightLayerWeightMultiplier, leftLayerWeightMultiplier);
    }
    protected void SettingAnimationValues(int layerIndex, float layerWeight, string animationClipSpeedAtributeName, float animationClipSpeed)
    {
        if (!float.IsNaN(layerWeight))
            animator.SetLayerWeight(layerIndex, layerWeight);
        if (!float.IsNaN(animationClipSpeed))
            animator.SetFloat(animationClipSpeedAtributeName, animationClipSpeed);
    }
    protected void GetLayersWeight(float layerWeight, float rightLayerWeightMultiplier, float leftLayerWeightMultiplier)
    {
        currentRightLayerMaskWeight = Mathf.SmoothDamp(currentRightLayerMaskWeight, layerWeight * rightLayerWeightMultiplier, ref rightLayerMaskWeightSmoothDamp, 0.25f);
        currentLeftLayerMaskWeight = Mathf.SmoothDamp(currentLeftLayerMaskWeight, layerWeight * leftLayerWeightMultiplier, ref leftLayerMaskWeightSmoothDamp, 0.25f);
    }
    protected void GetClipsSpeed(float animationClipSpeed, float rightClipSpeedMultiplier, float leftClipSpeedMultiplier)
    {
        currentRightClipSpeed = Mathf.SmoothDamp(currentRightClipSpeed, animationClipSpeed * rightClipSpeedMultiplier, ref rightClipSpeedSmoothDamp, 0.25f);
        currentLeftClipSpeed = Mathf.SmoothDamp(currentLeftClipSpeed, animationClipSpeed * leftClipSpeedMultiplier, ref leftClipSpeedSmoothDamp, 0.25f);
    }
}
