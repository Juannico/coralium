
public class LionFishAnimationController : BaseAnimationController
{
    private int backFinLayerIndex = 1;
    private string backFinClipAtributeName = "BackFinSpeed";
    private int leftFinLayerIndex = 2;
    private string leftFinClipAtributeName = "LeftFinSpeed";
    private int rightFinLayerIndex = 3;
    private string rightFinClipAtributeName = "RightFinSpeed";
    public override void SetBaseAnimationValues(float layerWeight, float animationClipSpeed, float sideValue)
    {
        float backFinSpeedMulplier = 1;
        float backFinLayetWeigth = backFinRotatingLayerWeight;
        SettingAnimationValues(backFinLayerIndex, backFinLayetWeigth, backFinClipAtributeName, animationClipSpeed * 0.9f * backFinSpeedMulplier);
        base.SetBaseAnimationValues(layerWeight, animationClipSpeed, sideValue);
        SettingAnimationValues(rightFinLayerIndex, currentRightLayerMaskWeight, rightFinClipAtributeName, currentRightClipSpeed);
        SettingAnimationValues(leftFinLayerIndex, currentLeftLayerMaskWeight, leftFinClipAtributeName, currentLeftClipSpeed);
    }

}
