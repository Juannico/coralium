using NaughtyAttributes;
using UnityEngine;
[CreateAssetMenu(fileName = "Inventory", menuName = "Coralium/Inventory")]
public class Inventory : ScriptableObject
{

    [Expandable] public CollectableData[] Collectables;

}
