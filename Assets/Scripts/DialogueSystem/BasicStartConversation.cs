using Cinemachine;
using NaughtyAttributes;
using PixelCrushers;
using PixelCrushers.DialogueSystem;
using UnityEngine;
using UnityEngine.Events;

public class BasicStartConversation : MonoBehaviour
{
    [ConversationPopup(true)] [SerializeField] private string conversation;
    [SerializeField] private bool stopPlayerActions = true;
    [SerializeField] private bool stopMoveCamera = false;
    [SerializeField] private bool lookConversant = true;
    [SerializeField] private UnityEvent OnConversationStarted;
    [SerializeField] private bool delayEnd;
    [ShowIf("delayEnd")] [SerializeField] private float delayTime = 1;
    [SerializeField] private UnityEvent OnConversationEnded;
    [field: SerializeField] public Transform Conversant { get; private set; }
    private Transform playerTransform;
    public TransformDelegate OnTransformConversationEnded;
    private CinemachineBrain cinemachineBrain;
    private PlayerController playerContoller;
    private void Awake()
    {
        if (cinemachineBrain == null)
            cinemachineBrain = Camera.main.GetComponent<CinemachineBrain>();
        if (stopPlayerActions)
            playerContoller = GameObject.FindGameObjectWithTag(Tags.Player).GetComponentInChildren<PlayerController>();
        playerTransform = playerContoller.BaseTransform;
    }
    public void StartConversation() => StartConversation((transform) => { });
    public void StartConversation(TransformDelegate onConversationEnded)
    {
        if (cinemachineBrain == null)
            cinemachineBrain = Camera.main.GetComponent<CinemachineBrain>();
        DialogueManager.instance.StartConversation(conversation, Conversant, playerTransform);
        InputDeviceManager.RegisterInputAction("Submit", PlayerInputManager.Instance.InputActions.UI.Submit);
        OnConversationStarted?.Invoke();
        GameManager gameManager = GameManager.Instance;
        // #TODO Hide HUD
        //gameManager.UIManager.SetUiVisibility(false);
        if (lookConversant)
            playerContoller.StateHandler.StateComponentRotate.LookAt(Conversant);
        OnTransformConversationEnded = onConversationEnded;
        OnTransformConversationEnded += (transform) =>
        {
            if (delayEnd)
                CoroutineHandler.ExecuteActionAfter(() => OnConversationEnded?.Invoke(),delayTime,this);
            else
                OnConversationEnded?.Invoke();
            if (lookConversant)
                playerContoller.StateHandler.StateComponentRotate.StopLookAt();
        };
        if (stopMoveCamera)
        {
            if (cinemachineBrain == null)
                cinemachineBrain = Camera.main.GetComponent<CinemachineBrain>();
            cinemachineBrain.enabled = false;
        }
        if (stopPlayerActions)
        {
            PlayerInputManager.Instance.SetInputModeUI();
            OnTransformConversationEnded += (transform) =>
            {
                // #TODO Show HUD
                //gameManager.UIManager.SetUiVisibility(true);
                PlayerInputManager.Instance.SetInputModePlayer();
                InputDeviceManager.UnregisterInputAction("Submit");

            };
            if (playerContoller != null)
            {
                playerContoller.StateHandler.Data.MovementVector = Vector2.zero;
                playerContoller.StateHandler.SwitchState(typeof(WaitStateComponent));
                playerContoller.Character.RigibodyForceHandle.StopMove();
            }
        }
        DialogueManager.instance.conversationEnded += OnConversationEndedInternal;
    }

    private void OnConversationEndedInternal(Transform actor)
    {
        if (stopMoveCamera)
            cinemachineBrain.enabled = true;
        OnTransformConversationEnded?.Invoke(actor);
        DialogueManager.instance.conversationEnded -= OnConversationEndedInternal;
    }
}
