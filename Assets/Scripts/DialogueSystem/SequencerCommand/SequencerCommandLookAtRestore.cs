// Custom SequencerCommand Create from Pixel Crushers.

using UnityEngine;

namespace PixelCrushers.DialogueSystem.SequencerCommands
{
    [AddComponentMenu("")] // Hide from menu.
    public class SequencerCommandLookAtRestore : SequencerCommand
    {

        private const float SmoothMoveCutoff = 0.05f;
        private Transform subject;
        private float duration;
        float startTime;
        float endTime;
        Quaternion originalRotation;
        Quaternion targetRotation;


        public void Start()
        {
            subject = GetSubject(0, sequencer.speaker);
            duration = GetParameterAsFloat(1, 0);
            bool yAxisOnly = !string.Equals(GetParameter(2), "allAxes", System.StringComparison.OrdinalIgnoreCase);

            if (DialogueDebug.logInfo) Debug.Log(string.Format(System.Globalization.CultureInfo.InvariantCulture, "{0}: Sequencer: LookAtRestore({1}, {2})", new System.Object[] { DialogueDebug.Prefix, subject, duration }));
            if ((subject == null) && DialogueDebug.logWarnings) Debug.LogWarning(string.Format("{0}: Sequencer: LookAt Restore Subject '{1}' wasn't found.", new System.Object[] { DialogueDebug.Prefix, GetParameter(1) }));

            // Set up the rotation:
            if ((subject != null))
            {
                targetRotation = subject.transform.parent.rotation;
                if (yAxisOnly) targetRotation = Quaternion.Euler(subject.rotation.eulerAngles.x, targetRotation.eulerAngles.y, subject.rotation.eulerAngles.z);
                if (duration > SmoothMoveCutoff)
                {
                    startTime = DialogueTime.time;
                    endTime = startTime + duration;
                    originalRotation = subject.rotation;
                }
                else
                {
                    Stop();
                }
            }
            else
            {
                Stop();
            }
        }

        public void Update()
        {
            // Keep smoothing for the specified duration:
            if (DialogueTime.time < endTime)
            {
                float elapsed = (DialogueTime.time - startTime) / duration;
                subject.rotation = Quaternion.Lerp(originalRotation, targetRotation, elapsed);
            }
            else
                Stop();
        }

        public void OnDestroy()
        {
            // Final rotation:
            if (subject != null)
            {
                subject.rotation = targetRotation;
            }

        }
    }
}
