using PixelCrushers;
using System;
using UnityEngine;

[Serializable]
public class TextTableFieldName
{
    public string FieldName;
    public TextTable TextTable;
    public Color TextColor = Color.white; 
}
