using UnityEngine;
using UnityEngine.UI;

public class DebugPlayerStats : MonoBehaviour
{
    private BaseCharacter playerCharacter;
    public BaseCharacter PlayerCharacter
    {
        get 
        {
            if (playerCharacter == null)
                playerCharacter = GameObject.FindGameObjectWithTag(Tags.Player)?.GetComponent<PlayerCharacter>()?.BaseCharacter;
            return playerCharacter; 
        }
        set =>       playerCharacter = value;
    }

    public void SetInvesibility(bool state)
    {
        if (!state)
            PlayerCharacter.Data.ControlOnInmune = false;
        PlayerCharacter.Data.IsImmune = state;
        if (state)
            PlayerCharacter.Data.ControlOnInmune = true;
    }
    public void SetInfiniteEnergy(bool state)
    {
        PlayerCharacter.Data.CurrentEnergy = Mathf.Infinity;
        PlayerCharacter.Data.IsInfinteEnergy = state;
    }
    public void GetInvisibility(Toggle toggle) => toggle.isOn = PlayerCharacter.Data.IsImmune;
    public void GetInfiniteEnergy(Toggle toggle) => toggle.isOn = PlayerCharacter.Data.IsInfinteEnergy;


}
