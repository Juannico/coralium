using UnityEngine;
using UnityEngine.UI;

public class DebugDropCheckboxMenu : DebugDropMenu
{
    [SerializeField] private Toggle toggle;
    protected override void Awake()
    {
        base.Awake();
        toggle.gameObject.SetActive(false);
    }

    public override void HideInformation() => toggle.gameObject.SetActive(false);
    public override void ShowInformation()
    {
        toggle.gameObject.SetActive(true);
        functions[this.index].GetBool?.Invoke(toggle);
    }
    public void OnCheckBoxToggled(bool state)
    {
        if (index < 0 || index >= functions.Length)
            return;
        functions[index].SetBool?.Invoke(state);
    }
}
