using UnityEngine;
using UnityEngine.UI;

public class DebugDropButtonMenu : DebugDropMenu
{
    [SerializeField] private Button button;
    private delegate void OnAction();
    private OnAction action;
    protected override void Awake()
    {
        base.Awake();
        button.gameObject.SetActive(false);
        button.onClick.AddListener(() => action?.Invoke());
    }
    public override void HideInformation() => button.gameObject.SetActive(false);
    public override void ShowInformation()
    {
        button.gameObject.SetActive(true);
        action = () => functions[this.index].Action?.Invoke();
    }
}
