using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using static UnityEngine.UI.Dropdown;

public class DebugDropMenu : MonoBehaviour
{
    [SerializeField] protected DebuggerFunction[] functions;
    [SerializeField] private string tittle;
    private Dropdown dropdown;
    protected int index = 0;
    public delegate void OnSwitchIndex(int intValue);
    public OnSwitchIndex OnSwitchDropIndexAction;
    protected virtual void Awake()
    {
        dropdown = GetComponent<Dropdown>();

        if (functions.Length > 0)
            SetDropDownOptions();
    }
    private void SetDropDownOptions()
    {
        if (dropdown == null)
            dropdown = GetComponent<Dropdown>();
        dropdown.ClearOptions();
        OptionData[] options = new OptionData[functions.Length + 1];
        options[0] = new OptionData(tittle);
        for (var i = 1; i < options.Length; i++)
            options[i] = new OptionData(functions[i - 1].Name);
        dropdown.AddOptions(options.ToList());
    }

    public  void OnSwitchDropIndex(int index)
    {
        if (index == 0)
        {
            HideInformation();
            return;
        }
        this.index = index - 1;
        ShowInformation();
        OnSwitchDropIndexAction?.Invoke(this.index);
    }
    public virtual void HideInformation() { }
    public virtual void ShowInformation() { }

    public void SetFunctions(DebuggerFunction[] functions)
    {
        this.functions = functions;
        SetDropDownOptions();
    }
}
