using NaughtyAttributes;
using UnityEngine;

public class DebugChangeLeves : MonoBehaviour
{
    [SerializeField] private DebugDropButtonMenu debugDropCheckboxMenu;
    [SerializeField] [Scene] private string defaultScene;
    [SerializeField] private LevelManager levelManager;

    private DebuggerFunction[] functions;
    private LoadSceneUtility loadSceneUtility;
    private PlayerCharacter playerCharacter;
    private void Awake()
    {
        loadSceneUtility = new LoadSceneUtility();
        functions = new DebuggerFunction[levelManager.Levels.Length];
        for (var i = 0; i < functions.Length; i++)
        {
            functions[i] = new DebuggerFunction();
            LevelData levelData = levelManager.Levels[i];
            if (levelData.LevelSceneName == defaultScene)
            {
                functions[i].Name = $"NOT level 0{i + 1} ";
                continue;
            }
            functions[i].Name = levelData.LevelSceneName;
            functions[i].Action.AddListener(() => SwitchLevel( levelData.LevelSceneName, levelManager.CurrentLevelData.LevelSceneName));
        }
        debugDropCheckboxMenu.SetFunctions(functions);
        debugDropCheckboxMenu.OnSwitchDropIndexAction = (int index ) =>
        {
            string levelName = levelManager.Levels[index].LevelSceneName;
            if (levelName == levelManager.CurrentLevelData.LevelSceneName || levelName == defaultScene)
                debugDropCheckboxMenu.HideInformation();
        };
    }
    private void Start()
    {
        playerCharacter = GameObject.FindGameObjectWithTag(Tags.Player).GetComponent<PlayerCharacter>();
    }
    private void SwitchLevel(string levelToLoad, string levelToUnload)
    {
        if (levelToLoad == levelToUnload)
            return;
        playerCharacter.GetComponent<PlayerController>().StateHandler.SwitchState(typeof(WaitStateComponent));
        if (loadSceneUtility == null)
            loadSceneUtility = new LoadSceneUtility();
        levelManager.TravelingWithDebug = true;
        levelManager.CurrentLevelData.UnloadLevel();
        levelManager.Save();
        Destroy(LevelSceneManager.Instance.gameObject);
        LoadingScreenManager.LoadSceneByName(levelToLoad, UnityEngine.SceneManagement.LoadSceneMode.Single, true);
    }
}
