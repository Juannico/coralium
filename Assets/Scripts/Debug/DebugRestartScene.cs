using UnityEngine;

public class DebugRestartScene : MonoBehaviour
{
    
    private PlayerInputActions inputActions;
    private void Awake()
    {
        inputActions = new PlayerInputActions();
    }

    private void OnEnable()
    {
        inputActions.Enable();
        inputActions.Debug.RestartScene.performed +=  (context) =>
        {
            PlayerCharacter player = GameObject.FindGameObjectWithTag(Tags.Player).GetComponent<PlayerCharacter>();
            player.BaseCharacter.Data.ResetDelegates();
            FindObjectOfType<RestartLevel>().Restart();
        };
    }
    private void OnDisable()
    {
        inputActions.Disable();
        inputActions.Debug.RestartScene.performed -= (context) =>
        {
            PlayerCharacter player = GameObject.FindGameObjectWithTag(Tags.Player).GetComponent<PlayerCharacter>();
            player.BaseCharacter.Data.ResetDelegates();
            FindObjectOfType<RestartLevel>().Restart();
        };
    }

}

