using UnityEngine;
using UnityEngine.UI;

public class DebugPlayerAbilities : MonoBehaviour
{
    [SerializeField] private DebugDropCheckboxMenu debugDropCheckboxMenu;
    [SerializeField] private PlayerAbilities playerAbilities;
    private DebuggerFunction[] functions;
    private BaseAbilityCast[] abilities;
    private void Awake()
    {
        abilities = playerAbilities.BaseAbilities;
        functions = new DebuggerFunction[abilities.Length];
        for (var i = 0; i < functions.Length; i++)
        {
            functions[i] = new DebuggerFunction();
            BaseAbilityCast ability = abilities[i];
            functions[i].Name = ability.gameObject.name;
            functions[i].SetBool.AddListener((bool state) => GetPlayerAbility(ability).IsLocked = !state);
            functions[i].GetBool.AddListener((Toggle toggle) => toggle.isOn = !GetPlayerAbility(ability).IsLocked);
        }
        debugDropCheckboxMenu.SetFunctions(functions);
    }
    private BaseAbilityCast GetPlayerAbility(BaseAbilityCast ability)
    {
        for (var i = 0; i < playerAbilities.BaseAbilities.Length; i++)
        {
            if (playerAbilities.BaseAbilities[i].gameObject == ability.gameObject)
                return playerAbilities.Abilities[i];
        }
        return null;
    }
}
