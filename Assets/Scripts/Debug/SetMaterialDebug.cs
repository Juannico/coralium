using NaughtyAttributes;
using UnityEngine;

public class SetMaterialDebug : MonoBehaviour
{
    [SerializeField] private Material materialToTest;
    [SerializeField] private SetMaterialProvider setMaterialProvider;
    [OnValueChanged("OnValueChangedCallback")]
    public bool changeMat;

    private void OnValueChangedCallback()
    {
        setMaterialProvider = ComponentUtilities.SetComponent<SetMaterialProvider>(gameObject);
        if (changeMat)
        {
            setMaterialProvider.SetMaterial(materialToTest, (material) => material.SetFloat("_Alpha", 1));
            return;
        }
        setMaterialProvider.RemoveMaterial(materialToTest);
    }
}
