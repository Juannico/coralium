using NaughtyAttributes;
using UnityEngine;

public class SetUpSceneDebug : MonoBehaviour
{
    [SerializeField] private LevelManager levelManager;
    [SerializeField] private ZoneManager zoneManager;
    [SerializeField] public Transform playerStartTransform;
    [SerializeField] private bool loadZonesByDefault;
    [ShowIf("loadZonesByDefault")] [SerializeField] private Zone[] zonesToLoadByDefault;
    public void Setup()
    {

        if (!loadZonesByDefault)
        {
            SetPlayerPosition setPlayerPosition = new SetPlayerPosition();
            setPlayerPosition.SetPosition(playerStartTransform.position, playerStartTransform.forward, levelManager);
            return;
        } 
        StartCoroutine(CoroutineHandler.ExecuteActionAfterUpdateUntil(() =>
        {
            return zoneManager.IsLoadComplete;
        }, () =>
        {
            for (int i = 0; i < zonesToLoadByDefault.Length; i++)
                zonesToLoadByDefault[i].Load();
            SetPlayerPosition setPlayerPosition = new SetPlayerPosition();
            setPlayerPosition.SetPosition(playerStartTransform.position, playerStartTransform.forward, levelManager);
        }));
    }
    private void OnDrawGizmos()
    {
        if (playerStartTransform == null)
            return;
        Gizmos.DrawRay(playerStartTransform.position, playerStartTransform.forward * 10);
    }
}
