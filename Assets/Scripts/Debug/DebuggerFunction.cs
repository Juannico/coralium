
using System;
using UnityEngine.Events;
using UnityEngine.UI;

[Serializable]
public class DebuggerFunction 
{
    public string Name;
    public UnityEvent Action = new UnityEvent();
    public UnityEvent<bool> SetBool = new UnityEvent<bool>();
    public UnityEvent<Toggle> GetBool = new UnityEvent<Toggle>();

}
