using System.Collections;
using System.Collections.Generic;
using TamarilloTools;
using UnityEngine;

public class EndDemo : MonoBehaviour
{
    [SerializeField] private GameData gameData;
    [SerializeField] private LevelManager levelManager;
    [SerializeField] private GameObject message;

    public void ShowEnd()
    {
        CoroutineHandler.ExecuteActionAfter(() => message.SetActive(true), 1f * 1.2f, this);
        FadeUI.FadeOut();
    }

    public void End()
    {
        Destroy(GameManager.Instance.gameObject);
        if (LevelSceneManager.Instance != null)
            Destroy(LevelSceneManager.Instance.gameObject);
        DataManager.Reset();
        levelManager.LevelInitialized = false;
        LoadingScreenManager.LoadSceneByName("MainMenu");
    }
}
