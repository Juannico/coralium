using Cinemachine;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class DebugPlayer : MonoBehaviour
{
     public int starIndexPlayer;
    private CameraController cameraController;
    [SerializeField] private Scrollbar playerSpeedScrollBar;
    [SerializeField] private Scrollbar cameraZoomScrollBar;
    [SerializeField] private Scrollbar cameraXSenisibilityScrollBar;
    [SerializeField] private Scrollbar cameraYSenisibilityScrollBar;
    [SerializeField] private Text playerSpeedTextInfo;
    [SerializeField] private Text cameraZoomTextInfo;
    [SerializeField] private Text cameraXSenisibilityTextInfo;
    [SerializeField] private Text cameraYSenisibilityTextInfo;

    public PlayerController CurrentPlayerController;

    private float startValuePlayerSpeed ;
    private float startValueCameraZoom ;
    private float startValueCameraXSensibility ;
    private float startValueCameraYSensibility ;

    public delegate void ChangePlayer(PlayerController player);
    public ChangePlayer changePlayer;
    private PlayerMovementData currentMovementData;
    private void Awake()
    {
        CurrentPlayerController = GameObject.FindGameObjectWithTag(Tags.Player).GetComponent<PlayerController>();
        cameraController = GameObject.FindGameObjectWithTag(Tags.CameraController).GetComponent<CameraController>();
        //Debug.Log(playerProgressHandle);
       
        RemoveScrollListener();
        AddScrollListener();
    }

    
    private void Start()
    {
        GetStartValues();
        GetScrollBarValues();
    }
    private void GetStartValues()
    {

            startValuePlayerSpeed = (CurrentPlayerController.Character.BaseCharacter.Data.Speed - 1) / 74;
            cameraController.SetCameraPlayer(true);
            startValueCameraZoom = cameraController.GetZoomCameraByDebug(true);
    }

    private void RemoveScrollListener()
    {
        playerSpeedScrollBar.onValueChanged.RemoveListener(ChangePlayerSpeed);
        cameraZoomScrollBar.onValueChanged.RemoveListener(CameraZoom);
    }

    private void AddScrollListener()
    {
        playerSpeedScrollBar.onValueChanged.AddListener(ChangePlayerSpeed);
        cameraZoomScrollBar.onValueChanged.AddListener(CameraZoom);
    }

    private void GetScrollBarValues()
    {
        playerSpeedScrollBar.value = (CurrentPlayerController.Character.BaseCharacter.Data.Speed - 1) / 74;
        cameraZoomScrollBar.value = cameraController.GetZoomCameraByDebug(true);

    }
    

    public void CameraZoom(float zoomValue) {
        cameraController.ZoomCameraByDebug(zoomValue);
        UpdateDebugStats();
    }

    public void ChangePlayerSpeed(float SpeedValue) {
        CurrentPlayerController.Character.BaseCharacter.Data.Speed = (SpeedValue * 74 ) +1 ;
        UpdateDebugStats();
    }
    
    private void UpdateDebugStats()
    {
        playerSpeedTextInfo.text = CurrentPlayerController.Character.BaseCharacter.Data.Speed.ToString();
        cameraZoomTextInfo.text = cameraController.GetZoomCameraByDebug().ToString();
    }
    void OnApplicationQuit()
    {
            ChangePlayerSpeed(startValuePlayerSpeed);          
            CameraZoom(startValueCameraZoom);
    }
}
