using UnityEngine;
public class DebugHandle : MonoBehaviour
{
    private PlayerInputActions inputActions;
    [SerializeField] private GameObject debugMenu;
    [SerializeField] private GameObject playerDebugMenu;

    private void Awake()
    {
        debugMenu.SetActive(false);
        //#if UNITY_EDITOR
        Cursor.lockState = CursorLockMode.Confined;
        Cursor.visible = false;

        inputActions = new PlayerInputActions();
        inputActions.Debug.OpenDebugMenu.performed += (callbackContext) => SwitchEnabledDebugMenu();
        playerDebugMenu.SetActive(false);
        //#endif
    }

    //#if UNITY_EDITOR
    private void Start()
    {
        playerDebugMenu.SetActive(false);
        //FindObjectOfType<SetUpSceneDebug>()?.Setup();


    }

    private void OnEnable()
    {
        inputActions.Enable();
    }
    private void OnDisable()
    {
        inputActions.Disable();
    }
    private void SwitchEnabledDebugMenu()
    {
        debugMenu.SetActive(!debugMenu.activeInHierarchy);
        Cursor.visible = debugMenu.activeInHierarchy;
        //cinemachineBrain.enabled = !debugMenu.activeInHierarchy;
    }
    public void SwitchDebugMenu(int index)
    {

        switch (index)
        {
            case 0:
                playerDebugMenu.SetActive(false);
                break;
            case 1:
                playerDebugMenu.SetActive(true);
                break;
        }
    }
    //#endif
}
