using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowFPS : MonoBehaviour
{
    private Text textUI;
    private float deltaTime = 0;
    // Start is called before the first frame update
    void Awake()
    {
        textUI = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        deltaTime += (Time.deltaTime - deltaTime) * 0.1f;
        float fps = 1.0f / deltaTime;
        textUI.text = Mathf.Ceil(fps).ToString();   
    }
}
