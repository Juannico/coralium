using NaughtyAttributes;
using System;
using UnityEngine;
using UnityEngine.Events;

public class CheckPointSequenceChecker : MonoBehaviour
{
    [SerializeField] private LayerData layerData;
    [SerializeField] private bool hideCheckPoints;
    [SerializeField] [ShowIf("hideCheckPoints")] private bool showByAmount = true;
    [SerializeField] [ShowIf("showByAmount")] private int checkPointByAmount = 4;
    [SerializeField] private bool countLast = true;
    [SerializeField] private UnityEvent OnCompleteEvent;
    [SerializeField] private UnityEvent OnRestartEvent;
    [Header("Reset Setting")]
    [SerializeField] private bool resetOnDistance;
    [ShowIf("resetOnDistance")] [SerializeField] private bool showGuide = false;
    [Header("Support Challenge Setting")]
    [SerializeField] private bool suportChallenge;
    [ShowIf("suportChallenge")] [SerializeField] private BaseChallenge challenge;
    [ShowIf("suportChallenge")] [SerializeField] private bool updateText;
    public int CheckPointCount { get; private set; }
    CheckPointProvider[] checkPoints;

    public Action OnReset;
    public Action OnCompleted;

    private bool activeCheck;
    private Transform targetPlayerTransform;
    private void Awake()
    {
        CheckPointCount = 0;
        checkPoints = GetComponentsInChildren<CheckPointProvider>(true);
        IntiliaizeCheckpoints();
        if (showByAmount)
            showByAmount = checkPointByAmount > 1;
        if (!hideCheckPoints)
            showByAmount = false;
    }
    private void Start()
    {
        checkPoints[0].gameObject.SetActive(true);

        if (!showByAmount)
            return;
        for (int i = 1; i < checkPointByAmount; i++)
            checkPoints[i].gameObject.SetActive(true);
    }
    private void Update()
    {
        if (!resetOnDistance)
            return;
        if (!activeCheck)
            return;
        float distance = Vector3.Distance(targetPlayerTransform.position, checkPoints[CheckPointCount].transform.position);
        if (distance < checkPoints[CheckPointCount].DetectDistance)
            return;
        ResetCheckPoint();
    }
    private void IntiliaizeCheckpoints()
    {
        for (int i = 0; i < checkPoints.Length; i++)
        {
            int checkPointIndex = i;
            checkPoints[i].CollisionHelper.TriggerEnter += (other) =>
            {
                if (!LayerUtilities.IsSameLayer(layerData.PlayerLayer, other.gameObject.layer))
                    return;
                targetPlayerTransform = other.transform;
                if (checkPointIndex == 0)
                    activeCheck = true;
                if (checkPoints[checkPointIndex].IsActive)
                    checkPoints[checkPointIndex].OnEnter(other, CheckPoint(checkPointIndex));
            };
            checkPoints[i].Initialize(this, i);
            if (!checkPoints[i].HideWithDelay)
            {
                checkPoints[i].gameObject.SetActive(false);
                continue;
            }
            int index = i;
            CoroutineHandler.ExecuteActionAfter(() =>
            {
                ComponentUtilities.SetComponent<UnHideGameObject>(checkPoints[index].gameObject);
                checkPoints[index].gameObject.SetActive(false);
            }, Constants.OneSecondDelay, this);
        }
    }
    public bool GotAllCheck()
    {
        float indexOffSet = 0;
        if (countLast)
            indexOffSet = 1;
        if (CheckPointCount + indexOffSet < checkPoints.Length)
            return false;
        return true;
    }
    public void ResetCheckPoint()
    {
        OnReset?.Invoke();
        OnRestartEvent?.Invoke();
        activeCheck = false;
        if (!hideCheckPoints)
            return;
        for (int i = 0; i < CheckPointCount + 1; i++)
        {
            checkPoints[i].Restart();
            checkPoints[i].gameObject.SetActive(i < checkPointByAmount);
        }
        CheckPointCount = 0;

    }
    private bool CheckPoint(int checkpointIndex)
    {
        if (CheckPointCount > checkpointIndex)
        {
            CoroutineHandler.ExecuteActionAfter(() => ResetCheckPoint(), Constants.OneSecondDelay * 2, this);
            return false;
        }
        checkPoints[CheckPointCount].IsActive = false;
        CheckPointCount++;
        if (suportChallenge)
        {
            if (updateText)
                challenge.UpdateInfo($"{CheckPointCount} / {checkPoints.Length}");
        }
        if (AllCheckPointsActivated())
            return true;
        if (!showByAmount)
        {
            checkPoints[CheckPointCount].gameObject.SetActive(true);
            return true;
        }
        if (CheckPointCount % checkPointByAmount != 0)
            return true;
        int untilIndex = checkPointByAmount + CheckPointCount;
        if (untilIndex > checkPoints.Length)
            untilIndex = checkPoints.Length;
        for (int i = CheckPointCount; i < untilIndex; i++)
            checkPoints[i].gameObject.SetActive(true);
        return true;
    }
    private bool AllCheckPointsActivated()
    {
        if (countLast && (CheckPointCount >= (checkPoints.Length - 1)))
        {
            checkPoints[CheckPointCount].IsActive = false;
            if (!showByAmount)
                checkPoints[CheckPointCount].gameObject.SetActive(true);
            CheckPointCount++;
        }
        if (CheckPointCount < checkPoints.Length)
            return false;
        OnCompleteEvent?.Invoke();
        activeCheck = false;
        return true;
    }
    public void CheckAll()
    {
        OnCompleted?.Invoke();
        if (checkPoints == null)
            checkPoints = GetComponentsInChildren<CheckPointProvider>(true);
        for (int i = 0; i < checkPoints.Length; i++)
        {
            checkPoints[i].Completed();
            checkPoints[i].gameObject.SetActive(false);
        }
    }
    public void ShowChallengUI()
    {
        if (!suportChallenge)
            return;
        if (updateText)
            challenge.UpdateInfo($"{CheckPointCount} / {checkPoints.Length}");
    }


#if UNITY_EDITOR
    private void OnDrawGizmosSelected()
    {
        if (!showGuide)
            return;
        CheckPointProvider[] checkpoints = GetComponentsInChildren<CheckPointProvider>(true);
        foreach (CheckPointProvider checkpoint in checkpoints)
            Gizmos.DrawWireSphere(checkpoint.transform.position, checkpoint.DetectDistance);
    }
#endif
}
