using UnityEngine;

public class CheckpointSave : MonoBehaviour
{
    [SerializeField] private LevelManager levelManager;
    [SerializeField] private LayerData layerData;
    [SerializeField] private Transform SpamPosition;

    private void OnTriggerEnter(Collider other)
    {
        if (!LayerUtilities.IsSameLayer(layerData.PlayerLayer, other.gameObject.layer))
            return;
        Vector3 spamwPosition = SpamPosition != null ? SpamPosition.transform.position : transform.position + Vector3.up * 2.5f;
        Vector3 spamwDirection= SpamPosition != null ? SpamPosition.transform.forward : transform.forward;
        levelManager.CurrentLevelData.Spawn.SetLevelCheckPoint(spamwPosition, spamwDirection, levelManager);
    }
}
