using System;
using FMODUnity;
using UnityEngine;
using UnityEngine.Events;

public class CheckPointProvider : MonoBehaviour
{
    public CollisionHelper CollisionHelper;
    public bool HideWithDelay = false;
    [SerializeField] private LayerData layerData;
    private CheckPointSequenceChecker checkPointSequenceChecker;
    [SerializeField] private UnityEvent rightEvent;
    [SerializeField] private UnityEvent wrongEvent;
    public Action<Collider> OnRightSuccessfullEnter;

    [Header("VFX")]
    [SerializeField] private BaseVisualEffect rightVfx;
    [SerializeField] private BaseVisualEffect wrongVfx;
    [Header("SFX")]
    [SerializeField] private StudioEventEmitter rightSfx;
    [SerializeField] private StudioEventEmitter wrongSfx;

    [HideInInspector] public bool IsActive;
    public float DetectDistance = 75;
    /// <summary>
    /// Initializes the Checkpoint assing the delgate to CheckPointSequenceChecker.
    /// </summary>
    /// <param name="checkPointSequenceChecker">The CheckPointSequenceChecker associated with the Checkpoint.</param>
    /// <param name="index">The index of the Checkpoint in the sequence.</param>
    public void Initialize(CheckPointSequenceChecker checkPointSequenceChecker, int index)
    {
        this.checkPointSequenceChecker = checkPointSequenceChecker;
        if (checkPointSequenceChecker == null)
            return;
        this.checkPointSequenceChecker.OnReset += () =>
        {
            if (index > checkPointSequenceChecker.CheckPointCount + 1)
                return;
            rightVfx?.ResetEffect();
            wrongVfx?.ResetEffect();
            IsActive = true;
        };
        IsActive = true;
    }
    /// <summary>
    /// Handles the behavior when an object enters the Checkpoint collider.
    /// </summary>
    /// <param name="other">The Collider of the entering object.</param>
    /// <param name="rightCheck">Indicates whether the Checkpoint check is correct.</param>
    public void OnEnter(Collider other, bool rightCheck)
    {
        if (!rightCheck)
        {
            wrongVfx?.StartEffect();
            wrongSfx?.Play();
            wrongEvent?.Invoke();
            CollisionHelper.enabled = true;
            return;
        }
        CollisionHelper.enabled = false;
        rightEvent?.Invoke();
        OnRightSuccessfullEnter?.Invoke(other);
        if (rightVfx != null)
            rightVfx.transform.position = other.ClosestPoint(transform.position) + transform.forward;
        rightVfx?.StartEffect();
        rightSfx?.Play();
    }
    public void Restart()
    {
        CollisionHelper.enabled = true;
        wrongEvent?.Invoke();
    }
    public void Completed()
    {
        if (rightVfx != null)
            rightVfx.transform.position = Vector3.up * 10000;
        rightVfx?.StartEffect();
    }
}
