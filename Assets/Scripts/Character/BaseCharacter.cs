using NaughtyAttributes;
using UnityEngine;


[CreateAssetMenu(fileName = "Character", menuName = "Coralium/Character/Base")]
public class BaseCharacter : ScriptableObject, ISerializationCallbackReceiver
{
    [field: SerializeField][field: ShowAssetPreview] public GameObject ModelPrefab { get; private set; }
    //[field: SerializeField][field: ShowAssetPreview] public GameObject Meshes { get; private set; }
    [Expandable] public LayerData LayerData;
    [field: SerializeField] public Data Data { get; set; }

    public void OnBeforeSerialize()
    {
        //Data.ResetDelegates();
    }

    public void OnAfterDeserialize()
    {
        Data.ExtraHealth = 0;
        Data.IsInfinteEnergy = false;
        Data.ResetDelegates();
    }
}
