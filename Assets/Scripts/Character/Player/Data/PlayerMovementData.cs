using System;
using UnityEngine;
[CreateAssetMenu(fileName = "PlayerMovementData", menuName = "Coralium/Character/Player Movement Data")]
[Serializable]

public class PlayerMovementData : MovementData
{
    [field: SerializeField] public IdleData IdleData { get; set; }
    [field: SerializeField] public SwinData SwinData { get;  set; }
    [field: SerializeField] public ImpulseData ImpulseData { get;  set; }
    [field: SerializeField] public FollowFishData FollowFishData{ get; set; }
    [field: SerializeField] public StopData StopData { get;  set; }
    [field: SerializeField] public DieData DieData { get;  set; }


}
