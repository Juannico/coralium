using NaughtyAttributes;
using Obi;
using UnityEngine;
using UnityEngine.InputSystem;

[RequireComponent(typeof(PlayerCharacter))]
public class PlayerController : MonoBehaviour
{
    public InputActionReference moveRef;
    [SerializeField] [Scene] private string RestartLoadScene;
    [field: SerializeField] public PlayerCharacter Character { get; private set; }
    public PlayerProgressHandle ProgressHandle;
    private PlayerAnimationController animationController;
    [field: SerializeField] public Magnet Magnet { get; private set; }
    public Transform BaseTransform { get; private set; }
    [SerializeField] private FindBreakableObjects findBreackbleObjects;
    [field: SerializeField] public GameObject TrailLine { get; private set; }
    public CameraController CameraController;
    [Expandable] public CameraData CameraData;
    public PlayerStateComponentHandler StateHandler { get; private set; }
    private IdleStateComponent idleState;
    private DieStateComponent dieState;
    public Vector2 MoveVector = Vector2.zero;
    [SerializeField] private LevelManager levelManager;
    private void Awake()
    {
        ProgressHandle.Initialize(this);
        Character = GetComponent<PlayerCharacter>();
        Character.Initialize();
        Magnet = Instantiate(Magnet);
        Magnet.Initialize(transform);
        findBreackbleObjects = Instantiate(findBreackbleObjects);
        findBreackbleObjects.Initialize(transform);
        if (CameraController == null)
            CameraController = GameObject.FindGameObjectWithTag(Tags.CameraController).GetComponent<CameraController>();
        TrailLine.SetActive(false);
        BaseTransform = Character.Model.transform.parent;
        StateHandler = gameObject.AddComponent<PlayerStateComponentHandler>();
        Character.BaseCharacter.Data.OnCharacterKnockBack = (knockBackDirection, duration, isCrashing) =>
        {
            StateHandler.OnKnokBack(knockBackDirection, duration, isCrashing);
            float multiplier = Mathf.Pow(duration, 0.5f);
            float shakeDurationMultiplier = 0.75f;
            CameraController.ShakeCamera(Character.CameraData.KnockBackShakeAmplitude * multiplier, Character.CameraData.KnockBackShakeFrecuency * multiplier, multiplier * shakeDurationMultiplier);
        };
        Character.Model.AddComponent<ObiCollider>().sourceCollider = Character.Collider;
        idleState = GetComponent<IdleStateComponent>();
        dieState = GetComponent<DieStateComponent>();
        animationController = GetComponentInChildren<PlayerAnimationController>(true);
        animationController.Initialize();
        TrailLine.transform.parent = BaseTransform.transform;
    }
    private void Start()
    {
        StateHandler.SwitchState(typeof(WaitStateComponent));
        levelManager.Player = gameObject;
    }
    private void OnEnable()
    {
        PlayerInputManager.Instance.BindInputActionPerformed(moveRef, Move);
        PlayerInputManager.Instance.BindInputActionCanceled(moveRef, StopMove);
    }
    public void Move(InputAction.CallbackContext ctx) => MoveVector = ctx.ReadValue<Vector2>();
    public void StopMove(InputAction.CallbackContext ctx) => MoveVector = Vector2.zero;
    private void Update()
    {
        Shader.SetGlobalVector("_TrackedPosition", transform.position + Vector3.up * Character.Collider.radius);
        float sideDirection = GetSideDirection();
        float animationSpeed = Character.Rigidbody.velocity.magnitude / (Character.BaseCharacter.Data.Speed);
        animationController.SetAnimationState(idleState.enabled || dieState.enabled, sideDirection, animationSpeed);
    }
    private float GetSideDirection()
    {
        if (idleState.enabled)
        {
            if (!StateHandler.Data.IsBackingStarIdlePosition)
                return 0f;
            return Vector3.Cross(BaseTransform.forward, Character.Rigidbody.velocity.normalized).y;
        }
        return Vector3.Cross(BaseTransform.forward, StateHandler.Data.MovementDirection.normalized).y;
    }
    public void StartDied() => StateHandler.SwitchState(typeof(DieStateComponent));



}
