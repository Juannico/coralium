using UnityEngine;
/// <summary>
/// Class for when you want to centralize the player's teleportation
/// </summary>
public class SetPlayerPosition
{
    private Vector3 playerPosition;
    private Vector3 playerDirection;
    private LevelManager levelManager;
    public void SetPosition(Vector3 playerPosition, Vector3 playerDirection, LevelManager levelManager)
    {
        this.playerPosition = playerPosition;
        this.playerDirection = playerDirection;
        this.levelManager = levelManager;
        PlayerFinded(levelManager.Player);
    }
    private void PlayerFinded(GameObject player)
    {
        if (player == null)
        {
            levelManager.OnPlayerFinded += PlayerFinded;
            return;
        }
        StateComponentRotate playerRotateComponent = player.GetComponentInChildren<StateComponentRotate>();
        player.transform.position = playerPosition;
        playerRotateComponent.InstantRotate(playerDirection);
        levelManager.CurrentLevelData.Spawn.SetLevelCheckPoint(player.transform.position, playerDirection, levelManager);
        levelManager.OnPlayerFinded -= PlayerFinded;
    }
}
