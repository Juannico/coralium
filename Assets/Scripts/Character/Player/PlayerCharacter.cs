using FMODUnity;
using NaughtyAttributes;
using TamarilloTools;
using UnityEngine;

public class PlayerCharacter : Character
{
    [SerializeField] [Expandable] private PlayerAbilities playerAbilities;

    [HideInInspector] public PlayerMovementData MovementData;
    [SerializeField] private Material recieveDamageMaterial;
    [SerializeField] private ParticleSystem turnAroundFishvfx;
    [Header("Player SFX")]
    [SerializeField] private StudioEventEmitter lowHealthSnapshot;
    [SerializeField] private StudioEventEmitter lowHealthEvent;
    [SerializeField] private StudioEventEmitter receiveDamageSFX;
    [SerializeField] private StudioEventEmitter deathSFX;
    private CameraController cameraController;
    [HideInInspector] public CameraData CameraData;
    [SerializeField] [Expandable] private FindEnemyData findEnemyData;
    private AutoAim autoAim;
    private PlayerController playerController;
    [SerializeField] private SlowMotion slowMotion;
    private Renderer rnderer;
    private void OnDestroy()
    {
        playerAbilities.ResetAbilities();
    }
    public override void Initialize()
    {
        base.Initialize();
        MovementData = BaseMovementData as PlayerMovementData;
        playerController = GetComponent<PlayerController>();
        cameraController = playerController.CameraController;
        CameraData = playerController.CameraData;
        ToggleLowHealthSnapshot();
        playerAbilities.InitializeAbilities(this);
        autoAim = new AutoAim(findEnemyData, this);
        playerController = GetComponent<PlayerController>();
        rnderer = Model.GetComponentInChildren<Renderer>();
        SetVfx();
    }
    public void SetVfx()
    {
    }
    public override void DoDamage(int damage, DamageTypes type, Vector3 damageDirection)
    {
        if (damageDirection != Vector3.zero)
            playerController.StateHandler.OnKnokBack(damageDirection, Constants.DamageBlinkDuration);
        base.DoDamage(damage, type, damageDirection);
        BaseCharacter.Data.IsRecievingDamage = true;
        cameraController.ShakeCamera(CameraData.DamageShakeAmplitude, CameraData.DamageShakeFrecuency);
        if (!BaseCharacter.Data.IsDead)
            receiveDamageSFX?.Play();
        ToggleLowHealthSnapshot();

        CoroutineHandler.ExecuteActionAfter(() =>
        {
            if (BaseCharacter.Data.IsDead)
                return;
            cameraController.StopCameraShake();
            BaseCharacter.Data.IsRecievingDamage = false;
        }, Constants.DamageBlinkDuration * BaseCharacter.Data.InmunityDuration, this);
        if (recieveDamageMaterial == null)
            return;
        Material previousMaterial = rnderer.material;
        setMaterialProvider.ChangeMaterial(rnderer.material, recieveDamageMaterial);
        CoroutineHandler.ExecuteActionAfter(() =>
        {
            if (!BaseCharacter.Data.IsDead)
                setMaterialProvider.ChangeMaterial(rnderer.material, previousMaterial);
        }, Constants.DamageBlinkDuration, this);
    }

    public void ToggleLowHealthSnapshot()
    {
        if (BaseCharacter.Data.CurrentHealth == 1)
        {
            lowHealthEvent?.Play();
            lowHealthEvent?.GetComponent<StudioGlobalParameterTrigger>().TriggerParameters();
            lowHealthSnapshot?.Play();
        }
        else
        {
            lowHealthEvent?.Stop();
            lowHealthSnapshot?.Stop();
        }
    }
    public override void Died()
    {
        base.Died();
        ToggleLowHealthSnapshot();
        deathSFX?.Play();
        playerAbilities.ResetAbilities();
        playerController.StartDied();
    }
    public override float GetEffectDuration() => MovementData.DieData.KnockBackDuration;

    public override void DiedAction()
    {
        base.DiedAction();
        float duration = MovementData.DieData.KnockBackDuration;

        float ExcecutionFadeTime = duration * 0.82f - 1f;
        if (ExcecutionFadeTime < 0)
        {
            FadeUI.FadeOut();
            return;
        }
        CoroutineHandler.ExecuteActionAfter(() =>
        {
            FadeUI.FadeOut();
        }, ExcecutionFadeTime, this);
        cameraController.ShakeCamera(1.5f, 5.5f, ExcecutionFadeTime * 0.5f);
    }
    public override void AfterDiedEffect()
    {
        BaseCharacter.Data.ResetDelegates();
        FindObjectOfType<RestartLevel>().Restart();
    }

    public override void EndCastAbility() => cameraController.RestoreZoomValue();
    public override Vector3 GetAbilityDirection() => autoAim.GetDirection();
    public override void OnImpulse(float impulseDuration, float impulseDistance, System.Action onMaxVelocityReached)
    {
        // slowMotion?.StarSlowMotion();
        playerController.StateHandler.OnImpulseStarted(impulseDuration, impulseDistance, onMaxVelocityReached);
    }
    public override void StopImpulse() => playerController.StateHandler.StopImpulse();

}
