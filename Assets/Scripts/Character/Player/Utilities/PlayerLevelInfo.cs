using NaughtyAttributes;
using System;
using UnityEngine;
using UnityEngine.SceneManagement;

[Serializable]
public class PlayerLevelInfo : ISerializationCallbackReceiver
{
    [Scene] public string LevelSceneName;
 
    [field:SerializeField] public Vector3 CheckPointPosition { get; private set; }
    [field:SerializeField] public Vector3 CheckPointDirection { get; private set; }
    [field:SerializeField] public float PlantIndex { get; private set; }
    
    public void OnAfterDeserialize()
    {
#if UNITY_EDITOR
        CheckPointPosition = Vector3.zero;
        CheckPointDirection = Vector3.zero;
        PlantIndex = 0;
#endif
    }

    public void OnBeforeSerialize()
    {

    }

    public void SetLevelCheckPoint(Vector3 starPosition,Vector3 startDirection) 
    {
        CheckPointPosition = starPosition;
        CheckPointDirection = startDirection;
        LevelSceneName = SceneManager.GetActiveScene().name;
        PlantIndex ++;
    }
}
