using System;
using UnityEngine;
using Cinemachine;
[Serializable]
public class PlayerProgressHandle
{
    private PlayerController playerController;
    Action<bool> unlockMovement;
    private CinemachineBrain cinemachineBrain;
    private PlayerInputManager playerInputManager;
    [SerializeField] private AbilityTutorialData abilityTutorialData;
    public void Initialize(PlayerController playerController)
    {
        this.playerController = playerController;
        cinemachineBrain = Camera.main.GetComponent<CinemachineBrain>();
        playerInputManager = PlayerInputManager.Instance;
        unlockMovement = new Action<bool>((bool state) =>
        {
            cinemachineBrain.enabled = true;
        });

    }

    public void LearnAbility(AbilityData abilityData, UnlockAbility abilityToUnlock, Action<bool> unlockEvent)
    {
        cinemachineBrain.enabled = false;
        unlockEvent += (state) => GameManager.Instance.TutorialHandler.StartTutorial(abilityData.abilityTutorialData, TutorialHandlerComponent.TutorialType.FullScreenAbilityPopUp);
        unlockEvent += unlockMovement;
        Action<bool> setLockUnlockMovement = unlockEvent;
        abilityToUnlock.Setlock(playerController, false, setLockUnlockMovement);
    }
}

