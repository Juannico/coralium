using FMODUnity;
using NaughtyAttributes;
using System.Collections;
using UnityEngine;

[RequireComponent(typeof(RigibodyForceHandle))]
public class Character : MonoBehaviour, IDamageable<int, DamageTypes>
{
    [Expandable] public BaseCharacter BaseCharacter;
    [SerializeField] public GameObject Model;
    [SerializeField] protected Transform modelContainer;
    [SerializeField] private GameObject inmunityObject;
    [SerializeField] protected bool diedEffect = true;
    public FEELSdata FEELSData;
    public LoadBar EnergiBar;
    public Rigidbody Rigidbody { get; private set; }

    public CapsuleCollider Collider { get; private set; }
    public Transform BaseTransform { get; private set; }
    public RigibodyForceHandle RigibodyForceHandle { get; private set; }
    [Header("Sounds")]
    [SerializeField] protected StudioEventEmitter movementEmitter;
    [Expandable] public MovementData BaseMovementData;

    protected SetMaterialProvider setMaterialProvider;

    public AbilitiesHandle AbilitiesHandle;
    public virtual void Initialize()
    {
        AbilitiesHandle = new AbilitiesHandle();
        Rigidbody = GetComponent<Rigidbody>();
        Data tempData = BaseCharacter.Data;
        BaseCharacter = BaseCharacter.Clone();
        BaseCharacter.Data = new Data(tempData);
        BaseTransform = modelContainer;
        modelContainer.name = $"{BaseCharacter.name}_{modelContainer.name}";
        modelContainer.localPosition = Vector3.zero;
        ///Model = Instantiate(BaseCharacter.ModelPrefab, transform.position + BaseCharacter.ModelPrefab.transform.position, modelContainer.rotation, modelContainer);
        Model.transform.SetAsFirstSibling();
        Collider = Model.GetComponent<CapsuleCollider>();
        BaseCharacter.Data.Initilize();
        RigibodyForceHandle = GetComponent<RigibodyForceHandle>();
        RigibodyForceHandle.Initialize(Rigidbody);
        setMaterialProvider = ComponentUtilities.SetComponent<SetMaterialProvider>(Model);
        if (BaseCharacter.Data.ChangeColorOnInmunity)
        {
            BaseCharacter.Data.OnInmunityChange += (inmunityState) =>
            {
                if (!BaseCharacter.Data.IsRecievingDamage)
                    inmunityObject?.SetActive(inmunityState);
            };
        }
    }
    public void TakeDamage(int damage, DamageTypes type, Vector3 damageDirection)
    {
        if (BaseCharacter.Data.IsDead)
            return;
        if (BaseCharacter.Data.IsImmune)
            return;
        if (BaseCharacter.Data.IsRecievingDamage)
            return;
        if (BaseCharacter.Data.CurrentShield > 0 && type == DamageTypes.Poison)
        {
            BaseCharacter.Data.CurrentShield--;
            return;
        }
        DoDamage(damage, type, damageDirection);
    }
    public virtual void DoDamage(int damage, DamageTypes type, Vector3 damageDirection)
    {
        AbilitiesHandle.DisrupAbilities();
        if (type == DamageTypes.Poison)
            damage = 1;
        BaseCharacter.Data.CurrentHealth -= damage;
        ApplyDamageTypeEffect(type);
        setMaterialProvider.SetMaterial(0, (material) => ControlShaderColor.ChangeColor(this, material, Constants.DamageColor, Constants.DamageBlinkDuration));
        if (BaseCharacter.Data.CurrentHealth <= 0)
            Died();
    }

    public virtual void Died()
    {
        if (BaseCharacter.Data.IsDead)
            return;
        BaseCharacter.Data.IsDead = true;
        DiedAction();
        if (BaseCharacter.Data.DiedMaterial == null || !diedEffect)
        {

            AfterDiedEffect();
            Model.AddComponent<UnHideGameObject>();
            return;
        }
        CoroutineHandler.ExecuteActionAfter(() =>
        {

            float timer = 0;
            float duration = GetEffectDuration();
            if (duration <= 0)
                duration = 1;
            Renderer renderer = Model.GetComponentInChildren<Renderer>();

            setMaterialProvider.ChangeMaterial(renderer.material, BaseCharacter.Data.DiedMaterial, (material) =>
            {
                StartCoroutine(CoroutineHandler.ExecuteActionAfterUpdateUntil(() =>
                {
                    Color baseColor = BaseCharacter.Data.DiedMaterial.GetColor("_Color");
                    timer += Time.deltaTime;
                    material.SetColor("_Color", baseColor);
                    material.SetFloat("_Disolve", timer / duration);
                    material.SetFloat("_AnchoLinea", renderer.material.GetFloat("_AnchoLinea") + Time.deltaTime * 0.02f);
                    return timer > duration * 0.9f;
                }
               , () =>
               {
                   AfterDiedEffect();
               }));
            });
        }, Constants.DamageBlinkDuration * 0.75f, this);
    }
    public virtual void DiedAction()
    {
        Collider.enabled = false;
    }
    public virtual float GetEffectDuration() => 1;
    public virtual void AfterDiedEffect()
    {
        BaseCharacter.Data.OnCharacterDead?.Invoke();
        BaseCharacter.Data.ResetDelegates();
    }
    public virtual void ApplyDamageTypeEffect(DamageTypes type)
    {
    }
    public virtual void EndCastAbility()
    {
    }
    public void SetAbilityFeelsTarget(BaseAbilityCast abilityCast, bool active)
    {
        if (active)
        {
            Transform targetTransform = modelContainer.GetChild(0);
            if (targetTransform.parent == abilityCast.TargetFeelsContainer)
                return;
            abilityCast.TargetFeelsContainer.parent = modelContainer;
            abilityCast.TargetFeelsContainer.SetAsFirstSibling();
            abilityCast.TargetFeelsContainer.localPosition = Vector3.zero;
            abilityCast.TargetFeelsContainer.localRotation = Quaternion.Euler(Vector3.zero);
            abilityCast.TargetFeelsContainer.localScale = Vector3.one;
            targetTransform.parent = abilityCast.TargetFeelsContainer;
            return;
        }
        if (abilityCast.TargetFeelsContainer.parent == abilityCast.transform && Model.transform.parent == modelContainer)
            return;
        Vector3 lastScale = Model.transform.localScale;
        Model.transform.parent = modelContainer;
        abilityCast.TargetFeelsContainer.parent = abilityCast.transform;
        abilityCast.TargetFeelsContainer.localPosition = Vector3.zero;
        abilityCast.TargetFeelsContainer.localRotation = Quaternion.Euler(Vector3.zero);
        abilityCast.TargetFeelsContainer.localScale = Vector3.one;
        Model.transform.localScale = lastScale;
        Model.transform.localPosition = Vector3.zero;
        Model.transform.localRotation = Quaternion.Euler(Vector3.zero);
    }
    public void ExitExternalForce()
    {
        if (!RigibodyForceHandle.AffectByExternalForce)
        {
            BaseCharacter.Data.IsInExternalForce = false;
            return;
        }
        BaseCharacter.Data.ExternalImpulseDirection = Vector3.zero;
        StartCoroutine(ExternalImpulse());
    }

    private IEnumerator ExternalImpulse()
    {
        Vector3 startForce = Rigidbody.velocity;
        if (startForce.magnitude > 8)
        {
            startForce.Normalize();
            startForce *= 8;
        }
        float interpolateAmount = 0;
        while (interpolateAmount < 1)
        {
            if (BaseCharacter.Data.ExternalImpulseDirection != Vector3.zero)
                yield break;
            interpolateAmount = Mathf.Clamp01(interpolateAmount + Time.deltaTime * 0.35f);
            RigibodyForceHandle.VelocityForceVector += Vector3.Lerp(startForce, Vector3.zero, interpolateAmount);
            yield return null;
        }
        BaseCharacter.Data.IsInExternalForce = false;
    }

    public virtual Vector3 GetAbilityDirection() => BaseTransform.forward;

    public void MovementSoundUpdate(float parameterValue)
    {
        // movementEmitter.SetParameter("Speed", parameterValue);
    }
    public virtual void OnImpulse(float impulseDuration, float impulseDistance, System.Action onMaxVelocityReached) { }
    public virtual void StopImpulse() { }

}
