using System;
using UnityEngine;
using UnityEngine.Events;

public class EnemyController : MonoBehaviour
{
    public EnemyCharacter Character { get; private set; }
    private BaseAnimationController animationController;
    public EnemyAbilities EnemyAbilities { get; private set; }
    public bool HorizontalMove = false;
    public CollisionHelper ActiveChase;
    [SerializeField] private float referenceSpeedMultiplier = 0.75f;
    private PlayerController player;
    public PlayerController Player
    {
        get
        {
            if (player == null)
                Player = GameObject.FindGameObjectWithTag(Tags.Player).GetComponent<PlayerController>();
            return player;
        }
        set => player = value;
    }


    private Vector3 lastForward;
    private Vector2 currentSmoothMovementDirection;

    public bool IsDead { get; private set; }
    public bool CanMove { get; private set; }
    public bool IsFocusingPlayer { get; set; }
    protected StateSharedData stateSharedData;
    protected IdleStateComponent idleState;
    protected DieStateComponent dieState;
    protected KnockBackStateComponent knockBackStateComponent;

    public StateSharedData EnemyStateSharedData { get => stateSharedData; }

    public UnityEvent OnDied;

    [HideInInspector] public DamageTypes damageType = DamageTypes.Basic;
    private void Awake()
    {
        Character = GetComponent<EnemyCharacter>();
        Character.Initialize();
        stateSharedData = ComponentUtilities.SetComponent<StateSharedData>(gameObject);
        stateSharedData.FEELSdata = Character.FEELSData;
        stateSharedData.BaseTransform = Character.BaseTransform;
    }
    protected virtual void Start()
    {
        EnemyAbilities = GetComponent<EnemyAbilities>();
        EnemyAbilities.InitializeAbilities(Character);
        animationController = GetComponentInChildren<LionFishAnimationController>(true);
        animationController?.Initialize();
        IsDead = false;
        CanMove = true;
        IsFocusingPlayer = false;
        Player = GameObject.FindGameObjectWithTag(Tags.Player).GetComponent<PlayerController>();
        SetStatesComponents(stateSharedData);
        SetCheck();
    }

    private void SetStatesComponents(StateSharedData stateSharedData)
    {
        idleState = ComponentUtilities.SetComponent<IdleStateComponent>(gameObject);
        dieState = ComponentUtilities.SetComponent<DieStateComponent>(gameObject);

        StateComponentRotate stateRotate = gameObject.AddComponent<StateComponentRotate>();
        stateSharedData.TargetRotation = Quaternion.Euler(Vector3.zero);
        stateRotate.Initialize(Character, stateSharedData);
        stateSharedData.CurrentFollowObject = Player.gameObject;
        stateSharedData.HorizontalMove = HorizontalMove;
    }
    protected virtual void Update()
    {
        SetAnimation();
        lastForward = Character.BaseTransform.forward;
        IsDead = Character.BaseCharacter.Data.IsDead;
        CanMove = GetCanMove();
    }
    private void OnDestroy()
    {
        if (ActiveChase != null)
        {
            ActiveChase.TriggerEnter -= CheckEnter;
            ActiveChase.TriggerExit -= CheckExit;
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (!LayerUtilities.IsSameLayer(Character.BaseCharacter.LayerData.PlayerLayer, other.gameObject.layer))
            return;
        if (!Character.BaseCharacter.Data.IsRecievingDamage)
            other.gameObject.GetComponentInParent<IDamageable<int, DamageTypes>>()?.TakeDamage(1, damageType, ColliderUtilities.GetContactDirection(other, transform.position));
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (!LayerUtilities.IsSameLayer(Character.BaseCharacter.LayerData.PlayerLayer, collision.gameObject.layer))
            return;
        if (!Character.BaseCharacter.Data.IsRecievingDamage)
            collision.gameObject.GetComponentInParent<IDamageable<int, DamageTypes>>()?.TakeDamage(1, damageType, ColliderUtilities.GetContactDirection(collision, transform.position));
    }
    private void SetAnimation()
    {
        float sideDirection = Vector3.Cross(lastForward, Character.BaseTransform.forward).normalized.y;
        float animationSpeed = Character.Rigidbody.velocity.magnitude / (Character.BaseCharacter.Data.Speed * referenceSpeedMultiplier);
        animationController?.SetAnimationState(idleState.enabled || dieState.enabled, sideDirection, animationSpeed);
    }
    public void SetStateDataDirectionValues(Vector2 movementVector, Vector2 movementDirection)
    {
        if (HorizontalMove)
        {
            movementVector.y = 0;
            movementDirection.y = 0;
        }
        stateSharedData.MovementVector = movementVector;
        stateSharedData.MovementDirection = Vector2.SmoothDamp(stateSharedData.MovementDirection, movementDirection, ref currentSmoothMovementDirection, 0.2f);
    }
    protected virtual bool GetCanMove()
    {
        return !Character.isStuning;
    }
    public virtual void SetTarget(bool shoulTargetPlayer = true, bool checkOnTrigger = false)
    {
        if (shoulTargetPlayer)
            stateSharedData.CurrentFollowObject = Player.Character.Model.gameObject;
    }
    public void SetExternalTarget(GameObject target) => stateSharedData.CurrentFollowObject = target;

    public void SetCheck()
    {
        if (ActiveChase != null)
        {
            ActiveChase.TriggerEnter += CheckEnter;
            ActiveChase.TriggerExit += CheckExit;
        }
    }
    private void CheckEnter(Collider collider)
    {
        if (!LayerUtilities.IsSameLayer(Character.BaseCharacter.LayerData.PlayerLayer, collider.gameObject.layer))
            return;
        IsFocusingPlayer = true;
        if (Player == null)
            Player = collider.GetComponentInParent<PlayerController>();
        SetTarget(true, true);
    }
    private void CheckExit(Collider collider)
    {
        if (!LayerUtilities.IsSameLayer(Character.BaseCharacter.LayerData.PlayerLayer, collider.gameObject.layer))
            return;
        IsFocusingPlayer = false;
    }

    public virtual bool CompleteRequirements() => true;
    public Transform GetTarget() => stateSharedData.CurrentFollowObject.transform;
}
