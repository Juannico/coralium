using System.Collections;
using UnityEngine;

public class DynamiteTriggerExplode : MonoBehaviour
{
    public float flo_tiempo = 1, flo_rango = 40;
    public float timerefresh= 0.01f;
    public void Start()
    {
        StartCoroutine(Explode());
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponentInParent<IDamageable<int, DamageTypes>>() != null)
        {           
            other.gameObject.GetComponentInParent<IDamageable<int, DamageTypes>>().TakeDamage(1,DamageTypes.Basic, ColliderUtilities.GetContactDirection(other, transform.position));
        }
    }
    IEnumerator Explode()
    {
        transform.localScale = Vector3.zero;
        while (transform.localScale.x < flo_rango)
        {
            transform.localScale += Vector3.one * (1 / (flo_tiempo));
            yield return new WaitForSeconds(timerefresh);
        }
        if (transform.localScale.x >= flo_rango)
        {
            Destroy(gameObject);
        }
    }
}
