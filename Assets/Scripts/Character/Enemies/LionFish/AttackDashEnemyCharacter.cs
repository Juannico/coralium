using System;
using System.Collections;
using UnityEngine;
public class AttackDashEnemyCharacter : EnemyCharacter
{
    public Vector3 StartPosition;
    [HideInInspector] public float CooldownTimer;
    [HideInInspector] public float Cooldown;
    private bool isAttacking = false;
    private bool isBacking = false;
    public Vector3 StartRotation { get; private set; }
    public float MaxDistance { get; private set; }
    private void Start()
    {
        StartPosition = transform.position;
        CooldownTimer = 0;
    }
    private void Update()
    {
        CooldownTimer += Time.deltaTime;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!LayerUtilities.IsSameLayer(BaseCharacter.LayerData.PlayerLayer, other.gameObject.layer))
            return;
        if (isBacking)
            return;
        other.gameObject.GetComponentInParent<IDamageable<int, DamageTypes>>()?.TakeDamage(BaseCharacter.Data.DamageMultiplier, DamageTypes.Basic, transform.forward);
        StartCoroutine(Back());
    }


    public void DetectEnemy(Collider other)
    {
        if (CooldownTimer < Cooldown || isAttacking)
            return;
        if (!LayerUtilities.IsSameLayer(BaseCharacter.LayerData.EnemyLayer, other.gameObject.layer))
            return;
        Vector3 targetPosition = other.GetComponentInParent<Character>().Model.transform.position;
        MaxDistance = Vector3.Distance(StartPosition, targetPosition)  +1 ;
        StartCoroutine(Attack(targetPosition));
    }
    private IEnumerator Attack(Vector3 targetPosition)
    {
        isBacking = false;
        isAttacking = true;
        Vector3 direction = targetPosition - Model.transform.position;
        Quaternion targetRotation = Quaternion.LookRotation(direction);
        while (transform.rotation != targetRotation) {
            transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, 10 * MovementData.ModelSpeedRotation * Time.fixedDeltaTime);
            yield return null;
        }
        while ( Vector3.Distance(StartPosition, Model.transform.position) < MaxDistance)
        {
            RigibodyForceHandle.VelocityForceVector = direction.normalized * BaseCharacter.Data.Speed;
            if (isBacking)
                yield break;
            yield return null;  
        }
        if (isBacking)
            yield break;
        Rigidbody.velocity = Vector3.zero;
        RigibodyForceHandle.VelocityForceVector = Vector3.zero;
        StartCoroutine(Back());
    }
    private IEnumerator Back()
    {
        Vector3 direction = StartPosition - Model.transform.position;
        Quaternion targetRotation = Quaternion.LookRotation(direction);
        isBacking = true;
        while (transform.rotation != targetRotation)
        {
            transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, 10 * MovementData.ModelSpeedRotation * Time.fixedDeltaTime);
            yield return null;
        }
        while (Vector3.Distance(StartPosition, Model.transform.position) >= 2)
        {
            RigibodyForceHandle.VelocityForceVector = direction.normalized * BaseCharacter.Data.Speed;
            yield return null;
        }
        isBacking = false;
        isAttacking = false;
        Rigidbody.velocity = Vector3.zero;
        transform.position = StartPosition;
        CooldownTimer = 0f;
        transform.rotation = Quaternion.Euler(StartRotation); 
    }
}
