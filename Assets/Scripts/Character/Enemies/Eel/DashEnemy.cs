using UnityEngine;
[RequireComponent(typeof(AttackDashEnemyCharacter))]

public class DashEnemy : MonoBehaviour
{
    private AttackDashEnemyCharacter character;
    [SerializeField] private float cooldown;
    [SerializeField] private CollisionHelper detectEnemyTrigger;
    private void Awake()
    {
        character = GetComponent<AttackDashEnemyCharacter>();     
        character.Initialize();
        character.Cooldown = cooldown;
    }
    private void OnEnable()
    {
        detectEnemyTrigger.TriggerEnter += character.DetectEnemy;
        detectEnemyTrigger.TriggerStay += character.DetectEnemy;
    }
    private void OnDisable()
    {
        detectEnemyTrigger.TriggerEnter -= character.DetectEnemy;
        detectEnemyTrigger.TriggerStay -= character.DetectEnemy;
    }
}
