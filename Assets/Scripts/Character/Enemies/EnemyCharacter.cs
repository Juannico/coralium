using FMODUnity;
using System.Collections;
using UnityEngine;

public class EnemyCharacter : Character
{
    [HideInInspector] public EnemyMovementData MovementData;
    [SerializeField] private float knockBackDuration = 1.5f;
    [SerializeField] private float knockBackMultiplier = 2.25f;
    [SerializeField] private bool autoAim = false;
    [Header("Specific SFX")]
    [SerializeField] private StudioEventEmitter deathSFX;
    [SerializeField] private EventEmitterData battleMusicData;
    protected EnemyController controller;
    public bool isStuning { get; private set; }
    public Vector3 TargetGuide = Vector3.up;
    public override void Initialize()
    {
        base.Initialize();
        controller = GetComponent<EnemyController>();
        MovementData = BaseMovementData as EnemyMovementData;
    }
    public override void DoDamage(int damage, DamageTypes type, Vector3 damageDirection)
    {
        if (!controller.CompleteRequirements())
            return;
        base.DoDamage(damage, type, damageDirection);
        if (BaseCharacter.Data.IsDead)
            return;
        BaseCharacter.Data.IsRecievingDamage = true;
        BaseCharacter.Data.OnCharacterKnockBack?.Invoke(damageDirection + Random.onUnitSphere, knockBackDuration, false);
        CoroutineHandler.ExecuteActionAfter(() => BaseCharacter.Data.IsRecievingDamage = false, Constants.DamageBlinkDuration, this);
    }
    public override float GetEffectDuration() => MovementData.DieData.KnockBackDuration * knockBackMultiplier * 0.75f;
    public override void Died()
    {
        if (BaseCharacter.Data.IsDead)
            return;
        gameObject.GetComponentInChildren<SetWorldHealthUiOnTopEnemy>()?.OffUI();
        //SoundManager.Instance.MusicManager?.SetMusicParameter(battleMusicData.ParameterName, 0);
        if (controller == null)
            controller = GetComponent<EnemyController>();
        if (controller.ActiveChase != null)
        {
            if (controller.ActiveChase.TryGetComponent(out MusicParameterTrigger musicParameterTrigger))
                musicParameterTrigger.SetParameter(true);
            controller.ActiveChase.Collider.enabled = false;
        }
        controller.OnDied?.Invoke();
        base.Died();
        deathSFX?.Play();
    }
    public override void AfterDiedEffect()
    {
        base.AfterDiedEffect();
        GetComponent<DropObjects>()?.Drops();
        Model.SetActive(false);
    }
    public override void ApplyDamageTypeEffect(DamageTypes type)
    {
        base.ApplyDamageTypeEffect(type);
    }
    private IEnumerator StopStunning()
    {
        yield return new WaitForSeconds(5);
        //ElectrifyMaterial?.SetFloat("_Alpha", 0);
        isStuning = false;
    }
    public override Vector3 GetAbilityDirection()
    {
        Vector3 direction = base.GetAbilityDirection();
        if (autoAim && controller.Player != null)
            direction = (controller.Player.transform.position - transform.position).normalized;
        return direction;
    }

#if UNITY_EDITOR
    private void OnDrawGizmosSelected()
    {
        if (Application.isPlaying)
            return;
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(transform.position + TargetGuide, Vector3.one * 5);
        Gizmos.DrawRay(transform.position + TargetGuide - (Vector3.up * 2.5f), Vector3.up * 5);
        Gizmos.DrawRay(transform.position + TargetGuide - (Vector3.right * 2.5f), Vector3.right * 5);
        Gizmos.DrawRay(transform.position + TargetGuide - (Vector3.forward * 2.5f), Vector3.forward * 5);
    }
#endif
}
