using UnityEngine;

public class EnemyZoneAbilityTrigger : MonoBehaviour
{
    [SerializeField] private EnemyController enemyController;
    [SerializeField] private EnemyAbilities enemyAbilities;
    [SerializeField] private int indexAbility = 0;
    [SerializeField] private bool repeatAttack;
    [SerializeField] private LayerData layerData;
    [SerializeField] private float tickMultiplier = 2;
    private float timer;
    private float tick;
    private bool isAttacking;
    private bool isCasting;

    private void Start()
    {
        isAttacking = false;
        enemyAbilities.SetCurrenAbility(indexAbility);
        tick = enemyAbilities.CurrentAbility.Data.Cooldown * tickMultiplier;
    }
    private void Update()
    {
        if (enemyController.Character.BaseCharacter.Data.IsDead)
        {
            enabled = false;
            return;
        }
        if (!repeatAttack)
            return;
        if (isCasting)
            return;
        timer += Time.deltaTime;
        if (!isAttacking)
            return;
        if (timer <= tick)
            return;
        UseAbility();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!LayerUtilities.IsSameLayer(layerData.PlayerLayer, other.gameObject.layer))
            return;
        if (enemyController.Player == null)
            enemyController.Player = other.GetComponentInParent<PlayerController>();
        isAttacking = true;
        if (!repeatAttack)
            UseAbility();
    }
    private void OnTriggerExit(Collider other)
    {
        if (!LayerUtilities.IsSameLayer(layerData.PlayerLayer, other.gameObject.layer))
            return;
        isAttacking = false;

    }
    private void UseAbility()
    {
        timer = 0;
        enemyAbilities.SetCurrenAbility(indexAbility);
        enemyAbilities.CurrentAbility.Load();
        if (!enemyAbilities.CurrentAbility.Data.HasLoadTime)
        {
            Attack();
            return;
        }
        isCasting = true;
        CoroutineHandler.ExecuteActionAfter(() => Attack(), enemyAbilities.CurrentAbility.Data.LoadTime, this);
    }
    protected virtual void Attack()
    {    
        enemyAbilities.CurrentAbility.Loaded();
        enemyAbilities.CurrentAbility.Cast(true);
        isCasting = false;
    }
}
