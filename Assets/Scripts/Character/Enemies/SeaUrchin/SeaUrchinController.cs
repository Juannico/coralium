using NaughtyAttributes;
using UnityEditor;
using UnityEngine;

public class SeaUrchinController : EnemyController
{
    [Header("Sea Urchin Settings")]
    [SerializeField] private float angle;
#if UNITY_EDITOR
    [SerializeField] private bool showDebugGizmo;
    [ShowIf("showDebugGizmo")] [SerializeField] private Transform enemyModel;
    [ShowIf("showDebugGizmo")] [SerializeField] private Color gizmosColor = Color.red;
    [ShowIf("showDebugGizmo")] [SerializeField] private float coneLengthMultiplier = 1;
    [ShowIf("showDebugGizmo")] [SerializeField] private int coneSizes = 4;
#endif
    public override bool CompleteRequirements()
    {
        Vector3 directionToPlyaer = Player.transform.position - Character.BaseTransform.position;
        return Vector3.Angle(-Character.BaseTransform.up, directionToPlyaer) <= angle / 2;
    }
#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        if (!showDebugGizmo)
            return;
        CapsuleCollider capCollider = gameObject.GetComponentInChildren<CapsuleCollider>(true);
        float angle = this.angle * 0.5f;
        float colliderRadius = 1;
        if (capCollider != null)
            colliderRadius = capCollider.radius;
        Handles.color = gizmosColor;
        float length = Mathf.Cos(angle * Mathf.Deg2Rad) * transform.lossyScale.x * coneLengthMultiplier * colliderRadius;
        float radius = Mathf.Tan(angle * Mathf.Deg2Rad) * length;
        Vector3 coneDirection = -transform.up.normalized;
        if (enemyModel != null)
            coneDirection = -enemyModel.transform.up.normalized;
        Vector3 targetDistance = transform.position + coneDirection * length;
        Handles.DrawWireDisc(targetDistance, coneDirection, radius);
        Gizmos.color = gizmosColor;
        int coneSizes = 4 * this.coneSizes;
        float angleStep = 360.0f / coneSizes;
        for (int i = 0; i < coneSizes; i++)
        {
            float coneRadiusAngle = i * angleStep;
            Quaternion rotation = Quaternion.AngleAxis(coneRadiusAngle, coneDirection);
            Vector3 rotatedDirection = rotation * Vector3.forward;
            Gizmos.DrawLine(transform.position, targetDistance + rotatedDirection * radius);
        }
    }
#endif
}
