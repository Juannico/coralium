using UnityEngine;
using System;
[Obsolete("Use shoot behaviour instead")]
public class SpikeTrigger : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponentInParent<PlayerCharacter>() != null)
        {
            other.gameObject.GetComponentInParent<IDamageable<int, DamageTypes>>().TakeDamage(1, DamageTypes.Basic, transform.forward);
            transform.parent.gameObject.SetActive(false);
            return;
        }
        if (TryGetComponent(out ExplosiveObject explosiveObject))
        {
            explosiveObject.StartExplode();
            transform.parent.gameObject.SetActive(false);
        }
        /*else if (other.gameObject.GetComponent<DynamiteAttack>() != null)
        {
            other.gameObject.GetComponent<DynamiteAttack>().Explotar();
        }*/
    }
}
