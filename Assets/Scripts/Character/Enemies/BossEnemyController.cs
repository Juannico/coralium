using NaughtyAttributes;
using UnityEngine;

public class BossEnemyController : EnemyController
{
    [SerializeField] protected int phaseAmount = 3;
    [SerializeField] protected bool extraPhase = false;
    [ReadOnly] [SerializeField] protected int currentPhase;

    // #TODO: Delete
    public bool PlayerDetected { get; protected set; }

    public int AttackAmount { get; protected set; }
    [HideInInspector]
    public bool IsImmune
    {
        get { return Character.BaseCharacter.Data.IsImmune; }
        set
        {
            if (!value)
                Character.BaseCharacter.Data.ControlOnInmune = false;
            Character.BaseCharacter.Data.IsImmune = value;
            if (value)
                Character.BaseCharacter.Data.ControlOnInmune = true;
        }
    }

    protected override void Start()
    {
        base.Start();
        PlayerDetected = false;

        if (phaseAmount < 1)
            phaseAmount = 1;
        if (phaseAmount > EnemyAbilities.BaseAbilities.Length)
            phaseAmount = EnemyAbilities.BaseAbilities.Length;
        int starAttackAmount = 1 + EnemyAbilities.BaseAbilities.Length - phaseAmount;
        if (extraPhase)
            starAttackAmount--;
        OnDied.AddListener(DiedPhase);
        AttackAmount = Mathf.Clamp(starAttackAmount, 1, EnemyAbilities.BaseAbilities.Length);
        currentPhase = 1;
    }

    public virtual void ChangeAttackAmount()
    {
        if (Character.BaseCharacter.Data.CurrentHealth <= (Character.BaseCharacter.Data.Health / phaseAmount) * (phaseAmount - currentPhase))
        {
            currentPhase++;
            AttackAmount++;
        }
        if (AttackAmount > EnemyAbilities.BaseAbilities.Length)
            AttackAmount = EnemyAbilities.BaseAbilities.Length;
    }
    protected virtual void DiedPhase() { }
    protected override bool GetCanMove()
    {
        // if (Character.BaseCharacter.Data.IsRecievingDamage)
        //   return false;
        return base.GetCanMove();
    }

    // #TODO: Refactor player detection
    // Gets called through Unity event in the challenge prefab
    public void OnPlayerDetected()
    {
        PlayerDetected = true;
    }
}
