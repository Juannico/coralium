using UnityEngine;


public class ObjectEnemy : MonoBehaviour
{
    [SerializeField] private int damage;
    [SerializeField] private LayerData layerData;

    private void OnCollisionEnter(Collision collision)
    {
        if (!LayerUtilities.IsSameLayer(layerData.PlayerLayer, collision.gameObject.layer))
            return;
        DoDamage(collision.transform);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (!LayerUtilities.IsSameLayer(layerData.PlayerLayer, other.gameObject.layer))
            return;
        DoDamage(other.transform);
    }

    private void DoDamage(Transform other) => other.gameObject.GetComponentInParent<IDamageable<int, DamageTypes>>().TakeDamage(damage, DamageTypes.Basic, (other.transform.position- transform.position));
}
