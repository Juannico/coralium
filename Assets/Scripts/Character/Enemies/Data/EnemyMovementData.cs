using System;
using UnityEngine;
[CreateAssetMenu(fileName = "EnemyMovementData", menuName = "Coralium/Character/Enemy Movement Data")]
[Serializable]
public class EnemyMovementData : MovementData
{
    [field: SerializeField] public IdleData IdleData { get; set; }
    [field: SerializeField] public ImpulseData SpecialImpulseData { get; set; }
    [field: SerializeField] public DieData DieData { get; set; }
    [field: SerializeField] public StopData StopData { get; set; }
}
