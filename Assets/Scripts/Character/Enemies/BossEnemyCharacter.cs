using FMODUnity;
using UnityEngine;

public class BossEnemyCharacter : EnemyCharacter
{
    [SerializeField] private StudioEventEmitter damageSFX;
    [field: SerializeField] public float PlayerDamageKnockBackDuration { get; private set; } = 2;
    private BossEnemyController bossEnemyController;

    public override void Initialize()
    {
        base.Initialize();
        bossEnemyController = controller as BossEnemyController;
    }
    public override void DoDamage(int damage, DamageTypes type, Vector3 damageDirection)
    {
        base.DoDamage(damage, type, damageDirection);
        damageSFX?.Play();
        bossEnemyController.ChangeAttackAmount();
        //GetComponent<BoatEnemyAi>()?.StopAttack();
    }
}

