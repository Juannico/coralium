using System;
using UnityEngine;

[Obsolete("Use EnemyCharacter instead")]
public class EnemigoBase : MonoBehaviour, IDamageable<int, DamageTypes>
{
    [SerializeField] private CollisionHelper collisionHelper;
    public int vida;
    private void OnEnable()
    {
        if (collisionHelper != null)
            collisionHelper.TriggerEnter += OnPlayerEnter;
    }

    private void OnDisable()
    {
        if (collisionHelper != null)
            collisionHelper.TriggerEnter -= OnPlayerEnter;
    }

    private void OnPlayerEnter(Collider other)
    {
        if (other.GetComponentInParent<PlayerCharacter>() == null)
            return;
        other.gameObject.GetComponentInParent<IDamageable<int, DamageTypes>>()?.TakeDamage(1, DamageTypes.Basic, ColliderUtilities.GetContactDirection(other, transform.position));
    }
    public void IsDead()
    {
        if (vida <= 0)
            Destroy(transform.parent.gameObject);
    }

    public void TakeDamage(int damage, DamageTypes type, Vector3 damageDirection)
    {
        vida -= damage;
        IsDead();
    }

}
