using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[Obsolete("Use BossEnemyController isntead")]
[RequireComponent(typeof(BossEnemyCharacter))]
public class BoatEnemyAi : MonoBehaviour
{
    private PlayerController player;
    public BossEnemyCharacter Character { get; private set; }
    [SerializeField] private float gridAttackTime;
    [SerializeField] private float boatVelocity;
    [SerializeField] private float maxRotationSpeed;
    private GridBehaviour grid;
    private bool isGridAttacking = false;

    [SerializeField] private ExplosiveObject explosiveObjectPrefab;
    [SerializeField] private GameObject explosiveObjectStartPositionParent;
    private Transform[] explosiveObjectStartPositions;
    [SerializeField] private float explosiveObjectAttackTime;
    [SerializeField] private int explosiveObjectAmount;
    [SerializeField] private float maxDistanceToPlayer;
    [SerializeField] private float maxDistanceToCenterr;
    private List<ExplosiveObject> explosiveObjects = new List<ExplosiveObject>();

    private bool shouldAttack;
    private Vector3 velocity;

    private Coroutine gridAttackCoroutine;
    private Coroutine fireExplodeObjectCoroutine;
    [SerializeField] private FlocksHandle flocksHandle;
    private Transform flockTransform;
    private Transform targetTransform;
    private int explosivesCounter;
    private int phase = 0;
#if  UNITY_EDITOR
    [SerializeField] private bool showGuide;

#endif
    // Start is called before the first frame update
    private void Awake()
    {
        Character = GetComponent<BossEnemyCharacter>();
        Character.Initialize();
        grid = GetComponent<GridBehaviour>();
        explosiveObjectPrefab.gameObject.SetActive(false);
        for (var i = 0; i < explosiveObjectAmount; i++)
        {
            ExplosiveObject explosiveObject = Instantiate(explosiveObjectPrefab, transform.parent);
            explosiveObjects.Add(explosiveObject);
        }
        targetTransform = new GameObject().transform;
        flockTransform = targetTransform;
        explosiveObjectStartPositions = new Transform[explosiveObjectStartPositionParent.transform.childCount];
        for (var i = 0; i < explosiveObjectStartPositions.Length; i++) {
            explosiveObjectStartPositions[i] = explosiveObjectStartPositionParent.transform.GetChild(i);
        }
    }
    private void Start()
    {
        //StartAttacks();
        targetTransform.position = GetTargetTransformPosition();
    }
    private void FixedUpdate()
    {
        if (Character.BaseCharacter.Data.IsDead)
            return;
        if (!shouldAttack)
            return;
        if (isGridAttacking)
            return;
        if (Vector3.Distance(targetTransform.position, transform.position) < 5)
            return;
        MoveBoat();
        if (!ShouldChasePlayer())
        {
            AttackFlocks();
            return;
        }
        if (targetTransform != player.transform)
            targetTransform = player.transform;
    }
    private void MoveBoat()
    {
        Vector3 direction = targetTransform.position - transform.position;
        direction.y = 0;
        float multiplier = 1;
        if (transform.forward.normalized != direction.normalized) { 
            transform.forward = Vector3.SmoothDamp(transform.forward, direction.normalized, ref velocity,  1, maxRotationSpeed);
            multiplier = Vector3.Dot(transform.forward, direction.normalized);
        }
        transform.position += transform.forward * boatVelocity * Time.deltaTime * multiplier;
      
    }
    private bool ShouldChasePlayer()
    {
        bool shouldChase = Vector3.Distance(player.transform.position, transform.position) < maxDistanceToPlayer && Vector3.Distance(transform.position, transform.parent.position) < maxDistanceToCenterr;
        return shouldChase;
    }
    private void AttackFlocks()
    {
        if (targetTransform == player.transform)
            targetTransform = flockTransform;
        flockTransform.position = GetTargetTransformPosition();
    }
    private Vector3 GetTargetTransformPosition()
    {
        if (flocksHandle.CurrentFlocks.Count == 0)
            return Vector3.zero;
        Flock flock = GetFlock();
        Vector3 targetPosition = Vector3.zero;
        for (var i = 0; i < flock.AllUnit.Count; i++)
            targetPosition += flock.AllUnit[i].transform.position;
        targetPosition /= flock.AllUnit.Count;
        return targetPosition;
    }
    private Flock GetFlock()
    {
        Flock flock = flocksHandle.CurrentFlocks[0];
        for (var i = 1; i < flocksHandle.CurrentFlocks.Count; i++)
        {
            if (flocksHandle.CurrentFlocks[i].AllUnit.Count > flock.AllUnit.Count)
                flock = flocksHandle.CurrentFlocks[i];
        }
        return flock;
    }
    public void StartAttacks()
    {
        shouldAttack = true;
        phase++;
        player = GameObject.FindGameObjectWithTag(Tags.Player).GetComponent<PlayerController>();
        if (!gameObject.activeInHierarchy)
            return;
        if (phase == 2)
        {
            fireExplodeObjectCoroutine = StartCoroutine(FireExplodeObject());
            return;
        }
        gridAttackCoroutine = StartCoroutine(StartGridAttack());
    }
    private IEnumerator StartGridAttack()
    {
        yield return new WaitForSeconds(gridAttackTime);
        if(phase == 1)
            yield return new WaitForSeconds(gridAttackTime);
        isGridAttacking = true;
     //   grid.gridCoroutine = StartCoroutine(grid.MoveGrid(targetTransform.position));
    }
    public void EndGridAttack()
    {
        isGridAttacking = false;
        if (phase == 3)
        {
            fireExplodeObjectCoroutine = StartCoroutine(FireExplodeObject());
            return ;
        }
        gridAttackCoroutine = StartCoroutine(StartGridAttack());
    }
    public void StopAttack()
    {
        
        if (isGridAttacking)
        {
          //  StopCoroutine(grid.gridCoroutine);
           // StartCoroutine(grid.ReturnGrid());
        }
        if (gridAttackCoroutine != null)
            StopCoroutine(gridAttackCoroutine);
        if (fireExplodeObjectCoroutine != null)
            StopCoroutine(fireExplodeObjectCoroutine);
        for (var i = 0; i < explosiveObjects.Count; i++)
        {
            explosiveObjects[i].StopMove();
        }
        if (Character.BaseCharacter.Data.IsDead)
            return;
        Invoke("StartAttacks", 2.5f);
    }
    private IEnumerator FireExplodeObject()
    {
        Transform startPositon = explosiveObjectStartPositions[0];
        float minDistance = Vector3.Distance(explosiveObjectStartPositions[0].position, targetTransform.position);
        for (var i = 1; i < explosiveObjectStartPositions.Length; i++) {
            float distance = Vector3.Distance(explosiveObjectStartPositions[i].position, targetTransform.position);
            if ( distance > minDistance )
            {
                minDistance = distance;
                startPositon = explosiveObjectStartPositions[i];
            }
        }
        Vector3 startPoisition = startPositon.position + (startPositon.position - transform.position).normalized + UnityEngine.Random.insideUnitSphere;
        startPoisition.y = startPositon.position.y;
        Vector3 targetPosition = targetTransform.position;
        //  fireExplodeObjectCoroutine = StartCoroutine(FireExplodeObject());
        bool shouldCreateBomp = true;
        for (var i = 0; i < explosiveObjects.Count; i++)
        {
            if (!explosiveObjects[i].gameObject.activeInHierarchy)
            {
                shouldCreateBomp = false;
                explosiveObjects[i].gameObject.SetActive(true);
                explosiveObjects[i].StartToMove(transform.position, startPoisition, targetPosition);
                break;
            }
        } 
        if (shouldCreateBomp)
        {
            ExplosiveObject explosiveObject = Instantiate(explosiveObjectPrefab, transform.parent);
            explosiveObject.gameObject.SetActive(true);
            explosiveObject.StartToMove(transform.position, startPoisition, targetPosition);
            explosiveObjects.Add(explosiveObject);
        }
        if (explosivesCounter < explosiveObjectAmount)
        {
            explosivesCounter++;
            yield return new WaitForSeconds(explosiveObjectAttackTime);
            fireExplodeObjectCoroutine = StartCoroutine(FireExplodeObject());
            yield break;
        }
        yield return new WaitForSeconds(explosiveObjectAttackTime * gridAttackTime);
        if (phase == 2) {
            fireExplodeObjectCoroutine = StartCoroutine(FireExplodeObject());
            yield break; 
        }
        gridAttackCoroutine = StartCoroutine(StartGridAttack());
        explosivesCounter = 0;
    }


#if  UNITY_EDITOR
    private void OnDrawGizmos()
    {
        if (!showGuide)
            return;
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.parent.position, maxDistanceToCenterr);
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, maxDistanceToPlayer);
    }
#endif
}
