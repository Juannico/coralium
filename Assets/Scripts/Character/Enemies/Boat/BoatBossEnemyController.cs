using UnityEngine;

public class BoatBossEnemyController : BossEnemyController
{
    [SerializeField] private float amountToFishToRecieveDamage = 5;
    [SerializeField] private FlocksHandle flocksHandle;
    [SerializeField] private BaseAbilityCast abilityToRecieveDamage;
    [SerializeField] private BaseAbilityCast playerSpecialAbility;
    [SerializeField] private Flock playerSpecialAbilityFlock;
    private bool isTargetingFlocks = true;
    private PlayerController playerController;
    private GameObject flock;
    protected override void Start()
    {
        base.Start();
        flock = new GameObject("BoatTargetFlock");
        playerController = GameObject.FindGameObjectWithTag(Tags.Player).GetComponent<PlayerController>();

    }
    /*
    private void Start()
    {
        /// TODO: Working on change FlockPlayerControl to AbilitySystem
        
        ///playerSpecialAbility = Instantiate(playerSpecialAbility);
        ///playerSpecialAbility.Initialize(Player.Character);
        ///playerSpecialAbilityFlock.gameObject.SetActive(false);
        
    }*/
    protected override void Update()
    {
        base.Update();
        if (isTargetingFlocks)
            flock.transform.position = GetTargetTransformPosition();
    }
    public override void SetTarget(bool shoulTargetPlayer = true, bool checkOnTrigger = false)
    {
        if (checkOnTrigger)
            return;
        isTargetingFlocks = !shoulTargetPlayer;
        if (shoulTargetPlayer)
        {
            base.SetTarget(shoulTargetPlayer);
            return;
        }
        stateSharedData.CurrentFollowObject = flock;
    }
    private Vector3 GetTargetTransformPosition()
    {
        if (flocksHandle == null)
            return Vector3.zero;
        if (flocksHandle.CurrentFlocks.Count == 0)
            return Vector3.zero;
        Flock flock = GetFlock();
        Vector3 targetPosition = Vector3.zero;
        for (var i = 0; i < flock.AllUnit.Count; i++)
            targetPosition += flock.AllUnit[i].transform.position;
        targetPosition /= flock.AllUnit.Count;
        return targetPosition;
    }
    private Flock GetFlock()
    {
        Flock flock = flocksHandle.CurrentFlocks[0];
        for (var i = 1; i < flocksHandle.CurrentFlocks.Count; i++)
        {
            if (flocksHandle.CurrentFlocks[i].AllUnit.Count > flock.AllUnit.Count)
                flock = flocksHandle.CurrentFlocks[i];
        }
        return flock;
    }
    public override bool CompleteRequirements()
    {
        return true;
        /// TODO: Working on change FlockPlayerControl to AbilitySystem
        /*
        if (playerController == null)
            playerController = GameObject.FindGameObjectWithTag(Tags.Player).GetComponent<PlayerController>();
        if (WrongAbility())
            return false;

        bool isComplete = playerController.FlockPlayerControl?.GetAmountOfFish() >= amountToFishToRecieveDamage;
        if (isComplete)
            playerController.FlockPlayerControl?.RemoveAllUnits();
        return isComplete;*/
    }

    protected override void DiedPhase()
    {
        /// TODO: Check died phase behaviour
        //LastBoatAbility();
        /// TODO: Working on change FlockPlayerControl to AbilitySystem
        //PlayerAbility();
        //SetFlock();
    }


    private void LastBoatAbility()
    {
        BaseAbilityCast ability = EnemyAbilities.Abilities[EnemyAbilities.BaseAbilities.Length - 1];
        Character.AbilitiesHandle.CanDisrupAbilities = false;
        ability.EndCastAbility += () =>
        {
            if (Character.AbilitiesHandle.AbilityBehaviours.Count > 1)
                return;
            playerSpecialAbility.CurrentBehaviour.Disrupt();
            gameObject.SetActive(false);
        };
        ability.Load();
        ability.Loaded();
        ability.Cast(true);
    }
    private void PlayerAbility()
    {
        CoroutineHandler.ExecuteActionAfter(() =>
        {
            playerSpecialAbility.Load();
            playerSpecialAbility.Loaded();
            playerSpecialAbility.Cast(true);
        }, 1.1f, this);
    }
    private void SetFlock()
    {
        /// TODO: Working on change FlockPlayerControl to AbilitySystem
        /*
        FlockUnit[] units = playerSpecialAbilityFlock.AllUnit.ToArray();
        for (var i = 0; i < units.Length; i++)
        {
            units[i].gameObject.SetActive(true);
            units[i].AssingFlock(Player.FlockPlayerControl.Flock);
        }*/
    }

    private bool WrongAbility()
    {
        BaseAbilityBehaviour[] currentActivePlayerAbilities = playerController.Character.AbilitiesHandle.AbilityBehaviours.ToArray();
        for (int i = 0; i < currentActivePlayerAbilities.Length; i++)
        {
            if (currentActivePlayerAbilities[i].Ability.GetType() == abilityToRecieveDamage.GetType())
                return false;
        }
        return true;
    }
}
