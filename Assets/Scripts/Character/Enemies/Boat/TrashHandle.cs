using System.Collections.Generic;
using UnityEngine;

public class TrashHandle : MonoBehaviour
{
    [SerializeField] private GameObject trashGroup;
    [SerializeField] private Transform gridTransform;
    [SerializeField] private int maxAmountOfTrash;
    private TrashObject[] trashObjects;
    private List<TrashObject> trashObjectsPool = new List<TrashObject>();
    private List<TrashObject> trashObjectsInGrid = new List<TrashObject>();
    private int amountOfTrash;
    void Awake()
    {
        trashObjects = trashGroup.GetComponentsInChildren<TrashObject>(true);
        trashGroup.transform.position = Vector3.down * 100;
        for (var i = 0; i < trashObjects.Length; i++)
        {
            trashObjects[i].gameObject.SetActive(false);
            CreateTrash(i);
        }
        for (var i = 0; i < trashObjects.Length * 3; i++)
            CreateTrash(i % trashObjects.Length);
    }
    public void GenerateTrash(float time)
    {
        CoroutineHandler.ExecuteActionAfter(() =>
        {
            int randomIndex = Random.Range(0, trashObjectsInGrid.Count - 1);
            trashObjectsInGrid[randomIndex].transform.parent = null;
            trashObjectsInGrid[randomIndex].CanScared = true;
            trashObjectsInGrid.RemoveAt(randomIndex);
            SetAmountOftrash(true);
        }, time, this);
    }
    public void SetAmountOftrash(bool add) => amountOfTrash = Mathf.Clamp(amountOfTrash + (add ? 1 : -1), 0, maxAmountOfTrash);
    public void SetTrashObjectInGrid()
    {

        if (amountOfTrash == maxAmountOfTrash)
            return;
        int objectsToAdd = Mathf.Min(3, maxAmountOfTrash - amountOfTrash);
        for (int i = 0; i < objectsToAdd; i++)
        {
            if (trashObjectsPool.Count < 1)
                CreateTrash(Random.Range(0, trashObjects.Length - 1));
            int randomIndex = Random.Range(0, trashObjectsPool.Count);
            TrashObject trashObject = trashObjectsPool[randomIndex];
            trashObject.transform.parent = gridTransform;
            trashObject.transform.localPosition = Vector3.zero + Vector3.up * Random.Range(-1.5f, 1.5f) + Vector3.right * Random.Range(-1.5f, 1.5f);
            trashObject.CanScared = false;
            trashObjectsInGrid.Add(trashObject);
            trashObjectsPool.RemoveAt(randomIndex);
            SetAmountOftrash(true);
        }
    }
    private void CreateTrash(int index)
    {
        TrashObject trashObject = Instantiate(trashObjects[index]);
        trashObject.transform.parent = trashGroup.transform;
        trashObject.SetTrashHandle(this);
        trashObjectsPool.Add(trashObject);
    }
    public void ActiveTrash(TrashObject trashObject)
    {
        trashObjectsPool.Add(trashObject);
    }
}
