using UnityEngine;

[RequireComponent(typeof(EnemyCharacter))]
public class StaticEnemy : MonoBehaviour
{
    private EnemyCharacter character;
    private void Awake()
    {
        character = GetComponent<EnemyCharacter>();
        character.Initialize();
    }
}
