using UnityEditor;
using UnityEngine;

public class InspectorHideAllChildren
{
#if UNITY_EDITOR
    [MenuItem("GameObject/Utilities/Hide/All", false, 0)]
    private static void HideAll()
    {
        HideChildern();
        HideParent();
    }
    [MenuItem("GameObject/Utilities/Hide/Parent",false,0)]
    private static void HideParent() {
        GameObject[] selectedObjects = Selection.gameObjects;
        for (int i = 0; i < selectedObjects.Length; i++)
            selectedObjects[i].SetActive(false);
        // Selection.activeGameObject.SetActive(false);
    }
    [MenuItem("GameObject/Utilities/Hide/Children", false, 0)]
    private static void HideChildern()
    {
        GameObject[] selectedObjects = Selection.gameObjects;

        for (int i = 0; i < selectedObjects.Length; i++)
        {
            Transform[] childSceneGo = selectedObjects[i].GetComponentsInChildren<Transform>(true);
            for (var j = 1; j < childSceneGo.Length; j++)
            {

                childSceneGo[j].gameObject.SetActive(false);
            }
        }
    }
    [MenuItem("GameObject/Utilities/Unhide/All", false, 0)]
    private static void UnhideAll()
    {
        UnhideChildern();
        UnhideParent();
        GameObject go = new GameObject();
        Object.DestroyImmediate(go);    
    }
    [MenuItem("GameObject/Utilities/Unhide/Parent", false, 0)]
    private static void UnhideParent()
    {
        GameObject[] selectedObjects = Selection.gameObjects;
        for (int i = 0; i < selectedObjects.Length; i++)
            selectedObjects[i].SetActive(true);
    }
    [MenuItem("GameObject/Utilities/Unhide/Children", false, 0)]
    private static void UnhideChildern()
    {
        GameObject[] selectedObjects = Selection.gameObjects;

        for (int i = 0; i < selectedObjects.Length; i++)
        {
            Transform[] childSceneGo = selectedObjects[i].GetComponentsInChildren<Transform>(true);
            for (var j = 0; j < childSceneGo.Length; j++)
                childSceneGo[j].gameObject.SetActive(true);
        }
    }
#endif
}
