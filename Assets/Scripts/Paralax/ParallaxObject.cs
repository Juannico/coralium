using UnityEngine;

public class ParallaxObject : MonoBehaviour
{
    [SerializeField] private float velocity = 2;
    private ParallaxController parallaxController;

    private void Start()
    {
        SetCallback(true);
    }
    private void OnEnable()
    {
        SetCallback(true);
    }
    private void OnDisable()
    {
        SetCallback(false);
    }
    private void OnDestroy()
    {
        SetCallback(false);
    }
    private void Move(Vector2 valueToMove)
    {
        Vector3 currentPosition = transform.position;
        currentPosition.x += velocity * valueToMove.x * Time.fixedDeltaTime;
        transform.position = currentPosition;
    }
    private void SetCallback(bool add)
    {
        if (parallaxController == null)
            parallaxController = FindObjectOfType<ParallaxController>();
        if (parallaxController == null)
            return;
        if (add)
            parallaxController.OnMove += Move;
        else
            parallaxController.OnMove -= Move;
    }
}
