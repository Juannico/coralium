using UnityEngine;

public class ParallaxController : MonoBehaviour
{
    public delegate void OnAction(Vector2 floatValue);
    public OnAction OnMove;

    private Vector2 lastPosition;
    private void Awake()
    {
        lastPosition = transform.position;
    }

    private void FixedUpdate()
    {
        if ((Vector2)transform.position == lastPosition)
            return;
        OnMove?.Invoke((Vector2)transform.position - lastPosition);
        lastPosition = transform.position;
    }
}
