using NaughtyAttributes;
using System;
using UnityEngine.InputSystem;
[Serializable]
public class InputReferenceData
{
    public int InputPriority = 0;
    public bool Delay = true;
    [AllowNesting][ShowIf("Delay")]public float DelayTime =  0.1f;
    public InputActionReference InputActionReference;

    public Action InputAdded;
    public Action InputRemoved;
    public Action InputActioned;

    public Action CurrentInputAdded;
    public Action CurrentInputActioned;
    public Action CurrentInputRemoved;
    public InputActionInteractionData InputActionInteractionData;
    private Action<InputAction.CallbackContext> started;
    public Action<InputAction.CallbackContext> Started
    {
        get => started;
        set
        {
            InputActionReference.action.started -= started;
            if (value == null)
                return;
            started = value;
            InputActionReference.action.started += started; 
        }
    }
    private Action<InputAction.CallbackContext> performed;
    public Action<InputAction.CallbackContext> Performed
    {
        get => performed;
        set
        {
            InputActionReference.action.performed -= performed;
            if (value == null)
                return;
            performed = value;
            InputActionReference.action.performed += performed;
        }
    }
    private Action<InputAction.CallbackContext> canceled;
    public Action<InputAction.CallbackContext> Canceled
    {
        get => canceled;
        set
        {
            InputActionReference.action.canceled -= canceled;
            if (value == null)
                return;
            canceled = value;
            InputActionReference.action.canceled += canceled;
        }
    }

}
