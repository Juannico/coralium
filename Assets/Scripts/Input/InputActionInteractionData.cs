using System;
using System.Globalization;
using UnityEngine;
using UnityEngine.InputSystem;
[Serializable]
public class InputActionInteractionData
{
    public InputActionReference InputActionReference;
    public InputAction action { get => InputActionReference.action; }

    public bool IsHold
    {
        get
        {
            return HasInteraction("duration");
        }
    }

    private bool HasInteraction(string interactionName)
    {
        try
        {
            string interaction = InputActionReference.action.interactions;
            string valueString = interaction.Split($"{interactionName}")[1];
            return true;
        }
        catch (Exception)
        {
        }
        return false;
    }
    private float holdDuration = -1;
    public float HoldDuration
    {
        get
        {
            if (holdDuration < 0)
                holdDuration = GetInteractionValue("duration");
            return holdDuration;
        }
    }
    private float GetInteractionValue(string interactionName)
    {
        float duration = 0;
        try
        {
            string interaction = InputActionReference.action.interactions;
            string valueString = interaction.Split($"{interactionName}=")[1].Split(')')[0];
            duration = float.Parse(valueString, NumberStyles.Float, CultureInfo.InvariantCulture);

        }
        catch (Exception e)
        {
            Debug.LogException(e);
        }
        return duration;
    }
}
