using System;
using System.Collections.Generic;
using UnityEngine.InputSystem;

[Serializable]
public class InputReference 
{
    public InputReferenceData InputReferenceData;
    public List<CombineInputReferenceData> CombineInputetReferenceData = new List<CombineInputReferenceData>();
    public InputAction InputAction
    {
        get => InputReferenceData.InputActionReference.action;
        
    }
    public void ResetInputAction()
    {
        InputReferenceData.Started = null;
        InputReferenceData.Performed = null;
        InputReferenceData.Canceled = null;
    }
}
