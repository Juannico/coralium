using System;

[Serializable]
public class CombineInputReferenceData
{
    public InputReferenceData InputReferenceDataToCombine;
    public Action ActionToCombineOnInputAdded;
    public Action ActionToCombineOnInputRemoved;
    public Action ActionToCombineOnInputActioned;



}
