using System.Collections.Generic;
using UnityEngine;

public class InputHandle : MonoBehaviour
{
    [HideInInspector] public List<InputReference> InputReferences = new List<InputReference>();
    [SerializeField] private List<InputReference> inputReferenceStack = new List<InputReference>();

    public void OnAddInputStack(InputReference inputReference, bool DoAction = true)
    {
        inputReferenceStack.Add(inputReference);
        SetActions(inputReference);
        if (DoAction)
            inputReference.InputReferenceData.CurrentInputAdded?.Invoke();
    }
    public void DoInputAction(InputReference inputReference)
    {
        if (inputReference.InputReferenceData.CurrentInputActioned == null)
            return;
        if (!inputReference.InputReferenceData.Delay)
        {
            inputReference.InputReferenceData.CurrentInputActioned?.Invoke();
            return;
        }
        CoroutineHandler.ExecuteActionAfter(() =>
        {
            inputReference.InputReferenceData.CurrentInputActioned?.Invoke();
        }, inputReference.InputReferenceData.DelayTime, this);
    }
    public void OnRemoveInputStack(InputReference inputReference, bool DoAction = true)
    {
        if (!inputReference.InputReferenceData.Delay)
        {
            if (DoAction)
                inputReference.InputReferenceData.CurrentInputRemoved?.Invoke();
            if (inputReferenceStack.Contains(inputReference))
                inputReferenceStack.Remove(inputReference);
            return;
        }
        
        if (DoAction)
            CoroutineHandler.ExecuteActionAfter(() => inputReference.InputReferenceData.CurrentInputRemoved?.Invoke(), inputReference.InputReferenceData.DelayTime, this);
        if (inputReferenceStack.Contains(inputReference))
            CoroutineHandler.ExecuteActionAfter(() => inputReferenceStack.Remove(inputReference), inputReference.InputReferenceData.DelayTime, this);
    }

    private void SetActions(InputReference inputReference)
    {
        inputReference.InputReferenceData.CurrentInputAdded = inputReference.InputReferenceData.InputAdded;
        inputReference.InputReferenceData.CurrentInputActioned = inputReference.InputReferenceData.InputActioned;
        inputReference.InputReferenceData.CurrentInputRemoved = inputReference.InputReferenceData.InputRemoved;
        CombineInputReferenceData combineInputReferenceData = GetCombineAction(inputReference);
        if (combineInputReferenceData != null)
            SetCombineActions(inputReference, combineInputReferenceData);
    }
    private CombineInputReferenceData GetCombineAction(InputReference inputReferenceData)
    {
        for (int i = 0; i < inputReferenceData.CombineInputetReferenceData.Count; i++)
        {
            for (int j = 0; j < inputReferenceStack.Count; j++)
            {
                if (inputReferenceData.CombineInputetReferenceData[i].InputReferenceDataToCombine != inputReferenceStack[j].InputReferenceData)
                    continue;
                return inputReferenceData.CombineInputetReferenceData[i];
            }
        }
        return null;
    }
    private void SetCombineActions(InputReference inputReference, CombineInputReferenceData combineInputReferenceData)
    {
        inputReference.InputReferenceData.CurrentInputAdded = combineInputReferenceData.ActionToCombineOnInputAdded;
        if (inputReference.InputReferenceData.InputPriority >= combineInputReferenceData.InputReferenceDataToCombine.InputPriority)
        {
            inputReference.InputReferenceData.CurrentInputActioned = combineInputReferenceData.ActionToCombineOnInputActioned;
            inputReference.InputReferenceData.CurrentInputRemoved = combineInputReferenceData.ActionToCombineOnInputRemoved;
            combineInputReferenceData.InputReferenceDataToCombine.CurrentInputActioned = null;
            combineInputReferenceData.InputReferenceDataToCombine.CurrentInputRemoved = null;
            return;
        }
        inputReference.InputReferenceData.CurrentInputActioned = null;
        inputReference.InputReferenceData.CurrentInputRemoved = null;
        combineInputReferenceData.InputReferenceDataToCombine.CurrentInputActioned = combineInputReferenceData.ActionToCombineOnInputActioned;
        combineInputReferenceData.InputReferenceDataToCombine.CurrentInputRemoved = combineInputReferenceData.ActionToCombineOnInputRemoved;
    }
}


