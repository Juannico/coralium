using System;
using System.Collections.Generic;
using Unity.Collections;
using Unity.Jobs;
using Unity.Mathematics;
using UnityEditor;
using UnityEngine;
using UnityEngine.Jobs;

public class Flock : TickFixedUpdate
{
    [Header("FLock Settings")]
    [SerializeField] private bool drawGuide = true;
    [SerializeField] public FlocksHandle FlocksHandle;
    [field: SerializeField] public bool canChangeFlock { get; private set; } = true;
    [HideInInspector] public bool useHightligthColor;
    [HideInInspector] public float hightligthPower;
    [HideInInspector] [ColorUsage(true, true)] public Color hightligthColor;
    private Color baseColor = Color.black * -10;
    [field: Header("Spawn Setup")]
    [field: SerializeField] public int FishAmount { get; private set; }
    [SerializeField] private FlockUnit[] fishPrefabs;
    private FlockUnit currentFishPrefab;
    [SerializeField] private Vector3 spawnLimit;
    [HideInInspector] public List<FlockUnit> AllUnit;
    private List<Transform> allunitTransform = new List<Transform>();
    [SerializeField] private Vector3 spawnDirection;
    [SerializeField] private float spawnTime;
    private float spawnWeight = 10;
    private bool spawning = true;
    private float spawnTimer = 0;
    [field: SerializeField] public bool followObject { get; set; }
    [HideInInspector] public Transform TargetToFollow;
    [HideInInspector] public TargetDestinations targetDestinations;
    [HideInInspector] public float followDistance;
    [HideInInspector] public float followWeight;
    [HideInInspector] public float followMaxSpeed;
    [HideInInspector] public bool isClosestToTarget;
    [HideInInspector] public bool isRandomOffset = false;
    [field: SerializeField] public bool randomValues { get; private set; }
    [HideInInspector] public float minSpeed;
    [HideInInspector] public float maxSpeed;
    [HideInInspector] public float cohesionDistance;
    [HideInInspector] public float cohesionWeight;
    [HideInInspector] public float avoidanceDistance;
    [HideInInspector] public float avoidanceWeight;
    [HideInInspector] public float aligementDistance;
    [HideInInspector] public float aligementWeight;
    [HideInInspector] public float circularWeight;
    [HideInInspector] public float circularRadius;
    [HideInInspector] public float circularArea;
    [HideInInspector] public float circularHeight;
    [HideInInspector] public float limitDistance;
    [HideInInspector] public float limitWeight;
    [HideInInspector] public float obstacleDistance;
    [HideInInspector] public float obstacleWeight;

    public bool shouldFollow;
    public float speedMultiplier = 1;
    [HideInInspector] public Action<FlockUnit, bool> ChangeFlockUnitAction;



    TransformAccessArray transformAccessArray;

    NativeArray<float3> moveVec;
    NativeArray<float3> unitForwardDirections;
    NativeArray<float3> unitCurrentVelocity;
    NativeArray<float3> unitPosition;
    NativeArray<float3> obstacleVecs;
    NativeArray<float> allUnitsSpeeds;
    NativeArray<bool> unitArriveDestiny;
    NativeArray<float3> targetPosition;
    NativeArray<float3> targetForwad;


    [SerializeField] [Range(0, 1)] private float factorValueToDeactive = 0.845f;

    private void Awake()
    {
        CreateFlock();
    }
    protected override void Start()
    {
        base.Start();
        shouldFollow = followObject;
        if (!followObject)
            isClosestToTarget = false;
        offset = -limitDistance;
    }
    protected override bool ContinueCondition() => AllUnit.Count > 0;
    protected override void SelfFixedUpdate()
    {
        if (spawning)
            SpawnHandle();
        transformAccessArray = new TransformAccessArray(allunitTransform.ToArray());
        moveVec = new NativeArray<float3>(AllUnit.Count, Allocator.TempJob);
        unitForwardDirections = new NativeArray<float3>(AllUnit.Count, Allocator.TempJob);
        unitCurrentVelocity = new NativeArray<float3>(AllUnit.Count, Allocator.TempJob);
        unitPosition = new NativeArray<float3>(AllUnit.Count, Allocator.TempJob);
        obstacleVecs = new NativeArray<float3>(AllUnit.Count, Allocator.TempJob);
        allUnitsSpeeds = new NativeArray<float>(AllUnit.Count, Allocator.TempJob);
        JobHandle moveJobHandle = MoveJob(GetMoveJobHandle());
        moveJobHandle.Complete();
        for (int i = 0; i < AllUnit.Count; i++)
        {
            AllUnit[i].CurrenVelocity = unitCurrentVelocity[i];
            AllUnit[i].SetSpeed(allUnitsSpeeds[i], speedMultiplier);

            if (ShoulDoFollowBehaviur())
            {
                if (unitArriveDestiny[i] && targetDestinations != null)
                    AllUnit[i].TargetDestination = targetDestinations.SetNewDestination(AllUnit[i].TargetDestination);
            }
        }
        transformAccessArray.Dispose();
        moveVec.Dispose();
        unitForwardDirections.Dispose();
        unitCurrentVelocity.Dispose();
        unitPosition.Dispose();
        obstacleVecs.Dispose();
        allUnitsSpeeds.Dispose();
        if (ShoulDoFollowBehaviur())
        {
            unitArriveDestiny.Dispose();
            targetPosition.Dispose();
            targetForwad.Dispose();
        }
    }
    private JobHandle GetMoveJobHandle()
    {
        JobHandle GetBehaviurJobHandle = GetBehaviurJob();

        if (ShoulDoFollowBehaviur())
        {
            JobHandle addFolloeVecJobHandle = AddFollowJob(GetBehaviurJobHandle);
            return MoveJob(addFolloeVecJobHandle);
        }
        return MoveJob(GetBehaviurJobHandle);

    }

    private JobHandle GetBehaviurJob()
    {
        for (int i = 0; i < AllUnit.Count; i++)
        {
            moveVec[i] = float3.zero;
            unitForwardDirections[i] = (float3)AllUnit[i].transform.forward;
            unitCurrentVelocity[i] = AllUnit[i].CurrenVelocity;
            unitPosition[i] = (float3)AllUnit[i].transform.position;
            obstacleVecs[i] = AllUnit[i].CalculateObstacleVec();
            allUnitsSpeeds[i] = AllUnit[i].Speed;
        }
        GetBehaviurVecJob getBehaviurVecJob = new GetBehaviurVecJob()
        {
            unitMoveVec = moveVec,
            unitForwardDirections = unitForwardDirections,
            unitCurrentVelocity = unitCurrentVelocity,
            unitPosition = unitPosition,
            allUnitsSpeeds = allUnitsSpeeds,
            cohesionDistance = cohesionDistance,
            avoidanceDistance = avoidanceDistance,
            aligementDistance = aligementDistance,
            limitDistance = limitDistance,
            obstacleDistance = obstacleDistance,
            cohesionWeight = cohesionWeight,
            avoidanceWeight = avoidanceWeight,
            aligementWeight = aligementWeight,
            limitWeight = limitWeight,
            obstacleWeight = obstacleWeight,
            fovAngle = currentFishPrefab.FOVAngle,
            minSpeed = minSpeed,
            maxSpeed = maxSpeed,
            flockPosition = transform.position,
            deltaTime = deltaTime,
            playerPosition = playerTransform == null ? transform.position : playerTransform.position,
            spawnDirection = spawnDirection.normalized * spawnWeight,
            shouldFollow = shouldFollow,
            isClosestToTarget = isClosestToTarget,
        };
        return getBehaviurVecJob.Schedule(AllUnit.Count, 5);
    }
    private JobHandle AddFollowJob(JobHandle getBehaviurVecJobHandle)
    {
        targetPosition = new NativeArray<float3>(AllUnit.Count, Allocator.TempJob);
        targetForwad = new NativeArray<float3>(AllUnit.Count, Allocator.TempJob);
        unitArriveDestiny = new NativeArray<bool>(AllUnit.Count, Allocator.TempJob);
        for (int i = 0; i < AllUnit.Count; i++)
        {
            if (isRandomOffset && followObject && AllUnit[i].IsRandomTargetOffset)
            {
                Vector3 sideOffset = TargetToFollow.transform.TransformPoint((Vector3.right * UnityEngine.Random.Range(-0.5f, 0.5f)) * 3.75f) - TargetToFollow.transform.position;
                sideOffset += TargetToFollow.transform.TransformPoint((Vector3.forward * UnityEngine.Random.Range(-0.5f, 0.5f)) * 3.75f) - TargetToFollow.transform.position;
                AllUnit[i].OffsetTarget = sideOffset;
                AllUnit[i].IsRandomTargetOffset = false;
            }
            unitArriveDestiny[i] = false;
            targetPosition[i] = AllUnit[i].OffsetTarget + AllUnit[i].TargetDestination.position;
            targetForwad[i] = targetPosition[i] - math.normalize((float3)AllUnit[i].transform.position);
        }
        AddFollowVecJob addFollowVecJob = new AddFollowVecJob()
        {
            unitMoveVec = moveVec,
            unitArriveDestiny = unitArriveDestiny,
            followTarget = targetPosition,
            followDirection = targetForwad,
            unitPosition = unitPosition,
            allUnitsSpeeds = allUnitsSpeeds,
            followDistance = followDistance,
            followWeight = followWeight,
            followMaxSpeed = followMaxSpeed,
            isClosestToTarget = isClosestToTarget,
            shouldFollow = shouldFollow,
            deltaTime = deltaTime,
        };
        return addFollowVecJob.Schedule(AllUnit.Count, 5, getBehaviurVecJobHandle);
    }
    private JobHandle MoveJob(JobHandle jobHandleToSchedule)
    {
        MoveTransformJob moveJob = new MoveTransformJob()
        {
            unitMoveVec = moveVec,
            unitForwardDirections = unitForwardDirections,
            unitCurrentVelocity = unitCurrentVelocity,
            unitSpeed = allUnitsSpeeds,
            obstacleVecs = obstacleVecs,
            smoothDamp = currentFishPrefab.SmoothDamp,
            deltaTime = deltaTime,
            speedMultiplier = speedMultiplier,
        };
        return moveJob.Schedule(transformAccessArray, jobHandleToSchedule);
    }

    private bool ShoulDoFollowBehaviur() => (shouldFollow || isClosestToTarget);

    public void CreateFlock()
    {
        if (randomValues)
            CreateAttributesvalues();
        // Instancia x cantidad de objetos 
        for (int i = 0; i < FishAmount; i++)
        {
            GetPrefab();
            var randompositon = MathUtilities.RandomVector(-Vector3.one, Vector3.one);
            randompositon = new Vector3(randompositon.x * (spawnLimit.x - 0.5f), randompositon.y * (spawnLimit.y - 0.5f), randompositon.z * (spawnLimit.z - 0.5f));
            var spawnposition = transform.position + randompositon;
            var rotation = new Vector3(0, UnityEngine.Random.Range(0, 359), 0);
            FlockUnit unit = Instantiate(currentFishPrefab, spawnposition, Quaternion.Euler(rotation), transform);
            unit.name = $"{gameObject.name}_{i}_{currentFishPrefab.name}";
            unit.AssingFlock(this);
            unit.InitilazedSpeed(UnityEngine.Random.Range(minSpeed, maxSpeed));
            unit.InitializedSpeedShaderController();
            unit.transform.parent = null;
        }
        spawning = spawnTime > 0f;
        if (spawning)
            return;
        spawnWeight = 0;
        spawnDirection = Vector3.zero;
    }

    private void SpawnHandle()
    {
        spawnTimer += Time.fixedDeltaTime;
        if (spawnTimer <= spawnTime)
            return;
        spawnWeight = 0;
        spawnDirection = Vector3.zero;
        spawning = false;
    }

    public void AddFlockUnit(FlockUnit flockUnit)
    {
        AllUnit.Add(flockUnit);
        allunitTransform.Add(flockUnit.transform);
        if (AllUnit.Count > 0 && !gameObject.activeInHierarchy)
        {
            gameObject.SetActive(true);
            FlocksHandle?.AddFlock(this);
        }
        ChangeFlockUnitAction?.Invoke(flockUnit, true);
        if (useHightligthColor)
        {
            flockUnit.material.SetColor("_HightligthColor", hightligthColor);
            flockUnit.material.SetFloat("_HighligthPower", hightligthPower);
        }
    }
    public void RemoveFlockUnit(FlockUnit flockUnit, bool removeOfHandleCount = false)
    {
        AllUnit.Remove(flockUnit);
        allunitTransform.Remove(flockUnit.transform);
        if (removeOfHandleCount)
            FlocksHandle?.RemoveFish();
        ChangeFlockUnitAction?.Invoke(flockUnit, false);
        if (useHightligthColor)
        {
            flockUnit.material.SetColor("_HightligthColor", baseColor);
            flockUnit.material.SetFloat("_HighligthPower", 0);
        }
        if (AllUnit.Count > 0)
            return;
        gameObject.SetActive(false);
        FlocksHandle?.RemoveFlock(this);
    }
    private void CreateAttributesvalues()
    {
        FishAmount = UnityEngine.Random.Range(40, 60);
        minSpeed = UnityEngine.Random.Range(1, 3.2f);
        maxSpeed = UnityEngine.Random.Range(1.2f, 3) + minSpeed;
        cohesionDistance = UnityEngine.Random.Range(1, 4.8f);
        avoidanceDistance = UnityEngine.Random.Range(1.2f, 5.3f);
        aligementDistance = UnityEngine.Random.Range(1, 5);
        limitDistance = UnityEngine.Random.Range(30, 80);
        cohesionWeight = UnityEngine.Random.Range(2, 5.8f);
        avoidanceWeight = UnityEngine.Random.Range(2, 6);
        aligementWeight = UnityEngine.Random.Range(2, 6.1f);
        limitWeight = UnityEngine.Random.Range(5, 8);

    }
    private void GetPrefab()
    {
        int index = UnityEngine.Random.Range(0, fishPrefabs.Length);
        currentFishPrefab = fishPrefabs[index];
    }
    private struct GetBehaviurVecJob : IJobParallelFor
    {
        [NativeDisableParallelForRestriction] public NativeArray<float3> unitMoveVec;
        [NativeDisableParallelForRestriction] public NativeArray<float3> unitForwardDirections;
        [NativeDisableParallelForRestriction] public NativeArray<float3> unitCurrentVelocity;
        [NativeDisableParallelForRestriction] public NativeArray<float3> unitPosition;
        [NativeDisableParallelForRestriction] public NativeArray<float> allUnitsSpeeds;

        public float3 playerPosition;

        public float3 flockPosition;
        public float cohesionDistance;
        public float avoidanceDistance;
        public float aligementDistance;
        public float limitDistance;
        public float obstacleDistance;
        public float cohesionWeight;
        public float avoidanceWeight;
        public float aligementWeight;

        public float limitWeight;
        public float obstacleWeight;
        public float fovAngle;
        public float minSpeed;
        public float maxSpeed;
        public float deltaTime;

        public float3 spawnDirection;

        public bool isClosestToTarget;
        public bool shouldFollow;
        public void Execute(int index)
        {
            float3 cohesionVec = float3.zero;
            int cohesionNeighbourdInFOV = 0;
            float3 avoidanceVec = float3.zero;
            int avoidanceNeighbourdInFOV = 0;
            float3 aligementVec = float3.zero;
            int aligementNeighbourdInFOV = 0;

            float speed = 0f;
            int amountOfNeightboards = 0;
            for (int i = 0; i < unitPosition.Length; i++)
            {
                float3 currentUnitPosition = unitPosition[index];
                float3 currentNeighbourPosition = unitPosition[i];
                float3 currentNeighbourDirection = unitForwardDirections[i];

                // Add "negithbords " If they are in the range of each behaviour
                if (math.all(currentUnitPosition != currentNeighbourPosition))
                {
                    float currentDistanceToNeighbourSqr = math.lengthsq(currentUnitPosition - currentNeighbourPosition);
                    if (currentDistanceToNeighbourSqr < cohesionDistance * cohesionDistance)
                    {
                        speed += allUnitsSpeeds[i];
                        if (IsInFOV(unitForwardDirections[index], unitPosition[index], currentNeighbourPosition, fovAngle) && math.all(currentNeighbourPosition != float3.zero))
                        {
                            cohesionNeighbourdInFOV++;
                            cohesionVec += currentNeighbourPosition;
                        }
                    }
                    if (currentDistanceToNeighbourSqr < avoidanceDistance * avoidanceDistance)
                    {
                        if (IsInFOV(unitForwardDirections[index], unitPosition[index], currentNeighbourPosition, fovAngle) && math.all(currentNeighbourPosition != float3.zero))
                        {
                            avoidanceNeighbourdInFOV++;
                            avoidanceVec += unitPosition[index] - currentNeighbourPosition;
                        }
                    }
                    if (currentDistanceToNeighbourSqr < aligementDistance * aligementDistance)
                    {
                        if (IsInFOV(unitForwardDirections[index], unitPosition[index], currentNeighbourPosition, fovAngle) && math.all(currentNeighbourPosition != float3.zero))
                        {
                            aligementNeighbourdInFOV++;
                            aligementVec += math.normalize(currentNeighbourDirection);
                        }
                    }
                }
            }
            if (amountOfNeightboards > 0)
                speed /= amountOfNeightboards;
            speed = math.clamp(speed, minSpeed, maxSpeed);
            speed *= 0.5f;

            if (math.all(spawnDirection != float3.zero))
                speed *= 2.5f;
            if (cohesionNeighbourdInFOV > 0)
            {
                cohesionVec /= cohesionNeighbourdInFOV;
                cohesionVec -= unitPosition[index];
                cohesionVec = math.normalize(cohesionVec) * cohesionWeight;
            }
            if (avoidanceNeighbourdInFOV > 0)
            {
                avoidanceVec /= avoidanceNeighbourdInFOV;
                avoidanceVec = math.normalize(avoidanceVec) * avoidanceWeight;
            }
            if (aligementNeighbourdInFOV > 0)
            {
                aligementVec /= aligementNeighbourdInFOV;
                aligementVec /= aligementNeighbourdInFOV;
                aligementVec = math.normalize(aligementVec) * aligementWeight;
            }
            float3 limitVec = float3.zero;
            float3 offsetToCenter = flockPosition - unitPosition[index];
            float distanceToCenter = math.length(offsetToCenter);
            if (distanceToCenter >= limitDistance * 0.9f)
            {
                limitVec = math.normalize(offsetToCenter);
                float speedMutliplier = distanceToCenter - (limitDistance * 0.9f);
                speedMutliplier = math.pow(speedMutliplier, 0.5f);
                speed *= math.clamp(speedMutliplier, 1, 2);
            }
            limitVec *= limitWeight;
            unitMoveVec[index] = cohesionVec + avoidanceVec + aligementVec + limitVec + spawnDirection;
            allUnitsSpeeds[index] = speed;
        }
        private bool IsInFOV(float3 forward, float3 unitPosition, float3 targetPosition, float angle) => Float3Angle(forward, targetPosition - unitPosition) <= angle;

        private float Float3Angle(float3 a, float3 b)
        {
            float dot = math.dot(a, b);
            float aMagnitude = math.length(a);
            float bMagnitude = math.length(b);
            return dot / (aMagnitude * bMagnitude);
        }


    }
    private struct AddFollowVecJob : IJobParallelFor
    {
        [NativeDisableParallelForRestriction] public NativeArray<float3> unitMoveVec;
        [NativeDisableParallelForRestriction] public NativeArray<bool> unitArriveDestiny;
        [NativeDisableParallelForRestriction] public NativeArray<float3> followTarget;
        [NativeDisableParallelForRestriction] public NativeArray<float3> followDirection;
        [NativeDisableParallelForRestriction] public NativeArray<float3> unitPosition;
        [NativeDisableParallelForRestriction] public NativeArray<float> allUnitsSpeeds;

        public float followDistance;
        public float followWeight;
        public float followMaxSpeed;

        public bool isClosestToTarget;
        public bool shouldFollow;

        public float deltaTime;

        public void Execute(int index)
        {
            float3 followVec = float3.zero;
            float speed = allUnitsSpeeds[index];
            if (math.all(followDirection[index] != float3.zero) && shouldFollow)
            {
                followVec = followTarget[index] - unitPosition[index];
                followVec = math.normalize(followVec) * GetFollowWeight(unitPosition[index], followTarget[index]);
                speed = GetFollowSpeed(speed, unitPosition[index], followTarget[index]);
            }
            float distance = math.distance(unitPosition[index], followTarget[index]);
            if (isClosestToTarget && math.all(followDirection[index] != float3.zero) && distance > followDistance)
            {
                unitMoveVec[index] = math.normalize(unitMoveVec[index]);
                followVec = math.normalize(followVec) * (distance - followDistance);
                // speed += 0.25f;
            }
            if (distance <= (followDistance + 1) * 1.5f && math.all(followDirection[index] != float3.zero))
            {
                unitArriveDestiny[index] = true;
            }
            float multiplier = 1;
            if (distance < followDistance)
                multiplier = 0;
            unitMoveVec[index] += followVec * multiplier;
            allUnitsSpeeds[index] = speed;
        }
        private float GetFollowWeight(float3 curretnPosition, float3 targetposition) => followWeight * math.distance(curretnPosition, targetposition);
        // detecta si hay un "vecino" dentro del rango de vision
        private float GetFollowSpeed(float currentSpeed, float3 curretnPosition, float3 targetposition)
        {
            float currentVelocity = currentSpeed;
            float targetSpeed = (math.distance(curretnPosition, targetposition) - followDistance) * 0.25f + currentSpeed;
            if (followDistance == 0)
                targetSpeed = followMaxSpeed;
            return isClosestToTarget ? ((math.distance(curretnPosition, targetposition) - 1.5f) * 1.25f) : Mathf.SmoothDamp(currentSpeed, targetSpeed, ref currentVelocity, 0.075f, 10000, deltaTime);
        }
    }

    private struct MoveTransformJob : IJobParallelForTransform
    {
        [NativeDisableParallelForRestriction] public NativeArray<float3> unitMoveVec;
        [NativeDisableParallelForRestriction] public NativeArray<float3> unitForwardDirections;
        [NativeDisableParallelForRestriction] public NativeArray<float3> unitCurrentVelocity;
        [NativeDisableParallelForRestriction] public NativeArray<float3> obstacleVecs;
        [NativeDisableParallelForRestriction] public NativeArray<float> unitSpeed;
        public float smoothDamp;
        public float deltaTime;
        public float speedMultiplier;
        public void Execute(int index, TransformAccess transform)
        {
            float3 moveVec = unitMoveVec[index];
            if (math.isnan(moveVec.x) || math.isnan(moveVec.y) || math.isnan(moveVec.z))
                moveVec = float3.zero;
            if (unitMoveVec[index].x != float.NaN && unitMoveVec[index].y != float.NaN && unitMoveVec[index].z != float.NaN)
                moveVec += obstacleVecs[index];
            Vector3 currentVelocity = unitCurrentVelocity[index];
            moveVec = Vector3.SmoothDamp(unitForwardDirections[index], moveVec, ref currentVelocity, smoothDamp, 10000, deltaTime);
            moveVec = math.normalize(moveVec) * unitSpeed[index] * speedMultiplier;
            if (math.all(moveVec == float3.zero))
                moveVec = math.normalize(unitForwardDirections[index]);
            transform.position = transform.position + (Vector3)moveVec * deltaTime;
            if (!math.all(moveVec == float3.zero))
                transform.rotation = Quaternion.LookRotation(math.normalize(moveVec));
            unitCurrentVelocity[index] = currentVelocity;
        }
    }
#if UNITY_EDITOR
    /// <summary>
    /// Guide in the Unity editor to know where the fish go when they are born
    /// </summary>
    private void OnDrawGizmos()
    {
        if (!drawGuide)
            return;
        Gizmos.color = Color.red;
        Gizmos.DrawLine(transform.position, transform.position + spawnDirection * 2);
        Gizmos.color = Color.cyan;
        Gizmos.DrawWireSphere(transform.position, limitDistance);
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireCube(transform.position, spawnLimit);
        if (circularWeight == 0)
            return;
        //Handles.color = new Color(1,0.5f,0,1);
        float height = circularHeight * 0.25f;
        Handles.DrawWireDisc(transform.position + transform.up * circularArea * 0.5f + transform.up * height, transform.up, circularRadius);
        Handles.DrawWireDisc(transform.position + transform.up * height, transform.up, circularRadius + circularArea * 0.5f);
        Handles.DrawWireDisc(transform.position + transform.up * height, transform.up, circularRadius - circularArea * 0.5f);
        Handles.DrawWireDisc(transform.position - transform.up * circularArea * 0.5f - transform.up * height, transform.up, circularRadius);
        Handles.DrawWireDisc(transform.position - transform.up * height, transform.up, circularRadius + circularArea * 0.5f);
        Handles.DrawWireDisc(transform.position - transform.up * height, transform.up, circularRadius - circularArea * 0.5f);

        Handles.DrawWireArc(transform.position + transform.right * circularRadius + transform.up * height, transform.forward, transform.right, 180, circularArea * 0.5f);
        Handles.DrawWireArc(transform.position + transform.forward * circularRadius + transform.up * height, transform.right, -transform.forward, 180, circularArea * 0.5f);
        Handles.DrawWireArc(transform.position - transform.right * circularRadius + transform.up * height, -transform.forward, -transform.right, 180, circularArea * 0.5f);
        Handles.DrawWireArc(transform.position - transform.forward * circularRadius + transform.up * height, -transform.right, transform.forward, 180, circularArea * 0.5f);

        Handles.DrawWireArc(transform.position + transform.right * circularRadius - transform.up * height, transform.forward, -transform.right, 180, circularArea * 0.5f);
        Handles.DrawWireArc(transform.position + transform.forward * circularRadius - transform.up * height, transform.right, transform.forward, 180, circularArea * 0.5f);
        Handles.DrawWireArc(transform.position - transform.right * circularRadius - transform.up * height, -transform.forward, transform.right, 180, circularArea * 0.5f);
        Handles.DrawWireArc(transform.position - transform.forward * circularRadius - transform.up * height, -transform.right, -transform.forward, 180, circularArea * 0.5f);
        if (circularHeight == 0)
            return;
        Handles.DrawLine(transform.position + transform.right * (circularRadius - circularArea * 0.5f) + transform.up * height, transform.position + transform.right * (circularRadius - circularArea * 0.5f) - transform.up * height);
        Handles.DrawLine(transform.position + transform.right * (circularRadius + circularArea * 0.5f) + transform.up * height, transform.position + transform.right * (circularRadius + circularArea * 0.5f) - transform.up * height);
        Handles.DrawLine(transform.position + transform.forward * (circularRadius - circularArea * 0.5f) + transform.up * height, transform.position + transform.forward * (circularRadius - circularArea * 0.5f) - transform.up * height);
        Handles.DrawLine(transform.position + transform.forward * (circularRadius + circularArea * 0.5f) + transform.up * height, transform.position + transform.forward * (circularRadius + circularArea * 0.5f) - transform.up * height);
        Handles.DrawLine(transform.position - transform.right * (circularRadius - circularArea * 0.5f) + transform.up * height, transform.position - transform.right * (circularRadius - circularArea * 0.5f) - transform.up * height);
        Handles.DrawLine(transform.position - transform.right * (circularRadius + circularArea * 0.5f) + transform.up * height, transform.position - transform.right * (circularRadius + circularArea * 0.5f) - transform.up * height);
        Handles.DrawLine(transform.position - transform.forward * (circularRadius - circularArea * 0.5f) + transform.up * height, transform.position - transform.forward * (circularRadius - circularArea * 0.5f) - transform.up * height);
        Handles.DrawLine(transform.position - transform.forward * (circularRadius + circularArea * 0.5f) + transform.up * height, transform.position - transform.forward * (circularRadius + circularArea * 0.5f) - transform.up * height);

    }
#endif
}
