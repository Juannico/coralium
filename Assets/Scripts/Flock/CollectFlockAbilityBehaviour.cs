using FMODUnity;
using NaughtyAttributes;
using UnityEngine;

public class CollectFlockAbilityBehaviour : BaseAbilityBehaviour
{
    [Header("Collect FlockAbility Settings ")]
    [HideInInspector] public Flock Flock;
    [SerializeField] public int maxAmountOfFlocks = 3;
    [SerializeField] private float smoothTimeChangeVelocity = 0.4f;
    [SerializeField] [Range(0, 1)] private float idleSpeedMultiplier = 0.75f;
    [SerializeField] private float impulseSpeedMultiplier = 2;
    [SerializeField] private float outlineSize;
    [SerializeField] [ColorUsage(true, true)] private Color outlineColor;
    [ShowNonSerializedField] private Color baseColor = Color.black * -10;
    [SerializeField] private StudioEventEmitter allySFX;

    private IdleStateComponent idleState;
    private StopStateComponent stopState;
    private ImpulseStateComponent impulseState;

    private int flocksAmount;
    private float targetMultiplier;
    private float speedVelocity;

    public override void Throw(BaseAbilityCast ability)
    {
        base.Throw(ability);
        transform.parent = ability.Character.transform;
        transform.localPosition = Vector3.zero;
        Flock = ability.Character.GetComponentInChildren<Flock>(true);
        Flock.TargetToFollow = this.transform;
        SetStateComponents(ability.Character.gameObject);
        Flock.gameObject.SetActive(true);
        CoroutineHandler.ExecuteActionAfter(SetFlock, Constants.halfSecondDelay, this);
    }
    public void Update()
    {
        if (Flock.speedMultiplier != targetMultiplier)
            Flock.speedMultiplier = Mathf.SmoothDamp(Flock.speedMultiplier, targetMultiplier, ref speedVelocity, smoothTimeChangeVelocity);
    }

    private void SetStateComponents(GameObject character)
    {
        idleState = character.GetComponent<IdleStateComponent>();
        idleState.OnStart += () =>
        {
            Flock.isClosestToTarget = false;
            Flock.shouldFollow = false;
            targetMultiplier = idleSpeedMultiplier;
        };
        idleState.OnEnd += () =>
        {
            Flock.shouldFollow = true;
            Flock.isClosestToTarget = true;
            targetMultiplier = 1;
        };
        stopState = character.GetComponent<StopStateComponent>();
        stopState.OnEnd += () =>
        {
            targetMultiplier = 1;
        };
        impulseState = character.GetComponent<ImpulseStateComponent>();
        impulseState.OnStart += () => targetMultiplier = impulseSpeedMultiplier;
        impulseState.OnEnd += () => targetMultiplier = 1;
    }
    public void AssingFlockHandle(FlocksHandle flocksHandle)
    {
        Flock.FlocksHandle = flocksHandle;
        Flock.SetTick(false);
    }
    public int GetAmountOfFish() => Flock.AllUnit.Count;
    public void RemoveAllUnits()
    {
        FlockUnit[] flocksToDelete = Flock.AllUnit.ToArray();
        for (var i = 0; i < flocksToDelete.Length; i++)
        {
            Flock.RemoveFlockUnit(flocksToDelete[i]);
            Destroy(flocksToDelete[i].gameObject);
        }
        flocksAmount = 0;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (flocksAmount >= maxAmountOfFlocks)
            return;
        FlockUnit unit = other.GetComponentInParent<FlockUnit>();
        if (unit == null)
            return;
        if (unit.IsDied)
            return;
        if (Flock == unit.AssignedFlock)
            return;
        unit.AssingFlock(Flock, true);
        if (unit.AssignedFlock.canChangeFlock)
            allySFX?.Play();
        flocksAmount++;
    }
    private void SetFlock()
    {
        if (Flock.AllUnit.Count == 0)
            return;
        FlockUnit unit = Flock.AllUnit[0];
        unit.AssignedFlock.RemoveFlockUnit(unit);
        Destroy(unit.gameObject);
        Destroy(unit);
    }

}
