using NaughtyAttributes;
using Unity.Jobs;
using UnityEngine;

public class FlockUnit : MonoBehaviour
{
    // flock system sacado de GameDevChef: https://www.youtube.com/watch?v=mBVarJm3Tgk&t=1700s
    [SerializeField] private float _FOVAngle;
    public float FOVAngle { get { return _FOVAngle; } }
    [SerializeField] private float _smoothDamp;
    public float SmoothDamp { get { return _smoothDamp; } }
    [SerializeField] private LayerData layerData;
    [SerializeField] private Vector3[] directionsToCheckWhenAvodingObstabcle;
    [SerializeField] private LayerMask oceanSurface;
    [SerializeField] [Expandable] private SwitchPropertyData switchPropiertyData;
    [ShowNonSerializedField] private Color diedColor = new Color(0.19f, 0.06f, 0.06f);
    public Vector3 CurrenVelocity { get; set; }
    public Vector3 CurrenPositionVelocity { get; set; } = Vector3.zero;
    private Vector3 currentObstacleAvoidanceVec;
    public Vector3 OffsetTarget { get; set; }
    public float Speed { get; private set; }
    public float Angle { get; set; }
    public Flock AssignedFlock { get; private set; }
    private SpeedShaderFishController speedShaderFishController;
    public bool IsDied { get; private set; } = false;
    private bool moveDied;
    public bool IsRandomTargetOffset { get; set; }
    public Transform TargetDestination { get; set; }

    [SerializeField] private float dieDuration = 2.75f;
    [SerializeField] private float disolveDuration = 2.1f;
    private float timer;

    RaycastHit[] hits = new RaycastHit[1];

    private Renderer rnderer;

    public Material material { get; private set; }
    private void Awake()
    {
        material = gameObject.GetComponentInChildren<Renderer>(true).material;
    }
    private void Update()
    {
        if (!IsDied)
            return;
        Disolving();
        if (!moveDied)
            return;
        Diying();
    }

    private void Disolving()
    {
        timer += Time.deltaTime;
        if (timer < dieDuration)
            return;
        rnderer.material.SetFloat("_Disolve", (timer - dieDuration) / disolveDuration);
        if ((timer - dieDuration) / disolveDuration > 1)
            gameObject.SetActive(false);
    }

    private void Diying()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, Vector3.up, out hit, 0.1f, oceanSurface))
            moveDied = false;
        transform.position += Vector3.up * 3 * Time.deltaTime;

    }

    public void AssingFlock(Flock flock, bool changeAllFlock = false)
    {
        if (AssignedFlock != null)
        {
            if (!AssignedFlock.canChangeFlock)
                return;
            AssignedFlock.RemoveFlockUnit(this);
            flock.name = flock.name.Replace(AssignedFlock.name, flock.name);
        }
        Angle = Random.Range(0, 359);
        Flock lastFlock = AssignedFlock;
        AssignedFlock = flock;
        TargetDestination = AssignedFlock.TargetToFollow;
        AssignedFlock.AddFlockUnit(this);
        OffsetTarget = Random.onUnitSphere * Random.Range(1.2f, 2.5f);
        IsRandomTargetOffset = true;
        if (!changeAllFlock)
            return;
        FlockUnit[] flockUnitToAssing = lastFlock.AllUnit.ToArray();
        for (int i = 0; i < flockUnitToAssing.Length; i++)
            flockUnitToAssing[i].AssingFlock(flock);
    }

    public void InitilazedSpeed(float speed) => this.Speed = speed;
    public void InitializedSpeedShaderController()
    {
        speedShaderFishController = new SpeedShaderFishController(GetComponentInChildren<MeshRenderer>(true).material, Speed);
    }
    public Vector3 CalculateObstacleVec()
    {
        if (AssignedFlock == null)
            return -transform.forward;
        Vector3 obstacleVec = Vector3.zero;
        int index = Physics.RaycastNonAlloc(transform.position, transform.forward, hits, AssignedFlock.obstacleDistance, layerData.FishObstacleLayer);
        if (index > 0)
            obstacleVec = FindBestWaytoAvoidObstable();
        else
            currentObstacleAvoidanceVec = Vector3.zero;
        obstacleVec *= AssignedFlock.obstacleWeight * 0.25f;
        return obstacleVec;
    }

    public Vector3 FindBestWaytoAvoidObstable()
    {
        // If it still look at the same object keep the same direction
        if (currentObstacleAvoidanceVec != Vector3.zero)
        {
            int index = Physics.RaycastNonAlloc(transform.position, transform.forward, hits, AssignedFlock.obstacleDistance, layerData.FishObstacleLayer);
            if (index == 0)
                return currentObstacleAvoidanceVec;
        }
        float maxDistance = int.MinValue;
        var selectedDirection = Vector3.zero;
        // Calcualte  directions and chose the farest distance from object
        for (int i = 0; i < directionsToCheckWhenAvodingObstabcle.Length; i++)
        {

            var currentDirection = transform.TransformDirection(directionsToCheckWhenAvodingObstabcle[i].normalized);
            int index = Physics.RaycastNonAlloc(transform.position, transform.forward, hits, AssignedFlock.obstacleDistance, layerData.FishObstacleLayer);
            if (index > 0)
            {
                float currentDistance = (hits[0].point - transform.position).sqrMagnitude;
                if (currentDistance > maxDistance)
                {
                    maxDistance = currentDistance;
                    selectedDirection = currentDirection;
                }
            }
            else
            {
                selectedDirection = currentDirection;
                currentObstacleAvoidanceVec = currentDirection.normalized;
                return selectedDirection.normalized;
            }
        }
        return selectedDirection.normalized;
    }
    public void SetSpeed(float speed, float speedMutliplier)
    {
        Speed = speed;
        speedShaderFishController?.ChangeSpeed(speed * speedMutliplier);
    }
    public void Kill()
    {
        if (IsDied)
            return;
        AssignedFlock.RemoveFlockUnit(this);
        transform.parent = null;
        IsDied = true;
        moveDied = true;
        timer = 0;
        rnderer = GetComponentInChildren<Renderer>(true);
        CopyMaterialProperties copyMaterialPropierties = new CopyMaterialProperties();
        copyMaterialPropierties.Copy(rnderer, switchPropiertyData);
        rnderer.material.SetColor("_BaseColor", diedColor);
        speedShaderFishController?.ChangeSpeed(0);
    }

}
