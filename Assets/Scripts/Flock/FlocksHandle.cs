using System.Collections.Generic;
using UnityEngine;

public class FlocksHandle : MonoBehaviour
{
    [SerializeField] private CollisionHelper detectPlayer;
    [SerializeField] private LayerData layerData;
    [SerializeField] private Flock[] flocksToSpawn;
    [SerializeField] private int startAmountFLocks;
    public List<Flock> CurrentFlocks { get; private set; } = new List<Flock>();
    private int currentAmoutnOfFish;
    [SerializeField] private int minAmoutnOfFish;
    private bool isSetting = true;
    private void Awake()
    {
        /// TODO: Working on change FlockPlayerControl to AbilitySystem

        /*
        detectPlayer.TriggerEnter += (other) =>
        {
            if (!LayerUtilities.IsSameLayer(layerData.PlayerLayer, other.gameObject.layer))
                return;
            FlockPlayerControl flockPlayerControl = GameObject.FindGameObjectWithTag(Tags.Player).GetComponent<FlockPlayerControl>();
            flockPlayerControl.enabled = true;
            flockPlayerControl.AssingFlockHandle(this);
        };
    */
    }
    void Start()
    {
        for (var i = 0; i < flocksToSpawn.Length; i++)
            CurrentFlocks.Add(flocksToSpawn[i]);
        int counter = 0;
        while (counter < startAmountFLocks)
        {
            int flockIndex = Random.Range(0, CurrentFlocks.Count - 1);
            if (CurrentFlocks[flockIndex].gameObject.activeInHierarchy)
                continue;
            CurrentFlocks[flockIndex].gameObject.SetActive(true);
            currentAmoutnOfFish += CurrentFlocks[flockIndex].FishAmount;
            counter++;
        }
        isSetting = false;
    }

    public void AddFish(int amount) => currentAmoutnOfFish += amount;
    public void AddFlock(Flock flockToAdd) => CurrentFlocks.Add(flockToAdd);
    public void RemoveFish()
    {
        currentAmoutnOfFish--;
        if (currentAmoutnOfFish < minAmoutnOfFish)
            GenerateFlock();
    }
    public void RemoveFlock(Flock flockToRemove) => CurrentFlocks.Remove(flockToRemove);
    private void GenerateFlock()
    {
        if (isSetting)
            return;
        int flockIndex = Random.Range(0, CurrentFlocks.Count - 1);
        if (!CurrentFlocks[flockIndex].gameObject.activeInHierarchy)
        {
            flocksToSpawn[flockIndex].gameObject.SetActive(true);
            flocksToSpawn[flockIndex].CreateFlock();
            currentAmoutnOfFish += CurrentFlocks[flockIndex].FishAmount;
            return;
        }
        flockIndex = Random.Range(0, flocksToSpawn.Length - 1);
        Flock flock = Instantiate(flocksToSpawn[flockIndex], transform);
        flock.gameObject.SetActive(true);
        currentAmoutnOfFish += flock.FishAmount;
        CurrentFlocks.Add(flock);

    }
}
