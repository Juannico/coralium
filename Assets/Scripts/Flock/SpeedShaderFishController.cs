using UnityEngine;

public class SpeedShaderFishController
{
    private Material material;
    private float startSpeedMovementX;
    private float startFrecuencyWaveX;
    private float startAmplitudFactor;

    private float refStartSpeedMovementX;
    private float refStartFrecuencyWaveX;
    private float refSpeedAmplitudeModifier;
    private float baseSpeed;

    private Vector2 speedMovementModifierRange;
    private Vector2 frecuencyWaveModifierRange;
    private Vector2 speedAmplitudeModifierRange;
    public SpeedShaderFishController(Material material, float baseSpeed) {
        this.material = material;
        this.baseSpeed = baseSpeed;
        speedMovementModifierRange = new Vector2(0.085f,2.2f);
        frecuencyWaveModifierRange = new Vector2(0.75f, 1.1f);
        speedAmplitudeModifierRange = new Vector2(0.25f, 1.75f);
        SetStartAttributes();
    }
    private void SetStartAttributes( ){
        startSpeedMovementX = material.GetFloat("Vector1_SpeedFactor");
        startFrecuencyWaveX = material.GetFloat("Vector1_FrequencyWaveZ");
        startAmplitudFactor = material.GetFloat("Vector1_AmplitudFactor");
    }
    public void ChangeSpeed(float speed)
    {
        float speedMovementModifier = Mathf.Clamp(speed / baseSpeed, speedMovementModifierRange.x, speedMovementModifierRange.y);
        float frecuencyWaveModifier = Mathf.Clamp(speed / baseSpeed, frecuencyWaveModifierRange.x, frecuencyWaveModifierRange.y);
        float speedAmplitudeModifier =  Mathf.Clamp(speed / (baseSpeed * 1.1f), speedAmplitudeModifierRange.x, speedAmplitudeModifierRange.y);
        float smoothSpeedMovement = Mathf.SmoothDamp(material.GetFloat("Vector1_SpeedFactor"), startSpeedMovementX * speedMovementModifier, ref refStartSpeedMovementX, 0.45f,Time.deltaTime);
        float smoothFrecuencyWave = Mathf.SmoothDamp(material.GetFloat("Vector1_FrequencyWaveZ"), startFrecuencyWaveX * frecuencyWaveModifier, ref refStartFrecuencyWaveX, 0.45f,Time.deltaTime);
        float smoothSpeedAmplitude =  Mathf.SmoothDamp(material.GetFloat("Vector1_AmplitudFactor"), startAmplitudFactor * speedAmplitudeModifier, ref refSpeedAmplitudeModifier, 0.45f,Time.deltaTime);
        material.SetFloat("Vector1_SpeedFactor", smoothSpeedMovement);
        material.SetFloat("Vector1_FrequencyWaveZ", smoothFrecuencyWave);
        material.SetFloat("Vector1_AmplitudFactor", smoothSpeedAmplitude);
    }
}
