using UnityEngine;

public class ActiveFlocksGroupTrigger : MonoBehaviour
{
    [SerializeField] private GameObject flocksGroups;
    [SerializeField] private LayerMask playerLayer;
    [SerializeField] private bool flockStartState = true;
    [SerializeField] private bool showGuide;
    [SerializeField] private ControlFog fogControl;
    private Vector3 enterDirection;
    private void Awake()
    {
        flocksGroups.SetActive(flockStartState);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (!LayerUtilities.IsSameLayer(playerLayer, other.gameObject.layer))
            return;
        flocksGroups.SetActive(!flocksGroups.activeInHierarchy);
        enterDirection = other.transform.forward;
    }
    private void OnTriggerExit(Collider other)
    {
        if (!LayerUtilities.IsSameLayer(playerLayer, other.gameObject.layer))
            return;
        if(Vector3.Dot(other.transform.forward, enterDirection) <= 0)
            flocksGroups.SetActive(!flocksGroups.activeInHierarchy);
    }
#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        if (!showGuide)
            return;
        float radius =   fogControl.Distance * (fogControl.Fallout + 0.1f) * 0.12f;
        Gizmos.DrawWireSphere(transform.position, radius);
    }
#endif
}
