using System;
using UnityEngine;

public class TargetDestinations : MonoBehaviour
{
    [field:SerializeField] public GameObject TargetPostionsParent { get;  private set; }
    public Transform[] targetPositions { get; private set; }
    protected int targetIndex = 0;
    public Transform currentTarget { get; private set; }
    private void Start()
    {
        targetPositions = new Transform[TargetPostionsParent.transform.childCount];
        for (var i = 0; i < targetPositions.Length; i++)
            targetPositions[i] = TargetPostionsParent.transform.GetChild(i);
    }
    public Transform GetStartDestination(Transform referenceTransform, int maxIndex = 0) {
        int index = 0;
        float minDistance = Vector3.Distance(referenceTransform.position, targetPositions[0].position);
        int length = maxIndex == 0 ? targetPositions.Length : maxIndex;
        for (var i = 1; i< length; i++)
        {
            float distance = Vector3.Distance(referenceTransform.position, targetPositions[0].position);
            if (distance < minDistance) 
            {
                index = i;
                minDistance = distance;
            }
        }
        Vector3 refeceTransformDirection = targetPositions[index].position - referenceTransform.position;
        Vector3 destinationDirecion = targetPositions[index + 1].position - targetPositions[index].position;
        if (Vector3.Dot(refeceTransformDirection.normalized, destinationDirecion.normalized) <= 0f)
            index++;
        if (index >= maxIndex)
            index = 0;
        return targetPositions[index];
    }
    public  Transform SetNewDestination(Transform lastTransform) {
        int index = Array.IndexOf(targetPositions, lastTransform);
        index ++;
        if (index >= targetPositions.Length)
            index = 0;
        return targetPositions[index];
    }   
}
