using MoreMountains.Feedbacks;
using UnityEngine;

[AddComponentMenu("")]
[FeedbackPath("Coralium/GPUParticleSystem/SetGPUParticleSystemEmisionRate")]
public class MMF_Coralium_SetGPUParticleSystemEmisionRate : MMF_Feedback
{
#if UNITY_EDITOR
    public override Color FeedbackColor { get { return MMFeedbacksInspectorColors.TransformColor; } }
#endif
    [MMFInspectorGroup("Visual Effect Settings", true, 54, true)]
    [SerializeField] private GPUParticleSystem particleSystem;
    [SerializeField] private float targetRate;
    protected override void CustomPlayFeedback(Vector3 position, float feedbacksIntensity = 1) => particleSystem.emissionRate.value1 = targetRate;
}
