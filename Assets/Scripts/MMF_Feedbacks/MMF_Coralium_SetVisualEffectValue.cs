using MoreMountains.Feedbacks;
using UnityEngine;

[AddComponentMenu("")]
[FeedbackPath("Coralium/VisualEffect/SetVisualEffectPropertyValue")]
public class MMF_Coralium_SetVisualEffectValue : MMF_Feedback
{
#if UNITY_EDITOR
    public override Color FeedbackColor { get { return MMFeedbacksInspectorColors.TransformColor; } }
#endif
    [MMFInspectorGroup("Visual Effect Settings", true, 54, true)]
    [SerializeField]private VisualEffectProperty visualEffect;
    protected override void CustomPlayFeedback(Vector3 position, float feedbacksIntensity = 1) => visualEffect.SetValue();
}
