using FMODUnity;
using UnityEngine;
using UnityEngine.VFX;

// #TODO: Delete this class once Projectiles support gets expanded. Lots of temporary copypasta here from ShootBehavior.cs
public sealed class RedShotProjectile : BaseProjectile
{
    [Header("FX")]
    [SerializeField] private StudioEventEmitter shootSFX, shootCollideSFX;
    [SerializeField] private VisualEffect disolvereffect, burbujas;
    [SerializeField] private MeshRenderer trail;
    [SerializeField] private float targetSizeMultiplier = 3.0f;
    [SerializeField] private float growSpeed = 10.0f;
    [SerializeField] private int damage = 1;
    [SerializeField] private DamageTypes damageType = DamageTypes.Basic;

    private Material trailMat = null;

    private bool shouldGrowProjectile = false;
    private Vector3 initialScale = Vector3.zero;
    private Vector3 targetScale = Vector3.zero;
    private float sizeLerpValue = 0.0f;

    protected override void Awake()
    {
        base.Awake();

        trailMat = trail.material;
    }

    public override void Activate()
    {
        // Set SFX values
        shootSFX.OverrideAttenuation = true;
        shootSFX.OverrideMaxDistance = 250;
        shootSFX.OverrideMinDistance = 1;
        shootCollideSFX.OverrideAttenuation = true;
        shootCollideSFX.OverrideMaxDistance = 250;
        shootCollideSFX.OverrideMinDistance = 1;

        shootSFX.Play();

        // Set VFX values
        trailMat.SetFloat("_Alpha", 1);

        shouldGrowProjectile = true;
        initialScale = gameObject.transform.localScale;
        targetScale = initialScale * targetSizeMultiplier;
        sizeLerpValue = 0.0f;

        base.Activate();
    }

    private void Update()
    {
        if (shouldGrowProjectile)
        {
            sizeLerpValue += growSpeed * Time.deltaTime;
            gameObject.transform.localScale = Vector3.Lerp(initialScale, targetScale, sizeLerpValue);

            if (sizeLerpValue >= 1.0f)
            {
                shouldGrowProjectile = false;
            }
        }
    }

    public override void Deactivate()
    {
        shouldGrowProjectile = false;
        sizeLerpValue = 0.0f;
        initialScale = Vector3.zero;
        targetScale = Vector3.zero;

        base.Deactivate();
    }

    protected override void OnSuccessfulTriggerEnter(Collider other)
    {
        base.OnSuccessfulTriggerEnter(other);

        IDamageable<int, DamageTypes> objectDamagable = other.GetComponentInParent<IDamageable<int, DamageTypes>>();

        if (objectDamagable != null)
        {
            objectDamagable.TakeDamage(damage, damageType, transform.forward);
        }

        if (other.TryGetComponent(out DropObjects dropObjects))
        {
            dropObjects.Drops();
        }
        if (other.TryGetComponent(out TrashObject trashObject))
        {
            trashObject.Vanish();
            return;
        }
        if (other.TryGetComponent(out CleanCoralController ControlLimpiarCoral))
        {
            ControlLimpiarCoral.StartClean();
            return;
        }
        if (other.TryGetComponent(out Pusheable pusheable))
        {
            pusheable.Push(transform.forward);
            return;
        }
    }
}