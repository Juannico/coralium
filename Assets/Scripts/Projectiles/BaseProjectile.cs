using UnityEngine;

public class BaseProjectile : MonoBehaviour
{
    // #TODO: Move this to projectile asset once created
    [SerializeField] protected float projectileSpeed = 100.0f;
    [SerializeField] protected LayerData layerData;

    protected GameObject sourceGameObject;
    protected Rigidbody rigidBody;

    public static BaseProjectile SpawnProjectile(GameObject prefab, GameObject sourceGameObject, Vector3 position, Quaternion rotation)
    {
        if (prefab == null)
        {
            Debug.LogWarning($"BaseProjectile::SpawnProjectile: prefab is null.");
            return null;
        }

        GameObject spawnedProjectileGO = Instantiate(prefab, position, rotation);
        BaseProjectile projectileComponent = spawnedProjectileGO.GetComponent<BaseProjectile>();
        projectileComponent.sourceGameObject = sourceGameObject;
        return projectileComponent;
    }
    protected virtual void Awake()
    {
        rigidBody = GetComponent<Rigidbody>();
    }

    public virtual void Activate()
    {
        StartMovement();
    }

    public virtual void Deactivate()
    {
        StopMovement();

        // #TODO: Add projectile pooling
        Destroy(gameObject);
    }

    protected virtual void StartMovement()
    {
        if (rigidBody == null)
        {
            Debug.Log("BaseProjectile::StartMovement: rigidBody is null.");
            return;
        }

        StopMovement();
        rigidBody.AddForce(transform.forward * projectileSpeed, ForceMode.VelocityChange);
    }

    protected virtual void StopMovement()
    {
        if (rigidBody == null)
        {
            Debug.Log("BaseProjectile::StopMovement: rigidBody is null.");
            return;
        }

        rigidBody.velocity = Vector3.zero;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!gameObject.activeInHierarchy || other.gameObject == gameObject || ShouldIgnoreLayer(other.gameObject.layer))
        {
            return;
        }

        OnSuccessfulTriggerEnter(other);
    }

    protected virtual bool ShouldIgnoreLayer(int ignoreLayer)
    {
        Debug.Log(gameObject.name);
        return LayerUtilities.IsSameLayer(layerData.IgnoreCollider, ignoreLayer) || LayerUtilities.IsSameLayer(layerData.Trigger, ignoreLayer) || (sourceGameObject.layer == ignoreLayer);
    }

    protected virtual void OnSuccessfulTriggerEnter(Collider other)
    {

    }
}