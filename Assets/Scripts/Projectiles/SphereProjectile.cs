using UnityEngine;

// #TODO: Check this class once Projectiles support gets expanded. Temporary for test tickTrow sphera ability.
public class SphereProjectile : BaseProjectile
{
    [SerializeField] private int damage = 1;
    [SerializeField] private DamageTypes damageType = DamageTypes.Basic;
    protected override void OnSuccessfulTriggerEnter(Collider other)
    {
        IDamageable<int, DamageTypes> objectDamagable = other.GetComponentInParent<IDamageable<int, DamageTypes>>();
        if (objectDamagable != null)
            objectDamagable.TakeDamage(damage, damageType, transform.forward);
        Deactivate();
    }
}
