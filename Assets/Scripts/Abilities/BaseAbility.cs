using System;
using UnityEngine;

public abstract class BaseAbility : MonoBehaviour
{
    // Event that gets triggered when the ability gets activated.
    public Action<BaseAbility> onAbilityStarted;

    // Event that gets triggered when the ability ends. If the bool argument is true, it means the ability was canceled.
    public Action<BaseAbility, bool> onAbilityEnded;

    public Action<BaseAbility, string, AbilityParams> onAbilityEvent;

    // Params passed to the ability. These values are meant to be calculated dynamically.
    protected AbilityParams abilityParams;

    // AbilityDescriptor passed to the ability. These values are meant to be static.
    private BaseAbilityDescriptor abilityDescriptor;

    private bool abilityIsActive = false;

    public AbilityParams Params { get { return abilityParams; } }
    public BaseAbilityDescriptor Descriptor { get { return abilityDescriptor; } }

    private void Awake()
    {
        OnAwake();
    }

    protected virtual void OnAwake()
    {

    }

    private void Start()
    {
        OnStart();
    }

    protected virtual void OnStart()
    {

    }

    /// <summary>
    /// Perform any checks that the ability needs to do to ensure it can be properly performed
    /// </summary>
    /// <returns></returns>
    protected virtual bool CommitCheck()
    {
        // Ability descriptor is a requirement for any ability
        return abilityDescriptor != null;
    }

    /// <summary>
    /// Tries to prepare the ability before activation
    /// </summary>
    /// <returns></returns>
    public virtual bool PrepareAbility(BaseAbilityDescriptor abilityDescriptor)
    {
        this.abilityDescriptor = abilityDescriptor;
        return CommitCheck();
    }

    /// <summary>
    /// Tries to prepare the ability before activation. Receives params.
    /// </summary>
    /// <param name="abilityParams"> The params passed to this ability. </param>
    /// <returns></returns>
    public virtual bool PrepareAbility(BaseAbilityDescriptor abilityDescriptor, AbilityParams abilityParams)
    {
        this.abilityParams = abilityParams;
        return PrepareAbility(abilityDescriptor);
    }

    /// <summary>
    /// Perform the ability
    /// </summary>
    public virtual void ActivateAbility()
    {
        abilityIsActive = true;
        onAbilityStarted?.Invoke(this);
    }

    private void Update()
    {
        // Only execute if the ability is active
        if (!IsAbilityActive())
        {
            return;
        }

        AbilityUpdate();
    }

    private void LateUpdate()
    {
        // Only execute if the ability is active
        if (!IsAbilityActive())
        {
            return;
        }

        AbilityLateUpdate();
    }

    private void FixedUpdate()
    {
        // Only execute if the ability is active
        if (!IsAbilityActive())
        {
            return;
        }

        AbilityFixedUpdate();
    }

    protected virtual void AbilityUpdate()
    {

    }

    protected virtual void AbilityLateUpdate()
    {

    }

    protected virtual void AbilityFixedUpdate()
    {

    }

    /// <summary>
    /// Finishes the ability
    /// </summary>
    public virtual void EndAbility()
    {
        OnAbilityEnded(false);
    }

    /// <summary>
    /// Cancels the ability
    /// </summary>
    public virtual void CancelAbility()
    {
        OnAbilityEnded(true);
    }

    protected virtual void OnAbilityEnded(bool wasCancelled)
    {
        abilityIsActive = false;
        onAbilityEnded?.Invoke(this, wasCancelled);
    }

    /// <summary>
    /// Triggered when this ability receives an event from an external entity
    /// </summary>
    /// <param name="eventName"></param>
    /// <param name="eventParams"></param>
    public virtual void ReceiveAbilityEvent(string eventName, AbilityParams eventParams)
    {

    }

    /// <summary>
    /// Sends event to anything bound to onAbilityEvent. See AbilityEventHandler
    /// </summary>
    /// <param name="eventName"></param>
    /// <param name="eventParams"></param>
    protected void SendAbilityEvent(string eventName, AbilityParams eventParams)
    {
        onAbilityEvent?.Invoke(this, eventName, eventParams);
    }

    /// <summary>
    /// Determines if the ability is active.
    /// </summary>
    /// <returns></returns>
    public bool IsAbilityActive()
    {
        return abilityIsActive;
    }

    protected virtual T GetAbilityDescriptor<T>() where T : BaseAbilityDescriptor
    {
        return (T)abilityDescriptor;
    }

    public virtual void DefineFxSpawnLocationAndRotation(BaseAbilityFX abilityFx, ref Vector3 location, ref Quaternion rotation)
    {
        // By default, use the transform of the owner of this component
        location = transform.position;
        rotation = transform.rotation;
    }
}