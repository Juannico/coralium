using UnityEngine;
using UnityEngine.InputSystem.Interactions;

[CreateAssetMenu(fileName = "PlayerAbilites", menuName = "Coralium/Abilities/Player Abilites")]
public class PlayerAbilities : ScriptableObject,ISerializationCallbackReceiver
{
    [field: SerializeField] public BaseAbilityCast[] BaseAbilities { get; private set; }
    [field: SerializeField] public BaseAbilityComboCast[] AbilitiesCombo { get; private set; }
    public BaseAbilityCast[] Abilities { get; private set; }
    private GameObject PlayerAbilitiesHandleGO;
    private InputHandle inputHandle;
    public void InitializeAbilities(PlayerCharacter character)
    {
        PlayerAbilitiesHandleGO = new GameObject("InputHandle");
        inputHandle = PlayerAbilitiesHandleGO.AddComponent<InputHandle>();
        Abilities = new BaseAbilityCast[BaseAbilities.Length + AbilitiesCombo.Length];
        GameObject abilitesContainer = new GameObject($"{character.gameObject.name}Abilities");
        abilitesContainer.transform.parent = character.transform;
        abilitesContainer.transform.parent = null;
        for (int i = 0; i < BaseAbilities.Length; i++)
        {
            Abilities[i] = Instantiate(BaseAbilities[i]);
            Abilities[i].name = $"Cast_{BaseAbilities[i].name}";
            Abilities[i].Initialize(character);
            Abilities[i].ParentObject.transform.parent = abilitesContainer.transform;
            CreateInputAction(Abilities[i]);
        }
        for (int i = 0; i < AbilitiesCombo.Length; i++)
        {

            Abilities[i + BaseAbilities.Length] = Instantiate(AbilitiesCombo[i]);
            Abilities[i + BaseAbilities.Length].name = $"Cast_{AbilitiesCombo[i].name}";
            BaseAbilityComboCast abiltyCombo = Abilities[i + BaseAbilities.Length] as BaseAbilityComboCast;
            for (int j = 0; j < BaseAbilities.Length; j++)
            {
                if (abiltyCombo.Ability == BaseAbilities[j])
                    abiltyCombo.Ability = Abilities[j];
                if (abiltyCombo.AbilityToCombine == BaseAbilities[j])
                    abiltyCombo.AbilityToCombine = Abilities[j];
            }
            Abilities[i + BaseAbilities.Length].Initialize(character);
            Abilities[i + BaseAbilities.Length].ParentObject.transform.parent = abilitesContainer.transform;
        }
    }

    private void CreateInputAction(BaseAbilityCast abilityCast)
    {
        if (!abilityCast.Data.HasInputAction)
            return;
        InputReference inputReferenece = abilityCast.Data.InputReference;
        if (!inputReferenece.InputAction.enabled)
            inputReferenece.InputAction.Enable();
        inputReferenece.InputReferenceData.Started = (ctx) => inputHandle.OnAddInputStack(inputReferenece);   
        inputReferenece.InputReferenceData.Performed = (ctx) => inputHandle.OnRemoveInputStack(inputReferenece);
        inputReferenece.InputReferenceData.Canceled += (ctx) =>
        {
            inputHandle.DoInputAction(inputReferenece);
            if (ctx.interaction is HoldInteraction)
                inputHandle.OnRemoveInputStack(inputReferenece, false);

        };
    }

    public void ResetAbilities()
    {
        for (int i = 0; i < BaseAbilities.Length; i++)
            ResetInputAction(Abilities[i]);
    }
    private void ResetInputAction(BaseAbilityCast abilityCast)
    {
        if (!abilityCast.Data.HasInputAction)
            return;
        InputReference inputReferenece = abilityCast.Data.InputReference;
        inputReferenece.InputReferenceData.Started = null;
        inputReferenece.InputReferenceData.Performed = null;
        inputReferenece.InputReferenceData.Canceled = null;
    }
    public BaseAbilityCast GetPlayerAbility(BaseAbilityCast baseAbilityCast)
    {
        for (int i = 0; i < BaseAbilities.Length; i++)
        {
            if (BaseAbilities[i] == baseAbilityCast)
                return Abilities[i];
        }
        return null;
    }
    public void OnBeforeSerialize()
    {
        
    }

    public void OnAfterDeserialize()
    {
        Abilities = null;
    }
}


