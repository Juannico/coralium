using UnityEngine;

public class EnemyAbilities : MonoBehaviour
{
    [field: SerializeField] public BaseAbilityCast[] BaseAbilities { get; private set; }
    public BaseAbilityCast[] Abilities { get; private set; }
    [HideInInspector] public BaseAbilityCast CurrentAbility;
    public void InitializeAbilities(EnemyCharacter character)
    {
        Abilities = new BaseAbilityCast[BaseAbilities.Length];
        GameObject abilitesContainer = new GameObject($"{character.gameObject.name}Abilities");
        abilitesContainer.transform.parent = character.transform;
        abilitesContainer.transform.parent = null;
        for (int i = 0; i < BaseAbilities.Length; i++)
        {
            Abilities[i] = Instantiate(BaseAbilities[i]);
            Abilities[i].name = $"Cast_{BaseAbilities[i].name}";
            Abilities[i].Initialize(character);
            Abilities[i].ParentObject.transform.parent = abilitesContainer.transform;
            BaseAbilityComboCast abiltyCombo = Abilities[i] as BaseAbilityComboCast;
            if (abiltyCombo == null)
                continue;
            abiltyCombo.Ability = Instantiate(abiltyCombo.Ability);
            abiltyCombo.Ability.Initialize(character);
            abiltyCombo.AbilityToCombine = Instantiate(abiltyCombo.AbilityToCombine);
            abiltyCombo.AbilityToCombine.Initialize(character);
        }
    }
    public void SetCurrenAbility(int abilityIndex)
    {
        if (Abilities == null)
        {
            Debug.LogWarning("EnemyAbility::SetCurrenAbility: Abilities is null.");
            return;
        }
        if (abilityIndex >= Abilities.Length)
            return;
        CurrentAbility = Abilities[abilityIndex];
    }

}
