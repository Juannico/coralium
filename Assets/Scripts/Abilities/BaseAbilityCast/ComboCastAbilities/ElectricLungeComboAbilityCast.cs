public class ElectricLungeComboAbilityCast : BaseAbilityComboCast
{
    public override bool Cast(bool ignoreLoad)
    {
        if (!base.Cast(ignoreLoad))
            return false;
        Character.BaseCharacter.Data.EndStateAction += () =>
        {
            CurrentBehaviour.gameObject.SetActive(false);
            EndCastAbility?.Invoke();
        };

        return true;
    }
}
