using UnityEngine;

public class ElectrickShootComboAbilityCast : BaseAbilityComboCast
{
    protected PoolHandler pool;
    protected override void CreateBehaviour(BaseAbilityBehaviour baseAbilityBehaviour)
    {
        pool = gameObject.AddComponent<PoolHandler>();
        int amountToPool = Mathf.CeilToInt(Data.Duration / Data.Cooldown);
        amountToPool++;
        GameObject behaviourParent = new GameObject($"Behaviour_{baseAbilityBehaviour.name}");
        behaviourParent.transform.parent = ParentObject.transform;
        pool.CreatePool(baseAbilityBehaviour.gameObject, amountToPool, behaviourParent.transform, true);
    }
    public override bool Cast(bool ignoreLoad)
    {
        if (!base.Cast(ignoreLoad))
            return false;
        EndCastAbility?.Invoke();
        return true;
    }
    protected override BaseAbilityBehaviour GetCurrentBehaviour() => pool.GetObject().GetComponent<BaseAbilityBehaviour>();
    protected override void SetTransformPositionRotationForThrow()
    {
        CurrentBehaviour.transform.position = Character.transform.position + Data.Direction * 2.5f;
        CurrentBehaviour.transform.forward = Data.Direction;
    }
}
