using NaughtyAttributes;
using System;
using UnityEngine;

[Serializable]
public class BaseAbilityComboCast : BaseAbilityCast
{
    [Expandable] public AbilityComboData ComboData;
    public BaseAbilityCast Ability;
    public BaseAbilityCast AbilityToCombine;
    [SerializeField] private bool trowSecondAbility;
    public override void Initialize(Character character)
    {
        SetComboData();
        base.Initialize(character);
        IsLocked = false;
        Data.IsCombined = true;
    }
    protected override void SetAction()
    {

        AddAction(Ability.Data.InputReference, AbilityToCombine.Data.InputReference.InputReferenceData);
        AddAction(AbilityToCombine.Data.InputReference, Ability.Data.InputReference.InputReferenceData);
    }
    private void AddAction(InputReference inputActionReferenceReciver, InputReferenceData inputReferenceDataToAdd)
    {
        CombineInputReferenceData combineInputReferenceData = new CombineInputReferenceData();
        combineInputReferenceData.InputReferenceDataToCombine = inputReferenceDataToAdd;
        combineInputReferenceData.ActionToCombineOnInputAdded = () => Load();
        combineInputReferenceData.ActionToCombineOnInputRemoved = () => Loaded();
        combineInputReferenceData.ActionToCombineOnInputActioned = () => Cast(false);
        for (int i = 0; i < inputActionReferenceReciver.CombineInputetReferenceData.Count; i++)
        {
            if (inputActionReferenceReciver.CombineInputetReferenceData[i].InputReferenceDataToCombine == inputReferenceDataToAdd)
            {
                inputActionReferenceReciver.CombineInputetReferenceData[i] = combineInputReferenceData;
                return;
            }
        }
        inputActionReferenceReciver.CombineInputetReferenceData.Add(combineInputReferenceData);
    }
    private void SetComboData()
    {
        Data.Distance = (Ability.Data.Distance + AbilityToCombine.Data.Distance) * ComboData.DistanceMultiplier;
        Data.Speed = (Ability.Data.Speed + AbilityToCombine.Data.Speed) * ComboData.SpeedMultiplier;
        Data.Damage = (int)((Ability.Data.Damage + AbilityToCombine.Data.Damage) * ComboData.DamageMultiplier);
        Data.Duration = (Ability.Data.Duration + AbilityToCombine.Data.Duration) * ComboData.DurationMultiplier;
        Data.EnergyCost = (Ability.Data.EnergyCost + AbilityToCombine.Data.EnergyCost) * ComboData.EnergyCostMultiplier;
        Data.Cooldown = (Ability.Data.Cooldown + AbilityToCombine.Data.Cooldown) * 0.5f;
    }
    public override bool Load()
    {
        if (!base.Load())
            return false;
        if (!Ability.Load())
            return false;
        if (!AbilityToCombine.Load())
            return false;
        return true;
    }
    public override bool Loaded()
    {
        if (!base.Loaded())
            return false;
        if (!Ability.Loaded())
            return false;
        if (!AbilityToCombine.Loaded())
            return false;
        return true;
    }
    public override bool Cast(bool ignoreLoad)
    {
        if (!base.Cast(ignoreLoad))
            return false;
        if (trowSecondAbility)
            CurrentBehaviour.GetComponent<ISetSecondAbilityBehaviour>()?.SetSecondAbility(AbilityToCombine);
        Ability.Data.BaseAbilityComboCast = this;
        Ability.Cast(ignoreLoad);
        Ability.Data.BaseAbilityComboCast = null;
        CoroutineHandler.ExecuteActionAfter(() => EndCast(), Data.Cooldown * Constants.halfSecondDelay, this);
        return true;
    }
    protected override void EndCast()
    {
        Data.IsCasting = false;
        Ability.Data.IsCasting = false;
        AbilityToCombine.Data.IsCasting = false;
    }
}
