using MoreMountains.Feedbacks;
using NaughtyAttributes;
using System;
using UnityEngine;
public class BaseAbilityCast : MonoBehaviour, ISaveStateJSON
{
    [Expandable] public AbilityData Data;
    [Header("Feels")]
    [SerializeField] private bool useFeels = false;
    [field: ShowIf("useFeels")] [field: SerializeField] public Transform TargetFeelsContainer { get; private set; }
    [ShowIf("useFeels")] [SerializeField] private MMFeedbacks loadFeels;
    [ShowIf("useFeels")] [SerializeField] private bool useAbilityDuration = false;
    [ShowIf("useFeels")] [SerializeField] private MMFeedbacks castFeels;
    public Character Character { get; private set; }
    [Header("")]
    [SerializeField] protected bool isLocked = false;
    [SerializeField] private bool autoCastOnUnlocked;
    public bool IsLocked
    {
        get { return isLocked; }
        set
        {
            isLocked = value;
            Data.OnSwitchLockAbility?.Invoke(value);
            State = value;
        }
    }
    protected bool isCharging = false;
    protected bool isCancelled;
    [HideInInspector] public BaseAbilityBehaviour CurrentBehaviour = null;
    private float timer;
    public float Timer
    {
        get => timer;
        set
        {
            timer = value;
            OnTimerChange?.Invoke(value);
        }
    }
    [HideInInspector] public Action<float> OnTimerChange;
    [HideInInspector] public Action EndCastAbility;
    public GameObject ParentObject { get; private set; }


    protected ChangeEmision changeEmision;
    [SerializeField] protected bool updateDirectionOnCast = true;

    [SerializeField] private bool useSaveJSON;
    public virtual void Initialize(Character character)
    {
        Data.ResetValue();
        Data = Data.Clone();
        Character = character;
        isCharging = false;
        Data.IsCasting = false;
        Data.IsCombined = false;
        Data.IsLoaded = false;
        Data.BaseAbilityComboCast = null;
        SetAction();
        ParentObject = new GameObject($"Ability_{gameObject.name.Replace("Cast_", "")}");
        transform.parent = ParentObject.transform;
        SetBehaviour(character);
        changeEmision = character.GetComponentInChildren<ChangeEmision>(true);
        Data.CastAbility += () =>
        {
            if (Data.ChangeColorOnCast)
                changeEmision?.SetEmision(Data.CharacterColor);
        };
        EndCastAbility += () => changeEmision?.RestarEmission();
        if (useFeels)
        {
            if (Data.HasLoadTime)
                loadFeels.DurationMultiplier = Data.LoadTime;
            if (useAbilityDuration)
            {
                castFeels.DurationMultiplier = Data.Duration;
            }
            castFeels.Events.OnComplete.AddListener(() => Character.SetAbilityFeelsTarget(this, false));
        }
        LoadJSON();
        if (!autoCastOnUnlocked)
            return;
        if (!IsLocked)
        {
            Cast(true);
            return;
        }
        bool casted = false;
        Data.OnSwitchLockAbility += (isLocked) =>
        {
            if (casted)
                return;
            if (isLocked)
                return;
            Cast(true);
            casted = true;
        };
    }
    private void SetBehaviour(Character character)
    {
        if (Data.BehaviourPrefab == null)
            return;
        if (!Data.BehaviourPrefab.TryGetComponent(out BaseAbilityBehaviour baseAbilityBehaviour))
            return;
        if (baseAbilityBehaviour.UseCollisionHelper)
            baseAbilityBehaviour.CollisionHelper = character.GetComponent<CollisionHelper>();
        CreateBehaviour(baseAbilityBehaviour);
    }
    protected virtual void CreateBehaviour(BaseAbilityBehaviour baseAbilityBehaviour)
    {
        CurrentBehaviour = Instantiate(baseAbilityBehaviour);
        CurrentBehaviour.gameObject.SetActive(false);
        CurrentBehaviour.transform.parent = ParentObject.transform;
        CurrentBehaviour.CollisionHelper = baseAbilityBehaviour.CollisionHelper;
    }
    protected virtual void SetAction()
    {
        if (!Data.HasInputAction)
            return;
        Data.InputReference.InputReferenceData.InputActionInteractionData.InputActionReference = Data.InputReference.InputReferenceData.InputActionReference;
        Data.InputReference.InputReferenceData.InputActioned = () => Cast(false);

        if (Data.InputReference.InputReferenceData.InputActionInteractionData.IsHold)
        {
            Data.InputReference.InputReferenceData.InputAdded = () => Load();
            Data.InputReference.InputReferenceData.InputRemoved = () => Loaded();
            return;
        }
        Data.InputReference.InputReferenceData.InputAdded = () =>
        {
            Load();
            Loaded();
        };

    }
    public virtual void Update()
    {
        if (isCharging || Data.IsLoaded)
        {
            Timer = 0;
            return;
        }
        Timer += Time.deltaTime;
    }
    public virtual bool Load()
    {
        Data.Direction = Character.GetAbilityDirection();
        isCancelled = false;
        if (!Character.AbilitiesHandle.CanCastAbitity())
            return false;
        if (!CompleteRequeriments())
            return false;
        isCharging = true;
        Data.LoadAbility?.Invoke();

        Character.AbilitiesHandle.AddAbilityCast(this);
        if (useFeels && Data.HasLoadTime)
        {
            Character.SetAbilityFeelsTarget(this, true);
            loadFeels.PlayFeedbacks();
        }
        return true;
    }

    private bool CompleteRequeriments()
    {
        if (IsLocked)
            return false;
        if (!Character.BaseCharacter.Data.CanMove)
            return false;
        if (Character.BaseCharacter.Data.IsScared)
            return false;
        if (Timer < Data.Cooldown)
            return false;
        if (Character.BaseCharacter.Data.CurrentEnergy < Data.EnergyCost)
            return false;
        return true;
    }
    public virtual bool Loaded()
    {
        if (!isCharging)
            return false;
        isCharging = false;
        Data.IsLoaded = true;
        Data.LoadedAbility?.Invoke();
        return true;
    }
    public virtual bool Cast(bool ignoreLoad)
    {
        if (isCancelled)
            return false;
        if (!Data.IsLoaded && !ignoreLoad)
        {
            CancelCast();
            return false;
        }
        Character.AbilitiesHandle.RemoveAbilityCast(this);
        if (useFeels && !Data.IsCasting)
        {
            if (Data.HasLoadTime)
                loadFeels.StopFeedbacks();
            else
                Character.SetAbilityFeelsTarget(this, true);
            castFeels.PlayFeedbacks();
        }
        Data.IsLoaded = false;
        Data.IsCasting = true;
        if (updateDirectionOnCast)
            Data.Direction = Character.GetAbilityDirection();
        Data.CastAbility?.Invoke();
        if (Data.IsCombined)
        {
            Character.BaseCharacter.Data.CurrentEnergy -= Data.EnergyCost;
            CurrentBehaviour = GetCurrentBehaviour();
            SetTransformPositionRotationForThrow();
            return true;
        }
        if (Data.BaseAbilityComboCast != null)
        {
            CurrentBehaviour = Data.BaseAbilityComboCast.CurrentBehaviour;
            CurrentBehaviour?.Throw(Data.BaseAbilityComboCast);
            return true;
        }
        Character.BaseCharacter.Data.CurrentEnergy -= Data.EnergyCost;
        CurrentBehaviour = GetCurrentBehaviour();
        SetTransformPositionRotationForThrow();
        CurrentBehaviour?.Throw(this);
        return true;

    }
    public virtual void CancelCast()
    {
        if (useFeels && Data.HasLoadTime)
        {
            Character.SetAbilityFeelsTarget(this, false);
            castFeels.StopFeedbacks();
            loadFeels.StopFeedbacks();
        }
        Data.IsLoaded = false;
        isCharging = false;
        isCancelled = true;
    }
    protected virtual void EndCast()
    {
        Character.EndCastAbility();
        Data.IsCasting = false;
    }

    protected virtual BaseAbilityBehaviour GetCurrentBehaviour()
    {
        CurrentBehaviour.gameObject.SetActive(true);
        return CurrentBehaviour;
    }
    protected virtual void SetTransformPositionRotationForThrow() { }

    public void EndFeelsOnAbilityBehaviour()
    {
        if (!useFeels)
            return;
        if (!castFeels.IsPlaying)
            return;
        Character.SetAbilityFeelsTarget(this, false);
        castFeels.StopFeedbacks();
    }
    #region SaveState
    public bool State
    {
        get => isLocked;
        set
        {
            isLocked = value;
            Save();
        }
    }

    public string KeyName => "Lock";

    public string Path => $"Abilities/{gameObject.name}";
    public void Save() => DataManager.SaveInterface(this, false);
    private void LoadJSON()
    {
        if (!useSaveJSON)
            return;
        DataManager.UpdateStateInterfaceValue(this, false);
        IsLocked = State;
    }
    #endregion
}
