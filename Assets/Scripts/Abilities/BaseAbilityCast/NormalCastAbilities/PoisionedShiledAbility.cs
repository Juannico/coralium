using UnityEngine;

public class PoisionedShiledAbility : BaseAbilityCast
{
    private float recoverShieldTimer = 0;
    private Coroutine recovelShieldCoroutine;
    public override void Initialize(Character character)
    {
        base.Initialize(character);
        Data.OnSwitchLockAbility += (isLocked) =>
        {
            if (!isLocked)
                Character.BaseCharacter.Data.CurrentShield = Character.BaseCharacter.Data.Shield;
        };
    }
    public override void Update()
    {
        if (IsLocked)
            return;
        base.Update();
    }

    public void StartToRecoverShield()
    {
        if (IsLocked)
            return;
        if (recovelShieldCoroutine != null)
            StopCoroutine(recovelShieldCoroutine);
        float recoverShieldTimer = 0;
        recovelShieldCoroutine = StartCoroutine(CoroutineHandler.ExecuteActionAfterUpdateUntil(() =>
            {
                recoverShieldTimer += Time.deltaTime;
                if (recoverShieldTimer > Data.Cooldown)
                {
                    recoverShieldTimer = 0;
                    Character.BaseCharacter.Data.CurrentShield++;
                }
                return Character.BaseCharacter.Data.CurrentShield >= Character.BaseCharacter.Data.Shield;
            }));
    }

    public void StopRecoverShield()
    {
        if (recovelShieldCoroutine != null)
            StopCoroutine(recovelShieldCoroutine);
    }
}
