
public class EcolocalizationAbility : BaseAbilityCast
{ 

    public override bool Cast(bool ignoreLoad)
    {
        if (!base.Cast(ignoreLoad))
            return false;
        CoroutineHandler.ExecuteActionAfter(() => EndCast(), Constants.halfSecondDelay, this);
        return true;
    }
    protected override void EndCast()
    {
        base.EndCast();
        EndCastAbility?.Invoke();
    }

}
