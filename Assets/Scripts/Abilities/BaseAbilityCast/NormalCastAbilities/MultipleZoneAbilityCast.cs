using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class MultipleZoneAbilityCast : MultipleAbilityCast
{
    [SerializeField] private Vector2[] positions;
    [SerializeField] private bool considerCurrentPosition;
    private Vector3[] lastPositions;
    private Vector3[] currentPositions;
    private List<Vector3> availablePositions;
    private Vector3 startPosition;
    public override void Initialize(Character character)
    {
        base.Initialize(character);
        int storePositionsAmount = amountOfAbilitiesToThrow;
        if (storePositionsAmount > positions.Length)
            storePositionsAmount = positions.Length;
        currentPositions = new Vector3[storePositionsAmount];
        lastPositions = new Vector3[storePositionsAmount];
        availablePositions = new List<Vector3>();
        startPosition = character.transform.position;
    }
    public override bool Load()
    {
        lastPositions = currentPositions;
        SetFreePositions(currentPositions);
        return base.Load();
    }
    protected override void SetTransformPositionRotationForThrow()
    {
        base.SetTransformPositionRotationForThrow();
        currentPositions[index] = castPositions[index];
        CurrentBehaviour.transform.forward = Data.Direction;
    }
    private void SetFreePositions(Vector3[] unavailablePositions)
    {
        availablePositions.Clear();
        for (int i = 0; i < positions.Length; i++)
        {
            if (Array.IndexOf(unavailablePositions, positions[i]) != -1)
                continue;
            availablePositions.Add(positions[i]);
        }
    }
    protected override Vector3 GetCastPosition()
    {
        Vector3 offset = considerCurrentPosition ? Character.transform.position : startPosition;
        if (availablePositions.Count == 0)
            SetFreePositions(lastPositions);
        int index = UnityEngine.Random.Range(0, availablePositions.Count - 1);
        Vector3 position = availablePositions[index];
        availablePositions.RemoveAt(index);
        return position + offset;
    }

#if UNITY_EDITOR
    [CustomEditor(typeof(MultipleZoneAbilityCast))]
    public class GesturesEditor : Editor
    {
        protected virtual void OnSceneGUI()
        {
            MultipleZoneAbilityCast multipleZoneAbilityCast = (MultipleZoneAbilityCast)target;
            for (int i = 0; i < multipleZoneAbilityCast.positions.Length; i++)
            {
                Handles.color = Color.cyan;
                multipleZoneAbilityCast.positions[i] = Handles.PositionHandle((Vector3)multipleZoneAbilityCast.positions[i] + multipleZoneAbilityCast.transform.position, Quaternion.identity) - multipleZoneAbilityCast.transform.position;
            }
        }
    }
#endif
}

