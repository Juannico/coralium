using UnityEngine;

public class ProjectileAbilityCast : BaseAbilityCast
{
    protected PoolHandler pool;
    [SerializeField] protected BaseLoadCastVisualEffect baseCastEffect;
    [SerializeField] protected bool waitForEndBehaviour = false;
    protected BaseLoadCastVisualEffect castEffect;
    protected GameObject parentCastEffect;
    public override void Initialize(Character character)
    {
        base.Initialize(character);
        if (baseCastEffect == null)
            return;
        baseCastEffect.gameObject.SetActive(false);
        castEffect = Instantiate(baseCastEffect);
        castEffect.Initialize(this);
        parentCastEffect = new GameObject($"CastEffects_{castEffect.name}");
        parentCastEffect.transform.parent = ParentObject.transform;
        castEffect.transform.parent = parentCastEffect.transform;

    }
    protected override void CreateBehaviour(BaseAbilityBehaviour baseAbilityBehaviour)
    {
        pool = gameObject.AddComponent<PoolHandler>();
        int amountToPool = GetAmountToPool();
        if (amountToPool < 0)
            amountToPool = 0;
        amountToPool++;
        GameObject behaviourParent = new GameObject($"Behaviours_{baseAbilityBehaviour.name}");
        behaviourParent.transform.parent = ParentObject.transform;
        pool.CreatePool(baseAbilityBehaviour.gameObject, amountToPool, behaviourParent.transform, true);
    }

    protected virtual int GetAmountToPool()
    {
        return Mathf.CeilToInt(Data.Duration / Data.Cooldown);
    }
    public override bool Load()
    {
        bool state = base.Load();
        SetLoadEffect();
        return state;

    }
    protected virtual void SetLoadEffect()
    {
        if (castEffect == null)
            return;
        castEffect.transform.position = GetCastPosition();
        castEffect.transform.forward = GetCastDirection();
        castEffect.StartEffect(Data.LoadTime);
    }
    public override bool Cast(bool ignoreLoad)
    {
        castEffect?.EndEffect();
        if (!base.Cast(ignoreLoad))
            return false;
        if (waitForEndBehaviour)
        {
            CurrentBehaviour.OnStopThrowing = () => EndCastAbility?.Invoke();
            return true;
        }
        CoroutineHandler.ExecuteActionAfter(() => EndCastAbility?.Invoke(), Constants.halfSecondDelay, this);
        return true;
    }
    public override void CancelCast()
    {
        base.CancelCast();
        castEffect?.DisruptEffect();
    }
    protected override BaseAbilityBehaviour GetCurrentBehaviour()
    {
        BaseAbilityBehaviour baseAbilityBehaviour = pool.GetObject().GetComponent<BaseAbilityBehaviour>();
        return baseAbilityBehaviour;
    }
    protected override void SetTransformPositionRotationForThrow()
    {
        CurrentBehaviour.transform.position = GetCastPosition();
        CurrentBehaviour.transform.forward = GetCastDirection();
    }
    protected virtual Vector3 GetCastPosition() => Character.transform.position + Data.Direction * 2.5f;
    protected virtual Vector3 GetCastDirection() => Data.Direction;
}
