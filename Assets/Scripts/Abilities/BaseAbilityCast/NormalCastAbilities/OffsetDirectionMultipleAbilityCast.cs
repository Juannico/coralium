using UnityEngine;

public class OffsetDirectionMultipleAbilityCast : MultipleAbilityCast
{
    [SerializeField] [Range(0, 360)] private float rangeOffset;
    private Quaternion[] offsetDirection;
    private float angle;
    public override void Initialize(Character character)
    {
        base.Initialize(character);
        angle = rangeOffset / 2;
        offsetDirection = new Quaternion[amountOfAbilitiesToThrow];
    }
    public override bool Load()
    {
        return base.Load();
    }
    protected override void SetLoadEffectByIndex(int i)
    {
        base.SetLoadEffectByIndex(i);
        offsetDirection[i] = Quaternion.Euler(0, 0, Random.Range(-angle, angle));
        if (castEffects[i] != null)
            castEffects[i].transform.forward = offsetDirection[i] * castEffects[i].transform.forward;
        
    }
    protected override void SetTransformPositionRotationForThrow()
    {
        base.SetTransformPositionRotationForThrow();
        if (index != 0)
            CurrentBehaviour.transform.forward = offsetDirection[index] * Data.Direction;
    }
}
