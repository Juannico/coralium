using FMODUnity;
using UnityEngine;

public class ShootSphereAbility : ProjectileAbilityCast
{
    [SerializeField] private StudioEventEmitter loadSFX;
    private DashEffectController shootSphereEffectController;
    private LoadBar dashBar;

    public LoadBar DashBar
    {
        get
        {
            if (dashBar == null)
            {
                dashBar = FindObjectOfType<LoadBar>();
            }
            return dashBar;
        }
        private set => dashBar = value;
    }
    public override void Initialize(Character character)
    {
        base.Initialize(character);
        shootSphereEffectController = character.GetComponentInChildren<DashEffectController>(true);
        if (DashBar != null)
            DashBar.gameObject.SetActive(false);
    }
    public override bool Load()
    {
        if (!base.Load())
            return false;
        DashBar.StartAutoLoad(Data.LoadTime);
        shootSphereEffectController.Charge();
        ChargeSFX(0);
        return true;
    }
    public override bool Loaded()
    {
        if (!base.Loaded())
            return false;
        shootSphereEffectController.StopCharge();
        return true;
    }
    public override bool Cast(bool ignoreLoad)
    {
        ChargeSFX(-1);
        if (!base.Cast(ignoreLoad))
            return false;
        CoroutineHandler.ExecuteActionAfter(() => EndCast(), Data.Cooldown * Constants.halfSecondDelay, this);
        return true;
    }
    private void ChargeSFX(float parameterValue)
    {
        if (loadSFX == null)
            return;
        if (parameterValue < 0)
        {
            loadSFX.Stop();
            return;
        }
        loadSFX.SetParameter("Hold & Release", parameterValue);
        if (!loadSFX.IsPlaying())
            loadSFX.Play();
    }
}
