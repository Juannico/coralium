using UnityEngine;

public class AreaAbilityCast : ProjectileAbilityCast
{
    public override void Initialize(Character character)
    {
        Data.Duration = Data.Distance / Data.Speed;
        base.Initialize(character);
    }

    public override bool Cast(bool ignoreLoad)
    {
        if(!base.Cast(ignoreLoad))
            return false;
        CoroutineHandler.ExecuteActionAfter( ()=> EndCast(), Data.Duration,this);
        return true;
    }
    protected override void SetTransformPositionRotationForThrow()
    {
        CurrentBehaviour.transform.position = Data.castPosition == Vector3.zero ? Character.transform.position : Data.castPosition;
    }
}
