
public class ShootAbilityCast : ProjectileAbilityCast
{
    public override bool Cast(bool ignoreLoad)
    {
        if (!base.Cast(ignoreLoad))
            return false;
        CoroutineHandler.ExecuteActionAfter(() => EndCast(), Data.Cooldown * Constants.halfSecondDelay, this);
        return true;
    }
}
