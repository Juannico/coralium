
using System;
using UnityEngine;

public class DashAbilityCast : BaseAbilityCast
{
    private DashEffectController dashEffectControl;
    public Action OnLungeActivated;
    public override void Initialize(Character character)
    {
        base.Initialize(character);
        dashEffectControl = character.GetComponentInChildren<DashEffectController>(true);
        Data.OnSwitchLockAbility += (isLocked) => isLocked = false;
        //Data.Cooldown = Data.Duration * 1.1f;
        dashEffectControl.StopEffect();
    }
    protected override void SetAction()
    {
        if (!Data.HasInputAction)
            return;
        Data.InputReference.InputReferenceData.InputActionInteractionData.InputActionReference = Data.InputReference.InputReferenceData.InputActionReference;

        Data.InputReference.InputReferenceData.InputAdded = () =>
        {
            Load();
            Loaded();
            Cast(false);

        };
        Data.InputReference.InputReferenceData.InputActioned = () =>
        {
            if (!(CurrentBehaviour as DashBehaviour).LungeActivated)
                dashEffectControl.DisruptCharge();
            Character.StopImpulse();
        };

    }

    public override bool Load()
    {
        Timer = Data.Cooldown;

        return base.Load();
    }
    public override void CancelCast()
    {
        base.CancelCast();
        dashEffectControl.DisruptCharge();
    }
    public override bool Cast(bool ignoreLoad)
    {
        if (!base.Cast(ignoreLoad))
            return false;
        dashEffectControl.Dash();
        dashEffectControl.Charge();
        Character.OnImpulse(Data.Duration, Data.Distance, ActiveLunge);
        Character.BaseCharacter.Data.EndStateAction = () =>
        {
            if (!(CurrentBehaviour as DashBehaviour).LungeActivated)
                dashEffectControl.StopCharge();
            dashEffectControl.StopEffect();
            CurrentBehaviour.Disrupt();
            EndCastAbility?.Invoke();
        };
        CoroutineHandler.ExecuteActionAfter(() => EndCast(), Data.Cooldown, this);
        return true;
    }

    private void ActiveLunge()
    {
        dashEffectControl.StopCharge();
        dashEffectControl.Lunge();
        (CurrentBehaviour as DashBehaviour).LungeActivated = true;
        OnLungeActivated?.Invoke();
    }
}
