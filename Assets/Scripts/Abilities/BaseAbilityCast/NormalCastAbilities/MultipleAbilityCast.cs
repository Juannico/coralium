using UnityEngine;

public class MultipleAbilityCast : ProjectileAbilityCast
{
    [SerializeField] protected int amountOfAbilitiesToThrow;
    [SerializeField] private float forwardDistance = 1;
    [SerializeField] private Vector3 castArea;
    [SerializeField] private bool showGuide = false;
    protected Vector3[] castPositions;
    protected BaseLoadCastVisualEffect[] castEffects;
    protected int index;

    public override void Initialize(Character character)
    {
        base.Initialize(character);
        castPositions = new Vector3[amountOfAbilitiesToThrow];
        castEffects = new BaseLoadCastVisualEffect[amountOfAbilitiesToThrow];
        if (baseCastEffect == null)
            return;
        for (int i = 0; i < castEffects.Length; i++)
        {
            if (i == 0)
            {
                castEffects[i] = castEffect;
                castEffects[i].name = $"{baseCastEffect.name}_{i}";
                castEffects[i].transform.localScale = Data.BehaviourPrefab.transform.localScale;
                castEffects[i].transform.parent = parentCastEffect.transform;
                castEffect = null;
                continue;
            }
            castEffects[i] = Instantiate(baseCastEffect);
            castEffects[i].name = $"{baseCastEffect.name}_{i}";
            castEffects[i].transform.localScale = Data.BehaviourPrefab.transform.localScale;
            castEffects[i].transform.parent = parentCastEffect.transform;
            castEffects[i].Initialize(this);
        }
    }
    public override bool Load()
    {
        index = 0;
        return base.Load();
    }
    public override bool Cast(bool ignoreLoad)
    {
        castEffects[index]?.EndEffect();
        if (!base.Cast(ignoreLoad))
            return false;
        index++;
        if (index == 1)
            CastAllOtherAbilities();
        return true;
    }

    private void CastAllOtherAbilities()
    {
        for (int i = 1; i < amountOfAbilitiesToThrow; i++)
            this.Cast(true);
        CoroutineHandler.ExecuteActionAfter(() => EndCast(), Data.Cooldown * Constants.halfSecondDelay, this);
    }

    public override void CancelCast()
    {
        base.CancelCast();
        for (int i = 0; i < castEffects.Length; i++)
            castEffects[i]?.DisruptEffect();
    }
    protected override void SetTransformPositionRotationForThrow()
    {
        base.SetTransformPositionRotationForThrow();
        CurrentBehaviour.transform.position = castPositions[index];
        CurrentBehaviour.transform.forward = Data.Direction;
    }
    protected override int GetAmountToPool()
    {
        return base.GetAmountToPool() * amountOfAbilitiesToThrow;
    }
    protected override void SetLoadEffect()
    {
        for (int i = 0; i < castPositions.Length; i++)
            SetLoadEffectByIndex(i);
    }

    protected virtual void SetLoadEffectByIndex(int i)
    {
        Vector3 position = GetCastPosition();
        position.z = 0;
        Vector3 direction = GetCastDirection();
        if (baseCastEffect != null)
        {
            castEffects[i].transform.position = position;
            castEffects[i].transform.forward = direction;
            castEffects[i].StartEffect(Data.LoadTime);
        }
        castPositions[i] = position;
    }

    protected override Vector3 GetCastPosition()
    {
        Vector2 randomPoint = MathUtilities.RandomVector2(castArea / -2, castArea / 2);
        Vector3 direction = MathUtilities.RotatePointFromVector2Direction(randomPoint, Character.BaseTransform.forward);
        return direction + Character.BaseTransform.forward + Character.transform.position + Data.Direction * 2.5f * forwardDistance;
    }
    private void OnDrawGizmos()
    {
        if (!showGuide)
            return;
        Gizmos.color = Color.red;
        Gizmos.DrawCube(transform.position + transform.forward * forwardDistance, castArea);
    }

}
