using System.Collections.Generic;
using UnityEngine;

/*
 * The way this FX works is that it will activate/deactivate based on the ability it got configured onto. 
 * The ability will send events so that this FX can handle as many enemies are requires.
*/
public class AbilityFX_LoadEnemySpawn : BaseCharacterAbilityFX
{
    [SerializeField] private GameObject spawnFxPrefab;

    private Dictionary<int, BaseFX> spawnedEffects;

    protected override void OnAwake()
    {
        base.OnAwake();

        spawnedEffects = new Dictionary<int, BaseFX>();
    }

    public override bool ReceiveAbilityEvent(string eventName, AbilityParams eventParams)
    {
        bool result = base.ReceiveAbilityEvent(eventName, eventParams);

        if (eventName == "fx.EnemySpawnRequest")
        {
            int id = eventParams.intValue;
            Vector3 location = eventParams.vector1Value;
            Quaternion rotation = Quaternion.Euler(eventParams.vector2Value);
            GameObject prefab = (GameObject)eventParams.object1Value;

            BeginNewSpawnEffect(id, location, rotation, prefab);

            result = true;
        }

        return result;
    }

    private void BeginNewSpawnEffect(int id, Vector3 location, Quaternion rotation, GameObject modelPrefab)
    {
        if (spawnFxPrefab == null)
        {
            Debug.LogError("AbilityFX_LoadEnemySpawn::BeginNewSpawnEffect: spawnFxPrefab is null.");
            return;
        }

        GameObject effectGO = Instantiate(spawnFxPrefab, location, rotation);

        BaseFX fx = effectGO.GetComponent<BaseFX>();

        IGameObjectHolder gameObjectHolder = (IGameObjectHolder)fx;
        
        if (gameObjectHolder != null)
        {
            gameObjectHolder.SetGameObject(modelPrefab);
        }
        else
        {
            Debug.LogError("AbilityFX_LoadEnemySpawn::BeginNewSpawnEffect: modelPrefab is null.");
            DeactivateFX();
            return;
        }

        spawnedEffects.Add(id, fx);
        
        // This effect must deactivate on its own
        fx.onEffectDeactivated += OnSpawnEffectDeactivated;

        fx.ActivateFX();
    }

    private void OnSpawnEffectDeactivated(BaseFX fx)
    {
        int idFound = -1;

        foreach(KeyValuePair<int, BaseFX> pair in spawnedEffects)
        {
            if (pair.Value == fx)
            {
                AbilityParams eventParams = new AbilityParams();

                // This is the id, which is required to know which effect finished so that the correct character gets spawned
                eventParams.intValue = pair.Key;

                SendAbilityFxEvent("fx.SpawnEffectCompleted", eventParams);
                break;
            }
        }

        if (idFound >= 0)
        {
            spawnedEffects.Remove(idFound);
        }

        fx.onEffectDeactivated -= OnSpawnEffectDeactivated;
    }
}
