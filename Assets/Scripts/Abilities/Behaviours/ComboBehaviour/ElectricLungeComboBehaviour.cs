using UnityEngine;

public class ElectricLungeComboBehaviour : DashBehaviour, ISetSecondAbilityBehaviour
{
    [HideInInspector] private BaseAbilityCast secondAbility;
    public void SetSecondAbility(BaseAbilityCast secondAbility) => this.secondAbility = secondAbility;
    protected override bool OnSuccessfulTriggerEnter(Collider other)
    {
        if (base.OnSuccessfulTriggerEnter(other))
        {
            CastSecondAbility(other.ClosestPoint(transform.position));
            return true;
        }
        return false;
    }
    protected override bool OnAbilityCollisionEnter(Collision collision)
    {
        secondAbility?.Cast(true);
        return base.OnAbilityCollisionEnter(collision);
    }

    public void CastSecondAbility(Vector3 castPosition)
    {

        if (secondAbility != null)
            secondAbility.Data.castPosition = castPosition;
        secondAbility?.Cast(true);
        if (secondAbility != null)
            secondAbility.Data.castPosition = Vector3.zero;
    }
}
