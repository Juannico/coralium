using UnityEngine;

public class ElectricShootComboBehaviour : ShootBehaviour, ISetSecondAbilityBehaviour
{
    [HideInInspector] private BaseAbilityCast secondAbility;
    [SerializeField] private bool stopContact = false;
    public void SetSecondAbility(BaseAbilityCast secondAbility) => this.secondAbility = secondAbility;
    protected override bool OnSuccessfulTriggerEnter(Collider other)
    {
        bool state = base.OnSuccessfulTriggerEnter(other);
        if (stopContact)
        {
            CastSecondAbility(other.ClosestPoint(transform.position));
            state = true;
        }
        return state;
    }
    public void CastSecondAbility(Vector3 castPosition)
    {
        if (secondAbility != null)
            secondAbility.Data.castPosition = castPosition;
        secondAbility?.Cast(true);
        if (secondAbility != null)
            secondAbility.Data.castPosition = Vector3.zero;
    }
}
