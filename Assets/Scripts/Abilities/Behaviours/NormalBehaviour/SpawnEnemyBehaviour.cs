using UnityEditor;
using UnityEngine;

public class SpawnEnemyBehaviour : SpawnBehaviour
{
    [SerializeField] private bool stopOnDied = true;

    protected override void Spawn()
    {
        base.Spawn();
        currentObjectSpawned.transform.rotation = Quaternion.identity;
        currentObjectSpawned.transform.GetChild(0).rotation = transform.rotation;
        EnemyController currentObjectSpawnedController = currentObjectSpawned.GetComponentInChildren<EnemyController>();
        EnemyController casterController = Ability.Character.GetComponent<EnemyController>();
        currentObjectSpawnedController.ActiveChase.gameObject.SetActive(false);
        currentObjectSpawnedController.ActiveChase = casterController.ActiveChase;
        currentObjectSpawnedController.IsFocusingPlayer = true;
        currentObjectSpawnedController.SetStateDataDirectionValues(Vector3.zero, Vector3.zero);
        currentObjectSpawnedController.Character.BaseCharacter.Data.OnCharacterDead += StopThrowing;
        if (stopOnDied)
            casterController.Character.BaseCharacter.Data.OnCharacterDead += StopThrowing;
    }

    protected override void StopThrowing()
    {
        if (currentObjectSpawned == null)
            return;
        EnemyCharacter currentObjectSpawnedController = currentObjectSpawned.GetComponentInChildren<EnemyCharacter>();
        currentObjectSpawnedController.Died();
        base.StopThrowing();
    }
}
