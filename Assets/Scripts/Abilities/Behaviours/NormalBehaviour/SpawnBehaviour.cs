using UnityEngine;

public class SpawnBehaviour : BaseAbilityBehaviour
{
    [field:SerializeField]public GameObject ObjectToSpawn { get; private set; }
    protected GameObject currentObjectSpawned;
    public override void Throw(BaseAbilityCast ability)
    {
        base.Throw(ability);
        Spawn();
    }
    protected virtual void Spawn()
    {
        if (currentObjectSpawned != null)
            Destroy(currentObjectSpawned);
        currentObjectSpawned = Instantiate(ObjectToSpawn, transform.position, transform.rotation,transform);
    }
    protected override void StopThrowing()
    {
        if (currentObjectSpawned == null)
            return;
        GameObject objetToDestoy = currentObjectSpawned;
        Destroy(objetToDestoy,0);
        currentObjectSpawned = null;
        base.StopThrowing();
    }
}

