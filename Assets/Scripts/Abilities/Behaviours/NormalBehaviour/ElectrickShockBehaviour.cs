using FMODUnity;
using UnityEngine;

public class ElectrickShockBehaviour : BaseAbilityBehaviour
{
    [Header("SFX")]
    [SerializeField] private StudioEventEmitter shockSFX;
    private Vector3 startScale;

    protected override void Awake()
    {
        startScale = transform.localScale;
    }
    public override void Throw(BaseAbilityCast ability)
    {
        base.Throw(ability);
        vfx?.StartEffect(ability.Data.Duration / 2.75f);
        shockSFX.Play();
        Vector3 startCourutineScale = startScale;
        float timer = 0;
        StartCoroutine(CoroutineHandler.ExecuteActionAfterUpdateUntil(() =>
        {
            timer += Time.deltaTime;
            transform.localScale = Vector3.Lerp(startScale, startScale * ability.Data.Distance, timer / ability.Data.Duration);
            return timer >= ability.Data.Duration;
        }, () => StopThrowing()));
    }
    protected override Vector3 GetDamageDirection(GameObject objectToDoDamage) => (objectToDoDamage.transform.position - transform.position).normalized;

}
