using FMODUnity;
using UnityEngine;

public class DashBehaviour : BaseAbilityBehaviour
{
    [Header("Dash settings")]
    [SerializeField] protected float knockBackDuration = 0.5f;
    [Header("Lunge settings")]
    [SerializeField] protected float OnLungeMultiplier = 2;
    [Header("Dash VFX")]
    [SerializeField] protected GPUParticleSystem dashCrashVfx;
    [SerializeField] private bool useVFXOnTrigger;
    [Header("Dash SFX")]
    [SerializeField] private StudioEventEmitter throwVfx;
    [SerializeField] private StudioEventEmitter dashCrashSFX;
    protected Vector2 knockBackDirection;
    private bool setDashCrahSFX = false;
    [HideInInspector] public bool LungeActivated;
    private StateSharedData staetSharedData = null;
    private Coroutine knockBackCorutine;
    private GameObject lastColiderOnKnockback;

    protected override void Start()
    {
        base.Start();
        dashCrashVfx = Instantiate(dashCrashVfx, transform).GetComponent<GPUParticleSystem>();
        dashCrashVfx.transform.parent = null;
        CollisionHelper.CollisionEnter += OnCollision;
        CollisionHelper.CollisionStay += OnCollision;
    }

    private void OnCollision(Collision collision)
    {
        if (!gameObject.activeInHierarchy)
            return;
        if (IsCollidingWithAbilityCaster(collision.gameObject))
            return;
        if (IsCollidingWithSameLayer(collision.gameObject))
            return;
        if (!OnAbilityCollisionEnter(collision))
            return;
        OnKnockBack(collision);
        StopThrowing();
    }
    public override void Throw(BaseAbilityCast ability)
    {
        base.Throw(ability);
        throwVfx?.Play();
        if (!setDashCrahSFX && dashCrashSFX != null)
        {
            setDashCrahSFX = true;
            dashCrashSFX.transform.parent = ability.Character.transform;
        }
        LungeActivated = false;
        staetSharedData = ability.Character.GetComponent<StateSharedData>();
    }
    protected virtual bool OnAbilityCollisionEnter(Collision collision)
    {
        if (collision.gameObject.TryGetComponent(out IDashable dashable))
        {
            bool didInteraction = dashable.MakeInteraction(out bool shoouldBounce);
            if (shoouldBounce)
            {
                if (lastColiderOnKnockback == collision.gameObject)
                    return !didInteraction;
                lastColiderOnKnockback = collision.gameObject;
                Vector2 bounceDirection = Vector2.Reflect(Ability.Character.BaseTransform.forward, collision.GetContact(0).normal);
                float timer = 0;
                if (knockBackCorutine != null)
                    StopCoroutine(knockBackCorutine);
                knockBackCorutine = StartCoroutine(CoroutineHandler.ExecuteActionAfterUpdateUntil(() =>
                    {
                        timer += Time.deltaTime;
                        staetSharedData.MovementDirection *= timer / knockBackDuration;
                        staetSharedData.MovementDirection += (Vector3)bounceDirection;
                        staetSharedData.MovementDirection.Normalize();
                        return !gameObject.activeInHierarchy || timer > knockBackDuration;
                    }, () =>
                    {
                        staetSharedData.MovementDirection = bounceDirection;
                        lastColiderOnKnockback = null;
                    }));
            }
            return !didInteraction;
        }
        DoDamage(collision.gameObject);
        ApplyEffect(collision.gameObject);
        return true;
    }

    protected override bool OnSuccessfulTriggerEnter(Collider other)
    {
        if (!base.OnSuccessfulTriggerEnter(other))
            return false;
        OnKnockBack(other);
        return true;
    }
    protected virtual void OnKnockBack(Collision collision)
    {
        dashCrashVfx.transform.position = collision.GetContact(0).point;
        dashCrashVfx.Restart();
        dashCrashVfx.Play();
        knockBackDuration = collision.gameObject.TryGetComponent(out BossEnemyCharacter bossEnemyCharacter) ? bossEnemyCharacter.PlayerDamageKnockBackDuration : knockBackDuration;
        knockBackDirection = Vector2.Reflect(Ability.Character.BaseTransform.forward, collision.GetContact(0).normal);
        Ability.Character.BaseCharacter.Data.OnCharacterKnockBack?.Invoke(knockBackDirection, knockBackDuration * (LungeActivated ? 2 : 1), true);
    }
    protected virtual void OnKnockBack(Collider other)
    {
        if (useVFXOnTrigger)
        {
            dashCrashVfx.transform.position = Ability.Character.transform.position - (Vector3)knockBackDirection;
            dashCrashVfx.Restart();
            dashCrashVfx.Play();
        }
        knockBackDirection = ColliderUtilities.GetContactDirection(other, Caster.transform.position);
        Ability.Character.BaseCharacter.Data?.OnCharacterKnockBack?.Invoke(-knockBackDirection, knockBackDuration, true);
    }
}
