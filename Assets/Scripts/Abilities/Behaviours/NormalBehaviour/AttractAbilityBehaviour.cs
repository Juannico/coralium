using UnityEditor;
using UnityEngine;

public class AttractAbilityBehaviour : BaseAbilityBehaviour
{
    private float strength = 5;
    [SerializeField] [Range(0, 1)] private float minStrengthMultiplier = 0.5f;
    [SerializeField] [Range(0, 180)] private float angle = 30f;
    [SerializeField] private LayerMask targetMask;
    [SerializeField] private int maxAmountToTargets = 10;
    [SerializeField] private bool showGuide;
    private float maxDistance = 50f;
    private Collider[] hitColliders;
    private Transform lastParent;

    private InhaleVFX inhaleVFX;
    protected override void Awake()
    {
        base.Awake();
        hitColliders = new Collider[maxAmountToTargets];
        inhaleVFX = vfx as InhaleVFX;
        if (inhaleVFX != null)
            inhaleVFX.Radius = angle / 2;
    }
    void FixedUpdate()
    {

        int hitsNumber = Physics.OverlapSphereNonAlloc(transform.position, maxDistance, hitColliders, targetMask);
        if (hitsNumber == 0)
            return;
        for (int i = 0; i < hitsNumber; i++)
        {
             Vector3 directionToTarget = hitColliders[i].transform.position - transform.position;
            if (!TargetUtilities.IsInFOV(transform.forward, directionToTarget, angle))
                continue;
            float fixedStrength = strength * Mathf.Lerp(minStrengthMultiplier, 1, directionToTarget.magnitude / maxDistance);
            RigibodyForceHandle rigibodyForceHandle = hitColliders[i].GetComponentInParent<RigibodyForceHandle>();
            if (rigibodyForceHandle != null)
                rigibodyForceHandle.ExternalVelocityForceVector += directionToTarget.normalized * -fixedStrength;
        }
    }

    public override void Throw(BaseAbilityCast ability)
    {
        base.Throw(ability);
        enabled = true;
        vfx.StartEffect();
        maxDistance = ability.Data.Distance * ability.Character.Model.transform.localScale.x;
        strength = ability.Data.Speed * ability.Character.Model.transform.localScale.x;
        inhaleVFX.Size =  maxDistance;
        lastParent = transform.parent;
        transform.parent = Caster.transform;
        transform.localPosition = Vector3.zero;
        //transform.localRotation = Quaternion.identity;
        CoroutineHandler.ExecuteActionAfter(() => StopThrowing(), ability.Data.Duration * 0.9f, this);
    }
    protected override void StopThrowing()
    {
        transform.parent = lastParent;
        vfx.EndEffect();

        enabled = false;
        CoroutineHandler.ExecuteActionAfter(() => base.StopThrowing(), Ability.Data.Duration * 0.075f, this);
    }
#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        if (!showGuide)
            return;
        Handles.color = Color.yellow;
        float radius = Mathf.Sin(angle * 0.5f * Mathf.Deg2Rad) * 2;
        radius *= vfx.transform.localScale.x;
        Vector3 targetDistance = transform.position + transform.forward.normalized * 2;
        targetDistance *= vfx.transform.localScale.x;
        Handles.DrawWireDisc(targetDistance, Vector3.forward, radius);

        Gizmos.color = Color.yellow;
        Gizmos.DrawLine(transform.position, targetDistance + transform.up * radius);
        Gizmos.DrawLine(transform.position, targetDistance - transform.up * radius);
        Gizmos.DrawLine(transform.position, targetDistance + transform.right * radius);
        Gizmos.DrawLine(transform.position, targetDistance - transform.right * radius);
        Gizmos.DrawWireSphere(transform.position,1);
    }
#endif
}
