using System.Collections.Generic;
using UnityEngine;

public class ConsecutiveDynamiteBehaviour : BaseAbilityBehaviour
{
    [SerializeField] private ExplosiveObject dynamitePrefab;
    private List<ExplosiveObject> dynamites = new List<ExplosiveObject>();
    [SerializeField] private float delayCast = 2;
    [SerializeField] private GameObject dynamiteStartPositionParent;
    private Transform[] dynamiteStartPositions;
    [SerializeField] private float amountOfDynamitesPerCast = 5;
    private int dynamiteCounter;
    private float timer = 0;
    private Transform flockTransform;
    private Transform targetTransform;

    private float dynamiteVelocity;
    protected override void Awake()
    {
        base.Awake();
        for (var i = 0; i < amountOfDynamitesPerCast; i++)
        {
            ExplosiveObject explosiveObject = Instantiate(dynamitePrefab, transform.parent);
            dynamites.Add(explosiveObject);
        }
        targetTransform = new GameObject().transform;
        flockTransform = targetTransform;
        dynamiteStartPositions = new Transform[dynamiteStartPositionParent.transform.childCount];
        for (var i = 0; i < dynamiteStartPositions.Length; i++)
            dynamiteStartPositions[i] = dynamiteStartPositionParent.transform.GetChild(i);
    }
    public override void Throw(BaseAbilityCast ability)
    {
        base.Throw(ability);
        timer = 0;
        dynamiteCounter = 0;
        targetTransform = ability.Character.GetComponent<StateSharedData>().CurrentFollowObject.transform;
        dynamiteVelocity = ability.Data.Speed;
        DropDynamite();
    }
    
    private void Update()
    {
        timer += Time.deltaTime;
        if (timer < delayCast)
            return;
        DropDynamite();
        if(dynamiteCounter > amountOfDynamitesPerCast )
            StopThrowing();
        timer = 0;
    }

    private void DropDynamite()
    {
        dynamiteCounter++;
        Vector3 startPositon = dynamiteStartPositions[0].position;
        float minDistance = Vector3.Distance(startPositon, targetTransform.position);
        for (var i = 1; i < dynamiteStartPositions.Length; i++)
        {
            float distance = Vector3.Distance(dynamiteStartPositions[i].position, targetTransform.position);
            if (distance < minDistance)
            {
                minDistance = distance;
                startPositon = dynamiteStartPositions[i].position;
            }
        }
        Vector3 startPoisition = startPositon + (startPositon - transform.position).normalized + Random.insideUnitSphere;
        startPoisition.y = startPositon.y;
        Vector3 targetPosition = targetTransform.position;
        for (var i = 0; i < dynamites.Count; i++)
        {
            if (dynamites[i].gameObject.activeInHierarchy)
                continue;
            dynamites[i].gameObject.SetActive(true);
            dynamites[i].StartToMove(transform.position, startPoisition, targetPosition, dynamiteVelocity);
            return;
        }
        ExplosiveObject explosiveObject = Instantiate(dynamitePrefab, transform.parent);
        explosiveObject.gameObject.SetActive(true);
        explosiveObject.StartToMove(transform.position, startPoisition, targetPosition, dynamiteVelocity);
        dynamites.Add(explosiveObject);
    }
}
