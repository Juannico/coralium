using UnityEngine;

public class SphereFollowBehaviour : BaseAbilityBehaviour
{
    [SerializeField] private RigibodyForceHandle rigibodyForceHandle;

    private Transform targetTransform;
    protected override void Awake()
    {
        base.Awake();
        rigibodyForceHandle = ComponentUtilities.SetComponent<RigibodyForceHandle>(gameObject);
        rigibodyForceHandle.Initialize(gameObject.GetComponent<Rigidbody>());
    }
    public override void Throw(BaseAbilityCast ability)
    {
        base.Throw(ability);
        targetTransform = ability.Character.GetComponent<StateSharedData>().CurrentFollowObject.transform;
        vfx?.StartEffect();
        CoroutineHandler.ExecuteActionAfter(() => StopThrowing(), ability.Data.Duration, this);
    }
    private void FixedUpdate()
    {
        if (targetTransform == null)
            return;
        transform.forward = (targetTransform.position - transform.position).normalized;
        rigibodyForceHandle.VelocityForceVector += transform.forward * Ability.Data.Speed;
    }
    protected override void StopThrowing()
    {
        vfx?.EndEffect();
        CoroutineHandler.ExecuteActionAfter(() => base.StopThrowing(), Constants.halfSecondDelay, this);
    }

}
