using UnityEngine;

public class BasicShootAbilityBehaviour : BaseAbilityBehaviour
{
    [SerializeField] private float offsetStart;
    [SerializeField] private bool scaleWithCaster;
    private Vector3 startScale;
    protected override void Awake()
    {
        base.Awake();
        startScale = transform.localScale;
    }
    public override void Throw(BaseAbilityCast ability)
    {
        base.Throw(ability);
        float scaleMultiplier = 1;
        if (scaleWithCaster)
        {
            scaleMultiplier = ability.Character.transform.lossyScale.magnitude / startScale.magnitude;
            transform.localScale = startScale * scaleMultiplier;
        }
        transform.position += transform.forward * offsetStart * scaleMultiplier;
        rb.velocity = Vector3.zero;
        rb.AddForce(transform.forward * ability.Data.Speed, ForceMode.VelocityChange);
        vfx?.StartEffect();
        CoroutineHandler.ExecuteActionAfter(() => StopThrowing(), ability.Data.Duration, this);
    }
    protected override bool OnSuccessfulTriggerEnter(Collider other)
    {
        if (base.OnSuccessfulTriggerEnter(other))
            return true;
        if (LayerUtilities.IsSameLayer(LayerData.EnviromenLayer, other.gameObject.layer))
            return true;
        return false;
    }

    protected override void StopThrowing()
    {
        vfx?.EndEffect();
        CoroutineHandler.ExecuteActionAfter(base.StopThrowing, Constants.halfSecondDelay, this);
    }
}
