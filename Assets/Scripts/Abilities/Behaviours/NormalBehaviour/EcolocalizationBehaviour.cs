using FMODUnity;
using UnityEngine;

public class EcolocalizationBehaviour : BaseAbilityBehaviour
{
    private Coroutine growCorutine;
    [SerializeField] private float markDuration;

    [Header("SFX")]
    [SerializeField]private StudioEventEmitter echoSFX;
    [SerializeField] private StudioEventEmitter allyFound;
    [SerializeField] private StudioEventEmitter enemyFound;

    private CameraController cameraController;
    protected override  void Awake()
    {
        echoSFX = GetComponent<StudioEventEmitter>();
        cameraController = GameObject.FindGameObjectWithTag(Tags.CameraController).GetComponent<CameraController>();
        cameraController.EcolocalizationCamera.SetActive(false);
    }
    public override void Throw(BaseAbilityCast ability)
    {
        base.Throw(ability);
        cameraController.EcolocalizationCamera.SetActive(true);
        echoSFX?.Play();
        transform.position = ability.Character.transform.position;
        transform.localScale = Vector3.one * 6;
        if (growCorutine != null)
            StopCoroutine(growCorutine);
        float Growing;
        float counter = 0;
        growCorutine = StartCoroutine(CoroutineHandler.ExecuteActionAfterUpdateUntil(() =>
        {
            counter += Time.deltaTime;
            Growing = ability.Data.Speed * Time.deltaTime * 2;
            transform.localScale = new Vector3(transform.localScale.x + Growing, transform.localScale.y + Growing, transform.localScale.z + Growing);
            return counter > ability.Data.Duration;
        }, () =>
        {
            transform.localScale = Vector3.zero;
            cameraController.EcolocalizationCamera.SetActive(false);
            gameObject.SetActive(false);
        }));
    }
    protected override bool OnSuccessfulTriggerEnter(Collider other)
    {
        if (LayerUtilities.IsSameLayer(LayerData.RadarPointLayer, other.gameObject.layer))
        {
            other.GetComponent<RadarPointMark>()?.Mark(markDuration);
            allyFound.Play();
        }
        if (LayerUtilities.IsSameLayer(LayerData.EnemyLayer, other.gameObject.layer))
            enemyFound.Play();
        return false;
    }
    protected override void StopThrowing() { }

    protected override bool IgnoreLayer(LayerMask ignorelayer) { return false; }

}
