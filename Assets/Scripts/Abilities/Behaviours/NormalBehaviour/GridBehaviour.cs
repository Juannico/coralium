using System.Collections;
using UnityEngine;
using System;

public class GridBehaviour : BaseAbilityBehaviour
{
    [SerializeField] GameObject grid;
    [SerializeField] CollisionHelper gridCollisionHelper;
    [SerializeField] private Transform start;
    [SerializeField] private Transform end;
    [SerializeField] [Range(0f, 2f)] private float startWeigth;
    [SerializeField] [Range(0f, 2f)] private float endWeigth;
    [SerializeField] [Range(0f, 2f)] private float targetWeigth;
    [SerializeField] private Flock flock;
    [SerializeField] private LineRenderer[] lineRenderers;
    private TrashHandle trashHandle;
    private Coroutine gridCoroutine;
    private float velocity;
    private bool firstThrow = true;
    protected override void Awake()
    {
        base.Awake();
        trashHandle = GetComponent<TrashHandle>();
        gridCollisionHelper.TriggerEnter += GridEnter;
    }
    private void DestroyFirstUnit()
    {
        FlockUnit unit = flock.AllUnit[0];
        unit.AssignedFlock.RemoveFlockUnit(unit);
        Destroy(unit.gameObject);
    }
    public override void Throw(BaseAbilityCast ability)
    {
        base.Throw(ability);
        velocity = ability.Data.Speed * 0.1f;
        if (firstThrow)
        {
            Invoke("DestroyFirstUnit", 0.1f);
            firstThrow = false;
        }
        gridCoroutine = StartCoroutine(MoveGrid(ability.Character.GetComponent<StateSharedData>().CurrentFollowObject.transform.position));
    }
    public override void Disrupt()
    {
        if (gridCoroutine != null)
            StopCoroutine(gridCoroutine);
        Vector3 startPosition = grid.transform.position;
        Vector3 targetPosition = start.position;
        if (Vector3.Distance(startPosition, targetPosition) > Vector3.Distance(startPosition, end.position))
            targetPosition = end.position;
        float interpolateAmount = 0;
        StartCoroutine(CoroutineHandler.ExecuteActionAfterUpdateUntil(() =>
        {
            interpolateAmount = Mathf.Clamp01(interpolateAmount + Time.deltaTime * velocity);
            grid.transform.position = Vector3.Lerp(startPosition, targetPosition, interpolateAmount);
            return interpolateAmount >= 1;
        }, () =>

        {
            SetLineRenderes(false);
            RemoveAllUnits();
            base.Disrupt();
        }));
    }
    private void RemoveAllUnits()
    {
        if (flock.AllUnit.Count == 0)
            return;
        FlockUnit[] flocksToDelete = flock.AllUnit.ToArray();
        for (var i = 0; i < flocksToDelete.Length; i++)
        {
            flock.RemoveFlockUnit(flocksToDelete[i], true);
            flocksToDelete[i].gameObject.SetActive(false);
        }
    }
    public IEnumerator MoveGrid(Vector3 targetPosition)
    {
        SetLineRenderes(true);
        grid.transform.position = start.position;
        trashHandle.SetTrashObjectInGrid();
        Vector3 direction1 = (start.position - transform.position) * 0.5f;
        direction1.y = 0;
        Vector3 curvePoint1 = MathUtilities.GetOppositeVectorInPlane(transform.position, start.position, targetPosition + direction1, startWeigth, targetWeigth);
        Vector3 direction2 = (end.position - transform.position) * 0.5f;
        direction2.y = 0;
        Vector3 curvePoint2 = MathUtilities.GetOppositeVectorInPlane(transform.position, end.position, targetPosition + direction2, endWeigth, targetWeigth);
        float interpolateAmount = 0;
        float totalDistance = GetTotalDistance(targetPosition, direction1, direction2);
        float fixedVelocity = velocity * GetVelocityPercentage(Vector3.Distance(start.position, targetPosition + direction1) * ((startWeigth + targetWeigth) / 2), totalDistance);
        while (interpolateAmount < 1)
        {
            interpolateAmount = Mathf.Clamp01(interpolateAmount + Time.deltaTime * fixedVelocity);
            grid.transform.position = MathUtilities.QuadraticLerp(start.position, curvePoint1, targetPosition + direction1, interpolateAmount);
            yield return null;
        }
        fixedVelocity = velocity * GetVelocityPercentage(Vector3.Distance(targetPosition + direction1, targetPosition + direction2), totalDistance);
        interpolateAmount = 0;
        trashHandle.GenerateTrash(UnityEngine.Random.Range(0.1f, 0.9f) / fixedVelocity);
        while (interpolateAmount < 1)
        {
            interpolateAmount = Mathf.Clamp01(interpolateAmount + Time.deltaTime * fixedVelocity * 2);
            grid.transform.position = Vector3.Lerp(targetPosition + direction1, targetPosition + direction2, interpolateAmount);
            yield return null;
        }
        fixedVelocity = velocity * GetVelocityPercentage(Vector3.Distance(end.position, targetPosition + direction2) * ((endWeigth + targetWeigth) / 2), totalDistance);
        interpolateAmount = 0;
        while (interpolateAmount < 1)
        {
            interpolateAmount = Mathf.Clamp01(interpolateAmount + Time.deltaTime * fixedVelocity);
            grid.transform.position = MathUtilities.QuadraticLerp(targetPosition + direction2, curvePoint2, end.position, interpolateAmount);
            yield return null;
        }
        transform.localPosition = Vector3.zero;
        RemoveAllUnits();
        SetLineRenderes(false);
        StopThrowing();
    }
    private float GetTotalDistance(Vector3 targetPosition, Vector3 direction1, Vector3 direction2) => (Vector3.Distance(start.position, targetPosition + direction1) * ((startWeigth + targetWeigth) / 2)) + (Vector3.Distance(targetPosition + direction1, targetPosition + direction2)) + (Vector3.Distance(end.position, targetPosition + direction2) * ((endWeigth + targetWeigth) / 2));
    private float GetVelocityPercentage(float distance, float maxdistance) => 1 - (distance / maxdistance);
    private void GridEnter(Collider other)
    {
        if (LayerUtilities.IsSameLayer(LayerData.PlayerLayer, other.gameObject.layer))
            other.GetComponentInParent<IDamageable<int, DamageTypes>>()?.TakeDamage(1, DamageTypes.Basic, ColliderUtilities.GetContactDirection(other, transform.position));
        FlockUnit unit = other.GetComponent<FlockUnit>();
        if (unit == null)
            return;
        unit.AssingFlock(flock);
        unit.transform.parent = flock.TargetToFollow;
    }
    private void SetLineRenderes(bool state)
    {
        for (var i = 0; i < lineRenderers.Length; i++)
            lineRenderers[i].gameObject.SetActive(state);
    }
}
