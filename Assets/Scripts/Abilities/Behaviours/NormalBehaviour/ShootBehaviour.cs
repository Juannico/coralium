using FMODUnity;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.VFX;
public class ShootBehaviour : BaseAbilityBehaviour
{
    private Material mat;
    [SerializeField] private MeshRenderer trail;
    private Material trailMat;
    [SerializeField] private GPUParticleSystem disolvEffect;
    [SerializeField] private GPUParticleSystem bubbles;
    [SerializeField] private float abilityScaleMultiplier = 1;
    [Header("SFX")]
    [SerializeField] private StudioEventEmitter shootSFX;
    [SerializeField] private StudioEventEmitter shootCollideSFX;

    [HideInInspector] public LayerMask IgonerLayer;
    private bool canVanish = true;
    private float vanishTime;
    protected float maxsize;
    private Coroutine shirnkCoroutine;
    private Collider selfCollider;
    protected override void Awake()
    {
        base.Awake();
        SetSFXValues();
        mat = gameObject.GetComponent<MeshRenderer>().material;
        selfCollider = gameObject.GetComponent<Collider>();
        trailMat = trail.material;
    }
    public override void Throw(BaseAbilityCast ability)
    {
        rb.velocity = Vector3.zero;
        base.Throw(ability);
        rb.AddForce(transform.forward * ability.Data.Speed, ForceMode.VelocityChange);
        shootSFX.Play();
        mat.SetFloat("_Alpha", 1);
        trailMat.SetFloat("_Alpha", 1);
        float duration = ability.Data.Duration;
        maxsize = ability.Data.Speed * duration * 0.1f * abilityScaleMultiplier;
        gameObject.transform.localScale = new Vector3(1, 1, 0.01f) * maxsize;
        canVanish = true;
        float durationMultiplier = 0.9f;
        vanishTime = duration * (1 - durationMultiplier);
        shirnkCoroutine = StartCoroutine(Shrink(duration * durationMultiplier * 0.5f));
        selfCollider.enabled = true;
        SetGameObjectState(bubbles.gameObject, true);
        bubbles.Play();
        CoroutineHandler.ExecuteActionAfter(() => StartVanish(), duration * durationMultiplier, this);
    }
    private void SetSFXValues()
    {
        shootSFX.OverrideAttenuation = true;
        shootSFX.OverrideMaxDistance = 250;
        shootSFX.OverrideMinDistance = 1;
        shootCollideSFX.OverrideAttenuation = true;
        shootCollideSFX.OverrideMaxDistance = 250;
        shootCollideSFX.OverrideMinDistance = 1;
    }
    protected override bool OnSuccessfulTriggerEnter(Collider other)
    {
        if (base.OnSuccessfulTriggerEnter(other))
            return true;
        if (!canInteract)
            return false;
        ShouldDropObject(other);

        if (other.TryGetComponent(out TrashObject trashObject))
        {
            trashObject.Vanish();
            return true;
        }
        if (other.TryGetComponent(out CleanCoralController ControlLimpiarCoral))
        {
            ControlLimpiarCoral.StartClean();
            return true;
        }
        if (other.TryGetComponent(out Pusheable pusheable))
        {
            pusheable.Push(transform.forward);
            return true;
        }
        if (LayerUtilities.IsSameLayer(LayerData.EnviromenLayer, other.gameObject.layer))
            return true;
        return false;
    }
    private void ShouldDropObject(Collider other)
    {
        if (other.TryGetComponent(out BaseInteractWithAbility baseStarAnimWIthAbility))
            return;
        if (!other.TryGetComponent(out DropObjects dropObjects))
            return;
        dropObjects.Drops();
        StopThrowing();
    }
    IEnumerator Shrink(float growTime)
    {
        float counter = 0;

        yield return new WaitForSeconds(growTime);
        float timeLerp;
        while (counter < growTime)
        {
            counter += Time.deltaTime;
            timeLerp = Mathf.Lerp(1, 0, counter / growTime);
            gameObject.transform.localScale = new Vector3(1, 1, 0.01f) * maxsize * timeLerp;
            yield return null;
        }
        rb.velocity = Vector3.zero;
        StartVanish();
    }

    private void StartVanish()
    {
        selfCollider.enabled = false;
        SetGameObjectState(disolvEffect.gameObject, true);
        disolvEffect.Play();
        bubbles.Stop();
        SetGameObjectState(bubbles.gameObject, false);

        shootSFX.Stop();
        canVanish = false;
        float counter = 0;
        float alphaLerp;
        StartCoroutine(CoroutineHandler.ExecuteActionAfterUpdateUntil(() =>
        {
            counter += Time.deltaTime;
            alphaLerp = Mathf.Lerp(1, 0, counter / vanishTime);
            mat.SetFloat("_Alpha", alphaLerp);
            trailMat.SetFloat("_Alpha", alphaLerp);
            return counter > vanishTime;
        }, () => base.StopThrowing()));

    }
    protected override void StopThrowing()
    {
        if (!canVanish)
            return;
        if (shirnkCoroutine != null)
            StopCoroutine(shirnkCoroutine);
        shootCollideSFX.Play();
        rb.velocity /= 4;
        StartVanish();
    }

}
