using UnityEngine;

public class ComboAblityBehaviour : BaseAbilityBehaviour
{
    [HideInInspector] private BaseAbilityCast secondAbility;
    public void SetSecondAbility(BaseAbilityCast secondAbility) => this.secondAbility = secondAbility;
    public override void Throw(BaseAbilityCast ability)
    {
        enabled = true;
        base.Throw(ability);
    }

    protected override bool OnSuccessfulTriggerEnter(Collider other)
    {
        if (base.OnSuccessfulTriggerEnter(other))
        {
            secondAbility?.Cast(true);               
            return true;
        }
        return false;
    }

}
