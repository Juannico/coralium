using System;
using UnityEngine;

public class BaseAbilityBehaviour : MonoBehaviour
{
    protected Rigidbody rb;
    [SerializeField] private bool damageSameLayer = false;
    [field: SerializeField] public bool CanBeDisrupt { get; private set; } = false;
    [field: SerializeField] public bool AllowCastOtherAbilities { get; private set; } = true;
    [field: SerializeField] public LayerData LayerData { get; set; }
    [SerializeField] private bool stopThrowingOnApplyDamage = true;
    [SerializeField] private bool stopThrowingOnApplyEffect = true;
    [SerializeField] protected bool canInteract = false;
    public BaseAbilityCast Ability { get; private set; }
    public bool UseCollisionHelper = false;
    [HideInInspector] public CollisionHelper CollisionHelper;
    [Header("VFX")]
    [SerializeField] protected BaseVisualEffect vfx;
    [HideInInspector] public Action OnStopThrowing;
    public GameObject Caster
    {
        get
        {
            if (Ability == null)
                return null;
            return Ability.Character.gameObject;
        }
    }
    protected virtual void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }
    protected virtual void Start()
    {
        if (CollisionHelper != null)
            CollisionHelper.TriggerEnter += OnAbilityTriggerEnter;
    }
    public virtual void Throw(BaseAbilityCast ability)
    {
        if (!gameObject.activeInHierarchy)
            gameObject.SetActive(true);
        this.Ability = ability;
        enabled = true;
        ability.Character.AbilitiesHandle.AddAbilityBehaviour(this);
    }
    public virtual void Disrupt()
    {
        if (enabled)
            StopThrowing();
    }
    protected virtual void StopThrowing()
    {
        OnStopThrowing?.Invoke();
        Ability?.EndFeelsOnAbilityBehaviour();
        Ability?.Data.EndAbility?.Invoke();
        gameObject.SetActive(false);
        Ability.Character.AbilitiesHandle.RemoveAbilityBehaviour(this);
        enabled = false;
    }
    private void OnTriggerEnter(Collider other)
    {
        OnAbilityTriggerEnter(other);
    }
    private void OnAbilityTriggerEnter(Collider other)
    {
        if (Ability == null)
            return;
        if (!gameObject.activeInHierarchy)
            return;
        if (other.gameObject == gameObject)
            return;
        if (IgnoreLayer(other.gameObject.layer))
            return;
        if (IsCollidingWithCaster(other.gameObject))
            return;
        if (IsCollidingWithAbilityCaster(other.gameObject))
            return;
        if (IsCollidingWithSameLayer(other.gameObject) && !damageSameLayer)
            return;
        if (OnSuccessfulTriggerEnter(other))
            StopThrowing();
    }
    protected virtual bool IgnoreLayer(LayerMask ignoreLayer) => LayerUtilities.IsSameLayer(LayerData.IgnoreCollider, ignoreLayer) || LayerUtilities.IsSameLayer(LayerData.Trigger, ignoreLayer);

    protected virtual bool OnSuccessfulTriggerEnter(Collider other)
    {
        if ((DoDamage(other.gameObject) && stopThrowingOnApplyDamage) || (ApplyEffect(other.gameObject) && stopThrowingOnApplyEffect))
            return true;
        return false;
    }

    private bool IsCollidingWithCaster(GameObject otherGameObject) => otherGameObject.GetComponentInParent<Character>() == Ability.Character;
    protected bool IsCollidingWithAbilityCaster(GameObject otherGameObject)
    {
        BaseAbilityBehaviour baseAbilityBehaviour = otherGameObject.GetComponentInParent<BaseAbilityBehaviour>();
        if (!baseAbilityBehaviour)
            return false;
        return baseAbilityBehaviour.Caster == Ability.Character.gameObject;
    }
    protected bool IsCollidingWithSameLayer(GameObject otherGameObject) => otherGameObject.layer == Ability.Character.gameObject.layer;
    protected virtual bool DoDamage(GameObject objectToDoDamage)
    {
        if (Ability.Data.Damage <= 0)
            return false;
        IDamageable<int, DamageTypes> objectDamagable = objectToDoDamage.GetComponentInParent<IDamageable<int, DamageTypes>>();
        if (objectDamagable == null)
            return false;
        objectDamagable.TakeDamage(Ability.Data.Damage * Ability.Character.BaseCharacter.Data.DamageMultiplier, Ability.Data.DamageType, GetDamageDirection(objectToDoDamage));
        return true;
    }
    protected bool ApplyEffect(GameObject objectToApplyEffect)
    {
        if (!objectToApplyEffect.TryGetComponent(out BaseStatusEffect onElemented))
            return false;
        onElemented.ApplyEffect(Ability.Data.DamageType);
        return true;
    }
    protected virtual Vector3 GetDamageDirection(GameObject objectToDoDamage) => transform.forward;
    protected void SetGameObjectState(GameObject objectToSet, bool state)
    {
        if (objectToSet.activeInHierarchy == !state)
            objectToSet.SetActive(state);
    }
}
