using System;
using UnityEngine;
[Serializable]
public class UnlockAbility
{
    [SerializeField] private PlayerAbilities playerAbilities;
    [field: SerializeField] public BaseAbilityCast Ability { get; private set; }
    [SerializeField] private GameObject effect;
    [SerializeField] private bool islocked;
    private BaseAbilityCast playerAbility;
    private ControlSpectrumScale controlSpectrumScale;
    public void Initialize()
    {
        controlSpectrumScale = GameObject.Instantiate(effect).GetComponentInChildren<ControlSpectrumScale>();
        controlSpectrumScale.transform.parent.gameObject.SetActive(false);
    }
    public void Setlock(PlayerController playerController, bool lockState, Action<bool> unlockMovement)
    {
        playerAbility = GetPlayerAbility();
        if (playerAbility == null)
            return;
        playerAbility.IsLocked = lockState;
        if (lockState == true)
            return;
        if (controlSpectrumScale == null)
            Initialize();
        controlSpectrumScale.transform.parent.gameObject.SetActive(true);
        controlSpectrumScale.transform.parent.parent = playerController.BaseTransform.transform;
        controlSpectrumScale.transform.parent.localPosition = Vector3.zero;
        controlSpectrumScale.transform.parent.localRotation = Quaternion.identity;
        controlSpectrumScale?.StartAnimation(unlockMovement);
    }

    private BaseAbilityCast GetPlayerAbility()
    {
        for (var i = 0; i < playerAbilities.BaseAbilities.Length; i++)
        {
            if (playerAbilities.BaseAbilities[i] == Ability)
                return playerAbilities.Abilities[i];
        }
        return null;
    }

}
