using System;
using UnityEngine;

public abstract class BaseCharacterAbility : BaseAbility
{
    // The character that owns this component
    protected Character owningCharacter;

    protected override bool CommitCheck()
    {
        if (owningCharacter == null)
        {
            Debug.LogError("BaseCharacterAbility::CommitCheck: OwningCharacter is null. Parent gameObject doesn't contain a Character component.");
            return false;
        }

        return base.CommitCheck();
    }

    public override bool PrepareAbility(BaseAbilityDescriptor abilityDescriptor)
    {
        owningCharacter = gameObject.GetComponent<Character>();

        return base.PrepareAbility(abilityDescriptor);
    }

    public override void DefineFxSpawnLocationAndRotation(BaseAbilityFX abilityFx, ref Vector3 location, ref Quaternion rotation)
    {
        // Character abilities use the character transform by default
        if (owningCharacter)
        {
            location = owningCharacter.transform.position;
            rotation = owningCharacter.transform.rotation;
            return;
        }

        Debug.LogWarning($"BaseCharacterAbility::DefineFxSpawnLocationAndRotation: Failed to define OwningCharacter location/rotation for FX {abilityFx.GetType()}, defaulting to ability owner.");

        base.DefineFxSpawnLocationAndRotation(abilityFx, ref location, ref rotation);
    }
}