using System.Collections.Generic;
using UnityEngine;

public class AbilitySystemComponent : MonoBehaviour
{
    // All the abilities that have been registered for the owner of this component.
    private List<BaseAbility> registeredAbilities;

    // All the abilities that are active at a certain moment.
    private List<BaseAbility> activeAbilities;

    // List of all AbilityEventHandlers. This is needed so that the handlers don't get Garbage Collected at the wrong times.
    private List<AbilityEventHandler> abilityEventHandlers = null;

    // The character that owns this AbilitySystem. This may be null if the owner isn't a Character.
    private Character owningCharacter;

    private void Awake()
    {
        registeredAbilities = new List<BaseAbility>();
        activeAbilities = new List<BaseAbility>();
        abilityEventHandlers = new List<AbilityEventHandler>();

        owningCharacter = GetComponent<Character>();

        // #TODO: Get defaultAbilities from a nice character asset
        BaseAbility[] defaultAbilities = GetComponents<BaseAbility>();

        for (int i = 0; i < defaultAbilities.Length; i++)
        {
            BaseAbility ability = defaultAbilities[i];

            if (ability == null)
            {
                Debug.LogWarning("AbilitySystemComponent::Awake: Default ability is null.");
                continue;
            }

            TryToRegisterAbility(ability);
        }
    }

    /// <summary>
    /// Registers the specified ability to the ability system. Once registered, it can be activated.
    /// </summary>
    /// <param name="ability"> The ability that will get registered. </param>
    /// <returns></returns>
    public bool TryToRegisterAbility(BaseAbility ability)
    {
        if (ability == null)
        {
            Debug.LogWarning("AbilitySystemComponent::TryToRegisterAbility: Passed ability is null.");
            return false;
        }

        if (IsAbilityRegistered(ability))
        {
            Debug.LogWarning("AbilitySystemComponent::TryToRegisterAbility: Passed ability is already registered.");
            return false;
        }

        registeredAbilities.Add(ability);

        ability.onAbilityStarted += OnAbilityStarted;
        ability.onAbilityEnded += OnAbilityEnded;

        return true;
    }

    /// <summary>
    /// Unregisters the specified ability to the ability system. Once unregistered, it can no longer be activated until it gets registered again.
    /// </summary>
    /// <param name="ability"> The ability that will get unregistered. </param>
    /// <returns></returns>
    public bool TryToUnregisterAbility(BaseAbility ability)
    {
        if (ability == null)
        {
            Debug.LogWarning("AbilitySystemComponent::TryToDeregisterAbility: Passed ability is null.");
            return false;
        }

        if (!IsAbilityRegistered(ability))
        {
            Debug.LogWarning("AbilitySystemComponent::TryToDeregisterAbility: Passed ability is not registered.");
            return false;
        }

        bool bAbilityDeactivated = true;

        // If the ability is currently active, try to end it before to be able to unregister it.
        if (IsAbilityActive(ability))
        {
            bAbilityDeactivated = TryEndAbility(ability);
        }

        if (bAbilityDeactivated)
        {
            ability.onAbilityStarted -= OnAbilityStarted;
            ability.onAbilityEnded -= OnAbilityEnded;

            return registeredAbilities.Remove(ability);
        }

        Debug.LogWarning("AbilitySystemComponent::TryToDeregisterAbility: Failed to Deregister ability.");
        return false;
    }

    /// <summary>
    /// Tries to activated the specified ability. It may fail if certain conditions are not met.
    /// </summary>
    /// <param name="ability"> The ability that will get activated. </param>
    /// <returns></returns>
    public bool TryActivateAbility(BaseAbility ability, BaseAbilityDescriptor abilityDescriptor)
    {
        if (ability == null)
        {
            return false;
        }

        if (!IsAbilityRegistered(ability))
        {
            Debug.LogWarning("AbilitySystemComponent::TryActivateAbility: Failed to activate ability. It's currently not registered.");
            return false;
        }

        if (IsAbilityActive(ability))
        {
            Debug.LogWarning("AbilitySystemComponent::TryActivateAbility: Failed to activate ability. It's already active.");
            return false;
        }

        if (!ability.PrepareAbility(abilityDescriptor))
        {
            Debug.LogWarning("AbilitySystemComponent::TryActivateAbility: Failed to activate ability. Preparation stage failed.");
            return false;
        }

        AbilityParams abilityParams = new AbilityParams();

        InternalActivateAbility(ability, abilityDescriptor, abilityParams);
        return true;
    }

    /// <summary>
    /// Tries to activated the specified ability. It may fail if certain conditions are not met.
    /// </summary>
    /// <param name="ability"> The ability that will get activated. </param>
    /// <param name="abilityParams"> Params that should be passed to the ability. </param>
    /// <returns></returns>
    public bool TryActivateAbility(BaseAbility ability, BaseAbilityDescriptor abilityDescriptor, AbilityParams abilityParams)
    {
        if (ability == null)
        {
            return false;
        }

        if (!IsAbilityRegistered(ability))
        {
            Debug.LogWarning("AbilitySystemComponent::TryActivateAbility: Failed to activate ability. It's currently not registered.");
            return false;
        }

        if (IsAbilityActive(ability))
        {
            Debug.LogWarning("AbilitySystemComponent::TryActivateAbility: Failed to activate ability. It's already active.");
            return false;
        }

        if (!ability.PrepareAbility(abilityDescriptor, abilityParams))
        {
            Debug.LogWarning("AbilitySystemComponent::TryActivateAbility: Failed to activate ability. Preparation stage failed.");
            return false;
        }

        InternalActivateAbility(ability, abilityDescriptor, abilityParams);
        return true;
    }

    private void InternalActivateAbility(BaseAbility ability, BaseAbilityDescriptor abilityDescriptor, AbilityParams abilityParams)
    {
        BaseAbilityFX[] abilityFXs = new BaseAbilityFX[abilityDescriptor.AbilityFXs.Length];

        for (int i = 0; i < abilityDescriptor.AbilityFXs.Length; i++)
        {
            GameObject FxPrefab = abilityDescriptor.AbilityFXs[i];

            if (FxPrefab == null)
            {
                Debug.LogWarning($"AbilitySystemComponent::InternalActivateAbility: FX prefab in index {i} is null.");
                continue;
            }

            Transform parentTransform = gameObject.transform;

            // At this point we assume the FX prefab has a component BaseAbilityFX, we don't want to GetComponent twice (for prefab and then for instance).
            // We'll handle destruction and warning if that's ever the case
            GameObject FxInstance = Instantiate(FxPrefab, parentTransform.position, parentTransform.rotation, parentTransform);

            if (FxInstance == null)
            {
                Debug.LogError("AbilitySystemComponent::InternalActivateAbility: Failed to instantiate FxInstance.");
                continue;
            }

            BaseAbilityFX abilityFX = FxInstance.GetComponent<BaseAbilityFX>();

            if (abilityFX == null)
            {
                Destroy(FxInstance);
                Debug.LogError($"AbilitySystemComponent::InternalActivateAbility: Specified prefab at index {i} doesn't have a BaseAbilityFX. Destroying prefab instance.");
                continue;
            }

            BaseCharacterAbilityFX characterAbilityFX = (BaseCharacterAbilityFX)abilityFX;

            if (characterAbilityFX != null)
            {
                if (owningCharacter == null)
                {
                    Debug.LogError($"AbilitySystemComponent::InternalActivateAbility: Failed to set owningCharacter to BaseCharacterAbilityFX due to invalid owningCharacter.");
                }
                else
                {
                    characterAbilityFX.SetOwningCharcater(owningCharacter);
                }
            }

            abilityFXs[i] = abilityFX;
        }

        AbilityEventHandler abilityEventHandler = new AbilityEventHandler(ability, abilityFXs);

        abilityEventHandlers.Add(abilityEventHandler);

        abilityEventHandler.onHandlerReleased += (AbilityEventHandler handler) => { 
            // We no longer need this handler, so we remove it from List so that it can be Garbage Collected
            abilityEventHandlers.Remove(handler); 
        };

        // At this point, all params have been confirmed to be initialized, so, we don't do checks here again
        ability.ActivateAbility();
    }

    /// <summary>
    /// Tries to end the specified ability. It may fail if certain conditions are not met.
    /// </summary>
    /// <param name="ability"> The ability that will get finished. </param>
    /// <returns></returns>
    public bool TryEndAbility(BaseAbility ability)
    {
        if (ability == null)
        {
            return false;
        }

        if (!IsAbilityActive(ability))
        {
            Debug.LogWarning("AbilitySystemComponent::TryEndAbility: Failed to activate ability. It's not active.");
            return false;
        }

        ability.EndAbility();
        return true;
    }

    /// <summary>
    /// Determines if the passed ability is registered.
    /// </summary>
    /// <param name="ability"> The ability to verify. </param>
    /// <returns></returns>
    public bool IsAbilityRegistered(BaseAbility ability)
    {
        if (ability == null)
        {
            return false;
        }

        return registeredAbilities.Contains(ability);
    }

    /// <summary>
    /// Determines if the passed ability is active.
    /// </summary>
    /// <param name="ability"> The ability to verify. </param>
    /// <returns></returns>
    public bool IsAbilityActive(BaseAbility ability)
    {
        if (ability == null)
        {
            return false;
        }

        return activeAbilities.Contains(ability);
    }

    private void OnAbilityStarted(BaseAbility ability)
    {
        activeAbilities.Add(ability);
    }

    private void OnAbilityEnded(BaseAbility ability, bool wasCancelled)
    {
        activeAbilities.Remove(ability);
    }
}