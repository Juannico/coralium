using UnityEngine;

public struct AbilityParams
{
    public bool boolValue;
    public int intValue;
    public float floatValue;
    public string stringValue;
    public Vector3 vector1Value;
    public Vector3 vector2Value;
    public Object object1Value;
    public Object object2Value;
}