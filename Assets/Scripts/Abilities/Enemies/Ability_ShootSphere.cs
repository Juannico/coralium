using UnityEngine;

public class Ability_ShootSphere : BaseAbility
{
    private RangedAttackAbilityDescriptor rangedAttackAbilityDescriptor;
    public override void ActivateAbility()
    {
        base.ActivateAbility();
        rangedAttackAbilityDescriptor = GetAbilityDescriptor<RangedAttackAbilityDescriptor>();
        BaseProjectile baseProjectile = BaseProjectile.SpawnProjectile(rangedAttackAbilityDescriptor.ProjectilePrefab, gameObject, transform.position + abilityParams.vector2Value, Quaternion.LookRotation(abilityParams.vector2Value));
        baseProjectile.Activate();
    }
}
