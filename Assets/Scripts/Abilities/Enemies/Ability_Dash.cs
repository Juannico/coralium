using System.Collections;
using UnityEngine;

public class Ability_Dash : BaseCharacterAbility
{
    // #TODO: Determine GO dynamically
    [SerializeField] private GameObject modelContainer;

    private Vector3 dashVector = Vector3.zero;
    private Vector3 normalizedDashVector = Vector3.zero;

    private float dashCurrentTime = 0.0f;
    private float dashDuration = 0.0f;

    // 1 = right, -1 = left
    private int rotationDirection = 1;

    private bool isChargingDash = false;
    private bool isPerformingDash = false;
    private bool isPerformingKnockback = false;

    private RigibodyForceHandle rigibodyForceHandle = null;
    private StateComponentRotate stateComponentRotate = null;
    private KnockBackStateComponent knockBackState = null;
    private DashAbilityDescriptor dashAbilityDescriptor = null;

    // #TODO: Delete this once we add Ability FX support
    private DashEffectController dashEffectController;

    protected override bool CommitCheck()
    {
        dashVector = abilityParams.vector1Value;
        normalizedDashVector = dashVector.normalized;

        dashAbilityDescriptor = GetAbilityDescriptor<DashAbilityDescriptor>();

        if (dashAbilityDescriptor == null)
        {
            Debug.LogError("Ability_Dash::CommitCheck: dashAbilityDescriptor is null.");
            return false;
        }

        if (dashAbilityDescriptor.DashSpeed <= 0)
        {
            Debug.LogError("Ability_Dash::CommitCheck: dashAbilityDescriptor has a DashSpeed <= 0.");
            return false;
        }

        if (dashVector.magnitude <= 0)
        {
            Debug.LogError("Ability_Dash::CommitCheck: dashVector magnitud is <= 0.");
            return false;
        }

        rigibodyForceHandle = owningCharacter.GetComponent<RigibodyForceHandle>();

        if (rigibodyForceHandle == null)
        {
            Debug.LogError("Ability_Dash::CommitCheck: owningCharacter doesn't have a RigidBodyForceHandle component.");
            return false;
        }

        stateComponentRotate = owningCharacter.GetComponent<StateComponentRotate>();

        if (stateComponentRotate == null)
        {
            Debug.LogError("Ability_Dash::CommitCheck: owningCharacter doesn't have a stateComponentRotate component.");
            return false;
        }

        return base.CommitCheck();
    }

    public override bool PrepareAbility(BaseAbilityDescriptor abilityDescriptor)
    {
        if (!base.PrepareAbility(abilityDescriptor))
        {
            return false;
        }

        knockBackState = ComponentUtilities.SetComponent<KnockBackStateComponent>(owningCharacter.gameObject);

        dashEffectController = owningCharacter.GetComponentInChildren<DashEffectController>(true);
        return true;
    }

    public override void ActivateAbility()
    {
        base.ActivateAbility();

        rotationDirection = Random.Range(0f, 1f) > 0.5f ? 1 : -1;

        if (dashEffectController != null)
        {
            dashEffectController.StopEffect();
        }

        owningCharacter.GetComponent<EnemyController>().EnemyStateSharedData.MovementDirection = normalizedDashVector;

        if (dashAbilityDescriptor.DashChargeDuration > 0)
        {
            BeginCharge();
        }
        else
        {
            BeginDash();
        }
    }

    protected virtual void BeginCharge()
    {
        if (isChargingDash)
        {
            return;
        }

        isChargingDash = true;

        StartCoroutine(ChargeDashDelay(dashAbilityDescriptor.DashChargeDuration));

        if (dashEffectController != null)
        {
            dashEffectController.Charge();
        }
    }

    private IEnumerator ChargeDashDelay(float duration)
    {
        yield return new WaitForSeconds(duration);

        EndCharge();
    }

    protected virtual void EndCharge()
    {
        if (!isChargingDash)
        {
            return;
        }

        isChargingDash = false;

        if (dashEffectController != null)
        {
            dashEffectController.StopCharge();
        }

        BeginDash();
    }

    protected virtual void BeginDash()
    {
        if (isPerformingDash)
        {
            return;
        }

        isPerformingDash = true;

        dashCurrentTime = 0.0f;
        dashDuration = (dashVector.magnitude * dashAbilityDescriptor.DashLengthMultiplier) / dashAbilityDescriptor.DashSpeed;

        stateComponentRotate.StateSharedData.SelfRotationMultiplier = dashAbilityDescriptor.DashSpeed * 0.2f;

        // End the ability after the expected duration of the dash
        StartCoroutine(EndDashDelay(dashDuration));

        if (dashEffectController != null)
        {
            dashEffectController.Lunge(0.1f);
        }
    }

    private IEnumerator EndDashDelay(float duration)
    {
        yield return new WaitForSeconds(duration);

        EndDash();
    }

    protected virtual void EndDash()
    {
        if (!isPerformingDash)
        {
            return;
        }

        isPerformingDash = false;

        if (dashEffectController != null)
        {
            dashEffectController.StopLunge();
        }

        EndAbility();
    }

    protected override void AbilityUpdate()
    {
        base.AbilityUpdate();

        if (isPerformingDash && !isPerformingKnockback)
        {
            ApplyExtraRotation();

            RaycastHit hit;

            if (ShouldPerformKnockback(out hit))
            {
                Vector3 knockbackDirection = Vector3.Reflect(owningCharacter.Model.transform.forward, hit.normal);

                // Not going to make use of owningCharacter.BaseCharacter.Data.OnCharacterKnockBack because the knockback was built in the BT
                StartKnockBack(knockbackDirection, 0.5f, true);
            }
        }
    }

    // #TODO: Copypasta from ImpulseStateComponent::SetExtraRotation. Maybe clean this up later?
    private void ApplyExtraRotation()
    {
        dashCurrentTime += Time.deltaTime;

        float time = dashCurrentTime / dashDuration;
        float angle = Mathf.Lerp(0, 360, time);
        angle %= 360;
        if (angle > 180)
            angle -= 360;

        stateComponentRotate.StateSharedData.ExtraRotation = Vector3.forward * angle * rotationDirection;
    }

    private bool ShouldPerformKnockback(out RaycastHit hit)
    {
        return Physics.SphereCast(owningCharacter.Model.transform.position, 3.0f, owningCharacter.Model.transform.forward, out hit, 10.0f, owningCharacter.BaseCharacter.LayerData.EnviromenLayer);
    }

    private void StartKnockBack(Vector3 knockBackDirection, float duration, bool isCrashing)
    {
        knockBackState.StateSharedData.isCrashing = isCrashing;
        knockBackDirection.y *= 2;
        knockBackState.StateSharedData.KnockBackDirection = knockBackDirection.normalized;
        knockBackState.StateSharedData.KnockBackDuration = duration;

        knockBackState.OnStop += OnKnockbackEnd;

        owningCharacter.BaseCharacter.Data.IsImmune = true;

        isPerformingKnockback = true;
        knockBackState.Enter();
    }

    private void OnKnockbackEnd(StateComponent state)
    {
        owningCharacter.BaseCharacter.Data.IsImmune = false;
        knockBackState.OnStop -= OnKnockbackEnd;
        state.Exit();
        isPerformingKnockback = false;
    }

    protected override void AbilityFixedUpdate()
    {
        if (isPerformingDash && !isPerformingKnockback)
        {
            ApplyImpulse();
        }
    }

    protected void ApplyImpulse()
    {
        rigibodyForceHandle.ExternalVelocityForceVector += normalizedDashVector * dashAbilityDescriptor.DashSpeed;
    }

    public override void EndAbility()
    {
        // Make sure effects get disabled
        if (dashEffectController != null)
        {
            dashEffectController.StopCharge();
            dashEffectController.StopEffect();
        }

        // Make sure the coroutine gets stopped if there was an early ability end
        if (isChargingDash)
        {
            StopCoroutine("ChargeDash");
        }
        if (isPerformingDash)
        {
            StopCoroutine("PerformDash");
        }

        if (isPerformingKnockback)
        {
            OnKnockbackEnd(knockBackState);
        }

        rigibodyForceHandle.StopMove();

        // Reset ability state
        dashVector = Vector3.zero;
        normalizedDashVector = Vector3.zero;
        rotationDirection = 1;
        dashCurrentTime = 0.0f;
        dashDuration = 0.0f;
        isPerformingDash = false;
        isChargingDash = false;
        rigibodyForceHandle = null;
        stateComponentRotate = null;
        dashAbilityDescriptor = null;

        base.EndAbility();
    }
}