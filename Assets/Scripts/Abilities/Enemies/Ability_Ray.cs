using UnityEngine;

public class Ability_Ray : BaseAbility
{
    [SerializeField] private float animationDuration = 0.5f;
    [SerializeField] private float duration = 4;
    [SerializeField] private int damage = 1;
    [SerializeField] protected LayerData layerData;

    [SerializeField] private DamageTypes damageType = DamageTypes.Basic;
    private float timer;
    private Vector3 startScale = new Vector3 (0,1,1);
    private Vector3 targetScale = Vector3.one;
    protected override void OnAwake()
    {
        transform.localScale = startScale;
        gameObject.SetActive(false);
    }
    protected override void AbilityFixedUpdate()
    {
        base.AbilityFixedUpdate();
        timer += Time.deltaTime;
        if (timer / animationDuration < 1)
            transform.localScale = Vector3.Lerp(startScale, targetScale, timer / animationDuration);
        if (timer < duration)
            return;
        EndAbility();
    }
    public override void ActivateAbility()
    {
        base.ActivateAbility();
        gameObject.SetActive(true);
        timer = 0;
    }
    public override void EndAbility()
    {
        base.EndAbility();
        transform.localScale = startScale;
        gameObject.SetActive(false);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (!gameObject.activeInHierarchy || other.gameObject == gameObject || ShouldIgnoreLayer(other.gameObject.layer))
            return;
        OnSuccessfulTriggerEnter(other);
    }
    protected virtual bool ShouldIgnoreLayer(LayerMask ignoreLayer)
    {
        return LayerUtilities.IsSameLayer(layerData.IgnoreCollider, ignoreLayer) || LayerUtilities.IsSameLayer(layerData.Trigger, ignoreLayer);
    }
    private void OnSuccessfulTriggerEnter(Collider other)
    {
        IDamageable<int, DamageTypes> objectDamagable = other.GetComponentInParent<IDamageable<int, DamageTypes>>();
        if (objectDamagable == null)
            return;
        Vector3 knockBackDirection = ColliderUtilities.GetContactDirection(other, transform.position);
        objectDamagable.TakeDamage(damage, damageType, knockBackDirection);
    }

}
