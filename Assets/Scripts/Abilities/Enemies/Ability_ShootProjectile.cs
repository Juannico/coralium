using UnityEngine;

public class Ability_ShootProjectile : BaseCharacterAbility
{
    private RangedAttackAbilityDescriptor rangedAttackAbilityDescriptor;
    private StateComponentRotate stateComponentRotate = null;

    private Vector3 aimDirection = Vector3.zero;

    protected override bool CommitCheck()
    {
        rangedAttackAbilityDescriptor = GetAbilityDescriptor<RangedAttackAbilityDescriptor>();

        if (rangedAttackAbilityDescriptor == null)
        {
            Debug.LogError("Ability_ShootProjectile::CommitCheck: rangedAttackAbilityDescriptor is null.");
            return false;
        }

        if (rangedAttackAbilityDescriptor.ProjectilePrefab == null)
        {
            Debug.LogError("Ability_ShootProjectile::CommitCheck: rangedAttackAbilityDescriptor has the projectilePrefab set to null.");
            return false;
        }

        stateComponentRotate = owningCharacter.GetComponent<StateComponentRotate>();

        if (stateComponentRotate == null)
        {
            Debug.LogError("Ability_Dash::CommitCheck: owningCharacter doesn't have a stateComponentRotate component.");
            return false;
        }

        if (abilityParams.vector1Value.magnitude <= 0)
        {
            Debug.LogError("Ability_ShootProjectile::CommitCheck: Vector param magnitude is <= 0.");
            return false;
        }

        aimDirection = (abilityParams.vector1Value - owningCharacter.transform.position).normalized;

        return base.CommitCheck();
    }

    public override void ActivateAbility()
    {
        base.ActivateAbility();

        // Temp
        owningCharacter.GetComponent<EnemyController>().EnemyStateSharedData.MovementDirection = aimDirection;
        // #TODO: Don't snap rotation
        stateComponentRotate.InstantRotate(aimDirection);

        ShootProjectile();

        EndAbility();
    }

    private void ShootProjectile()
    {
        BaseProjectile baseProjectile = BaseProjectile.SpawnProjectile(rangedAttackAbilityDescriptor.ProjectilePrefab, owningCharacter.gameObject, transform.position + owningCharacter.BaseTransform.forward * 10.0f, Quaternion.LookRotation(owningCharacter.BaseTransform.forward));
        baseProjectile.Activate();
    }

    protected override void OnAbilityEnded(bool bWasCancelled)
    {
        // Reset ability state
        rangedAttackAbilityDescriptor = null;
        stateComponentRotate = null;
        aimDirection = Vector3.zero;

        base.OnAbilityEnded(bWasCancelled);
    }
}