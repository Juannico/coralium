using UnityEngine;

public class Ability_SummonEnemies : BaseCharacterAbility
{
    private SummonEnemiesAbilityDescriptor summonEnemiesAbilityDescriptor;

    private struct EnemySpawnRequest
    {
        public int id;
        public Vector3 position;
        public Quaternion rotation;
        public GameObject prefab;

        public bool IsValidRequest()
        {
            return prefab != null;
        }
    }

    private EnemySpawnRequest[] enemySpawnRequests;
    private int numSpawnedEnemies = 0;

    protected override bool CommitCheck()
    {
        summonEnemiesAbilityDescriptor = GetAbilityDescriptor<SummonEnemiesAbilityDescriptor>();

        if (summonEnemiesAbilityDescriptor == null)
        {
            Debug.LogError("Ability_SummonEnemies::CommitCheck: summonEnemiesAbilityDescriptor is null.");
            return false;
        }

        if (summonEnemiesAbilityDescriptor.EnemiesToSummon.Length <= 0)
        {
            Debug.LogError("Ability_SummonEnemies::CommitCheck: summonEnemiesAbilityDescriptor has an array EnemiesToSummon with 0 items.");
            return false;
        }

        return base.CommitCheck();
    }

    public override void ActivateAbility()
    {
        base.ActivateAbility();

        InitializeEnemySpawnRequests();
    }

    protected override void AbilityUpdate()
    {
        base.AbilityUpdate();

        if (numSpawnedEnemies >= enemySpawnRequests.Length)
        {
            EndAbility();
        }
    }

    private bool InitializeEnemySpawnRequests()
    {
        bool success = false;

        EnemySpawnConfig[] enemiesToSummon = summonEnemiesAbilityDescriptor.EnemiesToSummon;

        int numEnemies = enemiesToSummon.Length;

        enemySpawnRequests = new EnemySpawnRequest[numEnemies];

        for (int i = 0; i < numEnemies; i++)
        {
            GameObject enemyPrefab = enemiesToSummon[i].gameObject;
            Vector3 locationOffset = enemiesToSummon[i].locationOffset;
            Vector3 rotationOffset = enemiesToSummon[i].rotationOffset;

            if (enemyPrefab == null)
            {
                Debug.LogWarning($"Ability_SummonEnemies::InitializeEnemySpawnRequests: EnemyPrebaf in index {i} is null.");
                continue;
            }

            EnemySpawnRequest currentRequest = new EnemySpawnRequest();

            currentRequest.id = i;

            currentRequest.prefab = enemyPrefab;

            currentRequest.position = owningCharacter.Model.transform.position + (owningCharacter.Model.transform.forward * locationOffset.z) + (owningCharacter.Model.transform.right * locationOffset.x) + (owningCharacter.Model.transform.up * locationOffset.y);
            currentRequest.position.z = 0;
            currentRequest.rotation = owningCharacter.Model.transform.rotation * Quaternion.Euler(rotationOffset);

            enemySpawnRequests[i] = currentRequest;

            Character character = currentRequest.prefab.GetComponentInChildren<Character>();

            // Only try to sync FXs if we have the right data
            if (character && character.BaseCharacter.ModelPrefab)
            {
                AbilityParams eventParams = new AbilityParams();

                eventParams.intValue = currentRequest.id;
                eventParams.object1Value = character.BaseCharacter.ModelPrefab;
                eventParams.vector1Value = currentRequest.position;
                eventParams.vector2Value = currentRequest.rotation.eulerAngles;

                SendAbilityEvent("fx.EnemySpawnRequest", eventParams);
            }
            else
            {
                Debug.LogWarning("Ability_SummonEnemies::InitializeEnemySpawnRequests: Failed to fetch FX data, defaulting to summon without FXs.");
                SummonEnemy(currentRequest);
            }
        }

        return success;
    }

    public override void ReceiveAbilityEvent(string eventName, AbilityParams eventParams)
    {
        base.ReceiveAbilityEvent(eventName, eventParams);

        if (eventName == "fx.SpawnEffectCompleted")
        {
            EnemySpawnRequest request = GetSpawnRequestFromId(eventParams.intValue);

            if (request.IsValidRequest())
            {
                SummonEnemy(request);
            }
            else
            {
                Debug.LogError("Ability_SummonEnemies::ReceiveAbilityEvent: Invalid spawn request.");
            }
        }
    }

    private EnemySpawnRequest GetSpawnRequestFromId(int id)
    {
        for (int i = 0; i < enemySpawnRequests.Length; i++)
        {
            EnemySpawnRequest request = enemySpawnRequests[i];

            if (id == request.id)
            {
                return request;
            }
        }

        return new EnemySpawnRequest();
    }

    private void SummonEnemy(EnemySpawnRequest spawnRequest)
    {
        if (spawnRequest.prefab == null)
        {
            Debug.LogWarning("Ability_SummonEnemies::ActivateAbility: enemyPrebaf is null.");
            return;
        }

        GameObject spawnedEnemyGO = Instantiate(spawnRequest.prefab, spawnRequest.position, spawnRequest.rotation);

        numSpawnedEnemies++;

        // #TODO: We shouldn't need to configure all the stuff below every time we spawn an enemy. This should be something the enemy does on its own.
        EnemyController currentObjectSpawnedController = spawnedEnemyGO.GetComponentInChildren<EnemyController>();
        EnemyController casterController = owningCharacter.GetComponent<EnemyController>();
        currentObjectSpawnedController.ActiveChase = casterController.ActiveChase;
        currentObjectSpawnedController.IsFocusingPlayer = true;
        currentObjectSpawnedController.SetStateDataDirectionValues(Vector3.zero, Vector3.zero);
    }

    protected override void OnAbilityEnded(bool bWasCancelled)
    {
        // Reset ability state
        summonEnemiesAbilityDescriptor = null;
        enemySpawnRequests = null;
        numSpawnedEnemies = 0;

        base.OnAbilityEnded(bWasCancelled);
    }
}