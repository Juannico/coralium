﻿using System;

public class AbilityEventHandler
{
    public Action<AbilityEventHandler> onHandlerReleased;

    private BaseAbility ability = null;

    private BaseAbilityFX[] abilityFXs = null;

    public BaseAbility Ability { get { return ability; } }

    public BaseAbilityFX[] AbilityFXs { get { return abilityFXs; } }

    public AbilityEventHandler(BaseAbility ability, BaseAbilityFX[] abilityFXs)
    {
        this.ability = ability;
        this.abilityFXs = abilityFXs;

        // Bind events
        ability.onAbilityStarted += OnAbilityStarted;
        ability.onAbilityEnded += OnAbilityEnded;
        ability.onAbilityEvent += OnAbilityEvent;

        for (int i = 0; i < abilityFXs.Length; i++)
        {
            BaseAbilityFX currentAbilityFx = abilityFXs[i];

            if (currentAbilityFx == null)
            {
                continue;
            }

            currentAbilityFx.onAbilityEffectEventTriggered += OnFxEvent;
        }
    }

    private void OnAbilityStarted(BaseAbility ability)
    {
        for (int i = 0; i < abilityFXs.Length; i++)
        {
            BaseAbilityFX currentAbilityFx = abilityFXs[i];

            if (currentAbilityFx == null)
            {
                continue;
            }

            currentAbilityFx.ReceiveAbilityStarted(ability.Descriptor, ability.Params);
        }
    }

    private void OnAbilityEnded(BaseAbility ability, bool wasCancelled)
    {
        // Unbind events
        ability.onAbilityStarted -= OnAbilityStarted;
        ability.onAbilityEnded -= OnAbilityEnded;
        ability.onAbilityEvent -= OnAbilityEvent;

        for (int i = 0; i < abilityFXs.Length; i++)
        {
            BaseAbilityFX currentAbilityFx = abilityFXs[i];

            if (currentAbilityFx == null)
            {
                continue;
            }

            currentAbilityFx.onAbilityEffectEventTriggered -= OnFxEvent;
            currentAbilityFx.ReceiveAbilityEnded();
        }

        // Eventually allows for this Object to get GCd
        onHandlerReleased?.Invoke(this);
    }

    private void OnAbilityEvent(BaseAbility ability, string eventName, AbilityParams eventParams)
    {
        for (int i = 0; i < abilityFXs.Length; i++)
        {
            BaseAbilityFX currentAbilityFx = abilityFXs[i];

            if (currentAbilityFx == null)
            {
                continue;
            }

            currentAbilityFx.ReceiveAbilityEvent(eventName, eventParams);
        }
    }

    private void OnFxEvent(BaseFX Fx, string eventName, AbilityParams eventParams)
    {
        ability.ReceiveAbilityEvent(eventName, eventParams);
    }
}
