using System.Collections.Generic;
using UnityEngine;

public class AbilitiesHandle 
{
    public List<BaseAbilityCast> AbilitiesCast;
    public List<BaseAbilityBehaviour> AbilityBehaviours;
    public AbilitiesHandle() 
    {
        AbilitiesCast = new List<BaseAbilityCast>();
        AbilityBehaviours = new List<BaseAbilityBehaviour>();
    }
    public bool CanDisrupAbilities = true;
    public bool CanCastAbitity() 
    {
        if (AbilityBehaviours.Count == 0)
            return true;
            for (int i = 0; i < AbilityBehaviours.Count; i++)
        {
            if (!AbilityBehaviours[i].AllowCastOtherAbilities)
                return false;
        }
        return true;
    }
    public void DisrupAbilities()
    {
        if (!CanDisrupAbilities)
            return;
        if (AbilitiesCast.Count > 0)
        {
            BaseAbilityCast[] storeAbilitiesCast = AbilitiesCast.ToArray();
            for (int i = 0; i < storeAbilitiesCast.Length; i++)
                RemoveAbilityCast(storeAbilitiesCast[i],true);
        }
        if (AbilityBehaviours.Count > 0)
        {
            BaseAbilityBehaviour[] storeAbilitiesBehaviour = AbilityBehaviours.ToArray();
            for (int i = 0; i < storeAbilitiesBehaviour.Length; i++)
                RemoveAbilityBehaviour(storeAbilitiesBehaviour[i], true);
        }
    }
    public void AddAbilityCast(BaseAbilityCast abilityCast) 
    {
        if (AbilitiesCast.Contains(abilityCast))
            return;
        AbilitiesCast.Add(abilityCast);
    }
    public void RemoveAbilityCast(BaseAbilityCast abilityCast, bool disrupt = false)
    {
        if (!AbilitiesCast.Contains(abilityCast))
            return;
        AbilitiesCast.Remove(abilityCast);
        if (disrupt)
        {
            abilityCast.CancelCast();
            //abilityCast.Cast(false);
        }
    }
    public void AddAbilityBehaviour(BaseAbilityBehaviour abilityBehaviour)
    {
        if (AbilityBehaviours.Contains(abilityBehaviour))
            return;
        AbilityBehaviours.Add(abilityBehaviour);
    }
    public void RemoveAbilityBehaviour(BaseAbilityBehaviour abilityBehaviour, bool disrupt = false)
    {
        if (!AbilityBehaviours.Contains(abilityBehaviour))
            return;
        AbilityBehaviours.Remove(abilityBehaviour);
        if (disrupt && abilityBehaviour.CanBeDisrupt)
            abilityBehaviour.Disrupt();
    }
}
