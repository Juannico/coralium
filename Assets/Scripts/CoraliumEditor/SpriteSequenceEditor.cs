#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(SpriteSequence))]
public class SpriteSequenceEditor : Editor
{
    private float timer;
    private bool animating;
    private SpriteSequence spriteSequence;

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        EditorGUILayout.Space();

        spriteSequence = (SpriteSequence)target;

        if (GUILayout.Button("Start Debug Animation"))
        {
            spriteSequence.enabled = true;
            spriteSequence.StartAnim();
            animating = true;
        }

        if (GUILayout.Button("Stop Debug Animation"))
        {
            spriteSequence.StopAnim();
            animating = false;
        }
        if (GUILayout.Button("Continue Debug Animation"))
        {
            spriteSequence.enabled = true;
            spriteSequence.ContinueAnim();
            animating = true;
        }
    }

    private void OnEnable()
    {
        EditorApplication.update += EditorUpdate;
    }

    private void OnDisable()
    {
        EditorApplication.update -= EditorUpdate;
    }

    private void EditorUpdate()
    {
        if (!animating)
            return;
        timer += Time.deltaTime;

        if (timer >= spriteSequence.GetTickRate())
        {
            timer = 0;
            spriteSequence.ChangeSprite();
            SceneView.RepaintAll();
        }
    }
}
#endif