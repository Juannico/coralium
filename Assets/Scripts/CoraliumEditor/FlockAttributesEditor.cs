using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif  
#if UNITY_EDITOR
[CustomEditor(typeof(Flock),true)]

public class FlockAttributesEditor : Editor
{
    private bool showSpeed;
    private bool showCohesion;
    private bool showAvoidance;
    private bool showAligement;
    private bool showCircular;
    private bool showLimit;
    private bool showObstacle;
    Flock flock;

    private void OnEnable()
    {
        flock = (Flock)target;
    }
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        flock.useHightligthColor = EditorGUILayout.Toggle(" Use highligth ", flock.useHightligthColor);
        if (flock.useHightligthColor)
        {
            EditorGUI.indentLevel++;
            flock.hightligthPower = EditorGUILayout.FloatField("Highligth power",flock.hightligthPower);
            flock.hightligthColor = EditorGUILayout.ColorField(new GUIContent("Highligth color"), flock.hightligthColor,false,true,true);
            EditorGUI.indentLevel--;
        }
        if (flock.followObject)
            ShowFollowAttr(flock);
        if (!flock.randomValues)
            ShowAttr(flock);

        EditorUtility.SetDirty(target);
    }
    public void ShowAttr(Flock flock)
    {
        EditorGUILayout.Space();
        EditorGUILayout.LabelField("FLOCK ATTRIBUTES SETUP", EditorStyles.boldLabel);
        EditorGUI.indentLevel++;
        showSpeed = EditorGUILayout.Foldout(showSpeed, "Speed");
        if (showSpeed)
        {
            EditorGUI.indentLevel++;
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Min ", GUILayout.MaxWidth(55));
            flock.minSpeed = EditorGUILayout.Slider(flock.minSpeed, 0.1f, 5);
            EditorGUILayout.LabelField("Max ", GUILayout.MaxWidth(55));
            flock.maxSpeed = EditorGUILayout.Slider(flock.maxSpeed, 5, 10);
            EditorGUILayout.EndHorizontal();
            EditorGUI.indentLevel--;
        }
        EditorGUILayout.Space();
        EditorGUILayout.LabelField("Behaviour", EditorStyles.boldLabel);
        EditorGUILayout.HelpBox("There are 4 behaviors defined by a distance to detect nearby fish and a weight exerted by this behavior on the fish ", MessageType.Info);
        EditorGUI.indentLevel++;
        showCohesion = EditorGUILayout.Foldout(showCohesion, new GUIContent("Cohesion", "Fish detects other fishes within the distance and goes to the center of them according to a weight"));
        if (showCohesion)
        {
            EditorGUI.indentLevel++;
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Distance to detect fishes to cohesion", GUILayout.MaxWidth(255));
            flock.cohesionDistance = EditorGUILayout.Slider(flock.cohesionDistance, 0, 10);
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Cohesion steering weight", GUILayout.MaxWidth(250));
            flock.cohesionWeight = EditorGUILayout.Slider(flock.cohesionWeight, 0, 10);
            EditorGUILayout.EndHorizontal();
            EditorGUI.indentLevel--;
        }
        EditorGUILayout.Space();
        showAvoidance = EditorGUILayout.Foldout(showAvoidance, new GUIContent("Avoidance", "Fish detects other fishes within the distance and moves away from them according to a weight"));
        if (showAvoidance)
        {
            EditorGUI.indentLevel++;
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Distance to detect fishes to avoid", GUILayout.MaxWidth(255));
            flock.avoidanceDistance = EditorGUILayout.Slider(flock.avoidanceDistance, 0, 10);
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Avoidance steering weight", GUILayout.MaxWidth(255));
            flock.avoidanceWeight = EditorGUILayout.Slider(flock.avoidanceWeight, 0, 10);
            EditorGUILayout.EndHorizontal();
            EditorGUI.indentLevel--;
        }
        EditorGUILayout.Space();
        showAligement = EditorGUILayout.Foldout(showAligement, new GUIContent("Aligement", "Fish detects other fishes within the distance and goes in the direction where they are going according to a weight"));
        if (showAligement)
        {
            EditorGUI.indentLevel++;
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Distance to detect fishes to aligement", GUILayout.MaxWidth(255));
            flock.aligementDistance = EditorGUILayout.Slider(flock.aligementDistance, 0, 10);
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Alignment steering weight ", GUILayout.MaxWidth(255));
            flock.aligementWeight = EditorGUILayout.Slider(flock.aligementWeight, 0, 10);
            EditorGUILayout.EndHorizontal();
            EditorGUI.indentLevel--;
        }
        EditorGUILayout.Space();
        showCircular = EditorGUILayout.Foldout(showCircular, new GUIContent("Circular", "Fish moves around the center of the flock according to a direction weight and maintaining a distance defined by a radius, an area and a height"));
        if (showCircular)
        {
            EditorGUI.indentLevel++;
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Radius of circle", GUILayout.MaxWidth(255));
            flock.circularRadius = EditorGUILayout.Slider(flock.circularRadius, 0, 100);
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Weight to be above the circle ", GUILayout.MaxWidth(255));
            flock.circularWeight = EditorGUILayout.Slider(flock.circularWeight, 0, 10);
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Area of the circle ", GUILayout.MaxWidth(255));
            flock.circularArea = EditorGUILayout.Slider(flock.circularArea, 0, 50);
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Heigh of the circle", GUILayout.MaxWidth(255));
            flock.circularHeight = EditorGUILayout.Slider(flock.circularHeight, 0, 25);
            EditorGUILayout.EndHorizontal();
            EditorGUI.indentLevel--;
        }
        EditorGUI.indentLevel--;
        EditorGUILayout.Space();
        showLimit = EditorGUILayout.Foldout(showLimit, new GUIContent("Limit", "Fish cannot move away from a certain distance from the center of the flock and returns according to the weight towards the center"));
        if (showLimit)
        {
            EditorGUI.indentLevel++;
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Radius of sphere limit", GUILayout.MaxWidth(255));
            flock.limitDistance = EditorGUILayout.Slider(flock.limitDistance, 0, 100);
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Weight for not exceed the limit ", GUILayout.MaxWidth(255));
            flock.limitWeight = EditorGUILayout.Slider(flock.limitWeight, 0, 10);
            EditorGUILayout.EndHorizontal();
            EditorGUI.indentLevel--;
        }
        EditorGUILayout.Space();
        showObstacle = EditorGUILayout.Foldout(showObstacle, new GUIContent("Obstacle", "Fish avoids objects layer within a distance and with a steering weight"));
        if (showObstacle)
        {
            EditorGUI.indentLevel++;
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Distance to detect obstacle", GUILayout.MaxWidth(255));
            flock.obstacleDistance = EditorGUILayout.Slider(flock.obstacleDistance, 0, 10);
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Weight to avoid obstacle", GUILayout.MaxWidth(255));
            flock.obstacleWeight = EditorGUILayout.Slider(flock.obstacleWeight, 0, 100);
            EditorGUILayout.EndHorizontal();
            EditorGUI.indentLevel--;
        }
    }
    public void ShowFollowAttr(Flock flock)
    {
        EditorGUILayout.Space();
        EditorGUILayout.LabelField("FOLLOW ATTRIBUTES SETUP", EditorStyles.boldLabel);
        EditorGUI.indentLevel++;
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("Objec to follow", GUILayout.MaxWidth(100));
        flock.TargetToFollow = EditorGUILayout.ObjectField(flock.TargetToFollow, typeof(Transform), true) as Transform;
        EditorGUILayout.EndHorizontal();
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("Target Destination", GUILayout.MaxWidth(250));
        flock.targetDestinations = EditorGUILayout.ObjectField(flock.targetDestinations, typeof(TargetDestinations), true) as TargetDestinations;
        EditorGUILayout.EndHorizontal();
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("Distance to which it approaches ", GUILayout.MaxWidth(250));
        flock.followDistance = EditorGUILayout.Slider(flock.followDistance, 0, 20);
        EditorGUILayout.EndHorizontal();
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("Follow steering weight", GUILayout.MaxWidth(250));
        flock.followWeight = EditorGUILayout.Slider(flock.followWeight, 10, 25);
        EditorGUILayout.EndHorizontal();
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("Max speed", GUILayout.MaxWidth(250));
        flock.followMaxSpeed = EditorGUILayout.Slider(flock.followMaxSpeed, 10, 50);
        EditorGUILayout.EndHorizontal();
        EditorGUILayout.BeginHorizontal();
        flock.isClosestToTarget = EditorGUILayout.Toggle("Shoul be always closest to target", flock.isClosestToTarget);
        EditorGUILayout.EndHorizontal(); 
        EditorGUILayout.BeginHorizontal();
        flock.isRandomOffset = EditorGUILayout.Toggle(" Follow position random offset ", flock.isRandomOffset);
        EditorGUILayout.EndHorizontal();
        EditorGUI.indentLevel--;

    }
}
#endif