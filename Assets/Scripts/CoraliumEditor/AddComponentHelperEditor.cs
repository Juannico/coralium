#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(AddComponentHelper))]

public class AddComponentHelperEditor : Editor
{
    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        AddComponentHelper addComponent = (AddComponentHelper)target;

        GUILayout.Space(10);
        addComponent.script = (TextAsset)EditorGUILayout.ObjectField(addComponent.script, typeof(TextAsset), false);
        if (addComponent.script == null)
        {
            EditorGUILayout.HelpBox("Please add a script file.", MessageType.Warning);
            return;
        }
        MonoScript script = AssetDatabase.LoadAssetAtPath<MonoScript>(AssetDatabase.GetAssetPath(addComponent.script));
        if (script == null)
        {
            EditorGUILayout.HelpBox("Selected asset is not a valid script.", MessageType.Warning);
            return;
        }
        System.Type scriptType = script.GetClass();
        if (scriptType == null)
        {
            EditorGUILayout.HelpBox("Selected asset is not a valid script.", MessageType.Warning);
            return;
        }
        addComponent.componentToAdd = scriptType;
        serializedObject.ApplyModifiedProperties();
    }
}
#endif