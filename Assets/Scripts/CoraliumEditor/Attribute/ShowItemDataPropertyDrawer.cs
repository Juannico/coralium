#if UNITY_EDITOR
using UnityEngine;
using NaughtyAttributes.Editor;
using System.Reflection;
using UnityEditor;


[CustomPropertyDrawer(typeof(ShowItemDataAttribute))]
public class ShowItemDataPropertyDrawer : PropertyDrawer
{
    private float helpBoxHeight = EditorGUIUtility.singleLineHeight * 2;

    private string helpboxText;
    private object target;
    sealed override public float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        float totalHeight = EditorGUI.GetPropertyHeight(property, includeChildren: true);
        Item item = GetItem(property);
        if (item == null)
        {
            totalHeight += EditorGUI.GetPropertyHeight(property, includeChildren: true) + helpBoxHeight / 2;
            return totalHeight;
        }
        if (target is ShopData && !item.CanBuy)
            totalHeight += EditorGUI.GetPropertyHeight(property, includeChildren: true) + helpBoxHeight / 2;
        if (!property.isExpanded)
            return totalHeight;
        totalHeight += EditorGUIUtility.singleLineHeight;
        using (SerializedObject serializedObject = new SerializedObject(item))
        {
            using (var iterator = serializedObject.GetIterator())
            {
                if (iterator.NextVisible(true))
                {
                    do
                    {
                        SerializedProperty childProperty = serializedObject.FindProperty(iterator.name);
                        if (childProperty.name.Equals("m_Script", System.StringComparison.Ordinal))
                            continue;
                        bool visible = PropertyUtility.IsVisible(childProperty);
                        if (!visible)
                            continue;
                        float height = EditorGUI.GetPropertyHeight(childProperty, includeChildren: true);
                        totalHeight += height;
                    }
                    while (iterator.NextVisible(false));
                }
            }
           // totalHeight += EditorGUIUtility.standardVerticalSpacing;
        }
        return totalHeight;
    }
    public sealed override void OnGUI(Rect rect, SerializedProperty property, GUIContent label)
    {
        Rect propertyRect = new Rect()
        {
            x = rect.x,
            y = rect.y,
            width = rect.width,
            height = EditorGUIUtility.singleLineHeight
        };
        Item item = GetItem(property);
        if (item == null)
        {
            Rect HelpBoxRect = new Rect()
            {
                x = rect.x,
                y = rect.y,
                width = rect.width,
                height = helpBoxHeight
            };
            NaughtyEditorGUI.HelpBox(HelpBoxRect, helpboxText, MessageType.Error);
            propertyRect.y += EditorGUI.GetPropertyHeight(property, includeChildren: true) + helpBoxHeight / 2;
            EditorGUI.PropertyField(propertyRect, property, label, false);
            return;
        }
        
        float yOffset = 0;
        if (target is ShopData && !item.CanBuy)
        {
            Rect HelpBoxRect = new Rect()
            {
                x = rect.x,
                y = rect.y,
                width = rect.width,
                height = helpBoxHeight
            };
            NaughtyEditorGUI.HelpBox(HelpBoxRect, "The item can't Buy, please check CanBuy or chose other item", MessageType.Warning);
            yOffset += EditorGUI.GetPropertyHeight(property, includeChildren: true) + helpBoxHeight / 2; ;
        }
        Rect foldoutRect = new Rect()
        {
            x = rect.x,
            y = rect.y + yOffset,
            width = EditorGUIUtility.labelWidth,
            height = EditorGUIUtility.singleLineHeight
        };
        propertyRect.y += yOffset;
        label.text = item.name;
        property.isExpanded = EditorGUI.Foldout(foldoutRect, property.isExpanded, label, toggleOnLabelClick: true);
        EditorGUI.PropertyField(propertyRect, property, label, false);
        if (!property.isExpanded)
            return;
        ShowItemChildPropierties(rect, item, yOffset);
    }
    private Item GetItem(SerializedProperty property)
    {
        ShowItemDataAttribute showDataAttribute = (ShowItemDataAttribute)attribute;
        target = PropertyUtility.GetTargetObjectWithProperty(property);
        if (showDataAttribute == null)
            return null;
        FieldInfo filedInfo = ReflectionUtility.GetField(target, showDataAttribute.GameData);
        GameData gameData = (GameData)filedInfo.GetValue(target);
        if (gameData == null)
        {
            helpboxText = $"The variable {showDataAttribute.GameData} is not assigned ";
            return null;
        }
        if (property.intValue >= gameData.Items.Length || property.intValue < 0)
        {
            helpboxText = $"Does not exist item with ID: {property.intValue}";
            return null;
        }
        return gameData.Items[property.intValue];
    }
    private void ShowItemChildPropierties(Rect rect, ScriptableObject scriptableObject, float yOffset)
    {
        yOffset += EditorGUIUtility.singleLineHeight * 1.1f;
        Rect boxRect = new Rect()
        {
            x = rect.x / 2,
            y = rect.y + yOffset,
            width = rect.width  + rect.x / 1.75f,
            height = rect.height - yOffset - EditorGUIUtility.singleLineHeight * 0.2f
        };
        yOffset += EditorGUIUtility.singleLineHeight * 0.25f;
        GUI.Box(boxRect, GUIContent.none);
        using (new EditorGUI.IndentLevelScope())
        {
            SerializedObject serializedObject = new SerializedObject(scriptableObject);
            serializedObject.Update();

            using (var iterator = serializedObject.GetIterator())
            {


                if (iterator.NextVisible(true))
                {
                    do
                    {
                        SerializedProperty childProperty = serializedObject.FindProperty(iterator.name);
                        if (childProperty.name.Equals("m_Script", System.StringComparison.Ordinal))
                        {
                            continue;
                        }
                        float childHeight = EditorGUI.GetPropertyHeight(childProperty, includeChildren: true);
                        Rect childRect = new Rect()
                        {
                            x = rect.x,
                            y = rect.y + yOffset,
                            width = rect.width,
                            height = childHeight
                        };
                        using (new EditorGUI.DisabledScope(true))
                            EditorGUI.PropertyField(childRect, childProperty, new GUIContent(childProperty.displayName), true);
                        yOffset += childHeight;
                    }
                    while (iterator.NextVisible(false));
                }
            }

            serializedObject.ApplyModifiedProperties();
        }
    }


}
#endif