#if UNITY_EDITOR
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using static UnityEditor.ShaderUtil;

[CustomPropertyDrawer(typeof(MaterialPropierty))]
public class MaterialPropertyPropetyDrawer : PropertyDrawer
{
    private string[] names = null;
    private string[] displayeNames = null;
    private int[] shaderPropiertiesIndex = null;
    private ShaderPropertyType[] shaderPropiertiesType;
    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        float offset = 0;
        if (property.isExpanded)
        {
            offset += EditorGUIUtility.singleLineHeight * 6;
            if (property.FindPropertyRelative("SetNewValue").boolValue)
                offset += EditorGUIUtility.singleLineHeight;
        }
        return base.GetPropertyHeight(property, label) + offset;
    }
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        float acumulativeHeigth = EditorGUIUtility.singleLineHeight;
        EditorGUI.BeginProperty(position, GUIContent.none, property);
        Rect propertyRect = new Rect()
        {
            x = position.x,
            y = position.y,
            width = position.width,
            height = acumulativeHeigth
        };
        SerializedProperty MaterialPropertyNameProp = property.FindPropertyRelative("materialPropertyName");
        SerializedProperty MaterialPropertyStringType = property.FindPropertyRelative("materialPropertyStringType");

        property.isExpanded = EditorGUI.Foldout(propertyRect, property.isExpanded, label, toggleOnLabelClick: true);
        if (property.isExpanded)
        {
            Rect selectRect = new Rect()
            {
                x = position.x,
                y = position.y + acumulativeHeigth,
                width = position.width,
                height = EditorGUIUtility.singleLineHeight
            };
            acumulativeHeigth += EditorGUIUtility.singleLineHeight;
            SerializedProperty rendererProp = property.FindPropertyRelative("renderer");
            Renderer renderer = (rendererProp.objectReferenceValue as Renderer);
            renderer = EditorGUI.ObjectField(selectRect, renderer, typeof(Renderer), true) as Renderer;
            selectRect.y = position.y + acumulativeHeigth;
            acumulativeHeigth += EditorGUIUtility.singleLineHeight;
            EditorGUI.PropertyField(selectRect, property.FindPropertyRelative("instantiateMaterial"), new GUIContent("Instantiate Material"), false);
            Material material = null;
            if (renderer != null)
            {
                rendererProp.objectReferenceValue = renderer;
                material = renderer.sharedMaterial;
            }
            if (material != null)
            {
                UpdateNames(material);
                int currentIndex = GetIndex(MaterialPropertyNameProp.stringValue);
                Rect popUpRect = new Rect()
                {
                    x = position.x,
                    y = position.y + acumulativeHeigth,
                    width = position.width,
                    height = EditorGUIUtility.singleLineHeight
                };
                acumulativeHeigth += EditorGUIUtility.singleLineHeight;
                int newIndex = EditorGUI.Popup(popUpRect, currentIndex, displayeNames);
                bool valueChanged = false;
                if ((newIndex != currentIndex) && (0 <= newIndex && newIndex < displayeNames.Length))
                {
                    currentIndex = newIndex;
                    MaterialPropertyNameProp.stringValue = names[currentIndex];
                    MaterialPropertyStringType.stringValue = shaderPropiertiesType[currentIndex].ToString();
                    if (shaderPropiertiesType[currentIndex] == ShaderPropertyType.Range)
                        MaterialPropertyStringType.stringValue = ShaderPropertyType.Float.ToString();
                    if (shaderPropiertiesType[currentIndex] == ShaderPropertyType.TexEnv)
                        MaterialPropertyStringType.stringValue = "Texture";
                    valueChanged = true;
                }
                ShowFieldVisualEffect(currentIndex, material, position, property, $"{displayeNames[currentIndex]}", valueChanged);
            }
        }
        using (new EditorGUI.DisabledScope(true))
            EditorGUI.PropertyField(propertyRect, MaterialPropertyNameProp, label, false);
        EditorGUI.EndProperty();
    }
    private void UpdateNames(Material material)
    {
        List<string> listNames = new List<string>();
        List<string> listDisplayNames = new List<string>();
        List<int> listPropiertiesIndex = new List<int>();
        List<ShaderPropertyType> listPropiertiesTypes = new List<ShaderPropertyType>();
        int amountOfPropierties = GetPropertyCount(material.shader);
        for (int i = 0; i < amountOfPropierties; i++)
        {
            if (IsShaderPropertyHidden(material.shader, 1))
                continue;
            string propertyName = GetPropertyName(material.shader, i);
            if (propertyName.StartsWith("_Queue"))
                continue;
            if (propertyName.StartsWith("unity_"))
                continue;
            listNames.Add(propertyName);
            if (propertyName.StartsWith("_"))
                propertyName = propertyName.Substring(1);
            listDisplayNames.Add(propertyName);
            listPropiertiesTypes.Add(GetPropertyType(material.shader, i));
            listPropiertiesIndex.Add(i);
        }
        names = listNames.ToArray();
        displayeNames = listDisplayNames.ToArray();
        shaderPropiertiesIndex = listPropiertiesIndex.ToArray();
        shaderPropiertiesType = listPropiertiesTypes.ToArray();
    }
    private int GetIndex(string currentSelection)
    {
        for (int i = 0; i < names.Length; i++)
        {
            if (string.Equals(currentSelection, names[i]))
                return i;
        }
        return 0;
    }

    private void ShowFieldVisualEffect(int index, Material material, Rect position, SerializedProperty property, string guiLabel, bool valueChanged)
    {
        SerializedProperty setNewValueProp = property.FindPropertyRelative("SetNewValue");
        Rect boolRect = new Rect()
        {
            x = position.x,
            y = position.y + EditorGUIUtility.singleLineHeight * 5,
            width = position.width,
            height = EditorGUIUtility.singleLineHeight
        };
        EditorGUI.PropertyField(boolRect, setNewValueProp, new GUIContent("Set new value"), false);
        Rect startFieldRect = new Rect()
        {
            x = position.x,
            y = position.y + EditorGUIUtility.singleLineHeight * 4,
            width = position.width,
            height = EditorGUIUtility.singleLineHeight
        };
        Rect newFieldRect = new Rect()
        {
            x = position.x,
            y = position.y + EditorGUIUtility.singleLineHeight * 6,
            width = position.width,
            height = EditorGUIUtility.singleLineHeight
        };
        string newPropertyName;
        SerializedProperty startValueProp;
        bool isRange = false;
        float minValueRange = 0;
        float maxValueRange = 1;
        switch (shaderPropiertiesType[index])
        {
            /*case var boolType when boolType == typeof(bool):
                propertyName = "NewBoolValue";
                break;
            */
            case ShaderPropertyType.Int:
                startValueProp = property.FindPropertyRelative("startIntValue");
                if (!setNewValueProp.boolValue && valueChanged)
                    startValueProp.intValue = material.GetInt(names[index]);
                newPropertyName = "newIntValue";
                break;
            case ShaderPropertyType.Float:
                startValueProp = property.FindPropertyRelative("startFloatValue");
                if (!setNewValueProp.boolValue)
                    startValueProp.floatValue = material.GetFloat(names[index]);
                newPropertyName = "newFloatValue";
                break;
            case ShaderPropertyType.Range:
                isRange = true;
                minValueRange = GetRangeLimits(material.shader, shaderPropiertiesIndex[index], 1);
                maxValueRange = GetRangeLimits(material.shader, shaderPropiertiesIndex[index], 2);
                startValueProp = property.FindPropertyRelative("startFloatValue");
                if (!setNewValueProp.boolValue && valueChanged)
                    startValueProp.floatValue = material.GetFloat(names[index]);
                newPropertyName = "newFloatValue";
                break;
            case ShaderPropertyType.Vector:
                startValueProp = property.FindPropertyRelative("startVectorValue");
                if (!setNewValueProp.boolValue && valueChanged)
                    startValueProp.vector4Value = material.GetVector(names[index]);
                newPropertyName = "newVectorValue";
                break;
            case ShaderPropertyType.Color:
                startValueProp = property.FindPropertyRelative("startColorValue");
                if (!setNewValueProp.boolValue && valueChanged)
                    startValueProp.colorValue = material.GetColor(names[index]);
                newPropertyName = "newColorValue";
                break;
            case ShaderPropertyType.TexEnv:
                startValueProp = property.FindPropertyRelative("startTextureValue");
                 if (!setNewValueProp.boolValue && valueChanged)
                     startValueProp.objectReferenceValue = material.GetTexture(names[index]);
                newPropertyName = "newTextureValue";
                break;
            default:
                return;
        }
        SerializedProperty newValueProp = property.FindPropertyRelative(newPropertyName);
        GUIContent startLabel = new GUIContent($"Start{ guiLabel }");
        GUIContent newLabel = new GUIContent($"New{ guiLabel }");
        using (new EditorGUI.DisabledScope(setNewValueProp.boolValue))
        {
            if (isRange)
                EditorGUI.Slider(startFieldRect, startValueProp, minValueRange, maxValueRange, startLabel);
            else
                EditorGUI.PropertyField(startFieldRect, startValueProp, startLabel, false);
        }
        using (new EditorGUI.DisabledScope(!setNewValueProp.boolValue))
        {
            if (isRange)
                EditorGUI.Slider(newFieldRect, newValueProp, minValueRange, maxValueRange, newLabel);
            else
                EditorGUI.PropertyField(newFieldRect, newValueProp, newLabel, false);
        }
    }
}
#endif