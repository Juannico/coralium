#if UNITY_EDITOR
using NaughtyAttributes.Editor;
using System.Reflection;
using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(ShowBookPageAttribute))]
public class ShowBookPagePropertyDrawer : PropertyDrawer
{
    private float helpBoxHeight = EditorGUIUtility.singleLineHeight * 2;

    private string helpboxText;
    private object target;
    private SerializedProperty bookPageProperty;
    sealed override public float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        float totalHeight = EditorGUI.GetPropertyHeight(property, includeChildren: true);
        BookPage bookPage = GetBookPage(property);
        if (bookPage == null)
        {
            totalHeight += EditorGUI.GetPropertyHeight(property, includeChildren: true) + helpBoxHeight / 2;
            return totalHeight;
        }
        if (!property.isExpanded)
            return totalHeight;
        totalHeight += EditorGUIUtility.singleLineHeight;
        
        FieldInfo[] fields = bookPage.GetType().GetFields(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
        foreach (FieldInfo field in fields)
        {
            SerializedProperty childProperty = bookPageProperty.FindPropertyRelative(field.Name);
            if (childProperty == null)
                continue;
            bool visible = PropertyUtility.IsVisible(childProperty);
            if (!visible)
                continue;
            float height = EditorGUI.GetPropertyHeight(childProperty, includeChildren: true);
            totalHeight += height;
        }
        
        // totalHeight += EditorGUIUtility.standardVerticalSpacing;
        return totalHeight;
    }
    public sealed override void OnGUI(Rect rect, SerializedProperty property, GUIContent label)
    {
        Rect propertyRect = new Rect()
        {
            x = rect.x,
            y = rect.y,
            width = rect.width,
            height = EditorGUIUtility.singleLineHeight
        };
        BookPage bookPage = GetBookPage(property);
        if (bookPage == null)
        {
            Rect HelpBoxRect = new Rect()
            {
                x = rect.x,
                y = rect.y,
                width = rect.width,
                height = helpBoxHeight
            };
            NaughtyEditorGUI.HelpBox(HelpBoxRect, helpboxText, MessageType.Error);
            propertyRect.y += EditorGUI.GetPropertyHeight(property, includeChildren: true) + helpBoxHeight / 2;
            EditorGUI.PropertyField(propertyRect, property, label, false);
            return;
        }
        Rect foldoutRect = new Rect()
        {
            x = rect.x,
            y = rect.y,
            width = EditorGUIUtility.labelWidth,
            height = EditorGUIUtility.singleLineHeight
        };
        label.text = $"Page number {property.intValue}";
        property.isExpanded = EditorGUI.Foldout(foldoutRect, property.isExpanded, label, toggleOnLabelClick: true);
        EditorGUI.PropertyField(propertyRect, property, label, false);
        if (!property.isExpanded)
            return;
        ShowPageChildPropierties(rect, bookPage, 0);
    }
    private BookPage GetBookPage(SerializedProperty property)
    {
        ShowBookPageAttribute showDataAttribute = (ShowBookPageAttribute)attribute;
        target = PropertyUtility.GetTargetObjectWithProperty(property);
        if (showDataAttribute == null)
            return null;
        FieldInfo bookFieldInfo = ReflectionUtility.GetField(target, showDataAttribute.Book);
        Book book = (Book)bookFieldInfo.GetValue(target);
        if (book == null)
        {
            helpboxText = $"The variable {showDataAttribute.Book} is not assigned ";
            return null;
        }
        if (property.intValue >= book.Pages.Length || property.intValue < 0)
        {
            helpboxText = $"Does not exist pages with index: {property.intValue}";
            return null;
        }
        SerializedProperty bookPagesProperty = GetBookPagesSerizlizedPropierties(book);
        bookPageProperty = bookPagesProperty.GetArrayElementAtIndex(property.intValue);
        return book.Pages[property.intValue];
    }

    private static SerializedProperty GetBookPagesSerizlizedPropierties(ScriptableObject scriptableObject)
    {
        SerializedObject serializedObject = new SerializedObject(scriptableObject);
        serializedObject.Update();

        using (var iterator = serializedObject.GetIterator())
        {
            if (iterator.NextVisible(true))
            {
                do
                {
                    SerializedProperty childProperty = serializedObject.FindProperty(iterator.name);
                    if (childProperty.name.Equals("Pages", System.StringComparison.Ordinal))
                        return childProperty;
                }
                while (iterator.NextVisible(false));
            }
        }
        return null;
    }

    private void ShowPageChildPropierties(Rect rect, BookPage bookPage, float yOffset)
    {
        yOffset += EditorGUIUtility.singleLineHeight * 1.1f;
        Rect boxRect = new Rect()
        {
            x = rect.x / 2,
            y = rect.y + yOffset,
            width = rect.width + rect.x / 1.75f,
            height = rect.height - yOffset - EditorGUIUtility.singleLineHeight * 0.2f
        };
        yOffset += EditorGUIUtility.singleLineHeight * 0.25f;
        GUI.Box(boxRect, GUIContent.none);

        FieldInfo[] fields = bookPage.GetType().GetFields(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.InvokeMethod);
        foreach (FieldInfo field in fields)
        {
            SerializedProperty childProperty = bookPageProperty.FindPropertyRelative(field.Name);
            if (childProperty == null)
                continue;
            float childHeight = EditorGUI.GetPropertyHeight(childProperty, includeChildren: true);
            Rect childRect = new Rect()
            {
                x = rect.x,
                y = rect.y + yOffset,
                width = rect.width,
                height = childHeight
            };
            using (new EditorGUI.DisabledScope(true))
                EditorGUI.PropertyField(childRect, childProperty, new GUIContent(childProperty.displayName), true);
            yOffset += childHeight;
        }
    }

}
#endif