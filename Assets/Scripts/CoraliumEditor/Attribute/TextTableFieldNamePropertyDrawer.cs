#if UNITY_EDITOR
using PixelCrushers;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(TextTableFieldName))]
public class TextTableFieldNamePropertyDrawer : PropertyDrawer
{
    private string[] names = null;
    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        float offset = EditorGUIUtility.singleLineHeight;
        if (property.isExpanded)
            offset += EditorGUIUtility.singleLineHeight * 2;
        return base.GetPropertyHeight(property, label) + offset;
    }
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(position, GUIContent.none, property);
        SerializedProperty fieldNameproperty = property.FindPropertyRelative("FieldName");
        position.height = EditorGUIUtility.singleLineHeight;
        SerializedProperty colorPropierty = property.FindPropertyRelative("TextColor");
        GUIContent colorLabel = new GUIContent("Color text");
        EditorGUI.PropertyField(position, colorPropierty, colorLabel, false);
        position.y += EditorGUIUtility.singleLineHeight;
        property.isExpanded = EditorGUI.Foldout(position, property.isExpanded, label, toggleOnLabelClick: true);
        if (property.isExpanded)
        {
            Rect selectRect = new Rect()
            {
                x = position.x,
                y = position.y + EditorGUIUtility.singleLineHeight,
                width = position.width,
                height = EditorGUIUtility.singleLineHeight
            };
            SerializedProperty textTableProp = property.FindPropertyRelative("TextTable");
            TextTable textTable = textTableProp.objectReferenceValue as TextTable;
            textTable = EditorGUI.ObjectField(selectRect, textTable, typeof(TextTable), true) as TextTable;
            if (textTable != null)
            {
                textTableProp.objectReferenceValue = textTable;
                UpdateNames(textTable);
                int currentIndex = GetIndex(fieldNameproperty.stringValue);
                Rect popUpRect = new Rect()
                {
                    x = position.x,
                    y = position.y + EditorGUIUtility.singleLineHeight * 2,
                    width = position.width,
                    height = EditorGUIUtility.singleLineHeight
                };
                int newIndex = EditorGUI.Popup(popUpRect, currentIndex, names);
                if ((newIndex != currentIndex) && (0 <= newIndex && newIndex < names.Length))
                {
                    currentIndex = newIndex;
                    fieldNameproperty.stringValue = names[currentIndex];
                }
            }
        }
        using (new EditorGUI.DisabledScope(true))
            EditorGUI.PropertyField(position, fieldNameproperty, label, false);
        EditorGUI.EndProperty();
    }
    private void UpdateNames(TextTable textTable)
    {
        List < string> list = new List<string>();
        if (textTable.fields.Count == 0)
            return;
        foreach (TextTableField value in textTable.fields.Values)
            list.Add(value.fieldName);
        names = list.ToArray();
    }
    private int GetIndex(string currentSelection)
    {
        for (int i = 0; i < names.Length; i++)
        {
            if (string.Equals(currentSelection, names[i]))
                return i;
        }
        return -1;
    }
}
#endif 