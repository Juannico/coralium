#if UNITY_EDITOR
using NaughtyAttributes.Editor;
using UnityEditor;
using UnityEngine;
[CustomPropertyDrawer(typeof(CheckISaveJSONPathAttribute))]
public class CheckISaveJSONPathPropertyDrawer : PropertyDrawer
{
    private float helpBoxHeight = EditorGUIUtility.singleLineHeight * 2;
    sealed override public float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        float totalHeight = EditorGUI.GetPropertyHeight(property, includeChildren: true);
        ScriptableObject scriptableObject = property.objectReferenceValue as ScriptableObject;
        if (scriptableObject == null)
            return totalHeight;
        if (scriptableObject is ISaveSOJSONPath)
            return totalHeight;
        totalHeight += EditorGUI.GetPropertyHeight(property, includeChildren: true) + helpBoxHeight / 2;
        return totalHeight;
    }
    public sealed override void OnGUI(Rect rect, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(rect, label, property);
        Rect propertyRect = new Rect()
        {
            x = rect.x,
            y = rect.y,
            width = rect.width,
            height = EditorGUIUtility.singleLineHeight
        };
        if (property.objectReferenceValue == null)
        {
            EditorGUI.EndProperty();
            return;
        }
        ScriptableObject scriptableObject = property.objectReferenceValue as ScriptableObject;
        if (!(scriptableObject is ISaveSOJSONPath))
        {
            Rect HelpBoxRect = new Rect()
            {
                x = rect.x,
                y = rect.y,
                width = rect.width,
                height = helpBoxHeight
            };
            rect.y += HelpBoxRect.height;
            NaughtyEditorGUI.HelpBox(HelpBoxRect, $"The ScriptableObject is not type {typeof(ISaveSOJSONPath)}", MessageType.Error);
            propertyRect.y += EditorGUI.GetPropertyHeight(property, includeChildren: true) + helpBoxHeight / 2;
        }
        EditorGUI.PropertyField(propertyRect, property, label, true);
        EditorGUI.EndProperty();
    }
}
#endif