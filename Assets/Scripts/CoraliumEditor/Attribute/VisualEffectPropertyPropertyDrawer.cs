#if UNITY_EDITOR
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.VFX;

[CustomPropertyDrawer(typeof(VisualEffectProperty))]
public class VisualEffectPropertyPropertyDrawer : PropertyDrawer
{
    private VFXExposedProperty[] exposedProperties;
    private string[] names = null;
    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        float offset = EditorGUIUtility.singleLineHeight;
        if (property.isExpanded)
            offset += EditorGUIUtility.singleLineHeight * 3;
        if(property.FindPropertyRelative("setNewValue").boolValue)
            offset += EditorGUIUtility.singleLineHeight ;
        return base.GetPropertyHeight(property, label) + offset;
    }
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(position, GUIContent.none, property);
        Rect propertyRect = new Rect()
        {
            x = position.x,
            y = position.y,
            width = position.width,
            height = EditorGUIUtility.singleLineHeight
        };
        SerializedProperty visualEffectPropertyNameProp = property.FindPropertyRelative("visualEffectPropertyName");
        SerializedProperty visualEffectPropertyTypeProp = property.FindPropertyRelative("visualEffectPropertyStringType");

        property.isExpanded = EditorGUI.Foldout(propertyRect, property.isExpanded, label, toggleOnLabelClick: true);
        if (property.isExpanded)
        {
            Rect selectRect = new Rect()
            {
                x = position.x,
                y = position.y + EditorGUIUtility.singleLineHeight,
                width = position.width,
                height = EditorGUIUtility.singleLineHeight
            };
            SerializedProperty visualEffectProp = property.FindPropertyRelative("visualEffect");
            VisualEffect visualEffect = visualEffectProp.objectReferenceValue as VisualEffect;
            visualEffect = EditorGUI.ObjectField(selectRect, visualEffect, typeof(VisualEffect), true) as VisualEffect;
            if (visualEffect != null)
            {
                visualEffectProp.objectReferenceValue = visualEffect;
                UpdateNames(visualEffect);
                int currentIndex = GetIndex(visualEffectPropertyNameProp.stringValue);
                Rect popUpRect = new Rect()
                {
                    x = position.x,
                    y = position.y + EditorGUIUtility.singleLineHeight * 2,
                    width = position.width,
                    height = EditorGUIUtility.singleLineHeight
                };
                int newIndex = EditorGUI.Popup(popUpRect, currentIndex, names);
                if ((newIndex != currentIndex) && (0 <= newIndex && newIndex < names.Length))
                {
                    currentIndex = newIndex;
                    visualEffectPropertyNameProp.stringValue = names[currentIndex];
                    visualEffectPropertyTypeProp.stringValue = exposedProperties[currentIndex].type.ToString();
                }
                SerializedProperty setNewValueProp = property.FindPropertyRelative("setNewValue");
                Rect boolRect = new Rect()
                {
                    x = position.x,
                    y = position.y + EditorGUIUtility.singleLineHeight * 3,
                    width = position.width,
                    height = EditorGUIUtility.singleLineHeight
                };
                EditorGUI.PropertyField(boolRect, setNewValueProp, new GUIContent("Set new value"), false);
                if (setNewValueProp.boolValue)
                    ShowFieldVisualEffect(exposedProperties[currentIndex].type, position, property, $"New {names[currentIndex]}");
            }
        }
        using (new EditorGUI.DisabledScope(true))
            EditorGUI.PropertyField(propertyRect, visualEffectPropertyNameProp, label, false);
        EditorGUI.EndProperty();
    }
    private void UpdateNames(VisualEffect visualEffect)
    {
        List<string> nameList = new List<string>();
        List<VFXExposedProperty> exposedProperties = new List<VFXExposedProperty>();
        visualEffect.visualEffectAsset.GetExposedProperties(exposedProperties);
        for (int i = 0; i < exposedProperties.Count; i++)
        {
            nameList.Add(exposedProperties[i].name);
        }
        names = nameList.ToArray();
        this.exposedProperties = exposedProperties.ToArray();
    }
    private int GetIndex(string currentSelection)
    {
        for (int i = 0; i < names.Length; i++)
        {
            if (string.Equals(currentSelection, names[i]))
                return i;
        }
        return 0;
    }

    private void ShowFieldVisualEffect(System.Type visualEffectType, Rect position, SerializedProperty property, string guiLabel)
    {
        Rect fieldRect = new Rect()
        {
            x = position.x,
            y = position.y + EditorGUIUtility.singleLineHeight * 4,
            width = position.width,
            height = EditorGUIUtility.singleLineHeight
        };
        string propertyName = "";
        switch (visualEffectType)
        {
            case var boolType when boolType == typeof(bool):
                propertyName = "newBoolValue";
                break;

            case var intType when intType == typeof(int):
                propertyName = "newIntValue";
                break;

            case var floatType when floatType == typeof(float):
                propertyName = "newFloatValue";
                break;

            case var vector2Type when vector2Type == typeof(Vector2):
                propertyName = "newVector2Value";
                break;

            case var vector3Type when vector3Type == typeof(Vector3):
                propertyName = "newVector3Value";
                break;
            case var colorType when colorType == typeof(Vector4):
                propertyName = "newColorValue";
                break;
            case var colorType when colorType == typeof(Texture):
                propertyName = "newTextureValue";
                break;
            default:
                return;
        }
        SerializedProperty newValueProp = property.FindPropertyRelative(propertyName);
        GUIContent label = new GUIContent(guiLabel);
        EditorGUI.PropertyField(fieldRect, newValueProp, label, false);
    }
}
#endif