#if UNITY_EDITOR
using FMODUnity;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(EventEmitterData))]
public class EventEmitterDataProperdyDrawer : PropertyDrawer
{
    private EditorEventRef editorEventRef;
    private EditorParamRef editorParamRef;
    private SerializedProperty eventReferenceProp;
    private string[] parameters;

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        float offset = 0;
        if (property.isExpanded)
        {
            offset += EditorGUIUtility.singleLineHeight * 3;
            if (editorEventRef != null && eventReferenceProp.isExpanded)
                offset += EditorGUIUtility.singleLineHeight * 5;
        }
        return offset + base.GetPropertyHeight(property, label);
    }

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(position, GUIContent.none, property);
        Rect propertyRect = new Rect()
        {
            x = position.x,
            y = position.y,
            width = position.width,
            height = EditorGUIUtility.singleLineHeight
        };
        EditorGUI.PropertyField(propertyRect, property, label, false);
        if (property.isExpanded)
        {
            EditorGUI.indentLevel++;
            eventReferenceProp = property.FindPropertyRelative("EventReference");
            Rect eventRect = new()
            {
                x = position.x,
                y = position.y + EditorGUIUtility.singleLineHeight,
                width = position.width,
                height = EditorGUIUtility.singleLineHeight
            };
            EditorGUI.PropertyField(eventRect, eventReferenceProp, new GUIContent("Event"), false);
            SerializedProperty pathProperty = eventReferenceProp.FindPropertyRelative("Path");
            editorEventRef = EventManager.EventFromString(pathProperty.stringValue);
            SerializedProperty parameterNameProperty = property.FindPropertyRelative("ParameterName");
            if (editorEventRef != null)
            {
                UpdateParameters();
                int currentIndex = GetIndex(parameterNameProperty.stringValue);
                Rect parameterNameRect = new()
                {
                    x = eventRect.x,
                    y = eventRect.y + EditorGUIUtility.singleLineHeight,
                    width = eventRect.width,
                    height = EditorGUIUtility.singleLineHeight
                };
                if (eventReferenceProp.isExpanded)
                    parameterNameRect.y += EditorGUIUtility.singleLineHeight * 5;
                if (eventReferenceProp.isExpanded)
                    position.y += EditorGUIUtility.singleLineHeight * 5;
                int newIndex = EditorGUI.Popup(parameterNameRect, "Parameter Name", currentIndex, parameters);
                ShowValueControl(property, parameterNameProperty, parameterNameRect, newIndex);
            }
            EditorGUI.indentLevel--;
        }
        EditorGUI.EndProperty();
    }

    private void ShowValueControl(SerializedProperty property, SerializedProperty parameterNameProperty, Rect parameterNameRect, int newIndex)
    {
        Rect parameterValueRect = new()
        {
            x = parameterNameRect.x,
            y = parameterNameRect.y + EditorGUIUtility.singleLineHeight,
            width = parameterNameRect.width,
            height = EditorGUIUtility.singleLineHeight
        };
        if (0 <= newIndex && newIndex < parameters.Length)
        {
            SerializedProperty parameterIndexroperty = property.FindPropertyRelative("ParameterIndex");
            parameterIndexroperty.intValue = newIndex;
            parameterNameProperty.stringValue = parameters[newIndex];
            editorParamRef = editorEventRef.LocalParameters[newIndex];
            SerializedProperty parameterValueProperty = property.FindPropertyRelative("ParameterValue");
            float parameterValue = 0;

            switch (editorParamRef.Type)
            {
                case ParameterType.Continuous:
                    parameterValue = EditorGUI.Slider(parameterValueRect, "Parameter Value", parameterValueProperty.floatValue, editorParamRef.Min, editorParamRef.Max);
                    break;
                case ParameterType.Discrete:
                    parameterValue = EditorGUI.IntSlider(parameterValueRect, "Parameter Value", (int)parameterValueProperty.floatValue, (int)editorParamRef.Min, (int)editorParamRef.Max);
                    break;
                case ParameterType.Labeled:
                    parameterValue = EditorGUI.Popup(parameterValueRect, "Parameter Value", (int)parameterValueProperty.floatValue, editorParamRef.Labels);
                    break;
            }
            parameterValueProperty.floatValue = parameterValue;
            return;
        }
        EditorGUI.LabelField(parameterValueRect, "Parameter Value");
    }

    private void UpdateParameters()
    {
        List<string> list = new List<string>();
        foreach (EditorParamRef editorParam in editorEventRef.LocalParameters)
            list.Add(editorParam.Name);
        parameters = list.ToArray();
    }
    private int GetIndex(string currentSelection)
    {
        for (int i = 0; i < parameters.Length; i++)
        {
            if (string.Equals(currentSelection, parameters[i]))
                return i;
        }
        return -1;
    }

}
#endif