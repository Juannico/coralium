using UnityEngine;
public class ShowItemDataAttribute : PropertyAttribute
{
    public string GameData;
    public ShowItemDataAttribute(string gameData)
    {
        GameData = gameData;
    }
}
