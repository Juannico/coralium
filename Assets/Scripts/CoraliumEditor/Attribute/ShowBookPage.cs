using UnityEngine;

public class ShowBookPageAttribute : PropertyAttribute
{
    public string Book;
    public ShowBookPageAttribute(string book) => Book = book;
}
