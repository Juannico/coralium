#if UNITY_EDITOR
using System;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(CollisionHelper))]
public class CollisionHelperAttributesEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        CollisionHelper myClass = (CollisionHelper)target;
        Delegate[] TriggerEnterDelegate = myClass.TriggerEnter?.GetInvocationList();
        GUIStyle styles = new GUIStyle(GUI.skin.label);
        styles.richText = true;
        styles.fontSize = 11;
        if (TriggerEnterDelegate != null && TriggerEnterDelegate.Length > 0)
        {
            EditorGUILayout.Space();
            EditorGUILayout.LabelField("TriggerEnter Methods:", EditorStyles.boldLabel);
            EditorGUI.indentLevel++;
            foreach (Delegate del in TriggerEnterDelegate)
                EditorGUILayout.LabelField($"<b>{del.Target}</b>: {del.Method.Name}", styles);
            EditorGUI.indentLevel--;
        }
        Delegate[] TriggerExitDelegate = myClass.TriggerExit?.GetInvocationList();
        if (TriggerExitDelegate != null && TriggerExitDelegate.Length > 0)
        {
            EditorGUILayout.Space();
            EditorGUILayout.LabelField("TriggerExit Methods:", EditorStyles.boldLabel);
            EditorGUI.indentLevel++;
            foreach (Delegate del in TriggerExitDelegate)
                EditorGUILayout.LabelField( $"<b>{del.Target}</b>: {del.Method.Name}", styles);
            EditorGUI.indentLevel--;
        }
        Delegate[] TriggerStayDelegate = myClass.TriggerStay?.GetInvocationList();
        if (TriggerStayDelegate != null && TriggerStayDelegate.Length > 0)
        {
            EditorGUILayout.Space();
            EditorGUILayout.LabelField("TriggerStay Methods:", EditorStyles.boldLabel);
            EditorGUI.indentLevel++;
            foreach (Delegate del in TriggerStayDelegate)
                EditorGUILayout.LabelField( $"<b>{del.Target}</b>: {del.Method.Name}", styles);
            EditorGUI.indentLevel--;
        }
        Delegate[] CollisionEnterDelegate = myClass.CollisionEnter?.GetInvocationList();
        if (CollisionEnterDelegate != null && CollisionEnterDelegate.Length > 0)
        {
            EditorGUILayout.Space();
            EditorGUILayout.LabelField("CollisionEnter Methods:", EditorStyles.boldLabel);
            EditorGUI.indentLevel++;
            foreach (Delegate del in CollisionEnterDelegate)
                EditorGUILayout.LabelField( $"<b>{del.Target}</b>: {del.Method.Name}", styles);
            EditorGUI.indentLevel--;
        }
        Delegate[] CollisionExitDelegate = myClass.CollisionExit?.GetInvocationList();
        if (CollisionExitDelegate != null && CollisionExitDelegate.Length > 0)
        {
            EditorGUILayout.Space();
            EditorGUILayout.LabelField("CollisionExit Methods:", EditorStyles.boldLabel);
            EditorGUI.indentLevel++;
            foreach (Delegate del in CollisionExitDelegate)
                EditorGUILayout.LabelField(del.Target.ToString() + ":" + del.Method.Name);
            EditorGUI.indentLevel--;
        }
        Delegate[] CollisionStayDelegate = myClass.CollisionStay?.GetInvocationList();
        if (CollisionStayDelegate != null && CollisionStayDelegate.Length > 0)
        {
            EditorGUILayout.Space();
            EditorGUILayout.LabelField("CollisionStay Methods:", EditorStyles.boldLabel);
            EditorGUI.indentLevel++;
            foreach (Delegate del in CollisionStayDelegate)
                EditorGUILayout.LabelField( $"<b>{del.Target}</b>: {del.Method.Name}", styles);
            EditorGUI.indentLevel--;
        }
    }
}
#endif