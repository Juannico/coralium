using NaughtyAttributes;
using System;
using UnityEngine;

[Serializable]
public class BookPage 
{
    [SerializeField] private bool found;
    public TextTableFieldName Title;
    [AllowNesting] [ShowAssetPreview] public Sprite Image;
    public bool Found
    {
        get => found;
        set
        {
            found = value;
            OnFoundChange?.Invoke(value);
        }
    }
    public Action<bool> OnFoundChange;
}
