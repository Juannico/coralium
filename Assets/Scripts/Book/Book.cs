using UnityEngine;
[CreateAssetMenu(fileName = "Book", menuName = "Coralium/Book", order = 52)]
public class Book : ScriptableObject, IResetSOOnExitPlay
{
    public BookPage[] Pages;

    public void ResetOnExitPlay()
    {
        for (int i = 0; i < Pages.Length; i++)
            Pages[i].Found = false;
    }
}
