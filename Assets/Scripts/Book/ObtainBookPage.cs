using FMODUnity;
using System;
using UnityEngine;
using NaughtyAttributes;

public class ObtainBookPage : Dropeable
{
    [Header("ObtainAmuletSettings")]
    [SerializeField] private Book book;
    [ShowBookPage("book")] [SerializeField] private int pageIndex;
    [SerializeField] private Transform targetTransform;
    [Header("SFX")]
    [SerializeField] private StudioEventEmitter collectSFX;
    protected override void Start()
    {
        if (!book.Pages[pageIndex].Found)
        {
            gameObject.SetActive(false);
            gameObject.AddComponent<UnHideGameObject>();
            return;
        }
        IsCollected = true;
        base.Start();

    }
    public override void StartToCollect(Transform otherTransform, Action action)
    {
        base.StartToCollect(otherTransform, action);
        collectSFX?.Play();
        book.Pages[pageIndex].Found = true;
        gameObject.SetActive(false);
    }
    public void DropPage()
    {
        gameObject.SetActive(true);
        DropObject(targetTransform.position);
    }
}
