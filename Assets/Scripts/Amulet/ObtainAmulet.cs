using FMODUnity;
using System;
using UnityEngine;

public class ObtainAmulet : Dropeable
{
    [Header("ObtainAmuletSettings")]
    [SerializeField] private Amulet amulet;
    [SerializeField] private Transform targetTransform;
    [Header("SFX")]
    [SerializeField] private StudioEventEmitter collectSFX;
    protected override void Start()
    {
        if (!amulet.Purchased)
        {
            gameObject.SetActive(false);
            gameObject.AddComponent<UnHideGameObject>();
            return;
        }
        IsCollected = true;
        base.Start();
    }
    public override void StartToCollect(Transform otherTransform, Action action)
    {
        base.StartToCollect(otherTransform, action);
        collectSFX?.Play();
        amulet.Purchased = true;
        amulet.Initialize(otherTransform.gameObject);
        gameObject.SetActive(false);
    }
    public void DropAmulet()
    {
        gameObject.SetActive(true);
        DropObject(targetTransform.position);
    }

    private void OnDrawGizmosSelected()
    {
        if (targetTransform == null)
            return;
        Gizmos.color = Color.cyan;
        Gizmos.DrawSphere(targetTransform.position, 1);
    }
}
