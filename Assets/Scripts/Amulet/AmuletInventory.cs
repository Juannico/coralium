using NaughtyAttributes;
using UnityEngine;
[CreateAssetMenu(fileName = "AmuletInventory", menuName = "Coralium/Amulet/Inventory", order = 52)]
public class AmuletInventory : ScriptableObject, IResetSOOnExitPlay
{
    [Expandable] public GameData GameData;
    [SerializeField] private TextTableFieldName ObteinedAmuletText;
    [SerializeField] private IconItemDataPopUp itemTutoDataPopUp;
    [ReadOnly] [Expandable] public Amulet[] equippedAmulets = new Amulet[4];
    [ReadOnly] [Expandable] public Amulet[] Amulets;
    [ShowNonSerializedField] private bool firstAmulet = true;
    private bool isInitilized = false;

    /// <summary>
    /// Create the amulet array and initialize them by assigning the Player GameObject.
    /// </summary>
    /// <param name="overwrite">Allows overwriting the amulets and reassigning the player if they were initialized.</param>
    public void Initialize(bool overwrite = false)
    {
        if (isInitilized)
        {
            if (overwrite)
            {
                GameObject player = GameObject.FindGameObjectWithTag(Tags.Player);
                foreach (Amulet amulet in Amulets)
                {
                    amulet.Initialize(player);
                    if (amulet.IsEquipped)
                        amulet.Equip();
                }
            }
            return;
        }
        isInitilized = true;
        Amulets = new Amulet[12];
        int amuletsIndex = 0;
        Item[] items = GameData.Items;
        GameObject playerGo = GameObject.FindGameObjectWithTag(Tags.Player);
        for (int i = 0; i < items.Length; i++)
        {
            Amulet amulet = items[i] as Amulet;
            if (!amulet)
                continue;
            amulet.Initialize(playerGo);
            if (amulet.IsEquipped)
                amulet.Equip();
            amulet.OnPurchaseChange += ShowTutoAmulet;
            Amulets[amuletsIndex] = amulet;
            amuletsIndex++;
        }
    }
    private void ShowTutoAmulet(bool purchaseState)
    {
        if (!Application.isPlaying)
            return;
        if (!purchaseState)
            return;
        if (!firstAmulet)
            return;
        DialogueTutorialPanel ItemCollectedPopUp = GameManager.Instance.UIManager.ItemCollectedTutoPopUp;
        if (itemTutoDataPopUp == null)
            return;
        firstAmulet = false;
        ItemCollectedPopUp.OpenPanel(itemTutoDataPopUp);
    }
    private void ShowAmuletObtained(bool purchaseState)
    {
        if (!Application.isPlaying)
            return;
        if (!purchaseState)
            return;
        ShowIconItemCollected iconItemCollectedTutoPopUp = GameManager.Instance.UIManager.IconItemCollectedTutoPopUp;
        if (iconItemCollectedTutoPopUp != null)
            iconItemCollectedTutoPopUp.Show(ObteinedAmuletText, itemTutoDataPopUp.Icon);
    }

    #region Reset OnSwitchPlaymode
    public void ResetOnExitPlay()
    {
        firstAmulet = true;
        isInitilized = false;
        if (Amulets == null)
            return;
        if (Amulets.Length == 0)
            return;
        foreach (Amulet amulet in Amulets)
        {
            if (amulet == null)
                continue;
            amulet.OnPurchaseChange -= ShowTutoAmulet;
            amulet.OnPurchaseChange -= ShowAmuletObtained;
        }
    }
    #endregion

}
