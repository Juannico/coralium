using Newtonsoft.Json;
using System;
using UnityEngine;

[Serializable]
[JsonObject(MemberSerialization.OptIn)]
public class Amulet : Item, ISaveSOJSONPath
{
    [JsonProperty]
    [Header("Amulet Settings")] [SerializeField] private bool isEquipped;
    public bool IsEquipped
    {
        get => isEquipped;
        set
        {
            isEquipped = value;
            Save();
        }
    }
#if UNITY_EDITOR
    private void OnEnable()
    {
        Save();
    }
#endif

    #region Reusable Amulet Methods
    /// <summary>
    /// Initializes the Amulet, attaching a save delegate when the amulet is purchased.
    /// Child classes can override this method to incorporate custom initialization logic.
    /// </summary>
    public virtual void Initialize(GameObject playerGO) => OnPurchaseChange += SaveOnPurchase;
    /// <summary>
    /// Equip the amulet, marking it as Equipped.
    /// This method is meant to be overridden by child classes to implement custom logic and invoke base method to update "IsEquipped" state.
    /// </summary>
    public virtual void Equip() => IsEquipped = true;
    /// <summary>
    /// Unequip the amulet, not marking it as Equipped.
    /// This method is meant to be overridden by child classes to implement custom logic and invoke base method to update "IsEquipped" state.
    /// </summary>
    public virtual void Unequip() => IsEquipped = false;
    #endregion 

    private void SaveOnPurchase(bool puschaseState) => Save();

    #region Override Item Methods
    public override bool Buy(CollectableData coin)
    {
        if (!base.Buy(coin))
            return false;
        Initialize(GameObject.FindGameObjectWithTag(Tags.Player));
        return true;
    }
    #endregion

    #region Reset OnSwitchPlaymode
    public override void ResetOnExitPlay()
    {
        base.ResetOnExitPlay();
        IsEquipped = false;
    }
    #endregion

    #region SaveJSON
    public string Path { get => $"AmuletData/{name}"; }
    private void Save() => DataManager.SaveObject(this, Path);
    #endregion
}
