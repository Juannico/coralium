using Newtonsoft.Json;
using UnityEngine;

public class EnergyAmulet : Amulet
{
    [Header("Energy Amulet Settings")]
    [JsonIgnore]
    [SerializeField] private int energyAmount;
    protected Data playerData;

    #region Override Amulet Methods
    public override void Initialize(GameObject playerGO)
    {
        base.Initialize(playerGO);
        playerData = playerGO.GetComponentInChildren<Character>().BaseCharacter.Data;
    }
    #endregion

    #region Reusable EnergyAmulet Methods
    protected virtual void AddEnergy() => playerData.CurrentEnergy += energyAmount;
    #endregion

}
