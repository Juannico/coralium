using Newtonsoft.Json;
using UnityEngine;

[CreateAssetMenu(fileName = "CollectorAmulet", menuName = "Coralium/Amulet/Collector")]
public class CollectorAmulet : Amulet
{
    [JsonIgnore]
    [Header("Collector Amulet Settings")]
    [SerializeField] private float distance;
    private Magnet magnet;

    #region Override Amulet Methods
    public override void Initialize(GameObject playerGO)
    {
        base.Initialize(playerGO);        
        magnet = playerGO.GetComponentInParent<PlayerController>().Magnet;
        if(IsEquipped)
            magnet.SetColliderRaius(distance);
        else
            magnet.ResetColliderRaius();
    }
    public override void Equip()
    {
        base.Equip();
        magnet.SetColliderRaius(distance);
    }

    public override void Unequip()
    {
        base.Unequip();
        magnet.ResetColliderRaius();
    }
    #endregion

}
