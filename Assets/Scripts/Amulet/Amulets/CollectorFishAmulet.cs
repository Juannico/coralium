using UnityEngine;

[CreateAssetMenu(fileName = "CollectorFishAmulet", menuName = "Coralium/Amulet/CollectorFish")]
public class CollectorFishAmulet : Amulet
{
}
