using Newtonsoft.Json;
using UnityEngine;

[CreateAssetMenu(fileName = "EcolocalizationAmulet", menuName = "Coralium/Amulet/Ecolocalization")]
public class EcolocalizationAmulet : Amulet
{
    [Header("Ecolocalization Amulet Settings")]
    [JsonIgnore]
    [SerializeField] private BaseAbilityCast ecolocalizationPrefab;
    [JsonIgnore]
    [SerializeField] private PlayerAbilities playerAbilities;
    private BaseAbilityCast ecolocalizationAbility;

    #region Override Amulet Methods
    public override void Initialize(GameObject playerGO)
    {
        base.Initialize(playerGO);
        ecolocalizationAbility = playerAbilities.GetPlayerAbility(ecolocalizationPrefab);
        ecolocalizationAbility.IsLocked = !IsEquipped;
    }
    public override void Equip()
    {
        base.Equip();
        ecolocalizationAbility.IsLocked = false;
    }
    public override void Unequip()
    {
        base.Unequip();
        ecolocalizationAbility.IsLocked = true;
    }
    #endregion

}
