using Newtonsoft.Json;
using UnityEngine;

[CreateAssetMenu(fileName = "IncreaseDamageAmulet", menuName = "Coralium/Amulet/IncreaseDamage")]
public class IncreaseDamageAmulet : Amulet
{
    [Header("IncreaseDamage Amulet Settings")]
    [JsonIgnore]
    [SerializeField] private int increaseDamageMultiplier;

    #region Override Amulet Methods
    private Data playerData;
    public override void Equip()
    {
        base.Equip();
        playerData.DamageMultiplier = increaseDamageMultiplier;
    }
    public override void Unequip()
    {
        base.Unequip();
        playerData.DamageMultiplier = 1;
    }
    public override void Initialize(GameObject playerGO)
    {
        base.Initialize(playerGO);
        playerData = playerGO.GetComponentInChildren<Character>().BaseCharacter.Data;
    }
    #endregion

}
