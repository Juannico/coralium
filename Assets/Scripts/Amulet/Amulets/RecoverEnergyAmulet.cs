using UnityEngine;

[CreateAssetMenu(fileName = "RecoverEnergyAmulet", menuName = "Coralium/Amulet/RecoverEnergy")]
public class RecoverEnergyAmulet : EnergyAmulet, IResetSOOnExitPlay
{
    [Header("RecoverEnergy Amulet Settings")]
    [SerializeField] private bool recoverOnDoDamage;
    private int currentHealth;

    #region Override Amulet Methods
    public override void Initialize(GameObject playerGO)
    {
        base.Initialize(playerGO);
        currentHealth = playerData.CurrentHealth;
        playerData.OnCharacterHealthChange += HeathChanged;
    }
    #endregion

    private void HeathChanged(int newHealth)
    {
        if (!IsEquipped)
        {
            currentHealth = newHealth;
            return;
        }
        if (recoverOnDoDamage)
            return;
        if (newHealth < currentHealth)
            AddEnergy();
        currentHealth = newHealth;
    }

    #region Reset OnSwitchPlaymode
    public override void ResetOnExitPlay()
    {
        base.ResetOnExitPlay();
        if (playerData != null)
            playerData.OnCharacterHealthChange -= HeathChanged;
    }
    #endregion

}

