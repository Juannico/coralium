using Newtonsoft.Json;
using UnityEngine;

[CreateAssetMenu(fileName = "ExtraLifeAmulet", menuName = "Coralium/Amulet/Extra Life")]
public class ExtraLifeAmulet : Amulet
{
    [Header("Statisticcs")]
    [JsonIgnore]
    [SerializeField] private int amountOfLife;
    private Data playerData;

    #region Override Amulet Methods
    public override void Initialize(GameObject playerGO)
    {
        base.Initialize(playerGO);
        playerData = playerGO.GetComponentInParent<PlayerCharacter>().BaseCharacter.Data;
    }
    public override void Equip()
    {
        base.Equip();
        playerData.ExtraHealth = amountOfLife;
    }
    public override void Unequip()
    {
        base.Unequip();
        playerData.ExtraHealth = 0;
    }
    #endregion

}
