using NaughtyAttributes;
using PixelCrushers.DialogueSystem;
using UnityEngine;

public class OpenShopUI : ConversationOnActionButton, ISaveStateJSON
{
    [Header("Open Shop Setting")]
    [SerializeField] private ShopUI shopUIPrefab;
    private ShopUI shopUI;
    [Header("Conversation")]
    [VariablePopup(true)] [SerializeField] private string firstTimeLuaName;
    [VariablePopup(true)] [SerializeField] private string OpenLuaName;
    [Header("LookPlayer")]
    [SerializeField] private bool lookPlayer;
    [ShowIf("lookPlayer")] [SerializeField] private CollisionHelper detectPlayer;
    [ShowIf("lookPlayer")] [SerializeField] private Transform model;
    [ShowIf("lookPlayer")] [SerializeField] private float lookVelocity = 0.2f;
    private bool lookingPlayer;
    private Transform targetPlayer;
    private Vector3 refSmoothVelocity;
    #region Unity Methods
    protected override void Awake()
    {
        base.Awake();
        if (!lookPlayer)
            return;
        detectPlayer.TriggerEnter += TriggerEnter;
        detectPlayer.TriggerExit += TriggerExit;
    }
    private void Update()
    {
        if (lookPlayer)
            HandleLookPlayer();
    }
    #endregion

    #region Override Conversation Methods
    protected override void DoAction()
    {
        DialogueLua.SetVariable(firstTimeLuaName, State);
        DialogueLua.SetVariable(OpenLuaName, true);
        base.DoAction();
    }
    protected override void DelayActionAfterConversation()
    {
        base.DelayActionAfterConversation();
        if (!DialogueLua.GetVariable(OpenLuaName).asBool)
            return;
        if (shopUI == null)
            shopUI = GameManager.Instance.UIManager.SetUIByType(shopUIPrefab);
        DisableActionButton();
        shopUI.OnPanelClosed += CloseShopUI;
        shopUI.gameObject.SetActive(true);
        if (State)
            shopUI.OnPanelClosed += FirsTimeOpened;
    }
    #endregion

    #region Main Methods
    private void TriggerEnter(Collider other)
    {
        if (!LayerUtilities.IsSameLayer(layerData.PlayerLayer, other.gameObject.layer))
            return;
        targetPlayer = other.transform;
        lookingPlayer = true;
    }
    private void TriggerExit(Collider other)
    {
        if (!LayerUtilities.IsSameLayer(layerData.PlayerLayer, other.gameObject.layer))
            return;
        lookingPlayer = false;
    }
    private void HandleLookPlayer()
    {
        Vector3 targetForward = Vector3.zero;
        if (lookingPlayer)
            targetForward = (targetPlayer.position - model.position).normalized;
        if (targetForward == model.forward)
            return;
        model.forward = Vector3.SmoothDamp(model.forward, targetForward, ref refSmoothVelocity, lookVelocity);
    }
    private void FirsTimeOpened()
    {
        DialogueLua.SetVariable(firstTimeLuaName, true);
        DialogueLua.SetVariable(OpenLuaName, false);
        StartConversation();
        State = false;
        shopUI.OnPanelClosed -= FirsTimeOpened;
    }
    private void CloseShopUI()
    {
        if (!State)
            conversation.Conversant.localEulerAngles = Vector3.zero;
        EnableActionButton();
        shopUI.OnPanelClosed -= CloseShopUI;
    }
    #endregion

    #region ISaveStateJSON
    private bool firstTime = true;
    public bool State
    {
        get => firstTime;
        set
        {
            firstTime = value;
            Save();
        }
    }
    public string KeyName => "FirstShopOpened";
    public string Path => $"Shop/{gameObject.name}";
    public void Save() => DataManager.SaveInterface(this);
    #endregion

}
