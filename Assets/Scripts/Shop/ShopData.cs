using NaughtyAttributes;
using UnityEngine;
[CreateAssetMenu(fileName = "ShopData", menuName = "Coralium/Shop/Shop Data")]
public class ShopData : ScriptableObject
{
    [SerializeField] [Expandable] public GameData GameData;
    [SerializeField] [Expandable] public CollectableData coin;
    [ShowItemData("GameData")] public int[] ItemsId;
}



