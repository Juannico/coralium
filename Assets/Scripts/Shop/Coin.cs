using NaughtyAttributes;
using UnityEngine;
[CreateAssetMenu(fileName = "Coin", menuName = "Coralium/Shop/Coin")]
public class Coin : CollectableData
{
    [BoxGroup("Coin Information")] [SerializeField] private string description;
}
