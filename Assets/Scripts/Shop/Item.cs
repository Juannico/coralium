using NaughtyAttributes;
using Newtonsoft.Json;
using System;
using UnityEngine;

[JsonObject(MemberSerialization.OptIn)]
public abstract class Item : ScriptableObject, IResetSOOnExitPlay, ISaveSOOnEnterPlay
{
    [Header("Item Information")]
    public TextTableFieldName Name;
    public TextTableFieldName Description;
    public int Price;
    [AllowNesting] [ShowAssetPreview] public Sprite Icon;
    [JsonProperty]
    [AllowNesting] [SerializeField] private bool purchased;
    public bool CanBuy;
    public virtual bool Purchased
    {
        get => purchased;
        set
        {
            purchased = value;
            OnPurchaseChange?.Invoke(value);
        }
    }
    public Action<bool> OnPurchaseChange;

    #region Reusable Item Methods
    /// <summary>
    /// Attempt to purchase the item.
    /// </summary>
    /// <param name="coin"> The collectable currency used for the purchase.</param>
    /// <returns>
    /// Returns true if the purchase is successful; otherwise, returns false.
    /// </returns>
    public virtual bool Buy(CollectableData coin)
    {
        if (coin.Amount < Price)
            return false;
        coin.Amount -= Price;
        Purchased = true;
        return true;
    }
    #endregion

    #region Reset OnSwitchPlaymode
    private bool savedStartPurchased;
    private bool savedStartCanBuy;
    public void SaveOnEnterPlay()
    {
        savedStartPurchased = purchased;
        savedStartCanBuy = CanBuy;
    }
    public virtual void ResetOnExitPlay()
    {
        purchased = savedStartPurchased;
        CanBuy = savedStartCanBuy;
        OnPurchaseChange = null;
    }
    #endregion

}
