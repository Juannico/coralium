using UnityEngine;

public class GameManager : MonoBehaviour, ISaveStateJSON
{
    [SerializeField] private GameData gameData;
    [field: SerializeField] public LevelManager LevelManager { get; private set; }
    [SerializeField] private AmuletInventory amuletInventory;
    public static GameManager Instance { get; private set; }

    public UIManager UIManager { get; private set; }
    public TutorialHandlerComponent TutorialHandler { get; private set; }

    private bool canUnlockbaBilities = true;
    private int restricAbilitiesAmount;
    public bool CanUnlockabBilities
    {
        get => canUnlockbaBilities;
        set
        {
            restricAbilitiesAmount += value ? 1 : -1;
            if (restricAbilitiesAmount < 0)
                restricAbilitiesAmount = 0;
            canUnlockbaBilities = restricAbilitiesAmount == 0;
        }
    }
    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
        Instance?.UIManager?.Initialize(false);
        if (Instance != null)
        {
            CoroutineHandler.ExecuteActionAfter(() => amuletInventory.Initialize(true), Constants.OneSecondDelay, Instance);
            Destroy(gameObject);
            return;
        }
        Instance = this;
        TutorialHandler = GetComponent<TutorialHandlerComponent>();
        CoroutineHandler.ExecuteActionAfter(() => amuletInventory.Initialize(), Constants.OneSecondDelay, this);
        UIManager = GetComponent<UIManager>();
        UIManager.Initialize(true);     
        restricAbilitiesAmount = 0;
        Cursor.lockState = CursorLockMode.Confined;
        Cursor.visible = false;
        LevelManager.LevelInitialized = true;
        PlayerInputManager.Instance.SetInputModePlayer();
        PlayerInputManager.Instance.EnableDebugInput();
    }
    void Start()
    {
        QualitySettings.vSyncCount = 0;
        SetFrameRate();    
        if (State)
            return;
        State = true;
    }

    public void SetGamePaused(bool paused)
    {
        if (paused == true)
        {
            Time.timeScale = 0;
            // Pausing the game leaves the input from UI enabled. So this pause game is for entering
            // the pause menu or similar should not be used if we want everything to be disablerd for some reason
            PlayerInputManager.Instance.SetInputModeUI();
        }
        else
        {
            // #TODO Check this interaction because we have some things that change the time scale 
            Time.timeScale = 1;
            PlayerInputManager.Instance.SetInputModePlayer();
        }
    }
    private void OnDestroy()
    {
        UIManager = null;
        restricAbilitiesAmount = 0;
    }
    private void SetFrameRate()
    {
#if UNITY_XBOXSERIES || UNITY_XBOXONE

            Application.targetFrameRate = 30;
            return;
#endif
        Application.targetFrameRate = 60;

    }

    #region ISaveStateJSON
    private bool gameInitialized;
    public bool State
    {
        get => gameInitialized;
        set
        {
            gameInitialized = value;
            Save();
        }
    }

    public string KeyName => "Initialized";

    public string Path => $"Game";
    public void Save() => DataManager.SaveInterface(this, false);
    #endregion
#if UNITY_EDITOR
    private void OnApplicationQuit()
    {
        DataManager.Reset(true);
    }
#endif

}
