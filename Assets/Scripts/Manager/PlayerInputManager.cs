
using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
/// <summary>
/// #TODO There are some missing properties to enable and disable input
/// </summary>
public class PlayerInputManager : MonoBehaviour
{
    public static PlayerInputManager Instance { get; private set; }

    public PlayerInputActions InputActions { get; private set; }
    public InputDeviceChangeHandler DeviceChange { get; private set; }

    [SerializeField] private PlayerAbilities playerAbilities;

    public PlayerInput PlayerInput { get; private set; }
    private GameManager gameManager;
    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
        if (Instance != null)
        {
            Destroy(gameObject);
            return;
        }
        Instance = this;

        PlayerInput = GetComponent<PlayerInput>();
        DeviceChange = GetComponent<InputDeviceChangeHandler>();
        InputActions = new PlayerInputActions();
    }
#if UNITY_EDITOR
    private void Update()
    {
        //print(EventSystem.current.ToString());
    }
#endif
    public void BindInputActionStarted(InputActionReference input, Action<InputAction.CallbackContext> callback) => InputActions.FindAction(input.ToInputAction().name).started += callback;
    public void UnbindInputActionStarted(InputActionReference input, Action<InputAction.CallbackContext> callback) => InputActions.FindAction(input.ToInputAction().name).started -= callback;
    public void BindInputActionPerformed(InputActionReference input, Action<InputAction.CallbackContext> callback) => InputActions.FindAction(input.ToInputAction().name).performed += callback;
    public void UnbindInputActionPerformed(InputActionReference input, Action<InputAction.CallbackContext> callback) => InputActions.FindAction(input.ToInputAction().name).performed -= callback;
    public void BindInputActionCanceled(InputActionReference input, Action<InputAction.CallbackContext> callback) => InputActions.FindAction(input.ToInputAction().name).canceled += callback;
    public void UnbindInputActionCanceled(InputActionReference input, Action<InputAction.CallbackContext> callback) => InputActions.FindAction(input.ToInputAction().name).canceled -= callback;

    public void EnablePause() => InputActions.PauseSystem.Enable();
    public void DisablePause() => InputActions.PauseSystem.Disable();
    public void EnableTabMenu() => InputActions.TabMenu.Enable();
    public void DisableTabMenu() => InputActions.TabMenu.Disable();
    public void EnableDebugInput() => InputActions.Debug.Enable();

    public void SetInputModeNone()
    {
        InputActions.Player.Disable();
        InputActions.UI.Disable();
        DisablePause();
    }

    public void SetInputModePlayer()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        InputActions.Player.Enable();
        InputActions.UI.Disable();
        EnablePause();
        DisableTabMenu();
    }

    public void SetInputModeUI(bool showCursor = true)
    {
        if (showCursor)
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
        InputActions.Player.Disable();
        InputActions.UI.Enable();
        DisablePause();
    }


    public void SetInputState(bool state)
    {
        SetAbilities(state);
    }
    public void SetAbilities(bool state)
    {
        if (playerAbilities.Abilities == null)
            return;
        if (gameManager == null)
            gameManager = GameManager.Instance;
        bool canUnlockabBilities = true;
        if (gameManager != null)
        {
            gameManager.CanUnlockabBilities = !state;
            canUnlockabBilities = gameManager.CanUnlockabBilities;
        }
        for (int i = 0; i < playerAbilities.Abilities.Length; i++)
        {
            if (!playerAbilities.Abilities[i].Data.HasInputAction)
                continue;
            if (state)
            {
                if (canUnlockabBilities)
                    playerAbilities.Abilities[i].Data.InputReference.InputAction.Enable();
            }
            else
                playerAbilities.Abilities[i].Data.InputReference.InputAction.Disable();

        }
    }
}
