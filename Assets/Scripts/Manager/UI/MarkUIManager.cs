using System.Collections.Generic;
using UnityEngine;

public class MarkUIManager
{
    public Transform Container;
    private List<MarkUI> markPool;
    private PoolHandler poolHandler;
    public MarkUIManager(GameObject gameObject)
    {
        poolHandler = gameObject.AddComponent<PoolHandler>();
        Container = new GameObject("MarkUIOnObjectPool").transform;
        Container.transform.parent = gameObject.transform;
        Container.transform.SetAsFirstSibling();
        markPool = new List<MarkUI>();
    }
    public MarkUI GetMarkUI(MarkUI WorldUiOnObjectPrefab)
    {
        for (int i = 0; i < markPool.Count; i++)
        {
            if (markPool[i].gameObject.activeInHierarchy)
                continue;
            if (markPool[i].markType != WorldUiOnObjectPrefab.markType)
                continue;
            markPool[i].gameObject.SetActive(true);
            return markPool[i];
        }
        MarkUI instantiate = poolHandler.InstantiateByType(WorldUiOnObjectPrefab);
        instantiate.transform.parent = Container;
        instantiate.name = $"{markPool.Count + 1}_{WorldUiOnObjectPrefab.name}_{WorldUiOnObjectPrefab.markType}";
        return instantiate;
    }


}
