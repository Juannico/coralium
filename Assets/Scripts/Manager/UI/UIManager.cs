using NaughtyAttributes;
using TamarilloTools;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.UI;

public class UIManager : MonoBehaviour
{
    [SerializeField] private InputActionReference pauseInputAction;
    [SerializeField] private LevelManager levelManager;
    [Header("UI Prefabs")]
    [SerializeField] private GameObject mainCanvasPrefab;
    [SerializeField] private GameObject challengeScreenPrefab;
    [SerializeField] private GameObject pauseMenuPrefab;
    [SerializeField] private GameObject buttonPromptPrefab;
    [SerializeField] private PopUpPanel popUpPanelPrefab;
    [SerializeField] private PopUpBuyPanel popUpBuyPanelPrefab;
    [SerializeField] private DialogueTutorialPanel itemCollectedPopUpPrefab;
    [SerializeField] private ShowIconItemCollected showIconItemCollectedPopUpPrefab;
    [BoxGroup("Save Icon Setting")]
    [SerializeField] private TextTableFieldName SaveText;
    [BoxGroup("Save Icon Setting")]
    [SerializeField] private Sprite SaveIcon;
    [Header("Tutorials")]
    [SerializeField] private TutorialData[] initialTutorialData;
    private int initialTutorialIndex = 0;

    public static UIManager Instance { get; private set; }

    private GameObject mainCanvas;
    public GameObject MainCanvas
    {
        get
        {
            if (mainCanvas != null)
                return mainCanvas;
            mainCanvas = GameObject.FindGameObjectWithTag(Tags.MainCanvas);
            if (mainCanvas == null)
                mainCanvas = Instantiate(mainCanvasPrefab);
            return mainCanvas;
        }
    }
    private PopUpPanel popUpPanel;
    public PopUpPanel PopUpPanel
    {
        get
        {
            if (popUpPanel == null)
                popUpPanel = Instantiate(popUpPanelPrefab, MainCanvas.transform);
            return popUpPanel;
        }
        private set => popUpPanel = value;
    }
    private PopUpBuyPanel popUpBuyPanel;
    public PopUpBuyPanel PopUpBuyPanel
    {
        get
        {
            if (popUpBuyPanel == null)
                popUpBuyPanel = Instantiate(popUpBuyPanelPrefab, MainCanvas.transform);
            return popUpBuyPanel;
        }
        private set => popUpBuyPanel = value;
    }
    private DialogueTutorialPanel itemCollectedPopUp;
    public DialogueTutorialPanel ItemCollectedTutoPopUp
    {
        get
        {
            if (itemCollectedPopUp == null)
                itemCollectedPopUp = Instantiate(itemCollectedPopUpPrefab, MainCanvas.transform);
            return itemCollectedPopUp;
        }
        private set => itemCollectedPopUp = value;
    }
    private ShowIconItemCollected iconItemCollectedPopUp;
    public ShowIconItemCollected IconItemCollectedTutoPopUp
    {
        get
        {
            if (iconItemCollectedPopUp == null)
                iconItemCollectedPopUp = Instantiate(showIconItemCollectedPopUpPrefab, MainCanvas.transform);
            return iconItemCollectedPopUp;
        }
        private set => iconItemCollectedPopUp = value;
    }
    private ChallengeUI infoZonePromp;
    public ChallengeUI InfoZonePromp
    {
        get
        {
            if (infoZonePromp == null)
                infoZonePromp = MainCanvas.GetComponentInChildren<ChallengeUI>();
            return infoZonePromp;
        }

        private set => infoZonePromp = value;
    }

    private DownScreenButtonText downScreenButttonText;


    private GameObject abilitiesContainer;
    public WorldUIManager WorldUIManager { get; private set; }
    public MarkUIManager MarkUIManager { get; private set; }
    /// <summary>
    /// Tutorial
    /// </summary>

    [HideInInspector] public bool GameInPauseMenu;

    public bool UIOpened { get; private set; }
    private int amountUIOppened = 0;

    private EventSystem eventSystem;
    private InputSystemUIInputModule inputSystemUIInput;
    public InputSystemUIInputModule InputSystemUIInput
    {
        get
        {
            if (inputSystemUIInput == null)
                inputSystemUIInput = FindObjectOfType<InputSystemUIInputModule>();
            return inputSystemUIInput;
        }

        private set => inputSystemUIInput = value;
    }
    // CACHE INPUT //
    private PlayerInputManager playerInputManager;
    // CACHE INPUT //
    [HideInInspector] public Vector2 CanvasSize;

    private bool canPause = true;
    private int restricPausePanelsAmount;

    public bool CanPause
    {
        get => canPause;
        set
        {
            restricPausePanelsAmount -= value ? 1 : -1;
            if (restricPausePanelsAmount < 0)
                restricPausePanelsAmount = 0;
            canPause = restricPausePanelsAmount == 0;
        }
    }
    private GameObject saveIcon;
    public void Initialize(bool firstTime)
    {
        if (Instance == null) Instance = this;
        playerInputManager = PlayerInputManager.Instance;
        restricPausePanelsAmount = 0; // ??
        // #TODO use the saved data to know when is the first time
        if (firstTime)
            GameManager.Instance.TutorialHandler.StartTutorial(initialTutorialData[initialTutorialIndex], TutorialHandlerComponent.TutorialType.InputControl);
        WorldUIManager = new WorldUIManager(gameObject);
        MarkUIManager = new MarkUIManager(MainCanvas);
        playerInputManager.BindInputActionPerformed(pauseInputAction, OpenPauseMenu);
        playerInputManager.EnablePause();
        //if (firstTime && !levelManager.LevelInitialized)
        //{
        //    CoroutineHandler.ExecuteActionAfter(() =>
        //    {
        //        ShowTutorialAbility showTutorialAbility = FindObjectOfType<ShowTutorialAbility>();
        //        showTutorialAbility?.ShowTutorial();
        //    }, Constants.halfSecondDelay, this);
        //}
        ////abilitiesContainer = MainCanvas.GetComponentInChildren<AbilityUI>().transform.parent.gameObject;
        //Rect canvasRect = MainCanvas.GetComponent<RectTransform>().rect;
        //CanvasSize = new Vector2(canvasRect.width, canvasRect.height);
        //if (eventSystem == null)
        //{
        //    eventSystem = MainCanvas.GetComponentInChildren<EventSystem>();
        //    eventSystem.transform.parent = transform;
        //}
        //EventSystem[] eventSystems = FindObjectsOfType<EventSystem>(true);
        //if (eventSystems.Length > 1)
        //{
        //    for (int i = 0; i < eventSystems.Length; i++)
        //    {
        //        if (eventSystem != eventSystems[i])
        //            Destroy(eventSystems[i].gameObject);

        //    }
        //}
        //if (pauseMenu == null)
        //    pauseMenu = MainCanvas.GetComponentInChildren<PauseMenu>();
        ////PopUpPanel.gameObject.SetActive(false);
        //PopUpBuyPanel.gameObject.SetActive(false);
        //SetUIOrder();
        //saveIcon = Instantiate(saveIconPrefab, MainCanvas.transform);
        //saveIcon.SetActive(false);
        //saveIcon.transform.SetAsFirstSibling();
    }

    private void OnDestroy()
    {
        bool shouldActiveInput = amountUIOppened > 0;
        amountUIOppened = 0;
        if (shouldActiveInput)
            playerInputManager?.SetInputState(true);
    }

    public void StartNextTutorial(GameObject objectToRemoveFromUI)
    {
        UIStackSystem.Instance.CloseOverlayScreen(objectToRemoveFromUI);
        initialTutorialIndex++;
        if (initialTutorialIndex >= initialTutorialData.Length) return;

        GameManager.Instance.TutorialHandler.StartTutorial(initialTutorialData[initialTutorialIndex], TutorialHandlerComponent.TutorialType.InputControl);
    }

    public ChallengeUI OpenChallengeScreen()
    {
        GameObject challengeScreen = UIStackSystem.Instance.OpenOverlay(MainCanvas, challengeScreenPrefab);
        return challengeScreen.GetComponent<ChallengeUI>();
    }

    public void OpenPauseMenu(InputAction.CallbackContext context)
    {
        UIStackSystem.Instance.OpenContentScreen(MainCanvas, pauseMenuPrefab);
        PlayerInputManager.Instance.DisablePause();
        GameManager.Instance.SetGamePaused(true);
    }

    /// <summary>
    /// Helper method to show button prompt. Doesn't use the Stack system and can be called from anywhere
    /// </summary>
    /// <param name="buttonData"></param>
    public void ShowButtonPrompt(ButtonPromptData buttonData)
    {
        // #TODO This is bad!! Create a component to find the container for the button prompt
        GameObject buttonContainer = GameObject.Find("Button_Prompt_Container");
        GameObject buttonPrompt = null;
        if (buttonContainer.transform.childCount <= 0)
        {
            buttonPrompt = Instantiate(buttonPromptPrefab, buttonContainer.transform);
        }
        else
        {
            buttonPrompt = buttonContainer.transform.GetChild(0).gameObject;
            buttonPrompt.SetActive(true);
        }
        ButtonPrompt buttonPromptComp = buttonPrompt.GetComponent<ButtonPrompt>();

        buttonPromptComp.ResetButton();
        buttonPromptComp.SetButtonPromptData(buttonData);
        buttonPromptComp.SetPlayBlinkingAnimation(false);
        buttonPromptComp.Init();
    }

    public void HideButtonPrompt()
    {
        // #TODO This is bad!! Create a component to find the container for the button prompt
        GameObject buttonContainer = GameObject.Find("Button_Prompt_Container");

        if (buttonContainer.transform.childCount > 0)
        {
            buttonContainer.transform.GetChild(0).gameObject.SetActive(false);
        }
    }

    private void SetUIOrder()
    {
        ItemCollectedTutoPopUp.transform.SetAsLastSibling();
        PopUpPanel.transform.SetAsLastSibling();
        PopUpBuyPanel.transform.SetAsLastSibling();
    }

    public T SetUIByType<T>(T uiPrefab) where T : Component
    {
        T ui = MainCanvas.GetComponentInChildren<T>(true);
        if (ui == null)
        {
            ui = Instantiate(uiPrefab, MainCanvas.transform);
            SetUIOrder();
        }
        return ui;
    }
    public void HandleUIOpenned(bool add)
    {
        amountUIOppened += add ? 1 : -1;
        if (amountUIOppened < 0)
            amountUIOppened = 0;
        bool lastUIOpenened = UIOpened;
        UIOpened = amountUIOppened > 0;
        if (lastUIOpenened != UIOpened)
            playerInputManager?.SetInputState(!UIOpened);
    }
    public void SetUiVisibility(bool state)
    {
        MarkUIManager.Container.gameObject.SetActive(state);
        abilitiesContainer?.SetActive(state);
        // InfoZonePromp it is now working at expected
        //InfoZonePromp.gameObject.SetActive(state);
    }
    public void ShowSaveIcon() => IconItemCollectedTutoPopUp.Show(SaveText, SaveIcon);

}
