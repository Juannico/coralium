using System;
using UnityEngine;
using UnityEngine.UI;

public class WorldUIManager
{ 
    private PoolHandler poolHandler;
    private Transform container;
    public WorldUIManager(GameObject gameObject) { 
        poolHandler = gameObject.AddComponent<PoolHandler>();
        container = new GameObject("WorldUIOnObjectPool").transform;
        container.transform.parent = gameObject.transform;
        poolHandler.CreatePool(null,0, container, true);
    }

    public BaseWorldSpaceUI GetWordUI(BaseWorldSpaceUI WorldUiOnObjectPrefab, Type UIType)
    {
        BaseWorldSpaceUI UiObject = poolHandler.GetObjectByType(UIType, WorldUiOnObjectPrefab);
        if (UiObject.transform.parent == container)
            UiObject.transform.SetParent(SetCanvasParent(UiObject.name));
        UiObject.transform.parent.localScale = Vector3.one * 0.01f;
        UiObject.transform.localPosition = Vector3.zero;
        UiObject.Initialize(UiObject.GetComponent<RectTransform>().transform.parent);
        return UiObject;
    }

    private Transform SetCanvasParent(string baseName)
    {
        Canvas canvas = new GameObject($"WorldSpaceCanvas_{baseName}").AddComponent<Canvas>();
        canvas.renderMode = RenderMode.WorldSpace;
        canvas.transform.localPosition = Vector3.zero;
        canvas.transform.localRotation = Quaternion.identity;
        canvas.transform.localScale = Vector3.one;
        canvas.gameObject.AddComponent<CanvasScaler>().dynamicPixelsPerUnit = 10f;
        canvas.sortingOrder = 1;
        return canvas.transform;
    }

    public void EnactiveInstaceUI(GameObject objectToEnactive)
    {
        if (container.transform == null)
            return;
        if (container.transform.parent == null)
            return;
        objectToEnactive.transform.parent.localScale = Vector3.one * 0.01f;
        objectToEnactive.transform.parent.SetParent(container);
        objectToEnactive.SetActive(false);
    }

}
