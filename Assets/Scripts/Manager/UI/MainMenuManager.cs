using NaughtyAttributes;
using TamarilloTools;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuManager : MonoBehaviour
{
    [SerializeField] private LevelManager levelManager;
    [SerializeField] private GameData gameData;
    [SerializeField] private GameObject mainMenuScreen;
    [Scene][SerializeField] private string musicScene;
    protected void Awake()
    {
        DataManager.Initialize(gameData);
    }

    private void Start()
    {
        SceneManager.LoadSceneAsync(musicScene, LoadSceneMode.Additive);
        UIStackSystem.Instance.OpenContentScreen(gameObject, mainMenuScreen);
        gameObject.AddComponent<FadeUI>();
        PlayerInputManager.Instance.SetInputModeUI(true);
    }
}
