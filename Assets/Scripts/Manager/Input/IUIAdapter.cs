public interface IUIAdapter
{
    void UpdateOnInputDeviceChanged(string newDevice);
}
