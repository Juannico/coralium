using System;
using UnityEngine;

public class UIInputAdapter : MonoBehaviour
{
    private IUIAdapter m_Adapter;

    private void Start()
    {
        GetAdapter();
    }

    private void GetAdapter()
    {
        m_Adapter = GetComponent<IUIAdapter>();
    }

    private void OnEnable()
    {
        if (m_Adapter == null) GetAdapter();
        AddOnInputDeviceChangeCallback();
    }

    private void OnDisable()
    {
        if (m_Adapter == null) GetAdapter();
        RemoveOnInputDeviceChangeCallback();
    }
    public void AddOnInputDeviceChangeCallback()
    {
        PlayerInputManager.Instance.DeviceChange.BindToInputDeviceChanged(m_Adapter.UpdateOnInputDeviceChanged);
    }

    public void RemoveOnInputDeviceChangeCallback()
    {
        PlayerInputManager.Instance.DeviceChange.UnbindFromInputDeviceChanged(m_Adapter.UpdateOnInputDeviceChanged);
    }

}
