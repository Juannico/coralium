using System;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Users;

public class InputDeviceChangeHandler : MonoBehaviour
{
    public Action<string> OnControlSchemaChangedEvent;

    private PlayerInput playerInput;
    public string CurrentControlSchema { get; private set; }

    //private bool initialized;
    private void Awake()
    {
        playerInput = PlayerInputManager.Instance.PlayerInput;
        CallOnDeviceChanged();
    }

    private void OnEnable()
    {
        InputUser.onChange += OnInputDeviceChange;
        CallOnDeviceChanged();
    }
    private void OnDisable()
    {
        InputUser.onChange -= OnInputDeviceChange;
    }
    private void OnDestroy()
    {
        InputUser.onChange -= OnInputDeviceChange;
        OnControlSchemaChangedEvent = null;
    }
    void OnInputDeviceChange(InputUser user, InputUserChange change, InputDevice device)
    {
        if (change == InputUserChange.ControlSchemeChanged && user.controlScheme != null)
        {
            CurrentControlSchema = user.controlScheme.Value.name.ToLower();
            CallOnDeviceChanged();
        }
    }
    public void CallOnDeviceChanged()
    {
        if (playerInput == null)
        {
            Debug.LogWarning("No CustomPlayerINput found!");
            return;
        }    

        CurrentControlSchema = PlayerInputManager.Instance.PlayerInput.currentControlScheme?.ToLower();
        OnControlSchemaChangedEvent?.Invoke(CurrentControlSchema);
    }

    public void BindToInputDeviceChanged(Action<string> callback)
    {
        OnControlSchemaChangedEvent += callback;
    }

    public void UnbindFromInputDeviceChanged(Action<string> callback)
    {
        OnControlSchemaChangedEvent -= callback;
    }
}

