
using UnityEngine;

public interface ISetSecondAbilityBehaviour
{
    public void SetSecondAbility(BaseAbilityCast secondAbility);
    public void CastSecondAbility(Vector3 castPosition);
}
