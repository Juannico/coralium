/// <summary>
/// Interface for saving the state of MonoBehaviour objects that need to persist their state.
/// </summary>
public interface ISaveStateJSON
{
    /// <summary>
    /// Gets or sets the current state of the object.
    /// </summary>
    public bool State { get; set; }
    /// <summary>
    /// Gets the unique key name associated with the object's state.
    /// </summary>
    public string KeyName { get; }
    /// <summary>
    /// Gets the path where the object's state will be saved.
    /// </summary>
    public string Path { get; }
    /// <summary>
    /// Saves the current state of the object to a persistent storage.
    /// </summary>
    public void Save();

}
