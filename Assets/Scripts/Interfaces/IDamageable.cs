using UnityEngine;

public interface IDamageable<D,T>
{
    void TakeDamage(D damage,T type, Vector3 damageDirection);
}
