public interface IEffect 
{
    void StartEffect(float duration = 0);    
}
