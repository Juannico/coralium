/// <summary>
/// Interface for saving the state of MonoBehaviour objects that need to persist their state.
/// </summary>
/// <remarks>
/// Use the [JsonObject(MemberSerialization.OptIn)] attribute in classes that inherit from this interface
/// to selectively save variables. Employ [JsonProperty] to annotate the variables you want to save in JSON.
/// </remarks>

public interface ISaveSOJSONPath
{
    /// <summary>
    /// Gets the path where the object's state will be saved.
    /// </summary>
    public string Path { get; }
}
