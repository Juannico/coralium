
public interface INavegable 
{
    public bool IsSelected { get; set; }
    public bool Interactable { get; set; }
    public void Select();
    public void Deselected();
}
