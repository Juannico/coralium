public interface IInteractable
{
    void MakeInteraction();
}
