
public interface IDashable
{
    /// <summary>
    /// Performs an interaction when colliding with it.
    /// </summary>
    /// <param name="shouldBounce">
    /// Outputs a value indicating whether the object should bounce after the interaction.
    /// </param>
    /// <returns>
    /// Returns true if the interaction was successful; otherwise, returns false.
    /// </returns>
    bool MakeInteraction(out bool shouldBounce);
}
