using UnityEngine;

public class Magnet : MonoBehaviour
{
    [SerializeField] private SphereCollider selfCollider;
    public Transform PlayerTransform { get; private set; }
    private float startColliderRadius;
    private void Awake()
    {
        startColliderRadius = selfCollider.radius;
    }
    /*
    private void OnTriggerEnter(Collider other)
    {
        if (!other.TryGetComponent(out BaseObjectCollectable baseObjectCollectable))
            return;
        if (baseObjectCollectable.IsCollected)
            return;
        baseObjectCollectable.StartToCollect(playerTransform, () => { });
    }*/
    public void Initialize(Transform playerTransform)
    {
        this.PlayerTransform = playerTransform;
    }
    private void Update()
    {
        transform.position = PlayerTransform.position;
    }
    public void SetColliderRaius(float radius)
    {
        selfCollider.radius = radius;
    }
    public void ResetColliderRaius()
    {
        selfCollider.radius = startColliderRadius;
    }

}
