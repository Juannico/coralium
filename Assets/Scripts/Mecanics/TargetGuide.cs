using NaughtyAttributes;
using UnityEngine;

public class TargetGuide : MonoBehaviour
{
    [SerializeField] private Character character;
    [SerializeField] [Expandable] private FindEnemyData findEnemyData;
    [SerializeField] private Mesh unlockOnMesh;
    [SerializeField] private FindEnemyData lockOnMesh;
    [SerializeField] private GameObject guideEnemyTargetPrefab;
    [SerializeField] private float guideEnemyTargetOfssetHeight = 0.75f;
    [SerializeField] private float onLockOnScale = 1.5f;

    private GameObject guideEnemyTarget;
    private GameObject currentTargeObject;
    RaycastHit[] raycastHit = new RaycastHit[10];
    private Camera mainCamera;
    private Animator animator;
    private bool lockOnActivated;

    private float offsetDistance;
    private void Awake()
    {
        if (character == null)
            character = gameObject.GetComponentInParent<Character>();
        guideEnemyTarget = Instantiate(guideEnemyTargetPrefab, Vector3.zero, guideEnemyTargetPrefab.transform.rotation, transform);
        guideEnemyTarget.SetActive(false);
        guideEnemyTarget.transform.parent = null;
        animator = guideEnemyTarget.GetComponent<Animator>();
        currentTargeObject = null;
        mainCamera = Camera.main;
        findEnemyData.OnSwitchLockOn += SwitchLockOn;
        offsetDistance = Vector3.Distance(mainCamera.transform.position, character.Model.transform.position);
    }
    private void Update()
    {
        ShowTargetGuide();
    }
    private void OnDestroy()
    {
        findEnemyData.OnSwitchLockOn -= SwitchLockOn;
    }



    private void ShowTargetGuide()
    {
        if (!lockOnActivated)
        {
            int amountOFTargets = TargetUtilities.GetTargetsAmount(character.Model.transform, findEnemyData.MaxDistanceToFindObjects, character.BaseCharacter.LayerData.EnemyLayer, ref raycastHit);

            if (amountOFTargets == 0)
            {
                SetGuideState(false);
                return;
            }

            Transform enemyTransform = TargetUtilities.GetTargetpositionByRaycastHits(character.BaseTransform.transform, raycastHit, character.BaseTransform, findEnemyData.FOVAngle);
            if (enemyTransform == null)
            {
                SetGuideState(false);
                return;
            }
            if (currentTargeObject != enemyTransform.gameObject)
            {
                findEnemyData.TargetEnemy = enemyTransform.GetComponentInParent<EnemyCharacter>();
                currentTargeObject = enemyTransform.gameObject;
            }
            if (findEnemyData.TargetEnemy == null || findEnemyData.TargetEnemy.BaseCharacter.Data.IsDead)
            {
                SetGuideState(false);
                return;
            }
        }
        if (findEnemyData.TargetEnemy == null)
            return;
        SetGuideState(true);
        guideEnemyTarget.transform.position = findEnemyData.TargetEnemy.transform.position + findEnemyData.TargetEnemy.TargetGuide;
    }
    private void SwitchLockOn(bool state)
    {
        lockOnActivated = state;
        animator.SetTrigger(state ? "Init" : "Hide");
    }
    private void SetGuideState(bool state)
    {
        guideEnemyTarget.SetActive(state);
        if (state)
            return;
        findEnemyData.TargetEnemy = null;
        currentTargeObject = null;
    }
}
