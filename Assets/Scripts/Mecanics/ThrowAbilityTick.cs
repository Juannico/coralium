using NaughtyAttributes;
using UnityEngine;

public class ThrowAbilityTick : MonoBehaviour
{
    [SerializeField] private bool useDirection;
    [ShowIf("useDirection")] [SerializeField] private Vector2 direction;
#if UNITY_EDITOR
    [ShowIf("useDirection")] [SerializeField] private bool showDirection;
#endif
    [SerializeField] private float delayActivation;
    [SerializeField] private float repeatTime;
    [SerializeField] private BaseAbility ability;
    [Expandable][SerializeField] private BaseAbilityDescriptor abilityDescriptor;
    private void Awake()
    {
        if (delayActivation > 0)
        {
            CoroutineHandler.ExecuteActionAfter(Throw, delayActivation, this);
            return;
        }
        Throw();
    }
    private void Throw()
    {
        AbilityParams abilityParams = new AbilityParams();
        if (useDirection)
            abilityParams.vector2Value = direction;
        if (ability.PrepareAbility(abilityDescriptor, abilityParams))
            ability.ActivateAbility();
        CoroutineHandler.ExecuteActionAfter(Throw, repeatTime, this);
    }
#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        if (showDirection && useDirection)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawRay(transform.position, direction * 15);
        }
    }
#endif
}
