using UnityEngine;

public class FollowBehaviour : MonoBehaviour
{
    [SerializeField] private float distanceToFollow;
    [SerializeField] private float followSpeed;
    [SerializeField] private float followSpeedMultiplierByDistance;
    [SerializeField] private Vector2 offsetDirection;
    private Transform followTransform;
    private bool shoudlFollow;
    private Vector3 currentVelocity;
    private float currentSpeedVelocity;
    private float currentSpeed;
    private float smoothDamp;
    private Vector3 targetPosition;
    public void Follow(float deltaTime)
    {
        if (!shoudlFollow)
            return;
        targetPosition = followTransform.position + transform.right * offsetDirection.x + transform.up * offsetDirection.y ;
        Vector3 direction = GetFollowDirection();
        direction = Vector3.SmoothDamp(transform.forward, direction, ref currentVelocity, 0.2f, 1000, deltaTime);
        direction *= GetFollowSpeed(deltaTime);
        transform.position += direction * deltaTime;
        if (direction != Vector3.zero)
            transform.forward = direction.normalized;
    }
    private Vector3 GetFollowDirection()
    {
        Vector3 direction = targetPosition - transform.position;
        direction += MathUtilities.RandomVector(-Vector3.one, Vector3.one);
        return direction.normalized;
    }
    private float GetFollowSpeed(float deltaTime)
    {
        float distance = Vector3.Distance(targetPosition, transform.position);
        distance -= distanceToFollow;
        float targespeed = followSpeed + distance * followSpeedMultiplierByDistance;
        smoothDamp = 0.07f;
        if (distance < 0)
        {
            targespeed = 0;
            smoothDamp = 0.1f;
        }
        targespeed = Mathf.SmoothDamp(currentSpeed, targespeed, ref currentSpeedVelocity, smoothDamp);
        return targespeed;
    }
    public void StartFollowing(Transform followTransform)
    {
        this.followTransform = followTransform;
        shoudlFollow = true;
        currentSpeed = 0;
    }
    public void StopFollowing()
    {
        currentSpeed = 0;
        followTransform = null;
        shoudlFollow = false;
    }
}
