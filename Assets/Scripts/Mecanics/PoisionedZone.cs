using UnityEngine;

public class PoisionedZone : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        other.GetComponentInParent<BaseStatusEffect>()?.ApplyEffect(DamageTypes.Poison);
    }
    private void OnTriggerExit(Collider other)
    {
        other.GetComponentInParent<BaseStatusEffect>()?.OffEffect();
    }
}
