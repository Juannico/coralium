using System;
using UnityEngine;


public class AddHealthOnTriggerEnter : BaseSaveJSONObjectCollectable
{
    protected override void Start()
    {
       // if (State)
         //   GameObject.FindGameObjectWithTag(Tags.Player).GetComponentInParent<Character>().BaseCharacter.Data.Health += amountToAdd;
        base.Start();
    }
    public override void StartToCollect(Transform otherTransform, Action action)
    {
        action = () => otherTransform.GetComponentInParent<Character>().BaseCharacter.Data.Health += amountToAdd;
        base.StartToCollect(otherTransform, action);
    }
}

