using UnityEngine;

public class AutoAim
{
    private FindEnemyData findEnemyData;
    private Character playerCharacter;

    public AutoAim(FindEnemyData findEnemyData, Character playerCharacter)
    {
        this.findEnemyData = findEnemyData;
        this.playerCharacter = playerCharacter;
    }

    public Vector3 GetDirection()
    {
        if (findEnemyData.TargetEnemy != null)
            return (findEnemyData.TargetEnemy.transform.position - playerCharacter.transform.position).normalized;
        return playerCharacter.BaseTransform.forward;
    }
}
