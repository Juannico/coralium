using NaughtyAttributes;
using TamarilloTools;
using UnityEngine;

public class Travel : BaseAddActionToButton
{
    [Header("TravelSetting")]
    [SerializeField] private GameObject travelUIPrefab;
    [SerializeField] [Expandable] private LevelManager levelManager;
    [SerializeField] private Transform targetTransform;
    [SerializeField] private BaseVisualEffect showVFX;
    [SerializeField] private ShopData shopData;
    [Header("Lock Setting")]
    //[SerializeField] private ButtonPromtInfo gamepadButtonInfoUnlock;
    //[SerializeField] private ButtonPromtInfo kbButtonInfoUnlock;
    [SerializeField] private PopPanelData buyTravel;
    [SerializeField] private PopPanelData notEnoughMoney;
    private PopUpBuyPanel popUpBuyPanel;
    private TravelData travelData;
    #region Unity Methods
    protected override void Awake()
    {
        base.Awake();
        travelData = levelManager.CurrentLevelData.Travel;
        if (!levelManager.CurrentLevelData.CanTravel)
        {
            gameObject.AddComponent<UnHideGameObject>();
            gameObject.SetActive(false);
            return;
        }
        if (!travelData.Locked)
        {
            showVFX.StartEffect();
            return;
        }
        //showVFX.ResetEffect();
        //popUpBuyPanel = GameManager.Instance.UIManager.PopUpBuyPanel;
    }
    private void Start()
    {
        if (levelManager.IsTraveling)
        {
            SetPlayerPosition setPlayerPosition = new SetPlayerPosition();
            setPlayerPosition.SetPosition(targetTransform.position, targetTransform.forward, levelManager);
        }
        levelManager.IsTraveling = false;
    }
    #endregion

    //protected override void SetInputActionText()
    //{
    //    if (travelData.Locked)
    //        actionButtonPrompt.SetButtonText(gamepadButtonInfoUnlock, kbButtonInfoUnlock);
    //    else
    //        base.SetInputActionText();
    //}

    #region Override Action Button Methods
    protected override void DoAction()
    {
        base.DoAction();
        if (travelData.Locked)
        {
            PopPanelData popPanelData = buyTravel;
            popPanelData.OnSubmit = () =>
            {
                shopData.coin.Amount -= travelData.Value;
                travelData.Locked = false;
                showVFX.StartEffect();
                //SetInputActionText();
            };
            if (shopData.coin.Amount < travelData.Value)
            {
                popPanelData = notEnoughMoney;
                popPanelData.OnSubmit = null;
            }
            //popUpBuyPanel.OpenPanel(popPanelData, travelData.Value);
            DisableActionButton();
            //popUpBuyPanel.OnPanelClosed += CloseTraveUI;
            return;
        }
        UIStackSystem.Instance.OpenContentScreen(UIManager.Instance.MainCanvas, travelUIPrefab);
    }
    #endregion

    #region Main Methods
    private void CloseTraveUI()
    {
        CoroutineHandler.ExecuteActionAfter(EnableActionButton,Constants.halfSecondDelay,this);
        popUpBuyPanel.OnPanelClosed -= CloseTraveUI;
    }
    #endregion

}
