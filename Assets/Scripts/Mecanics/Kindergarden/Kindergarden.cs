using PixelCrushers.DialogueSystem;
using UnityEngine;

public class Kindergarden : ConversationOnActionButton, ISaveStateJSON
{
    [Header("Kindergarden setting")]
    [SerializeField] private Animator diveAnimator;
    [SerializeField] private CollectableData rainbowPolypData;
    [Header("Conversation")]
    [VariablePopup(true)] [SerializeField] private string firstTimeLuaName;
    [VariablePopup(true)] [SerializeField] private string kindergardenLuaName;

    #region Unity Methods
    protected override void Awake()
    {
        base.Awake();
        DataManager.UpdateStateInterfaceValue(this);
        diveAnimator.SetBool("Swim", true);
    }
    #endregion

    #region Override Action Button Methods
    protected override void DoAction()
    {
        DialogueLua.SetVariable(firstTimeLuaName, State);
        DialogueLua.SetVariable(kindergardenLuaName, rainbowPolypData.LastCheckAmount < rainbowPolypData.Amount);
        rainbowPolypData.LastCheckAmount = rainbowPolypData.Amount;
        State = false;
        diveAnimator.SetBool("Swim", false);
        base.DoAction();
    }
    #endregion

    #region Override Conversation Methods
    protected override void ActionAfterConversation(Transform transform = null)
    {
        diveAnimator.SetBool("Swim", true);
    }
    #endregion

    #region ISaveStateJSON
    private bool firstTime = true;
    public bool State
    {
        get => firstTime;
        set
        {
            firstTime = value;
            Save();
        }
    }
    public string KeyName => "FirstKindergardenInteracted";
    public string Path => $"KinderGarden/{gameObject.name}";
    public void Save() => DataManager.SaveInterface(this);
    #endregion

}
