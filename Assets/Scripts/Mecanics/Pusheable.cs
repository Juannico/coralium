using System.Collections;
using UnityEngine;

public class Pusheable : MonoBehaviour
{
    private RouteFollower routFollower;
    private bool pushing;
    private void Awake()
    {
        routFollower = GetComponent<RouteFollower>();
    }
    public void Push(Vector3 direction)
    {
        if (pushing)
            return;
        pushing = true;
        StartCoroutine(Pushing(direction));
    }
    IEnumerator Pushing(Vector3 direction)
    {
        Vector3 startPosition = transform.position;
        float timer = 0;
        if (routFollower != null)
            routFollower.ShouldMove = false;
        while (timer < 1.5f)
        {
            timer += Time.deltaTime;
            transform.position += direction * Time.deltaTime * 2.2f / (timer * 0.5f + 0.1f);
            yield return null;
        }
        if (routFollower == null)
        {
            pushing = false;
            yield break;
        }
        StartCoroutine(BackRout(startPosition));
    }
    private IEnumerator BackRout(Vector3 targetPosition)
    {
        Vector3 startPosition = transform.position;
        float interpolateAmount = 0;
        while (interpolateAmount < 1)
        {
            interpolateAmount = Mathf.Clamp01(interpolateAmount + Time.deltaTime * 0.55f);
            transform.position = Vector3.Slerp(startPosition, targetPosition, Mathf.Pow(interpolateAmount, 2));
            yield return null;
        }
        pushing = false;
        if (routFollower != null)
            routFollower.ShouldMove = true;
    }
}
