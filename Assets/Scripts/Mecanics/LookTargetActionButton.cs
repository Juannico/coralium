using UnityEngine;

public class LookTargetActionButton : BaseAddActionToButton
{
    [Header("Init Legend Settings")]
    [SerializeField] private LookTargetCamera lookCameraTarget;
    private bool isWatchingLegend;

    #region Override Action Button Methods
    protected override void DoAction()
    {
        if (isWatchingLegend)
            return;
        isWatchingLegend = true;
        PlayerInputManager.Instance.BindInputActionCanceled(inputAction, StopWatchingLegend);
        //actionButtonPrompt.SetState(false);
        lookCameraTarget.StartLookTarget();
    }
    #endregion

    #region Main Methods
    protected void StopWatchingLegend(UnityEngine.InputSystem.InputAction.CallbackContext obj)
    {
        isWatchingLegend = false;
        //actionButtonPrompt.SetState(true);
        PlayerInputManager.Instance.UnbindInputActionCanceled(inputAction, StopWatchingLegend);
    }
    #endregion
}
