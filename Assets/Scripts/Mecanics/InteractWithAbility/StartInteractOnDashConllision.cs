using UnityEngine;

public class StartInteractOnDashConllision : BaseInteractWithAbility, IDashable
{
    [SerializeField] private bool shouldBounce;
    public bool MakeInteraction(out bool shouldBounce)
    {
        shouldBounce = this.shouldBounce;
        return IsStartingInteract();
    }

}
