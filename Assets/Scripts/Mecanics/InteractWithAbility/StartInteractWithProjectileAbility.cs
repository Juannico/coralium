using System;
using UnityEngine;

public class StartInteractWithProjectileAbility : BaseInteractWithAbility
{
    [SerializeField] private LayerData layer;
    [SerializeField] private AbilitiesBehaviour abilityBehaviour;

    private Type abilityType;
    private void Awake()
    {
        abilityType = Type.GetType(abilityBehaviour.ToString());
        abilityType = typeof(ShootBehaviour);
    }
    private void OnTriggerEnter(Collider other)
    {
        CheckAbility(other.gameObject);
    }
    private void OnCollisionEnter(Collision collision)
    {
        CheckAbility(collision.gameObject);
    }
    private void CheckAbility(GameObject gameObject)
    {
        Component component;
        if (!gameObject.TryGetComponent(abilityType, out component))
            return;
        BaseAbilityBehaviour baseAbilityBehaviour = component as BaseAbilityBehaviour;
        if (!LayerUtilities.IsSameLayer(layer.PlayerLayer, baseAbilityBehaviour.Caster.layer))
            return;
        IsStartingInteract();
    }
}
