using FMODUnity;
using UnityEngine;
using UnityEngine.Events;

public class BaseInteractWithAbility : MonoBehaviour, ISaveStateJSON
{
    [SerializeField] private int id;
    [SerializeField] private StudioEventEmitter interactSFX;
    [SerializeField] private UnityEvent<bool> interactEvent;
    [SerializeField] private UnityEvent afterAnimationEvent;
    [SerializeField] private UnityEvent restartEvent;
    [SerializeField] private float delaySavedEvent = 0;
    [SerializeField] private UnityEvent savedEvent;

    private void Start()
    {
        DataManager.UpdateStateInterfaceValue(this);
        if (!State)
            return;
        if (delaySavedEvent != 0)
            CoroutineHandler.ExecuteActionAfter(() => savedEvent?.Invoke(), delaySavedEvent, this);
        else
            savedEvent?.Invoke();
        StartInteract(true);
    }
    public void Restart()
    {
        State = false;
        restartEvent?.Invoke();
    }
    protected bool IsStartingInteract()
    {
        if (!ShouldStartInteract())
            return false;
        StartInteract();
        return true;
    }
    private bool ShouldStartInteract()
    {
        if (State)
            return false;
        State = true;
        return true;
    }
    private void StartInteract(bool onSaved = false)
    {
        interactSFX?.Play();
        interactEvent?.Invoke(onSaved);
    }
    public void EndAnimation()
    {
        GetComponent<DropObjects>()?.Drops();
        afterAnimationEvent?.Invoke();
    }

    #region ISaveStateJSON
    private bool interacted = false;
    public bool State
    {
        get => interacted;
        set
        {
            interacted = value;
            Save();
        }
    }
    public string KeyName => "AnimationPlayed";
    public string Path => $"AbilityInteractable/{gameObject.name}_{id}";
    public void Save() => DataManager.SaveInterface(this);
    #endregion
}
