using NaughtyAttributes;
using UnityEngine;

public class ShowDashbleGuide : SetWorldUiOnTopObject
{
    [Header("Dash Guide Settings")]
    [SerializeField] private CollisionHelper detectPlayer;
    [SerializeField] private AbilityData dashData;
    [SerializeField] private LayerData layerData;
    [SerializeField] private Vector3 guideScale = Vector3.one;
    private Camera mainCamera;
    private bool isPlayerInside;
    private bool showing;
    private TutorialTrigger tutorialTrigger;
    protected override void Awake()
    {
        base.Awake();
        tutorialTrigger = gameObject.AddComponent<TutorialTrigger>();
        tutorialTrigger.SetTutorialProperties(TutorialHandlerComponent.TutorialType.SimplePopUp, dashData.abilityTutorialData);
        mainCamera = Camera.main;
        detectPlayer.TriggerEnter += TriggerEnter;
        detectPlayer.TriggerExit += TriggerExit;
        detectPlayer.transform.localScale = Vector3.one * dashData.Distance * 2;
    }
    protected override void Start()
    {
        base.Start();
        baseWorldSpaceUI.OnTopUI.localScale =  0.01f * guideScale;
    }
    private void Update()
    {
        if (!baseWorldSpaceUI.IsActive)
            return;
        if (!isPlayerInside)
            return;
        if (!IsObjectVisible())
        {
            if (showing)
            {
                SetUIDisplay(false);
                showing = false;
            }
            return;
        }
        if (showing)
            return;
        SetUIDisplay(true);
        showing = true;
    }
    private void TriggerEnter(Collider other)
    {
        if (!LayerUtilities.IsSameLayer(layerData.PlayerLayer, other.gameObject.layer))
            return;
        isPlayerInside = true;
        tutorialTrigger.StartTutorial();
    }
    private void TriggerExit(Collider other)
    {
        if (!LayerUtilities.IsSameLayer(layerData.PlayerLayer, other.gameObject.layer))
            return;
        isPlayerInside = false;
        showing = false;
        SetUIDisplay(false);
    }
    private bool IsObjectVisible() => TargetUtilities.IsInFOV(mainCamera.transform.forward, mainCamera.transform.position, transform.position, 95);
    public void Enactive()
    {
        detectPlayer.TriggerEnter -= TriggerEnter;
        detectPlayer.TriggerExit -= TriggerExit;
        detectPlayer.gameObject.SetActive(false);
        showing = false;
        SetUIDisplay(false);
        enabled = false;
    }
    public void Active()
    {
        detectPlayer.TriggerEnter += TriggerEnter;
        detectPlayer.TriggerExit += TriggerExit;
        detectPlayer.gameObject.SetActive(true);
        enabled = true;
        if (!isPlayerInside)
            return;
        showing = true;
        SetUIDisplay(true);
        
    }
}
