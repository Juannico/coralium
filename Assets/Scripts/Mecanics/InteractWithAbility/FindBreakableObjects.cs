using NaughtyAttributes;
using System.Collections.Generic;
using UnityEngine;

public class FindBreakableObjects : MonoBehaviour, ISaveStateJSON
{
    [SerializeField] private PlayerAbilities playerAbilities;
    [SerializeField] private BaseAbilityCast abilityCast;
    // Not using the tutorial data from the BaseAbilityCast because it holds other information
    [SerializeField] private TutorialData breakObjectTutorialData;
    [ReadOnly] private List<HighligthBreakableObjectVFX> HighligthBreakableObjects = new List<HighligthBreakableObjectVFX>();
    private Transform playerTransform;

    public void Initialize(Transform playerTransform)
    {
        transform.localScale = Vector3.one * abilityCast.Data.Distance * 2;
        BaseAbilityCast ability = playerAbilities.GetPlayerAbility(abilityCast);
        ability.Data.CastAbility += () => gameObject.SetActive(true);
        ability.Data.EndAbility += () =>
        {
            TurnOffRocksLightMaterial();
            gameObject.SetActive(false);
        };
        this.playerTransform = playerTransform;
        gameObject.SetActive(false);
        DataManager.UpdateStateInterfaceValue(this, false);
    }
    private void Update()
    {
        transform.position = playerTransform.position;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent(out HighligthBreakableObjectVFX highligthBreakableObjectVFX))
        {
            highligthBreakableObjectVFX.AddToHigthLigh();
            HighligthBreakableObjects.Add(highligthBreakableObjectVFX);
            if (!State)
                return;
            GameManager.Instance.TutorialHandler.StartTutorial(breakObjectTutorialData, TutorialHandlerComponent.TutorialType.SimplePopUp);
            State = false;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.TryGetComponent(out HighligthBreakableObjectVFX highligthBreakableObjectVFX))
        {
            highligthBreakableObjectVFX.RemoveFromHigthLigh();
            HighligthBreakableObjects.Remove(highligthBreakableObjectVFX);
        }
    }
    public void TurnOffRocksLightMaterial()
    {
        for (var i = 0; i < HighligthBreakableObjects.Count; i++)
            HighligthBreakableObjects[i].RemoveFromHigthLigh();
        HighligthBreakableObjects.Clear();
    }

    #region ISaveStateJSON
    private bool firstTime = true;
    public bool State
    {
        get => firstTime;
        set
        {
            firstTime = value;
            Save();
        }
    }
    public string KeyName => "FirstTime";
    public string Path => $"FindBreackbleObject/{gameObject.name}";
    public void Save() => DataManager.SaveInterface(this, false);
    #endregion
}
