using UnityEngine;
using NaughtyAttributes;


public class Boost : MonoBehaviour
{
    [SerializeField] private bool useCheckPointProvider;
    [ShowIf("useCheckPointProvider")] [SerializeField] private CheckPointProvider checkPointProvider;
    [SerializeField] private bool autoInitialize;
    [ShowIf("autoInitialize")] [SerializeField] private CollisionHelper collisionHelper;
    [ShowIf("autoInitialize")] [SerializeField] private LayerData layerData;
    [SerializeField] private float boostStrength = 2;
    [SerializeField] private float boostDuration = 1;
    [SerializeField] private bool canChangeDirection = false;
    [SerializeField] private bool showGuide;
    [SerializeField] private Transform VfxContainer;
    [SerializeField] private GPUParticleSystem boostVFX;
    [ShowIf("showGuide")] [SerializeField] private float rayLength = 7.5f;

    private void Awake()
    {
        if (useCheckPointProvider)
            checkPointProvider.OnRightSuccessfullEnter += StartBoost;
        if (!autoInitialize)
            return;
        collisionHelper.TriggerEnter += (other) =>
        {
            if (!LayerUtilities.IsSameLayer(layerData.PlayerLayer, other.gameObject.layer))
                return;
            StartBoost(other);
        };
    }
    private void StartBoost(Collider other)
    {
        boostVFX.Restart();
        boostVFX.transform.parent = other.transform;
        boostVFX.transform.localPosition = Vector3.forward;
        boostVFX.transform.localRotation = Quaternion.identity;
        StateComponentHandler componentHandler = other.GetComponentInParent<PlayerController>().StateHandler;
        float timer = 0;
        float startMovementSpeedModifier = componentHandler.Data.MovementSpeedModifier;
        float startSelfRotationMultiplier = componentHandler.Data.SelfRotationMultiplier;
        componentHandler.Data.IsInExternalControl = !canChangeDirection;
        componentHandler.Data.CurrenBoostStrength = gameObject;
        StartCoroutine(CoroutineHandler.ExecuteActionAfterUpdateUntil(() =>
        {
            componentHandler.Data.MovementDirection = transform.forward;
            componentHandler.Data.MovementSpeedModifier = boostStrength;
            componentHandler.Data.SelfRotationMultiplier = 5;
            timer += Time.deltaTime;
            return timer > boostDuration || componentHandler.Data.CurrenBoostStrength != gameObject;
        }, () =>
        {
            componentHandler.Data.IsInExternalControl = false;
            componentHandler.Data.MovementSpeedModifier = startMovementSpeedModifier;
            componentHandler.Data.SelfRotationMultiplier = startSelfRotationMultiplier;
            componentHandler.Data.CurrenBoostStrength = null;
            boostVFX.transform.parent = VfxContainer;
            boostVFX.transform.localRotation = Quaternion.identity;
            boostVFX.transform.localPosition = Vector3.zero;
        }));
    }
    private void OnDrawGizmos()
    {
        if (!showGuide)
            return;
        Gizmos.color = Color.cyan;
        Gizmos.DrawRay(transform.position, transform.forward * transform.localScale.magnitude * rayLength);
    }

}
