using UnityEngine;

public class JumpOutOcean : MonoBehaviour
{
    [SerializeField] private LayerData layerData;
    [SerializeField] private float jumpMutilplier = 2;
    private PlayerController playerController;
    private PlayerCharacter playerCharacter;
    private bool isJumpting;
    private Vector2 enterVelocity;
    private Vector2 exitVelocity;
    private float jumpTime;
    private float timer;
    private void FixedUpdate()
    {
        if (!isJumpting)
            return;
        timer += Time.fixedDeltaTime;
        playerCharacter.RigibodyForceHandle.VelocityForceVector.y = 0;
        playerCharacter.RigibodyForceHandle.ExternalVelocityForceVector = Vector3.Lerp(enterVelocity, exitVelocity, timer / jumpTime);
        if (timer < jumpTime)
            return;
        isJumpting = false;
        playerCharacter.BaseCharacter.Data.IsInExternalForce = false;
        playerController.StateHandler.Stop();
    }
    private void OnTriggerEnter(Collider other)
    {
        if (isJumpting)
            return;
        if (!LayerUtilities.IsSameLayer(layerData.PlayerLayer, other.gameObject.layer))
            return;
        if (playerController == null)
        {
            playerController = other.GetComponentInParent<PlayerController>();
            playerCharacter = playerController.Character;
        }
        playerController.StateHandler.Stop();
        isJumpting = true;
        timer = 0;
        playerCharacter.BaseCharacter.Data.IsInExternalForce = true;
        enterVelocity = playerCharacter.RigibodyForceHandle.Rb.velocity * jumpMutilplier;
        exitVelocity = enterVelocity;
        exitVelocity.y *= -1;
        playerCharacter.RigibodyForceHandle.ExternalVelocityForceVector = enterVelocity;
        jumpTime = playerCharacter.RigibodyForceHandle.Rb.velocity.y / 9.8f;
        jumpTime *= 2;
    }
}
