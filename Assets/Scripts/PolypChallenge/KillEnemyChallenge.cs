using NaughtyAttributes;
using UnityEngine;

public class KillEnemyChallenge : BaseChallenge
{
    [SerializeField] private EnemyCharacter enemyCharacter;
    [SerializeField] private GameObject lockDownColliders;
    [SerializeField] private CollisionHelper activeChallenge;
    [SerializeField] private bool deactiveCollider;
    [ShowIf("doKinematic")] [SerializeField] private Transform diedTarget;
    protected override void InitializeChallenge()
    {
        base.InitializeChallenge();
        if (lockDownColliders != null)
            lockDownColliders.SetActive(false);
        enemyCharacter.BaseCharacter.Data.OnCharacterDead += ChallengeComplete;
        activeChallenge.TriggerEnter += (other) =>
        {
            if (isComplete)
                return;
            if (!LayerUtilities.IsSameLayer(enemyCharacter.BaseCharacter.LayerData.PlayerLayer, other.gameObject.layer))
                return;
            if (lockDownColliders != null)
                CoroutineHandler.ExecuteActionAfter(() => lockDownColliders.SetActive(true), 0.1f, this);
            if (deactiveCollider)
                activeChallenge.gameObject.SetActive(false);
        };
    }
    protected override void ChallengeCompleted()
    {
        base.ChallengeCompleted();
        enemyCharacter.Died();
        enemyCharacter.Model.SetActive(false);
        activeChallenge.gameObject.AddComponent<UnHideGameObject>();
        activeChallenge.gameObject.SetActive(false);
    }
    public override void ChallengeComplete()
    {
        if (polyp.Data.IsCaptured && doKinematic)
        {
            kinematic.OnStartKinematic += () => CoroutineHandler.ExecuteActionAfter(() => enemyCharacter.transform.position = diedTarget.position, Constants.halfSecondDelay, this);
            kinematic.OnEndKinematic += () =>
                {
                    enemyCharacter.Model.SetActive(false);
                    enemyCharacter.GetComponent<DropObjects>()?.Drops();
                };
        }
        base.ChallengeComplete();
        if (lockDownColliders != null)
            lockDownColliders?.SetActive(false);
    }
    public override void ShowUI()
    {
        base.ShowUI();
        challengeScreen.UpdateInfo("");
    }
}
