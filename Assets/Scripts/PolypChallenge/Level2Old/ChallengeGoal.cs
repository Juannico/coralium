using NaughtyAttributes;
using UnityEngine;

public class ChallengeGoal : BaseChallenge
{
    public CheckPointSequenceChecker checker;
    [SerializeField] private bool shouldHideObject;
    [SerializeField] [ShowIf("shouldHideObject")] private GameObject objectToUnHide;
    [SerializeField] private CollisionHelper collisionHelper;
    protected override void Start()
    {
        if (shouldHideObject)
            objectToUnHide.SetActive(true);
        base.Start();
    }

    private void TriggerEnter(Collider other)
    {
        if (!checker.GotAllCheck())
            return;
        ChallengeComplete();
        collisionHelper.TriggerEnter -= TriggerEnter;
    }

    #region Override BaseChallenge Methods
    protected override void InitializeChallenge()
    {
        base.InitializeChallenge();
        collisionHelper.TriggerEnter += TriggerEnter;
    }
    public override void ChallengeComplete()
    {
        if (shouldHideObject)
            objectToUnHide.SetActive(false);
        base.ChallengeComplete();
    }
    protected override void ChallengeCompleted()
    {
        base.ChallengeCompleted();
        checker.CheckAll();
    }
    #endregion

}
