using UnityEngine;

public class ChaseAndCatchObject : BaseChallenge
{
    [SerializeField] private RouteFollower routeFollower;
    [SerializeField] private float distanceToRun;
    [SerializeField] private float velocityMultiplier;
    [SerializeField] private bool showTargueGuidArea;
    [SerializeField] private Transform targetDropPosition;
    [SerializeField] private float dropTime;
    private Transform playerTransform;
    private bool shouldRun;
    private float idleVelocity;
    private Transform modelTransform;
    private Vector3 startAngle;
    private float smoothVelocity;
    protected override void Start()
    {
        modelTransform = transform.GetChild(0);
        if (modelTransform == null)
            throw new System.Exception("gameObject doesn't have child");
        base.Start();
        playerTransform = GameObject.FindGameObjectWithTag(Tags.Player).transform;
    }
    private void Update()
    {
        if (!shouldRun)
        {
            routeFollower.Velocity = idleVelocity;
            return;
        }
        float distance = Vector3.Distance(transform.position, playerTransform.position);
        if (routeFollower.Velocity != idleVelocity * velocityMultiplier)
            routeFollower.Velocity = Mathf.SmoothDamp(routeFollower.Velocity, idleVelocity * velocityMultiplier, ref smoothVelocity, 0.25f);
        if (distance > distanceToRun * 0.25f)
            routeFollower.direction = Vector3.Dot((transform.position - playerTransform.position).normalized, transform.forward) > 0 ? 1 : -1;
        Vector3 targetRotation = routeFollower.direction > 0 ? startAngle : (Vector3.right * 180f) + startAngle;
        modelTransform.localRotation = Quaternion.Euler(targetRotation);

    }
    private void OnTriggerEnter(Collider other)
    {
        if (!LayerUtilities.IsSameLayer(layerData.PlayerLayer, other.gameObject.layer))
            return;
        if (shouldRun)
        {
            ChallengeComplete();
            return;
        }
        shouldRun = true;
    }
    private void OnTriggerExit(Collider other)
    {
        if (!LayerUtilities.IsSameLayer(layerData.PlayerLayer, other.gameObject.layer))
            return;
        shouldRun = false;
    }

    #region Override BaseChallenge Methods
    protected override void InitializeChallenge()
    {
        base.InitializeChallenge();
        SphereCollider sphereCollider = GetComponent<SphereCollider>();
        sphereCollider.radius = distanceToRun;
        idleVelocity = routeFollower.Velocity;
        startAngle = modelTransform.localRotation.eulerAngles;
        polyp.gameObject.AddComponent<UnHideGameObject>();
        polyp.gameObject.SetActive(false);
    }
    public override void ChallengeComplete()
    {
        // gameObject.SetActive(false);
        // modelTransform.localRotation = Quaternion.Euler(Vector3.up * -90f);
        modelTransform.LookAt(playerTransform.position);
        routeFollower.Reset(true);
        DropPolyp();
        routeFollower.enabled = false;
        enabled = false;
    }
    public override void ShowUI()
    {
        base.ShowUI();
        challengeScreen.UpdateInfo("");
    }
    protected override void ChallengeCompleted()
    {
        base.ChallengeCompleted();
        modelTransform.localRotation = Quaternion.Euler(Vector3.up * -90f);
        routeFollower.Reset(true);
        routeFollower.enabled = false;
        enabled = false;
    }
    #endregion

    private void DropPolyp()
    {
        polyp.transform.position = modelTransform.position;
        Vector3 targetScale = polyp.transform.localScale;
        Vector3 startPosition = polyp.transform.position;
        float timer = 0;
        polyp.gameObject.SetActive(true);
        StartCoroutine(CoroutineHandler.ExecuteActionAfterUpdateUntil(() =>
        {
            timer += Time.deltaTime;
            polyp.transform.localScale = Vector3.Slerp(Vector3.zero, targetScale, timer / dropTime);
            polyp.transform.position = Vector3.Slerp(startPosition, targetDropPosition.position, timer / dropTime);
            return timer >= dropTime;
        }, () =>
        {
            polyp.transform.localScale = targetScale;
            polyp.transform.position = targetDropPosition.position;
            base.ChallengeComplete();
        }));
    }
#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        if (!showTargueGuidArea)
            return;
        SphereCollider sphereCollider = GetComponent<SphereCollider>();
        if (sphereCollider == null)
            return;

        Gizmos.color = Color.red;
        float multiplier = Application.isPlaying ? 1 : distanceToRun * 2;
        Gizmos.DrawWireSphere(transform.position, sphereCollider.radius * multiplier);

    }
#endif

}
