using UnityEngine;

public class CleanCoralChallenge : BaseChallenge
{
    [Header("Clean Coral Settings")]
    [SerializeField] private SetMaterialPropierty[] jailMaterialPropiety;
    [SerializeField] private float fixedPowValue = 0.75f;
    [SerializeField] private LookTargetCamera lookTargetCamera;
    private CleanCoralController[] corals;
    private int cleanCoralCounter = 0;
    protected override void Start()
    {
        corals = GetComponentsInChildren<CleanCoralController>(true);
        for (int i = 0; i < corals.Length; i++)
            corals[i].OnDirty += OnCoralClean;
        base.Start();
    }
    private void OnCoralClean(bool isDirty)
    {
        if (isDirty)
            return;
        cleanCoralCounter++;
        bool lookPolyp = cleanCoralCounter == 1 || cleanCoralCounter == corals.Length;
        float targetValueJail = Mathf.Pow((float)cleanCoralCounter / corals.Length, fixedPowValue);
        foreach (SetMaterialPropierty setMaterialPropierty in jailMaterialPropiety)
            setMaterialPropierty.ChangePropiertyByValue(targetValueJail, lookPolyp);
        if (lookPolyp)
            lookTargetCamera.StartLookTarget();
        UpdateInfo($" {cleanCoralCounter}/{corals.Length}");
        if (cleanCoralCounter == corals.Length)
            ChallengeComplete();
    }
    #region Override BaseChallenge Methods
    public override void ShowUI()
    {
        base.ShowUI();
        challengeScreen.UpdateInfo($" {cleanCoralCounter}/{corals.Length}");
    }
    protected override void ChallengeCompleted()
    {
        base.ChallengeCompleted();
        for (int i = 0; i < corals.Length; i++)
            corals[i].Cleaned();
    }
    #endregion

}
