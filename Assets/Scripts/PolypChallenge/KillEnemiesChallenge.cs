using UnityEngine;

public class KillEnemiesChallenge : BaseChallenge
{
    [Header("Kill enemies Settings")]
    [SerializeField] protected CollisionHelper activeChase;
    [SerializeField] private int amountOfEnemiesToSpawn;
    [SerializeField] private Vector3 spawnArea;
    [SerializeField] protected GameObject enemyPrefab;
    [SerializeField] private bool showGuide;
    private int enemiesKillCounter = 0;

    private EnemyController[] enemies;
    #region Override BaseChallenge Methods
    protected override void InitializeChallenge()
    {
        base.InitializeChallenge();
        enemies = new EnemyController[amountOfEnemiesToSpawn];
        for (int i = 0; i < amountOfEnemiesToSpawn; i++)
            enemies[i] = CreateEnemy(i);
    }
    public override void ShowUI()
    {
        base.ShowUI();
        UpdateInfo($" {enemiesKillCounter}/{amountOfEnemiesToSpawn}");
    }
    #endregion

    #region Reusable KillEnemiesChallenge Methods
    protected virtual EnemyController CreateEnemy(int i)
    {
        Quaternion randomRotation = Quaternion.Euler(MathUtilities.RandomVector2(Vector3.one * -180, Vector3.one * 180));
        Vector3 randomPositionOffset = MathUtilities.RandomVector2((Vector2)spawnArea * -0.5f, (Vector2)spawnArea * 0.5f);
        string name = $"{enemyPrefab.name}_ {i}";
        Vector2 enemyPosition = transform.position + randomPositionOffset;
        EnemyController enemyController = Instantiate(enemyPrefab, enemyPosition, Quaternion.identity, transform).GetComponentInChildren<EnemyController>();
        enemyController.transform.parent = null;
        enemyController.name = name;
        enemyController.transform.GetChild(0).rotation = randomRotation;
        CollisionHelper sourceEnemyActiveChase = enemyController.ActiveChase;
        sourceEnemyActiveChase.gameObject.SetActive(false);
        enemyController.ActiveChase = activeChase;
        enemyController.ActiveChase.TransferDelegates(sourceEnemyActiveChase, false);
        enemyController.Character.BaseCharacter.Data.OnCharacterDead += enemyKilled;
        return enemyController;
    }
    #endregion
    private void enemyKilled()
    {
        enemiesKillCounter++;
        if (useUI && !isComplete)
            UpdateInfo($" {enemiesKillCounter}/{amountOfEnemiesToSpawn}");
        if (enemiesKillCounter == amountOfEnemiesToSpawn)
            ChallengeComplete();
    }

#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        if (!showGuide)
            return;
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(transform.position, spawnArea);
    }
#endif
}
