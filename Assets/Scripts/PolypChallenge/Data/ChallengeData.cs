using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Challenge Data", menuName = "Coralium/Challenges/Create Challenge Data", order = 51)]
public class ChallengeData : ScriptableObject
{
    public Sprite Icon;
    public Color Color;
    public TextTableFieldName Title;
    public TextTableFieldName Description;
    public TextTableFieldName Progress;
    public TextTableFieldName Instruction;
}
