using UnityEngine;
using NaughtyAttributes;
using FMODUnity;
using UnityEngine.Events;

public class TotemCollider : MonoBehaviour, IDashable
{
    public int id;
    PuzzleTotems puzzle;
    [SerializeField] private bool highLightOnCompleted;
    [ShowIf("highLightOnCompleted")] [SerializeField] private Material rightMaterialVFX;
    [ShowIf("highLightOnCompleted")] [SerializeField] private Material wrongMaterialVFX;
    [ShowIf("activeWithSpecialDash")] [SerializeField] private Material hightLightMaterial;
    [SerializeField] private bool hideOtherObject;
    [ShowIf("hideOtherObject")] [SerializeField] private GameObject objectToHide;
    [Header("Guide Settings ")]
    public Vector3 TargetGuidePosition;
    [SerializeField] private bool showGuide = false;
    [Header("SFX")]
    [SerializeField] private StudioEventEmitter rightSFX;
    [SerializeField] private StudioEventEmitter wrongSFX;
    [Header("Events")]
    [SerializeField] private UnityEvent rightUnityEvent;
    [SerializeField] private UnityEvent wrongUnityEvent;
    private HighligthBreakableObjectVFX highligthBreakableObjectVFX;
    private SetMaterialProvider setMaterialProvider;
    private bool isInitialized = false;
    [HideInInspector] public bool CanInteract = true;
    private bool isRigth;
    private void Awake()
    {
        if (!isInitialized)
            Initialize();
    }

    private void Initialize()
    {
        isInitialized = true;
        highligthBreakableObjectVFX = gameObject.AddComponent<HighligthBreakableObjectVFX>();
        highligthBreakableObjectVFX.HigthLighMaterial = hightLightMaterial;
        setMaterialProvider = ComponentUtilities.SetComponent<SetMaterialProvider>(gameObject);
        puzzle = gameObject.GetComponentInParent<PuzzleTotems>();
    }
    public bool MakeInteraction(out bool shoulBounce)
    {
        shoulBounce = true;
        if (!CanInteract)
            return true;
        isRigth = puzzle.OnTotemContact(id);
        SetState(true);
        return true;
    }
    public void SetState(bool useEffect, bool restart = false)
    {
        CanInteract = !useEffect;
        if (!useEffect && restart)
            isRigth = true;
        if (useEffect && !restart)
        {
            if (isRigth)
            {
                rightSFX?.Play();
                rightUnityEvent?.Invoke();
            }
            else
            {
                wrongSFX?.Play();
                wrongUnityEvent?.Invoke();
            }
        }
        if (hideOtherObject && objectToHide != null)
        {
            ComponentUtilities.SetComponent<UnHideGameObject>(objectToHide);
            objectToHide.SetActive(!isRigth);
        }
        if (!isInitialized)
            Initialize();
        if (highLightOnCompleted)
        {
            SetHightLigh(useEffect);
            gameObject.SetActive(true);
            return;
        }
    }

    private void SetHightLigh(bool state)
    {
        Material materialToSet = isRigth ? rightMaterialVFX : wrongMaterialVFX;
        if (state)
        {
            setMaterialProvider.SetMaterial(materialToSet);
            highligthBreakableObjectVFX.CanDetect = false;
            return;
        }
        setMaterialProvider.RemoveMaterial(materialToSet);
        highligthBreakableObjectVFX.CanDetect = true;
    }

    public void Complete()
    {
        isRigth = true;
        SetState(true);
    }
#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        if (!showGuide)
            return;
        Gizmos.color = Color.cyan;
        Gizmos.DrawWireSphere(transform.position + TargetGuidePosition, 5);
    }
#endif
}
