using BehaviorDesigner.Runtime;
using UnityEngine;

public class ProtectFromEnemiesChallenge : KillEnemiesChallenge
{
    [Header("Protect Settings")]
    [SerializeField] private GameObject[] objectsToProtect;
    [SerializeField] private CollisionHelper activeKinematic;
    [SerializeField] private BaseKinematic startkinematic;

    private void OnDestroy()
    {
        activeKinematic.TriggerEnter -= ActiveKinematic;
    }

    #region Override BaseChallenge Methods
    protected override void InitializeChallenge()
    {
        activeKinematic.TriggerEnter += ActiveKinematic;
        base.InitializeChallenge();
    }
    public override void ChallengeComplete()
    {
        base.ChallengeComplete();
        activeKinematic.Collider.enabled = false;
        activeKinematic.TriggerEnter -= ActiveKinematic;
    }
    #endregion

    private void ActiveKinematic(Collider other)
    {
        if (!LayerUtilities.IsSameLayer(layerData.PlayerLayer, other.gameObject.layer))
            return;
        startkinematic.StartKinematic();
        activeKinematic.Collider.enabled = false;
    }
}
