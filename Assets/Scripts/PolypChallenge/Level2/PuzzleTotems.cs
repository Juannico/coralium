using NaughtyAttributes;
using UnityEngine;
using UnityEngine.Events;

public class PuzzleTotems : BaseChallenge
{
    [Header("Puzzele Totems Challenge Settings")]
    [SerializeField] private bool randomOrder;
    private TotemCollider[] totems;
    [ReadOnly] private int counter = 1;
    [HideInInspector] public int size;
    [SerializeField] private bool shouldRestart = true;
    [SerializeField] private UnityEvent restartEvent;
    [SerializeField] private bool useGuide = true;
    [SerializeField] [ShowIf("useGuide")] private Transform targetGuide;
    private Vector3 defaultPosition;
    // Start is called before the first frame update
    protected override void Start()
    {
        totems = GetComponentsInChildren<TotemCollider>(true);
        base.Start();
        if (!randomOrder && useGuide)
        {
            defaultPosition = targetGuide.position;
            targetGuide.position = totems[0].transform.position + totems[0].TargetGuidePosition;
        }
        size = totems.Length;
    }

    #region Override BaseChallenge Methods
    public override void ShowUI()
    {
        base.ShowUI();
        challengeScreen.UpdateInfo($" {counter - 1}/{totems.Length}");
    }
    protected override void ChallengeCompleted()
    {
        base.ChallengeCompleted();
        if (!randomOrder && useGuide)
            targetGuide.position = defaultPosition;
        for (int i = 0; i < totems.Length; i++)
        {
            totems[i].gameObject.AddComponent<UnHideGameObject>();
            totems[i].Complete();
        }
    }
    public override void ChallengeComplete()
    {
        base.ChallengeComplete();
        for (int i = 0; i < totems.Length; i++)
            totems[i].CanInteract = false;
    }
    #endregion
    private void Restart()
    {
        for (int i = 0; i < totems.Length; i++)
            totems[i].SetState(false, true);
        restartEvent?.Invoke();
        SetCounter(1);
        if (useGuide)
            targetGuide.position = totems[0].transform.position + totems[0].TargetGuidePosition;
    }
    public bool OnTotemContact(int id)
    {

        if (randomOrder)
        {
            if (counter < size)
            {
                SetCounter(counter + 1);
                return true;
            }
            ChallengeComplete();
            return true;
        }
        if (counter != id)
        {
            if (shouldRestart)
                CoroutineHandler.ExecuteActionAfter(() => Restart(), Constants.OneSecondDelay, this);
            return false;
        }
        if (id == size)
        {
            ChallengeComplete();
            return true;
        }
        SetCounter(counter + 1);
        if (useGuide)
            targetGuide.position = totems[id].transform.position + totems[id].TargetGuidePosition;
        return true;
    }
    private void SetCounter(int value)
    {
        counter = value;
        if (!isComplete)
            UpdateInfo($" {counter - 1}/{totems.Length}");
    }

}
