using UnityEngine;

public class Race : BaseChallenge
{
    [SerializeField] private RouteFollower routeFollower;
    [SerializeField] private GameObject startDoor;
    [SerializeField] private CollisionHelper finish;
    [SerializeField] private float timeToStartRace;
    [SerializeField] private CounterUI counterUI;
    private float timeToEndRace;
    private bool arrive;
    private bool started;
    protected override void Start()
    {
        base.Start();
        startDoor.AddComponent<UnHideGameObject>();
        startDoor.SetActive(false);
        timeToEndRace = routeFollower.Route.path.length / routeFollower.Velocity;
        if (isComplete)
            return;
        routeFollower.Reset(true);
        SetRace();
    }
    protected override void InitializeChallenge()
    {
        base.InitializeChallenge();
        AddDelegates();
        started = false;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (!LayerUtilities.IsSameLayer(layerData.PlayerLayer, other.gameObject.layer))
            return;
        if (routeFollower.ShouldMove)
            return;
        startDoor.SetActive(true);
        finish.gameObject.SetActive(true);
        counterUI.StartCount(timeToStartRace);
        CoroutineHandler.ExecuteActionAfter(() =>
        {
            StartRace();
        }, timeToStartRace, this);
    }

    private void SetRace()
    {
        arrive = false;
        if (!routeFollower.gameObject.activeInHierarchy)
            routeFollower.gameObject.SetActive(true);
        routeFollower.SetStart();
        CoroutineHandler.ExecuteActionAfter(() => finish.gameObject.SetActive(false), Constants.halfSecondDelay, this);
    }
    private void AddDelegates()
    {
        ComponentUtilities.SetComponent<UnHideGameObject>(finish.gameObject);
        finish.TriggerEnter += (other) =>
        {
            if (!LayerUtilities.IsSameLayer(layerData.PlayerLayer, other.gameObject.layer))
                return;
            finish.gameObject.SetActive(false);
            EndRace();
        };
    }
    private void StartRace()
    {
        routeFollower.Reset(false);
        startDoor.SetActive(false);
        started = true;
        CoroutineHandler.ExecuteActionAfter(() =>
        {
            if (!started)
                return;
            arrive = true;
            EndRace();
        }, timeToEndRace, this);
    }
    private void EndRace()
    {
        string message = arrive ? "lose" : "win";
        bool restartRace = arrive;
        arrive = true;
        routeFollower.Reset(true);
        ComponentUtilities.SetComponent<UnHideGameObject>(routeFollower.gameObject);
        routeFollower.gameObject.SetActive(false);
        started = false;
        CoroutineHandler.ExecuteActionAfter(() =>
        {
            if (restartRace)
            {
                SetRace();
                return;
            }
            ChallengeComplete();

        }, Constants.OneSecondDelay, this);
    }
    public override void ChallengeComplete()
    {
        base.ChallengeComplete();
        ComponentUtilities.SetComponent<UnHideGameObject>(gameObject);
        gameObject.SetActive(false);
    }
}
