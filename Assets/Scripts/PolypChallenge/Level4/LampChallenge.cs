using UnityEngine;

public class LampChallenge : BaseChallenge
{
    private Lamp[] smallLamps;
    private BigLamp bigLamp;
    private int lampIndex;
    [SerializeField] private GameObject bigLampMesh;
    private SetMaterialProvider bigLampMeshSetMaterialProvider;
    [SerializeField] private BaseVisualEffect targetVFX;
    [SerializeField] private Material electrocutedMaterial;
    private float electricityAmountMultiplier;
    protected override  void Start()
    {
        SetSmallLamps();
        bigLamp = gameObject.GetComponentInChildren<BigLamp>(true);
        bigLampMeshSetMaterialProvider = ComponentUtilities.SetComponent<SetMaterialProvider>(bigLampMesh);
        electricityAmountMultiplier = electrocutedMaterial.GetFloat("_CantidadElec") / smallLamps.Length;
        base.Start();
    }


    #region Override BaseChallenge Methods
    public override void ShowUI()
    {
        base.ShowUI();
        challengeScreen.UpdateInfo($" {lampIndex}/{smallLamps.Length}");
    }
    public override void ChallengeComplete()
    {
        base.ChallengeComplete();
        targetVFX.StartEffect(0);
    }
    protected override void ChallengeCompleted()
    {
        base.ChallengeCompleted();
        for (int i = 0; i < smallLamps.Length; i++)
            smallLamps[i].ElectrocutedEffect.OnEffect();
        bigLamp.SetToTargetRotationt();
        SetBigLamp();
    }
    #endregion

    private void SetSmallLamps()
    {
        Lamp[] lamps = gameObject.GetComponentsInChildren<Lamp>(true);
        smallLamps = new Lamp[lamps.Length - 1];
        int index = 0;
        for (int i = 0; i < lamps.Length; i++)
        {
            if (typeof(Lamp) != lamps[i].GetType())
                continue;
            smallLamps[index] = lamps[i];
            index++;
        }
    }
    public void OnSmallLampAffected(bool electrocuted)
    {
        lampIndex += electrocuted ? 1 : -1;

        bigLampMeshSetMaterialProvider.SetMaterial(electrocutedMaterial, (material) =>
        {
            material.SetFloat("_Alpha", 1);
            material.SetFloat("_CantidadElec", electricityAmountMultiplier * lampIndex);
        });
        UpdateInfo($" {lampIndex}/{smallLamps.Length}");
        if (lampIndex == smallLamps.Length)
            SetBigLamp();
    }
    private void SetBigLamp()
    {
        bigLamp.ElectrocutedEffect.OnEffect();
        for (int i = 0; i < smallLamps.Length; i++)
            smallLamps[i].ElectrocutedEffect.TurnOffAutomaticOffEffect();
        CheckLampTarget();
    }
    public void CheckLampTarget()
    {
        if (bigLamp.IsTargeting)
            ChallengeComplete();
    }
}
