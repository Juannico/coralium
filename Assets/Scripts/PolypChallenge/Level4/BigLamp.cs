using UnityEngine;

public class BigLamp : Lamp, IDashable
{
    [SerializeField] private float roateAmount = 90;
    [SerializeField] private float rotateVelocity = 1;
    [SerializeField] private float targetRotate = 180;
    private bool rotating = false;
    public bool IsTargeting
    {
        get { return transform.localRotation.eulerAngles.y == targetRotate && ElectrocutedEffect.IsAffectedByElement; }
    }
    public bool MakeInteraction(out bool shouldBounce)
    {
        shouldBounce = true;
        if (IsTargeting)
            return true;
        if (rotating)
            return true;
        rotating = true;
        Vector3 targetRotation = transform.localRotation.eulerAngles + Vector3.up * roateAmount;
        Vector3 startRotation = transform.localRotation.eulerAngles;
        float timer = 0;
        StartCoroutine(CoroutineHandler.ExecuteActionAfterUpdateUntil(() =>
        {
            timer += Time.deltaTime * rotateVelocity;
            Vector3 rotate = Vector3.Lerp(startRotation, targetRotation, timer);
            transform.localRotation = Quaternion.Euler(rotate);
            return rotate == targetRotation;
        }, () =>
        {
            rotating = false;
            lampChallenge.CheckLampTarget();
        }));
        return true;
    }
    public void SetToTargetRotationt()
    {
        transform.localRotation = Quaternion.Euler(transform.localRotation.eulerAngles + Vector3.up * targetRotate);
    }
}
