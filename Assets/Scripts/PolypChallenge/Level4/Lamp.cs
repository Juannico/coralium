using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lamp : MonoBehaviour
{
     [field: SerializeField]public BaseStatusEffect ElectrocutedEffect { get; private set; }
    [SerializeField] private GameObject ray;
    protected LampChallenge lampChallenge;
    private bool isOn;
    private void Awake()
    {
        ray.SetActive(false);
        lampChallenge = GetComponentInParent<LampChallenge>();
        if (ElectrocutedEffect == null)
            ElectrocutedEffect = gameObject.GetComponentInChildren<BaseStatusEffect>();
        SetElectrocuttedEffecActions();
    }

    protected virtual void SetElectrocuttedEffecActions()
    {
        ElectrocutedEffect.OnAffectedByElementAction += () => SetState(true);
        ElectrocutedEffect.OnRemoveEffectByElement += () => SetState(false);
    }

    protected void SetState(bool electrocuted)
    {
        if (isOn == electrocuted)
            return;
        isOn = electrocuted;
        ray?.SetActive(electrocuted);
        if(typeof(Lamp) == this.GetType())
            lampChallenge?.OnSmallLampAffected(electrocuted);
    }
}
