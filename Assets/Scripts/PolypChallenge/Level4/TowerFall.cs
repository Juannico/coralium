using UnityEngine;
using UnityEngine.Events;

public class TowerFall : BaseChallenge, IDashable
{
    [SerializeField] private int hitNumber;
    [SerializeField] private int fallDistance = 32;
    [SerializeField] private float fallVelocity = 2.7f;
    [SerializeField] private bool showGuide;
    [SerializeField] private UnityEvent shakeFeel;
    private int currentHit;
    public bool MakeInteraction(out bool shouldBounce)
    {
        shouldBounce = false;
        currentHit++;
        if (currentHit >= hitNumber)
            ChallengeComplete();
        shakeFeel?.Invoke();
        return true;
    }

    #region Override BaseChallenge Methods
    public override void ChallengeComplete()
    {
        float timer = 0;
        Vector3 polypStartPosition = polyp.transform.position;
        Vector3 polyptargetPosition = polypStartPosition - Vector3.up * fallDistance;
        GetComponent<Renderer>().enabled = false;
        GetComponent<Collider>().enabled = false;
        if (!polyp.Data.IsCaptured)
        {
            base.ChallengeComplete();
            gameObject.SetActive(false);
            return;
        }
        StartCoroutine(CoroutineHandler.ExecuteActionAfterUpdateUntil(() =>
        {
            timer += Time.deltaTime;
            polyp.transform.position = Vector3.Lerp(polypStartPosition, polyptargetPosition, timer / fallVelocity);
            return timer > fallVelocity;
        }, () =>
        {
            base.ChallengeComplete();
            gameObject.SetActive(false);
        }));

    }
    #endregion

#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        if (!showGuide)
            return;
        if (polyp == null)
            return;
        Gizmos.color = Color.red;
        Vector3 targetPosition = polyp.transform.position - Vector3.up * fallDistance;
        Gizmos.DrawCube(targetPosition, polyp.transform.localScale);
    }
#endif
}
