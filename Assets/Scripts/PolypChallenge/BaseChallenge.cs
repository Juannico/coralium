using FMODUnity;
using NaughtyAttributes;
using UnityEngine;
using UnityEngine.Events;

public class BaseChallenge : MonoBehaviour
{
    [Header("Base Settings")]
    [SerializeField] protected Polyp polyp;
    [SerializeField] private UnityEvent challengeCompleted;
    [SerializeField] protected bool doKinematic;
    [ShowIf("doKinematic")] [SerializeField] protected BaseKinematic kinematic;
    [SerializeField] protected bool useUI;
    [ShowIf("useUI")] [SerializeField] protected ChallengeData challengeData;
    [ShowIf("useUI")] [SerializeField] private bool activeExternal;
    [ShowIf("useUI")] [SerializeField] private CollisionHelper showUI;
    [SerializeField] protected bool unlockAbility;
    [ShowIf("unlockAbility")] [SerializeField] private UnlockAbility abilityToUnlock;
    [ShowIf("unlockAbility")] [SerializeField] private AbilityData abilityData;
    [HideInInspector] private PlayerProgressHandle progressHandle;
    [SerializeField] protected LayerData layerData;
    protected ChallengeUI challengeScreen;
    [Header("SFX")]
    [SerializeField] private StudioEventEmitter completeChallengeSFX;

    protected bool isComplete = false;
    protected virtual void Start()
    {
        if (useUI && polyp.Data.IsCaptured)
            SetUpInfoZoneUI();
        if (!polyp.Data.IsCaptured)
        {
            ChallengeCompleted();
            return;
        }
        InitializeChallenge();
    }
    private void SetUpInfoZoneUI()
    {
        if (activeExternal)
            return;
        showUI.TriggerEnter += (other) =>
        {
            if (isComplete)
                return;
            if (!LayerUtilities.IsSameLayer(layerData.PlayerLayer, other.gameObject.layer))
                return;
            ShowUI();
        };
        showUI.TriggerExit += (other) =>
        {
            if (isComplete)
                return;
            if (!LayerUtilities.IsSameLayer(layerData.PlayerLayer, other.gameObject.layer))
                return;
            HideUI();
        };
    }
    public void HideUI()
    {
        if (useUI)
            challengeScreen?.HideInfoZone();
    }
    #region Reusable BaseChallenge Methods
    public virtual void ShowUI()
    {
        if (!useUI)
            return;
        challengeScreen = UIManager.Instance.OpenChallengeScreen();
        challengeScreen.Init(challengeData);
    }
    public virtual void UpdateInfo(string infoText)
    {
        if (useUI)
            challengeScreen?.UpdateInfo(infoText);
    }
    protected virtual void InitializeChallenge()
    {
        if (!unlockAbility)
            return;
        if (abilityToUnlock.Ability.IsLocked)
            abilityToUnlock.Initialize();
    }
    protected virtual void ChallengeCompleted() => ChallengeComplete();
    public virtual void ChallengeComplete()
    {
        challengeCompleted?.Invoke();
        if (polyp.Data.IsCaptured)
        {
            completeChallengeSFX?.Play();
            if (doKinematic)
                kinematic?.StartKinematic();
            if (useUI)
                CoroutineHandler.ExecuteActionAfter(HideUI, Constants.OneSecondDelay, this);
            if (unlockAbility)
            {
                progressHandle = GameObject.FindGameObjectWithTag(Tags.Player).GetComponentInParent<PlayerController>().ProgressHandle;
                progressHandle.LearnAbility(abilityData, abilityToUnlock, (state) => { });
            }
        }
        isComplete = true;
        polyp.Liberate();
    }
    #endregion
}
