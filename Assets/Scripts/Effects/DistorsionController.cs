using System;
using UnityEngine;

public class DistorsionController : MonoBehaviour
{
    [Header("Idle Settings")]
    [SerializeField] private float distorsionDuration;
    [SerializeField] private float timeBetweenDistorsion;
    [SerializeField] private MaterialPropierty offAlpha;
    [SerializeField] private MaterialPropierty onAlpha;
    [SerializeField] private MaterialPropierty offDistorsion;
    [SerializeField] private MaterialPropierty onDistorsion;
    [SerializeField] private float transitionDuration;
    [Header("Ocean Current Distorsion Settings")]
    [SerializeField] private MaterialPropierty distorsion;
    [SerializeField] private MaterialPropierty maskradious;
    [SerializeField] private MaterialPropierty scale;
    [SerializeField] private MaterialPropierty velocity;
    [SerializeField] private float currentTransitionDuration;
    private bool isInOceanCurrentDistorsion;
    private Camera mainCamera;
    private float lastOrtographicSize;
    private float aspectRatio = 16f / 9f;
    private void Awake()
    {
        onAlpha.Initialize();
        offAlpha.Initialize();
        onDistorsion.Initialize();
        offDistorsion.Initialize();
        distorsion.Initialize();
        maskradious.Initialize();
        scale.Initialize();
        velocity.Initialize();
        onAlpha.SetValue();
        onDistorsion.SetValue();
        CoroutineHandler.ExecuteActionAfter(OffDistorsion, distorsionDuration, this);
        if (mainCamera == null)
            mainCamera = Camera.main;
        aspectRatio = (float)Screen.width / Screen.height;
        transform.localScale = new Vector3(2 * aspectRatio, 2, 2) * mainCamera.orthographicSize;
        lastOrtographicSize = mainCamera.orthographicSize;
    }
    private void Update()
    {
        if (lastOrtographicSize == mainCamera.orthographicSize)
            return;
        transform.localScale = new Vector3(2 * aspectRatio, 2, 2) * mainCamera.orthographicSize;
        lastOrtographicSize = mainCamera.orthographicSize;
    }
    private void OnDistorsion()
    {
        /*if (isInOceanCurrentDistorsion)
            return;*/
        SetDistorsion(onDistorsion, transitionDuration, true, false);
        CoroutineHandler.ExecuteActionAfter(OffDistorsion, distorsionDuration + transitionDuration, this);
    }

    private void OffDistorsion()
    {
        /*if (isInOceanCurrentDistorsion)
            return;*/
        SetDistorsion(offDistorsion, transitionDuration, true, false, false);
        CoroutineHandler.ExecuteActionAfter(OnDistorsion, timeBetweenDistorsion + transitionDuration, this);
    }

    private void SetDistorsion(MaterialPropierty distorsionMaterialPropierty, float transitionDuration, bool idle, bool ignoreAlpha = true, bool alphaOn = true)
    {
        float timer = 0;
        if (alphaOn && !ignoreAlpha)
            onAlpha.SetValue();
        distorsionMaterialPropierty.StartSmoothValue();
        StartCoroutine(CoroutineHandler.ExecuteActionAfterUpdateUntil(() =>
        {
            timer += Time.deltaTime;
            float smoothValue = timer / transitionDuration;
            distorsionMaterialPropierty.SetSmoothValue(smoothValue);
            return timer >= transitionDuration || idle == isInOceanCurrentDistorsion;
        }, () =>
        {
            if (idle && isInOceanCurrentDistorsion)
                return;
            if (!alphaOn && !ignoreAlpha)
                offAlpha.SetValue();
            distorsionMaterialPropierty.SetSmoothValue(1);
        }));
    }

    public void SetDistorsionCurrent(bool activate)
    {
        if (!activate)
        {
            isInOceanCurrentDistorsion = false;
            OffDistorsion();
            maskradious.ResetValue();
            CoroutineHandler.ExecuteActionAfter(() =>
            {
                scale.ResetValue();
                velocity.ResetValue();
            }, Constants.OneSecondDelay, this);
            return;
        }
        isInOceanCurrentDistorsion = true;
        SetDistorsion(maskradious, currentTransitionDuration, false, false);
        CoroutineHandler.ExecuteActionAfter(() =>
        {
            SetDistorsion(distorsion, currentTransitionDuration, false);
            SetDistorsion(scale, currentTransitionDuration, false);
            SetDistorsion(velocity, currentTransitionDuration, false);
        }, Constants.OneSecondDelay, this);
    }
}
