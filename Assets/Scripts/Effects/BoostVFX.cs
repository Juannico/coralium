using NaughtyAttributes;
using UnityEngine;
using UnityEngine.VFX;

public class BoostVFX : BaseVisualEffect
{
    [SerializeField] private Renderer rnderer;
    [SerializeField] private GPUParticleSystem sparks;
    [SerializeField] [ColorUsage(true, true)] private Color effectColor;
    [ReadOnly] [SerializeField] [ColorUsage(true, true)] private Color startMaterialColor;
    [SerializeField] private SetMaterialPropierty disolvePropierty;
    private void Awake()
    {
        startMaterialColor = rnderer.sharedMaterial.color;
    }
    private void Start()
    {
        ResetEffect();
    }
    public override void StartEffect(float duration = 0)
    {
        if (startMaterialColor == null)
            startMaterialColor = rnderer.sharedMaterial.color;
        if (sparks != null)
        {
            sparks.gameObject.SetActive(true);
            sparks.Play();
        }
        rnderer.sharedMaterial.color = effectColor;
        disolvePropierty.ChangePropierty();
        base.StartEffect(duration);

    }
    public override void ResetEffect()
    {
        rnderer.sharedMaterial.color = startMaterialColor;
        disolvePropierty.Reset();
    }
}
