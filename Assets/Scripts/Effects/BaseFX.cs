using System;
using UnityEngine;

public abstract class BaseFX : MonoBehaviour
{
    public Action<BaseFX> onEffectActivated;
    public Action<BaseFX> onEffectDeactivated;

    protected bool effectIsActive = false;

    // Delegate that can be used to know when something happens in the FX. Useful in situations where gameplay needs to wait for a specific stage of the FX.
    public Action<BaseFX, string> onEffectEventTriggered;

    public bool EffectIsActive { get { return  effectIsActive; } }

    private void Awake()
    {
        OnAwake();
    }

    protected virtual void OnAwake()
    {

    }

    private void Start()
    {
        OnStart();
    }

    protected virtual void OnStart()
    {

    }

    public virtual void ActivateFX()
    {
        if (effectIsActive)
        {
            return;
        }

        effectIsActive = true;

        onEffectActivated?.Invoke(this);
    }

    private void Update()
    {
        if (EffectIsActive)
        {
            UpdateFX();
        }
    }

    /// <summary>
    /// Regular Update function but it only runs if the effect is active.
    /// </summary>
    protected virtual void UpdateFX()
    {

    }

    public virtual void DeactivateFX()
    {
        if (!effectIsActive)
        {
            return;
        }

        effectIsActive = false;

        onEffectDeactivated?.Invoke(this);

        // #TODO: Add FX pooling
        Destroy(gameObject);
    }

    protected void SendFxEvent(string eventName)
    {
        onEffectEventTriggered?.Invoke(this, eventName);
    }
}
