using NaughtyAttributes;
using UnityEngine;

public class RadarPointMark : MonoBehaviour
{
    [SerializeField] private bool useMarkUI;
    [ShowIf("useMarkUI")] [SerializeField] private MarkUI markUIprefab;
    [ShowIf("useMarkUI")] [SerializeField] private Vector3 offset;
    private Material mat;
    private MarkUI markUI;
    private MarkUIManager markUIManager;
    private bool isMarking;
    private float duration;
    private float timer;
    private bool autoHide = true;
    private bool onlyUIMark;
    private void Awake()
    {
        mat = GetComponent<Renderer>().material;
        if (!useMarkUI)
            return;
        if (markUIprefab == null)
            throw new System.Exception($" MarkUIprefab is not assigned in GameObject : {gameObject.name}");
        markUIManager = GameManager.Instance.UIManager.MarkUIManager;
        enabled = false;
    }
    private void FixedUpdate()
    {
        if (!isMarking)
            return;
        timer += Time.deltaTime;
        float alphaValue = Mathf.Lerp(0, 1, Mathf.PingPong(timer / duration, 1));
        if (!onlyUIMark)
            mat.SetFloat("_AlphaP", Mathf.Sqrt(alphaValue));
        if (timer > duration * 2 && autoHide)
            enabled = false;
    }
    private void OnDisable()
    {
        if (!Application.isPlaying)
            return;
        if (!isMarking)
            return;
        mat.SetFloat("_AlphaP", 0);
        if (useMarkUI && markUI != null)
            markUI.gameObject.SetActive(false);
        isMarking = false;
        autoHide = false;
    }
    public void Mark(float duration, bool onlyUIMark = false, bool autoHide = true)
    {
        if (isMarking)
        {
            timer -= duration;
            return;
        }
        this.onlyUIMark = onlyUIMark;
        enabled = true;
        this.duration = duration;
        this.autoHide = autoHide;
        timer = 0;
        isMarking = true;
        if (!useMarkUI)
            return;
        markUI = markUIManager.GetMarkUI(markUIprefab);
        markUI.ShowMark(transform, offset);

    }
    public void Unmark()
    {
        enabled = false;
    }
    private void OnDrawGizmosSelected()
    {
        if (!useMarkUI)
            return;
        Gizmos.color = Color.black;
        Gizmos.DrawWireSphere(transform.position + offset, 1);
    }
}
