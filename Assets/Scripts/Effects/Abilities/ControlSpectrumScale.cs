using NaughtyAttributes;
using System;
using System.Collections;
using UnityEngine;

public class ControlSpectrumScale : MonoBehaviour
{
    [SerializeField] private GPUParticleSystem meshParticles;
    private RouteFollower routFollower;
    [SerializeField] private MeshRenderer model;
    [SerializeField] private int rate;
    [Header("Times")]
    [SerializeField] private float tiempoaparicion;
    [SerializeField] private float tiempoespera;
    [SerializeField] private float tiempodesaparicion;
    private void Awake()
    {
        model.transform.localScale = Vector3.zero;
    }
    public void StartAnimation(Action<bool> setLockAbility)
    {
        if (routFollower == null)
            routFollower = GetComponent<RouteFollower>();
        meshParticles.emissionRate.value1 = 0f;
        routFollower.SetVelocityByDuration(tiempoaparicion + tiempoespera + tiempodesaparicion * 1.05f);
        StartCoroutine(Animation("_Alpha", 0, 1, setLockAbility));
    }
#if UNITY_EDITOR
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.O))
            StartAnimation(null);
    }
#endif

    IEnumerator Animation(string floatstring, float inicial, float final, Action<bool> setLockAbility)
    {
        float timer = 0;
        Material mat = model.material;
        meshParticles.gameObject.SetActive(true);
        meshParticles.Play();
        while (timer < tiempoaparicion)
        {
            timer += Time.deltaTime;
            float fixedValue = Mathf.Lerp(inicial, final, timer / tiempoaparicion);
            model.transform.localScale = fixedValue * Vector3.one;
            meshParticles.emissionRate.value1 = fixedValue * rate;
            mat.SetFloat(floatstring, fixedValue);
            yield return null;
        }
        yield return new WaitForSeconds(tiempoespera);
        timer = 0;
        while (timer < tiempodesaparicion)
        {
            timer += Time.deltaTime;
            float fixedValue = Mathf.Lerp(final, inicial, timer / tiempodesaparicion);
            mat.SetFloat(floatstring, fixedValue);
            model.transform.localScale = fixedValue * Vector3.one;
            meshParticles.emissionRate.value1 = fixedValue * rate;
            yield return null;
        }
        setLockAbility(true);
        meshParticles.Stop();
        gameObject.SetActive(false);
        Destroy(gameObject);
    }
}
