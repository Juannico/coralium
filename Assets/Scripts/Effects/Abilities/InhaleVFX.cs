using UnityEngine;
using UnityEngine.VFX;

public class InhaleVFX : BaseVisualEffect
{
    [SerializeField] private GPUParticleSystem InhaleEffect;
    [SerializeField] private float maxSize = 1f;
    [SerializeField] private float minSize = 0.9f;
    private float radius = 1;
    public float Radius
    {
        get => radius;
        set 
        {
            radius = Mathf.Tan(value * Mathf.Deg2Rad);
            InhaleEffect.param1 = radius;
        }
    }
    public float Size
    {
        get => transform.localScale.x;
        set
        {
            transform.localScale = Vector3.one * value;
            InhaleEffect.startSize.value1 = maxSize * value;
            InhaleEffect.startSize.value2 = minSize * value;
            InhaleEffect.center = -transform.forward * value;
            InhaleEffect.extents = Vector3.one * value ;
        }
    }
    public override void StartEffect(float duration = 0)
    {
        InhaleEffect.Play();
    }
    public override void EndEffect()
    {
        InhaleEffect.Stop();
    }
}
