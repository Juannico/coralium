using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public interface IGameObjectHolder
{
    GameObject GetGameObject();
    void SetGameObject(GameObject GO);
}

public class FX_DelayedSparks : BaseFX, IGameObjectHolder
{
    [Header("FX Behavior")]
    [SerializeField] private float duration = 1.0f;

    [Header("FX References")]
    [SerializeField] private Material sourceMaterial;
    [SerializeField] private VisualEffect vfxGraphLoadSparks;
    [SerializeField] private VisualEffect vfxGraphBurst;

    private GameObject modelPrefab = null;

    private float timer = 0.0f;
    private Material instancedMaterial;
    private bool stop;
    private bool burstStopped;

    public override void ActivateFX()
    {
        base.ActivateFX();

        if (modelPrefab == null)
        {
            Debug.Log("FX_DelayedSparks::ActivateFX: modelPrefab is null.");
            DeactivateFX();
            return;
        }

        instancedMaterial = new Material(sourceMaterial);

        transform.localScale = modelPrefab.transform.localScale;
        GenerateMeshes();

        stop = false;
        enabled = true;
        vfxGraphLoadSparks?.Play();
        timer = 0;
        instancedMaterial.SetFloat("_AlphaP", 1);
        burstStopped = false;
    }

    protected override void UpdateFX()
    {
        base.UpdateFX();

        timer += Time.deltaTime;

        instancedMaterial.SetFloat("_HigthligthPower", timer / duration);

        if (timer >= duration)
        {
            DeactivateFX();
            return;
        }
    }

    public override void DeactivateFX()
    {
        base.DeactivateFX();

        instancedMaterial.SetFloat("_AlphaP", 0);
        instancedMaterial.SetFloat("_HigthligthPower", 0);
        timer = 0;
        vfxGraphBurst.Play();
        vfxGraphLoadSparks.Stop();
        vfxGraphBurst.Stop();
        stop = true;
    }

    private void GenerateMeshes()
    {
        Dictionary<Mesh, Vector3> uniqueMeshes = new Dictionary<Mesh, Vector3>();

        MeshFilter[] meshFilters = modelPrefab.GetComponentsInChildren<MeshFilter>();
        SkinnedMeshRenderer[] skMeshRenderers = modelPrefab.GetComponentsInChildren<SkinnedMeshRenderer>();

        for (int i = 0; i < meshFilters.Length; i++)
        {
            MeshFilter currentMeshFilter = meshFilters[i];

            if (!uniqueMeshes.ContainsKey(currentMeshFilter.sharedMesh))
            {
                uniqueMeshes.Add(currentMeshFilter.sharedMesh, currentMeshFilter.transform.localPosition);
            }
        }

        for (int i = 0; i < skMeshRenderers.Length; i++)
        {
            SkinnedMeshRenderer currentSkRenderer = skMeshRenderers[i];

            if (!uniqueMeshes.ContainsKey(currentSkRenderer.sharedMesh))
            {
                uniqueMeshes.Add(currentSkRenderer.sharedMesh, currentSkRenderer.transform.localPosition);
            }
        }

        foreach (KeyValuePair<Mesh, Vector3> kvp in uniqueMeshes)
        {
            GameObject instance = new GameObject(kvp.Key.name + "_Mesh");

            instance.transform.SetParent(transform);
            instance.transform.localPosition = kvp.Value;
            instance.transform.rotation = Quaternion.identity;
            instance.transform.localScale = Vector3.one;

            instance.AddComponent<MeshRenderer>().material = instancedMaterial;
            instance.AddComponent<MeshFilter>().mesh = kvp.Key;
        }
    }

    // IGameObjectHolder
    public GameObject GetGameObject()
    {
        return modelPrefab;
    }

    public void SetGameObject(GameObject GO)
    {
        modelPrefab = GO;
    }
    //~IGameObjectHolder
}