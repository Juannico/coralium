using FMODUnity;
using System.Collections;
using UnityEngine;

public class OndaRadarController : MonoBehaviour
{
    [SerializeField] private float velocidad;
    [SerializeField] private float tiempodevida;
    private Collider selfCollider;
    [SerializeField] private bool IsLocked;
    [SerializeField] private float EnergyCost;
    [Header("SFX")]
    [SerializeField] private StudioEventEmitter ecoSFX;

    void Start()
    {
        this.transform.localScale = Vector3.zero;
        selfCollider = GetComponent<Collider>();
        selfCollider.enabled = false;
    }
    private IEnumerator crecer()
    {
        float crecimiento; 
        float counter = 0;
        selfCollider.enabled = true;
        while (counter < tiempodevida)
        {
            counter += Time.deltaTime;
            crecimiento = velocidad * Time.deltaTime;
            this.transform.localScale = new Vector3(this.transform.localScale.x + crecimiento, this.transform.localScale.y + crecimiento, this.transform.localScale.z + crecimiento);
            yield return null;
        }
        selfCollider.enabled = false;
        this.transform.localScale = Vector3.zero;
    }
    public void StartEcolocalization(Vector3 position) {
        if (IsLocked)
           return;
        ecoSFX.Play();
        transform.position = position;
        this.transform.localScale = Vector3.one * 6;
        StopCoroutine(crecer());
        StartCoroutine(crecer());
    }
}
