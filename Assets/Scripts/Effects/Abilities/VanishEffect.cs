using System.Collections;
using UnityEngine;
using UnityEngine.VFX;

public class VanishEffect : BaseVisualEffect
{
    private Material material;
    private VisualEffect visualEffect;
    private void Awake()
    {
        material = GetComponent<MeshRenderer>().material;
        visualEffect = GetComponentInChildren<VisualEffect>(true);
    }
    public override void StartEffect( float duraction = 0)
    {
        StartCoroutine(Vanishing());
    }

    private IEnumerator Vanishing()
    {
        visualEffect.Play();
        float timer = 0f;
        float alpha = 0f;
        while (timer < duration)
        {
            timer += Time.deltaTime;
            alpha = timer / 2;
            material.SetFloat("_Disolve", alpha);
            yield return null;
        }
        material.SetFloat("_Disolve", 0);
        gameObject.SetActive(false);
    }
}
