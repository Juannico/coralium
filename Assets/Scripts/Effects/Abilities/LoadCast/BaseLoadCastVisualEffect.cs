using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseLoadCastVisualEffect : BaseVisualEffect
{
    public virtual void Initialize(BaseAbilityCast abilityCast) { }
}
