using UnityEngine;
using UnityEngine.VFX;

public class LoadElectricityEffect : BaseLoadCastVisualEffect
{
    [SerializeField] private GPUParticleSystem sparks;
    [SerializeField] private ParticleSystem particlesRays;

    [SerializeField] private float targetGlowP = 1f;
    [SerializeField] private float targetAlpha = 0.9f;
    [SerializeField] private float targetSparksRate = 30;
    [SerializeField] private bool shouldScale = true;
    private Material material;
    private float startGlowP;
    private float startAlpha;
    private float startSparksRate;
    private Coroutine loadCoroutine;
    private Vector3 targetScale;

    private void Awake()
    {
        material = gameObject.GetComponent<Renderer>().material;
        startGlowP = material.GetFloat("_GlowP");
        startAlpha = material.GetFloat("_AlphaP");
        startSparksRate = sparks.emissionRate.value1;
        particlesRays.Stop();
    }
    private void Update()
    {
        timer += Time.deltaTime;
        float fixTime = timer / (duration * 0.9f);
        float glowP = Mathf.Lerp(startGlowP, targetGlowP, fixTime);
        float alpha = Mathf.Lerp(startAlpha, targetAlpha, fixTime);
        float sparksRate = Mathf.Lerp(startSparksRate, targetSparksRate, fixTime);
        material.SetFloat("_GlowP", glowP);
        material.SetFloat("_AlphaP", alpha);
        sparks.emissionRate.value1 = sparksRate;
        if (shouldScale)
            transform.localScale = Vector3.Lerp(targetScale * 0.5f, targetScale, fixTime * 1.5f);
        if(timer > duration * 0.9f && particlesRays.isPlaying)
            particlesRays.Stop();
    }
    public override void StartEffect(float duration = 0)
    {
        if (!gameObject.activeInHierarchy)
            gameObject.SetActive(true);
        targetScale = transform.localScale;
        sparks.effectLength = duration;
        sparks.gameObject.SetActive(true);
        sparks.Play();
        particlesRays.Play();
        this.duration = duration;
        timer = 0;
    }
    public override void EndEffect()
    {
        if (loadCoroutine != null)
            StopCoroutine(loadCoroutine);
        sparks.Stop();
        particlesRays.Stop();
        if(!material)
            material = gameObject.GetComponent<Renderer>().material;
        material.SetFloat("_GlowP", startGlowP);
        material.SetFloat("_AlphaP", startAlpha);
        gameObject.SetActive(false);
        transform.localScale = targetScale;
    }
}
