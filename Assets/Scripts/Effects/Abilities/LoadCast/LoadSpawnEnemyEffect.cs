using System.Collections.Generic;
using UnityEngine;

public class LoadSpawnEnemyEffect : BaseLoadCastVisualEffect
{
    [SerializeField] private Material sourceMaterial;
    [SerializeField] private GPUParticleSystem loadSparks;
    [SerializeField] private GPUParticleSystem burstSparks;
    private Material instancedMaterial;
    private bool stop;
    private bool burstStopped;
    private void Update()
    {
        timer += Time.deltaTime;
        if (timer == 0)
            return;
        if (stop)
        {
            if (timer > 0.1 && !burstStopped)
            {
                burstSparks.Stop();
                burstStopped = true;
            }
            if (timer > 0.25f)
                gameObject.SetActive(false);
            return;
        }
        instancedMaterial.SetFloat("_HigthligthPower", (timer / duration));
    }
    public override void Initialize(BaseAbilityCast abilityCast)
    {
        base.Initialize(abilityCast);
        instancedMaterial = new Material(sourceMaterial);
        // Change for add component and pass mesh
        SpawnBehaviour spawnBehaviour = abilityCast.Data.BehaviourPrefab.GetComponent<SpawnBehaviour>();
        if (spawnBehaviour == null)
            throw new System.Exception($"{abilityCast.Data.BehaviourPrefab} does not {typeof(SpawnBehaviour)} type");
        GameObject gameObject = spawnBehaviour.ObjectToSpawn.GetComponentInChildren<Character>().BaseCharacter.ModelPrefab;
        transform.localScale = gameObject.transform.localScale;
        GenerateMeshes(gameObject);
    }
    private void GenerateMeshes(GameObject gameObject)
    {
        Dictionary<Mesh, Vector3> uniqueMeshes = new Dictionary<Mesh, Vector3>();
        AddMeshes<MeshFilter>(ref uniqueMeshes, gameObject);
        AddMeshes<SkinnedMeshRenderer>(ref uniqueMeshes, gameObject);
        foreach (KeyValuePair<Mesh, Vector3> kvp in uniqueMeshes)
        {
            GameObject instance = new GameObject(kvp.Key.name + "_Mesh");
            instance.transform.SetParent(transform);
            instance.transform.localPosition = kvp.Value;
            instance.transform.localScale = Vector3.one;
            instance.AddComponent<MeshRenderer>().material = instancedMaterial;
            instance.AddComponent<MeshFilter>().mesh = kvp.Key;
        }
    }

    private void AddMeshes<T>(ref Dictionary<Mesh, Vector3> uniqueMeshes, GameObject gameObject)
    {
        T[] list = gameObject.GetComponentsInChildren<T>();
        for (int i = 0; i < list.Length; i++)
        {
            if (list[i] is MeshFilter)
            {
                MeshFilter meshFilters = list[i] as MeshFilter;
                if (!uniqueMeshes.ContainsKey(meshFilters.sharedMesh))
                    uniqueMeshes.Add(meshFilters.sharedMesh, meshFilters.transform.localPosition);
                continue;
            }
            if (!(list[i] is SkinnedMeshRenderer))
                continue;
            SkinnedMeshRenderer skinnedMeshRenderer = list[i] as SkinnedMeshRenderer;
            if (!uniqueMeshes.ContainsKey(skinnedMeshRenderer.sharedMesh))
                uniqueMeshes.Add(skinnedMeshRenderer.sharedMesh, skinnedMeshRenderer.transform.localPosition);
        }
    }
    public override void StartEffect(float duration = 0)
    {
        if (!gameObject.activeInHierarchy)
            gameObject.SetActive(true);
        stop = false;
        enabled = true;
        loadSparks.effectLength = duration;
        loadSparks.gameObject.SetActive(true);
        loadSparks?.Play();
        this.duration = duration;
        timer = 0;
        instancedMaterial.SetFloat("_AlphaP", 1);
        burstStopped = false;
    }
    public override void EndEffect()
    {
        instancedMaterial.SetFloat("_AlphaP", 0);
        timer = 0;
        if (!burstSparks.gameObject.activeInHierarchy)
            burstSparks.gameObject.SetActive(true);
        burstSparks.Play();
        loadSparks.Stop();
        stop = true;
    }
    public override void DisruptEffect()
    {
        instancedMaterial.SetFloat("_AlphaP", 0);
        timer = 0;
        if (!burstSparks.gameObject.activeInHierarchy)
            burstSparks.gameObject.SetActive(true);
        burstSparks.Play();
        loadSparks.Stop();
        burstSparks.Stop();
        stop = true;
    }
}
