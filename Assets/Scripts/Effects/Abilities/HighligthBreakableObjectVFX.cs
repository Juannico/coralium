using UnityEngine;

public class HighligthBreakableObjectVFX : MonoBehaviour
{

    [SerializeField] private SetMaterialProvider setMaterialProvider;
    public Material HigthLighMaterial;
    [HideInInspector] public bool CanDetect = true;
    private void Awake()
    {
        if (setMaterialProvider == null)
            setMaterialProvider = ComponentUtilities.SetComponent<SetMaterialProvider>(gameObject);
    }

    public void AddToHigthLigh()
    {
        if (CanDetect)
            setMaterialProvider.SetMaterial(HigthLighMaterial);
    }
    public void RemoveFromHigthLigh()
    {
        if (CanDetect)
            setMaterialProvider.RemoveMaterial(HigthLighMaterial);
    }
}
