using UnityEngine;
using UnityEngine.VFX;

public class DashEffectController : MonoBehaviour
{
    [SerializeField] private Animator lungeEffectAnimator;
    [SerializeField] private GPUParticleSystem burstBubbles;
    [SerializeField] private float dashBubblesRate = 75;
    [SerializeField] private float lungeBubblesRate = 157;
    [SerializeField] private GameObject trail;
    [SerializeField] private SkinnedMeshRenderer fishRenderer;
    private Material fishMat;
    [SerializeField] private GPUParticleSystem loadSparks;
    [SerializeField] private GPUParticleSystem burstSparks;
    [SerializeField] private float velocidadlineacargado;
    [SerializeField] private GPUParticleSystem lineSpeedEffect;
    [SerializeField] private VisualEffect distorsionEffect;
    [SerializeField] [Range(0, 1)] private float lineSpeedEffectDistanceMultiplier = 0.75f;
    private float lineChargePower;
    private bool loadingLunge;
    void Awake()
    {

        fishMat = fishRenderer.materials[0];
        lineChargePower = fishMat.GetFloat("_PoderLinea");
        fishMat.SetFloat("_PoderLinea", 0);
        trail.SetActive(false);

        if (lineSpeedEffect != null)
            lineSpeedEffect.gameObject.SetActive(false);
        if (distorsionEffect != null)
            distorsionEffect.transform.parent = null;

    }
    public void DisruptCharge()
    {
        SetGameObjectState(loadSparks.gameObject, false);
        fishMat.SetFloat("_VelocidadLinea", 0);
    }
    public void Charge()
    {
        SetGameObjectState(loadSparks.gameObject, true);
        loadSparks?.Play();
        fishMat.SetFloat("_VelocidadLinea", 0);
    }
    public void StopCharge()
    {
        SetGameObjectState(loadSparks.gameObject, false);
        SetGameObjectState(burstSparks.gameObject, true);
        burstSparks.Play();
        fishMat.SetFloat("_VelocidadLinea", velocidadlineacargado);
        fishMat.SetFloat("_PoderLinea", lineChargePower);
    }
    public void Lunge(float delayEffect = 0)
    {
        fishMat.SetFloat("_VelocidadLinea", 0);
        loadingLunge = true;
        lungeEffectAnimator.SetBool("Effect", true);
        lungeEffectAnimator.SetTrigger("StartEffect");
        CoroutineHandler.ExecuteActionAfter(() =>
        {
            if (!loadingLunge)
                return;
            loadingLunge = false;
            burstBubbles.emissionRate.value1 = lungeBubblesRate;
            SetGameObjectState(burstBubbles.gameObject, true);
            burstBubbles.Play();
            trail.SetActive(true);
        }, delayEffect, this);

    }
    public void Dash()
    {
        burstBubbles.emissionRate.value1 = dashBubblesRate;
        SetGameObjectState(burstBubbles.gameObject, true);
        burstBubbles.Play();
        SetGameObjectState(lineSpeedEffect.gameObject, true);
        lineSpeedEffect.Play();
        distorsionEffect.transform.position = transform.position + transform.forward;
        distorsionEffect.transform.forward = transform.forward;
        CoroutineHandler.ExecuteActionAfter(() => distorsionEffect.Play(), 0.1f, this);
        trail.SetActive(true);
    }
    public void StopEffect()
    {
        trail.SetActive(false);
        StopLunge();
        SetGameObjectState(burstBubbles.gameObject, false);
        if (lineSpeedEffect != null)
        {
            CoroutineHandler.ExecuteActionAfter(() =>
            {
                lineSpeedEffect.Stop();
                SetGameObjectState(lineSpeedEffect.gameObject, false);
            }, 0.1f, this);
        }

        if (fishMat == null)
            fishMat = fishRenderer.materials[0];
        fishMat.SetFloat("_PoderLinea", 0);
    }

    private void SetGameObjectState(GameObject objectToSet, bool state)
    {
        if (objectToSet.activeInHierarchy == !state)
            objectToSet.SetActive(state);
    }

    public void StopLunge()
    {
        loadingLunge = false;
        lungeEffectAnimator.SetBool("Effect", false);
        SetGameObjectState(burstBubbles.gameObject, false);
        /*  if (burstBubbles.state == GPUParticleSystem.GPUParticleSystemState.Playing)
              burstBubbles.Stop();*/
        trail.SetActive(false);
    }
    public void SetLoadSparksDuration(float duration) => loadSparks.effectLength = duration;
}
