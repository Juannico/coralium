using System.Collections;
using UnityEngine;
using UnityEngine.VFX;

public class ControlVFXElectricidad : BaseVisualEffect
{
    [SerializeField] private VisualEffect vfxgrraph;
    [SerializeField] private GameObject attackObject;
    [SerializeField] private GameObject model;
    private Material matModel;
    [SerializeField] private ParticleSystem particlesRays;
    private void Awake()
    {
        matModel = model.GetComponent<MeshRenderer>().material;
    }
    public override void StartEffect(float duration)
    {
        if (matModel == null)
            matModel = model.GetComponent<MeshRenderer>().material;
        // model.transform.localScale = Vector3.zero;
        StartCoroutine(StartEffectCoroutine());
        particlesRays.Play();
        vfxgrraph.Play();
        base.StartEffect();
    }
    private IEnumerator StartEffectCoroutine()
    {
        float counter = 0;
        float temp = 0;
        while (counter < duration)
        {
            counter += Time.deltaTime;
            temp = Mathf.Lerp(0, 1, counter / duration);
            matModel.SetFloat("_Alpha", temp);
            yield return null;
        }
        counter = 0;
        while (counter < duration / 2)
        {
            counter += Time.deltaTime;
            temp = Mathf.Lerp(1, 0, counter / (duration / 2));
            matModel.SetFloat("_Alpha", temp);
            yield return null;
        }
    }
}
