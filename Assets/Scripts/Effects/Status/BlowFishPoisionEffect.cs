using UnityEngine;

public class BlowFishPoisionEffect : BaseVisualEffect
{
    [SerializeField] private Material poisionedMaterial;
    [SerializeField] private SkinnedMeshRenderer skinnedMeshRenderer;
    [SerializeField] private SkinnedMeshRenderer skinnedMeshRendererRadarPoint;
    [SerializeField] private float scaleMultiplier = 3;
    [SerializeField][Range(0,1)] private float animSquashMultiplier = 0.75f;
    [SerializeField] private float inflateTrasitionDuration = 0.25f;
    [SerializeField] private float targetInflate = 88;
    private float startInflate;
    private float endInflat;
    private Vector3 startScale;
    private Vector3 endScale;
    private Vector3 baseScale;
    private Vector3 offsetScale;
    private float offsTimerMultiplier;
    private bool stop = true;
    protected SetMaterialProvider setMaterialProvider;
    protected virtual void Awake()
    {
        setMaterialProvider = ComponentUtilities.SetComponent<SetMaterialProvider>(skinnedMeshRenderer.gameObject);
        baseScale = skinnedMeshRenderer.transform.localScale;
        offsetScale = baseScale * scaleMultiplier * animSquashMultiplier;
        skinnedMeshRenderer.SetBlendShapeWeight(0, 100);
        enabled = false;
    }
    private void Update()
    {
        timer += Time.deltaTime;
        if (timer > inflateTrasitionDuration)
        {
            if (stop)
                enabled = false;
            return;
        }
        float fixTimer = Mathf.Pow(timer / inflateTrasitionDuration,2f);
        float inflate = Mathf.Lerp(startInflate, endInflat, fixTimer);
        Vector3 scale = Vector3.Lerp(startScale, endScale, fixTimer);
        Vector3 offsetScale =  Vector3.Lerp(Vector3.zero, this.offsetScale, Mathf.PingPong(Mathf.Pow(fixTimer, offsTimerMultiplier) * 2, 1));
        skinnedMeshRenderer.SetBlendShapeWeight(0, inflate);
        skinnedMeshRendererRadarPoint?.SetBlendShapeWeight(0, inflate);
        skinnedMeshRenderer.transform.localScale = scale + offsetScale;
    }
    public override void StartEffect(float duration = 0)
    {
        enabled = true;
        startInflate = skinnedMeshRenderer.GetBlendShapeWeight(0);
        startScale = skinnedMeshRenderer.transform.localScale;
        endInflat = targetInflate;
        endScale = baseScale * scaleMultiplier;
        timer = 0;
        offsTimerMultiplier = 2;
        stop = false;
        SetPoisioneMaterial();
    }
    public override void EndEffect()
    {
        stop = true;
        startInflate = skinnedMeshRenderer.GetBlendShapeWeight(0);
        startScale = skinnedMeshRenderer.transform.localScale;
        endInflat = 100;
        endScale = baseScale ;
        timer = 0;
        offsTimerMultiplier = 0.5f;
        setMaterialProvider.RemoveMaterial(poisionedMaterial);
    }

    private void SetPoisioneMaterial()
    {
        setMaterialProvider.SetMaterial(poisionedMaterial, (material) =>
        {
            float counter = 0;
            float alpha;
            StartCoroutine(CoroutineHandler.ExecuteActionAfterUpdateUntil(() =>
            {
                counter += Time.deltaTime;
                alpha = Mathf.Lerp(0, 1, counter / inflateTrasitionDuration);
                material.SetFloat("_Alpha", alpha);
                return counter > inflateTrasitionDuration;
            }));
        });
    }

}
