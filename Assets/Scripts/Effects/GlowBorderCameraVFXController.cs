using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class GlowBorderCameraVFXController : BaseVisualEffect
{
    [SerializeField] private GPUParticleSystem glowVFX;
    private Camera mainCamera;
    private float lastOrtographicSize;
    public override void StartEffect(float duration = 0)
    {
        glowVFX.gameObject.SetActive(true);
        glowVFX.Play();
        if (mainCamera == null)
            mainCamera = Camera.main;
        transform.localScale = new Vector3(2, 1, 1) * mainCamera.orthographicSize;
        lastOrtographicSize = mainCamera.orthographicSize;
    }
    private void Update()
    {
        if (lastOrtographicSize == mainCamera.orthographicSize)
            return;
        transform.localScale = new Vector3(2, 1, 1) * mainCamera.orthographicSize;
        lastOrtographicSize = mainCamera.orthographicSize;
    }
    public override void EndEffect()
    {
        if (glowVFX.state == GPUParticleSystem.GPUParticleSystemState.Playing)
            glowVFX.Stop();
        glowVFX.gameObject.SetActive(false);
    }
}
