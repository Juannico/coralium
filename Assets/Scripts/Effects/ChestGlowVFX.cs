using UnityEngine;
using UnityEngine.VFX;

public class ChestGlowVFX : BaseVisualEffect
{
    [SerializeField] private GPUParticleSystem effect;
    [SerializeField] private MeshRenderer mesh;
    private Material mat;
    // Start is called before the first frame update
    void Awake()
    {
        mat = mesh.material;
        gameObject.SetActive(false);
    }
    public override void StartEffect(float duration = 0)
    {
        gameObject.SetActive(true);
        effect.Play();
        CoroutineHandler.ExecuteActionAfter(() =>
        {
            float counter = 0;
            float temp = 1;
            mat.SetFloat("_Alpha", 1);
            StartCoroutine(CoroutineHandler.ExecuteActionAfterUpdateUntil(() =>
            {
                counter += Time.deltaTime;
                temp = Mathf.Lerp(1, 0, counter / 1);
                mat.SetFloat("_Alpha", temp);
                return counter >= duration;
            }));
            base.StartEffect();
        }, delay, this);
    }
}
