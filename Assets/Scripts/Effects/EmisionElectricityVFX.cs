using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EmisionElectricityVFX : BasecVFXObjectElectricity
{
    [SerializeField] private SetMaterialPropierty emissionPropierty;
    [SerializeField] private bool electrigyOtherObject;
    [ShowIf("electrigyOtherObject")] [SerializeField] private BaseStatusEffect otherObject;
    protected override void Awake()
    {
        base.Awake();
        emissionPropierty.Reset();
        if (electrigyOtherObject)
            otherObject.TurnOffAutomaticOffEffect();
    }
    public override void StartEffect(float duration = 0)
    {
        base.StartEffect(duration);
        emissionPropierty.ChangePropierty();
        if (electrigyOtherObject)
            otherObject.OnEffect();
    }
    public override void EndEffect()
    {
        base.EndEffect();
        emissionPropierty.Reset();
        if (electrigyOtherObject)
            otherObject.OffEffect();
    }
}
