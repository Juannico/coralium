using FMODUnity;
using System.Collections;
using UnityEngine;
using UnityEngine.VFX;
//using static UnityEditor.Recorder.OutputPath;

public class FloorDust : MonoBehaviour
{
    [SerializeField] private float distanceToFloor = 1.5f;
    [SerializeField] private LayerMask floorLayer;
    [SerializeField] private TerrainLayer dirtTerrainLayer;
    [SerializeField] private GPUParticleSystem dustVisualEffect;
    [SerializeField] private GPUParticleSystem bubbleVisualEffect;
    [Header("SFX")]
    [SerializeField] private StudioEventEmitter floorDustSFX;
    private float startDustRate;
    private float startBubbleRate;
    private RaycastHit[] raycastHits = new RaycastHit[1];
    private bool shouldDust;
    private Coroutine setVisualEffectRateCourutine;
    private GameObject visualEffectContainer;
    private PlayerController playerController;
    private Terrain currentTerrain;

    private ImpulseStateComponent impulseState;
    private void Awake()
    {
        visualEffectContainer = dustVisualEffect.gameObject.transform.parent.gameObject;
        startDustRate = dustVisualEffect.emissionRate.value1;
        startBubbleRate = bubbleVisualEffect.emissionRate.value1;
        dustVisualEffect.emissionRate.value1 = 0;
        bubbleVisualEffect.emissionRate.value1 = 0;
        //dustVisualEffect.Stop();
        //bubbleVisualEffect.Stop();
        dustVisualEffect.gameObject.SetActive(false);
        bubbleVisualEffect.gameObject.SetActive(false);
        playerController = GetComponent<PlayerController>();

    }
    void Start()
    {
        impulseState = GetComponent<ImpulseStateComponent>();
        if (playerController != null)
            visualEffectContainer.transform.parent = playerController.BaseTransform.transform;
    }
    void Update()
    {
        IsTouchingFloor();
    }
    public void IsTouchingFloor()
    {
        if (!IsMoving())
            return;

        int raycastHitLength = Physics.RaycastNonAlloc(transform.position, -transform.up, raycastHits, distanceToFloor, floorLayer);
        if (raycastHitLength < 1)
        {
            ShouldStopDust(0.1f);
            return;
        }
        if (!raycastHits[0].transform.TryGetComponent(out currentTerrain))
            return;
        if (!IsOverFloorLayer())
        {
            ShouldStopDust(0.05f);
            return;
        }
        ShouldBeDust();
        visualEffectContainer.transform.position = raycastHits[0].point;
    }
    private bool IsMoving()
    {
        if (playerController == null)
            return true;
        if (PlayerInputManager.Instance.InputActions.Player.Movement.ReadValue<Vector2>().magnitude != 0 || impulseState.enabled)
            return true;
        ShouldStopDust(0.5f);
        return false;
    }
    private void ShouldStopDust(float waitTimeToStop)
    {
        if (!shouldDust)
            return;
        shouldDust = false;
        if (setVisualEffectRateCourutine != null)
            StopCoroutine(setVisualEffectRateCourutine);
        setVisualEffectRateCourutine = StartCoroutine(SettingVisualEffectRate(0, 0, false, waitTimeToStop));
        floorDustSFX?.Stop();
    }
    private void ShouldBeDust()
    {
        if (shouldDust)
            return;
        shouldDust = true;
        if (setVisualEffectRateCourutine != null)
            StopCoroutine(setVisualEffectRateCourutine);
        setVisualEffectRateCourutine = StartCoroutine(SettingVisualEffectRate(startDustRate, startBubbleRate, true, 0.05f));
        floorDustSFX?.Play();
    }
    private bool IsOverFloorLayer()
    {
        Vector3 terrainPosition = raycastHits[0].point - currentTerrain.transform.position;
        Vector3 splatMapPosition = new Vector3(terrainPosition.x / currentTerrain.terrainData.size.x, 0, terrainPosition.z / currentTerrain.terrainData.size.z);
        int x = Mathf.FloorToInt(splatMapPosition.x * currentTerrain.terrainData.alphamapWidth);
        int z = Mathf.FloorToInt(splatMapPosition.z * currentTerrain.terrainData.alphamapHeight);
        float[,,] alphaMap = currentTerrain.terrainData.GetAlphamaps(x, z, 1, 1);
        int index = 0;
        for (int i = 1; i < alphaMap.Length; i++)
        {
            if (alphaMap[0, 0, i] > alphaMap[0, 0, index])
                index = i;
        }
        if (currentTerrain.terrainData.terrainLayers.Length <= 0)
            return false;
        return currentTerrain.terrainData.terrainLayers[index] == dirtTerrainLayer;
    }
    IEnumerator SettingVisualEffectRate(float targetDustRate, float targetBubbleRate, bool state, float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        if (state)
        {
            dustVisualEffect.gameObject.SetActive(true);
            bubbleVisualEffect.gameObject.SetActive(true);
            dustVisualEffect.Play();
            bubbleVisualEffect.Play();
        }
        float timer = 0f;
        float startDustRate = dustVisualEffect.emissionRate.value1;
        float startBubbleRate = bubbleVisualEffect.emissionRate.value1;
        while (timer <= 1)
        {
            timer += Time.deltaTime;
            dustVisualEffect.emissionRate.value1 = Mathf.Lerp(startDustRate, targetDustRate, timer);
            bubbleVisualEffect.emissionRate.value1 = Mathf.Lerp(startBubbleRate, targetBubbleRate, timer);
        }
        if (state)
            yield break;
        dustVisualEffect.Stop();
        bubbleVisualEffect.Stop();
        yield return new WaitForSeconds(3f);

        dustVisualEffect.gameObject.SetActive(false);
        bubbleVisualEffect.gameObject.SetActive(false);

    }
}
