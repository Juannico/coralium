using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseAbilityFX : BaseFX
{
    [SerializeField] protected bool activateWhenAbilityStarts = true;
    [SerializeField] protected bool deactivateWhenAbilityEnds = true;

    public Action<BaseAbilityFX, string, AbilityParams> onAbilityEffectEventTriggered;

    protected BaseAbilityDescriptor abilityDescriptor = null;
    protected AbilityParams initialAbilityParams;

    /// <summary>
    /// Triggered once the ability starts.
    /// </summary>
    public virtual void ReceiveAbilityStarted(BaseAbilityDescriptor abilityDescriptor, AbilityParams abilityParams)
    {
        if (!EffectIsActive && activateWhenAbilityStarts)
        {
            this.abilityDescriptor = abilityDescriptor;
            initialAbilityParams = abilityParams;

            ActivateFX();
        }
    }

    /// <summary>
    /// Triggered once a specific event gets fired in the ability. Use this to have the FX react to the gameplay.
    /// </summary>
    /// <param name="eventName"></param>
    /// <param name="eventParams"></param>
    /// <returns> Returns true if the event was handled correctly. </returns>
    public virtual bool ReceiveAbilityEvent(string eventName, AbilityParams eventParams)
    {
        return false;
    }

    /// <summary>
    /// Triggered once the ability ends.
    /// </summary>
    public virtual void ReceiveAbilityEnded()
    {
        if (EffectIsActive && deactivateWhenAbilityEnds)
        {
            DeactivateFX();
        }
    }

    protected void SendAbilityFxEvent(string eventName, AbilityParams eventParams)
    {
        onAbilityEffectEventTriggered?.Invoke(this, eventName, eventParams);

        SendFxEvent(eventName);
    }
}
