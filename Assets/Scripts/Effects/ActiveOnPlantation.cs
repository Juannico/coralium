using UnityEngine;

public class ActiveOnPlantation : MonoBehaviour
{
    protected GrowControl growControl;
    /*[SerializeField] [ColorUsage(false, true)] private Color LigthEmission;
    [SerializeField] private SetMaterialProvider setMaterialProvider;*/

    private void Start()
    {
        growControl = FindObjectOfType<GrowControl>(true);
       /* if (setMaterialProvider == null)
            setMaterialProvider = ComponentUtilities.SetComponent<SetMaterialProvider>(gameObject);*/
        if (growControl.Data.IsPlanted)
        {
            gameObject.SetActive(true);
           // setMaterialProvider.SetMaterial(0, (material) => material.SetColor("_Emission", LigthEmission));
            return;
        }
        growControl.ObjectsToTurnOn.Add(this);
        gameObject.AddComponent<UnHideGameObject>();
        gameObject.SetActive(false);
    }
    private void OnDestroy()
    {
        if (growControl != null)
            growControl.ObjectsToTurnOn.Remove(this);
    }

    public void TurnOn()
    {
        gameObject.SetActive(true);
        //setMaterialProvider.SetMaterial(0, (material) => material.SetColor("_Emission", LigthEmission));
    }

}
