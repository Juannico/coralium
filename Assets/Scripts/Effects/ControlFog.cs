using UnityEngine;

public class ControlFog : MonoBehaviour
{
    [SerializeField] private LevelManager levelManager;

    [SerializeField] private Material material;
    [SerializeField] private float transitionTime = 3;
    public float Distance = 20;
    public float Fallout = 300;
    [ColorUsage(true, true)]
    [SerializeField] private Color color;
    private Coroutine changeColorCoroutine;
    private void Awake()
    {
        if (LevelSceneManager.Instance == null)
        {
            SetStartValues();
            return;
        }
        if (LevelSceneManager.Instance.Initialized)
        {
            ChangeFOG(transitionTime, color);
            return;
        }
        SetStartValues();
    }
    private void SetStartValues()
    {
        if (material == null)
            return;
        if (!levelManager.CurrentLevelData.Spawn.IsSaved)
            material.SetColor("_FogColor", color);
        material.SetFloat("_Distance", Distance);
        material.SetFloat("_FallOut", Fallout);
    }
    public void ChangeFOG(float duration, Color color1, bool onlyChangeColor = false)
    {
        if (changeColorCoroutine != null)
            StopCoroutine(changeColorCoroutine);
        float timer = 0;
        Color startColor = material.GetColor("_FogColor");
        float startDistance = material.GetFloat("_Distance");
        float startFallout = material.GetFloat("_FallOut");
        changeColorCoroutine = StartCoroutine(CoroutineHandler.ExecuteActionAfterUpdateUntil(() =>
            {
                timer += Time.deltaTime;
                material.SetColor("_FogColor", Color.Lerp(startColor, color1, timer / duration));
                if (onlyChangeColor)
                    return timer > duration;
                material.SetFloat("_Distance", Mathf.Lerp(startDistance, Distance, timer / duration));
                material.SetFloat("_FallOut", Mathf.Lerp(startFallout, Fallout, timer / duration));
                return timer > duration;
            }));
    }
    public void ChangeFOGDistance(float startDistance, float targerDistance, float time)
    {
        float timer = 0;
        float distance = startDistance;
        StartCoroutine(CoroutineHandler.ExecuteActionAfterUpdateUntil(() =>
        {
            timer += Time.deltaTime;
            distance = Mathf.Lerp(startDistance, targerDistance, timer / time);
            Distance = distance;
            material.SetFloat("_Distance", Distance);
            return timer > time;
        }));
    }
    private void OnApplicationQuit()
    {
        if (material == null)
            return;
        material.SetFloat("_Distance", 3000);
    }
}
