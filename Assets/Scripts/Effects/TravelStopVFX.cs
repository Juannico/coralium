using UnityEngine;
using UnityEngine.VFX;

public class TravelStopVFX : BaseVisualEffect
{
    [SerializeField] private SetMaterialProvider materialProvider;
    [SerializeField] private GPUParticleSystem particleForm;
    [SerializeField] private RouteFollower travelRout;
    [SerializeField] private float routDurationMultiplier = 2;
    [HideInInspector] public bool IsTraveling;
    private void Start()
    {
        travelRout.ShouldMove = false;
    }
    public override void ResetEffect()
    {
        particleForm.Play();
        materialProvider.SetMaterial(0, (material) => material.SetFloat("_Disolve", 1));
        travelRout.SetEnd();
        particleForm.transform.parent = null;
        // travelRout.SetStart();
    }
    public override void StartEffect(float duration = 0)
    {

        if (duration == 0)
            duration = this.duration;
        StartCoroutine(CoroutineHandler.ExecuteActionAfterUpdateUntil(() =>
        {
            timer += Time.deltaTime;
            materialProvider.SetMaterial(0, (material) => material.SetFloat("_Disolve", Mathf.SmoothStep(1, 0, timer / duration)));
            return timer > duration;
        }));
        float routDuration = duration * routDurationMultiplier * 0.99f;
        travelRout.SetVelocityByDuration(routDuration);
        travelRout.Reset(false);
        CoroutineHandler.ExecuteActionAfter(() =>
        {
            travelRout.ShouldMove = false;
            travelRout.SetEnd();
        }, routDuration, this);
        CoroutineHandler.ExecuteActionAfter(() => particleForm.Stop(), routDuration, this);

    }
}
