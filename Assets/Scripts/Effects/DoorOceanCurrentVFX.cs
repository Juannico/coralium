using UnityEngine;
using UnityEngine.VFX;

public class DoorOceanCurrentVFX : BaseVisualEffect
{
    [Header("Door Settings")]
    [SerializeField] private Transform[] doors;
    [SerializeField] private GlowBorderCameraVFXController cameraBorderGlow;
    private void Awake()
    {
        Camera cameraMain = Camera.main;
        GlowBorderCameraVFXController glowBorderCameraVFXController = cameraMain.GetComponentInChildren<GlowBorderCameraVFXController>(true);
        if (glowBorderCameraVFXController != null)
        {
            DestroyImmediate(cameraBorderGlow.gameObject);
            cameraBorderGlow = glowBorderCameraVFXController;
            return;
        }
        cameraBorderGlow.transform.parent = cameraMain.transform;
        cameraBorderGlow.transform.localRotation = Quaternion.Euler(Vector3.right * 90);
        cameraBorderGlow.transform.localPosition = Vector3.forward;
        EndEffect();
    }
    private void Start()
    {
        cameraBorderGlow.gameObject.SetActive(false);
    }
    public override void StartEffect(float duration = 0)
    {
        if (!cameraBorderGlow.gameObject.activeInHierarchy)
            cameraBorderGlow.gameObject.SetActive(true);
        cameraBorderGlow.StartEffect();
        foreach (Transform door in doors)
            door.localScale *= -1;
    }
    public override void EndEffect()
    {
        cameraBorderGlow.EndEffect();
        foreach (Transform door in doors)
            door.localScale *= -1;
    }
}
