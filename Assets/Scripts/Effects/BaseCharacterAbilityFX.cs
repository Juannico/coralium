﻿public abstract class BaseCharacterAbilityFX : BaseAbilityFX
{
    protected Character owningCharacter;

    public void SetOwningCharcater(Character owningCharacter)
    {
        this.owningCharacter = owningCharacter;
    }
}
