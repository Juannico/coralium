using UnityEngine;
using UnityEngine.VFX;

public class BasecVFXObjectElectricity : BaseVisualEffect
{
    [Header("VFX")]
    [SerializeField] protected GameObject model;
    [SerializeField] private Material electrocutedMaterial;
    [SerializeField] private GPUParticleSystem shockParticle = null;
    [SerializeField] private ParticleSystem particlesRays = null;
    [SerializeField] private GameObject ligth = null;
    protected SetMaterialProvider setMaterialProvider;
    protected virtual void Awake()
    {
        if (model != null)
            setMaterialProvider = ComponentUtilities.SetComponent<SetMaterialProvider>(model);
        if (ligth != null)
            ligth.SetActive(false);
    }
    public override void StartEffect(float duration = 0)
    {
        if (ligth != null)
            ligth?.SetActive(true);
        particlesRays?.Play();
        if (!shockParticle.gameObject.activeInHierarchy)
            shockParticle.gameObject.SetActive(true);
        shockParticle?.Play();
        if (electrocutedMaterial == null || model == null)
            return;
        setMaterialProvider.SetMaterial(electrocutedMaterial, (material) =>
        {
            float counter = 0;
            float alpha;
            StartCoroutine(CoroutineHandler.ExecuteActionAfterUpdateUntil(() =>
            {
                counter += Time.deltaTime;
                alpha = Mathf.Lerp(0, 1, counter / 0.5f);
                material.SetFloat("_Alpha", alpha);
                return counter > 0.5f;
            }));
        });
        base.StartEffect();
    }
    public override void EndEffect()
    {
        if (ligth != null)
            ligth?.SetActive(false);
        if (electrocutedMaterial != null && model != null)
            setMaterialProvider.RemoveMaterial(electrocutedMaterial);
    }
}
