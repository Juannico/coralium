using UnityEngine;

public class ChangeEmision : MonoBehaviour
{
    private Material material;
    private Color baseColorEmision;
    private void Awake()
    {
        material = GetComponentInChildren<Renderer>(true).material;
        baseColorEmision = material.GetColor("_ColorEmission");
    }

    public void SetEmision(Color newColor)
    {
        material.SetColor("_ColorEmission", newColor);
    }

    public void RestarEmission()
    {
        material.SetColor("_ColorEmission", baseColorEmision);
    }
}
