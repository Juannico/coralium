using Cinemachine;
using FMODUnity;
using System.Collections;
using System.Collections.Generic;
using TamarilloTools;
using UnityEngine;
using UnityEngine.Events;

public class GrowControl : MonoBehaviour
{
    [SerializeField] private int tutorialIndexToShowAfterPlant;
    [SerializeField] private int powerPromptIndex;
    [SerializeField] private GameObject[] coralsmeshes;
    // Tiempo que se demora creciendo cada coral
    [SerializeField] private float timeToGrow = 1;
    [SerializeField] private float timeTransition;
    [SerializeField] private Vector2 offsetVector;
    [SerializeField] private CinemachineSmoothPath cinemachineSmoothPath;
    private KinematicsHandler kinematicsHandler;

    [SerializeField] private ControlFog controlfog;
    [SerializeField] private float FogTransitionDistance;
    [SerializeField] private GameObject objectToHide;
    private float fogBaseDistance;

    [SerializeField] private StudioEventEmitter plantPanningSFX;

    [SerializeField] private UnityEvent plantEvent;

    [HideInInspector] public GrowCoralData Data;
    private SetMaterialProvider[] coralsMateiralProvider;
    [HideInInspector] public List<ActiveOnPlantation> ObjectsToTurnOn = new List<ActiveOnPlantation>();
    private void Awake()
    {
        kinematicsHandler = GetComponent<KinematicsHandler>();
        coralsMateiralProvider = new SetMaterialProvider[coralsmeshes.Length];
        for (int i = 0; i < coralsmeshes.Length; i++)
            coralsMateiralProvider[i] = ComponentUtilities.SetComponent<SetMaterialProvider>(coralsmeshes[i]);
    }
    private void Start()
    {
        controlfog = GameObject.FindGameObjectWithTag(Tags.ControlFOG).GetComponent<ControlFog>();
    }

    public void SetCoralState(bool state)
    {
        for (int i = 0; i < coralsMateiralProvider.Length; i++)
        {
            coralsMateiralProvider[i].SetMaterial(0, (material) => material.SetFloat("Vector1_Grow", state ? 1 : 0));
            coralsMateiralProvider[i].gameObject.SetActive(state);
        }
        if (state)
            plantEvent?.Invoke();
        else
        {
            // if (Data.AbilityToUnlock.Ability.IsLocked)
            Data.AbilityToUnlock.Initialize();
        }
    }

    public void StartToGrow(bool planted)
    {
        if (!planted)
            PlayerInputManager.Instance.SetInputModeNone();
        Grow(planted);
        if (planted)
            return;
        CoroutineHandler.ExecuteActionAfter(() => kinematicsHandler.StartSmoothPathCinematic(cinemachineSmoothPath, timeTransition, timeToGrow), Constants.halfSecondDelay, this);

        FadeUI.FadeOut();
        CoroutineHandler.ExecuteActionAfter(() => FadeUI.FadeIn(), Constants.halfSecondDelay + 1f * 2, this);
    }
    public void Grow(bool planted)
    {
        if (planted)
        {
            for (int i = 0; i < coralsMateiralProvider.Length; i++)
            {
                coralsMateiralProvider[i].SetMaterial(0, (material) => material.SetFloat("Vector1_Grow", 1));
                coralsMateiralProvider[i].gameObject.SetActive(true);
            }
            return;
        }
        if (controlfog == null)
            controlfog = GameObject.FindGameObjectWithTag(Tags.ControlFOG)?.GetComponent<ControlFog>();
        fogBaseDistance = controlfog.Distance;
        controlfog?.ChangeFOGDistance(fogBaseDistance, FogTransitionDistance, 2);
        for (int i = 0; i < coralsMateiralProvider.Length; i++)
        {
            coralsMateiralProvider[i].SetMaterial(0, (material) => StartCoroutine(Growing(material)));
            coralsMateiralProvider[i].gameObject.SetActive(true);
        }
        for (int i = 0; i < ObjectsToTurnOn.Count; i++)
            ObjectsToTurnOn[i].TurnOn();
        CoroutineHandler.ExecuteActionAfter(FinishGrow, timeToGrow + timeTransition, this);
    }
    public void SetHideObject()
    {
        if (objectToHide != null)
            objectToHide.SetActive(false);
    }
    IEnumerator Growing(Material matp)
    {
        yield return new WaitForSeconds(timeTransition);
        float timer = 0;
        float attrvalue;
        plantPanningSFX?.Play();
        while (timer < timeToGrow)
        {
            timer += Time.deltaTime;
            attrvalue = Mathf.Lerp(0, 1, timer / timeToGrow);
            matp.SetFloat("Vector1_Grow", attrvalue);
            yield return null;
        }
        matp.SetFloat("Vector1_Grow", 1);
    }
    public void FinishGrow()
    {
        controlfog?.ChangeFOGDistance(FogTransitionDistance, fogBaseDistance, 2);
        kinematicsHandler.StopSmoothPathCinematic();
        CoroutineHandler.ExecuteActionAfter(() =>
        {
            Data.ProgressHandle.LearnAbility(Data.abilityData, Data.AbilityToUnlock, (state) => plantEvent?.Invoke());
        }, Constants.OneSecondDelay + FadeUI.GetFadeDuration() * 2, this);
        if (objectToHide != null)
            objectToHide.SetActive(false);
    }
}
