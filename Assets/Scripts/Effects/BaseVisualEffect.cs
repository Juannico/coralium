using UnityEngine;

public class BaseVisualEffect : MonoBehaviour
{
    [Header("Base Settings")]
    [SerializeField] protected float duration = 1;
    [SerializeField] protected float delay = 1;
    protected float timer;
    public virtual void StartEffect(float duration = 0)
    {
        if (duration != 0)
            CoroutineHandler.ExecuteActionAfter(() => EndEffect(), duration, this);
    }
    public virtual void EndEffect() { }
    public virtual void ResetEffect() { }
    public virtual void DisruptEffect() { }

    public float GetDuration() => duration;
}
