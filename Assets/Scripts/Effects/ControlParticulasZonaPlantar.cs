using FMODUnity;
using UnityEngine;
using UnityEngine.VFX;

public class ControlParticulasZonaPlantar : MonoBehaviour
{
    [SerializeField] private VisualEffect vfxgrraph;
    [SerializeField] private StudioEventEmitter plantSFX;

    private void OnEnable()
    {
        plantSFX?.Play();
    }
    public void Vanish()
    {
        plantSFX?.Stop();
        if (vfxgrraph == null)
            return;
        vfxgrraph.SetInt("Rate", 0);
        vfxgrraph.SetInt("RatePiso", 0);
    }
}
