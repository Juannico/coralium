using NaughtyAttributes;
using UnityEngine;

public class LightController : MonoBehaviour
{
    [SerializeField] [Expandable] private LevelManager levelManager;
    [Header("Light Settings")]
    [SerializeField] [ColorUsage(true)] private Color targetFilterColor;
    [SerializeField] [Range(1500, 20000)] private float targetTemperature;
    [SerializeField] private float targetIntensity = 1;
    [SerializeField] private float transitionDuration;
    [SerializeField] private float enviromentIntensityMultiplier = 1;
    [Header("Guide Settings")]
    [SerializeField] private Color rayColor;
    [SerializeField] private float rayDistance = 5;

    private Quaternion startRotation;
    private Color startColor;
    private float startTemperature;
    private float startIntensity;
    private float startEnviromentIntensityMultiplier;
    private float timer = 0;

    private bool lookingForGeneralLigth = true;
    private void Awake()
    {
        if (debugLight != null)
            Destroy(debugLight.gameObject);
        if (lookingForGeneralLigth)
            SetGeneralLigth();
    }
    private void SetGeneralLigth()
    {
        if (levelManager.GeneralLight == null)
        {
            GameObject generalLightObject = GameObject.FindGameObjectWithTag(Tags.GeneralLight);
            if (generalLightObject == null)
                return;

            levelManager.GeneralLight = generalLightObject.GetComponent<Light>();
            levelManager.GeneralLight.color = targetFilterColor;
            levelManager.GeneralLight.colorTemperature = targetTemperature;
            levelManager.GeneralLight.transform.rotation = transform.rotation;
            levelManager.GeneralLight.intensity = targetIntensity;
            RenderSettings.ambientIntensity = enviromentIntensityMultiplier;
            levelManager.CurretnAmbientIntensity = RenderSettings.ambientIntensity;
            lookingForGeneralLigth = false;
            gameObject.SetActive(false);
            return;
        }
        startColor = levelManager.GeneralLight.color;
        startTemperature = levelManager.GeneralLight.colorTemperature;
        startRotation = levelManager.GeneralLight.transform.rotation;
        startIntensity = levelManager.GeneralLight.intensity;
        RenderSettings.ambientIntensity = levelManager.CurretnAmbientIntensity;
        startEnviromentIntensityMultiplier = RenderSettings.ambientIntensity;

        lookingForGeneralLigth = false;
    }

    private void Update()
    {
        if (lookingForGeneralLigth)
        {
            SetGeneralLigth();
            return;
        }
        timer += Time.deltaTime;
        levelManager.GeneralLight.color = Vector4.Lerp(startColor, targetFilterColor, timer / transitionDuration);
        levelManager.GeneralLight.colorTemperature = Mathf.Lerp(startTemperature, targetTemperature, timer / transitionDuration);
        levelManager.GeneralLight.transform.rotation = Quaternion.Lerp(startRotation, transform.rotation, timer / transitionDuration);
        levelManager.GeneralLight.intensity = Mathf.Lerp(startIntensity, targetIntensity, timer / transitionDuration);
        RenderSettings.ambientIntensity = Mathf.Lerp(startEnviromentIntensityMultiplier, enviromentIntensityMultiplier, timer / transitionDuration);
        if (timer <= transitionDuration)
            return;
        levelManager.GeneralLight.color = targetFilterColor;
        levelManager.GeneralLight.colorTemperature = targetTemperature;
        levelManager.GeneralLight.transform.rotation = transform.rotation;
        levelManager.GeneralLight.intensity = targetIntensity;
        RenderSettings.ambientIntensity = enviromentIntensityMultiplier;
        levelManager.CurretnAmbientIntensity = RenderSettings.ambientIntensity;
        gameObject.SetActive(false);
    }
    private Light debugLight;

#if UNITY_EDITOR
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = rayColor;
        Gizmos.DrawRay(transform.position, transform.forward * rayDistance);
    }
    [Button("Debug Light", enabledMode: EButtonEnableMode.Editor)]
    private void DebuLight()
    {
        if (debugLight == null)
        {
            debugLight = new GameObject("Debug Directional Light").AddComponent<Light>();
            debugLight.type = LightType.Directional;
            debugLight.useColorTemperature = true;
            debugLight.intensity = 1;
        }
        debugLight.color = targetFilterColor;
        debugLight.colorTemperature = targetTemperature;
        debugLight.transform.rotation = transform.rotation;
        debugLight.intensity = targetIntensity;

    }
    [Button("Remove Debug Light", enabledMode: EButtonEnableMode.Editor)]
    private void RemoveDebuLight()
    {
        if (debugLight != null)
            DestroyImmediate(debugLight.gameObject);
    }
#endif
}
