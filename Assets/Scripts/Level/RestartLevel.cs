using UnityEngine;
using UnityEngine.SceneManagement;

public class RestartLevel : MonoBehaviour
{
    [HideInInspector] public LevelManager LevelManager;

    public void Restart() 
    {
        LevelManager.LastSpawmLevelData.ZonesLoaded = LevelManager.LastSpawmLevelData.Spawn.ZonesLoadedSaved;
        LevelManager.CurrentLevelData.UnloadLevel();
        LevelManager.Save();
        Destroy(LevelSceneManager.Instance.gameObject);
        LoadingScreenManager.LoadSceneByName(LevelManager.LastSpawmLevelData.LevelSceneName, LoadSceneMode.Single,true);
    }
}
