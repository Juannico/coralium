using NaughtyAttributes;
using UnityEngine;

public class Zone : MonoBehaviour
{
    [SerializeField] private int index;
    [SerializeField] [Scene] private string[] scenesZone;
    [field: SerializeField] public bool LoadOnTravel { get; private set; } = true;
    private LevelManager levelManager;
    private ZoneManager zoneManager;
    private LoadSceneUtility loadSceneUtility;
    [Header("FogZonce")]
    [SerializeField] private bool changeFogColor;
    [ShowIf("changeFogColor")] [SerializeField] private ControlFog controlFog;
    [ShowIf("changeFogColor")] [ColorUsage(true, true)] [SerializeField] private Color colorFog1;
    [ShowIf("changeFogColor")] [SerializeField] private float transitionDuration = 1.5f;

    public float ZonesLength => scenesZone.Length;
    private void Awake()
    {
        loadSceneUtility = new LoadSceneUtility();
    }
    /// <summary>
    /// Sets up the zone with references to the level manager and zone manager.
    /// </summary>
    public void SetupZone(LevelManager levelManager, ZoneManager zoneManager)
    {
        this.levelManager = levelManager;
        this.zoneManager = zoneManager;
    }
    /// <summary>
    /// Loads the scenes associated with the zone.
    /// </summary>
    /// <param name="addToScreenManager">Use true when the zone is loaded in loading screen</param>
    public void Load(bool addToScreenManager = false)
    {
        if (loadSceneUtility == null)
            loadSceneUtility = new LoadSceneUtility();
        for (int i = 0; i < scenesZone.Length; i++)
        {
            AsyncOperation operation = loadSceneUtility.LoadScene(scenesZone[i]);
            if (addToScreenManager)
                LoadingScreenManager.AddOperation(operation);
        }
        if (zoneManager.IsLoadComplete)
            levelManager.CurrentLevelData.ZonesLoaded[index] = true;
        if (addToScreenManager || !changeFogColor)
            return;
        controlFog.ChangeFOG(transitionDuration, colorFog1, true);
    }
    /// <summary>
    /// Unloads the scenes associated with the zone.
    /// </summary>
    public void Unload()
    {
        for (int i = 0; i < scenesZone.Length; i++)
            loadSceneUtility.UnloadScene(scenesZone[i]);
        if (zoneManager.IsLoadComplete)
            levelManager.CurrentLevelData.ZonesLoaded[index] = false;
    }
}
