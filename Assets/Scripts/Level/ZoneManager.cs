using NaughtyAttributes;
using UnityEngine;

public class ZoneManager : MonoBehaviour
{
    [SerializeField] private LevelManager levelManager;
    [SerializeField] [Scene] private string[] defaultScenes;
    [SerializeField] private Zone defaultZone;
    [SerializeField] private Zone[] zones;
    [SerializeField] private SetUpSceneDebug setUpSceneDebug;
    public bool IsLoadComplete { get; private set; }
    private LoadSceneUtility loadSceneUtility;
    private bool isZoneSetup;
    private bool traveled;
    [HideInInspector] public int AmountOfZonesToload;
    private float loadZoneProgress = 0;
    [HideInInspector] public float LoadZoneProgressFixed;
    public float LoadZoneProgress
    {
        get => loadZoneProgress;
        set
        {
            loadZoneProgress = value;
            LoadZoneProgressFixed = value / (progressLength);
        }
    }
    private float progressLength = 1.1f;
    private void Awake()
    {
        LoadingScreenManager.ZoneManager = this;
        levelManager.ZoneManager = this;
        traveled = levelManager.IsTraveling;
        IsLoadComplete = false;
        isZoneSetup = levelManager.CurrentLevelData.ZonesLoaded.Count > 0;
       
        for (var i = 0; i < zones.Length; i++)
        {
            zones[i].SetupZone(levelManager, this);
            if (isZoneSetup)
            {
                if (levelManager.CurrentLevelData.ZonesLoaded[i])
                    zones[i].Load();
                continue;
            }
            levelManager.CurrentLevelData.ZonesLoaded.Add(true);
        }
        LoadZoneProgress = 0;
        AmountOfZonesToload = 1;
        progressLength = 0;
        progressLength += defaultZone.ZonesLength;

    }
    private void Start()
    {
        defaultZone.SetupZone(levelManager, this);
        defaultZone.Load(true);
        SoundManager.Instance.MusicManager.SetMusicParameter(levelManager.CurrentLevelData.eventEmitterData);
    }
    private void OnDestroy()
    {
        LoadingScreenManager.ZoneManager = null;
    }
    private void LoadZone(int index)
    {
        zones[index].Load(true);
    }
    public void UnLoadZone(int index)
    {
        zones[index].Unload();
    }
    /// <summary>
    ///Unloads the scenes associated with the level.
    /// </summary>
    public void UnloadLevel()
    {
        IsLoadComplete = false;
        levelManager.CurrentLevelData.UnloadLevel();
        defaultZone.Unload();
        for (var i = 0; i < zones.Length; i++)
        {
            if (levelManager.CurrentLevelData.ZonesLoaded[i])
                UnLoadZone(i);
        }
        for (int i = 0; i < defaultScenes.Length; i++)
            loadSceneUtility.UnloadScene(defaultScenes[i]);
    }
    /// <summary>
    /// Check if all scenes are loaded and Setup each zone when all sceenes is loaded.
    /// </summary>
    public void CompletLoadZone()
    {
        if (IsLoadComplete)
            return;
        AmountOfZonesToload--;
        if (AmountOfZonesToload > 0)
            return;
        IsLoadComplete = true;
        for (var i = 0; i < zones.Length; i++)
            SetUpZone(i);
        if(!isZoneSetup)
            setUpSceneDebug?.Setup();
    }

    private void SetUpZone(int index)
    {
        if (isZoneSetup)
        {
            if (!levelManager.CurrentLevelData.ZonesLoaded[index])
                UnLoadZone(index);
            return;
        }
        if (traveled && zones[index].LoadOnTravel)
            return;
        UnLoadZone(index);
    }
}
