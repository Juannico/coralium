using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using NaughtyAttributes;
using UnityEditor;
#if (UNITY_EDITOR)
[ExecuteInEditMode]
#endif
public class SetUpScene : MonoBehaviour
{
    [InfoBox("Don't forget to hide SceneObject with the button 'Hide' before closing the scene", EInfoBoxType.Warning)]
    [SerializeField] private GameObject sceneGO;
    [SerializeField] private int amountOfObecjectPerFrame;
    [SerializeField] private LevelManager levelManager;

    private Transform[] childSceneGo;
    private int childCounter = 0;
    private float progressStep;
    private float test;
    private void Awake()
    {
#if (UNITY_EDITOR)
        if (!EditorApplication.isPlaying)
            return;
        if (levelManager == null)
            levelManager = AssetDatabase.LoadAssetAtPath<LevelManager>("Assets/ScriptableObjects/Level/LevelManager.asset");
#endif

        sceneGO.SetActive(false);
        childSceneGo = sceneGO.transform.GetComponentsInChildren<Transform>(true);
        if (sceneGO.activeInHierarchy)
        {
            for (var i = 0; i < childSceneGo.Length; i++)
                childSceneGo[i].gameObject.SetActive(false);
        }
        childCounter = 0;
        sceneGO.SetActive(true);
        progressStep = (float)amountOfObecjectPerFrame / childSceneGo.Length;
    }
    private void Start()
    {
#if (UNITY_EDITOR)
        if (!EditorApplication.isPlaying)
            return;
#endif
    }
    private void Update()
    {
#if (UNITY_EDITOR)
        if (!EditorApplication.isPlaying)
            return;
#endif
        for (var i = 0; i < amountOfObecjectPerFrame && childCounter < childSceneGo.Length; i++)
        {
            if (childSceneGo[childCounter] == null)
            {
                childCounter++;
                continue;
            }
            if (!childSceneGo[childCounter].TryGetComponent(out UnHideGameObject unHideGameObject))
                childSceneGo[childCounter].gameObject.SetActive(true);
            childCounter++;
        }
        levelManager.ZoneManager.LoadZoneProgress += progressStep;
        test += progressStep;
        if (childCounter < childSceneGo.Length)
            return;
        test -= progressStep;
        levelManager?.ZoneManager.CompletLoadZone();
        gameObject.SetActive(false);
    }
    #region Debug
#if (UNITY_EDITOR)
    [Button("Hide", enabledMode: EButtonEnableMode.Editor)]
    public void HideAll()
    {
        Transform[] childSceneGo = sceneGO.transform.GetComponentsInChildren<Transform>(true);
        for (var i = 0; i < childSceneGo.Length; i++)
        {
            if (childSceneGo[i] != null)
                childSceneGo[i].gameObject.SetActive(false);
        }
    }
    [Button("UnHide", enabledMode: EButtonEnableMode.Editor)]
    private void UnhideAll()
    {
        Transform[] childSceneGo = sceneGO.transform.GetComponentsInChildren<Transform>(true);
        for (var i = 0; i < childSceneGo.Length; i++)
        {
            //if (!childSceneGo[i].TryGetComponent(out UnHideGameObject unHideGameObject))
            childSceneGo[i].gameObject.SetActive(true);
        }
    }
    [Button("Count", enabledMode: EButtonEnableMode.Editor)]
    private void CountChildAmount()
    {
        Transform[] childSceneGo = sceneGO.transform.GetComponentsInChildren<Transform>(true);
        Debug.Log($"Child amount: {childSceneGo.Length}. Frame duration: {Mathf.Ceil(childSceneGo.Length / (float)amountOfObecjectPerFrame)}");
    }
#endif
    #endregion
}
