using NaughtyAttributes;
using System;
using UnityEngine;

public class LevelSceneManager : MonoBehaviour
{
    [SerializeField] private int levelIndex;
    [SerializeField] [Expandable] private LevelManager levelManager;
    [SerializeField] [Expandable] private GameData gameData;
    [SerializeField] [Scene] private string[] defaultScenes;
    public static LevelSceneManager Instance { get; private set; }

    public Action<int> OnLoadZone;
    public Action<int> OnUnLoadZone;
    private LoadSceneUtility loadSceneUtility;
    public bool Initialized;
    private void Awake()
    {
        levelManager.LevelIndex = levelIndex;
        levelManager.Save();
        if (Instance != null)
        {
            Destroy(gameObject);
            return;
        }
        DontDestroyOnLoad(this.gameObject);
        Instance = this;
#if UNITY_EDITOR
        if (DataManager.Initialize(gameData))
            DataManager.LoadData(1);

#endif
        DataManager.Update(gameData);
        gameObject.AddComponent<RestartLevel>().LevelManager = levelManager;
        SetPlayerPosition();
        loadSceneUtility = new LoadSceneUtility();
        LoadScenes();
    }
    private void Start()
    {
        if (levelManager.CurrentLevelData.Spawn.IsSaved && !levelManager.TravelingWithDebug)
            return;
        levelManager.TravelingWithDebug = false;
       
        Initialized = true;
    }
    public void LoadScenes()
    {
        for (int i = 0; i < defaultScenes.Length; i++)
            loadSceneUtility.LoadScene(defaultScenes[i]);
    }

    private void SetPlayerPosition()
    {
        if (!levelManager.CurrentLevelData.Spawn.IsSaved || levelManager.TravelingWithDebug)
            return;
        Vector3 targetPosition = levelManager.CurrentLevelData.Spawn.CheckPointPosition;
        Vector3 targetForward = levelManager.CurrentLevelData.Spawn.CheckPointDirection;
        SetPlayerPosition setPlayerPosition = new SetPlayerPosition();
        setPlayerPosition.SetPosition(targetPosition, targetForward, levelManager);
    }
}
