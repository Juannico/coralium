using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadSceneUtility
{ 
    public AsyncOperation LoadScene(string sceneName)
    {
        if (!SceneManager.GetSceneByName(sceneName).isLoaded)
            return SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);
        return null;
    }
    public AsyncOperation UnloadScene(string sceneName)
    {
        if (SceneManager.GetSceneByName(sceneName).isLoaded)
            return SceneManager.UnloadSceneAsync(sceneName);
        return null;
    }


}
