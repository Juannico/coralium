using UnityEngine;

public class SwitchZones : MonoBehaviour
{
    [SerializeField] private Zone zone1ToSwitch;
    [SerializeField] private Zone zone2ToSwitch;

    public void SwitchToZone1()
    {
        zone1ToSwitch?.Load();
        zone2ToSwitch?.Unload();
    }

    public void SwitchToZone2()
    {
        zone2ToSwitch?.Load();
        zone1ToSwitch?.Unload();
    }
}
