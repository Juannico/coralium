using NaughtyAttributes;
using UnityEngine;

public class SwitchLevels : MonoBehaviour
{
    [SerializeField] private LevelManager levelManager;
    [SerializeField][Scene] private string levelOne;
    [SerializeField][Scene] private string levelTwo;
    private LoadSceneUtility loadSceneUtility;
    private void Awake()
    {
        loadSceneUtility = new LoadSceneUtility();
    }
    public void SwitchLevelTwoToOne()
    {
        SwitchLevel(levelOne, levelTwo);
    }

    public void SwitchLevelOneToTwo()
    {
        SwitchLevel(levelTwo, levelOne);
    }

    private void SwitchLevel(string levelToLoad, string levelToUnload)
    {
        if (loadSceneUtility == null)
            loadSceneUtility = new LoadSceneUtility();
        levelManager.ZoneManager.UnloadLevel();
        loadSceneUtility.LoadScene(levelToLoad);
        loadSceneUtility.UnloadScene(levelToUnload);
    }

}
