using NaughtyAttributes;
using Newtonsoft.Json;
using System;
using UnityEngine;

[CreateAssetMenu(fileName = "LevelManager", menuName = "Coralium/Manager/Level Manager")]
[JsonObject(MemberSerialization.OptIn)]
public class LevelManager : ScriptableObject, ISerializationCallbackReceiver, IResetSOOnExitPlay, ISaveSOJSONPath
{
    [JsonProperty]
    public LevelData[] Levels;

    [NonSerialized] public ZoneManager ZoneManager;
    [JsonProperty]
    [ReadOnly] [SerializeField] private int levelIndex;
    public int LevelIndex
    {
        get => levelIndex;
        set
        {
            levelIndex = value;
            Save();
        }
    }
    public LevelData CurrentLevelData
    {
        get { return Levels[LevelIndex]; }
    }
    [JsonProperty]
    [ReadOnly] [SerializeField] private int lastSpawmLevelDataIndex;
    public int LastSpawmLevelDataIndex
    {
        get => lastSpawmLevelDataIndex;
        set
        {
            lastSpawmLevelDataIndex = value;
            Save();
        }
    }
    [HideInInspector]
    public LevelData LastSpawmLevelData
    {
        get { return Levels[LastSpawmLevelDataIndex]; }
    }
    public delegate void OnIntChange(int intValue);
    public OnIntChange OnLevelChange;

    [NonSerialized] public bool IsTraveling;
    [NonSerialized] public bool IsChangingLevel = false;
    [JsonProperty]
    [ReadOnly] [SerializeField] private bool levelInitialized;
    public bool LevelInitialized
    {
        get => levelInitialized;
        set
        {
            levelInitialized = value;
            Save();
        }
    }
    [NonSerialized] public Light GeneralLight;
    [NonSerialized] public float CurretnAmbientIntensity;
    /// <summary>
    /// Temp variable to change lv with debug
    /// </summary>
    public bool TravelingWithDebug;

    public delegate void OnGameObject(GameObject go);
    public OnGameObject OnPlayerFinded;
    private GameObject player;
    public GameObject Player
    {
        get => player;
        set
        {
            player = value;
            OnPlayerFinded?.Invoke(value);
        }
    }

    public void OnAfterDeserialize()
    {

    }
    public void OnBeforeSerialize()
    {

    }
    public void ResetLevelData()
    {

        for (int i = 0; i < Levels.Length; i++)
            Levels[i].ResetValues();
        LevelInitialized = false;
        levelIndex = 0;
        OnPlayerFinded = null;
        player = null;
    }
    public void ResetOnExitPlay()
    {
        ResetLevelData();
    }
    #region SaveJSON
    public string Path { get => $"Manager/{name}"; }
    public void Save() => DataManager.SaveObject(this, Path);
    #endregion
}
