
using UnityEngine;

public class OnPlayerPoisioned : BaseStatusEffect
{
    [SerializeField] private PlayerAbilities playerAbilities;
    [SerializeField] private Character character;
    [SerializeField] private BaseAbilityCast posionedShieldAbility;
    private Coroutine coroutine;
    [SerializeField] private float tickDamage;

    private void Start()
    {
        for (var i = 0; i < playerAbilities.Abilities.Length; i++)
        {
            if (playerAbilities.BaseAbilities[i] == posionedShieldAbility)
            {
                PoisionedShiledAbility poisionedShiledAbility = playerAbilities.Abilities[i] as PoisionedShiledAbility;
                OnAffectedByElementAction += () => poisionedShiledAbility.StopRecoverShield();
                OnRemoveEffectByElement += () => poisionedShiledAbility.StartToRecoverShield();
                return;
            }
        }
    }
    #region Override StatusEffect Methods
    public override void OnEffect()
    {
        base.OnEffect();
        character.BaseCharacter.Data.IsPoisioned = true;
        float timer = 0;
        if (coroutine != null)
            StopCoroutine(coroutine);
        coroutine = StartCoroutine(CoroutineHandler.ExecuteActionAfterUpdateUntil(() =>
            {
                timer += Time.fixedDeltaTime;
                if (timer >= tickDamage)
                {
                    timer = 0f;
                    character.TakeDamage(1, elementType, Vector3.zero);
                }
                return !IsAffectedByElement;
            }));
    }
    public override bool OffEffect()
    {
        if (!base.OffEffect())
            return false;
        StopCoroutine(coroutine);
        character.BaseCharacter.Data.IsPoisioned = false;
        return true;
    }
    #endregion

}
