using FMODUnity;
using NaughtyAttributes;
using UnityEngine;

public class BaseStatusEffect : MonoBehaviour
{
    [SerializeField] protected DamageTypes elementType;
    [SerializeField] private bool hasDuration;
    [ShowIf("hasDuration")] [SerializeField] private float effectDuration;
    [SerializeField] private bool affectByExternal = true;
    [Header("VFX")]
    [SerializeField] private BaseVisualEffect effect;
    [Header("SFX")]
    [SerializeField] private StudioEventEmitter elementSound;
    public bool IsAffectedByElement { get; private set; }

    public delegate void AffectedByElementAction();
    public AffectedByElementAction OnAffectedByElementAction;
    public AffectedByElementAction OnRemoveEffectByElement;

    public Coroutine OffEffectCoroutine { get; private set; }
    protected virtual void Awake()
    {
    }
    private void OnTriggerEnter(Collider other)
    {
        BaseStatusEffect baseStatusEffect = other.GetComponentInParent<BaseStatusEffect>();
        if (baseStatusEffect == null)
            return;
        if (baseStatusEffect == this)
            return;
        baseStatusEffect.ApplyEffect(elementType);
    }
    private void OnTriggerExit(Collider other)
    {
        BaseStatusEffect baseStatusEffect = other.GetComponentInParent<BaseStatusEffect>();
        if (baseStatusEffect == null)
            return;
        if (baseStatusEffect == this)
            return;
        if (baseStatusEffect.elementType == elementType)
            baseStatusEffect.OffEffect();
    }

    #region Reusable StatusEffect Methods
    /// <summary>
    /// Activates the elemental effect.
    /// Child classes can override this method to incorporate custom initialization logic.
    /// </summary>
    public virtual void OnEffect()
    {
        IsAffectedByElement = true;
        elementSound?.Play();
        effect?.StartEffect();
        if (OffEffectCoroutine != null)
            StopCoroutine(OffEffectCoroutine);
        if (hasDuration)
            OffEffectCoroutine = StartCoroutine(CoroutineHandler.ExecuteActionAfterEnumerator(() => OffEffect(), effectDuration));
        OnAffectedByElementAction?.Invoke();

    }
    /// <summary>
    /// Deactivates the elemental effects.
    /// Child classes can override this method to incorporate custom initialization logic.
    /// </summary>
    /// <returns>True if the effect was activated and it was successfully turned off.</returns>
    public virtual bool OffEffect()
    {
        if (!IsAffectedByElement)
            return false;
        IsAffectedByElement = false;
        elementSound?.Stop();
        effect?.EndEffect();
        OnRemoveEffectByElement?.Invoke();
        return true;
    }
    #endregion

    /// <summary>
    /// Applies an effect to the object if it can be affected by external object and the damage type matches.
    /// </summary>
    /// <param name="damageType"> The damage type applied. </param>
    public void ApplyEffect(DamageTypes damageType)
    {
        if (!affectByExternal)
            return;
        if (elementType != damageType)
            return;
        OnEffect();
    }
    /// <summary>
    /// Deactive auto stop effect after turn On.
    /// </summary>
    public void TurnOffAutomaticOffEffect()
    {
        hasDuration = false;
        if (OffEffectCoroutine != null)
            StopCoroutine(OffEffectCoroutine);
    }
}
