using NaughtyAttributes;

public class EnemyStatusEffect : BaseStatusEffect
{
    private EnemyController controller;

    protected override void Awake()
    {
        base.Awake();
        controller = gameObject.GetComponentInParent<EnemyController>();
    }

    #region Override StatusEffect Methods
    public override void OnEffect()
    {
        base.OnEffect();
        controller.damageType = elementType;
    }
    public override bool OffEffect()
    {
        if (!base.OffEffect())
            return false;
        controller.damageType = DamageTypes.Basic;
        return true;
    }
    #endregion

    #region Debug
#if (UNITY_EDITOR)
    [Button("SwitchEffect", enabledMode: EButtonEnableMode.Playmode)]
    private void SwitchEffect()
    {
        if (IsAffectedByElement)
        {
            OffEffect();
            return;
        }
        OnEffect();
    }

#endif
    #endregion
}
