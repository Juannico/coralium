public class AmbienParameterTrigger : BaseSoundParameterTrigger
{
    protected override void SetStartedEventEmitterData()
    {
        base.SetStartedEventEmitterData();
        startedEventEmitterData.ParameterValue = musicManager.GetAmbienceParameter(eventEmitterData);
    }
    protected override void ChangeParameter() => musicManager?.SetAmbienceParameter(eventEmitterData, smoothDuration);
    protected override void RestoreParameter() => musicManager?.SetAmbienceParameter(startedEventEmitterData, smoothDuration);
}
