using UnityEngine;
/// <summary>
/// Data to get each bank created in FMOD
/// </summary>
[CreateAssetMenu(fileName = "SoundVolumenData", menuName = "Coralium/Sound/Sound Volumen Data")]
public class SoundVolumenData : ScriptableObject
{
    public VolumenData MasterVolumen;
    public VolumenData MusicVolumen;
    public VolumenData SfxVolumen;
    public VolumenData UIVolumen;

    public void Initialize()
    {
        MasterVolumen.Initialize();
        MusicVolumen.Initialize();
        SfxVolumen.Initialize();
        UIVolumen.Initialize();
    }
    public VolumenData GetVolumenData(BusPath path)
    {
        switch (path)
        {
            case BusPath.Master:
                return MasterVolumen;
            case BusPath.Music:
                return MusicVolumen;
            case BusPath.Sfx:
                return SfxVolumen;
            case BusPath.UI:
                return UIVolumen;
        }
        return null;
    }
}

