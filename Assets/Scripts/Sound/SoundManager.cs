using UnityEngine;

public class SoundManager : MonoBehaviour
{
    [SerializeField] private SoundVolumenData soundVolumenData;
    public static SoundManager Instance { get; private set; }
    public MusicManager MusicManager;
    void Awake()
    {
        DontDestroyOnLoad(gameObject);
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
            return;
        }
        Instance = this;
        soundVolumenData.Initialize();
    }
}
