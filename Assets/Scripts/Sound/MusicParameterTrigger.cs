public class MusicParameterTrigger : BaseSoundParameterTrigger
{
    protected override void SetStartedEventEmitterData()
    {
        base.SetStartedEventEmitterData();
        startedEventEmitterData.ParameterValue = musicManager.GetMusicParameter(eventEmitterData);
    }
    protected override void ChangeParameter() => musicManager?.SetMusicParameter(eventEmitterData, smoothDuration);
    protected override void RestoreParameter() => musicManager?.SetMusicParameter(startedEventEmitterData, smoothDuration);
}
