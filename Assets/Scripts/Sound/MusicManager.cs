using FMODUnity;
using UnityEngine;

public class MusicManager : MonoBehaviour
{

    public StudioEventEmitter ambienceManager;
    private StudioEventEmitter musicEmitter;
    // Start is called before the first frame update
    private void Awake()
    {
        musicEmitter = GetComponent<StudioEventEmitter>();
        PlayMusic();
        PlayAmbience();
    }
    private void OnDestroy()
    {
        StopAmbience();
        StopMusic();
    }
    public void PlayAmbience() => ambienceManager?.Play();
    public void StopAmbience() => ambienceManager?.Stop();
    public void SetAmbienceParameter(EventEmitterData eventEmitterData, float smoothDuration = 0) => SetParameter(ambienceManager, eventEmitterData, smoothDuration);
    public float GetAmbienceParameter(EventEmitterData eventEmitterData) => GetParameter(ambienceManager, eventEmitterData);
    public void PlayMusic() => musicEmitter?.Play();
    public void StopMusic() => musicEmitter?.Stop();
    public bool IsMusicPlaying() => musicEmitter.IsPlaying();
    public void SetMusicParameter(string parameterName, float parameterValue) => musicEmitter.SetParameter(parameterName, parameterValue);
    public void SetMusicParameter(EventEmitterData eventEmitterData, float smoothDuration = 0) => SetParameter(musicEmitter, eventEmitterData, smoothDuration);
    public float GetMusicParameter(EventEmitterData eventEmitterData) => GetParameter(musicEmitter, eventEmitterData);
    private void SetParameter(StudioEventEmitter studioEventEmitter, EventEmitterData eventEmitterData, float smoothDuration = 0)
    {
        if (eventEmitterData.EventReference.Guid != studioEventEmitter.EventReference.Guid)
        {
            Debug.LogError($"target GUID {eventEmitterData.EventReference.Guid} does not correspond {studioEventEmitter.EventReference.Guid} in musicEmitter");
            return;
        }
        if (!studioEventEmitter.IsPlaying())
            studioEventEmitter.Play();
        if (smoothDuration > 0)
        {
            float timer = 0;
            float startParameterValue = musicEmitter.Params[eventEmitterData.ParameterIndex].Value;
            StartCoroutine(CoroutineHandler.ExecuteActionAfterUpdateUntil(() =>
            {
                timer += Time.deltaTime;
                float value = Mathf.Lerp(startParameterValue, eventEmitterData.ParameterValue, timer / smoothDuration);
                studioEventEmitter.SetParameter(eventEmitterData.ParameterName, eventEmitterData.ParameterValue);
                return timer >= smoothDuration;
            }, () => studioEventEmitter.SetParameter(eventEmitterData.ParameterName, eventEmitterData.ParameterValue)));
            return;
        }
        studioEventEmitter.SetParameter(eventEmitterData.ParameterName, eventEmitterData.ParameterValue);

    }
    public float GetParameter(StudioEventEmitter studioEventEmitter, EventEmitterData eventEmitterData) => studioEventEmitter.Params[eventEmitterData.ParameterIndex].Value;
}
