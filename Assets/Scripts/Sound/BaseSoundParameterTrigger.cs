using NaughtyAttributes;
using UnityEngine;

public abstract class BaseSoundParameterTrigger : MonoBehaviour
{
    [SerializeField] private LayerData layerData;
    [SerializeField] protected EventEmitterData eventEmitterData;
    [SerializeField] private bool useSmoothTransition;
    [ShowIf("useSmoothTransition")] [SerializeField] protected float smoothDuration = 0;
    [SerializeField] private bool useExternalHandle;
    [ShowIf("useExternalHandle")] [SerializeField] private bool paraemterChanged = false;
    protected EventEmitterData startedEventEmitterData;
    protected MusicManager musicManager;
    private void Start()
    {
        if (SoundManager.Instance == null)
        {
            enabled = false;
            return;
        }
        musicManager = SoundManager.Instance.MusicManager;
        SetStartedEventEmitterData();
        if (paraemterChanged && useExternalHandle)
            SetParameter(false);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (useExternalHandle)
            return;
        if (!LayerUtilities.IsSameLayer(layerData.PlayerLayer, other.gameObject.layer))
            return;
        SetParameter(false);
    }
    private void OnTriggerExit(Collider other)
    {
        if (useExternalHandle)
            return;
        if (!LayerUtilities.IsSameLayer(layerData.PlayerLayer, other.gameObject.layer))
            return;
        SetParameter(true);
    }

    public void SetParameter(bool recover)
    {
        if (recover)
            RestoreParameter();
        else
            ChangeParameter();
    }
    #region Reusable SounParameterTrigger Methods
    /// <summary>
    /// Intitialize the startedEventEmitterData.
    /// This method is meant to be overridden by child classes to implement custom logic and invoke base method.
    /// </summary>
    protected virtual void SetStartedEventEmitterData() => startedEventEmitterData = new EventEmitterData(eventEmitterData);
    /// <summary>
    /// Set the eventEmitterData.
    /// This method is meant to be overridden by child classes to implement custom logic .
    /// </summary>
    protected abstract void ChangeParameter();
    /// <summary>
    /// Restore the eventEmitterData to startedEventEmitterData.
    /// This method is meant to be overridden by child classes to implement custom logic and invoke base method.
    /// </summary>
    protected abstract void RestoreParameter();
    #endregion
}
