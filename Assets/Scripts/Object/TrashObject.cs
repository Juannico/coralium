using System.Collections;
using UnityEngine;

public class TrashObject : MonoBehaviour
{
    [SerializeField] private float scaredTime = 2;
    [SerializeField] private GameObject worldTrashObject;
    [SerializeField] private GameObject playerTrashObject;
    [SerializeField] private GPUParticleSystem disolveParticle;
    private TrashHandle trashHandle;
    private Character currentCharacter;
    private Collider selfCollider;
    private Material material;
    public bool CanScared { get; set; } = true;
    private void Awake()
    {
        worldTrashObject.SetActive(true);
        playerTrashObject.SetActive(false);
        selfCollider = GetComponent<Collider>();
        material = transform.GetChild(0).GetComponent<MeshRenderer>().material;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<EcolocalizationAbility>() != null)
            return;
        PlayerController player = other.gameObject.GetComponentInParent<PlayerController>();
        if (player == null)
            return;
        if (!CanScared)
            return;
        CanScared = false;
        player.Character.BaseCharacter.Data.IsScared = true;
        currentCharacter = player.Character;
        StartCoroutine(WaitToStopScare());
        worldTrashObject.SetActive(false);
        playerTrashObject.SetActive(true);
        transform.parent = player.BaseTransform.transform;
        transform.localRotation = Quaternion.identity;
        transform.localPosition = Vector3.zero;
        selfCollider.enabled = false;
    }
    public void SetTrashHandle(TrashHandle trashHandle) => this.trashHandle = trashHandle;
    public void Vanish()
    {
        selfCollider.enabled = false;
        StartCoroutine(Vanishing());
    }
    private IEnumerator WaitToStopScare()
    {
        yield return new WaitForSeconds(scaredTime);
        currentCharacter.BaseCharacter.Data.IsScared = false;
        worldTrashObject.SetActive(true);
        playerTrashObject.SetActive(false);
        transform.parent = null;
        Vector3 startPosition = transform.position;
        Vector3 targetPosition = startPosition + Random.onUnitSphere * Random.Range(2.5f, 5f);
        float interpolateAmount = 0f;
        transform.localRotation = Quaternion.identity;
        while (interpolateAmount < 1)
        {
            interpolateAmount = Mathf.Clamp01(interpolateAmount + Time.deltaTime * 0.55f);
            transform.position = Vector3.Slerp(startPosition, targetPosition, Mathf.Sqrt(interpolateAmount));
            yield return null;
        }
        selfCollider.enabled = true;
        CanScared = true;
    }
    private IEnumerator Vanishing()
    {
        disolveParticle.Play();
        float timer = 0f;
        while (timer < 2)
        {
            timer += Time.deltaTime;
            float alpha = timer / 2;
            material.SetFloat("_Disolve", alpha);
            yield return null;
        }
        material.SetFloat("_Disolve", 0);
        if (trashHandle != null)
        {
            selfCollider.enabled = true;
            trashHandle?.ActiveTrash(this);
            trashHandle?.SetAmountOftrash(false);
            yield break;
        }
        gameObject.SetActive(false);
    }
}
