using System.Collections;
using UnityEngine;
using UnityEngine.VFX;

public class ExplosionController : MonoBehaviour
{
    private Material mat;
    [SerializeField] private ParticleSystem burbujas1, busbujas2;
    [SerializeField] private GPUParticleSystem chispas, chispas2;
    private Collider selfCollider;

    private void Awake()
    {
        mat = gameObject.GetComponent<MeshRenderer>().material;
        selfCollider = GetComponent<Collider>();
        selfCollider.enabled = false;
        Initialize();
    }
    private void OnTriggerEnter(Collider other)
    {
        PlayerController playerController = other.GetComponentInParent<PlayerController>();
        if (playerController != null)
            other.GetComponentInParent<IDamageable<int, DamageTypes>>()?.TakeDamage(1, DamageTypes.Basic, ColliderUtilities.GetContactDirection(other, transform.position));
        if (other.TryGetComponent(out FlockUnit flockUnit))
            flockUnit.Kill();
    }
    public void Initialize()
    {
        mat.SetFloat("_SizeInterior", 0);
        mat.SetFloat("_SizeExterior", 0);
        mat.SetFloat("_IntensidadColor1", 1);
    }
    /// <summary>
    /// Activates explosion effects and initiates the lerping of explode material properties.
    /// </summary>
    public void Explode()
    {
        selfCollider.enabled = true;
        chispas2.Play();
        burbujas1.Play();
        CoroutineHandler.ExecuteActionAfter(() =>
        {
            chispas.Play();;
            busbujas2.Play();
        }, Constants.OneSecondDelay, this);
        StartCoroutine(LerpPropierty(0, "_SizeInterior", 0.2f, 0, 2f));
        StartCoroutine(LerpPropierty(0, "_SizeExterior", 0.2f, 0, 1.5f));
        StartCoroutine(LerpPropierty(0.2f, "_SizeInterior", 0.2f, 2, 0));
        StartCoroutine(LerpPropierty(0.3f, "_SizeExterior", 0.3f, 1.5f, 0f));
        StartCoroutine(LerpPropierty(0.4f, "_SizeInterior", 0.2f, 0, 1));
        StartCoroutine(LerpPropierty(0.4f, "__IntensidadColor1", 0.2f, 0, 2));
        StartCoroutine(LerpPropierty(0.6f, "_SizeInterior", 0.2f, 1, 0));
        StartCoroutine(LerpPropierty(0.6f, "__IntensidadColor1", 0.2f, 2, 0));
    }
    public void Stop()
    {
        Initialize();
        selfCollider.enabled = false;
        chispas2.Stop();
        burbujas1.Stop();
    }
    /// <summary>
    /// Lerps a material property over time.
    /// </summary>
    /// <param name="delay">Seconds to wait before starting the property change.</param>
    /// <param name="matPropierty">The reference name of the material property.</param>
    /// <param name="duration">Lerp duration in seconds.</param>
    /// <param name="initialValue">The initial value of the material property.</param>
    /// <param name="finalValue">The final value of the material property.</param>
    IEnumerator LerpPropierty(float delay, string matPropierty, float duration, float initialValue, float finalValue)
    {
        yield return new WaitForSeconds(delay);
        float counter = 0;
        while (counter < duration)
        {
            counter += Time.deltaTime;
            float temp = Mathf.Lerp(initialValue, finalValue, counter / duration);
            mat.SetFloat(matPropierty, temp);
            yield return null;
        }
    }
}
