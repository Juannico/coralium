using FMODUnity;
using PathCreation;
using System.Collections;
using UnityEngine;

public class ExplosiveObject : MonoBehaviour
{
    [SerializeField] private PathCreator rout;
    [SerializeField] private float velocity = 5;
    [SerializeField] private Vector2 randomRange;
    [SerializeField] float timeToExplode;
    [SerializeField] private ExplosionController controlExplosion;
    [SerializeField] private GameObject model;
    [SerializeField] private bool autoExplode = false;
    [SerializeField] private StudioEventEmitter explosionSFX;
    private Coroutine CurrentCourutine;
    private void Start()
    {
        if (autoExplode)
            StartCoroutine(MoveObject());
    }
    /// <summary>
    /// Move the object trhough defiend path vezier.
    /// </summary>
    private IEnumerator MoveObject()
    {
        if (rout == null)
            yield break;
        float timer = 0;
        while (timer < 0.99)
        {
            timer = Mathf.Clamp(timer + Time.deltaTime * velocity * 0.1f, 0, 0.99f);
            transform.position = rout.path.GetPointAtTime(timer);
            transform.rotation = rout.path.GetRotation(timer);
            yield return null;
        }
        timeToExplode = 0;
        StartCoroutine(Explode());
        transform.position = rout.path.GetPointAtTime(0.99f);
        transform.rotation = rout.path.GetRotation(0.99f);
    }
    /// <summary>
    /// Sets the values and starts the coroutine that moves the object using quadratic Bezier curve interpolation.
    /// </summary>
    /// <param name="pivotVector">The point to find the middle point for quadratic Bezier curve interpolation.</param>
    /// <param name="startVector">The initial position of the object.</param>
    /// <param name="targetVector">The target position of the object.</param>
    /// <param name="velocity">The velocity for interpolation.</param>
    public void StartToMove(Vector3 pivotVector, Vector3 startVector, Vector3 targetVector, float velocity = 0)
    {
        model.SetActive(true);
        if (velocity > 0)
            this.velocity = velocity;
        CurrentCourutine = StartCoroutine(MoveObject(pivotVector, startVector, targetVector));
        StartExplode();
    }
    private IEnumerator MoveObject(Vector3 pivotVector, Vector3 startVector, Vector3 targetVector)
    {
        targetVector += Random.insideUnitSphere * Random.Range(randomRange.x, randomRange.y);
        Vector3 middlePoint = MathUtilities.GetOppositeVectorInPlane(pivotVector, startVector, targetVector, -0.5f, 1);
        float interpolateAmount = 0;
        transform.position = startVector;
        while (interpolateAmount < 1)
        {
            interpolateAmount = Mathf.Clamp(interpolateAmount + Time.deltaTime * velocity * 0.1f, 0, 1f);
            transform.position = MathUtilities.QuadraticLerp(startVector, middlePoint, targetVector, interpolateAmount);
            yield return null;
        }
    }
    /// <summary>
    /// Stops the coroutine responsible for moving the object through Bezier curve interpolation.
    /// </summary>
    public void StopMove()
    {
        if (CurrentCourutine == null)
            return;
        StopCoroutine(CurrentCourutine);
    }
    /// <summary>
    /// Initiates the coroutine to activate the explosion of the object.
    /// </summary>
    public void StartExplode() => StartCoroutine(Explode());
    private IEnumerator Explode()
    {
        yield return new WaitForSeconds(timeToExplode);
        explosionSFX?.Play();
        controlExplosion.Explode();
        model.SetActive(false);
        if (CurrentCourutine != null)
            StopCoroutine(CurrentCourutine);
        yield return new WaitForSeconds(2f);
        gameObject.SetActive(false);
        controlExplosion.Stop();
    }
}
