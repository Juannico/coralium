using FMODUnity;
using NaughtyAttributes;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DropObjects : MonoBehaviour
{
    [SerializeField] [InfoBox("GameObjects prefab find in /Prefab/ObjectCollectable", EInfoBoxType.Warning)] private BaseObjectCollectable[] dropObjectPrefabs;
    [SerializeField] private Transform targetObjectTransform;
    [SerializeField] private float randomAreaSize = 5;
    [SerializeField] private bool showTargueGuidArea;
    [ShowIf("showTargueGuidArea")] [SerializeField] private Vector3 offsetGuide;
    [SerializeField] private StudioEventEmitter dropSFX;
    [SerializeField] private UnityEvent dropEvent;
    public List<BaseObjectCollectable> DroppedObjects { get; private set; }
    private bool canDrop = true;
    /// <summary>
    /// Drops objects inside a random area, maintaining object drop probability.
    /// </summary>
    /// <param name="ignoreProbability">By default, uses the object drop probability; otherwise, always drops objects.</param>
    public void Drops(bool ignoreProbability = false)
    {
        if (!canDrop)
            return;
        if (dropObjectPrefabs.Length == 0)
            return;
        //dropSFX?.Play();
        dropEvent?.Invoke();
        DroppedObjects = new List<BaseObjectCollectable>();
        for (var i = 0; i < dropObjectPrefabs.Length; i++)
        {
            if (dropObjectPrefabs[i] == null) continue;
            int probability = Random.Range(0, 100);
            if (probability > dropObjectPrefabs[i].ProbabilityToDrop && !ignoreProbability)
                continue;
            BaseObjectCollectable droppedObject = Instantiate(dropObjectPrefabs[i], transform.position, Quaternion.identity);
            droppedObject.gameObject.SetActive(true);
            droppedObject.transform.parent = transform;
            droppedObject.transform.parent = null;
            Vector2 targetPosition = targetObjectTransform != null ? targetObjectTransform.position : (Vector2)transform.position;
            targetPosition += Random.insideUnitCircle * Random.Range(1f, randomAreaSize * transform.lossyScale.magnitude);
            droppedObject.DropObject(targetPosition);
            DroppedObjects.Add(droppedObject);
        }
    }
    public void DeactiveDrop() => canDrop = false;
#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        if (!showTargueGuidArea)
            return;
        Vector3 targetPosition = transform.position + offsetGuide;
        if (targetObjectTransform != null)
            targetPosition = targetObjectTransform.position;
        Gizmos.color = Color.cyan;
        Gizmos.DrawSphere(targetPosition, 1);
        Gizmos.DrawWireSphere(targetPosition, randomAreaSize * transform.lossyScale.magnitude);
    }
    
#endif
}
