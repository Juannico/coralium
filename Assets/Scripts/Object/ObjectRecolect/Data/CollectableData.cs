using System;
using NaughtyAttributes;
using Newtonsoft.Json;
using UnityEngine;
[JsonObject(MemberSerialization.OptIn)]
[CreateAssetMenu(fileName = "Collectable Data", menuName = "Coralium/Add Collectable Data Template", order = 52)]
public class CollectableData : ScriptableObject, IResetSOOnExitPlay, ISaveSOJSONPath
{
    public bool ShowInfo;

    [ShowAssetPreview] public Sprite collectableSprite;
    [JsonProperty]
    [ReadOnly] [SerializeField] private int amount;
    [SerializeField] private bool showInfoFirstCollection = false;
    [AllowNesting] [Expandable] [ShowIf("showInfoFirstCollection")] [SerializeField] private TutorialData collectableTutorialData;
    [ReadOnly] [SerializeField] private bool firstCollection = true;
    [HideInInspector]
    public int Amount
    {
        get => amount;
        set
        {
            if (showInfoFirstCollection && firstCollection && value > amount)
            {
                firstCollection = false;
                ShowTutoCollectable();
            }
            OnCollected?.Invoke(value - amount);
            OnCollectedWithCollectableData?.Invoke(value - amount, this);
            amount = value;
            Save();
        }
    }
    [JsonProperty]
    [HideInInspector] public int LastCheckAmount;
    public delegate void OnAction(int intToChange);
    public delegate void OnActionWithCollectable(int intToChange, CollectableData collectableData);
    public OnAction OnCollected;
    public OnActionWithCollectable OnCollectedWithCollectableData;
    public void ShowTutoCollectable()
    {
        if (!Application.isPlaying)
            return;
        GameManager.Instance.TutorialHandler.StartTutorial(collectableTutorialData, TutorialHandlerComponent.TutorialType.SimplePopUp);
    }
#if UNITY_EDITOR
    private void OnEnable()
    {
        Save();
    }

    public void AddMoneyDebug(int value) => Amount += value;
#endif

    #region Reset OnSwitchPlaymode
    public void ResetOnExitPlay()
    {
        OnCollected = null;
        OnCollectedWithCollectableData = null;
        amount = 0;
        firstCollection = true;
    }
    #endregion

    #region SaveJSON
    public string Path { get => $"CollectableData/{name}"; }
    private void Save() => DataManager.SaveObject(this, Path);
    #endregion

}
