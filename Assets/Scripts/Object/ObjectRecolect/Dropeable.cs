using System;
using UnityEngine;

public class Dropeable : MonoBehaviour
{
    [SerializeField] protected float recollectVelocity = 1.25f;
    public bool IsCollected = true;
    private Collider selfCollider;

    protected virtual void Start()
    {
        selfCollider = gameObject.GetComponent<Collider>();

        if (!IsCollected)
            return;
        gameObject.AddComponent<UnHideGameObject>();
        gameObject.SetActive(false);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (IsCollected)
            return;
        if (!other.TryGetComponent(out Magnet magnet))
            return;
        StartToCollect(magnet.PlayerTransform, () => { });
    }
    private void OnTriggerStay(Collider other)
    {
        if (IsCollected)
            return;
        if (!other.TryGetComponent(out Magnet magnet))
            return;
        StartToCollect(magnet.PlayerTransform, () => { });
    }
    public void DropObject() => DropObject(transform.position + Vector3.up);
    /// <summary>
    /// Drops the object smoothly to a target position.
    /// </summary>
    /// <param name="targetPosition">The final position where the object is dropped.</param>
    public void DropObject(Vector3 targetPosition)
    {
        IsCollected = false;
        if(selfCollider == null)
            selfCollider = gameObject.GetComponent<Collider>();
        selfCollider.enabled = false;
        Vector3 startPosition = transform.position;
        float interpolateAmount = 0f;
        Vector3 startOffset = (targetPosition - startPosition).normalized;

        StartCoroutine(CoroutineHandler.ExecuteActionAfterUpdateUntil(() =>
        {
            interpolateAmount = Mathf.Clamp01(interpolateAmount + Time.deltaTime * recollectVelocity * 0.75f);
            Vector3 offSet = Vector3.Lerp(Vector3.zero, startOffset * 2.5f, Mathf.PingPong(Mathf.Sqrt(interpolateAmount) * 2, 1));
            transform.position = Vector3.Slerp(startPosition, targetPosition, Mathf.Pow(interpolateAmount, 2)) + offSet;
            return interpolateAmount >= 1;
        }, () =>
        {
            transform.position = targetPosition;
            selfCollider.enabled = true;
            IsCollected = false;
        }));
    }

    #region Reusable Dropeable Methods
    /// <summary>
    /// Set the Iscollected value true.
    /// This method is meant to be overridden by child classes to implement custom logic and invoke base method to set "IsCollected" true.
    /// </summary>
    public virtual void StartToCollect(Transform otherTransform, Action action) => IsCollected = true;
    #endregion

}
