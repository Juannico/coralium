using System;
using UnityEngine;

public class BaseSaveJSONObjectCollectable : BaseObjectCollectable, ISaveStateJSON
{
    [SerializeField] private int id;

    protected virtual void Awake()
    {
        DataManager.UpdateStateInterfaceValue(this);
    }
    public override void StartToCollect(Transform otherTransform, Action action)
    {
        base.StartToCollect(otherTransform, action);
        State = IsCollected;
    }
    #region ISaveStateJSON
    public string KeyName => "IsCollected";
    public string Path => $"Collectable/{objectData.name}/{gameObject.name}_{id}";
    public bool State
    {
        get => IsCollected;
        set
        {
            IsCollected = value;
            Save();
        }
    }
    public void Save() => DataManager.SaveInterface(this);
    #endregion
}
