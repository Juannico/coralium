using System;
using UnityEngine;

public class RecoverHealthObjectCollectable : BaseObjectCollectable
{
    #region Override Dropeable Methods
    public override void StartToCollect(Transform otherTransform, Action action)
    {
        action = () =>
        {
            otherTransform.GetComponentInParent<Character>().BaseCharacter.Data.CurrentHealth += amountToAdd;
            otherTransform.GetComponentInParent<PlayerCharacter>().ToggleLowHealthSnapshot();
        };
        base.StartToCollect(otherTransform, action);
    }
    #endregion
}
