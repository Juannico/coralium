using System;
using UnityEngine;

public class RechargeEnergyObjectCollectable : BaseObjectCollectable
{
    [SerializeField] private float energyToRecover;

    #region Override Dropeable Methods
    public override void StartToCollect(Transform otherTransform, Action action)
    {
        action = () => otherTransform.GetComponentInParent<Character>().BaseCharacter.Data.CurrentEnergy += energyToRecover;
        base.StartToCollect(otherTransform, action);
    }
    #endregion

}
