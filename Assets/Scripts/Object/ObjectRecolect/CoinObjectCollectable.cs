using System;
using UnityEngine;

public class CoinObjectCollectable : BaseSaveJSONObjectCollectable
{
    [SerializeField] private int coinValue;
    protected override void Awake()
    {
        base.Awake();
        amountToAdd = coinValue;
    }
}
