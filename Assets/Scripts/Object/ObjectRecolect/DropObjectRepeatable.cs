using UnityEngine;

public class DropObjectRepeatable : MonoBehaviour
{
    [SerializeField] private DropObjects dropObjects;
    [SerializeField] private float dropObjectCooldown;
    private float timer;
    private void Awake()
    {    
        if (dropObjects == null)
            dropObjects = GetComponent<DropObjects>();
        DropTick();
        timer = 0; 
    }
    private void DropTick()
    {
        dropObjects.Drops(true);
        for (int i = 0; i < dropObjects.DroppedObjects.Count; i++)
            dropObjects.DroppedObjects[i].OnCollected += StartDrop;
    }

    private void StartDrop()
    {
        CoroutineHandler.ExecuteActionAfter(() => DropTick(), dropObjectCooldown, this);
        for (int i = 0; i < dropObjects.DroppedObjects.Count; i++)
            dropObjects.DroppedObjects[i].OnCollected -= StartDrop;
    }
}
