using System;
using UnityEngine;

public class PoisionResistObjectCollectable : BaseObjectCollectable
{

    #region Override Dropeable Methods
    public override void StartToCollect(Transform otherTransform, Action action)
    {
        action = () =>
        {
            Character character = otherTransform.GetComponentInParent<Character>();
            if (character != null)
                character.BaseCharacter.Data.CurrentShield += 20;
        };
        base.StartToCollect(otherTransform, action);
    }
    #endregion

}
