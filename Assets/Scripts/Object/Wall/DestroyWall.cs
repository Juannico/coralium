using FMODUnity;
using UnityEngine;

public class DestroyWall : MonoBehaviour
{
    [SerializeField] private GameObject container;
    [SerializeField] private GameObject particlesContainer;
    [Header("SFX")]
    [SerializeField] private StudioEventEmitter wallSFX;
    private void Awake()
    {
        wallSFX.OverrideAttenuation = true;
        wallSFX.OverrideMaxDistance = 500;
    }
    public void Destroy(bool saved)
    {
        if (saved)
        {
            gameObject.AddComponent<UnHideGameObject>();
            gameObject.SetActive(false);
            return;
        }
        if (!wallSFX.IsPlaying())
            wallSFX?.Play();
        gameObject.GetComponent<DropObjects>()?.Drops();
        particlesContainer.SetActive(true);
        container.gameObject.SetActive(false);
        gameObject.GetComponent<Collider>().enabled = false;
    }

}
