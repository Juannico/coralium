using FMODUnity;
using NaughtyAttributes;
using UnityEngine;
using UnityEngine.Events;

public class GrowCoral : BaseAddActionToButton
{
    [Header("Grow Settings")]
    [SerializeField] private GrowControl growControl;
    [SerializeField] [Expandable] private GrowCoralData data;
    [SerializeField] private bool usePolypGuide;
    [ShowIf("usePolypGuide")] [SerializeField] private GameObject polypInstruction;
    [SerializeField] [Expandable] private LevelManager levelManager;
    [SerializeField] protected bool useUI;
    [ShowIf("useUI")] [SerializeField] protected ChallengeData infoZoneData;
    [ShowIf("useUI")] [SerializeField] private CollisionHelper showUI;
    [SerializeField] private bool useConversation;
    [ShowIf("useConversation")] [SerializeField] private BasicStartConversation conversation;
    [SerializeField] private UnityEvent OnPlanted;
    [SerializeField] private GPUParticleSystem areaParticles;
    [SerializeField] private GPUParticleSystem baseParticles;
    [Header("SFX")]
    [SerializeField] private StudioEventEmitter plantVFX;
    protected ChallengeUI challengeScreen;
    private Collider selfCollider;

    #region Unity Methods
    protected override void Awake()
    {
        base.Awake();
        if (useUI && !data.IsPlanted)
            SetUpInfoZoneUI();
        levelManager.CurrentLevelData.GrowCoralData = data;
        data.collectableData.Amount = levelManager.CurrentLevelData.GrowCoralFoodAmount;
        growControl.Data = data;
        selfCollider = gameObject.GetComponent<Collider>();

        selfCollider.enabled = false;
    }

    private void Start()
    {
        if (data.IsPlanted)
        {
            growControl.SetHideObject();
            OnPlanted?.Invoke();
            growControl.StartToGrow(true);
            areaParticles.Stop();
            baseParticles.Stop();
        }
        growControl.SetCoralState(data.IsPlanted);
        if (data.collectableData.Amount >= data.FoodNumber)
            selfCollider.enabled = true;
    }
    private void OnDestroy()
    {
        data.collectableData.OnCollected -= UpdateInfoZone;
    }
    #endregion

    #region Override Action Button Methods
    protected override void DoAction()
    {
        data.IsPlanted = true;
        DisableActionButton();
        growControl.Data.ProgressHandle = GameObject.FindGameObjectWithTag(Tags.Player).GetComponentInParent<PlayerController>().ProgressHandle;
        if (useConversation)
        {
            conversation.StartConversation((transfomr) => Plant());
            return;
        }
        Plant();
    }
    #endregion

    #region Main Methods
    private void Plant()
    {
        CoroutineHandler.ExecuteActionAfter(() => OnPlanted?.Invoke(),Constants.OneSecondDelay,this);
        data.collectableData.OnCollected -= UpdateInfoZone;
        selfCollider.enabled = false;
        data.collectableData.Amount -= data.FoodNumber;
        growControl.StartToGrow(false);
        gameObject.GetComponent<ControlParticulasZonaPlantar>()?.Vanish();
        SoundManager.Instance.MusicManager.SetMusicParameter("Level 1 Intensity", data.LevelIntensityMusic);
        challengeScreen.HideInfoZone();
        showUI.gameObject.SetActive(false);
        plantVFX.Play();
        areaParticles.Stop();
        baseParticles.Stop();

    }
    private void SetUpInfoZoneUI()
    {
        data.collectableData.OnCollected += UpdateInfoZone;
        showUI.TriggerEnter += (other) =>
        {
            if (data.IsPlanted)
                return;
            if (!LayerUtilities.IsSameLayer(layerData.PlayerLayer, other.gameObject.layer))
                return;
            challengeScreen = UIManager.Instance.OpenChallengeScreen();
            challengeScreen.Init(infoZoneData);
            challengeScreen.UpdateInfo($" {data.collectableData.Amount}/{data.FoodNumber}");
        };
        showUI.TriggerExit += (other) =>
        {
            if (data.IsPlanted)
                return;
            if (!LayerUtilities.IsSameLayer(layerData.PlayerLayer, other.gameObject.layer))
                return;
            challengeScreen.HideInfoZone();
        };
    }
    private void UpdateInfoZone(int valueAdded)
    {
        int value = data.collectableData.Amount + valueAdded;
        levelManager.CurrentLevelData.GrowCoralFoodAmount = value;
        if (data.IsPlanted)
            return;
        challengeScreen?.UpdateInfo($" {value}/{data.FoodNumber}");    
        if (value < data.FoodNumber)
            return;
        if (selfCollider == null)
            selfCollider = gameObject.GetComponent<Collider>();
        selfCollider.enabled = true;
    }
    #endregion

}
