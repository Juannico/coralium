using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.VFX;

public class CleanCoralController : MonoBehaviour
{
    private Material mat;
    private float cleanTime = 3;
    [SerializeField] private SetMaterialProvider setMaterialProvider;
    [SerializeField] private SetMaterialProvider oilMaterialProvider;
    [SerializeField] private GPUParticleSystem cleanParticles;
    [SerializeField] private List<Renderer> trash;
    [SerializeField] private UnityEvent CoralCleaned;
    private bool isDirty = false;
    public bool IsDirty
    {
        get { return isDirty; }
        set
        {
            isDirty = value;
            OnDirty?.Invoke(value);
        }
    }
    public Action<bool> OnDirty;
    private void Awake()
    {
        if (mat == null)
            mat = gameObject.GetComponentInChildren<Renderer>(true).material;
        if (setMaterialProvider == null)
            setMaterialProvider = transform.parent.gameObject.AddComponent<SetMaterialProvider>();
        IsDirty = true;
        setMaterialProvider.SetMaterial(mat, (material) => material.SetFloat("_Dirty", 1));
    }
    /// <summary>
    /// Plays visual effects and initiates coroutines to smoothly clean the coral and trash.
    /// </summary>
    public void StartClean()
    {
        if (!IsDirty)
            return;
        IsDirty = false;
        cleanParticles.gameObject.SetActive(true);
        cleanParticles.Play();
        oilMaterialProvider.SetMaterial(0, (material) => StartCoroutine(CleanCoral(material, "_Alpha")));
        setMaterialProvider.SetMaterial(mat, (material) => StartCoroutine(CleanCoral(material, "_Dirty",true)));
        StartCoroutine(CleanTrash());
        DropObjects dropObjects = gameObject.GetComponentInChildren<DropObjects>();
        if (dropObjects != null)
        {
            dropObjects.Drops();
            dropObjects.DeactiveDrop();
            dropObjects.enabled = false;
        }
    }
    private IEnumerator CleanCoral(Material mat, string floatName, bool callDelegate = false)
    {
        float timer = 0;
        float startValue = mat.GetFloat(floatName);
        while (timer < (cleanTime * 0.9f))
        {
            timer += Time.deltaTime;
            float value = Mathf.Lerp(startValue, 0, timer / (cleanTime * 0.9f));
            mat.SetFloat(floatName, value);
            yield return null;
        }
        if (callDelegate)
            CoralCleaned?.Invoke();
    }
    IEnumerator CleanTrash()
    {
        cleanParticles.Play();
        float timer = 0;
        while (timer < (cleanTime * 0.6f))
        {
            timer += Time.deltaTime;
            float disolveValue = Mathf.Lerp(0, 1, timer / (cleanTime * 0.6f));
            for (int i = 0; i < trash.Count; i++)
                SetMateialDisolvePropierty(trash[i].material, disolveValue);
            yield return null;
        }
    }
    /// <summary>
    /// Set the coral and trash materials cleaned.
    /// </summary>
    public void Cleaned()
    {
        if (setMaterialProvider == null)
            setMaterialProvider = transform.parent.gameObject.AddComponent<SetMaterialProvider>();
        setMaterialProvider.SetMaterial(mat, (material) => material.SetFloat("_Dirty", 0));
        for (int i = 0; i < trash.Count; i++)
            SetMateialDisolvePropierty(trash[i].material, 1);
    }
    private void SetMateialDisolvePropierty(Material material, float disolveValue) => material.SetFloat("_Disolve", disolveValue);

}
