using UnityEngine;

public class Turtle : MonoBehaviour, ISaveStateJSON
{
    [SerializeField] private CollectableData turtleCollectableData;
    [SerializeField] private LayerData layerData;
    [SerializeField] private int id = 0;
    [Header("VFX")]
    [SerializeField] private float transitionDuration;
    [SerializeField] private SetMaterialProvider modelMaterialProvider;
    [SerializeField] private GPUParticleSystem vfx;
    [SerializeField] private SetMaterialProvider rope;
    [SerializeField] private RouteFollower model;

    private float timer;
    private bool liberated;
    private float pathDuration;
    private bool vfxPlayed;
    private void Awake()
    {
        DataManager.UpdateStateInterfaceValue(this);
        if (!State)
            return;
        gameObject.AddComponent<UnHideGameObject>();
        gameObject.SetActive(false);
    }
    private void Start()
    {
        model.Reset(true);
        pathDuration = model.Route.path.length / model.Velocity;
        if (vfx.gameObject.activeInHierarchy)
            vfx.gameObject.SetActive(false);
    }
    private void Update()
    {
        if (!State)
            return;
        timer += Time.deltaTime;
        if (!model.ShouldMove)
        {
            if (!DisolvingTrash())
                return;
        }
        if (timer < pathDuration - transitionDuration)
            return;
        if (!vfxPlayed)
        {
            vfxPlayed = true;
            if (!vfx.gameObject.activeInHierarchy)
                vfx.gameObject.SetActive(true);
            vfx.Play();
        }
        modelMaterialProvider.SetMaterial(0, (material) => material.SetFloat("_Disolve", Mathf.SmoothStep(0, 1, (timer - pathDuration + transitionDuration) / transitionDuration)));
        if (timer < pathDuration)
            return;
        gameObject.SetActive(false);
    }
    public void Liberate(bool onSaved)
    {
        State = true;
        if (onSaved)
        {
            gameObject.SetActive(false);
            rope.gameObject.SetActive(false);
            return;
        }
    }

    private bool DisolvingTrash()
    {
        rope.SetMaterial(0, (material) => material.SetFloat("_Disolve", Mathf.SmoothStep(0, 1, timer / transitionDuration)));
        if (timer < transitionDuration)
            return false;
        rope.gameObject.SetActive(false);
        turtleCollectableData.Amount++;
        model.Reset(false);
        timer = 0;
        return true;
    }

    #region ISaveStateJSON
    public bool State
    {
        get => liberated;
        set
        {
            liberated = value;
            Save();
        }
    }
    public string KeyName => "Recollected";
    public string Path => $"Turtle/{gameObject.name}_{id}";
    public void Save() => DataManager.SaveInterface(this);
    #endregion

}

