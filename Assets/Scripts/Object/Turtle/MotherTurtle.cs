using PixelCrushers.DialogueSystem;
using UnityEngine;

public class MotherTurtle : ConversationOnActionButton
{
    [Header("Mother turtle setting")]
    [SerializeField] private CollectableData turtleCollectableData;
    [Header("Spawn turtle setting")]
    [SerializeField] private GameObject turtrlchildPrefab;
    [SerializeField] private GameObject[] routs;
    [Header("Conversation")]
    [VariablePopup(true)] [SerializeField] private string turtleLuaName;

    #region Unity Methods
    protected override void Awake()
    {
        base.Awake();
        if (turtleCollectableData.Amount == 0)
            return;
        for (int i = 0; i < turtleCollectableData.Amount; i++)
        {
            if (i >= routs.Length)
                throw new System.Exception($"Not enought routs");
            routs[i].gameObject.SetActive(true);
            Instantiate(turtrlchildPrefab, routs[i].transform.position, routs[i].transform.rotation, routs[i].transform.GetChild(0));
        }
    }
    #endregion

    #region Override Action Button Methods
    protected override void DoAction()
    {
        DialogueLua.SetVariable(turtleLuaName, turtleCollectableData.Amount);
        base.DoAction();
    }
    #endregion

}
