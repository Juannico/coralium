using UnityEngine;

public class CopyMaterialProperties
{
    public void Copy(Renderer renderer, SwitchPropertyData switchPropertyData)
    {
        Material sourceMaterial = renderer.material;
        renderer.material = switchPropertyData.targetMaterial;

        SwitchProperty[] switchProperties = switchPropertyData.SwitchProperties;
        for (int i = 0; i < switchProperties.Length; i++)
            CopyProperty(sourceMaterial, renderer.material, switchProperties[i]);
    }

    private void CopyProperty(Material sourceMaterial, Material targetMaterial, SwitchProperty switchProperty)
    {
        //Change switproperty names
        if (!sourceMaterial.HasProperty(switchProperty.SourceProperty))
            Debug.LogError($"{sourceMaterial} does not have {switchProperty.SourceProperty} property");

        if (!targetMaterial.HasProperty(switchProperty.TargetProperty))
            Debug.LogError($"{targetMaterial} does not have {switchProperty.TargetProperty} property");
        switch (switchProperty.Type)
        {
            case PropertyType.Texture:
                targetMaterial.SetTexture(switchProperty.TargetProperty, sourceMaterial.GetTexture(switchProperty.SourceProperty));
                break;
            case PropertyType.Float:
                targetMaterial.SetFloat(switchProperty.TargetProperty, sourceMaterial.GetFloat(switchProperty.SourceProperty));
                break;
            case PropertyType.Color:
                targetMaterial.SetColor(switchProperty.TargetProperty, sourceMaterial.GetColor(switchProperty.SourceProperty));
                break;
            case PropertyType.Vector:
                targetMaterial.SetVector(switchProperty.TargetProperty, sourceMaterial.GetVector(switchProperty.SourceProperty));
                break;
            case PropertyType.Keyword:
                bool state = sourceMaterial.IsKeywordEnabled(switchProperty.SourceProperty);
                if (state)
                {
                    targetMaterial.EnableKeyword(switchProperty.SourceProperty);
                    break;
                }
                targetMaterial.DisableKeyword(switchProperty.SourceProperty);
                break;
        }
    }
}
