using System;
using UnityEngine;
[CreateAssetMenu(fileName ="SwitchPropertyData",menuName ="Coralium/Shader/SwitchPropertyData")]
public class SwitchPropertyData : ScriptableObject
{
    public Material targetMaterial;
    public SwitchProperty[] SwitchProperties;
}
[Serializable]
public class SwitchProperty
{
    public PropertyType Type;
    public string SourceProperty;
    public string TargetProperty;
}
public enum PropertyType
{ 
    Texture,
    Color,
    Float,
    Vector,
    Keyword,
}
