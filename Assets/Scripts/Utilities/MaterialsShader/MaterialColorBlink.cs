using System.Collections;
using Unity.VisualScripting;
using UnityEngine;

public class MaterialColorBlink : MonoBehaviour
{
    public void ActivateBlink(Material material, Color toColor, float timeToTransition)
    {
        StartCoroutine(ColorBlink(material, toColor, timeToTransition));
    }
    private IEnumerator ColorBlink(Material material, Color toColor, float timeToTransition)
    {
        Color previousColor = material.GetColor("_Color");
        Color previousColorEmission = new Color();
        if(material.HasProperty("_ColorEmission"))
            previousColorEmission = material.GetColor("_ColorEmission");
        float interpValue = 0;
        while (interpValue <= timeToTransition)
        {
            material.SetColor("_Color", Color.Lerp(previousColor, toColor, Mathf.PingPong(interpValue * 2 / timeToTransition,1)));
            if (material.HasProperty("_ColorEmission"))
                material.SetColor("_ColorEmission", Color.Lerp(previousColorEmission, toColor, Mathf.PingPong(interpValue * 2 / timeToTransition,1)));
            interpValue += Time.deltaTime;
            yield return null;
        }

        // Fail safe for when the time is too low and might change the base colro permanently
        material.SetColor("_Color", previousColor);
        if (material.HasProperty("_ColorEmission"))
            material.SetColor("_ColorEmission", previousColorEmission);
        Destroy(this);
    }
}

public class ControlShaderColor
{
    public static void ChangeColor(MonoBehaviour outer, Material material, Color toColor, float timeToTransition)
    {
        MaterialColorBlink matColor = outer.GetComponent<MaterialColorBlink>();
        if (matColor != null) return;
        matColor = outer.AddComponent<MaterialColorBlink>();
        matColor.ActivateBlink(material, toColor, timeToTransition);
    }
}
