using NaughtyAttributes;
using UnityEngine;

public class SetMaterialPropierty : MonoBehaviour
{

    [SerializeField] private MaterialPropierty materialPropierty;
    [SerializeField] private bool lerpValues;
    [ShowIf("lerpValues")] [SerializeField] private float lerpVelocity = 0.5f;
    [Header("Transition Settings")]
    [HideIf("lerpValues")] [SerializeField] private bool smoothTransition;
    [ShowIf("smoothTransition")] [SerializeField] private float transitionDuration;
    [ShowIf("smoothTransition")] [SerializeField] private bool pingpong;

    private float timer;

    private float transitionTime;
    private float targetSmoothvalue;

    [ShowNonSerializedField] private float valueLerp;
    private void Awake()
    {
        materialPropierty.ResetValue();
        materialPropierty.Initialize();
        transitionTime = transitionDuration;
        targetSmoothvalue = 1;
        if (pingpong)
        {
            transitionTime = transitionDuration * 2;
            targetSmoothvalue = 0;
        }
        enabled = false;
        if (!lerpValues)
            return;
        smoothTransition = false;
        materialPropierty.StartSmoothValue();
        timer = 0;
        valueLerp = 0;

    }

    private void Update()
    {
        timer += Time.deltaTime;
        valueLerp = timer / transitionDuration;
        if (pingpong && valueLerp > 1)
            valueLerp = 2 - valueLerp;
        materialPropierty.SetSmoothValue(valueLerp);
        if (timer < transitionTime)
            return;
        materialPropierty.SetSmoothValue(targetSmoothvalue);
        materialPropierty.PropertyChanged = true;
        enabled = false;
    }
    public void ChangePropierty()
    {
        enabled = true;
        if (!smoothTransition)
        {
            materialPropierty.SetValue();
            valueLerp = 1;
            enabled = false;
            return;
        }
        materialPropierty.StartSmoothValue();
        timer = 0;
        valueLerp = 0;
    }
    public void ChangePropiertyByValue(float value, bool smooth)
    {
        if (!lerpValues)
            return;
        if (smooth)
        {
            float fixedValue = valueLerp;
            StartCoroutine(CoroutineHandler.ExecuteActionAfterUpdateUntil(() =>
           {
               fixedValue += Time.deltaTime * lerpVelocity;
               materialPropierty.SetSmoothValue(fixedValue);
               return fixedValue >= value;
           },
           () => ChangePropiertyByValue(value, false)));
            return;
        }
        valueLerp = value;
        materialPropierty.SetSmoothValue(value);
    }
    public void Reset()
    {
        materialPropierty.ResetValue();
    }
    private void OnValidate()
    {
        if (materialPropierty == null)
            return;
        if (materialPropierty.SetNewValue)
            materialPropierty.SetValue();
        else
            materialPropierty.ResetValue();
    }
}
