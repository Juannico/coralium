using NaughtyAttributes;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class SetMaterialProvider : MonoBehaviour
{
    [ReadOnly] [SerializeField] private Renderer[] renderers;

    private bool isInitialized;
    private void Awake()
    {
        if (!isInitialized)
            Initialize();
    }

    private void Initialize()
    {
        isInitialized = true;
        Renderer[] allRenderers = GetComponentsInChildren<Renderer>(true);
        List<Renderer> renderers = new List<Renderer>();
        for (int i = 0; i < allRenderers.Length; i++)
        {
            if (!allRenderers[i].enabled)
                continue;
            if (AllowComponentType(allRenderers[i]))
                renderers.Add(allRenderers[i]);
        }
        this.renderers = renderers.ToArray();
    }

    private static bool AllowComponentType(Renderer renderer)
    {
        if (renderer.GetComponent<VisualEffect>())
            return false;
        if (renderer.GetComponent<RadarPointMark>())
            return false;
        if (renderer.GetComponent<ParticleSystem>())
            return false;
        if (renderer.GetComponent<TrailRenderer>())
            return false;
        if (renderer.gameObject.name.Contains("ParticleHolder_"))
            return false;
        return true;
    }

    private void AddMaterial(Material materialToAdd, Action<Material> materialSet = null)
    {
        for (int i = 0; i < renderers.Length; i++)
        {
            if (renderers[i] == null)
                continue;
            Material[] mats = new Material[renderers[i].materials.Length + 1];
            for (int j = 0; j < mats.Length - 1; j++)
            {
                mats[j] = renderers[i].materials[j];
            }
            mats[mats.Length - 1] = materialToAdd;
            renderers[i].materials = mats;
            materialSet?.Invoke(renderers[i].materials[mats.Length - 1]);
        }
    }
    public void RemoveMaterial(Material materialToRemove)
    {

        if (!isInitialized)
            Initialize();
        for (int i = 0; i < renderers.Length; i++)
        {
            if (renderers[i] == null)
                continue;
            List<Material> mats = new List<Material>();
            for (int j = 0; j < renderers[i].materials.Length; j++)
            {
                Material sharedMaterial = renderers[i].sharedMaterials[j];
                if (sharedMaterial.shader != materialToRemove.shader)
                {
                    mats.Add(renderers[i].materials[j]);
                }
            }
            renderers[i].materials = mats.ToArray();
        }
    }

    public void ChangeMaterial(Material material, Material materialToChange, Action<Material> materialSet = null)
    {

        if (!isInitialized)
            Initialize();
        for (int i = 0; i < renderers.Length; i++)
        {
            if (renderers[i] == null)
                continue;
            int index = -1;
            Material[] mats = new Material[renderers[i].materials.Length];
            for (int j = 0; j < mats.Length; j++)
            {
                Material sharedMaterial = renderers[i].sharedMaterials[j];
                mats[j] = renderers[i].materials[j];
                if (material.shader == sharedMaterial.shader)
                {
                    mats[j] = materialToChange;
                    index = j;
                }
            }
            renderers[i].materials = mats;
            if (index >= 0)
                materialSet?.Invoke(renderers[i].materials[index]);

        }
    }
    public void SetMaterial(Material materialToSet, Action<Material> materialSet = null)
    {

        if (!isInitialized)
            Initialize();
        bool findMaterial = false;
        for (int i = 0; i < renderers.Length; i++)
        {
            if (renderers[i] == null)
                continue;
            if (materialSet == default)
            {
                materialSet?.Invoke(renderers[i].materials[0]);
                continue;
            }

            for (int j = 0; j < renderers[i].materials.Length; j++)
            {
                Material sharedMaterial = renderers[i].sharedMaterials[j];
                if (sharedMaterial.shader == materialToSet.shader)
                {
                    materialSet?.Invoke(renderers[i].materials[j]);
                    findMaterial = true;
                    continue;
                }
            }
        }
        if (!findMaterial)
            AddMaterial(materialToSet, materialSet);
    }
    public void SetMaterial(int index = 0, Action<Material> materialSet = null)
    {

        if (!isInitialized)
            Initialize();
        for (int i = 0; i < renderers.Length; i++)
        {
            if (renderers[i] == null)
                continue;
            int renderMatIndex = Mathf.Clamp(index, 0, renderers[i].materials.Length - 1);
            materialSet?.Invoke(renderers[i].materials[renderMatIndex]);
        }
    }
}
