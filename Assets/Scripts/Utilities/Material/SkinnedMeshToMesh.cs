using System.Collections;
using UnityEngine;
using UnityEngine.VFX;

public class SkinnedMeshToMesh : MonoBehaviour
{
    public SkinnedMeshRenderer mesh;
    public VisualEffect vfxgrraph;
    public float refreshrate;
    void Start()
    {
        StartCoroutine(UpdateGraph());
    }
   IEnumerator UpdateGraph()
    {
        while (gameObject.activeSelf)
        {
            Mesh meshtemp = new Mesh();
            mesh.BakeMesh(meshtemp);
            vfxgrraph.SetMesh("Mesh", meshtemp);            
            yield return new WaitForSeconds(refreshrate);
        }
    }
}
