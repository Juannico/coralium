using System;
using UnityEngine;
using UnityEngine.Rendering;
//using static UnityEditor.ShaderUtil;

[Serializable]
public class MaterialPropierty
{
    [SerializeField] private Renderer renderer;
    [SerializeField] private bool instantiateMaterial = true;
    [SerializeField] private string materialPropertyName;
    [SerializeField] private string materialPropertyStringType;
    public ShaderPropertyType PropiertyType { get => (ShaderPropertyType)Enum.Parse(typeof(ShaderPropertyType), materialPropertyStringType); }
    public bool SetNewValue;
    [SerializeField] private int startIntValue;
    [SerializeField] private int newIntValue;
    [SerializeField] private float startFloatValue;
    [SerializeField] private float newFloatValue;
    [SerializeField] [ColorUsage(true, true)] private Color startColorValue;
    [SerializeField] [ColorUsage(true, true)] private Color newColorValue;
    [SerializeField] private Vector3 startVectorValue;
    [SerializeField] private Vector3 newVectorValue;
    [SerializeField] private Texture startTextureValue;
    [SerializeField] private Texture newTextureValue;
    private object startValue;

    public bool PropertyChanged;
    public void Initialize()
    {
        if (!instantiateMaterial)
            return;
        string lastName = $"{renderer.sharedMaterial}(instantiated)";
        renderer.sharedMaterial = GameObject.Instantiate(renderer.sharedMaterial);
        renderer.sharedMaterial.name = lastName;
        //SetValue();
    }
    public void ResetValue() => UpdateValue(true);

    public void SetValue() => UpdateValue(false);

    private void UpdateValue(bool reset)
    {
        if (renderer == null)
            return;
        if (renderer.sharedMaterial == null)
            return;
        switch (PropiertyType)
        {
            case ShaderPropertyType.Int:
                renderer.sharedMaterial.SetInt(materialPropertyName, reset ? startIntValue : newIntValue);
                break;
            case ShaderPropertyType.Float:
                renderer.sharedMaterial.SetFloat(materialPropertyName, reset ? startFloatValue : newFloatValue);
                break;

            case ShaderPropertyType.Vector:
                renderer.sharedMaterial.SetVector(materialPropertyName, reset ? startVectorValue : newVectorValue);
                break;
            case ShaderPropertyType.Color:
                renderer.sharedMaterial.SetColor(materialPropertyName, reset ? startColorValue : newColorValue);
                break;
            case ShaderPropertyType.Texture:
                renderer.sharedMaterial.SetTexture(materialPropertyName, reset ? startTextureValue : newTextureValue);
                break;
        }
        if (!reset)
            PropertyChanged = true;
    }
    public void StartSmoothValue()
    {
        startValue = GetValue();
    }
    public void SetSmoothValue(float time)
    {
        switch (PropiertyType)
        {
            case ShaderPropertyType.Int:
                int intValue = (int)Mathf.Lerp((int)startValue, newIntValue, time);
                renderer.sharedMaterial.SetInt(materialPropertyName, intValue);
                break;
            case ShaderPropertyType.Float:

                float floatValue = Mathf.Lerp((float)startValue, newFloatValue, time);
                renderer.sharedMaterial.SetFloat(materialPropertyName, floatValue);
                break;
            case ShaderPropertyType.Vector:
                Vector4 vectorValue = Vector4.Lerp((Vector4)startValue, newVectorValue, time);
                renderer.sharedMaterial.SetVector(materialPropertyName, vectorValue);
                break;
            case ShaderPropertyType.Color:
                Color colorValue = Vector4.Lerp((Color)startValue, newVectorValue, time);
                renderer.sharedMaterial.SetColor(materialPropertyName, (Color)colorValue);
                break;
            case ShaderPropertyType.Texture:
                renderer.sharedMaterial.SetTexture(materialPropertyName, newTextureValue);
                break;
        }
    }
    public object GetValue()
    {
        switch (PropiertyType)
        {
            case ShaderPropertyType.Int:
                return renderer.sharedMaterial.GetInt(materialPropertyName);
            case ShaderPropertyType.Float:
                return renderer.sharedMaterial.GetFloat(materialPropertyName);
            case ShaderPropertyType.Vector:
                return renderer.sharedMaterial.GetVector(materialPropertyName);
            case ShaderPropertyType.Color:
                return renderer.sharedMaterial.GetColor(materialPropertyName);
            case ShaderPropertyType.Texture:
                return renderer.sharedMaterial.GetTexture(materialPropertyName);
        }
        return null;
    }
}
