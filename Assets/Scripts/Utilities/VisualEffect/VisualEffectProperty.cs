using System;
using UnityEngine;
using UnityEngine.VFX;

[Serializable]
public class VisualEffectProperty
{
    [SerializeField] private VisualEffect visualEffect;
    [SerializeField] private string visualEffectPropertyName;
    [SerializeField] private string visualEffectPropertyStringType;
    public Type VisualEffectPropertyType { get => Type.GetType(visualEffectPropertyStringType); }
    [SerializeField] private bool setNewValue;
    [SerializeField] private bool newBoolValue;
    private bool startBoolValue;
    [SerializeField] private int newIntValue;
    private int startIntValue;
    [SerializeField] private float newFloatValue;
    private float startFloatValue;
    [SerializeField] private Color newColorValue;
    private Color startColorValue;
    [SerializeField] private Vector2 newVector2Value;
    private Vector2 startVector2Value;
    [SerializeField] private Vector3 newVector3Value;
    private Vector3 startVector3Value;
    [SerializeField] private Texture newTextureValue;
    private Texture startTextureValue;
    public void Initialize()
    {
        switch (VisualEffectPropertyType)
        {
            case var boolType when boolType == typeof(bool):
                startBoolValue = visualEffect.GetBool(visualEffectPropertyName);
                break;

            case var intType when intType == typeof(int):
                startIntValue = visualEffect.GetInt(visualEffectPropertyName);
                break;

            case var floatType when floatType == typeof(float):
                startFloatValue = visualEffect.GetFloat(visualEffectPropertyName);
                break;

            case var vector2Type when vector2Type == typeof(Vector2):
                startVector2Value = visualEffect.GetVector2(visualEffectPropertyName);
                break;

            case var vector3Type when vector3Type == typeof(Vector3):
                startVector3Value = visualEffect.GetVector3(visualEffectPropertyName);
                break;
            case var colorType when colorType == typeof(Vector4):
                startColorValue = visualEffect.GetVector4(visualEffectPropertyName);
                break;
            case var colorType when colorType == typeof(Texture):
                startTextureValue = visualEffect.GetTexture(visualEffectPropertyName);
                break;
        }
    }
    public void ResetValue() => UpdateValue(true);

    public void SetValue() => UpdateValue(false);

    private void UpdateValue( bool reset = false)
    {
        switch (VisualEffectPropertyType)
        {
            case var boolType when boolType == typeof(bool):
                visualEffect.SetBool(visualEffectPropertyName, reset ? startBoolValue : newBoolValue);
                break;

            case var intType when intType == typeof(int):
                visualEffect.SetInt(visualEffectPropertyName, reset ? startIntValue : newIntValue);
                break;

            case var floatType when floatType == typeof(float):
                visualEffect.SetFloat(visualEffectPropertyName, reset ? startFloatValue : newFloatValue);
                break;

            case var vector2Type when vector2Type == typeof(Vector2):
                visualEffect.SetVector2(visualEffectPropertyName, reset ? startVector2Value : newVector2Value);
                break;

            case var vector3Type when vector3Type == typeof(Vector3):
                visualEffect.SetVector3(visualEffectPropertyName, reset ? startVector3Value : newVector3Value);
                break;
            case var colorType when colorType == typeof(Vector4):
                visualEffect.SetVector4(visualEffectPropertyName, reset ? startColorValue : newColorValue);
                break;
            case var colorType when colorType == typeof(Texture):
                visualEffect.SetTexture(visualEffectPropertyName, reset ? startTextureValue : newTextureValue);
                break;
        }
    }
}


