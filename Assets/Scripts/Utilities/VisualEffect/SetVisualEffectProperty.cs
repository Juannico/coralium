using NaughtyAttributes;
using UnityEngine;

public class SetVisualEffectProperty : MonoBehaviour
{
    [SerializeField] private bool multiplesMaterialPropierty;
    [HideIf("multiplesMaterialPropierty")][SerializeField] private VisualEffectProperty visualEffectPropierty;
    [ShowIf("multiplesMaterialPropierty")] [SerializeField] private VisualEffectProperty[] visualEffectPropierties;

    private void Awake()
    {
        if (!multiplesMaterialPropierty)
        {
            visualEffectPropierty.Initialize();
            return;
        }
        foreach (VisualEffectProperty visualEffectPropierty in visualEffectPropierties)
            visualEffectPropierty.Initialize();
    }
    public void SetVisualEffectPropierty()
    {
        if(!multiplesMaterialPropierty)
        {
            visualEffectPropierty.SetValue();
            return;
        }
        foreach (VisualEffectProperty visualEffectPropierty in visualEffectPropierties)
            visualEffectPropierty.SetValue();
    }
    public void ResetVisualEffectPropierty()
    {
        if (!multiplesMaterialPropierty)
        {
            visualEffectPropierty.ResetValue();
            return;
        }
        foreach (VisualEffectProperty visualEffectPropierty in visualEffectPropierties)
            visualEffectPropierty.ResetValue();
    }
}
