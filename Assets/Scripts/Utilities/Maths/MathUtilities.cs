using Unity.Mathematics;
using UnityEngine;

public static class MathUtilities
{
    private static System.Random random = new System.Random();
    public static Vector3 RandomVector(Vector3 min, Vector3 max) => new Vector3(UnityEngine.Random.Range(min.x, max.x), UnityEngine.Random.Range(min.y, max.y), UnityEngine.Random.Range(min.z, max.z));
    public static Vector2 RandomVector2() => new Vector2(UnityEngine.Random.Range(-1f, 1f), UnityEngine.Random.Range(-1f, 1f));
    public static Vector2 RandomVector2(Vector2 min, Vector2 max)
    {
        float randomX = (float)(random.NextDouble() * (max.x - min.x) + min.x);
        float randomY = (float)(random.NextDouble() * (max.y - min.y) + min.y);
        return new Vector2(randomX, randomY);
    }


    public static Vector2 RotatePointFromVector2Direction(Vector2 point,Vector2 direction)
    {
        float angle = Vector2.SignedAngle(direction, point) * Mathf.Deg2Rad;
        float sin = Mathf.Sin(angle);
        float cos = Mathf.Cos(angle);
        return new Vector2(point.x * cos - point.y * sin, point.x * sin + point.y * cos);
    }
    /// TODO: check if the now method work like comment
    public static Vector3 QuadraticLerp(Vector3 startPoint, Vector3 middlePoint, Vector3 EndPoint, float time)
    {
        time = Mathf.Clamp01(time);
        /* Vector3 ab = Vector3.Lerp(startPoint, middlePoint, time);
         Vector3 bc = Vector3.Lerp(middlePoint, EndPoint, time);
         return Vector3.Lerp(ab, bc, time);*/
        return (Mathf.Pow(1 - time, 2) * startPoint) + (2 * (1 - time) * time * middlePoint) + (Mathf.Pow(time, 2) * EndPoint);
    }
    public static Vector3 CubicLerp(Vector3 startPoint, Vector3 middlePoint1, Vector3 middlePoint2, Vector3 EndPoint, float time)
    {
        time = Mathf.Clamp01(time);
        return (Mathf.Pow(1 - time, 3) * startPoint) + (3 * Mathf.Pow(1 - time, 2) * time * middlePoint1) + (3 * (1 - time) * Mathf.Pow(time, 2) * middlePoint2) + (Mathf.Pow(time, 3) * EndPoint);
    }
    public static Vector3 GetOppositeVectorInPlane(Vector3 pivotVector, Vector3 vectorA, Vector3 vectorB, float pointAWeight = 1, float pointBWeight = 1)
    {
        Vector3 oppositeVector = (vectorA + (vectorB - pivotVector) * pointAWeight) + (vectorB + (vectorA - pivotVector) * pointBWeight);
        oppositeVector /= 2;
        return oppositeVector;
    }
    public static float GetRotationDiference(float CurrentRotation, float lastRotation)
    {

        if (CurrentRotation == lastRotation)
            return 0f;
        float angleDiference = CurrentRotation - lastRotation;
        if (angleDiference < -180f)
            angleDiference += 360;
        if (angleDiference > 180f)
            angleDiference -= 360;
        return angleDiference;
    }

    public static float3 Float3Normalize(float3 value)
    {
        float maxValue = value.x + value.y + value.z;
        if (maxValue > 1)
        {
            float factor = 1 / maxValue;
            return new float3(value.x * factor, value.y * factor, value.z * factor);
        }
        return value;
    }
    public static bool IsInRange(float number, float min, float max) => number >= min && number <= max;

}
