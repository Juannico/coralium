using UnityEngine;

public class HideByDistance : MonoBehaviour
{
    [SerializeField] private Transform transformToHide;
    [SerializeField] private float distance = 5;


    private void Update()
    {
        if (Vector3.Distance(transform.position, transformToHide.position) > distance)
            return;
        transformToHide.gameObject.SetActive(false);
        enabled = false;
    }
    private void OnDrawGizmosSelected()
    {
        Gizmos.DrawWireSphere(transform.position,distance);
    }
}
