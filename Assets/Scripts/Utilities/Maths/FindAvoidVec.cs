using UnityEngine;

public static class FindAvoidVec
{
    static RaycastHit[] hits = new RaycastHit[5];
    public static Vector3 CalculateObstacleVec(Transform transform, LayerMask avoidLayer, float radius, float rayDistance, Vector3 lastAvoidVec)
    {
        Vector3 avoidanceVec = Vector3.zero;
        int index = Physics.SphereCastNonAlloc(transform.position, radius, transform.forward, hits, rayDistance, avoidLayer);
        if (index > 0)
            avoidanceVec = FindBestWaytoAvoidObstable(transform, avoidLayer, radius, rayDistance, lastAvoidVec);
        return avoidanceVec;
    }

    public static Vector3 FindBestWaytoAvoidObstable(Transform transform, LayerMask avoidLayer, float radius, float rayDistance, Vector3 lastAvoidVec)
    {
        // Si sigue viendo el mismo objeto mantine la misma direccion
        if (lastAvoidVec != Vector3.zero)
        {
            int index = Physics.SphereCastNonAlloc(transform.position, radius, transform.forward, hits, rayDistance, avoidLayer);
            if (index == 0)
                return lastAvoidVec;
        }
        float maxDistance = int.MinValue;
        var selectedDirection = Vector3.zero;
        Vector3[] checkAvoidVec = { transform.up, -transform.up, transform.right, -transform.right };
        // Calcuala x cantidad de dirreciones y escoge la distancia mas  alejada del obstaculo 
        for (int i = 0; i < checkAvoidVec.Length; i++)
        {

            var currentDirection = transform.TransformDirection(checkAvoidVec[i].normalized);
            int index = Physics.RaycastNonAlloc(transform.position, transform.forward, hits, rayDistance, avoidLayer);
            if (index > 0)
            {
                float currentDistance = (hits[0].point - transform.position).sqrMagnitude;
                if (currentDistance > maxDistance)
                {
                    maxDistance = currentDistance;
                    selectedDirection = currentDirection;
                }
            }
            else
            {
                selectedDirection = currentDirection;
                return selectedDirection.normalized;
            }
        }
        return selectedDirection.normalized;
    }
}
