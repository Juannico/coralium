using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
/// <summary>
/// This static class detects changes in player mode states, specifically when entering or exiting play mode.
/// It iterates over all Scriptable Objects in the project that implement the <see cref="IResetSOOnExitPlay"/> or <see cref="ISaveSOOnEnterPlay"/> interfaces 
/// and triggers corresponding methods based on the play mode state change.
/// </summary>
static class SOPlayModeResetter
{
    [InitializeOnLoadMethod]
    private static void RegisterResets()
    {
        EditorApplication.playModeStateChanged += ActionOnSOSwitchPlayMode;
    }
    /// <summary>
    /// Detects play mode state changes and iterates over Scriptable Objects implementing the specified interfaces 
    /// to trigger reset or save actions accordingly.
    /// </summary>
    /// <param name="change">Unity enum indicating the state of Playmode and EditMode.</param>
    private static void ActionOnSOSwitchPlayMode(PlayModeStateChange change)
    {
        if (change != PlayModeStateChange.ExitingPlayMode && change != PlayModeStateChange.EnteredPlayMode)
            return;
        ScriptableObject[] scriptableObjects = FindAssets<ScriptableObject>();
        foreach (var scriptableObject in scriptableObjects)
        {
            if (scriptableObject is IResetSOOnExitPlay && change == PlayModeStateChange.ExitingPlayMode)
                (scriptableObject as IResetSOOnExitPlay).ResetOnExitPlay();
            if (scriptableObject is ISaveSOOnEnterPlay && change == PlayModeStateChange.EnteredPlayMode)
                (scriptableObject as ISaveSOOnEnterPlay).SaveOnEnterPlay();
        }
    }
    private static T[] FindAssets<T>() where T : Object
    {
        var guids = AssetDatabase.FindAssets($"t:{typeof(T)}");
        var assets = new T[guids.Length];
        for (int i = 0; i < guids.Length; i++)
        {
            var path = AssetDatabase.GUIDToAssetPath(guids[i]);
            assets[i] = AssetDatabase.LoadAssetAtPath<T>(path);
        }
        return assets;
    }
}
#endif
/// <summary>
/// Interface for Scriptable Objects that need to perform a reset action when exiting play mode.
/// </summary>
public interface IResetSOOnExitPlay
{
    public void ResetOnExitPlay();
}
/// <summary>
/// Interface for Scriptable Objects that need to perform a save action when entering play mode.
/// </summary>
public interface ISaveSOOnEnterPlay
{
    public void SaveOnEnterPlay();
}
