using UnityEngine;

public class SwitchObjects : MonoBehaviour
{
    [SerializeField] private GameObject defaultGameObject;
    [SerializeField] private GameObject targetGameObject;
    private void Awake()
    {
        RestoreObject();
    }

    public void SetTargeObject()
    {
        defaultGameObject.gameObject.SetActive(false);
        targetGameObject.gameObject.SetActive(true);
    }
    public void RestoreObject()
    {
        defaultGameObject.gameObject.SetActive(true);
        targetGameObject.gameObject.SetActive(false);
    }
    private void OnDisable()
    {
        RestoreObject();
    }
}
