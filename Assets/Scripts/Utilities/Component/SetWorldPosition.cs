using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
[ExecuteInEditMode]
#endif
public class SetWorldPosition : MonoBehaviour
{
    [SerializeField] private Vector3 worldPosition;
    private void Awake()
    {
        transform.position = worldPosition;
    }
#if UNITY_EDITOR
    private void Update()
    {
        transform.position = worldPosition;
    }
#endif

}
