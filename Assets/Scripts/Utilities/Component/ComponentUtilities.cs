using System;
using System.Reflection;
using UnityEngine;

public static class ComponentUtilities 
{
    public static T AddComponent<T>(this GameObject game, T duplicate) where T : Component
    {
        Type type = duplicate.GetType();
        T target = game.AddComponent<T>();
        BindingFlags flags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Default | BindingFlags.DeclaredOnly;
        PropertyInfo[] propertiesInfosInfo = type.GetProperties(flags);
        foreach (PropertyInfo propertyInfo in propertiesInfosInfo)
        {
            if (propertyInfo.CanWrite)
                propertyInfo.SetValue(target, propertyInfo.GetValue(duplicate));
        }
        FieldInfo[] finfos = type.GetFields(flags);
        foreach (var finfo in finfos)
            finfo.SetValue(duplicate, finfo.GetValue(duplicate));
        return target;
    }
    public static T SetComponent<T>(GameObject gameObject) where T : Component
    {
        T[] components = gameObject.GetComponents<T>();
        for (int  i = 0;  i < components.Length; i++)
        {
            if (typeof(T) == components[i].GetType())
                return components[i];
        }
        return gameObject.AddComponent<T>();
    }
}
