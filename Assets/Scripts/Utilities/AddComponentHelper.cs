using UnityEngine;

public class AddComponentHelper : MonoBehaviour
{
    public TextAsset script;
    public System.Type componentToAdd;
    
    public void AddComponent()
    {
        if (componentToAdd != null)
            gameObject.AddComponent(componentToAdd);
    }
    
}
