
using UnityEditor;
using UnityEngine;

public class ShowAngleView : MonoBehaviour
{
#if UNITY_EDITOR
    [SerializeField] private float distance = 500;
    [SerializeField] private float angle = 60;
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.cyan;
        Gizmos.DrawRay(transform.position, transform.forward * distance);
        Handles.color = Color.yellow;
        float radius = Mathf.Tan(angle * Mathf.Deg2Rad) * distance;
        Vector3 targetDistance = transform.position + transform.forward.normalized * distance;
        Handles.DrawWireDisc(targetDistance, Vector3.forward, radius);

        Gizmos.color = Color.yellow;
        Gizmos.DrawLine(transform.position, targetDistance + transform.up * radius);
        Gizmos.DrawLine(transform.position, targetDistance - transform.up * radius);
        Gizmos.DrawLine(transform.position, targetDistance + transform.right * radius);
        Gizmos.DrawLine(transform.position, targetDistance - transform.right * radius);
        Gizmos.DrawWireSphere(transform.position, 1);
    }
#endif
}
