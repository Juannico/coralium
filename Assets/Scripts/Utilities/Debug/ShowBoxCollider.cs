using UnityEngine;

public class ShowBoxCollider : MonoBehaviour
{
    [SerializeField] private BoxCollider boxCollider;
    [SerializeField] private Color gizmozColor;
    private void Awake()
    {
        Destroy(this);
    }
    private void OnDrawGizmos()
    {
        if (boxCollider == null)
        {
            boxCollider = gameObject.GetComponent<BoxCollider>();
            if (boxCollider == null)
                return;
        }
        Gizmos.matrix = Matrix4x4.TRS(transform.position, transform.rotation, transform.lossyScale);
        Gizmos.color = gizmozColor;
        Gizmos.DrawWireCube(boxCollider.center, boxCollider.size);
        Gizmos.matrix = Matrix4x4.identity;
    }
}
