using UnityEngine;

public class ShowGizmo : MonoBehaviour
{
#if UNITY_EDITOR
    [SerializeField] private bool show = true;
    [SerializeField] private Color color;
    [SerializeField] private float size = 5;

    private void OnDrawGizmos()
    {
        if (!show)
            return;
        Gizmos.color = color;
        Gizmos.DrawSphere(transform.position, size);
    }
#endif
}
