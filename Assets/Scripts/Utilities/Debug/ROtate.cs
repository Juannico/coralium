using UnityEngine;
#if UNITY_EDITOR
[ExecuteInEditMode]
#endif
public class Rotate : MonoBehaviour
{
    [SerializeField] private float rotateVeloicty = 5f;
    [SerializeField] private bool rotate = true;


    private void Update()
    {
        transform.localEulerAngles += Time.deltaTime * rotateVeloicty * Vector3.up;
    }
}
