using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowPosition : MonoBehaviour
{
    [SerializeField] private float rayDistance = 35;
    private void Awake()
    {
        Debug.Log($"WARNIG:GameObject with naem ({gameObject.name})");
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.cyan;
        Gizmos.DrawRay(transform.position, Vector3.up * rayDistance);
    }
}
