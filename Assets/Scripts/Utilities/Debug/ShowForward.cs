using UnityEngine;

public class ShowForward : MonoBehaviour
{
#if UNITY_EDITOR
    [SerializeField] private float rayDistance = 5;
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.cyan;
        Gizmos.DrawRay(transform.position, transform.forward * rayDistance);
    }
#endif
}
