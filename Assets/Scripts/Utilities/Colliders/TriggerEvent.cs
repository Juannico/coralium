using UnityEngine;
using UnityEngine.Events;

public class TriggerEvent : MonoBehaviour
{
    [SerializeField] private UnityEvent enterEvent;
    [SerializeField] private UnityEvent exitEvent;
    [SerializeField] private LayerData layerData;
    private void OnTriggerEnter(Collider other)
    {
        if (!LayerUtilities.IsSameLayer(layerData.PlayerLayer, other.gameObject.layer))
            return;
        enterEvent?.Invoke();
    }
    private void OnTriggerExit(Collider other)
    {
        if (!LayerUtilities.IsSameLayer(layerData.PlayerLayer, other.gameObject.layer))
            return;
        exitEvent?.Invoke();
    }
}
