using UnityEngine;

public class CollisionHelper : MonoBehaviour
{
    public delegate void TriggerDelegate(Collider other);
    public delegate void CollisionDelegate(Collision collider);
    public TriggerDelegate TriggerEnter;
    public TriggerDelegate TriggerExit;
    public TriggerDelegate TriggerStay;
    public CollisionDelegate CollisionEnter;
    public CollisionDelegate CollisionExit;
    public CollisionDelegate CollisionStay;
    public Collider Collider;
    private void OnTriggerEnter(Collider other) => TriggerEnter?.Invoke(other);
    private void OnTriggerExit(Collider other) => TriggerExit?.Invoke(other);
    private void OnTriggerStay(Collider other) => TriggerStay?.Invoke(other);
    private void OnCollisionEnter(Collision collision) => CollisionEnter?.Invoke(collision);
    private void OnCollisionExit(Collision collision) => CollisionExit?.Invoke(collision);
    private void OnCollisionStay(Collision collision) => CollisionStay?.Invoke(collision);
    private void Awake()
    {
        if (Collider == null)
            Collider = gameObject.GetComponent<Collider>();
    }
    public void TransferDelegates(CollisionHelper sourceCollisionHelper, bool overwrite = true)
    {
        if (!overwrite)
        {
            TriggerEnter += sourceCollisionHelper.TriggerEnter;
            TriggerExit += sourceCollisionHelper.TriggerExit;
            TriggerStay += sourceCollisionHelper.TriggerStay;
            CollisionEnter += sourceCollisionHelper.CollisionEnter;
            CollisionExit += sourceCollisionHelper.CollisionExit;
            CollisionStay += sourceCollisionHelper.CollisionStay;
            return;
        }
        TriggerEnter = sourceCollisionHelper.TriggerEnter;
        TriggerExit = sourceCollisionHelper.TriggerExit;
        TriggerStay = sourceCollisionHelper.TriggerStay;
        CollisionEnter = sourceCollisionHelper.CollisionEnter;
        CollisionExit = sourceCollisionHelper.CollisionExit;
        CollisionStay = sourceCollisionHelper.CollisionStay;
    }
}
