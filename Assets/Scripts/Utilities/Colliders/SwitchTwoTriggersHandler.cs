using UnityEngine;
using UnityEngine.Events;

public class SwitchTwoTriggersHandler : MonoBehaviour
{
    [SerializeField] private LayerData layerData;
    [Header("Triggers")]
    [SerializeField] private CollisionHelper oneTrigger;
    [SerializeField] private CollisionHelper twoTrigger;
    [Header("Functions")]
    [SerializeField] private UnityEvent switchTwoToOneTriggers;
    [SerializeField] private UnityEvent switchOneToTwoTriggers;
    private int triggerIndex = 0;
    private void Awake()
    {
        oneTrigger.TriggerEnter -= OnEnterOneTrigger;
        oneTrigger.TriggerEnter += OnEnterOneTrigger;
        twoTrigger.TriggerEnter -= OnEnterTwoTrigger;
        twoTrigger.TriggerEnter += OnEnterTwoTrigger;
    }

    private void OnEnterOneTrigger(Collider other)
    {
        if (!LayerUtilities.IsSameLayer(layerData.PlayerLayer, other.gameObject.layer))
            return;
        if (triggerIndex == 2)
            switchTwoToOneTriggers?.Invoke();
        triggerIndex = 1;
    }
    private void OnEnterTwoTrigger(Collider other)
    {
        if (!LayerUtilities.IsSameLayer(layerData.PlayerLayer, other.gameObject.layer))
            return;
        if (triggerIndex == 1)
            switchOneToTwoTriggers?.Invoke();
        triggerIndex = 2;
    }
}
