using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ColliderUtilities 
{

    public static Vector3 GetContactDirection(Collider collider,Vector3 objectPosition)
    {
        Vector3 contactPosition = collider.ClosestPoint(objectPosition);
        Vector3 direction = collider.transform.position - contactPosition;
        return direction.normalized;
    }
    public static Vector3 GetContactDirection(Collision collision, Vector3 otherPoistion)
    {
        Vector3 contactPosition = collision.contacts[0].point;
        Vector3 direction = collision.transform.position - contactPosition;
        return direction.normalized;
    }
}
