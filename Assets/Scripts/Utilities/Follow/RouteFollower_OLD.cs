using UnityEngine;
using PathCreation;
using System;
[Obsolete("Use RoutFollowe instead")]
public class RouteFollower_OLD : MonoBehaviour
{
    public PathCreator ruta;
    public float velocidad = 5;
    float distanciarecorrida = 0;
    [SerializeField] public bool ShouldFollow { get; set; } = true;

    private void Start()
    {
    }

    
    // Update is called once per frame
    void FixedUpdate()
    {
        if (!ShouldFollow)
            return;
        if (ruta == null)
            return;
        distanciarecorrida += Time.deltaTime * velocidad;
        transform.position = ruta.path.GetPointAtDistance(distanciarecorrida);
        transform.rotation = ruta.path.GetRotationAtDistance(distanciarecorrida);
    }
}
