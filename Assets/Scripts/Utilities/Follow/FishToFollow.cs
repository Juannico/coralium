using UnityEngine;

public class FishToFollow : MonoBehaviour
{
        [field: SerializeField][field: Range(0.1f, 10f)] public float ForwardStrength { get; private set; } = 2f;
        [field: SerializeField][field: Range(0.1f, 2.5f)] public float CenterStrength { get; private set; } = 2f;
       [field: SerializeField][field: Range(0.1f, 20f)] public float FollowMaxDistance { get; private set; } = 2f;
        [field: SerializeField][field: Range(0.1f, 10f)] public float FollowMinDistance { get; private set; } = 2f;
}
