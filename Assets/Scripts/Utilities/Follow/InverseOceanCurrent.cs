using UnityEngine;

public class InverseOceanCurrent : MonoBehaviour, ISaveStateJSON
{
    [SerializeField] private GameObject oceanCurrent;
    [SerializeField] private Animator buttonsAnimator;
    [SerializeField] private RouteFollowerKinematic routeFollowerKinematic;
    private bool isForwardDirection = true;

    private void Awake()
    {
        DataManager.UpdateStateInterfaceValue(this);
    }
    public void InverseCurrent()
    {
        if (!GuideKinematic())
        {
            ChangeDirection();
            return;
        }
        CoroutineHandler.ExecuteActionAfter(ChangeDirection, routeFollowerKinematic.GetDelay() * 4, this);
    }

    private void ChangeDirection()
    {
        isForwardDirection = !isForwardDirection;
        buttonsAnimator.SetBool("IsForwardDirection", isForwardDirection);
        oceanCurrent.transform.localScale = oceanCurrent.transform.localScale + Vector3.back * oceanCurrent.transform.localScale.z * 2;
    }

    private bool GuideKinematic()
    {
        if (!State)
            return false;
        State = false;
        routeFollowerKinematic.StartKinematic();
        return true;
    }
    #region ISaveStateJSON
    private bool firstTime = true;
    public bool State
    {
        get => firstTime;
        set
        {
            firstTime = value;
            Save();
        }
    }
    public string KeyName => "FirstTime";
    public string Path => $"OceanCurrentKinematic/{gameObject.name}";
    public void Save() => DataManager.SaveInterface(this);
    #endregion
}
