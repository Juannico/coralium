using Cinemachine;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.VFX;

public class OceanCurrentDoors : OceanCurrent
{
    [Header("Door Settings")]
    [SerializeField] private LayerData layerData;
    [SerializeField] private LevelManager levelManager;
    [SerializeField] private CollisionHelper forwardCollisionHeleper;
    [SerializeField] private UnityEvent forwardEvent;
    [SerializeField] private CollisionHelper backwardCollisionHeleper;
    [SerializeField] private UnityEvent backwardEvent;
    [SerializeField] private BaseVisualEffect visualEffect;
    [SerializeField] private GPUParticleSystem enterVFX;
    [SerializeField] private RigibodyForceHandle insideCurrentEffectContainer;
    [SerializeField] private GameObject lineEffectContainer;
    [SerializeField] private float offPosition = 5.25f;
    private bool insideCurrent;
    protected Vector3[] forwardTargetPositions;
    protected Vector3[] backwardTargetPositions;
    protected GPUParticleSystem[] lineEffects;
    private int indexinsideCurrentEffectContainer;
    private Transform playerTransform;
    private PlayerInputManager playerInputManager;
    private CinemachineConfiner2D cinemachineConfiner2D;
    protected override void Awake()
    {
        enterVFX.Stop();
        base.Awake();
        if (gameObject.TryGetComponent(out Collider collider))
            Destroy(collider);
        forwardTargetPositions = targetPositions;
        backwardTargetPositions = new Vector3[forwardTargetPositions.Length];
        for (int i = forwardTargetPositions.Length; i > 0; i--)
            backwardTargetPositions[forwardTargetPositions.Length - i] = forwardTargetPositions[i - 1];
        SetCollissionTrigger(forwardCollisionHeleper, forwardEvent, true);
        SetCollissionTrigger(backwardCollisionHeleper, backwardEvent, false);
        insideCurrentEffectContainer.gameObject.SetActive(false);
        playerInputManager = PlayerInputManager.Instance;
        levelManager.IsChangingLevel = false;
        if (lineEffectContainer != null)
            lineEffects = lineEffectContainer.GetComponentsInChildren<GPUParticleSystem>(true);
        cinemachineConfiner2D = GameObject.FindGameObjectWithTag(Tags.PlayerVirtualCamera).GetComponentInChildren<CinemachineConfiner2D>();
    }

    protected override void FixedUpdate()
    {
        base.FixedUpdate();
        if (!insideCurrent)
            return;
        Vector3 direction = GetDirection(insideCurrentEffectContainer.transform.position, ref indexinsideCurrentEffectContainer);
        insideCurrentEffectContainer.transform.forward = direction;
        insideCurrentEffectContainer.ExternalVelocityForceVector += direction * currentDirectionStrength /* offsetMultiplier*/;
    }
    private void OnDestroy()
    {
        if (!insideCurrent)
            return;
        playerInputManager.SetInputState(true);
        levelManager.IsChangingLevel = false;
    }
    private void SetCollissionTrigger(CollisionHelper collissionHelper, UnityEvent unityEvent, bool forwardDirection)
    {
        collissionHelper.TriggerEnter += (other) =>
        {
            if (!LayerUtilities.IsSameLayer(layerData.PlayerLayer, other.gameObject.layer))
                return;
            insideCurrentEffectContainer.gameObject.SetActive(!insideCurrent);
            lineEffectContainer.SetActive(!insideCurrent);
            if (insideCurrent)
            {
                OnCharacterExit(other);
                insideCurrent = false;
                enterVFX.transform.forward = (targetPositions[targetPositions.Length - 4] - targetPositions[targetPositions.Length - 1]).normalized;
                enterVFX.transform.position = other.transform.position + enterVFX.transform.forward;
                enterVFX.Play();
                playerInputManager.SetAbilities(true);
                CoroutineHandler.ExecuteActionAfter(() => visualEffect.EndEffect(), 1, this);
                levelManager.IsChangingLevel = false;
                cinemachineConfiner2D.enabled = true;
                return;
            }
            cinemachineConfiner2D.enabled = false;
            levelManager.IsChangingLevel = true;
            playerInputManager.SetAbilities(false);
            playerTransform = other.transform;
            insideCurrent = true;
            indexinsideCurrentEffectContainer = 0;
            targetPositions = forwardDirection ? forwardTargetPositions : backwardTargetPositions;
            bool canEvalutenextPosition = indexinsideCurrentEffectContainer < originalPositionLength - 1;
            int fixedIndex = GetFixedIndex(canEvalutenextPosition ? indexinsideCurrentEffectContainer : indexinsideCurrentEffectContainer - 1);
            Vector3 direction = targetPositions[canEvalutenextPosition ? fixedIndex + 3 : fixedIndex - 1] - targetPositions[canEvalutenextPosition ? fixedIndex : fixedIndex - 4];
            Vector3 startPosition = playerTransform.position + direction.normalized * offPosition;
            Vector3 nextPostion = GetOceanTargetPositionWithCurves(startPosition, startPosition, ref indexinsideCurrentEffectContainer);

            insideCurrentEffectContainer.transform.forward = (nextPostion - insideCurrentEffectContainer.transform.position).normalized;
            insideCurrentEffectContainer.transform.position = nextPostion;
            foreach (GPUParticleSystem lineEffect in lineEffects)
                lineEffect.Play();
            enterVFX.transform.forward = (targetPositions[3] - targetPositions[0]).normalized;
            enterVFX.transform.position = other.transform.position + enterVFX.transform.forward;
            enterVFX.Play();
            visualEffect.StartEffect();
            unityEvent?.Invoke();
            OnCharacterEnter(other);
        };
    }

}
