using UnityEngine;
using PathCreation;
using NaughtyAttributes;
using System;

public class RouteFollower : TickFixedUpdate
{
    [Header("RouteFollower Settings")]
    public PathCreator Route;
    public float Velocity = 5;
    private float distanceTraveled = 0;
  
    public bool ShouldMove { get; set; }
    [HideInInspector] public int direction;
    [SerializeField] private bool useGuide = false;
    [ShowIf("useGuide")]  public float GuideDistanceTraveled = 0;
    [ShowIf("useGuide")]  private bool useGuideToStartPosition = true;
    private void Awake()
    {
        if (useGuide && useGuideToStartPosition)
            distanceTraveled = GuideDistanceTraveled;
        ShouldMove = true;
        direction = 1;
    }
    protected override bool ContinueCondition()
    {
        if (!ShouldMove)
            return false;
        if (Route == null)
            return false;
        return true;
    }
    protected override void SelfFixedUpdate()
    {      
        distanceTraveled += deltaTime * Velocity * direction;
        transform.position = Route.path.GetPointAtDistance(distanceTraveled);
        transform.rotation = Route.path.GetRotationAtDistance(distanceTraveled);
    }
    public void SetStart() => SetAtPosition(0);
    public void SetEnd() => SetAtPosition((int)Route.path.length);

    public void SetAtPosition(int distanceTraveled)
    {
        transform.position = Route.path.GetPointAtDistance(distanceTraveled);
        transform.rotation = Route.path.GetRotationAtDistance(distanceTraveled);
    }
#if UNITY_EDITOR
    private void OnValidate()
    {
        if (!useGuide)
            return;
        SetTestRoutPath();
    }
#endif
    public void Reset(bool shoulStop)
    {
        ShouldMove = !shoulStop;
        distanceTraveled = 0;
    }
    public void SetVelocityByDuration(float duration) => Velocity = Route.path.length / duration;

#if UNITY_EDITOR
    public void SetTestRoutPath()
    {
        GuideDistanceTraveled = Mathf.Clamp(GuideDistanceTraveled, 0, Route.path.length);
        transform.position = Route.path.GetPointAtDistance(GuideDistanceTraveled);
        transform.rotation = Route.path.GetRotationAtDistance(GuideDistanceTraveled);
    }
#endif
}
