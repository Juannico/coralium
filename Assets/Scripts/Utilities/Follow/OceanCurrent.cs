using FMODUnity;
using NaughtyAttributes;
using System;
using System.Collections.Generic;
using UnityEngine;

public class OceanCurrent : MonoBehaviour
{
    [Header("Current Settings")]
    [SerializeField] private GameObject targetPostionsParent;
    [SerializeField] private bool useCurves;
    [SerializeField] [Range(15f, 50f)] protected float currentDirectionStrength = 8.5f;
    [SerializeField] protected float smooth = 2f;
    [SerializeField] protected float smoothMultiplier = 2f;
    [SerializeField] private LayerMask EnviromentLayer;
    [SerializeField] private bool deactivePlayerAbilities;
    [ShowIf("deactivePlayerAbilities")] [SerializeField] private GameObject deactiveObject;
    [Header("Init Guide")]
    [SerializeField] private bool showGuide;
    [ShowIf(EConditionOperator.And, "useCurves", "showGuide")] [SerializeField] private Color guideColor;
    [ShowIf(EConditionOperator.And, "useCurves", "showGuide")] [SerializeField] private int lineParts = 10;
    protected Vector3[] targetPositions;
    protected int originalPositionLength;
    private List<Character> charactersToMove = new List<Character>();
    private List<int> targetIndex = new List<int>();
    private List<Vector3> refVelocitiesDirection = new List<Vector3>();
    private List<Vector3> directions = new List<Vector3>();
    [SerializeField] private Flock flock;
    private List<FlockUnit> flockUnits = new List<FlockUnit>();
    private List<Flock> lastFlocks = new List<Flock>();
    private TargetDestinations targetDestinations;
    [Header("SFX Settings")]
    [SerializeField] private StudioEventEmitter streamSFXEmitter;

    protected virtual void Awake()
    {
        targetDestinations = GetComponent<TargetDestinations>();
        Transform[] tempTargetPositions = targetPostionsParent.GetComponentsInChildren<Transform>(true);
        targetPositions = CreatePathValues(tempTargetPositions);
        for (var i = 0; i < targetPositions.Length; i++)
        {
            if (targetDestinations == null)
                continue;
            Transform transformToAdd = Instantiate(tempTargetPositions[i + 1]);
            transformToAdd.parent = targetDestinations.TargetPostionsParent.transform;
            transformToAdd.position = tempTargetPositions[i + 1].transform.position;
            transformToAdd.SetSiblingIndex(i);
        }
        if (deactivePlayerAbilities)
            deactiveObject.SetActive(false);
    }
    private void Start()
    {
        if (flock != null)
            Invoke("DestroyFirstUnit", 0.1f);
    }
    private Vector3[] CreatePathValues(Transform[] tempTargetPositions)
    {
        if (!useCurves)
        {
            Vector3[] pathValues = new Vector3[tempTargetPositions.Length - 1];
            for (int i = 0; i < pathValues.Length; i++)
                pathValues[i] = tempTargetPositions[i + 1].position;
            return pathValues;
        }
        originalPositionLength = tempTargetPositions.Length - 1;
        int length = originalPositionLength * 2 + originalPositionLength - 2;
        Vector3[] pathValuesWithCurve = new Vector3[length];
        pathValuesWithCurve[0] = tempTargetPositions[1].position;
        pathValuesWithCurve[1] = tempTargetPositions[1].position + (tempTargetPositions[2].position - tempTargetPositions[1].position).normalized * smooth;
        if (originalPositionLength > 2)
        {
            for (int i = 2; i < originalPositionLength; i++)
            {
                Vector3 prewPosition = tempTargetPositions[i - 1].position;
                Vector3 currenPosition = tempTargetPositions[i].position;
                Vector3 nextPosition = tempTargetPositions[i + 1].position;
                int fixedIndex = GetFixedIndex(i - 1);
                Vector3 forwardirection = (currenPosition - prewPosition).normalized;
                Vector3 backwardirection = (currenPosition - nextPosition).normalized;
                pathValuesWithCurve[fixedIndex - 1] = currenPosition + (backwardirection + forwardirection * smoothMultiplier).normalized * smooth;
                pathValuesWithCurve[fixedIndex] = currenPosition;
                pathValuesWithCurve[fixedIndex + 1] = currenPosition + (forwardirection + backwardirection * smoothMultiplier).normalized * smooth;
            }
        }
        pathValuesWithCurve[length - 2] = tempTargetPositions[originalPositionLength].position + (tempTargetPositions[originalPositionLength - 1].position - tempTargetPositions[originalPositionLength].position).normalized * smooth;
        pathValuesWithCurve[length - 1] = tempTargetPositions[originalPositionLength].position;
#if UNITY_EDITOR
        if (!Application.isPlaying)
        {
            foreach (Vector3 paths in pathValuesWithCurve)
            {
                Debug.DrawRay(paths + Vector3.up, Vector3.down * 2);
                Debug.DrawRay(paths + Vector3.right, Vector3.left * 2);
                Debug.DrawRay(paths + Vector3.forward, Vector3.back * 2);
            }
        }
#endif
        return pathValuesWithCurve;
    }

    protected int GetFixedIndex(int i) => 3 * (i % originalPositionLength);
    private void DestroyFirstUnit()
    {
        FlockUnit unit = flock.AllUnit[0];
        unit.AssignedFlock.RemoveFlockUnit(unit);
        Destroy(unit.gameObject);
    }
    protected virtual void FixedUpdate()
    {
        MoveCharacter();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (OnCharacterEnter(other))
            return;
        OnFishEnter(other);
    }
    private void OnTriggerExit(Collider other)
    {
        OnCharacterExit(other);
    }
    private void MoveCharacter()
    {
        if (charactersToMove.Count <= 0)
            return;
        for (int i = 0; i < charactersToMove.Count; i++)
        {
            if (charactersToMove[i].BaseCharacter.Data.IsInmuneToExternalForce)
                continue;
            int currentIndex = targetIndex[i];
            Vector3 direction = GetDirection(charactersToMove[i].transform.position, ref currentIndex);
            if (IsCollidingNextPosition(charactersToMove[i], direction))
                direction = Vector3.zero;
            targetIndex[i] = currentIndex;
            Vector3 refVelocityDirection = refVelocitiesDirection[i];
            this.directions[i] = direction;
            refVelocitiesDirection[i] = refVelocityDirection;
            charactersToMove[i].RigibodyForceHandle.ExternalVelocityForceVector *= 0.85f;
            charactersToMove[i].RigibodyForceHandle.ExternalVelocityForceVector += this.directions[i] * currentDirectionStrength;
            charactersToMove[i].BaseCharacter.Data.ExternalImpulseDirection = this.directions[i];
        }
    }
    public Vector3 GetDirection(Vector3 objectPosition, ref int index)
    {
        if (!useCurves)
            return GetOceanTargetPosition(objectPosition, ref index);
        return (GetOceanTargetPositionWithCurves(objectPosition, objectPosition, ref index) - objectPosition).normalized;
    }
    protected Vector3 GetOceanTargetPosition(Vector3 objectPostion, ref int index)
    {
        bool canEvalutenextPosition = index < targetPositions.Length - 1;
        Vector3 currentPosition = canEvalutenextPosition ? targetPositions[index] : targetPositions[index - 1];
        Vector3 nexPosition = canEvalutenextPosition ? targetPositions[index + 1] : targetPositions[index];
        float dot = Vector3.Dot((nexPosition - currentPosition).normalized, (nexPosition - objectPostion).normalized);
        if (dot < 0f && canEvalutenextPosition)
            index++;
        return (nexPosition - currentPosition).normalized;
    }
    protected Vector3 GetOceanTargetPositionWithCurves(Vector3 startObjectPosition, Vector3 objectPostion, ref int index, float timer = -1)
    {
        bool canEvalutenextPosition = index < originalPositionLength - 1;
        int fixedIndex = GetFixedIndex(canEvalutenextPosition ? index : index - 1);
        Vector3 currentPivot = targetPositions[canEvalutenextPosition ? fixedIndex : fixedIndex - 4];
        Vector3 currentPivotAncle = targetPositions[canEvalutenextPosition ? fixedIndex + 1 : fixedIndex - 3];
        Vector3 nextPivotAncle = targetPositions[canEvalutenextPosition ? fixedIndex + 2 : fixedIndex - 2];
        Vector3 nextPivot = targetPositions[canEvalutenextPosition ? fixedIndex + 3 : fixedIndex - 1];
        if (timer >= 0)
            return MathUtilities.CubicLerp(currentPivot, currentPivotAncle, nextPivotAncle, nextPivot, timer);
        timer = GetTimer(ref objectPostion, currentPivot, nextPivot);
        if (timer > 0.98f && canEvalutenextPosition)
        {
            index++;
            return GetOceanTargetPositionWithCurves(startObjectPosition, objectPostion, ref index);
        }
        Vector3 targetPosition = MathUtilities.CubicLerp(currentPivot, currentPivotAncle, nextPivotAncle, nextPivot, timer);
        float dot = Vector3.Dot((nextPivot - currentPivot).normalized, (targetPosition - startObjectPosition).normalized);
        if (dot <= 0.15f && canEvalutenextPosition)
        {
            float addTime = currentDirectionStrength / Vector3.Distance(currentPivot, nextPivot);
            timer += addTime * Time.fixedDeltaTime * 2;
            targetPosition = MathUtilities.CubicLerp(currentPivot, currentPivotAncle, nextPivotAncle, nextPivot, timer);

        }
        return targetPosition;
    }

    private float GetTimer(ref Vector3 objectPosition, Vector3 currentPivot, Vector3 nextPivot)
    {
        Vector3 direction = (nextPivot - currentPivot).normalized;
        objectPosition += direction * (Mathf.Pow(currentDirectionStrength, 0.5f) + 2);
        Vector3 relativePosition = objectPosition - currentPivot;
        return Vector3.Dot(relativePosition, direction) / Vector3.Distance(currentPivot, nextPivot);
    }
    private int GetIndex(Vector3 objectPosition)
    {
        int index = 0;
        float shortestDistance = Vector3.Distance(objectPosition, targetPositions[index]);
        for (int i = 1; i < originalPositionLength; i++)
        {
            int fixedIndex = i;
            if (useCurves)
                fixedIndex = GetFixedIndex(i);
            float currentDistance = Vector3.Distance(objectPosition, targetPositions[fixedIndex]);
            if (currentDistance < shortestDistance)
            {
                shortestDistance = currentDistance;
                index = fixedIndex;
            }
        }
        return index;
    }
    private bool IsCollidingNextPosition(Character character, Vector3 direction)
    {
        float maxDistance = character.Collider.bounds.extents.magnitude + 1.25f;
        if (!Physics.SphereCast(character.transform.position, character.Collider.radius, direction.normalized, out RaycastHit hit, maxDistance, EnviromentLayer))
            return false;
        return Vector3.Dot(direction.normalized, hit.normal) < 0;
    }
    protected bool OnCharacterEnter(Collider other)
    {
        Character characterToMove = other.GetComponentInParent<Character>();
        if (characterToMove == null)
            return false;
        if (deactivePlayerAbilities)
        {
            if (other.GetComponentInParent<PlayerController>() != null)
                deactiveObject.SetActive(true);
        }
        characterToMove.BaseCharacter.Data.IsInExternalForce = true;
        this.charactersToMove.Add(characterToMove);
        refVelocitiesDirection.Add(Vector3.zero);
        int index = GetIndex(characterToMove.transform.position);
        directions.Add(GetDirection(characterToMove.transform.position, ref index));
        targetIndex.Add(index);
        MoveCharacter();
        streamSFXEmitter?.Play();
        return true;
    }
    private void OnFishEnter(Collider other)
    {
        if (flock == null)
            return;
        FlockUnit unit = other.GetComponent<FlockUnit>();
        if (unit == null)
            return;
        if (flock == unit.AssignedFlock)
            return;
        //unit.OffsetTarget += (Vector3.up * UnityEngine.Random.Range(-1.5f, 1.5f)) * 2;
        lastFlocks.Add(unit.AssignedFlock);
        unit.AssingFlock(flock);
        unit.TargetDestination = targetDestinations?.GetStartDestination(unit.transform, targetPositions.Length);
        flockUnits.Add(unit);
    }
    protected bool OnCharacterExit(Collider other)
    {
        Character characterToMove = other.GetComponentInParent<Character>();
        if (characterToMove == null)
            return false;
        if (deactivePlayerAbilities)
        {
            if (other.GetComponentInParent<PlayerController>() != null)
                deactiveObject.SetActive(false);
        }
        characterToMove.ExitExternalForce();
        int index = this.charactersToMove.IndexOf(characterToMove);
        charactersToMove.RemoveAt(index);
        refVelocitiesDirection.RemoveAt(index);
        directions.RemoveAt(index);
        targetIndex.RemoveAt(index);
        streamSFXEmitter?.Stop();
        return true;
    }
#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        if (Application.isPlaying)
            return;
        if (!showGuide || !useCurves)
            return;
        Transform[] tempTargetPositions = targetPostionsParent.GetComponentsInChildren<Transform>(true);
        targetPositions = CreatePathValues(tempTargetPositions);
        Gizmos.color = guideColor;
        for (int i = 0; i < originalPositionLength - 1; i++)
        {
            Vector3 startPosition = tempTargetPositions[i + 1].position;
            for (int j = 1; j < lineParts + 1; j++)
            {
                Vector3 targetPosition = GetOceanTargetPositionWithCurves(startPosition, startPosition, ref i, ((float)j) / (lineParts));
                Gizmos.DrawLine(startPosition, targetPosition);
                startPosition = targetPosition;
            }
        }
    }
#endif
}