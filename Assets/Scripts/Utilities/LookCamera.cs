using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
[ExecuteInEditMode]
#endif
public class LookCamera : MonoBehaviour
{
    [SerializeField] private Vector3 offset;
    [SerializeField] private Transform cameraTransform;
    private void Start()
    {
        cameraTransform = Camera.main.transform;
    }
    private void Update()
    {
        if (cameraTransform == null)
            return;
        Vector3 target = cameraTransform.position;
        target.y = transform.position.y;
        transform.forward = (transform.position - target).normalized;
    }
}
