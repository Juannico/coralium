using UnityEngine;

public abstract class TickFixedUpdate : MonoBehaviour
{
    [SerializeField] private int baseFrameWait = 3;
    [SerializeField] private int maxFrameWait = 30;
    protected float deltaTime;
    private bool reduceFrameRate;
    private int frameRateCount;
    private int frameWait;
    private float lastOrthograpichSize;
    private float minDistance;
    protected Transform playerTransform;
    private bool doTick;
    protected float offset;
    protected virtual void Start()
    {
        doTick = true;
        if (playerTransform == null)
        {
            if (GameObject.FindGameObjectWithTag(Tags.Player) != null)
                playerTransform = GameObject.FindGameObjectWithTag(Tags.Player).transform;
        }
        frameRateCount = Time.frameCount;
        if (Camera.main.orthographicSize != lastOrthograpichSize)
        {
            lastOrthograpichSize = Camera.main.orthographicSize;
            float aspectRatio = (float)Screen.width / Screen.height;

            float minXDistance = aspectRatio * lastOrthograpichSize;
            float minYDistance = 2 * lastOrthograpichSize / aspectRatio;
            minDistance = Mathf.Sqrt(minXDistance * minXDistance + minYDistance * minYDistance);
        }
    }
    private void FixedUpdate()
    {
        if (!ContinueCondition())
            return;
        if (playerTransform != null && doTick)
        {
            if (Camera.main.orthographicSize != lastOrthograpichSize)
            {
                lastOrthograpichSize = Camera.main.orthographicSize;
                float aspectRatio = Screen.width / Screen.height;
                float minXDistance = aspectRatio * lastOrthograpichSize;
                float minYDistance = 2 * lastOrthograpichSize / aspectRatio;
                minDistance = Mathf.Sqrt(minXDistance * minXDistance + minYDistance * minYDistance);
            }
            Vector2 cameraPosition = Camera.main.transform.position;
            float distance = Vector2.Distance(cameraPosition, transform.position) + offset;
            reduceFrameRate = distance > minDistance;
            frameWait = Mathf.FloorToInt(distance / minDistance) * baseFrameWait;
            if (frameWait > maxFrameWait)
                frameWait = maxFrameWait;
        }
        deltaTime = Time.fixedDeltaTime;
        if (reduceFrameRate)
        {
            if (frameRateCount + frameWait > Time.frameCount)
                return;
            frameRateCount = Time.frameCount;
            deltaTime *= frameWait;
        }
        SelfFixedUpdate();
    }

    protected abstract bool ContinueCondition();
    protected abstract void SelfFixedUpdate();
    public void SetTick(bool tickState) => doTick = tickState;
}
