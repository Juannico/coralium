using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProvideGrassShaderValues : MonoBehaviour
{
    [SerializeField] private float radius = 5.7f;
    private Material material;
    private Transform targetTransform;
    private void Awake()
    {
        material = GetComponent<Renderer>().material;
        GameObject player = GameObject.FindGameObjectWithTag(Tags.Player);
        if (player == null)
        {
            enabled = false;
            return;
        }
        targetTransform = player.transform;
        material.SetFloat("_Radius", radius);
        Vector3 randomVector = MathUtilities.RandomVector(-Vector3.one, Vector3.one);
        material.SetVector("_RandomDirection", randomVector);

    }
    private void Update()
    {
        material.SetVector("_TrackedPosition", targetTransform.position);
        if (Vector3.Distance(targetTransform.position, transform.position) > radius * 2f)
            material.SetVector("_AvoidDirection", Vector3.Cross(targetTransform.forward, (transform.position - targetTransform.position)).y > 0 ? Vector3.down : Vector3.up);


    }
}
