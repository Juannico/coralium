using UnityEngine;

public class SetLinePosition : MonoBehaviour
{
    [SerializeField] private Transform targetTransform;
    [SerializeField] private int linePosition = 1;
    private LineRenderer lineRenderer;
    void Start()
    {
        lineRenderer = GetComponent<LineRenderer>();
    }
    void Update()
    {
        if (lineRenderer == null)
            return;
        lineRenderer.SetPosition(0, transform.position);
        lineRenderer.SetPosition(linePosition, targetTransform.position);
    }
}
