using PixelCrushers;
using UnityEngine;

public static class LocalizeUIUtilities
{
    public static void SetLocalizeUI(TextTableFieldName textTableFieldName, LocalizeUI localizeUI)
    {
        localizeUI.textTable = textTableFieldName.TextTable;
        localizeUI.fieldName = textTableFieldName.FieldName;
        localizeUI.UpdateText();
        if (localizeUI.textMeshProUGUI != null)
        {
            localizeUI.textMeshProUGUI.color = textTableFieldName.TextColor;
            return;
        }
        if (localizeUI.gameObject.activeInHierarchy)
            localizeUI.StartCoroutine(CoroutineHandler.ExecuteActionAfterUpdateUntil(() => localizeUI.textMeshProUGUI != null, () => localizeUI.textMeshProUGUI.color = textTableFieldName.TextColor));
    }
}
