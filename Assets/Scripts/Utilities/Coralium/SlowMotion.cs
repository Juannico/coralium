using System;
using UnityEngine;

public class SlowMotion : MonoBehaviour
{
    [SerializeField] private float slowMotionDuration;
    [SerializeField] private float slowTimeScale;
    [SerializeField] private float transitionTime = 0.2f;
    private float startTimeScale;
    private float startFixedDeltaTime;
    public bool IsInSlowMotion { get; private set; }
    private void Start()
    {
        startTimeScale = Time.timeScale;
        startFixedDeltaTime = Time.fixedDeltaTime;
    }

    public void StarSlowMotion(float duration = 0)
    {
        IsInSlowMotion = true;
        SetSlowMotion(slowTimeScale, startFixedDeltaTime * slowTimeScale, () => SetSlowMotion(startTimeScale, startFixedDeltaTime));
    }

    public void SetSlowMotion(float targetTimeScale, float targetFixedDeltaTime, Action action = null)
    {
        float timer = 0;
        float startSlowtImeScale = Time.timeScale;
        float startSlowFixedDeltaTime = Time.fixedDeltaTime;
        StartCoroutine(CoroutineHandler.ExecuteActionAfterUpdateUntil(() =>
            {
                timer += Time.deltaTime;
                Time.timeScale = Mathf.Lerp(startSlowtImeScale, targetTimeScale, timer / transitionTime);
                Time.fixedDeltaTime = Mathf.Lerp(startSlowFixedDeltaTime, targetFixedDeltaTime, timer / transitionTime);
                return timer > transitionTime;
            }, () =>
            {
                Time.timeScale = targetTimeScale;
                Time.fixedDeltaTime = targetFixedDeltaTime;
                action?.Invoke();
                IsInSlowMotion = false;
            }));
    }
}
