using UnityEngine;

public static class Constants
{
    public static Color DamageColor = Color.red;
    public static float DamageBlinkDuration = 0.75f;
    public static float LayerIgnoreEcolocalizationIndex = 17;
    public static string GamepadString = "gamepad";
    public static string KBMString = "keyboardmouse";
    public static float halfSecondDelay = 0.5f;
    public static float OneSecondDelay = 1f;
}
public static class Tags 
{
    public static string PlayerVirtualCamera = "PlayerVirtualCamera";
    public static string LockOnCamera = "LockOnCamera";
    public static string OceanCurrentCamera = "OceanCurrentCamera";
    public static string CameraController = "CameraController";
    public static string Player = "Player";
    public static string CoralPanning = "CoralPanning";
    public static string KinematicsHandler = "KinematicsHandler";
    public static string MainCanvas = "MainCanvas";
    public static string FlocksHandle = "FlockHandle";
    public static string ControlFOG = "ControlFOG";
    public static string MinimapCamera = "MinimapCamera";
    public static string GeneralLight = "GeneralLight";
}

public enum DamageTypes
{
    Basic,
    Poison,
    Electric
}
public enum AbilitiesBehaviour
{
    EcolocalizationBehaviour,
    ElectrickShockBehaviour,
    ShootBehaviour,
    ShootElectricSphereAbilityBehaviour,
    SphereFollowBehaviour,
}

