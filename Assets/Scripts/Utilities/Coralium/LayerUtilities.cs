using UnityEngine;

public static class LayerUtilities 
{
    private static bool ContainsLayer(LayerMask layerMask, int layer) => (1 << layer & layerMask) != 0;
    public static bool IsSameLayer(LayerMask layerMask, int intLayer) => ContainsLayer(layerMask, intLayer);
}
