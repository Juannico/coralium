using UnityEngine;
using UnityEditor;
#if (UNITY_EDITOR)
using UnityEditor.SceneManagement;

[ExecuteInEditMode]
#endif
public class InstancePrefabEditorMode : MonoBehaviour
{
    Character character;
    GameObject templModel;
    GameObject beforTempModel;
    void Awake()
    {
        DestroyModel();
        character = GetComponent<Character>();
        DestroyModel();
    }
    private void OnDisable()
    {
        DestroyModel();
    }
    private void OnEnable()
    {
        DestroyModel();
    }
    private void DestroyModel()
    {
        Transform[] transforms = GetComponentsInChildren<Transform>();
        for (int i = 0; i < transforms.Length; i++)
        {
            if (transforms[i] == null)
                continue;
            if (transforms[i].name == "EditPrebabGuide")
                DestroyImmediate(transforms[i].gameObject);
        }
    }
#if (UNITY_EDITOR)
    private void Update()
    {
        if (EditorApplication.isPlaying)
        {
            DestroyModel();
            enabled = false;
            return;
        }
        if (PrefabStageUtility.GetCurrentPrefabStage() != null)
            return;
        if(character == null)
            character = GetComponent<Character>();
        if (templModel == null)
        {
            templModel = Instantiate(character.BaseCharacter.ModelPrefab, transform.position + character.BaseCharacter.ModelPrefab.transform.position, character.BaseCharacter.ModelPrefab.transform.rotation, transform);
            templModel.name = "EditPrebabGuide";
            beforTempModel = character.BaseCharacter.ModelPrefab;
        }
        if (beforTempModel != character.BaseCharacter.ModelPrefab)
        {
            DestroyImmediate(templModel);
            templModel = Instantiate(character.BaseCharacter.ModelPrefab, transform.position + character.BaseCharacter.ModelPrefab.transform.position, character.BaseCharacter.ModelPrefab.transform.rotation, transform);
            templModel.name = "EditPrebabGuide";
            beforTempModel = character.BaseCharacter.ModelPrefab;

        }
    }
#endif
}


