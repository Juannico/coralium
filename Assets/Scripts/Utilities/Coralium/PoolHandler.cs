using System;
using System.Collections.Generic;
using UnityEngine;

public class PoolHandler : MonoBehaviour
{
    private List<GameObject> objectPool;
    private GameObject objectToPool;
    private Transform parentTransform;
    private bool rename;
    public void CreatePool(GameObject objectToPool, int numberOfObjects, Transform parentTransform = null, bool rename = false)
    {
        objectPool = new List<GameObject>();
        this.objectToPool = objectToPool;
        this.parentTransform = parentTransform;
        this.rename = rename;
        for (int i = 0; i < numberOfObjects; i++)
        {
            GameObject instantiate = Instantiate(objectToPool, transform);
            instantiate.SetActive(false);
            if (rename)
                instantiate.name = $"{i + 1}_{objectToPool.name}";
            instantiate.transform.parent = this.parentTransform;
            objectPool.Add(instantiate);

        }
    }

    public GameObject GetObject(GameObject objectToPool = null)
    {
        for (int i = 0; i < objectPool.Count; i++)
        {
            if (objectPool[i].activeInHierarchy)
                continue;
            objectPool[i].SetActive(true);
            return objectPool[i];
        }
        return GetInstance(objectToPool);
    }

    public T GetObjectByType<T>(Type UIType, T objectToPool) where T : Component
    {
        for (int i = 0; i < objectPool.Count; i++)
        {
            if (objectPool[i].activeSelf)
                continue;
            if (objectPool[i].GetComponent(UIType) == null)
                continue;
            objectPool[i].SetActive(true);
            return objectPool[i].GetComponent<T>();
        }
        return GetInstance(objectToPool.gameObject).GetComponent<T>();
    }

    private GameObject GetInstance(GameObject objectToPool)
    {
        if (objectToPool == null)
            objectToPool = this.objectToPool;
        if (objectToPool == null)
        {
            Debug.LogError("Don't set object to pool by deafault when created the pool.");
            return null;
        }
        GameObject instantiate = Instantiate(objectToPool, transform);
        instantiate.transform.SetParent(parentTransform, false);
        if (rename)
            instantiate.name = $"{objectPool.Count + 1}_{objectToPool.name}";
        instantiate.SetActive(true);
        objectPool.Add(instantiate);
        return instantiate;
    }

    public T InstantiateByType<T>(T objectToPool) where T : Component => Instantiate(objectToPool);
}
