﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Assertions;

public class CoroutineHandler : MonoBehaviour
{
    /// <summary>
    /// This method will execute a coroutine passed by parameter after the duration passed by parameter
    /// </summary>
    /// <param name="action"></param>
    /// <param name="duration"></param>
    /// <param name="useUnescaledTimte"></param>
    /// <returns></returns>
    public static IEnumerator ExecuteActionAfterEnumerator(Action action, float duration, bool useUnescaledTimte = false)
    {
        if (!useUnescaledTimte)
        {
            yield return new WaitForSeconds(duration);
            action?.Invoke();
            yield break;
        }
        float startTime = Time.realtimeSinceStartup;
        float elapsedTime = 0f;

        while (elapsedTime < duration)
        {
            elapsedTime = Time.realtimeSinceStartup - startTime;
            yield return null;
        }
        action?.Invoke();

    }

    public static void ExecuteActionAfter(Action action, float duration, MonoBehaviour objectCalling)
    {
        objectCalling.StartCoroutine(ExecuteActionAfterEnumerator(action, duration));
    }
    public static void ExcuteActionAfterUnescaledTime(Action action, float duration, MonoBehaviour objectCalling)
    {
        objectCalling.StartCoroutine(ExecuteActionAfterEnumerator(action, duration, true));
    }
    /// <summary>
    /// A "virtual" update where an action is repeated until the condition passed by paramter is met
    /// </summary>
    /// <param name="actionToRepeat"></param>
    /// <param name="exitCondition"></param>
    /// <returns></returns>
    public static IEnumerator UpdateUntil(Action actionToRepeat, bool exitCondition)
    {
        while (!exitCondition)
        {
            actionToRepeat?.Invoke();
            yield return null;
        }
    }


    /// <summary>
    /// A "virtual" update where an action is repeated until the condition passed by paramter is met
    /// </summary>
    /// <param name="actionToRepeat"></param>
    /// <param name="exitCondition"></param>
    /// <param name="actionAfter"></param>
    /// <returns></returns>
    public static IEnumerator ExecuteActionAfterUpdateUntil(Func<bool> actionToRepeat, Action actionAfter = null)
    {
        bool exitCondition = false;
        Assert.IsNotNull(actionToRepeat);
        while (!exitCondition)
        {
            exitCondition = (bool)(actionToRepeat?.Invoke());
            yield return null;
        }
        actionAfter?.Invoke();
    }

    /// <summary>
    /// This method will execute an action a number of times with a set interval of time in between them until the number of executions is met
    /// </summary>
    /// <param name="repeatableAction"></param>
    /// <param name="exitCondition"></param>
    /// <param name="maxTimeBetweenRepeats"></param>
    /// <returns></returns>
    public static IEnumerator RepeatFor(Action repeatableAction, int timesToExecute, float maxTimeBetweenRepeats, bool randomTime = false)
    {
        int countTimes = 0;
        float lastUpdate = 0f;
        float realTimeBetweenRepeats = maxTimeBetweenRepeats;

        //If it is set to be a randomTime then change the time 
        if (randomTime)
        {
            realTimeBetweenRepeats = UnityEngine.Random.Range(0.1f, maxTimeBetweenRepeats);
        }
        while (countTimes < timesToExecute)
        {
            if (realTimeBetweenRepeats < Time.time - lastUpdate)
            {
                repeatableAction?.Invoke();
                //Change the time
                if (randomTime)
                {
                    realTimeBetweenRepeats = UnityEngine.Random.Range(0.1f, maxTimeBetweenRepeats);
                }
                lastUpdate = Time.time;
                countTimes++;
            }
            yield return null;
        }
    }

    /// <summary>
    /// This method will execute an action for a set period of time. Thin of it like an Update that stops running after a certain amount of seconds have passed
    /// </summary>
    /// <param name="repeatableAction"></param>
    /// <param name="exitCondition"></param>
    /// <param name="maxTimeBetweenRepeats"></param>
    /// <returns></returns>
    public static IEnumerator RepeatForSeconds(Action repeatableAction, float timesInSeconds)
    {
        float lastUpdate = Time.time;

        while (timesInSeconds > Time.time - lastUpdate)
        {
            repeatableAction?.Invoke();
            yield return null;
        }

    }
}
