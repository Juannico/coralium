using UnityEngine;

public static class TargetUtilities
{
    public static int GetTargetsAmount(Transform transform, float radius, LayerMask layer, ref RaycastHit[] raycastHits) => Physics.SphereCastNonAlloc(transform.position, radius, transform.forward, raycastHits, 0f, layer);
    public static int GetTargetsAmount(Vector3 position,Vector3 direction, float maxDistance, LayerMask layer, ref RaycastHit[] raycastHits) => Physics.SphereCastNonAlloc(position, maxDistance, direction, raycastHits, 0f, layer);
    public static Transform GetTargetpositionByRaycastHits(Transform transform, RaycastHit[] raycastHits)
    {
        Transform enemyTransform = raycastHits[0].transform;
        float shortestDistance = Vector3.Distance(raycastHits[0].transform.position, transform.position);
        for (var i = 1; i < raycastHits.Length; i++)
        {
            if (raycastHits[i].transform == null)
                return enemyTransform;
            float distance = Vector3.Distance(raycastHits[i].transform.position, transform.position);
            if (distance < shortestDistance)
            {
                shortestDistance = distance;
                enemyTransform = raycastHits[i].transform;
            }
        }
        return enemyTransform;
    }
    internal static Transform GetTargetpositionByRaycastHits(Transform transform, RaycastHit[] raycastHits, Transform camera, float FOVAngle)
    {
        Transform enemyTransform = null;
        float shortestDistance = 5000f;
        for (int i = 0; i < raycastHits.Length; i++)
        {
            if (raycastHits[i].transform == null)
                continue;
            float distance = Vector3.Distance(raycastHits[i].transform.position, transform.position);
            if (!IsInFOV(camera.forward, camera.position, raycastHits[i].transform.position, FOVAngle))
                continue;
            if (distance < shortestDistance)
            {
                shortestDistance = distance;
                enemyTransform = raycastHits[i].transform;
            }
        }
        return enemyTransform;
    }
    public static bool IsInFOV(Vector3 viewDirection, Vector3 viewPosition, Vector3 targetPosition, float angle)
    {
        Vector3 directionToTarget = targetPosition - viewPosition;
        return IsInFOV(viewDirection, directionToTarget, angle);
    }
    internal static bool IsInFOV(Vector3 viewDirection, Vector3 directionToTarget, float angle)
    {
        float currentAngle = Vector3.Angle(viewDirection, directionToTarget);
        return currentAngle < angle / 2;
    }
}
