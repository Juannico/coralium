using NaughtyAttributes;
using UnityEngine;

public class RigibodyForceHandle : MonoBehaviour
{
    public Rigidbody Rb { get; private set; }
    [ReadOnly] public Vector3 VelocityForceVector;
    [ReadOnly] public bool AffectByExternalForce = true;
    [ReadOnly] public Vector3 ExternalVelocityForceVector;
    [SerializeField] private float affectByExternalForceVelocityMultiplier = 0.75f;
    [SerializeField] private bool autoInitialize;
    [ShowIf("autoInitialize")] [SerializeField] private Rigidbody autoRigyBody;
    private void Awake()
    {
        if (autoInitialize)
            Initialize(autoRigyBody);
    }
    public void Initialize(Rigidbody rigidbody)
    {
        this.Rb = rigidbody;
    }
    private void FixedUpdate()
    {
        if (Rb == null)
            throw new System.Exception($"{gameObject.name} Doesn�t have rigybody componet");
        if (!AffectByExternalForce)
            ExternalVelocityForceVector = Vector3.zero;
        else
            VelocityForceVector *= affectByExternalForceVelocityMultiplier;
        if (VelocityForceVector == Vector3.zero && ExternalVelocityForceVector == Vector3.zero)
        {
            VelocityForceVector = Vector3.zero;
            ExternalVelocityForceVector = Vector3.zero;
            return;
        }
        Rb.AddForce((VelocityForceVector + ExternalVelocityForceVector) - Rb.velocity, ForceMode.VelocityChange);
        VelocityForceVector = Vector3.zero;
        ExternalVelocityForceVector = Vector3.zero;
    }
    public void StopMove()
    {
        VelocityForceVector = Vector3.zero;
        ExternalVelocityForceVector = Vector3.zero;
        Rb.velocity = Vector3.zero;
    }
}
