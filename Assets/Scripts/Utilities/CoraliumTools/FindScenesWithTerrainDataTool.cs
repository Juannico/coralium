#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;
using System.IO;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class FindScenesWithTerrainDataTool : EditorWindow
{
    private TerrainData terrainData;
    private Vector2 scrollPosition;
    private GUIStyle style;
    private static bool scenesFound = false;
    List<string> scenesWithTerrain;
    [MenuItem("Assets/Get Scenes with Terrain Data", false, 1)]
    private static void FindScenesWithTerrainDataWindow()
    {
        FindScenesWithTerrainDataTool window = GetWindow<FindScenesWithTerrainDataTool>("Scenes With Terrain Data");
        window.terrainData = (TerrainData)Selection.activeObject;
        window.Show();
        scenesFound = false;
    }
    private void OnGUI()
    {
        if (!scenesFound)
        {
            GUILayout.Space(5);
            FindScenesWithTerrain();

            scenesFound = true;
        }
        DisplayScenes(scenesWithTerrain);
        if (GUILayout.Button("Close"))
            Close();
    }

    private void FindScenesWithTerrain()
    {
        if (terrainData == null)
        {
            EditorUtility.DisplayDialog("", "Selected item is not a Terrain Data!", "OK");
            Close();
            return;
        }
        scenesWithTerrain = new List<string>();

        string[] sceneGuids = AssetDatabase.FindAssets("t:Scene");
        foreach (string sceneGuid in sceneGuids)
        {
            string scenePath = AssetDatabase.GUIDToAssetPath(sceneGuid);
            if (string.IsNullOrEmpty(scenePath))
                continue;

            if (IsSceneInReadOnlyPackage(scenePath))
                continue;
            EditorSceneManager.OpenScene(scenePath);
            Terrain[] terrains = FindObjectsOfType<Terrain>();
            if (terrains == null || terrains.Length == 0)
                continue;
            foreach (Terrain terrain in terrains)
            {
                if (terrain.terrainData != terrainData)
                    continue;
                scenesWithTerrain.Add(scenePath);
                break;
            }
        }
        if (scenesWithTerrain.Count == 0)
        {
            Close();
            EditorUtility.DisplayDialog("Warning", $"No found Scenes with { terrainData }!", "OK");
            return;
        }
        style = new GUIStyle(GUI.skin.button);
        style.alignment = TextAnchor.MiddleLeft;
        scrollPosition = GUILayout.BeginScrollView(scrollPosition);

        GUILayout.EndScrollView();
    }

    private void DisplayScenes(List<string> scenesWithTerrain)
    {
        if (scenesWithTerrain.Count == 0)
            return;
        GUILayout.BeginVertical();
        GUILayout.Label($"Scenes with {terrainData}:", EditorStyles.boldLabel);
        foreach (string scenPath in scenesWithTerrain)
        {
            string[] paths = scenPath.Split('/');
            string[] file = paths[paths.Length - 1].Split('.');
            string label = file[0];
            if (GUILayout.Button(label, style))
                EditorSceneManager.OpenScene(scenPath);
        }
        GUILayout.Space(5);
        GUILayout.EndVertical();
    }

    private bool IsSceneInReadOnlyPackage(string path) => UnityEditor.PackageManager.PackageInfo.FindForAssetPath(path) != null;
}
#endif
