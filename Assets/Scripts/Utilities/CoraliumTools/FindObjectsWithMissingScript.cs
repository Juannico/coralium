#if UNITY_EDITOR
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class FindObjectsWithMissingScript : EditorWindow
{
    private static List<GameObject> matchingObjects = new List<GameObject>();
    private Vector2 scrollPosition;
    [MenuItem("GameObject / Utilities / Find object with missing Script")]
    public static void ShowWindow()
    {
        GetWindow(typeof(FindObjectsWithMissingScript));
    }
    private void OnGUI()
    {
        GUILayout.Label("Find Objects With Missing Script", EditorStyles.boldLabel);
        Find();
        GUILayout.Space(10);
        if (matchingObjects.Count > 0)
        {
            GUILayout.Label("Matching Objects:");

            scrollPosition = EditorGUILayout.BeginScrollView(scrollPosition);

            foreach (GameObject go in matchingObjects)
            {
                if (GUILayout.Button(go.name))
                    Selection.activeObject = go;
            }
            EditorGUILayout.EndScrollView();
            return;
        }
        Close();
    }

    private static void Find()
    {
        matchingObjects.Clear();
        GameObject[] go = Selection.gameObjects;
        foreach (GameObject g in go)
        {
            matchingObjects.Clear();
            Transform[] children = g.GetComponentsInChildren<Transform>();
            foreach (var child in children)
            {
                Component[] components = child.GetComponents<Component>();
                foreach (var component in components)
                {
                    if (component != null)
                        continue;
                    matchingObjects.Add(child.gameObject);
                    break;
                }
            }
        }
        if (matchingObjects.Count == 0)
            EditorUtility.DisplayDialog("", $"No objects with missing script", "OK");
    }
}

#endif