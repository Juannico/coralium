#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;
using System.Collections.Generic;
using UnityEngine.VFX;
using System;

public class FindObjectWithVIsualEffect : EditorWindow
{
    private VisualEffectAsset visualEffect;
    private Vector2 scrollPosition;
    private GUIStyle style;
    private List<string> scenesWithTerrain;
    private List<GameObject> matchingObjects;
    [MenuItem("Assets/Get Scenes and Object with VisualEffec", false, 1)]
    private static void FindScenesWithVisualEffectDataWindow()
    {
        FindObjectWithVIsualEffect window = GetWindow<FindObjectWithVIsualEffect>("Scenes and Object with VisualEffec");
        window.visualEffect = (VisualEffectAsset)Selection.activeObject;
        window.Show();
    }
    private void OnGUI()
    {
        if (visualEffect == null)
        {
            EditorUtility.DisplayDialog("", "Selected item is not a Terrain Data!", "OK");
            Close();
            return;
        }
        visualEffect = EditorGUILayout.ObjectField("Material To Find:", visualEffect, typeof(VisualEffectAsset), false) as VisualEffectAsset;
        style = new GUIStyle(GUI.skin.button);
        style.alignment = TextAnchor.MiddleLeft;
        scrollPosition = GUILayout.BeginScrollView(scrollPosition);
        if (GUILayout.Button("Find GameObject"))
            Find();
        DisplayObjects();
        if (GUILayout.Button("Find Scenes"))
            FindScenesWithTVisualEffect();
        DisplayScenes();
        EditorGUILayout.EndScrollView();
        if (GUILayout.Button("Close"))
            Close();
        if (GUILayout.Button("Debug Message"))
            ShowDebugMessage();
    }

    private void ShowDebugMessage()
    {
        string message = "";
        if (scenesWithTerrain != null)
        {
            if (scenesWithTerrain.Count > 0)
                message = $"{message} Scenes:\n";
            foreach (string scenPath in scenesWithTerrain)
            {
                string[] paths = scenPath.Split('/');
                string[] file = paths[paths.Length - 1].Split('.');
                string label = file[0];
                message = $"{message}     * {label}.\n";
            }
        }
        if (matchingObjects != null)
        {
            if (matchingObjects.Count > 0)
                message = $"{message} GameObjects:\n";
            foreach (GameObject go in matchingObjects)
            {
                    message = $"{message}     * {go.name}.\n";
            }
        }
        if (string.IsNullOrEmpty(message))
        {
            EditorUtility.DisplayDialog("Warning", $"No message to show!", "OK");
            return;
        }
        message = $"--- {visualEffect.name} ---\n {message}";
        Debug.Log(message);
    }

    private void FindScenesWithTVisualEffect()
    {
        scenesWithTerrain = new List<string>();

        string[] sceneGuids = AssetDatabase.FindAssets("t:Scene");
        foreach (string sceneGuid in sceneGuids)
        {
            string scenePath = AssetDatabase.GUIDToAssetPath(sceneGuid);
            if (string.IsNullOrEmpty(scenePath))
                continue;

            if (IsSceneInReadOnlyPackage(scenePath))
                continue;
            EditorSceneManager.OpenScene(scenePath);
            VisualEffect[] visualEffects = FindObjectsOfType<VisualEffect>(true);
            if (visualEffects == null || visualEffects.Length == 0)
                continue;
            foreach (VisualEffect visualEffect in visualEffects)
            {
                if (visualEffect.visualEffectAsset != this.visualEffect)         
                    continue;                         
                if (PrefabUtility.IsPartOfAnyPrefab(visualEffect.gameObject))
                    continue;
                scenesWithTerrain.Add(scenePath);
                break;
            }
        }
        if (scenesWithTerrain.Count == 0)
        {
            EditorUtility.DisplayDialog("Warning", $"No found Scenes with { visualEffect.name }!", "OK");
            return;
        }
    }
    private void Find()
    {
        matchingObjects = new List<GameObject>();
        string[] guids = AssetDatabase.FindAssets("t:GameObject");

        foreach (string guid in guids)
        {
            string assetPath = AssetDatabase.GUIDToAssetPath(guid);
            GameObject go = AssetDatabase.LoadAssetAtPath<GameObject>(assetPath);

            VisualEffect[] visualEffects = go.GetComponentsInChildren<VisualEffect>();

            foreach (VisualEffect visualEffect in visualEffects)
            {
                if (visualEffect.visualEffectAsset == this.visualEffect)
                {

                    matchingObjects.Add(go);
                    break;
                }
            }
        }

        if (matchingObjects.Count == 0)
        {
            EditorUtility.DisplayDialog("Warning", $"No found GameObjects with { visualEffect.name }!", "OK");
            return;
        }
    }
    private void DisplayScenes()
    {
        if (scenesWithTerrain == null)
            return;
        if (scenesWithTerrain.Count == 0)
            return;
        GUILayout.BeginVertical();
        GUILayout.Label($"Scenes with {visualEffect}:", EditorStyles.boldLabel);
        foreach (string scenPath in scenesWithTerrain)
        {
            string[] paths = scenPath.Split('/');
            string[] file = paths[paths.Length - 1].Split('.');
            string label = file[0];
            if (GUILayout.Button(label, style))
                EditorSceneManager.OpenScene(scenPath);
        }
        GUILayout.Space(5);
        GUILayout.EndVertical();
    }
    private void DisplayObjects()
    {
        if (matchingObjects == null)
            return;
        if (matchingObjects.Count == 0)
            return;
        GUILayout.BeginVertical();
        GUILayout.Label($"GameObjects with {visualEffect}:");
        foreach (GameObject go in matchingObjects)
        {
            if (GUILayout.Button(go.name, style))
                Selection.activeObject = go;
        }
        GUILayout.Space(5);
        GUILayout.EndVertical();
    }
    private bool IsSceneInReadOnlyPackage(string path) => UnityEditor.PackageManager.PackageInfo.FindForAssetPath(path) != null;
}
#endif