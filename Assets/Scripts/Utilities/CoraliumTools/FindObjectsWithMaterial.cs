#if UNITY_EDITOR
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class FindObjectsWithMaterial : EditorWindow
{

    public Material MaterialToFind;

    private List<GameObject> matchingObjects = new List<GameObject>();
    private Vector2 scrollPosition;

    [MenuItem("Tools/Find/ Objects with Material")]
    public static void ShowWindow()
    {
        GetWindow(typeof(FindObjectsWithMaterial));
    }

    private void OnGUI()
    {
        GUILayout.Label("Find Objects with Material", EditorStyles.boldLabel);

        MaterialToFind = EditorGUILayout.ObjectField("Material To Find:", MaterialToFind, typeof(Material), false) as Material;

        if (GUILayout.Button("Find Objects"))
            Find();
        GUILayout.Space(10);

        if (matchingObjects.Count > 0)
        {
            GUILayout.Label("Matching Objects:");

            scrollPosition = EditorGUILayout.BeginScrollView(scrollPosition);

            foreach (GameObject go in matchingObjects)
            {
                if (GUILayout.Button(go.name))
                    Selection.activeObject = go;
            }

            EditorGUILayout.EndScrollView();
        }
    }

    public void Find(Material materialToFind = null)
    {
        if (materialToFind != null)
            MaterialToFind = materialToFind;
        matchingObjects.Clear();

        string[] guids = AssetDatabase.FindAssets("t:GameObject");

        foreach (string guid in guids)
        {
            string assetPath = AssetDatabase.GUIDToAssetPath(guid);
            GameObject go = AssetDatabase.LoadAssetAtPath<GameObject>(assetPath);

            Renderer[] renderers = go.GetComponentsInChildren<Renderer>();

            foreach (Renderer renderer in renderers)
            {
                if (renderer.sharedMaterial == MaterialToFind)
                {
                    matchingObjects.Add(go);
                    break;
                }
            }
        }

        if (matchingObjects.Count == 0)
            EditorUtility.DisplayDialog("",$"No objects found using material {MaterialToFind.name}", "OK");
    }
}
#endif