#if UNITY_EDITOR
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class FindMaterialsByShader : EditorWindow
{

    public Shader ShaderToFind;

    private List<Material> matchingMaterials = new List<Material>();
    private Vector2 scrollPosition;

    [MenuItem("Tools/Find/ Materials with Shader")]
    public static void ShowWindow()
    {
        GetWindow(typeof(FindMaterialsByShader));
    }

    private void OnGUI()
    {
        GUILayout.Label("Find Materials by Shader", EditorStyles.boldLabel);

        ShaderToFind = EditorGUILayout.ObjectField("Shader To Find:", ShaderToFind, typeof(Shader), false) as Shader;

        if (GUILayout.Button("Find Materials"))
            Find();
        GUILayout.Space(10);
       
        if (matchingMaterials.Count > 0)
        {
            GUILayout.Label("Matching Materials:");

            scrollPosition = EditorGUILayout.BeginScrollView(scrollPosition);

            foreach (Material material in matchingMaterials)
            {
                GUILayout.BeginHorizontal();
                if (GUILayout.Button(material.name)) 
                    Selection.activeObject = material;
                if (GUILayout.Button($"Find GameObject with <b>{ material.name}</b> material "))
                {
                    FindObjectsWithMaterial findObjectsWithMaterial = EditorWindow.GetWindow<FindObjectsWithMaterial>();
                    findObjectsWithMaterial.Find(material);
                }
                GUILayout.EndHorizontal();
            }
            EditorGUILayout.EndScrollView();
            GUI.skin.button.richText = true;
        }
    }

    private void Find()
    {

        matchingMaterials.Clear();

        string[] guids = AssetDatabase.FindAssets("t:Material");

        foreach (string guid in guids)
        {
            string assetPath = AssetDatabase.GUIDToAssetPath(guid);
            Material material = AssetDatabase.LoadAssetAtPath<Material>(assetPath);

            if (material.shader == ShaderToFind)
                matchingMaterials.Add(material);
        }
        if (matchingMaterials.Count == 0)
            EditorUtility.DisplayDialog("",$"No materials found using shader {ShaderToFind.name}", "OK");
    }

}
#endif