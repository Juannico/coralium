#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System.IO;

public class FolderSizeTool : EditorWindow
{
    private string FolderPath = "";
    private Vector2 scrollPosition;
    GUIStyle style;
    [MenuItem("Assets/Calculate Folder Size", false, 1)]
    private static void CalculateFolderSize()
    {
        FolderSizeTool window = GetWindow<FolderSizeTool>("Folder Size");
        window.FolderPath = AssetDatabase.GetAssetPath(Selection.activeObject);
        window.Show();
    }

    private void OnGUI()
    {
        GUILayout.Space(5);
        GUILayout.Label("Folder: " + FolderPath, EditorStyles.boldLabel);
        CalculateSize();
        GUILayout.Space(10);
        if (GUILayout.Button("Close"))
            Close();
    }

    private void CalculateSize()
    {
        if (string.IsNullOrEmpty(FolderPath))
        {
            EditorUtility.DisplayDialog("", "No folder selected!", "OK");
            Close();
            return;
        }
        if (!Directory.Exists(FolderPath))
        {
            EditorUtility.DisplayDialog("", "Selected item is not a folder!", "OK");
            Close();
            return;
        }
        style = new GUIStyle(GUI.skin.button);
        style.alignment = TextAnchor.MiddleLeft;
        DirectoryInfo folderDir = new DirectoryInfo(FolderPath);
        long folderSize = CalculateSize(folderDir);
        GUILayout.Label($"Folder Selectd: {FolderPath}.");
        GUILayout.Label($"Total Size: {FormatSize(folderSize)}");
        GUILayout.Space(10);
        scrollPosition = GUILayout.BeginScrollView(scrollPosition);
        DisplayFilesAndSizes(folderDir,FolderPath);
        GUILayout.EndScrollView();
    }

    private void DisplayFilesAndSizes(DirectoryInfo directory,string path)
    {
        GUILayout.BeginVertical();
        GUILayout.Label($"Files in {directory.Name}:", EditorStyles.boldLabel);
        long folderSize = CalculateSize(directory);
        GUILayout.Label($"Folder size {FormatSize(folderSize)}", EditorStyles.boldLabel);
        FileInfo[] files = directory.GetFiles();
        foreach (var file in files)
        {
            if (file.Extension == ".meta")
                continue;
            
            Object obj = AssetDatabase.LoadAssetAtPath<Object>(path + "/" + file.Name);
            if (obj == null)
            {
                Debug.Log(file.FullName);
                Debug.Log(path);
                continue;
            }
            string label = $"{obj.name} in {directory.Name}. Size: {FormatSize(file.Length)}";
            if (GUILayout.Button(label, style))
                Selection.activeObject = obj;
        }
        DirectoryInfo[] subDirectories = directory.GetDirectories();
        foreach (var subDirectory in subDirectories)
        {
            string subdirectoryPath = $"{path}/{subDirectory.Name}";
            DisplayFilesAndSizes(subDirectory, subdirectoryPath);
        }
        GUILayout.Space(5);
        GUILayout.EndVertical();
    }

    private long CalculateSize(DirectoryInfo directory)
    {
        long size = 0;

        FileInfo[] files = directory.GetFiles();
        foreach (var file in files)
            size += file.Length;
        DirectoryInfo[] subDirectories = directory.GetDirectories();
        foreach (var subDirectory in subDirectories)
            size += CalculateSize(subDirectory);
        return size;
    }
    private string FormatSize(long bytes)
    {
        string[] sizes = { "B", "KB", "MB", "GB" };
        double size = bytes;
        int index = 0;
        while (size >= 1024 && index < sizes.Length - 1)
        {
            size /= 1024;
            index++;
        }
        return string.Format("{0:0.##} {1}", size, sizes[index]);
    }
}
#endif