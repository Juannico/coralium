﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.Internal;
using System.Collections.Generic;
using System;
using UnityEditor;
using TamarilloTools;

public class LoadingScreenManager : MonoBehaviour
{

    [Header("Loading Visuals")]
    public Image loadingIcon;
    public Button loadingDoneButtonn;
    //public Text loadingText;
    public TextMeshProUGUI loadingText;
    public MultipleProgressBars progressBar;

    [Header("Timing Settings")]
    public float waitOnLoadEnd = 0.25f;

    [Header("Loading Settings")]
    public static LoadSceneMode loadSceneMode = LoadSceneMode.Additive;
    public ThreadPriority loadThreadPriority;
    public bool shouldShowVisuals = false;


    AsyncOperation operation;
    Scene currentScene;

    public static int sceneToLoad = 2;
    // IMPORTANT! This is the build index of your loading scene. You need to change this to match your actual scene index
    public static int loadingSceneIndex = 1;

    private static bool loadingLevel = false;
    private static List<AsyncOperation> zonesOperations;
    private static int zonesLoaded;
    public static ZoneManager ZoneManager;
    private static bool firstZoneAdded;
    public static void LoadScene(int levelNum)
    {
        Application.backgroundLoadingPriority = ThreadPriority.High;
        sceneToLoad = levelNum;
        SceneManager.LoadSceneAsync(sceneToLoad);
    }

    public static void LoadSceneByName(string sceneName)
    {
        loadSceneMode = LoadSceneMode.Single;
        Application.backgroundLoadingPriority = ThreadPriority.High;
        sceneToLoad = SceneIndexFromName(sceneName);
        SceneManager.LoadSceneAsync(loadingSceneIndex);
        loadingLevel = false;
    }
    public static void LoadSceneByName(string sceneName, [DefaultValue("LoadSceneMode.Single")] LoadSceneMode mode, bool loadLevel = false)
    {
        loadSceneMode = mode;
        Application.backgroundLoadingPriority = ThreadPriority.High;
        sceneToLoad = SceneIndexFromName(sceneName);
        loadingLevel = loadLevel;
        firstZoneAdded = false;
        zonesLoaded = 0;
        if (zonesOperations == null)
            zonesOperations = new List<AsyncOperation>();
        zonesOperations.Clear();
        SceneManager.LoadSceneAsync(loadingSceneIndex);
    }

    public static void AddOperation(AsyncOperation asyncOperation)
    {
        if (!loadingLevel)
            return;
        firstZoneAdded = true;
        asyncOperation.completed += (asyncOperation) => zonesLoaded++;
        zonesOperations.Add(asyncOperation);
    }

    void Start()
    {
        Time.timeScale = 1;
        if (sceneToLoad < 0)
            return;
        currentScene = SceneManager.GetSceneByBuildIndex(loadingSceneIndex);
        if (loadSceneMode == LoadSceneMode.Single && loadingLevel)
            DontDestroyOnLoad(gameObject);
        StartCoroutine(LoadAsync(sceneToLoad));
    }

    private static string NameFromIndex(int BuildIndex)
    {
        string path = SceneUtility.GetScenePathByBuildIndex(BuildIndex);
        int slash = path.LastIndexOf('/');
        string name = path.Substring(slash + 1);
        int dot = name.LastIndexOf('.');
        return name.Substring(0, dot);
    }

    public static int SceneIndexFromName(string sceneName)
    {
        for (int i = 0; i < SceneManager.sceneCountInBuildSettings; i++)
        {
            string testedScreen = NameFromIndex(i).Trim().ToLower();
            if (testedScreen.Equals(sceneName.Trim().ToLower())) return i;
        }
        return -1;
    }

    private IEnumerator LoadAsync(int levelNum)
    {
        if (shouldShowVisuals)
            ShowLoadingVisuals();
        FadeUI.FadeIn();
        yield return new WaitForSeconds(FadeUI.GetFadeDuration());
        float startTime = Time.realtimeSinceStartup;
        float elapsedTime = 0f;
        float duration = 1f;
        while (elapsedTime < duration)
        {
            elapsedTime = Time.realtimeSinceStartup - startTime;
            yield return null;
        }
        StartOperation(levelNum);

        float lastProgress = 0f;

        // operation does not auto-activate scene, so it's stuck at 0.9
        while (!DoneLoading())
        {
            float progress = GetProgress();
            if (Mathf.Approximately(progress, lastProgress) == false)
            {
                progressBar.FillAmount = progress;
                lastProgress = operation.progress;
            }
            yield return null;
        }
        if (shouldShowVisuals)
            ShowCompletionVisuals();
        // yield return new WaitForSeconds(waitOnLoadEnd);
        startTime = Time.realtimeSinceStartup;
        elapsedTime = 0f;
        duration = waitOnLoadEnd;
        while (elapsedTime < duration)
        {
            elapsedTime = Time.realtimeSinceStartup - startTime;
            yield return null;
        }
        FadeUI.FadeOut();
        yield return new WaitForSeconds(FadeUI.GetFadeDuration() * 0.9f);

        startTime = Time.realtimeSinceStartup;
        elapsedTime = 0f;
        while (elapsedTime < duration)
        {
            elapsedTime = Time.realtimeSinceStartup - startTime;
            yield return null;
        }
        FinishLoading();
    }



    private void StartOperation(int levelNum)
    {
        Application.backgroundLoadingPriority = loadThreadPriority;
        operation = SceneManager.LoadSceneAsync(levelNum, loadSceneMode);
        if (loadSceneMode == LoadSceneMode.Single && !loadingLevel)
            operation.allowSceneActivation = false;
    }

    private bool DoneLoading()
    {
        if (!loadingLevel)
            return (loadSceneMode == LoadSceneMode.Additive && operation.isDone) || (loadSceneMode == LoadSceneMode.Single && operation.progress >= 0.9f);
        if (!firstZoneAdded)
            return false;
        int amountZonesoperations = zonesOperations.Count;
        if (amountZonesoperations < zonesLoaded)
            return false;
        if (ZoneManager == null)
            return false;
        if (ZoneManager.LoadZoneProgressFixed <= 1)
            return false;
        return true;
    }
    private float GetProgress()
    {
        if (!loadingLevel)
            return operation.progress;
        float progress = 0;
        int amountZonesoperations = zonesOperations.Count;
        for (int i = 0; i < amountZonesoperations; i++)
            progress += zonesOperations[i].progress;
        if (amountZonesoperations > 1)
            progress /= amountZonesoperations;
        if (ZoneManager != null)
            progress += ZoneManager.LoadZoneProgressFixed;
        progress /= 2;
        return (progress + operation.progress) / 2;
    }
    void ShowLoadingVisuals()
    {
        loadingIcon.gameObject.SetActive(true);
        loadingDoneButtonn.gameObject.SetActive(false);

        progressBar.FillAmount = 0f;
    }

    void ShowCompletionVisuals()
    {
        //StopAllCoroutines();
        loadingIcon.gameObject.SetActive(false);
        loadingDoneButtonn.gameObject.SetActive(true);
    }

    private void FinishLoading()
    {
        if (loadSceneMode == LoadSceneMode.Additive)
            SceneManager.UnloadSceneAsync(currentScene.name);
        else
        {
            if (loadingLevel)
                Destroy(gameObject);
            operation.allowSceneActivation = true;
        }
        //EditorApplication.isPaused = true;
    }
}
