using NaughtyAttributes;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using UnityEngine.Video;

public class VideoCutsceneHandler : MonoBehaviour
{
    public enum CutsceneType
    {
        New_Scene = 0,
        In_Game
    }

    [SerializeField] private InputActionReference skipCutsceneInputReference;
    [SerializeField] private ButtonPrompt skipButtonPrompt;
    [SerializeField][Scene] private string sceneAfterCutscene;
    [SerializeField][Scene] private string cutsceneScene;
    private VideoPlayer player;
    public CutsceneType cutSceneType;
    // Start is called before the first frame update
    void Start()
    {
        MusicManager musicManager = SoundManager.Instance.MusicManager;
        musicManager?.StopAmbience();
        musicManager?.StopMusic();
        player = GetComponent<VideoPlayer>();

        skipButtonPrompt.SetHoldInputActionReference(skipCutsceneInputReference);

        PlayerInputManager.Instance.SetInputModePlayer();

        PlayerInputManager.Instance.BindInputActionPerformed(skipCutsceneInputReference, OnCutsceneEndAction);

        CoroutineHandler.ExecuteActionAfter(() =>
        {
            OnCutsceneEndAction();
        }, (float)player.clip.length + 1, this);
    }

    private void OnDisable()
    {
        PlayerInputManager.Instance.UnbindInputActionPerformed(skipCutsceneInputReference, OnCutsceneEndAction);
    }
    public void OnCutsceneEndAction(InputAction.CallbackContext ctx = default)
    {
        switch (cutSceneType)
        {
            case CutsceneType.New_Scene:
                NewSceneLoad();
                break;
            case CutsceneType.In_Game:
                EndCutsceneInGame();
                break;
            default:
                break;
        }
    }
    private void NewSceneLoad()
    {
        LoadingScreenManager.LoadSceneByName(sceneAfterCutscene, LoadSceneMode.Additive, true);
        // LoadingScreenManager.LoadSceneByName(sceneAfterCutscene);
    }
    private void EndCutsceneInGame()
    {
        SceneManager.UnloadSceneAsync(cutsceneScene);
    }
}
