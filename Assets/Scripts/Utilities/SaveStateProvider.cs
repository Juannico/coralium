using UnityEngine;
using UnityEngine.Events;

public class SaveStateProvider : MonoBehaviour, ISaveStateJSON
{
    [SerializeField] private string path;
    [SerializeField] private string saveName;
    [SerializeField] private UnityEvent OnStateSaved;

    private void Awake()
    {
        DataManager.UpdateStateInterfaceValue(this);
        if (!State)
            return;
        OnStateSaved?.Invoke();
        enabled = false;
    }

    public void SetState(bool value)
    {
        OnStateSaved?.Invoke();
        State = value;
    }
    #region ISaveStateJSON
    private bool state;
    public bool State
    {
        get => state;
        set
        {
            state = value;
            Save();
        }
    }

    public string KeyName => saveName;

    public string Path => path;
    public void Save() => DataManager.SaveInterface(this);
    #endregion
}
