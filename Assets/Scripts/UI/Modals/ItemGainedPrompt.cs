using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

public class ItemGainedPrompt : MonoBehaviour
{
    [SerializeField] private Inventory inventory;
    [SerializeField] private Image itemImage;
    [SerializeField] private TextMeshProUGUI itemCurrentCountText;
    [SerializeField] private TextMeshProUGUI itemGainedText;
    [SerializeField] private float duration = 2f;

    private bool showingPopUp;
    private Queue<Tuple<CollectableData, int, int>> popUpQueue = new Queue<Tuple<CollectableData, int, int>>();
    private CollectableData[] collectObjects;
    private void Start()
    {
        collectObjects = inventory.Collectables;
        for (int i = 0; i < collectObjects.Length; i++)
            collectObjects[i].OnCollectedWithCollectableData += SetPopUP;
    }
    private void OnDestroy()
    {
        for (int i = 0; i < collectObjects.Length; i++)
            collectObjects[i].OnCollectedWithCollectableData -= SetPopUP;
    }
    private void SetPopUP(int valueAdded, CollectableData collectObject)
    {
        if (valueAdded <= 0)
            return;
        if (!showingPopUp)
        {
            showingPopUp = true;
            ActivatePopUp(collectObject, collectObject.Amount, valueAdded);
            return;
        }
        foreach (Tuple<CollectableData, int, int> tuple in popUpQueue)
        {
            if (collectObject != tuple.Item1)
                continue;
            Tuple<CollectableData, int, int> tupleToModify = popUpQueue.Dequeue();
            Tuple<CollectableData, int, int> updatedTuple = Tuple.Create(tupleToModify.Item1, tupleToModify.Item2, tupleToModify.Item3 + valueAdded);
            popUpQueue.Enqueue(updatedTuple);
            return;
        }
        popUpQueue.Enqueue(Tuple.Create(collectObject, collectObject.Amount, valueAdded));
    }

    private void ActivatePopUp(CollectableData data, int currentAmount, int gained)
    {
        Assert.IsNotNull(data);
        Assert.IsNotNull(itemImage);
        Assert.IsNotNull(itemCurrentCountText);
        Assert.IsNotNull(itemGainedText);

        itemImage.sprite = data.collectableSprite;
        itemCurrentCountText.text = $"{currentAmount}";
        itemGainedText.text = $"+{gained}";

        CoroutineHandler.ExecuteActionAfter(HidePopUp, duration, this);
    }

    private void HidePopUp()
    {
        // #todo Fix Animation
        CoroutineHandler.ExecuteActionAfter(() =>
        {
            if (popUpQueue.Count > 0)
            {
                Tuple<CollectableData, int, int> queauedPopUp = popUpQueue.Dequeue();
                ActivatePopUp(queauedPopUp.Item1, queauedPopUp.Item2, queauedPopUp.Item3);
                return;
            }
            showingPopUp = false;
        }, 5, this);
    }
}
public class PopUpData
{
    public CollectableData Data;
    public int Gained;
}
