using TMPro;
using UnityEngine;

public class GymMarkUI : MarkUI
{
    [SerializeField] private TextMeshProUGUI gymText;
    public void SetText(string text) => gymText.text = text;

}
