using UnityEngine;
public enum MarkUITypes
{
    Polyp,
    RainbowPolyp,
    Enemy,
    GymGuide,
}

public class MarkUI : MonoBehaviour
{
    [SerializeField] public MarkUITypes markType;
    private Transform target;
    private Camera mainCamera;
    private Vector2 canvasSize;
    private Vector2 size;
    private Vector3 offset;
    private float pivotX = 0.5f;

    private bool isInitialize;
    private void Start()
    {
        if (isInitialize)
            return;
        Initialize();
        gameObject.SetActive(false);
    }

    private void Initialize()
    {
        mainCamera = Camera.main;
        canvasSize = GameManager.Instance.UIManager.CanvasSize;
        //TODO: wait for understand how is saving de CanvasSize;
        canvasSize = new Vector2(1920, 1080);
        Rect rect = gameObject.GetComponent<RectTransform>().rect;
        size = new Vector2(rect.width, rect.height);
        size *= transform.lossyScale;
        isInitialize = true;
    }

    private void FixedUpdate()
    {
        Vector2 screenPosition = mainCamera.WorldToScreenPoint(target.position + offset);
        screenPosition.x = Mathf.Clamp(screenPosition.x, size.x * pivotX, canvasSize.x - size.x * pivotX);
        screenPosition.y = Mathf.Clamp(screenPosition.y, 0, canvasSize.y - size.y);
        /// CheckMarkPostition(ref screenPosition);
        transform.position = screenPosition;
    }
    public void ShowMark(Transform target, Vector3 offset)
    {
        if (!isInitialize)
            Initialize();
        gameObject.SetActive(true);
        this.target = target;
        this.offset = offset;
    }

    private void CheckMarkPostition(ref Vector2 screenPosition)
    {
        Vector3 targetDirection = target.position - mainCamera.transform.position;
        targetDirection.Normalize();
        if (Vector3.Dot(mainCamera.transform.forward, targetDirection) > 0)
            return;
        float distanceToBottom = screenPosition.y;
        float distanceToTop = canvasSize.y - screenPosition.y;
        float distanceToLeft = screenPosition.x;
        float distanceToRight = canvasSize.x - screenPosition.x;
        float minHorizontalDistance = Mathf.Min(distanceToLeft, distanceToRight);
        float minVerticalDistance = Mathf.Min(distanceToTop, distanceToBottom);
        float minDistance = Mathf.Min(minHorizontalDistance, minVerticalDistance);
        switch (minDistance)
        {
            case float d when d == distanceToBottom:
                screenPosition.y = 0;
                break;
            case float d when d == distanceToTop:
                screenPosition.y = canvasSize.y - size.y;
                break;
            case float d when d == distanceToLeft:
                screenPosition.x = size.x * pivotX;
                break;
            case float d when d == distanceToBottom:
                screenPosition.x = canvasSize.x - size.x * pivotX;
                break;
        }
    }
}
