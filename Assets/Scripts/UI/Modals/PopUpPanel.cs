using PixelCrushers;
using UnityEngine;
using TMPro;

public class PopUpPanel : MenuUI
{
    [SerializeField] private CoraliumButton acceptButton;
    [SerializeField] private CoraliumButton cancelButton;
    [SerializeField] private LocalizeUI descriptionLocalizeUI;
    [SerializeField] private TextMeshProUGUI descriptionTMP;
    [SerializeField] private LocalizeUI acceptLocalizeUI;
    [SerializeField] private LocalizeUI cancelLocalizeUI;

    private PopPanelData popPanelData;
    private Vector3 startPosition;

    protected override void Awake()
    {
        base.Awake();
        buttons = new INavegable[] { acceptButton, cancelButton };
        startPosition = acceptButton.transform.position;
    }
    protected override void OnDisable()
    {
        base.OnDisable();
        popPanelData = null;
        //interactions.Cancel.performed -= ClosePanel;
    }
    protected override void OnDestroy()
    {
        base.OnDestroy();
        popPanelData = null;
        //interactions.Cancel.performed -= ClosePanel;
    }
    public void Accep() => popPanelData?.OnSubmit?.Invoke();

    public void OpenPanel(PopPanelData popPanelData)
    {
        if (gameObject.activeInHierarchy)
            throw new System.Exception("Popup panel is already open");
        gameObject.SetActive(true);
        //interactions.Cancel.performed += ClosePanel;
        this.popPanelData = popPanelData;
        this.popPanelData.OnSubmit += () => gameObject.SetActive(false);
        LocalizeUIUtilities.SetLocalizeUI(popPanelData.Description, descriptionLocalizeUI);
        LocalizeUIUtilities.SetLocalizeUI(popPanelData.Accept, acceptLocalizeUI);
        if (popPanelData.UseSpriteAsset)
            descriptionTMP.spriteAsset = popPanelData.spriteAsset;
        if (popPanelData.OneButton)
        {
            Vector3 targetPosition = acceptButton.transform.localPosition;
            targetPosition.x = 0;
            cancelButton.gameObject.SetActive(false);
            acceptButton.transform.localPosition = targetPosition;
          
            return;
        }
        acceptButton.transform.position = startPosition;
        cancelButton.gameObject.SetActive(true);
        LocalizeUIUtilities.SetLocalizeUI(popPanelData.Cancel, cancelLocalizeUI);
    }

    private void ClosePanel(UnityEngine.InputSystem.InputAction.CallbackContext ctx) => gameObject.SetActive(false);
}
