using MoreMountains.Feedbacks;
using TMPro;
using UnityEngine;

public class PopUpBuyPanel : PopUpPanel
{
    [SerializeField] private TextMeshProUGUI priceText;
    [SerializeField] private ShopData shopData;
    [SerializeField] private ItemCollectedInfo coinInfo;
    [SerializeField] private MMFeedbacks coinFeedback;
    protected override void Awake()
    {
        base.Awake();
        coinInfo.SetInfo(shopData.coin);
    }
    public void OpenPanel(PopPanelData popPanelData, int price)
    {
        OpenPanel(popPanelData);     
        priceText.text = price.ToString();
        if (price > shopData.coin.Amount)
            coinFeedback.PlayFeedbacks();
    }
}
