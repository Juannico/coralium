using PixelCrushers;
using TamarilloTools;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class ChallengeUI : MonoBehaviour
{
    [SerializeField] private Image iconImage;
    [SerializeField] private Image iconImageColor;
    [SerializeField] private LocalizeUI title;
    [SerializeField] private LocalizeUI description;
    [SerializeField] private LocalizeUI progress;
    [SerializeField] private TextMeshProUGUI variableText;
    [SerializeField] private InputActionReference inputAction;

    private ChallengeData challengeData;
    private void Awake()
    {
        // LeanTween animations
    }

    public void Init(ChallengeData challengeData)
    {
        this.challengeData = challengeData;
        iconImage.sprite = challengeData.Icon;
        iconImageColor.color = challengeData.Color;
        LocalizeUIUtilities.SetLocalizeUI(challengeData.Title, title);
        LocalizeUIUtilities.SetLocalizeUI(challengeData.Description, description);
        LocalizeUIUtilities.SetLocalizeUI(challengeData.Progress, progress);
    }
    public void HideInfoZone()
    {
        // Lean tween then remove fron overlay
        UIStackSystem.Instance.CloseOverlayScreen(gameObject);
    }
    public void UpdateInfo(string newText) => variableText.text = newText;
}
