using PixelCrushers;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowIconItemCollected : MonoBehaviour
{
    [SerializeField] private LocalizeUI description;
    [SerializeField] private Image icon;
    [SerializeField] private float duration = 2f;

    private bool showingPopUp;
    private Queue<Tuple<TextTableFieldName, Sprite>> popUpQueue = new Queue<Tuple<TextTableFieldName, Sprite>>();

    public void Show(TextTableFieldName descriptionTextTableFieldName, Sprite image)
    {
        if (!showingPopUp)
        {
            showingPopUp = true;
            gameObject.SetActive(true);
            ActivatePopUp(descriptionTextTableFieldName, image);
            return;
        }
        foreach (Tuple<TextTableFieldName, Sprite> tuple in popUpQueue)
        {
            if (descriptionTextTableFieldName != tuple.Item1)
                continue;
            Tuple<TextTableFieldName, Sprite> tupleToModify = popUpQueue.Dequeue();
            Tuple<TextTableFieldName, Sprite> updatedTuple = Tuple.Create(tupleToModify.Item1, tupleToModify.Item2);
            popUpQueue.Enqueue(updatedTuple);
            return;
        }
        popUpQueue.Enqueue(Tuple.Create(descriptionTextTableFieldName, image));
    }
    private void ActivatePopUp(TextTableFieldName descriptionTextTableFieldName, Sprite image)
    {
        LocalizeUIUtilities.SetLocalizeUI(descriptionTextTableFieldName, description);
        icon.sprite = image;
        CoroutineHandler.ExecuteActionAfter(HidePopUp, duration, this);
    }

    private void HidePopUp()
    {
        // #TODO Fix animation
       // CoroutineHandler.ExecuteActionAfter(() =>
      //  {
            if (popUpQueue.Count > 0)
            {
                Tuple<TextTableFieldName, Sprite> queauedPopUp = popUpQueue.Dequeue();
                ActivatePopUp(queauedPopUp.Item1, queauedPopUp.Item2);
                return;
            }
            showingPopUp = false;
            gameObject.SetActive(false);
       //}, 5, this);
    }
}
