using NaughtyAttributes;
using PixelCrushers;
using System;
using TamarilloTools;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class DialogueTutorialPanel : BaseTutorialUI
{
    [SerializeField] private LocalizeUI title;
    [SerializeField] private GameObject iconContainer;
    [SerializeField] private LocalizeUI description;
    [SerializeField] private TextMeshProUGUI descriptionTMP;
    [SerializeField] private ButtonPrompt instructionButton;

    public override void Init(TutorialData dataToDisplay)
    {
        base.Init(dataToDisplay);
        LocalizeUIUtilities.SetLocalizeUI(dataToDisplay.TutorialTitle, title);
        LocalizeUIUtilities.SetLocalizeUI(dataToDisplay.TutorialDescription, description);

        if (dataToDisplay as AbilityTutorialData)
        {
            instructionButton.SetButtonPromptData(((AbilityTutorialData)dataToDisplay).AbilityPromptData);
            instructionButton.gameObject.SetActive(true);
        }

        for (int i = 0; i < dataToDisplay.TutorialImageSequences.Length; i++)
        {
            Instantiate(dataToDisplay.TutorialImageSequences[i], iconContainer.transform);
        }
    }

    public override void NextTutorialStep(InputAction.CallbackContext ctx)
    {
        base.NextTutorialStep(ctx);
        PlayerInputManager.Instance.SetInputModePlayer();
        UIStackSystem.Instance.CloseOverlayScreen(gameObject);
    }

    public void OpenPanel(Item item)
    {
        gameObject.SetActive(true);
        LocalizeUIUtilities.SetLocalizeUI(item.Name, title);
        iconContainer.AddComponent<Image>().sprite = item.Icon;
        LocalizeUIUtilities.SetLocalizeUI(item.Description, description);

        instructionButton.gameObject.SetActive(false);
    }

    public void OpenPanel(IconItemDataPopUp itemTutoDataPopUp)
    {
        gameObject.SetActive(true);
        LocalizeUIUtilities.SetLocalizeUI(itemTutoDataPopUp.Title, title);
        iconContainer.AddComponent<Image>().sprite = itemTutoDataPopUp.Icon;
        if (itemTutoDataPopUp.UseSpriteAsset)
            descriptionTMP.spriteAsset = itemTutoDataPopUp.spriteAsset;
        LocalizeUIUtilities.SetLocalizeUI(itemTutoDataPopUp.Description, description);
        instructionButton.gameObject.SetActive(itemTutoDataPopUp.UseButtonInstruction);
        //instructionButton. = itemTutoDataPopUp.buttonInstructionData;
    }
}
[Serializable]
public class IconItemDataPopUp
{
    public TextTableFieldName Title;
    public TextTableFieldName Description;
    public bool UseSpriteAsset;
    [AllowNesting][ShowIf("UseSpriteAsset")] public TMP_SpriteAsset spriteAsset;
    [ShowAssetPreview] public Sprite Icon;
    public bool UseButtonInstruction;
    [ShowIf("UseButtonInstruction")][AllowNesting] public ButtonPromptData buttonInstructionData;

}
