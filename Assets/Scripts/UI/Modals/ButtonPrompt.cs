using NaughtyAttributes;
using PixelCrushers;
using System;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class ButtonPrompt : MonoBehaviour, IUIAdapter
{
    [Header("Button Prompt References")]
    [SerializeField] protected GameObject buttonContainer;
    [SerializeField] protected LocalizeUI promptText;

    [Header("Data")]
    [Expandable][SerializeField] protected ButtonPromptData buttonData;

    [BoxGroup("Animation")]
    [SerializeField] private bool playBlinkingAnimation;
    [BoxGroup("Animation")]
    [ShowIf("playBlinkingAnimation")]
    [SerializeField] private float blinkDuration = 0.5f;

    [Header("Input Action")]
    [SerializeField] private bool isHold;
    [ShowIf("isHold")][SerializeField] protected Image holdImage;
    private InputActionInteractionData holdInputActionData;
    private bool isHolding = false;
    private float holdTime = 0;

    private GameObject kbItem;
    private GameObject gamepadItem;

    private CanvasGroup canvasGroup;

    private void Awake()
    {
        canvasGroup = gameObject.AddComponent<CanvasGroup>();
        gameObject.AddComponent<UIInputAdapter>();
    }

    private void OnEnable()
    {
        Init();
    }
    private void Update()
    {
        if (!isHolding)
            return;
        holdTime += Time.deltaTime;
        holdImage.fillAmount = holdTime / holdInputActionData.HoldDuration;
    }

    private void OnDisable()
    {
        if (isHold == true)
        {
            RemoveHoldCallBacks();
            return;
        }
        // Only cancel animation for non-hold buttons because cancelling
        // of hold animations is done on complete hold
        LeanTween.cancel(gameObject);
    }

    public void SetButtonPromptData(ButtonPromptData data)
    {
        buttonData = data;
    }

    public void SetHoldInputActionReference(InputActionReference newReference)
    {
        holdInputActionData = new InputActionInteractionData();
        holdInputActionData.InputActionReference = newReference;
        AddHoldCallbacks();
    }
    public void Init()
    {
        if (buttonData == null) throw new Exception("ButtonPrompt:: No button prompt data");

        if (kbItem == null || gamepadItem == null)
        {
            kbItem = Instantiate(buttonData.kbButtonPrefab, buttonContainer.transform);
            gamepadItem = Instantiate(buttonData.gamepadButtonPrefab, buttonContainer.transform);
        }

        UpdateOnInputDeviceChanged(PlayerInputManager.Instance.DeviceChange.CurrentControlSchema);

        if (playBlinkingAnimation)
        {
            LeanTween.alphaCanvas(canvasGroup, 0, blinkDuration).setRepeat(-1).setLoopPingPong().setIgnoreTimeScale(true);
        }
        else
        {
            LeanTween.cancel(gameObject);
        }
    }

    public void ResetButton()
    {
        if (kbItem != null)
        {
            Destroy(kbItem);
            kbItem = null;
        }

        if (gamepadItem != null)
        {
            Destroy(gamepadItem);
            gamepadItem = null;
        }

        buttonData = null;

        canvasGroup.alpha = 1.0f;
    }

    protected void AddHoldCallbacks()
    {
        PlayerInputManager.Instance.BindInputActionStarted(holdInputActionData.InputActionReference, StartHoldGraphics);
        PlayerInputManager.Instance.BindInputActionPerformed(holdInputActionData.InputActionReference, CompleteHold);
        PlayerInputManager.Instance.BindInputActionCanceled(holdInputActionData.InputActionReference, HideHoldGraphics);
    }

    protected void RemoveHoldCallBacks()
    {
        PlayerInputManager.Instance.UnbindInputActionStarted(holdInputActionData.InputActionReference, StartHoldGraphics);
        PlayerInputManager.Instance.UnbindInputActionPerformed(holdInputActionData.InputActionReference, CompleteHold);
        PlayerInputManager.Instance.UnbindInputActionCanceled(holdInputActionData.InputActionReference, HideHoldGraphics);
    }

    private void StartHoldGraphics(InputAction.CallbackContext ctx)
    {
        holdImage.gameObject.SetActive(true);
        holdImage.fillAmount = 0;
        holdTime = 0;
        isHolding = true;
    }

    // Leaving it separate just in case is needed...
    private void CompleteHold(InputAction.CallbackContext ctx)
    {
        RemoveHoldCallBacks();
        HideHoldGraphics(ctx);
        LeanTween.cancel(gameObject);
    }

    private void HideHoldGraphics(InputAction.CallbackContext ctx)
    {
        holdImage.gameObject.SetActive(false);
        isHolding = false;
    }


    public void UpdateOnInputDeviceChanged(string newDevice)
    {
        if (newDevice == Constants.GamepadString)
        {
            kbItem.SetActive(false);
            gamepadItem.SetActive(true);
            promptText.textTable = buttonData.gamepadPromptText.TextTable;
            promptText.fieldName = buttonData.gamepadPromptText.FieldName;
        }
        else
        {
            kbItem.SetActive(true);
            gamepadItem.SetActive(false);
            promptText.textTable = buttonData.kbPromptText.TextTable;
            promptText.fieldName = buttonData.kbPromptText.FieldName;
        }
        promptText.UpdateText();
    }
    public void SetPlayBlinkingAnimation(bool bPlayAnimation) => playBlinkingAnimation = bPlayAnimation;
}
