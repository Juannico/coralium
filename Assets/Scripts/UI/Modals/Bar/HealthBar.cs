using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    [SerializeField] private Image barImage;
    [SerializeField] private Gradient gradient;
    private float maxValue;
    private Vector3 scaleVector;
    public void Initilize(int maxValue)
    {
        this.maxValue = maxValue;
        barImage.color = gradient.Evaluate(1);
        scaleVector = Vector3.one;
    }
    public void SetHealth(float currentValue)
    {
        float value = Mathf.Lerp(0, 1, currentValue / maxValue);
        barImage.fillAmount = value;
        barImage.color = gradient.Evaluate(value);
        scaleVector.x = value;
        barImage.rectTransform.localScale = scaleVector;
    }
}
