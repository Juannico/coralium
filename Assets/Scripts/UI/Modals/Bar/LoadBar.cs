using UnityEngine;
using UnityEngine.UI;

public class LoadBar : MonoBehaviour
{
    [SerializeField] private Image barImage;

    private Coroutine loadBarCoroutine;

    private float timer;
    private float duration;
    void Start()
    {
        barImage.fillAmount = 0;
    }
    private void Update()
    {
        timer += Time.deltaTime;
        float value = Mathf.Lerp(0, 1, timer / duration);
        barImage.fillAmount = value;
    }
    public void StartAutoLoad(float duration)
    {
        gameObject.SetActive(true);
        timer = 0;
        this.duration = duration;
    }
    public void StopAutoLoad()
    {
        if (loadBarCoroutine != null)
            StopCoroutine(loadBarCoroutine);
        gameObject.SetActive(false);
    }
}
