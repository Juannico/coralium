using TamarilloTools;
using UnityEngine;
using UnityEngine.InputSystem;

public class MainMenu : BaseScreenUI
{

    [Header("Sub-screens")]
    [SerializeField] private GameObject loadGameScreen;
    [SerializeField] private GameObject controlsScreen;
    [SerializeField] private GameObject settingsScreen;


    public override void GoBackToPreviousScreen(InputAction.CallbackContext ctx)
    {
        UIStackSystem.Instance.GoBackToPreviousScreen(false);
    }

    public void StartLoadingGame()
    {
        FadeUI.FadeOut();
    }

    public void OpenLoadGameScreen() => UIStackSystem.Instance.OpenContentScreen(transform.parent.gameObject, loadGameScreen);
    public void OpenControlScreen() => UIStackSystem.Instance.OpenContentScreen(transform.parent.gameObject, controlsScreen);
    public void OpenSettingsScreen() => UIStackSystem.Instance.OpenContentScreen(transform.parent.gameObject, settingsScreen);
    public void ExitGame()
    {
        Application.Quit();
    }
}

