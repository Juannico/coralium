using FMODUnity;
using NaughtyAttributes;
using TamarilloTools;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.UI;
using UnityEngine.UI;

public class BaseScreenUI : MonoBehaviour
{
    [field: SerializeField] public InputActionReference CloseScreenInputActionReference { get; private set; }

    [SerializeField] private bool lockPause = false;

    [ReadOnly] protected INavegable[] buttons;
    [SerializeField] private bool delayActivation;
    [ShowIf("delayActivation")][SerializeField] private float delayActivationTime = 1f;
    protected UIManager uiManager;
    private InputSystemUIInputModule inputSystemUIInputModule;

    public delegate void OnAction();
    public OnAction OnPanelClosed;

    protected virtual void Awake()
    {
        SetInputSystemUIInputModule();
    }
    protected virtual void Start()
    {
        SetSelectedObjectInPanel();
    }
    protected virtual void OnEnable()
    {
        SetInputSystemUIInputModule();
        SetSelectedObjectInPanel();
        PlayerInputManager.Instance.BindInputActionPerformed(CloseScreenInputActionReference, GoBackToPreviousScreen);
        if (delayActivation)
        {
            CoroutineHandler.ExcuteActionAfterUnescaledTime(DelayEnabled, delayActivationTime, this);
            return;
        }
        DelayEnabled();
    }
    protected virtual void DelayEnabled()
    {
        if (lockPause && uiManager != null)
            uiManager.CanPause = false;
    }
    protected virtual void OnDisable()
    {
        OnPanelClosed?.Invoke();
        PlayerInputManager.Instance.UnbindInputActionPerformed(CloseScreenInputActionReference, GoBackToPreviousScreen);
        // if (inputActions != null)
        if (lockPause && uiManager != null)
            uiManager.CanPause = true;
    }
    protected virtual void OnDestroy()
    {
        PlayerInputManager.Instance.UnbindInputActionPerformed(CloseScreenInputActionReference, GoBackToPreviousScreen);
    }

    public virtual void GoBackToPreviousScreen(InputAction.CallbackContext ctx)
    {
        UIStackSystem.Instance.GoBackToPreviousScreen();
    }

    /// <summary>
    /// This method will select the first item it finds under this panel that can be selected
    /// </summary>
    protected void SetSelectedObjectInPanel()
    {
        Selectable[] selectableItems = gameObject.GetComponentsInChildren<Selectable>();

        for (int i = 0; i < selectableItems.Length; i++)
        {
            if (selectableItems[i].gameObject.activeInHierarchy &&
                selectableItems[i].interactable &&
                selectableItems[i].navigation.mode != Navigation.Mode.None)
            {
                EventSystem.current.SetSelectedGameObject(selectableItems[i].gameObject);
                selectableItems[i].Select();
                return;
            }
        }
    }

    private void SetInputSystemUIInputModule()
    {
        inputSystemUIInputModule = FindObjectOfType<InputSystemUIInputModule>();
    }
}
