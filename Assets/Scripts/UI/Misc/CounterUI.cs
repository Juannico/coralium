using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CounterUI : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI counteText;
    private float startTime;
    private void Update()
    {
        startTime -= Time.deltaTime;
        counteText.text = Mathf.CeilToInt(startTime).ToString();
        if (startTime < 0)
            gameObject.SetActive(false);
    }

    public void StartCount(float time)
    {
        startTime = time;
        gameObject.SetActive(true);
    }
}
