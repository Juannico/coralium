using UnityEngine;

public class SetWorldHealthUiOnTopEnemy : SetWorldUiOnTopObject
{
    [SerializeField] private EnemyCharacter character;
    protected override void Start()
    {
        base.Start();
        EnemyHealthOnTopUI enemyHealthOnTopUI = baseWorldSpaceUI.GetComponentInChildren<EnemyHealthOnTopUI>();
        if (enemyHealthOnTopUI == null)
            throw new System.Exception($"{prefabUI} prefab does not  of type {typeof(EnemyHealthOnTopUI)}");
        enemyHealthOnTopUI.Initialize(character.BaseCharacter.Data);
    }
}
