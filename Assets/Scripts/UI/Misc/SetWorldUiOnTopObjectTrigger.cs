using UnityEngine;

public class SetWorldUiOnTopObjectTrigger : SetWorldUiOnTopObject
{
    [SerializeField] private LayerData layerData;
    private void OnTriggerEnter(Collider other)
    {
        if (!LayerUtilities.IsSameLayer(layerData.PlayerLayer, other.gameObject.layer))
            return;
        SetUIDisplay(true);
    }
    private void OnTriggerExit(Collider other)
    {
        if (!LayerUtilities.IsSameLayer(layerData.PlayerLayer, other.gameObject.layer))
            return;
        SetUIDisplay(false);

    }
}
