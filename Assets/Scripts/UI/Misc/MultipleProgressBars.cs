using UnityEngine;
using UnityEngine.UI;

public class MultipleProgressBars : MonoBehaviour
{
    [SerializeField] private Image[] progressBars;
    private float fillAmount = 0;
    public float FillAmount
    {
        get => fillAmount;
        set
        {
            fillAmount = value;
            int progressBarAmount = progressBars.Length;
            for (int i = 0; i < progressBarAmount; i++)
            {
                float amount = fillAmount * progressBarAmount;
                amount = Mathf.Clamp01(amount - i);
                progressBars[i].fillAmount = amount;
            }
        }
    }
}
