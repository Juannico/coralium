using NaughtyAttributes;
using UnityEngine;

public class SetWorldUiOnTopObject : MonoBehaviour
{
    [Header("BWorld UI Settings")]
    [SerializeField] protected BaseWorldSpaceUI prefabUI;
    [SerializeField] private Vector3 targetPosition;
    [SerializeField] private bool showOnStart = true;
    [SerializeField] private bool overrideScale;
    [ShowIf("overrideScale")][SerializeField] private float scaleMultiplier;

    private WorldUIManager worldUIManager;
    protected BaseWorldSpaceUI baseWorldSpaceUI;
    private bool isActive;
    private bool isApplicationQuitting = false;
    protected virtual void Awake()
    {
        worldUIManager = GameManager.Instance.UIManager.WorldUIManager;
        
    }
    protected virtual void Start()
    {
        baseWorldSpaceUI = worldUIManager.GetWordUI(prefabUI, prefabUI.GetType());
        baseWorldSpaceUI.OnTopUI.SetParent(transform, false);
        baseWorldSpaceUI.OnTopUI.localPosition = targetPosition;
        if (overrideScale)
            baseWorldSpaceUI.OnTopUI.localScale *=  scaleMultiplier;
        isActive = true;
        SetUIDisplay(showOnStart);
        baseWorldSpaceUI.OnTopUI.forward = Vector3.forward;
    }
    private void OnApplicationQuit()
    {
        isApplicationQuitting = true;
    }
    private void OnDestroy()
    {
        if (!isApplicationQuitting)
            OffUI();
    }

    public void OffUI()
    {
        if (GameManager.Instance == null)
            return;
        if (!isActive)
            return;
        if (baseWorldSpaceUI == null)
            return;
        if (!Application.isPlaying)
            return;
        if (baseWorldSpaceUI.gameObject.activeInHierarchy)
            baseWorldSpaceUI.gameObject.SetActive(false);
        baseWorldSpaceUI.IsActive = false;
        worldUIManager?.EnactiveInstaceUI(baseWorldSpaceUI.transform.GetChild(0).gameObject);
        isActive = false;
    }

    public void SetUIDisplay(bool show)
    {
        baseWorldSpaceUI?.SetUIDisplay(show);
    }
#if UNITY_EDITOR

    [SerializeField] private bool showGuide;
    private void OnDrawGizmos()
    {
        if (!showGuide)
            return;
        Gizmos.color = Color.cyan;
        Gizmos.DrawLine(transform.position, transform.TransformPoint(targetPosition));
        Gizmos.DrawSphere(transform.TransformPoint(targetPosition), 1);
    }
#endif
}
