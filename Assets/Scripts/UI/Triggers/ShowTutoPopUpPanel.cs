using NaughtyAttributes;
using UnityEngine;

public class ShowTutoPopUpPanel : MonoBehaviour, ISaveStateJSON
{
    [SerializeField] private bool useMultipleTriggers;
    [ShowIf("useMultipleTriggers")] [SerializeField] private CollisionHelper[] triggers;
    [HideIf("useMultipleTriggers")] [SerializeField] private CollisionHelper trigger;
    [SerializeField] private bool deactiveTriggers = false;
    [SerializeField] private LayerData layerData;
    [SerializeField] private IconItemDataPopUp iconItemDataPopUp;
    private void Awake()
    {
        DataManager.UpdateStateInterfaceValue(this);
        if (!State)
        {
            gameObject.SetActive(false);
            if (!deactiveTriggers)
                return;
            if (!useMultipleTriggers)
            {
                trigger.gameObject.SetActive(false);
                return;
            }
            for (int i = 0; i < triggers.Length; i++)
                triggers[i].gameObject.SetActive(false);

            return;
        }
        if (!useMultipleTriggers)
        {
            trigger.TriggerEnter += TriggerEnter;
            return;
        }
        for (int i = 0; i < triggers.Length; i++)
            triggers[i].TriggerEnter += TriggerEnter;
    }


    private void TriggerEnter(Collider other)
    {
        if (!LayerUtilities.IsSameLayer(layerData.PlayerLayer, other.gameObject.layer))
            return;
        DialogueTutorialPanel ItemCollectedPopUp = GameManager.Instance.UIManager.ItemCollectedTutoPopUp;
        //if (iconItemDataPopUp != null)
        //    ItemCollectedPopUp.OpenPanel(iconItemDataPopUp);
        if (!useMultipleTriggers)
        {
            trigger.TriggerEnter -= TriggerEnter;
            if (deactiveTriggers)
                trigger.gameObject.SetActive(false);
            gameObject.SetActive(false);
            return;
        }
        for (int i = 0; i < triggers.Length; i++)
        {
            triggers[i].TriggerEnter -= TriggerEnter;
            if (deactiveTriggers)
                triggers[i].gameObject.SetActive(false);
        }
        gameObject.SetActive(false);
    }
    #region ISaveStateJSON
    private bool firstTime = true;
    public bool State
    {
        get => firstTime;
        set
        {
            firstTime = value;
            Save();
        }
    }
    public string KeyName => "FirstTime";
    public string Path => $"PopUpTutorial/{gameObject.name}";
    public void Save() => DataManager.SaveInterface(this);
    #endregion
}
