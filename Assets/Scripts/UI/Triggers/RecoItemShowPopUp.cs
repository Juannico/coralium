using NaughtyAttributes;
using UnityEngine;

public class RecoItemShowPopUp : MonoBehaviour
{
    [SerializeField] private bool isItem;
    [ShowIf("isItem")] [SerializeField] private Item item;
    [HideIf("isItem")] [SerializeField] private CollectableData collectable;
    private void Awake()
    {
        if (isItem)
            item.OnPurchaseChange += ShowOnPurchaseItem;
        else
            collectable.OnCollected += ShowOnCollectedObject;
    }
    private void OnDestroy()
    {
        if (isItem)
            item.OnPurchaseChange -= ShowOnPurchaseItem;
        else
            collectable.OnCollected -= ShowOnCollectedObject;
    }
    private void ShowOnPurchaseItem(bool purchaseState)
    {
        if (!purchaseState)
            return;
        // #TODO Add panel open for purchase completed
        //itemCollectedPopUp.OpenPanel(item);
        item.OnPurchaseChange -= ShowOnPurchaseItem;
    }
    private void ShowOnCollectedObject(int valueToAdd)
    {
        if (valueToAdd <= 0)
            return;
        // #TODO Add panel open for purchase completed
        //itemCollectedPopUp.OpenPanel(collectable);
        //GameManager.Instance.TutorialHandler.StartTutorial(collectable., TutorialHandlerComponent.TutorialType.SimplePopUp);
        collectable.OnCollected -= ShowOnCollectedObject;
    }
}


