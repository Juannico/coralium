using TamarilloTools;
using UnityEngine;

public class ShowZoneName : MonoBehaviour
{
    [SerializeField] private GameObject zoneTitlePrefab;
    [SerializeField] private TextTableFieldName zoneName;
    [SerializeField] private float duration;
    private ZoneTitle zoneTitle;
    public void ShowUI()
    {
        zoneTitle = UIStackSystem.Instance.OpenOverlay(UIManager.Instance.MainCanvas, zoneTitlePrefab).GetComponent<ZoneTitle>();
        zoneTitle.Init(zoneName, duration);
    }
}
