using NaughtyAttributes;
using UnityEngine;

public class CreditsManager : MonoBehaviour
{
    [SerializeField] private GameObject objectToScroll;
    [SerializeField] private float scrollSpeed = 1f;
    [SerializeField] private float endOffset = 50;
    [SerializeField] private GameObject finalPanel;
    [Scene][SerializeField] private string menuScene;

    private RectTransform objectToScrollRect;
    private PlayerInputActions playerActions;
    // Start is called before the first frame update
    void Start()
    {
        objectToScrollRect = objectToScroll.GetComponent<RectTransform>();
        playerActions = new PlayerInputActions();
        playerActions.Player.Action.performed += (ctx) => { LoadingScreenManager.LoadSceneByName(menuScene); };
        StartCoroutine(CoroutineHandler.ExecuteActionAfterUpdateUntil(() =>
        {
            Vector2 temp = objectToScrollRect.anchoredPosition;
            temp.y += Time.deltaTime * scrollSpeed;
            objectToScrollRect.anchoredPosition = temp;
            return objectToScrollRect.anchoredPosition.y >= objectToScrollRect.sizeDelta.y + endOffset;
        },ActivateFinalPanel));
    }

    private void OnDisable()
    {
        playerActions.Disable();
    }

    private void ActivateFinalPanel()
    {
        playerActions.Enable();
        finalPanel.SetActive(true);
    }
}
