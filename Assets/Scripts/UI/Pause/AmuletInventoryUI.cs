using PixelCrushers;
using UnityEngine;
using UnityEngine.UI;

public class AmuletInventoryUI : BaseScreenUI
{
    [Header("Invetory UI Settings")]
    [SerializeField] private AmuletInventory amuletInventory;
    [SerializeField] private AmuletInfo amuletInfoPrefab;
    [SerializeField] private GridLayoutGroup equipAmuletsContainer;
    [SerializeField] private GridLayoutGroup amuletsContainer;
    [SerializeField] private RectTransform selectAmuletContainer;
    [SerializeField] private LocalizeUI amuletName;
    [SerializeField] private LocalizeUI amuletDescription;
    [Header("Popup panel Settings")]
    [SerializeField] private PopPanelData EquipPopPanelData;
    [SerializeField] private PopPanelData UnEquiPopPanelData;
    [SerializeField] private PopPanelData AllEquippedPopPanelData;

    private AmuletInfo selectAmulet;
    private bool selectingAmulet;
    private INavegable[] amuletButtons;
    private AmuletInfo[] equippedAmulet;
    private int equippedAmuletIndex = 0;
    private PopUpPanel popUpPanel;
    protected override void Awake()
    {     
        equippedAmuletIndex = 0;
        amuletInventory.Initialize();
        base.Awake();
        popUpPanel = GameManager.Instance.UIManager.PopUpPanel;
    }
    protected override void Start()
    {
        amuletButtons = new INavegable[amuletInventory.Amulets.Length ];
        SetSelectedInfoAmuletUI();
        SetEquipAmuletUI();
        SetAmuletUI();
        buttons = amuletButtons;
        base.Start();
    }
    protected override void OnDisable()
    {
        base.OnDisable();
        if (selectingAmulet)
        {
            selectingAmulet = false;
            for (int i = amuletInventory.Amulets.Length; i < amuletButtons.Length; i++)
                amuletButtons[i].Interactable = true;
        }
        selectingAmulet = false;
        if (popUpPanel == null)
            popUpPanel = GameManager.Instance.UIManager.PopUpPanel;
        popUpPanel.gameObject.SetActive(false);
    }
    private void SetAmuletUI()
    {
        for (int i = 0; i < amuletInventory.Amulets.Length; i++)
        {
            AmuletInfo amuletInfo = CreateAmuletInfo(amuletsContainer.transform, amuletsContainer.cellSize.x, amuletsContainer.cellSize.y);
            amuletButtons[i] = amuletInfo ;
            Amulet amulet = amuletInventory.Amulets[i];
            amuletInfo.SetInfo(amulet);
            amuletInfo.OnClick.AddListener(() =>
            {
                if (selectingAmulet)
                    return;
                if (!amulet.Purchased)
                    return;
                selectingAmulet = true;
                PopPanelData popPanelData = AllEquippedPopPanelData;
                if (!amulet.IsEquipped)
                {
                    popPanelData = EquipPopPanelData;
                    popPanelData.OnSubmit = () =>
                    {
                        amulet.Equip();
                        equippedAmulet[equippedAmuletIndex].SetInfo(amulet, false);
                        equippedAmuletIndex++;
                    };
                    if (equippedAmuletIndex >= equippedAmulet.Length)
                        popPanelData = AllEquippedPopPanelData;
                }
                else
                {
                    popPanelData = UnEquiPopPanelData;
                    popPanelData.OnSubmit = () =>
                    {
                        amulet.Unequip();
                        for (int i = 0; i < equippedAmuletIndex; i++)
                        {
                            if (equippedAmulet[i].Amulet != amulet)
                                continue;
                            equippedAmulet[i].SetInfo(null, false);
                            break;
                        }
                        equippedAmuletIndex--;
                    };
                }
                popPanelData.OnSubmit += () =>
                {
                    amuletInfo.Select();
                };
                popUpPanel.OpenPanel(popPanelData);
                popUpPanel.OnPanelClosed += ReturnAfterClosePopUpPanel;
                selectingAmulet = false;
            });
            amuletInfo.OnSelected.AddListener(() =>
            {
                if (amulet == null)
                    return;
                if (!amulet.Purchased)
                    return;
                selectAmulet.SetInfo(amulet, false);
                LocalizeUIUtilities.SetLocalizeUI(amulet.Name, amuletName);
                LocalizeUIUtilities.SetLocalizeUI(amulet.Description, amuletDescription);
            });
            if (amulet.IsEquipped)
            {
                if (equippedAmuletIndex >= equippedAmulet.Length)
                {
                    amulet.IsEquipped = false;
                    continue;
                }
                equippedAmulet[equippedAmuletIndex].SetInfo(amulet, false);
                equippedAmuletIndex++;
            }
        }
    }
    private void SetEquipAmuletUI()
    {
        equippedAmulet = new AmuletInfo[amuletInventory.equippedAmulets.Length];
        for (int i = 0; i < amuletInventory.equippedAmulets.Length; i++)
        {
            AmuletInfo amuletInfo = CreateAmuletInfo(equipAmuletsContainer.transform, equipAmuletsContainer.cellSize.x, equipAmuletsContainer.cellSize.y);
            equippedAmulet[i] = amuletInfo;
            equippedAmulet[i].Interactable = false;
        }
    }
    private void SetSelectedInfoAmuletUI()
    {
        selectAmulet = CreateAmuletInfo(selectAmuletContainer, selectAmuletContainer.rect.width, selectAmuletContainer.rect.height);
        selectAmulet.SetInfo(null, false);
    }
    private AmuletInfo CreateAmuletInfo(Transform parent, float width, float height)
    {
        AmuletInfo amuletInfo = Instantiate(amuletInfoPrefab, parent);
        SetAmulerInfoScale(amuletInfo, width, height);
        return amuletInfo;
    }

    private void SetAmulerInfoScale(AmuletInfo amuletInfo, float width, float height)
    {
        RectTransform rectTransform = amuletInfo.GetComponent<RectTransform>();
        Vector3 newScale = new Vector3(width / rectTransform.rect.width, height / rectTransform.rect.height, 1);
        rectTransform.localScale = newScale;
    }

    private void ReturnAfterClosePopUpPanel()
    {
        popUpPanel.OnPanelClosed -= ReturnAfterClosePopUpPanel;
    }
}
