using FMODUnity;
using TamarilloTools;
using UnityEngine;
using UnityEngine.InputSystem;

public class PauseMenu : BaseScreenUI
{
    [SerializeField] private LevelManager levelManager;
    [Header("SFX")]
    [SerializeField] private StudioEventEmitter pauseSnapshot;
    [Header("Other Menus")]
    [SerializeField] private GameObject settingsMenuPrefab;
    [SerializeField] private GameObject controlsMenuPrefab;

    protected override void OnEnable()
    {
        base.OnEnable();
        pauseSnapshot?.Play();
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();
        pauseSnapshot?.Stop();
    }

    public override void GoBackToPreviousScreen(InputAction.CallbackContext ctx)
    {
        base.GoBackToPreviousScreen(ctx);
        pauseSnapshot?.Stop();
        GameManager.Instance.SetGamePaused(false);
        PlayerInputManager.Instance.SetInputModePlayer();
        PlayerInputManager.Instance.EnablePause();
    }

    // Wrapper to use on the onClick in the inspector for the continue button
    public void Resume() => GoBackToPreviousScreen(default);

    public void OpenSettings() => UIStackSystem.Instance.OpenContentScreen(UIManager.Instance.MainCanvas, settingsMenuPrefab);
    public void OpenControls() => UIStackSystem.Instance.OpenContentScreen(UIManager.Instance.MainCanvas, controlsMenuPrefab);

    public void GoToMainMenu()
    {
        Resume();
        Destroy(GameManager.Instance.gameObject);
        if (LevelSceneManager.Instance != null)
            Destroy(LevelSceneManager.Instance.gameObject);
        pauseSnapshot?.Stop();
        PlayerCharacter playerCharacter = GameObject.FindGameObjectWithTag(Tags.Player).GetComponent<PlayerCharacter>();
        levelManager.CurrentLevelData.Spawn.SetLevelCheckPoint(playerCharacter.transform.position, playerCharacter.BaseTransform.forward, levelManager);
        LoadingScreenManager.LoadSceneByName("MainMenu");
    }
}
