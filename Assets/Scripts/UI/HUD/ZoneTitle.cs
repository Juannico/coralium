using PixelCrushers;
using TamarilloTools;
using TMPro;
using UnityEngine;

public class ZoneTitle : MonoBehaviour
{
    [SerializeField] private float fadeDuration = 1f;
    private LocalizeUI title;
    private TextMeshProUGUI titleText;
    private float duration;

    private void Start()
    {
        GetComponent<CanvasGroup>().alpha = 0;
        LeanTween.alphaCanvas(GetComponent<CanvasGroup>(), 1, fadeDuration);
        LeanTween.alphaCanvas(GetComponent<CanvasGroup>(), 0, fadeDuration)
            .setDelay(duration)
            .setOnComplete(() => UIStackSystem.Instance.CloseOverlayScreen(gameObject));
    }

    public void Init(TextTableFieldName textTableFieldName, float duration = 3)
    {
        title = GetComponent<LocalizeUI>();
        titleText = GetComponent<TextMeshProUGUI>();

        this.duration = duration;
        title.textTable = textTableFieldName.TextTable;
        title.fieldName = textTableFieldName.FieldName;
        titleText.color = textTableFieldName.TextColor;
        title.UpdateText();
    }
}
