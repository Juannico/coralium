
using UnityEngine;
using UnityEngine.UI;

public class InventoryAbilityInfo : MonoBehaviour
{
    [Header("Inventory Ability Button Settings")]
    [SerializeField] private BaseAbilityCast baseAbilityCastPrefab;
    [SerializeField] private PlayerAbilities playerAbilities;
    [SerializeField] private Image icon;
    [HideInInspector] public BaseAbilityCast Ability;
    [SerializeField] private AbilityTutorialData uIAbilityTutorialData;
    [SerializeField] private Color lockColor;

    public void SetInfo()
    {
        Ability = playerAbilities.GetPlayerAbility(baseAbilityCastPrefab);
        //icon.sprite = Ability.Data.abilityInfo.Sprite;
        // I have no idea what this does 
        //if (uIAbilityTutorialData != null)
        //    Ability.Data.abilityTutorialData.AbilityPromptData = uIAbilityTutorialData.AbilityPromptData;
    }

    public void SetLock(bool lockSatete) => icon.color = !lockSatete ? lockColor : Color.white;
}
