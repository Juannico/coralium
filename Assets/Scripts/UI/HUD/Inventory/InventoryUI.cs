using PixelCrushers;
using UnityEngine;
using UnityEngine.UI;

public class InventoryUI : MonoBehaviour
{
    [Header("Abilities")]
    [SerializeField] private LocalizeUI PowersText;
    [SerializeField] private TextTableFieldName PowersName;
    [SerializeField] private InventoryAbilityInfo[] inventoryAbilityInfos;
    [Header("Coins/Polyp")]
    [SerializeField] private ItemCollectedInfo polyp;
    [SerializeField] private ItemCollectedInfo coin;
    [Header("Items")]
    [SerializeField] private LocalizeUI ItemsText;
    [SerializeField] private TextTableFieldName ItemsName;
    [SerializeField] private Inventory inventory;
    [SerializeField] private ItemCollectedInfo itemCollectedInfoPrefab;
    [SerializeField] private GridLayoutGroup container;
    private ItemCollectedInfo[] itemCollectedInfos;
    private int showCollectablesAmount = 0;
    protected  void Awake()
    {
        for (int i = 0; i < inventory.Collectables.Length; i++)
        {
            if (inventory.Collectables[i].ShowInfo)
                showCollectablesAmount++;
        }

        LocalizeUIUtilities.SetLocalizeUI(PowersName, PowersText);
        LocalizeUIUtilities.SetLocalizeUI(ItemsName, ItemsText);
        SetItems();
    }
    protected  void Start()
    {
        SetAbilityInfoUI();
    }

    protected void OnDestroy()
    {
        for (int i = 0; i < inventoryAbilityInfos.Length; i++)
        {
            if (inventoryAbilityInfos[i].Ability != null)
                inventoryAbilityInfos[i].Ability.Data.OnSwitchLockAbility -= inventoryAbilityInfos[i].SetLock;
        }
    }
    private void SetItems()
    {
        CollectableData[] items = inventory.Collectables;
        itemCollectedInfos = new ItemCollectedInfo[showCollectablesAmount];
        int index = 0;
        for (int i = 0; i < inventory.Collectables.Length; i++)
        {
            if (!inventory.Collectables[i].ShowInfo)
                continue;
            ItemCollectedInfo itemCollectedInfo = Instantiate(itemCollectedInfoPrefab, container.transform);
            itemCollectedInfo.SetInfo(items[i]);
            itemCollectedInfos[index] = itemCollectedInfo;
            itemCollectedInfo.gameObject.SetActive(true);
            index++;
        }
    }
    private void SetAbilityInfoUI()
    {
        int baseIndex = itemCollectedInfos.Length + 2;
        for (int i = 0; i < inventoryAbilityInfos.Length; i++)
        {
            InventoryAbilityInfo inventoryAbilityInfo = inventoryAbilityInfos[i];
            inventoryAbilityInfo.SetInfo();
            inventoryAbilityInfo.SetLock(!inventoryAbilityInfo.Ability.IsLocked);
            inventoryAbilityInfo.Ability.Data.OnSwitchLockAbility += inventoryAbilityInfo.SetLock;
        }
    }
}
