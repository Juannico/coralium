using UnityEngine;

public class NavigateButtons : InputButton
{
    [Header("Navigate Settings")]
    [SerializeField] private Color baseColor;
    [SerializeField] private Color selectedColor;
    [SerializeField] private Color inactiveColor;
    [SerializeField] private BaseScreenUI panel;
    [SerializeField] private TextTableFieldName tutorialTexTable;
    private bool firstTime = true;
    protected override void Awake()
    {
        OnInteractableChange.AddListener(ChangeInteractableEffect);
        base.Awake();
        Deselected();
    }

    protected override void ButtonSelectedEffect()
    {
        base.ButtonSelectedEffect();
        buttonImage.color = selectedColor;
        if (!firstTime)
            return;
        firstTime = false;
    }

    protected override void ButtonDeselectedEffect()
    {
        base.ButtonDeselectedEffect();
        buttonImage.color = baseColor;
    }

    private void ChangeInteractableEffect(bool isInteractable)
    {
        Color activeColor = IsSelected ? selectedColor : baseColor;
        buttonImage.color = isInteractable ? activeColor : inactiveColor;
    }
}
