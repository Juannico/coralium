using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CoralPlantMeterUI : MonoBehaviour
{
    [SerializeField] private GameObject meterBackground;
    [SerializeField] private GameObject meterBar;
    [SerializeField] private TextMeshProUGUI countInfo;
    [SerializeField] private CollectableData polypCollectableData;
    [SerializeField] private LevelManager levelManager;
    [SerializeField] private float showDuration = 4f;

    private void Start()
    {
        polypCollectableData.OnCollected += ShowMeterCurrentState;
    }
    private void OnEnable()
    {
        SetInfoVisibility(false);
    }
    private void OnDestroy()
    {
        polypCollectableData.OnCollected -= ShowMeterCurrentState;
    }
    public void ShowMeterCurrentState(int valueAdded)
    {
        int value = polypCollectableData.Amount + valueAdded;
        if (value <= 0)
            return;
        SetInfoVisibility(true);
       
        UpdateCoralGraphics(value);
        countInfo.text = $"{value}/{levelManager.CurrentLevelData.GrowCoralData.FoodNumber}";
    }


    private void UpdateCoralGraphics(int value)
    {
        meterBar.GetComponent<Image>().fillAmount = (float)value / levelManager.CurrentLevelData.GrowCoralData.FoodNumber;
        CoroutineHandler.ExecuteActionAfter(() =>
        {
            SetInfoVisibility(false);
        }, showDuration, this);
    }

    private void SetInfoVisibility(bool state)
    {
        meterBackground.SetActive(state);
        meterBar.SetActive(state);
        countInfo.gameObject.SetActive(state);
    }
}
