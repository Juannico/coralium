using TMPro;
using UnityEngine;

public class LoadGameButton : CoraliumButton
{
    //TODO: This Class will be implementent all necesary to show loadData from a game or create game
    [Header("Load Game Button Settings")]
    [SerializeField] private TextMeshProUGUI descriptionText;
    public CoraliumButton DeleteButton;

    public void SetInfo(bool created)
    {
        descriptionText.text = created ? "Load Game" : "Create Game";
        DeleteButton.gameObject.SetActive(created);
    }

}
