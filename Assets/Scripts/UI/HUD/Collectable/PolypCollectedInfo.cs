using UnityEngine;

public class PolypCollectedInfo : ItemCollectedInfo
{
    [SerializeField] private LevelManager levelManager;

    private void OnEnable()
    {
        if(levelManager.CurrentLevelData == null)
        {
            gameObject.SetActive(false);
            return;
        }
        UpdateInfo(0);
    }
    protected override string GetTextAmount(int valueAdded = 0)
    {
        if (levelManager.CurrentLevelData.GrowCoralData.IsPlanted)
            return $"{levelManager.CurrentLevelData.GrowCoralData.FoodNumber}/{levelManager.CurrentLevelData.GrowCoralData.FoodNumber}";
        return $"{CollectableData.Amount + valueAdded}/{levelManager.CurrentLevelData.GrowCoralData.FoodNumber}";
    }
}
