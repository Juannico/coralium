using NaughtyAttributes;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ItemCollectedInfo : MonoBehaviour
{
    [Header("Item Collecte Button Settings")]
    [SerializeField] private Image Icon;
    [SerializeField] private TextMeshProUGUI amount;
    public bool HideInAmountZero;
    public bool HideNumberOne;
    [SerializeField] private bool autoFill = false;
    [Expandable] [ShowIf("autoFill")] public CollectableData CollectableData;
    private bool delegateSubscribed = false;
    private void Awake()
    {
        if (autoFill)
            SetInfo(CollectableData);
    }
    public void SetInfo(CollectableData collectableData)
    {
        if (CollectableData != null && delegateSubscribed)
            CollectableData.OnCollected -= UpdateInfo;
        CollectableData = collectableData;
        Icon.sprite = collectableData.collectableSprite;
        amount.text = GetTextAmount(0);
        UpdateInfo(0);
    }

    protected virtual string GetTextAmount(int valueAdded = 0) => (CollectableData.Amount + valueAdded).ToString();

    private void OnEnable()
    {
        if (CollectableData == null)
            return;
        if (delegateSubscribed)
            return;
        CollectableData.OnCollected += UpdateInfo;
        delegateSubscribed = true;
        UpdateInfo(0);
    }
    private void OnDisable()
    {
        UnSubscribe();
    }
    private void OnDestroy()
    {
        if (Application.isPlaying)
            UnSubscribe();
    }
    protected void UpdateInfo(int valueAdded)
    {
        if (!gameObject.activeInHierarchy)
            gameObject.SetActive(true);
        string textValue = GetTextAmount(valueAdded);
        if (HideInAmountZero)
        {
            int value = int.Parse(textValue);
            if (value <= 0)
            {
                gameObject.SetActive(false);
                return;
            }
            if (!gameObject.activeInHierarchy)
                gameObject.SetActive(true);
            if (value <= 1 && HideNumberOne)
                textValue = "";
        }
        amount.text = textValue;
    }
    private void UnSubscribe()
    {
        if (CollectableData == null)
            return;
        if (!delegateSubscribed)
            return;
        CollectableData.OnCollected -= UpdateInfo;
        delegateSubscribed = false;
    }
}
