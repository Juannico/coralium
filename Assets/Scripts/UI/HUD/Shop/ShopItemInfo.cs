using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ShopItemInfo : CoraliumButton
{
    [SerializeField] private Image coinIcon;
    [SerializeField] private Image icon;
    [SerializeField] private TextMeshProUGUI price;

    public void SetInfo(Item item)
    {
        icon.sprite = item.Icon;
        price.text = $"${item.Price}";
    }
    public void Enactive()
    {
        //coinIcon.color = Color.gray;
        icon.color = Color.gray;
        price.color = Color.gray;
    }
}
