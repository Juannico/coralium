using NaughtyAttributes;
using PixelCrushers;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.UI;
using UnityEngine.UI;

public class ShopUI : MenuUI
{
    [Expandable] [SerializeField] private ShopData shopData;
    [Header("Panels")]
    [SerializeField] private GameObject selectItemPanel;
    [SerializeField] private GameObject buyItemPanel;
    [SerializeField] private GameObject confirmPanel;
    [SerializeField] private GameObject buttonsPanel;
    [Header("SelectItem")]
    [SerializeField] private ScrollRect scrollRect;
    [SerializeField] private float padding = 24;
    [SerializeField] private float scrollVelocity = 24;
    [SerializeField] private ItemCollectedInfo coinInfo;
    [SerializeField] private ShopItemInfo itemInfoPrefab;
    [SerializeField] private LocalizeUI itemName;
    [SerializeField] private LocalizeUI itemDescription;
    [Header("Buy")]
    [SerializeField] private CoraliumButton acceptBuyButton;
    [SerializeField] private CoraliumButton cancelBuyButton;
    [SerializeField] private Image buyItemImage;
    [SerializeField] private LocalizeUI buyItemName;
    [SerializeField] private LocalizeUI buyItemAdvice;
    [SerializeField] private TextTableFieldName buyQuestion;
    [SerializeField] private TextTableFieldName notEnoughMoney;
    [Header("Confirm")]
    [SerializeField] private float confirmMessageDuration;

    private Item currentItem;
    private ShopItemInfo currentshopItemButton;
    private int panelIndex = 0;
    private int itemIndex = 0;
    private bool shouldBuy;

    private ShopItemInfo[] shopItemButtons;
    private CoraliumButton[] buyItemButtons;

    private InputSystemUIInputModule inputSystemUIInput;
    private float lastUpdate;

    private float fixedScrollDisplacement;
    [ShowNonSerializedField] private bool scrollig;
    [ShowNonSerializedField] private float timer;
    [ShowNonSerializedField] private float scrollingDuration;
    [ShowNonSerializedField] private float startScrollValue;
    [ShowNonSerializedField] private float targetScrollValue;
    protected override void Awake()
    {
        base.Awake();
        SetItemsList();
        SetSelectAndCancelButtons();
        buttons = shopItemButtons;
        coinInfo.SetInfo(shopData.coin);
        inputSystemUIInput = GameManager.Instance.UIManager.InputSystemUIInput;
        lastUpdate = 0;
        scrollRect.verticalScrollbar.size = 0.75f;
        gameObject.SetActive(false);

    }
    protected override void OnEnable()
    {
        base.OnEnable();
        panelIndex = 1;
        SwithchPanels();
        if (shopItemButtons != null)
            buttons = shopItemButtons;
    }
    private void Update()
    {
        if (!scrollig)
        {
            timer = 0;
            return;
        }
        timer += Time.unscaledDeltaTime;
        scrollRect.verticalScrollbar.value = Mathf.Lerp(startScrollValue, targetScrollValue, timer / scrollingDuration);
        if ((timer / scrollingDuration) >= 1)
            scrollig = false;
    }

    private void SetItemsList()
    {
        shopItemButtons = new ShopItemInfo[shopData.ItemsId.Length];
        for (int i = 0; i < shopData.ItemsId.Length; i++)
        {
            Item item = shopData.GameData.GetItem(shopData.ItemsId[i]);
            if (item == null)
            {
                Debug.LogError("Does not find item with id: {id}");
                return;
            }
            if (!item.CanBuy)
                continue;
            ShopItemInfo shopItemInfo = Instantiate(itemInfoPrefab, scrollRect.content);
            shopItemButtons[i] = shopItemInfo;
            if (shopItemInfo == null)
                throw new System.Exception("Item Info Prefab did not have the ShopItemInfo script assigned");
            shopItemInfo.SetInfo(item);
            int index = i;
            shopItemInfo.OnSelected.AddListener(() =>
            {
                SetItemInfo(item);
                SetFixedScrollDisplacement();
                if (fixedScrollDisplacement > 0 && itemIndex != index)
                    MoveScroll(shopItemInfo.transform as RectTransform);
                currentshopItemButton = shopItemInfo;
                itemIndex = index;
            });
            shopItemInfo.OnClick.AddListener(Select);
            if (i != 0)
                continue;
            SetItemInfo(item);
            currentshopItemButton = shopItemInfo;
        }
    }
    private void SetSelectAndCancelButtons()
    {
        acceptBuyButton.OnSelected.AddListener(() => shouldBuy = true);
        acceptBuyButton.OnClick.AddListener(() =>
        {
            if (panelIndex < 2)
                return;
            Select();
        });
        cancelBuyButton.OnSelected.AddListener(() => shouldBuy = false);
        cancelBuyButton.OnClick.AddListener(() =>
        {
            if (panelIndex < 2)
                return;
            Back();
        });
        buyItemButtons = new CoraliumButton[] {
            acceptBuyButton,
            cancelBuyButton
        };
    }
    public void Buy()
    {
        if (!currentItem.Buy(shopData.coin))
            return;
        currentshopItemButton.Enactive();
        currentshopItemButton.Interactable = false;
        // buyButton.interactable = false;
    }
    private void SelectInteraction(InputAction.CallbackContext ctx) => Select();
    private void Select()
    {
        if (lastUpdate + inputSystemUIInput.moveRepeatRate > Time.unscaledTime)
            return;
        lastUpdate = Time.unscaledTime;
        panelIndex++;
        if (panelIndex >= 4)
            panelIndex = 1;
        SwithchPanels();
    }
    private void BackInteraction(InputAction.CallbackContext ctx) => Back();
    private void Back()
    {
        if (lastUpdate + inputSystemUIInput.moveRepeatRate > Time.unscaledTime)
            return;
        lastUpdate = Time.unscaledTime;
        if (panelIndex == 3)
            panelIndex--;
        panelIndex--;
        SwithchPanels();
    }
    private void SwithchPanels()
    {
        switch (panelIndex)
        {
            case 0:
                gameObject.SetActive(false);
                break;
            case 1:
                selectItemPanel.SetActive(true);
                buttonsPanel.SetActive(true);
                buyItemPanel.SetActive(false);
                confirmPanel.SetActive(false);
                buttons = shopItemButtons;
                break;
            case 2:
                bool canBuy = shopData.coin.Amount >= currentItem.Price;
                buyItemPanel.SetActive(true);
                buttonsPanel.SetActive(true);
                selectItemPanel.SetActive(false);
                confirmPanel.SetActive(false);
                acceptBuyButton.gameObject.SetActive(canBuy);
                cancelBuyButton.gameObject.SetActive(canBuy);
                if (canBuy)
                    acceptBuyButton.Select();
                TextTableFieldName textTableFieldNameToUse = canBuy ? buyQuestion : notEnoughMoney;
                LocalizeUIUtilities.SetLocalizeUI(textTableFieldNameToUse, buyItemAdvice);
                LocalizeUIUtilities.SetLocalizeUI(currentItem.Name, buyItemName);
                buyItemImage.sprite = currentItem.Icon;
                buttons = buyItemButtons;
                // IsAnyButtonSelected();
                break;
            case 3:
                buttons = shopItemButtons;
                if (!shouldBuy || shopData.coin.Amount < currentItem.Price)
                {
                    panelIndex = 1;
                    SwithchPanels();
                    return;
                }
                Buy();
                confirmPanel.SetActive(true);
                buyItemPanel.SetActive(false);
                buttonsPanel.SetActive(false);
                // IsAnyButtonSelected();
                break;
        }
    }
    private void SetFixedScrollDisplacement()
    {
        float displacement = scrollRect.content.rect.height - scrollRect.viewport.rect.height;
        if (displacement < 0)
            fixedScrollDisplacement = 0;
        fixedScrollDisplacement = 1 / displacement;
    }
    private void MoveScroll(RectTransform item)
    {
        float distance = scrollRect.viewport.position.y - item.transform.position.y;
        if (distance < 0)
        {
            StartScroll(distance - padding);
            return;
        }
        distance -= scrollRect.viewport.rect.height - item.rect.height;
        if (distance < 0)
            return;
        StartScroll(distance + padding);
    }

    private void StartScroll(float distance)
    {
        scrollingDuration = Mathf.Abs(distance / (scrollVelocity * 10));
        startScrollValue = scrollRect.verticalScrollbar.value;
        targetScrollValue = startScrollValue - (distance * fixedScrollDisplacement);
        scrollig = true;
    }

    private void SetItemInfo(Item item)
    {
        LocalizeUIUtilities.SetLocalizeUI(item.Name, itemName);
        LocalizeUIUtilities.SetLocalizeUI(item.Description, itemDescription);
        //buyButton.interactable = shopData.coin.Amount >= item.Price;
        currentItem = item;
    }
}