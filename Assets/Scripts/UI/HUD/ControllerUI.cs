using UnityEngine;
[RequireComponent(typeof(UIInputAdapter))]
public class ControllerUI : BaseScreenUI, IUIAdapter
{
    [SerializeField] private GameObject kbUI;
    [SerializeField] private GameObject gamePadUI;
    [SerializeField] private CoraliumButton backButton;
    [SerializeField] private ButtonPrompt gamepadBackPrompt;
    protected override void Awake()
    {
        base.Awake();
        gameObject.AddComponent<UIInputAdapter>();
        backButton.OnClick.AddListener(() => GoBackToPreviousScreen(default));
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        UpdateOnInputDeviceChanged(PlayerInputManager.Instance.DeviceChange.CurrentControlSchema);
    }

    public void UpdateOnInputDeviceChanged(string newDevice)
    {
        kbUI.SetActive(false);
        gamePadUI.SetActive(false);
        if (newDevice.ToLower().Equals(Constants.GamepadString))
        {
            gamepadBackPrompt.gameObject.SetActive(true);
            backButton.gameObject.SetActive(false);
            gamePadUI.SetActive(true);
        }
        else
        {
            kbUI.SetActive(true);
            gamepadBackPrompt.gameObject.SetActive(false);
            backButton.gameObject.SetActive(true);
        }
    }
}
