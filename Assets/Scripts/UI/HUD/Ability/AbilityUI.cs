using TMPro;
using UnityEngine;

public class AbilityUI : MonoBehaviour
{
    [SerializeField] private BaseAbilityCast baseAbilityCastPrefab;
    [SerializeField] private PlayerAbilities playerAbilities;
    [SerializeField] private GameObject kbIcon;
    [SerializeField] private GameObject gamepadIcon;
    [SerializeField] private TextMeshProUGUI timer;
    private GameObject timerContainer;
    private BaseAbilityCast baseAbilityCast;
    private float lastTime;
    private void Awake()
    {
        PlayerInputManager.Instance.DeviceChange.BindToInputDeviceChanged(SwitchPromptGraphics);
        PlayerInputManager.Instance.DeviceChange.CallOnDeviceChanged();
    }
    private void Start()
    {
        baseAbilityCast = playerAbilities.GetPlayerAbility(baseAbilityCastPrefab);
        baseAbilityCast.Data.OnSwitchLockAbility += SetAbilityUIVisibility;
        timerContainer = timer.transform.parent.gameObject;
        timerContainer.gameObject.SetActive(false);
        SetAbilityUIVisibility(baseAbilityCast.IsLocked);
    }
    private void OnDestroy()
    {
        RemoveCallBacks();
    }
    protected virtual void RemoveCallBacks()
    {
        PlayerInputManager.Instance.DeviceChange.UnbindFromInputDeviceChanged(SwitchPromptGraphics);
        baseAbilityCast.OnTimerChange -= UpdateTime;
        baseAbilityCast.Data.OnSwitchLockAbility -= SetAbilityUIVisibility;
    }
    private void SwitchPromptGraphics(string newSchema)
    {
        kbIcon.SetActive(newSchema != "gamepad");
        gamepadIcon.SetActive(newSchema == "gamepad");
    }
    private void UpdateTime(float time)
    {
        time = baseAbilityCast.Data.Cooldown - time;
        if (time <= 0)
            time = 0;
        if (lastTime == time)
        {
            if (timerContainer.activeInHierarchy)
                timerContainer.SetActive(false);
            return;
        }
        if (!timerContainer.activeInHierarchy)
            timerContainer.SetActive(true);
        timer.text = time.ToString("0.0");
        lastTime = time;
    }
    private void SetAbilityUIVisibility(bool state) => gameObject.SetActive(!state);
}
