using NaughtyAttributes;
using UnityEngine;

public class DashGuide : MonoBehaviour
{
    [ShowNonSerializedField]private int amounObjectsInView;
    public void Show()
    {
        amounObjectsInView++;
        if (!gameObject.activeInHierarchy)
            gameObject.SetActive(true);
    }

    public void Hide()
    {
        amounObjectsInView--;
        if (amounObjectsInView > 0)
            return;
        if (gameObject.activeInHierarchy)
            gameObject.SetActive(false);
    }
}
