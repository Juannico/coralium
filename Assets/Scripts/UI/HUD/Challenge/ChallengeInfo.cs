using PixelCrushers;
using TMPro;
using UnityEngine;

public class ChallengeInfo : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI variableText;
    [SerializeField] private LocalizeUI localizeUI;

    public void SetInfo(TextTableFieldName textTableFieldName)
    {
        localizeUI.fieldName = textTableFieldName.FieldName;
        localizeUI.textTable = textTableFieldName.TextTable;
        localizeUI.UpdateText();
    }
    public void UpdateInfo(string newText)
    {
        variableText.text = newText;
    }
}
