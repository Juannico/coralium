using NaughtyAttributes;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

public class PlayerEnergy : MonoBehaviour
{
    [SerializeField] private Image characterBackgroundImage;
    [SerializeField] private Image characterFillImage;
    [SerializeField] private Animator animator;
    [SerializeField] private SpriteSequence effectSpriteSequence;
    [SerializeField] private MoveToTarget effectPivot;
    [SerializeField] private SpriteSequence idleSpriteSequence;
    [SerializeField] private float fillSpeed;
    [Expandable] [SerializeField] private BaseCharacter character;

    private float targetFillAmount;
    private float startFillAmount;
    private float timer;

    private Vector3 middlePosition;

    private void Awake()
    {
        Assert.IsNotNull(character, "Character was null");
        character.Data.OnCharacterEnergyChange += UpdateEnergyUI;
        characterFillImage.fillAmount = 1;
        middlePosition = new Vector3(Screen.width * 0.5f, Screen.height * 0.5f, 0);
        enabled = false;
    }
    private void Update()
    {
        timer += Time.deltaTime * fillSpeed;
        characterFillImage.fillAmount = Mathf.Lerp(startFillAmount, targetFillAmount, timer);
        if (characterFillImage.fillAmount == targetFillAmount)
            enabled = false;
    }
    private void OnDestroy()
    {
        character.Data.OnCharacterEnergyChange -= UpdateEnergyUI;
    }
    public void UpdateEnergyUI(float newEnergy)
    {
        targetFillAmount = newEnergy / character.Data.Energy;
        startFillAmount = characterFillImage.fillAmount;
        timer = 0;
        if (targetFillAmount == startFillAmount)
            return;
        if (targetFillAmount > startFillAmount)
        {
            effectPivot.transform.position = middlePosition;
            effectSpriteSequence.StartAnim();
            animator.SetTrigger("RestoreEnergy");
            effectPivot.StartToMove(false);
        }
        enabled = true;
    }
    public void Idle() => idleSpriteSequence.StartAnim();
}
