using UnityEngine;

public class PlayerHealthPoint : MonoBehaviour
{
    [SerializeField] private GameObject inactiveHealthPoint;
    [SerializeField] private GameObject activeHealthPoint;
    [SerializeField] private SpriteSequence shineSpriteSequnce;
    [SerializeField] private MoveToTarget healtPointMoveToTarget;
    [SerializeField] private Animator animator;
    private Vector3 middlePosition;

    public delegate void OnAction();
    public OnAction Reset;
    private void Awake()
    {
        activeHealthPoint.SetActive(false);
        middlePosition = new Vector3(Screen.width * 0.5f, Screen.height * 0.5f, 0);
    }
    public void AddHealthPoint(bool firstTime = false)
    {
        activeHealthPoint.SetActive(true);
        if (firstTime)
            return;
        healtPointMoveToTarget.transform.position = middlePosition;
        animator.SetTrigger("AddHealth");
        healtPointMoveToTarget.StartToMove(false);
    }
    public void EndAddAnimation()
    {
        Reset?.Invoke();
    }
    public void ResetIdle()
    {
        animator.SetTrigger("Reset");
    }
    public void RemoveHealthPoint()
    {
        animator.SetTrigger("LoseHealth");
    }

    public void StartIdle()
    {
        if (activeHealthPoint.activeInHierarchy)
            shineSpriteSequnce.StartAnim();
        else
            shineSpriteSequnce.StopAnim();
    }
}
