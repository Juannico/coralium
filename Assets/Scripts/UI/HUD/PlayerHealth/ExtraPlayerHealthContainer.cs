using NaughtyAttributes;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class ExtraPlayerHealthContainer : MonoBehaviour
{
    [Header("UI Prefabs")]
    [SerializeField] private ExtraPlayerHealthPoint extraPlayerHealthPointPrefab;
    [Header("Player")]
    [Expandable][SerializeField] private BaseCharacter mainCharacter;
    private List<ExtraPlayerHealthPoint> extraPlayerHealthPoints = new List<ExtraPlayerHealthPoint>();

    private void Awake()
    {
        if (mainCharacter == null) throw new System.Exception("No base character assigned!");
        mainCharacter.Data.OnCharacterExtraHealthChange += UpdateUIHealth;
    }
    private void OnDestroy()
    {
        mainCharacter.Data.OnCharacterExtraHealthChange += UpdateUIHealth;
    }

    public void UpdateUIHealth(int newExtraHealth)
    {
        while (newExtraHealth > extraPlayerHealthPoints.Count)
        {
            ExtraPlayerHealthPoint extraPlayerHealthPoint = Instantiate(extraPlayerHealthPointPrefab, transform);
            extraPlayerHealthPoints.Add(extraPlayerHealthPoint);
        }
        for (int i = 0; i < extraPlayerHealthPoints.Count; i++)
        {
            extraPlayerHealthPoints[i].RemoveHealthPoint();
            if (newExtraHealth - 1 >= i)
                extraPlayerHealthPoints[i].AddHealthPoint();
        }
    }
}
