using UnityEngine;

public class ExtraPlayerHealthPoint : MonoBehaviour
{
    [SerializeField] private GameObject activeHealthPoint;

    private void Awake()
    {
        activeHealthPoint.SetActive(false);
    }
    /// <summary>
    /// Probably have to change this for animations later on
    /// </summary>
    public void AddHealthPoint() => activeHealthPoint.SetActive(true);
    public void RemoveHealthPoint() => activeHealthPoint.SetActive(false);
}
