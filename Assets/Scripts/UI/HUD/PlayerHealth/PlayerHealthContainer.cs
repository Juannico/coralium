using NaughtyAttributes;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealthContainer : MonoBehaviour
{
    [Header("UI Prefabs")]
    [SerializeField] private PlayerHealthPoint playerHealthPointPrefab;
    [Header("Player")]
    [Expandable] [SerializeField] private BaseCharacter mainCharacter;

    private PlayerHealthPoint[] playerHealthPoints;
    private int healthPointIndex;
    private int currenHealth;

    private void Awake()
    {
        if (mainCharacter == null)
            throw new System.Exception("No base character assigned!");
        mainCharacter.Data.OnCharacterInitialized += PopulatePlayerHealth;
        mainCharacter.Data.OnCharacterHealthChange += UpdateUIHealth;
        mainCharacter.Data.OnCharacterHealthAmountChange += SetPlayerHealthPoints;
    }
    private void OnDestroy()
    {
        mainCharacter.Data.OnCharacterInitialized -= PopulatePlayerHealth;
        mainCharacter.Data.OnCharacterHealthChange -= UpdateUIHealth;
        mainCharacter.Data.OnCharacterHealthAmountChange -= SetPlayerHealthPoints;
    }
    public void PopulatePlayerHealth()
    {
        if (playerHealthPointPrefab == null)
            throw new System.Exception("No player health point prefab assigned on UIManager");
        int maxHealth = mainCharacter.Data.MaxHealth;
        playerHealthPoints = new PlayerHealthPoint[maxHealth];
        healthPointIndex = mainCharacter.Data.Health;
        currenHealth = mainCharacter.Data.CurrentHealth;
        for (int i = 0; i < mainCharacter.Data.MaxHealth; i++)
        {
            playerHealthPoints[i] = Instantiate(playerHealthPointPrefab, transform);
            playerHealthPoints[i].Reset += ResetIdle;
            playerHealthPoints[i].name = $"{playerHealthPointPrefab.name}_{i}";
            if (playerHealthPoints[i] == null) throw new System.Exception("Health point prefab did not have the PlayerHealthPoint script assigned");
            if (i < healthPointIndex)
            {
                playerHealthPoints[i].AddHealthPoint(true);
                playerHealthPoints[i].gameObject.SetActive(true);
                if (i >= currenHealth)
                    playerHealthPoints[i].RemoveHealthPoint();
                continue;
            }

            playerHealthPoints[i].gameObject.SetActive(false);
            playerHealthPoints[i].RemoveHealthPoint();
        }
    }

    public void UpdateUIHealth(int newHealth)
    {
        if (playerHealthPoints == null || playerHealthPoints.Length <= 0)
            return;
        if (newHealth == currenHealth)
            return;
        int count = newHealth - currenHealth;
        if (newHealth > currenHealth)
        {
            for (int i = currenHealth; i < currenHealth + count; i++)
                playerHealthPoints[i].AddHealthPoint();
        }
        else
        {
            for (int i = currenHealth; i > currenHealth + count; i--)
                playerHealthPoints[i - 1].RemoveHealthPoint();
        }
        currenHealth = newHealth;
    }

    public void SetPlayerHealthPoints(int newMaxHealth)
    {
        if (playerHealthPoints == null || playerHealthPoints.Length <= 0)
            return;
        for (int i = 0; i < playerHealthPoints.Length; i++)
        {
            if (!playerHealthPoints[i].gameObject.activeInHierarchy)
                playerHealthPoints[i].gameObject.SetActive(i < newMaxHealth);
        }
    }
    private void ResetIdle()
    {
        for (int i = 0; i < currenHealth; i++)
        {
            if (playerHealthPoints[i].gameObject.activeInHierarchy)
                playerHealthPoints[i].ResetIdle();
        }
    }
}
