using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPoisionShieldPoint : MonoBehaviour
{
    [SerializeField] private GameObject inactivePoisionedShieldPoint;
    [SerializeField] private GameObject activePoisionedShieldPoint;

    private void Awake()
    {
        inactivePoisionedShieldPoint.SetActive(false);
    }
    /// <summary>
    /// Probably have to change this for animations later on
    /// </summary>
    public void AddHealthPoint() => inactivePoisionedShieldPoint.SetActive(true);
    public void RemoveHealthPoint() => inactivePoisionedShieldPoint.SetActive(false);
}
