using NaughtyAttributes;
using UnityEngine;

public class PlayerPoisionShieldContainer : MonoBehaviour
{
    [SerializeField] private int showTime;
    [Header("UI Prefabs")]
    [SerializeField] private GameObject PlayerPoisionedShiedlPointPrefab;
    [Header("Player")]
    [Expandable] [SerializeField] private BaseCharacter mainCharacter;

    private PlayerPoisionShieldPoint[] playerPoisionedShieldPoints;

    private float shieldValue;
    private float timer;

    private void Awake()
    {
        shieldValue = 0;
        if (mainCharacter == null) throw new System.Exception("No base character assigned!");
        mainCharacter.Data.OnCharacterInitialized += PopulatePlayerPoisionShield;
        mainCharacter.Data.OnCharacterShieldChange += UpdateUIPoisionShield;
    }
    private void OnDestroy()
    {
        mainCharacter.Data.OnCharacterInitialized -= PopulatePlayerPoisionShield;
        mainCharacter.Data.OnCharacterShieldChange -= UpdateUIPoisionShield;
    }
    private void Update()
    {
        timer += Time.deltaTime;
        if (timer < showTime)
            return;
        if (shieldValue == 0 || shieldValue == mainCharacter.Data.Shield)
            gameObject.SetActive(false);
    }
    private void PopulatePlayerPoisionShield()
    {
        if (PlayerPoisionedShiedlPointPrefab == null) throw new System.Exception("No player health point prefab assigned on UIManager");
        playerPoisionedShieldPoints = new PlayerPoisionShieldPoint[mainCharacter.Data.Shield];
        for (int i = 0; i < mainCharacter.Data.Shield; i++)
        {
            playerPoisionedShieldPoints[i] = Instantiate(PlayerPoisionedShiedlPointPrefab, transform).GetComponent<PlayerPoisionShieldPoint>();
            if (playerPoisionedShieldPoints[i] == null) throw new System.Exception("Health point prefab did not have the PlayerHealthPoint script assigned");
        }
        gameObject.SetActive(false);
    }
    private void UpdateUIPoisionShield(int newExtraHealth)
    {
        if (newExtraHealth != shieldValue)
            gameObject.SetActive(true);
        shieldValue = newExtraHealth;
        timer = 0;
        for (int i = 0; i < playerPoisionedShieldPoints.Length; i++)
        {
            playerPoisionedShieldPoints[i].RemoveHealthPoint();
            if (newExtraHealth - 1 >= i)
                playerPoisionedShieldPoints[i].AddHealthPoint();
        }
    }
}
