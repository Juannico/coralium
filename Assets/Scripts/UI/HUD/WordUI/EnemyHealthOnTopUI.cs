using System;
using UnityEngine;
using UnityEngine.UI;

public class EnemyHealthOnTopUI : BaseWorldSpaceUI
{
    [SerializeField] private HealthBar healthBar;
    [SerializeField] private Image backGround; 
    private Data data;
    public virtual void Initialize(Data data)
    {
        this.data = data;
        healthBar.Initilize(data.Health);
        this.data.OnCharacterHealthChange += UpdateHealtBar;
        backGround.color = data.UIColor;
    }

    private void UpdateHealtBar(int intToChange) => healthBar.SetHealth(intToChange);
    private void OnEnable()
    {
        if (data != null)
            data.OnCharacterHealthChange += UpdateHealtBar;
    }
    private void OnDisable()
    {
        if (data != null)
            data.OnCharacterHealthChange -= UpdateHealtBar;
    }
}
