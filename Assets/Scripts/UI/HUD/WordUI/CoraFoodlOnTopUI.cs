using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CoraFoodlOnTopUI : BaseWorldSpaceUI
{
    [SerializeField] private Image meterBar;
    [SerializeField] private TextMeshProUGUI countInfo;
    [SerializeField] private CollectableData polypCollectableData;
    [SerializeField] private LevelManager levelManager;
    public override void Initialize(Transform onTopUI)
    {
        base.Initialize(onTopUI);
        UpdateUI(0);
        polypCollectableData.OnCollected += UpdateUI;
    }
    private void OnDestroy()
    {
        polypCollectableData.OnCollected -= UpdateUI;
    }
    private void UpdateUI(int valueAdded)
    {
        int value = Mathf.Clamp(polypCollectableData.Amount + valueAdded, 0, levelManager.CurrentLevelData.GrowCoralData.FoodNumber);
        if (levelManager.CurrentLevelData.GrowCoralData.IsPlanted)
            gameObject.SetActive(false);
        countInfo.text = $"{value}/{levelManager.CurrentLevelData.GrowCoralData.FoodNumber}";
        meterBar.fillAmount = (float)value / levelManager.CurrentLevelData.GrowCoralData.FoodNumber;

    }
}
