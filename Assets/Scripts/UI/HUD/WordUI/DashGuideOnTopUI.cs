using UnityEngine;

public class DashGuideOnTopUI : BaseWorldSpaceUI
{
    [SerializeField] private Animator animator;

    public override void Initialize(Transform onTopUI)
    {
        base.Initialize(onTopUI);
        animator.SetTrigger("Hide");
    }
    public override void SetUIDisplay(bool show)
    {
        if (show)
            base.SetUIDisplay(true);
        animator.SetTrigger(show ? "Init" : "Hide");
    }
    public void Hide() => base.SetUIDisplay(false);
}
