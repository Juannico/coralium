using UnityEngine;
using UnityEngine.UI;

public class BossEnemyHealthOnTopUI : EnemyHealthOnTopUI
{
    [SerializeField] private Image[] decorations;
    public override void Initialize(Data data)
    {
        base.Initialize(data);
        for (int i = 0; i < decorations.Length; i++)
            decorations[i].color = data.UIColor;
    }
}
