using UnityEngine;

public class BaseWorldSpaceUI : MonoBehaviour
{
    public BaseWorldSpaceUI type { get => this; }
    [HideInInspector] public Transform OnTopUI;
    [HideInInspector] public bool IsActive;
    public virtual void Initialize(Transform onTopUI)
    {
        IsActive = true;
        OnTopUI = onTopUI;
    }


    public virtual void SetUIDisplay(bool show)
    {
        if (IsActive)
            OnTopUI.gameObject.SetActive(show);
    }
}
