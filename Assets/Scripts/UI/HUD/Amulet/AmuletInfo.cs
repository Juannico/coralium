using UnityEngine;
using UnityEngine.UI;

public class AmuletInfo : CoraliumButton
{
    [Header("Amulet Button Settings")]
    [SerializeField] private Image icon;
    [SerializeField] private Image border;
    [SerializeField] private Color lockedColor;
    [SerializeField] private Color enactiveColor;
    [SerializeField] private bool amuletChangeScaleOnSelect = true;

    [field:SerializeField]public Amulet Amulet { get; private set; }
    protected override void Awake()
    {
        base.Awake();
    }
    public void SetInfo(Amulet amulet, bool changeInteractable = true)
    {

        if (amulet == null)
        {
            icon.color = lockedColor;
            icon.sprite = null;
            return;
        }
        Amulet = amulet;
        if (amulet.Purchased)
            icon.sprite = amulet.Icon;
        icon.color = Color.white;
        border.color = Color.white;
        amulet.OnPurchaseChange += (purchaseState) =>
        {
            if (!purchaseState)
                return;
            Interactable = true;
            icon.sprite = amulet.Icon;
        };
        OnInteractableChange.AddListener((interactableState) =>
        {
            if (amulet == null)
            {
                icon.color = Color.white;
                border.color = Color.white;
                return;
            }
            if (!amulet.Purchased)
            {
                icon.color = lockedColor;
                border.color = enactiveColor;
                return;
            }
            if (!interactableState)
            {
                icon.color = enactiveColor;
                border.color = enactiveColor;
                return;
            }
            icon.color = Color.white;
            border.color = Color.white;
        });
        if (changeInteractable)
            Interactable = amulet.Purchased;
    }

    protected override void ButtonSelectedEffect()
    {
        base.ButtonSelectedEffect();
        if (!amuletChangeScaleOnSelect)
            return;
        // Use LeanTween instead
        //icon.transform.localScale = targetScale;
        //border.transform.localScale = targetScale;
    }

    protected override void ButtonDeselectedEffect()
    {
        base.ButtonDeselectedEffect();
        if (!amuletChangeScaleOnSelect)
            return;
        icon.transform.localScale = Vector3.one;
        border.transform.localScale = Vector3.one;
    }
}
