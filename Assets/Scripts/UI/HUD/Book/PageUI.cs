using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PageUI : MonoBehaviour
{
    [SerializeField] private Image background;
    [SerializeField] private Image image;
    [SerializeField] private TextMeshProUGUI pageIndex;
    [SerializeField] private Color foundColor;
    [SerializeField] private Color notFoundColor;
    public void SetPage(BookPage bookPage, int index)
    {
        image.sprite = bookPage.Image;
        pageIndex.text = index.ToString();
        if (bookPage.Found)
            return;
        image.gameObject.SetActive(false);
        background.color = notFoundColor;
        bookPage.OnFoundChange += (foundState) =>
        {
            image.gameObject.SetActive(true);
            if (!foundState)
                background.color = foundColor;
        };
    }
}
