using UnityEngine;
using UnityEngine.InputSystem;

public class BookUI : BaseScreenUI
{
    [SerializeField] private Book book;
    [SerializeField] private PageUI pageUIPrefab;
    [SerializeField] private int moveAmount;
    [SerializeField] private float transitionDuration = 0.75f;
    [SerializeField] private Transform container;
    private int moveIndex = 0;
    private Vector3 targetPosition;
    private bool isMoving;
    protected override void Awake()
    {
        base.Awake();
        SetPages();
    }



    protected override void OnDisable()
    {
        base.OnDisable();
        if (!isMoving)
            return;
        container.transform.position = targetPosition;
        isMoving = false;
    }

    private void Navigate(InputAction.CallbackContext ctx)
    {
        if (isMoving)
            return;
        float direction = ctx.ReadValue<Vector2>().x;
        if (direction == 0)
            return;
        if ((direction > 0 && moveIndex == book.Pages.Length) || (direction < 0 && moveIndex == 0))
            return;
        isMoving = true;
        Vector3 move = Vector3.left * moveAmount;
        if (direction < 0)
            move *= -1;
        Vector3 startPosition = container.position;
        targetPosition = startPosition + move;
        float timer = 0;
        moveIndex += direction > 0 ? 1 : -1;
        StartCoroutine(CoroutineHandler.ExecuteActionAfterUpdateUntil(() =>
        {
            timer += Time.unscaledDeltaTime;
            float fixedTime = timer / transitionDuration;
            container.position = Vector3.Lerp(startPosition, targetPosition, Mathf.Pow(fixedTime, 3));
            return timer >= transitionDuration;
        }, () =>
         {
             container.position = targetPosition;
             isMoving = false;
         }
        ));
    }
    private void SetPages()
    {
        for (int i = 0; i < book.Pages.Length; i++)
        {
            PageUI pageUi = Instantiate(pageUIPrefab, container);
            pageUi.SetPage(book.Pages[i],i);
        }
    }
}
