using UnityEngine;

public class SelectMenu : MonoBehaviour
{
    [SerializeField] private InputButton[] buttons;
    [SerializeField] private GameObject gamepadUI;
    [SerializeField] private GameObject panel;
    private PlayerInputActions inputActions;
    private int buttonIndex = 0;
    private int prewButtonIndex = 0;
    private bool moving;
    private UIManager uiManager;
    private void Awake()
    {
        inputActions = new PlayerInputActions();
        for (int i = 0; i < buttons.Length; i++)
        {
            int index = i;
            buttons[i].OnClick.AddListener(() =>
            {
                prewButtonIndex = index;
                HandleSelectMenu();
                buttonIndex = index;
            });
            buttons[i].SetAction(true);
        }
    }
    private void Start()
    {
        for (int i = 0; i < buttons.Length; i++)
            HanldedSelecButton(buttons[i]);
        buttons[0].ButtonSelected();
        panel.SetActive(false);
        uiManager = GameManager.Instance.UIManager;
    }
    private void OnEnable()
    {
        inputActions.Enable();
        AddDelegates();
    }

    private void OnDisable()
    {
        inputActions.Disable();
        RemoveDelegates();
    }
    private void OnDestroy()
    {
        RemoveDelegates();
    }
    private void AddDelegates()
    {
        PlayerInputManager.Instance.DeviceChange.BindToInputDeviceChanged(SwitchPromptGraphics);
        if (inputActions == null)
            inputActions = new PlayerInputActions();
        inputActions.TabMenu.RT.performed += MoveRightCallBack;
        inputActions.TabMenu.LT.performed += MoveLeftCallBack;
        inputActions.UI.Cancel.performed += ClosePanel;
        for (int i = 0; i < buttons.Length; i++)
            buttons[i].SetAction(true);
    }

    private void RemoveDelegates()
    {
        PlayerInputManager.Instance.DeviceChange.UnbindFromInputDeviceChanged(SwitchPromptGraphics);
        if (inputActions == null)
            return;
        inputActions.TabMenu.RT.performed -= MoveRightCallBack;
        inputActions.TabMenu.LT.performed -= MoveLeftCallBack;
        inputActions.UI.Cancel.performed -= ClosePanel;
        for (int i = 0; i < buttons.Length; i++)
            buttons[i].SetAction(false);
    }
    private void HanldedSelecButton(CoraliumButton coraliumButton)
    {
        coraliumButton.ExternalSet = true;
        coraliumButton.OnClick.AddListener(() =>
        {
            for (int i = 0; i < buttons.Length; i++)
            {
                CoraliumButton button = buttons[i];
                if (!button.Interactable)
                    continue;
                if (button == coraliumButton)
                    button.ButtonSelected();
                else
                    button.Deselected();
            }
        }
        );
    }

    private void MoveRightCallBack(UnityEngine.InputSystem.InputAction.CallbackContext ctx) => HandleControllerNavigation(true);
    private void MoveLeftCallBack(UnityEngine.InputSystem.InputAction.CallbackContext ctx) => HandleControllerNavigation(false);
    private void HandleControllerNavigation(bool goRigth)
    {
        if (!panel.activeInHierarchy)
            return;
        int sideNumber = goRigth ? 1 : -1;
        buttonIndex = (buttonIndex + sideNumber) % buttons.Length;
        if (buttonIndex < 0)
            buttonIndex = buttons.Length - 1;
        if (buttons[buttonIndex].Interactable)
        {
            moving = true;
            buttons[buttonIndex].OnClick?.Invoke();
            moving = false;
            return;
        }
        HandleControllerNavigation(goRigth);
    }
    private void SwitchPromptGraphics(string newSchema)
    {
        if (newSchema == "gamepad")
            gamepadUI.SetActive(true);
        else
            gamepadUI.SetActive(false);
    }
    private void HandleSelectMenu()
    {
        if (!panel.activeInHierarchy)
        {
            panel.SetActive(true);
            return;
        }
        if (prewButtonIndex == buttonIndex && !moving)
            panel.SetActive(false);

    }
    private void ClosePanel(UnityEngine.InputSystem.InputAction.CallbackContext ctx) => panel.SetActive(false);

}

