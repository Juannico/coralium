using NaughtyAttributes;
using System;
using UnityEngine;

public class MapUI : BaseScreenUI
{
    [SerializeField] private MapData mapData;
    [SerializeField] private GameObject[] mapParts;
    [SerializeField] private float minScale;
    [SerializeField] private float maxScale;
    [SerializeField] private float moveVelocity = 10;
    [SerializeField] private float zoomVelocity = 1;
    [SerializeField] private RectTransform area;
    [SerializeField] private RectTransform mapTransform;
    [SerializeField] private GameObject upArrow;
    [SerializeField] private GameObject downArrow;
    [SerializeField] private GameObject leftArrow;
    [SerializeField] private GameObject rightArrow;
    [SerializeField] private ButtonPrompt zoomInButton;
    [SerializeField] private ButtonPrompt zoomOutButton;
    [SerializeField] private int zoomInButtonDataIndex;
    [SerializeField] private int zoomOutButtonDataIndex;
    [Header("MapButton")]
    [SerializeField] private NavigateButtons mapNavigateButton;
    [ReadOnly] private Vector2 maxMoveDistance;
    private Vector3 moveDirection;
    private Vector2 minBorders;
    private Vector2 maxBorder;
    private float padding;

    private bool startValuesSetted;
    private Vector3 centerPosition;
    private Vector2 outputPlane;
    private Transform playertTransform;
    private Vector2 offsetMutiplier;

    private GameObject minimapCameraObject;
    protected override void Awake()
    {
        base.Awake();
        maxMoveDistance.x = mapTransform.rect.width - area.rect.width;
        maxMoveDistance.y = mapTransform.rect.height - area.rect.height;
        maxMoveDistance *= 0.5f;
        padding = upArrow.GetComponent<RectTransform>().rect.height;
        minBorders.x = leftArrow.transform.position.x - padding * 0.5f;
        minBorders.y = downArrow.transform.position.y - padding * 0.5f;
        maxBorder.x = rightArrow.transform.position.x + padding * 0.5f;
        maxBorder.y = upArrow.transform.position.y + padding * 0.5f;
        SetMapStartValues();
        mapData.Initialize();
        for (int i = 0; i < mapData.mapParts.Length; i++)
        {
            bool partUnlocked = mapData.mapParts[i].IsUnlocked;
            mapParts[i].SetActive(!partUnlocked);
            if (!partUnlocked)
                mapData.mapParts[i].OnUnlockeIndex += UnlockMap;
        }
    }
    private void SetMapStartValues()
    {
        if (startValuesSetted)
            return;
        startValuesSetted = true;
        minimapCameraObject = GameObject.FindGameObjectWithTag(Tags.MinimapCamera);

        if (minimapCameraObject != null)
        {
            SetValues();
            return;
        }
        CoroutineHandler.ExecuteActionAfterUpdateUntil(() =>
        {
            minimapCameraObject = GameObject.FindGameObjectWithTag(Tags.MinimapCamera);
            return minimapCameraObject != null;
        }, () => SetValues());
    }

    private void SetValues()
    {
        Camera minimapCamera = minimapCameraObject.GetComponent<Camera>();
        minimapCameraObject.SetActive(false);
        centerPosition = minimapCamera.transform.position;
        outputPlane = new Vector2(minimapCamera.pixelRect.width, minimapCamera.pixelRect.height);
        offsetMutiplier.x = mapTransform.rect.width / (3 * outputPlane.x);
        offsetMutiplier.y = mapTransform.rect.height / (3 * outputPlane.y);
       // playertTransform = GameObject.FindGameObjectWithTag(Tags.Player).transform;
        //SetStarMapPosition();
    }
    private void Update()
    {
        MoveMap();
        Zoom();
    }
    protected override void OnEnable()
    {
        base.OnEnable();
        SetZoomButton(zoomInButton, zoomInButtonDataIndex);
        SetZoomButton(zoomOutButton, zoomOutButtonDataIndex);
        SetMapStartValues();
        minimapCameraObject.SetActive(true);
    }
    protected override void OnDisable()
    {
        base.OnDisable();
        zoomInButton.gameObject.SetActive(false);
        zoomOutButton.gameObject.SetActive(false);
        minimapCameraObject.SetActive(false);
    }
    private void SetZoomButton(ButtonPrompt buttonpromp, int index)
    {
        buttonpromp.gameObject.SetActive(true);
    }
    protected override void OnDestroy()
    {
        base.OnDestroy();
        foreach (MapItem mapPart in mapData.mapParts)
        {
            if (!mapPart.IsUnlocked)
                mapPart.OnUnlockeIndex -= UnlockMap;
        }
    }
    private void MoveMap()
    {
        //moveDirection = interactions.MoveMap.ReadValue<Vector2>().normalized;
        HandleMouseMoveMap();
        SetArrowActive(upArrow, moveDirection.y > 0);
        SetArrowActive(downArrow, moveDirection.y < 0);
        SetArrowActive(leftArrow, moveDirection.x < 0);
        SetArrowActive(rightArrow, moveDirection.x > 0);
        if (moveDirection == Vector3.zero)
            return;
        SetMapPosition(mapTransform.localPosition + (moveVelocity * -moveDirection * Time.unscaledDeltaTime * 100));
    }

    private void HandleMouseMoveMap()
    {
        Vector2 mousePosition = Vector2.zero;// interactions.Point.ReadValue<Vector2>();
        if (mousePosition == Vector2.zero)
            return;
        if (!MathUtilities.IsInRange(mousePosition.x, minBorders.x, maxBorder.x) || !MathUtilities.IsInRange(mousePosition.y, minBorders.y, maxBorder.y))
            return;
        bool inXRange = false;
        if (MathUtilities.IsInRange(mousePosition.x, minBorders.x, minBorders.x + padding))
        {
            moveDirection.x = -1;
            inXRange = true;
        }
        if (!inXRange && MathUtilities.IsInRange(mousePosition.x, maxBorder.x - padding, maxBorder.x))
            moveDirection.x = 1;
        bool inYRange = false;
        if (MathUtilities.IsInRange(mousePosition.y, minBorders.y, minBorders.y + padding))
        {
            moveDirection.y = -1;
            inYRange = true;
        }
        if (!inYRange && MathUtilities.IsInRange(mousePosition.y, maxBorder.y - padding, maxBorder.y))
            moveDirection.y = 1;
    }
    private void SetArrowActive(GameObject arrow, bool active)
    {
        if (arrow.activeInHierarchy != active)
            arrow.SetActive(active);
    }
    private void Zoom()
    {
        float zoomValue = Vector2.zero.y; // interactions.ScrollWheel.ReadValue<Vector2>().y; ;
        if (zoomValue == 0)
            return;
        if (mapTransform.localScale.x >= maxScale && zoomValue > 0)
            return;
        if (mapTransform.localScale.x <= minScale && zoomValue < 0)
            return;
        float valueToAdd = zoomValue > 0 ? zoomVelocity * 100 : -zoomVelocity * 100;
        Vector3 newScale = mapTransform.localScale + Vector3.one * valueToAdd * Time.unscaledDeltaTime;
        if (newScale.x > maxScale)
            newScale = Vector3.one * maxScale;
        if (newScale.x < minScale)
            newScale = Vector3.one * minScale;
        mapTransform.localScale = newScale;
        maxMoveDistance.x = (mapTransform.rect.width * newScale.x) - area.rect.width;
        maxMoveDistance.y = (mapTransform.rect.height * newScale.x) - area.rect.height;
        maxMoveDistance *= 0.5f;
        SetMapPosition(mapTransform.localPosition);
    }

    private void SetMapPosition(Vector3 newposition)
    {
        newposition.x = Mathf.Clamp(newposition.x, -maxMoveDistance.x, maxMoveDistance.x);
        newposition.y = Mathf.Clamp(newposition.y, -maxMoveDistance.y, maxMoveDistance.y);
        if (newposition == mapTransform.localPosition)
            return;
        mapTransform.localPosition = newposition;
    }

    private void SetStarMapPosition()
    {
        Vector3 direction = centerPosition - playertTransform.position;
        Vector3 offset = new Vector3(-direction.z, direction.x, direction.y);
        offset.z = 0;
        offset *= offsetMutiplier;
        transform.localScale = Vector3.one;
        maxMoveDistance.x = mapTransform.rect.width - area.rect.width;
        maxMoveDistance.y = mapTransform.rect.height - area.rect.height;
        maxMoveDistance *= 0.5f;
        SetMapPosition(offset);
    }
    private void UnlockMap(int index)
    {
        mapParts[index].SetActive(false);
        mapData.mapParts[index].OnUnlockeIndex -= UnlockMap;
    }
}
