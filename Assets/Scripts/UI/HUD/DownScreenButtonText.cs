using UnityEngine;

public class DownScreenButtonText : MonoBehaviour
{
    [SerializeField] private ButtonPrompt buttonPromp;
    public delegate void OnAction();
    private DeactivateAbilitiesOnUI deactivateAbilitiesOnUI;
    private void Awake()
    {
        deactivateAbilitiesOnUI = gameObject.GetComponent<DeactivateAbilitiesOnUI>();
        deactivateAbilitiesOnUI.enabled = false;
    }
    public void Show(ButtonPromptData buttonPromtInfo)
    {
        gameObject.SetActive(true);
        buttonPromp.SetButtonPromptData(buttonPromtInfo);
        deactivateAbilitiesOnUI.enabled = true;
    }
    public void HideUI()
    {
        gameObject.SetActive(false);
        deactivateAbilitiesOnUI.enabled = false;
    }
}
