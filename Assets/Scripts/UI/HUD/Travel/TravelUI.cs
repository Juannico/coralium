using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TravelUI : MenuUI, IUIAdapter
{
    [SerializeField] private LevelManager levelManager;
    [SerializeField] private TravelLevelInfo travelLevelInfoPrefab;
    [SerializeField] private TextMeshProUGUI targetLevelText;
    [SerializeField] private Transform container;
    [SerializeField] private Button travelButton;
    [SerializeField] private Button exitButton;
    private LoadSceneUtility loadSceneUtility;
    private PlayerController playerController;
    private int indexLevel;
    private TravelLevelInfo[] travelLevelInfos;
    protected override void Awake()
    {
        base.Awake();
        loadSceneUtility = new LoadSceneUtility();
        
        targetLevelText.text = "Select level";
        List<TravelLevelInfo> travelLevelInfos = new List<TravelLevelInfo>();
        for (int i = 0; i < levelManager.Levels.Length; i++)
        {
            LevelData levelData = levelManager.Levels[i];
            if (!levelData.CanTravel)
                continue;
            TravelLevelInfo travelLevelInfo = Instantiate(travelLevelInfoPrefab, container);
            if (travelLevelInfo == null)
                throw new System.Exception("Travel Level Info Prefab did not have the TravelLevelInfo script assigned");
            travelLevelInfo.SetInfo(levelData);
            int index = i;
            travelLevelInfo.OnClick.AddListener(() =>
            {
                targetLevelText.text = levelData.LevelSceneName;
                indexLevel = index;
            });
            travelLevelInfos.Add(travelLevelInfo);
        }
        this.travelLevelInfos = travelLevelInfos.ToArray();
        buttons = this.travelLevelInfos;
    }
    protected override void Start()
    {
        playerController = GameObject.FindGameObjectWithTag(Tags.Player).GetComponent<PlayerController>();
    }

    public void UpdateOnInputDeviceChanged(string newDevice)
    {
        if (newDevice == Constants.GamepadString)
        {
            travelButton.interactable = false;
            exitButton.interactable = false;
            return;
        }
        travelButton.interactable = true;
        exitButton.interactable = true;
    }

    public void Travel()
    {
        if (loadSceneUtility == null)
            loadSceneUtility = new LoadSceneUtility();
        playerController.GetComponent<RigibodyForceHandle>().Rb.velocity = Vector3.zero;
        playerController.StateHandler.SwitchState(typeof(WaitStateComponent));
        levelManager.ZoneManager.UnloadLevel();
        levelManager.IsTraveling = true;
        loadSceneUtility.UnloadScene(levelManager.CurrentLevelData.LevelSceneName);
        Destroy(LevelSceneManager.Instance.gameObject);
        LoadingScreenManager.LoadSceneByName(levelManager.Levels[indexLevel].LevelSceneName, LoadSceneMode.Additive, true);
        gameObject.SetActive(false);
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();
        for (int i = 0; i < travelLevelInfos.Length; i++)
            travelLevelInfos[i].ClearData();
    }
    private void TravelInteracion(InputAction.CallbackContext ctx) => Travel();
    private void ExitInteracion(InputAction.CallbackContext ctx) => gameObject.SetActive(false);
}
