using TMPro;
using UnityEngine;

public class TravelLevelInfo : CoraliumButton
{
    [SerializeField] private TextMeshProUGUI levelName;
    [SerializeField] private GameObject locked;
    private TravelData travelData;
    public void SetInfo(LevelData levelData)
    {
        levelName.text = levelData.LevelSceneName;
        travelData = levelData.Travel;
        travelData.OnLockChanged += UnlockedLevel;
        UnlockedLevel(travelData.Locked);
    }

    public void ClearData()
    {
        travelData.OnLockChanged -= UnlockedLevel;
    }
    private void UnlockedLevel(bool lockedState)
    {
        locked.SetActive(lockedState);
        Interactable = !lockedState;
        levelName.gameObject.SetActive(!lockedState);

    }

}
