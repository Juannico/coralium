using NaughtyAttributes;
using TamarilloTools;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadGameMenu : BaseScreenUI
{
    [SerializeField] private LevelManager levelManager;
    [SerializeField] private GameData gameData;
    [Scene] [SerializeField] private string gameStartScene;
    [SerializeField] private Transform loadGameButtonsContainer;
    [Header("Buttons")]
    [SerializeField] private LoadGameButton loadGameButtonPrefab;
    private LoadGameButton[] loadGameButtons;


    protected override void Awake()
    {
        base.Awake();
        gameObject.AddComponent<UIInputAdapter>();
        bool[] dataActive = DataManager.DataActive;
        loadGameButtons = new LoadGameButton[dataActive.Length];
        for (int i = 0; i < loadGameButtons.Length; i++)
        {
            LoadGameButton loadGameButton = loadGameButtons[i] = Instantiate(loadGameButtonPrefab, loadGameButtonsContainer);
            int index = i;
            loadGameButton.OnClick.AddListener(() => LoadGame(index));
            loadGameButton.SetInfo(dataActive[index]);
            loadGameButton.DeleteButton.OnClick.AddListener(() =>
            {
                DataManager.DeleteData(index + 1);
                loadGameButton.SetInfo(dataActive[index]);
            });
        }

    }
    private void LoadGame(int index)
    {
        UIStackSystem.Instance.CloseAllContentScreens();
        bool isNewGame = !DataManager.DataActive[index];

        DataManager.LoadData(index + 1);
        DataManager.Update(gameData);
        if (isNewGame)
        {
            levelManager.ResetLevelData();
            LoadingScreenManager.LoadSceneByName(gameStartScene);
            return;
        }
        LoadingScreenManager.LoadSceneByName(levelManager.LastSpawmLevelData.LevelSceneName, LoadSceneMode.Additive, true);
    }
}
