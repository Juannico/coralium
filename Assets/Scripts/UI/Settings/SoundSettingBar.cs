using UnityEngine;

public class SoundSettingBar : SettingSliderOption
{
    [SerializeField] private BusPath volumen;
    [SerializeField] private SoundVolumenData soundVolumenData;
    private VolumenData volumenData;
    protected  void Awake()
    {
        volumenData = soundVolumenData.GetVolumenData(volumen);
        startValueText = volumenData.Volume.ToString("0.0");
    }
#if UNITY_EDITOR
    private void OnValidate()
    {
        if (soundVolumenData == null)
            return;
        volumenData = soundVolumenData.GetVolumenData(volumen);
    }
#endif
    protected override float GetSliderValue() => volumenData.Volume;
    protected override void OnValueChange(float value)
    {
        volumenData.SetVolumen(value);
        base.OnValueChange(volumenData.Volume);    
    }

}
