using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SettingSliderOption : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI textValue;
    [SerializeField] private Slider slider;
    protected string startValueText;
    private  void Start()
    {
        slider.onValueChanged.AddListener(OnValueChange);
        slider.value = GetSliderValue();
        textValue.text = startValueText;
    }

    protected virtual float GetSliderValue() => 0f;
    protected virtual void OnValueChange(float value) => textValue.text = value.ToString("0.0");

}
