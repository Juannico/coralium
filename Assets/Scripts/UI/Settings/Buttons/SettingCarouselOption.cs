using NaughtyAttributes;
using PixelCrushers;
using TamarilloTools;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class SettingCarouselOption : MonoBehaviour
{
    // KNOWN ISSUE
    // The input with controller might lose its referencve permanently if the player switches from controles to mouse 
    // This can be fixed by turning the left and right buttons of the prefab into simple images with IPointerDown handlers
    // This way the Event System can't select them and the target selection will never be null
    [SerializeField] private InputActionReference navigateOptionInput;
    [SerializeField] private GameObject selectionImage;
    [SerializeField] protected UICarousel carousel;

    protected void Awake()
    {
        carousel = GetComponentInChildren<UICarousel>();
        carousel.OnCarouselSelected.AddListener(
            () =>
            {
                PlayerInputManager.Instance.BindInputActionPerformed(navigateOptionInput, NavigateOptions);
            });
        carousel.OnCarouselDeselected.AddListener(
            () =>
            {
                PlayerInputManager.Instance.UnbindInputActionPerformed(navigateOptionInput, NavigateOptions);
            });
    }

    private void OnDisable()
    {
        PlayerInputManager.Instance.UnbindInputActionPerformed(navigateOptionInput, NavigateOptions);
    }

    public void NavigateOptions(InputAction.CallbackContext callbackContext)
    {
        if (callbackContext.ReadValue<Vector2>().x > 0)
        {
            carousel.NextPage();
        }
        else if (callbackContext.ReadValue<Vector2>().x < 0)
        {
            carousel.PreviousPage();
        }
    }
}

