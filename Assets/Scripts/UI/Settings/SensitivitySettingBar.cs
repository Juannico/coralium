using UnityEngine;

public class SensitivitySettingBar : SettingBar
{
    private float barValue;

    public float BarValue
    {
        get => barValue;
        set
        {
            barValue = value;
            barValue = Mathf.Clamp(value, 1, 10);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        BarValue = PlayerPrefs.GetFloat("Sensitivity");
        //playerMov = FindObjectOfType<PlayerMovement>();
        UpdateTextAndProgressValue();
    }

    public override void OnLeftPressed()
    {
        base.OnLeftPressed();
        BarValue -= 0.5f;
        PlayerPrefs.SetFloat("Sensitivity", BarValue);
        //if (playerMov != null) playerMov.Sensibilidad = BarValue;
        UpdateTextAndProgressValue();
    }

    public override void OnRightPressed()
    {
        base.OnRightPressed();
        BarValue += 0.5f;
        PlayerPrefs.SetFloat("Sensitivity", BarValue);
        //if (playerMov != null) playerMov.Sensibilidad = BarValue;
        UpdateTextAndProgressValue();
    }

    public void UpdateTextAndProgressValue()
    {
        fillBar.fillAmount = BarValue / 10;
        barValueText.text = BarValue.ToString();
    }
}
