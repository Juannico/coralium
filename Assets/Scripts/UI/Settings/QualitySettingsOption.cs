using UnityEngine;
using UnityEngine.EventSystems;

public class QualitySettingsOption : SettingCarouselOption
{
    // #TODO If xbox hide this option
    private int qualityIndex;

    protected void Start()
    {
        qualityIndex = QualitySettings.GetQualityLevel();
        carousel.SetActiveIndex(qualityIndex);
        carousel.OnItemChanged.AddListener(() =>
        {
            QualitySettings.SetQualityLevel(carousel.GetActiveIndex());
        });
    }
}
