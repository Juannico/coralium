using System;
using UnityEngine.Events;

[Serializable]
public class SettingOption 
{
    public TextTableFieldName SettingName;
    public UnityEvent<int> Event;


}
