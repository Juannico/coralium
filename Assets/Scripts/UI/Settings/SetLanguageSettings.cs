using PixelCrushers.DialogueSystem;
using System.Collections.Generic;
using UnityEngine;

public class SetLanguageSettings : SettingCarouselOption
{
    private Dictionary<int, string> languageEntries;
    private void Start()
    {
        languageEntries = new Dictionary<int, string>()
        { 
            { 0, "es" },
            { 1, "en" }
        };
        carousel.SetActiveIndex(DataManager.LanguageIndex);
        carousel.OnItemChanged.AddListener(() =>
        {
            DialogueManager.SetLanguage(languageEntries[carousel.GetActiveIndex()]);
            DataManager.LanguageIndex = carousel.GetActiveIndex();
        });
    }
}
