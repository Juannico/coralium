using FMODUnity;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem.UI;
using UnityEngine.UI;

public class SettingBar : MonoBehaviour
{
    [Header("Base Attributes")]
    [SerializeField] private bool isNumberBar;
    [SerializeField] private string barName;
    [Header("Base Setting Bar Components")]
    [SerializeField] private TextMeshProUGUI barNameText;
    [SerializeField] protected GameObject optionsContainer;
    [SerializeField] protected TextMeshProUGUI barValueText;
    [SerializeField] protected Image fillBar;
    [Header("Arrows")]
    [SerializeField] private GameObject leftArrow;
    [SerializeField] private GameObject rightArrow;
    protected float repeatDelay;
    protected float lastUpdate;

    public bool IsSelected { get; private set; }

    private void OnValidate()
    {
        if (barNameText == null) return;
        if (optionsContainer == null) return;
        if (barValueText == null) return;
        optionsContainer.SetActive(true);
        barValueText.gameObject.SetActive(false);
        if (isNumberBar)
        {
            optionsContainer.SetActive(false);
            barValueText.gameObject.SetActive(true);
        }
            
        barNameText.text = barName;
    }

    private void Start()
    {
        leftArrow.AddComponent<Button>().onClick.AddListener(OnLeftPressed);
        rightArrow.AddComponent<Button>().onClick.AddListener(OnRightPressed);
    }

    protected virtual void OnEnable()
    {
        repeatDelay = FindObjectOfType<InputSystemUIInputModule>().moveRepeatRate;
    }

    protected virtual void Update()
    {
        if (!IsSelected) return;
        if (Input.GetAxis("Horizontal") < 0 && lastUpdate + repeatDelay < Time.time)
        {
            OnLeftPressed();
            lastUpdate = Time.time;
        }
        else if (Input.GetAxis("Horizontal") > 0 && lastUpdate + repeatDelay < Time.time)
        {
            OnRightPressed();
            lastUpdate = Time.time;
        }
    }
    public void SelectBar()
    {
        OnSelect();
        IsSelected = true;
    }
    public void OnSelect()
    {
        leftArrow.SetActive(true);
        rightArrow.SetActive(true);
    }

    public void OnDeselect()
    {
        //Comment this while no controiller supported
        //leftArrow.SetActive(false);
        //rightArrow.SetActive(false);
        IsSelected = false;
    }
    public virtual void OnLeftPressed()
    {
        leftArrow.GetComponent<StudioEventEmitter>().Play();
    }
    public virtual void OnRightPressed() 
    {
        rightArrow.GetComponent<StudioEventEmitter>().Play();
    }
}
