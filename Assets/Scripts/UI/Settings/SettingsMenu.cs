using UnityEngine;
using UnityEngine.InputSystem;

public class SettingsMenu : BaseScreenUI, IUIAdapter
{
    [SerializeField] private CoraliumButton backButton;
    [SerializeField] private ButtonPrompt gamepadBackPrompt;
    protected override void Awake()
    {
        base.Awake();
        gameObject.AddComponent<UIInputAdapter>();
        backButton.OnClick.AddListener(() => GoBackToPreviousScreen(default));
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        UpdateOnInputDeviceChanged(PlayerInputManager.Instance.DeviceChange.CurrentControlSchema);
    }
    public void UpdateOnInputDeviceChanged(string newDevice)
    {
        if(newDevice == Constants.GamepadString)
        {
            gamepadBackPrompt.gameObject.SetActive(true);
            backButton.gameObject.SetActive(false);
        }
        else
        {
            gamepadBackPrompt.gameObject.SetActive(false);
            backButton.gameObject.SetActive(true);
        }
    }
}
