using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class BuildVersionText : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI buildVersionText;

    private void Awake()
    {
        buildVersionText.text = $"Build version {Application.version}";
    }
}
