using NaughtyAttributes;
using UnityEngine;

public class CameraSensibilityBar : SettingSliderOption
{
    [SerializeField][Expandable] private CameraData cameraData;
    protected void Awake()
    {
        startValueText = cameraData.SensibilityValue.ToString("0.0");
    }
    protected override float GetSliderValue() =>  Mathf.InverseLerp(cameraData.MinSensibility,cameraData.MaxSensibility,cameraData.SensibilityValue);
    protected override void OnValueChange(float value)
    {
        cameraData.SensibilityValue = Mathf.Lerp(cameraData.MinSensibility, cameraData.MaxSensibility, value);
        base.OnValueChange(cameraData.SensibilityValue);
    }
}
