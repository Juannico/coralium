using PixelCrushers;
using UnityEngine;

public class UpdateTextLanguage : MonoBehaviour
{
    [SerializeField] private TextTableFieldName textTableFieldName;
    [SerializeField] private LocalizeUI localizeUI;

    private void Awake()
    {
        if (localizeUI == null)
            localizeUI = ComponentUtilities.SetComponent<LocalizeUI>(gameObject);
        LocalizeUIUtilities.SetLocalizeUI(textTableFieldName,localizeUI);
    }
#if UNITY_EDITOR
    private void OnValidate()
    {
        if (localizeUI == null)
            return;
        LocalizeUIUtilities.SetLocalizeUI(textTableFieldName, localizeUI);
    }
#endif
}
