using FMODUnity;
using NaughtyAttributes;
using PixelCrushers;
using System;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[Serializable]
public class CoraliumButton : Selectable, ISubmitHandler, INavegable
{

    [Header("Button Setting")]
    public bool ExternalSet;
    [SerializeField] private TextTableFieldName textTableFieldName;
    [SerializeField] private LocalizeUI localizeUI;
    [SerializeField] protected UnityEngine.UI.Image buttonImage;
    [SerializeField] private LocalizeUI textButtonImage;

    [Header("Button Effects")]
    [SerializeField] private GameObject[] selectionImages;
    [SerializeField] private bool boldText;
    [ShowIf("boldText")] [SerializeField] protected TextMeshProUGUI buttonText;

    [Header("Buttons events")]
    public UnityEvent OnClick;
    public UnityEvent OnSelected;
    public UnityEvent OnDeselected;
    public UnityEvent<bool> OnInteractableChange;

    private StudioEventEmitter sfxEmitter;
    public bool IsSelected { get; set; }
    public bool Interactable
    {
        get => interactable;
        set
        {
            interactable = value;
            OnInteractableChange.Invoke(value);
        }
    }

    protected override void Awake()
    {
        sfxEmitter = GetComponent<StudioEventEmitter>();
        SetSelectImage(false);
        if (textTableFieldName != null && localizeUI != null)
            LocalizeUIUtilities.SetLocalizeUI(textTableFieldName, localizeUI);
        if (!boldText)
            return;
        if (buttonText == null)
            throw new Exception($"Not TextMeshProUGUI assigned to kbPromptText in {gameObject.name}");
    }
    protected override void Start()
    {
        localizeUI?.UpdateText();
    }
    protected override void OnDisable()
    {
        base.OnDisable();
        Deselected();
    }
    public override void Select()
    {
        base.Select();
        ButtonSelected();
    }
    public override void OnSelect(BaseEventData eventData)
    {
        PlayButtonSelectSFX();
        base.OnSelect(eventData);
        if (!ExternalSet)
            ButtonSelected();
    }

    public override void OnDeselect(BaseEventData eventData)
    {
        base.OnDeselect(eventData);
        if (!ExternalSet)
            Deselected();
    }
#if UNITY_EDITOR
    protected override void OnValidate()
    {
        if (Application.isPlaying)
            return;
        if (textTableFieldName == null)
            return;
        if (localizeUI == null)
            return;
        if (textTableFieldName.TextTable == null)
            return;
        if (textTableFieldName.FieldName == null)
            return;
        LocalizeUIUtilities.SetLocalizeUI(textTableFieldName, localizeUI);
    }
#endif
    public void ButtonSelected()
    {
        OnSelected.Invoke();
        IsSelected = true;
        ButtonSelectedEffect();
    }

    protected virtual void ButtonSelectedEffect()
    {
        SetSelectImage(true);

        if (boldText)
        {
            buttonText.text = $"<b>{buttonText.text}</b>";
        }
    }

    private void PlayButtonSelectSFX()
    {
        sfxEmitter?.Play();
    }

    public void Deselected()
    {
        OnDeselected?.Invoke();
        IsSelected = false;
        ButtonDeselectedEffect();

    }
    protected virtual void ButtonDeselectedEffect()
    {
        SetSelectImage(false);
        if (boldText)
            buttonText.text = buttonText.text.Replace("<b>", "").Replace("</b>", "");
    }
    public override void OnPointerEnter(PointerEventData eventData)
    {
        base.OnPointerEnter(eventData);
        if (!Interactable)
            return;
        if (!ExternalSet)
            ButtonSelected();
    }
    public override void OnPointerExit(PointerEventData eventData)
    {
        base.OnPointerExit(eventData);
        if (!Interactable)
            return;
        if (!ExternalSet)
            Deselected();
    }
    public override void OnPointerDown(PointerEventData eventData)
    {
        base.OnPointerDown(eventData);
        if (eventData.button != PointerEventData.InputButton.Left)
            return;
        Press();
    }

    public virtual void OnSubmit(BaseEventData eventData)
    {
        Press();
    }
    private void Press()
    {
        if (!IsActive() || !IsInteractable())
            return;
        UISystemProfilerApi.AddMarker("Button.onClick", this);
        OnClick.Invoke();
    }
    private void SetSelectImage(bool state)
    {
        if (selectionImages == null || selectionImages.Length <= 0)
        {
            Debug.LogWarning($"No target images when selecting this button: {gameObject.name}");
            return;
        }

        for (int i = 0; i < selectionImages.Length; i++)
        {
            selectionImages[i].SetActive(state);
        }
    }

    public void UpdateTextTableFieldName(TextTableFieldName textTableFieldName)
    {
        this.textTableFieldName = textTableFieldName;
        LocalizeUIUtilities.SetLocalizeUI(textTableFieldName, localizeUI);
    }
}