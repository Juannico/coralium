using UnityEngine;

public class SetOnSwitchSchema : MonoBehaviour
{
    [SerializeField] private bool activeOnGamepad;
    private void Start()
    {
        AddCallbacks();
        PlayerInputManager.Instance.DeviceChange.CallOnDeviceChanged();
    }
    private void OnDestroy()
    {
        RemoveCallBacks();
    }
    protected virtual void AddCallbacks()
    {
        PlayerInputManager.Instance.DeviceChange.BindToInputDeviceChanged(SwitchPromptGraphics);
    }
    protected virtual void RemoveCallBacks()
    {
        PlayerInputManager.Instance.DeviceChange.UnbindFromInputDeviceChanged(SwitchPromptGraphics);
    }
    private void SwitchPromptGraphics(string newSchema)
    {
        bool state = newSchema == "gamepad";
        state = state ? activeOnGamepad : !activeOnGamepad ;
        gameObject.SetActive(state);
    }
}
