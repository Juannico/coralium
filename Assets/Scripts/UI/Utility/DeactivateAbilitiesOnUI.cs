using UnityEngine;

public class DeactivateAbilitiesOnUI : MonoBehaviour
{
    [SerializeField] private PlayerAbilities playerAbilities;
    private GameManager gameManager;

    private void OnEnable()
    {
        SetAbilities(false);
    }
    private void OnDisable()
    {
        SetAbilities(true);
    }

    private void SetAbilities(bool state)
    {
        if (playerAbilities.Abilities == null)
            return;
        if (gameManager == null)
            gameManager = GameManager.Instance;
        bool canUnlockabBilities = true;
        if (gameManager != null)
        {
            gameManager.CanUnlockabBilities = !state;
            canUnlockabBilities = gameManager.CanUnlockabBilities;
        }
        for (int i = 0; i < playerAbilities.Abilities.Length; i++)
        {
            if (!playerAbilities.Abilities[i].Data.HasInputAction)
                continue;
            if (state)
            {
                if (canUnlockabBilities)
                {
                    playerAbilities.Abilities[i].Data.InputReference.InputAction.Enable();
                }
            }
            else
                playerAbilities.Abilities[i].Data.InputReference.InputAction.Disable();

        }
    }
}
