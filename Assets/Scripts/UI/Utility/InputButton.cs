using NaughtyAttributes;
using UnityEngine;

public class InputButton : CoraliumButton
{
    [Header("Input Settings")]
    [SerializeField] private bool useInteraction = true;
    [ShowIf("useInteraction")] public InputActionInteractionData InteractionData;
    [SerializeField] private bool externalInteractionControl = true;
    protected override void OnEnable()
    {
        if (externalInteractionControl)
            return;
        SetAction(false);
    }
    protected override void OnDisable()
    {
        if (externalInteractionControl)
            return;
        SetAction(false);
    }
    protected override void OnDestroy()
    {
        if (externalInteractionControl)
            return;
        SetAction(false);
    }
    private void Interact(UnityEngine.InputSystem.InputAction.CallbackContext ctx)
    {
        OnClick?.Invoke();
    }
    public void SetAction(bool add)
    {
        if (!useInteraction)
            return;
        if (add)
        {
            InteractionData.action.Enable();
            InteractionData.action.performed += Interact;
            return;
        }
        InteractionData.action.performed -= Interact;
        InteractionData.action.Disable();
    }
}

