using NaughtyAttributes;
using System;
using TMPro;

[Serializable]
public class PopPanelData 
{
    public TextTableFieldName Description;
    public TextTableFieldName Accept;
    public bool OneButton = false;
    public TextTableFieldName Cancel;
    public delegate void On();
    public On OnSubmit;
    public bool UseSpriteAsset;
    [AllowNesting][ShowIf("UseSpriteAsset")] public TMP_SpriteAsset spriteAsset;
}
