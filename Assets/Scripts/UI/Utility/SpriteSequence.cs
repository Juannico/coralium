using UnityEngine;
using UnityEngine.UI;

public class SpriteSequence : MonoBehaviour
{
    [SerializeField] private Image image;
    [SerializeField] private Sprite[] sprites;
    [SerializeField] private float tickRate;
    [SerializeField] private bool loop;
    [SerializeField] private bool autoStart;
    private int spriteIndex;
    private float timer;
    private void Awake()
    {
        enabled = autoStart;
        timer = 0;
        if (autoStart)
            StartAnim();
    }

    private void Update()
    {
        timer += Time.deltaTime;
        if (timer < tickRate)
            return;
        timer = 0;
        ChangeSprite();
    }
    public void StartAnim()
    {
        enabled = true;
        spriteIndex = 0;
        image.sprite = sprites[spriteIndex];
        timer = 0;
    }
    public void ContinueAnim() => enabled = true;
    public void StopAnim() => enabled = false;
    public void ChangeSprite()
    {
        spriteIndex++;
        if (spriteIndex >= sprites.Length)
        {
            if (!loop)
            {
                enabled = false;
                return;
            }
            spriteIndex = 0;
        }
        image.sprite = sprites[spriteIndex];
    }
    public float GetTickRate() => tickRate;
}
