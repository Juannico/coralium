using Cinemachine;
using UnityEngine;

public class MenuUI : BaseScreenUI
{
    private CinemachineBrain cinemachineBrain;
    private float lastTimeScaleValue = 1;
    protected override void Start()
    {
        base.Start();
        if (cinemachineBrain == null)
            SetCinemachineBrain();

    }
    protected override void OnEnable()
    {
        base.OnEnable();
        if (Time.timeScale != 0)
            lastTimeScaleValue = Time.timeScale;
        Time.timeScale = 0;
        Cursor.visible = true;
        if (cinemachineBrain == null)
        {
            SetCinemachineBrain(true, false);
            return;
        }
        cinemachineBrain.enabled = false;
    }
    protected override void OnDisable()
    {
        base.OnDisable();
        if (uiManager.UIOpened)
            return;
        Time.timeScale = lastTimeScaleValue;
        Cursor.visible = false;
        if (cinemachineBrain == null)
        {
            SetCinemachineBrain(true, true);
            return;
        }
        cinemachineBrain.enabled = true;
    }
    private void SetCinemachineBrain(bool changeState = false, bool newState = false)
    {
        Camera camera = Camera.main;
        if (camera != null)
        {
            cinemachineBrain = Camera.main.GetComponent<CinemachineBrain>();
            if (changeState)
                cinemachineBrain.enabled = newState;
            return;
        }
        CoroutineHandler.ExecuteActionAfterUpdateUntil(() =>
        {
            Camera camera = Camera.main;
            return camera != null;
        }, () =>
        {
            cinemachineBrain = Camera.main.GetComponent<CinemachineBrain>();
            if (changeState)
                cinemachineBrain.enabled = newState;
        });
    }

}
