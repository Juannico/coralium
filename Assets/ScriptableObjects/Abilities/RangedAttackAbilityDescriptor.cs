﻿using UnityEngine;

[CreateAssetMenu(fileName = "AbilityDescriptor_RangedAttack", menuName = "Coralium/Abilities/Descriptors/RangedAttack")]
public class RangedAttackAbilityDescriptor : BaseAbilityDescriptor
{
    [SerializeField] protected GameObject projectilePrefab;

    public GameObject ProjectilePrefab { get {  return projectilePrefab; } }
}