﻿using System;
using UnityEngine;

[Serializable]
public struct EnemySpawnConfig
{
    public GameObject gameObject;
    public Vector3 locationOffset;
    public Vector3 rotationOffset;
}

[CreateAssetMenu(fileName = "AbilityDescriptor_SummonEnemies", menuName = "Coralium/Abilities/Descriptors/SummonEnemies")]
public class SummonEnemiesAbilityDescriptor : BaseAbilityDescriptor
{
    [SerializeField] protected EnemySpawnConfig[] enemiesToSummon;

    public EnemySpawnConfig[] EnemiesToSummon { get { return enemiesToSummon; } }
}