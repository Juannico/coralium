using UnityEngine;

/*
 * An AbilityDescriptor is a ScriptableObject used to define the static data that the ability should be using during execution.
 */
public abstract class BaseAbilityDescriptor : ScriptableObject
{
    [SerializeField] protected float abilityCooldown;
    [SerializeField] protected GameObject[] abilityFXs;

    public float AbilityCooldown { get { return abilityCooldown; } }
    public GameObject[] AbilityFXs { get {  return abilityFXs; } }
}