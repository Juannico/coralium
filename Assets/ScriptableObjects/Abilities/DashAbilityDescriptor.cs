﻿using UnityEngine;

[CreateAssetMenu(fileName = "AbilityDescriptor_Dash", menuName = "Coralium/Abilities/Descriptors/Dash")]
public class DashAbilityDescriptor : BaseAbilityDescriptor
{
    // The speed of the dash attack
    [SerializeField] protected float dashSpeed = 100.0f;
    [SerializeField] protected float dashChargeDuration = 0.5f;
    [SerializeField] protected float dashLengthMultiplier = 1.25f;

    public float DashSpeed { get { return dashSpeed; } }
    public float DashChargeDuration { get { return dashChargeDuration; } }
    public float DashLengthMultiplier { get {  return dashLengthMultiplier; } }
}